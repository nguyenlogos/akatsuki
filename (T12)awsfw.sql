-- Adminer 4.6.3 PostgreSQL dump

DROP TABLE IF EXISTS "admin_configs";
DROP SEQUENCE IF EXISTS admin_configs_id_seq;
CREATE SEQUENCE admin_configs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."admin_configs" (
    "id" integer DEFAULT nextval('admin_configs_id_seq') NOT NULL,
    "key" character varying(64) NOT NULL,
    "value" character varying(256)
) WITH (oids = false);


DROP TABLE IF EXISTS "ami";
CREATE TABLE "public"."ami" (
    "cid" integer,
    "imageid" character varying(128),
    "imagelocation" character varying(1024),
    "state" character varying(256),
    "ownerid" character varying(128),
    "public" character varying(32),
    "platform" character varying(32),
    "architecture" character varying(32),
    "imagetype" character varying(32),
    "kernelid" character varying(64),
    "ramdiskid" character varying(64),
    "imageowneralias" character varying(64),
    "name" character varying(1024),
    "description" character varying(1024),
    "rootdevicetype" character varying(64),
    "rootdevicename" character varying(64),
    "virtualizationtype" character varying(64),
    "hypervisor" character varying(64),
    "lastchecked" timestamp,
    "region" character varying(128)
) WITH (oids = false);


DROP TABLE IF EXISTS "ami_enable";
DROP SEQUENCE IF EXISTS ami_enable_id_seq;
CREATE SEQUENCE ami_enable_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."ami_enable" (
    "cid" integer NOT NULL,
    "imageid" character varying(128) NOT NULL,
    "id" integer DEFAULT nextval('ami_enable_id_seq') NOT NULL,
    CONSTRAINT "ami_enable_cid_imageid_key" UNIQUE ("cid", "imageid"),
    CONSTRAINT "ami_enable_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "ami_tags";
CREATE TABLE "public"."ami_tags" (
    "cid" integer,
    "imageid" character varying(128),
    "key" character varying(512),
    "value" character varying(512)
) WITH (oids = false);


DROP TABLE IF EXISTS "azone";
CREATE TABLE "public"."azone" (
    "cid" integer,
    "name" character varying(128),
    "state" character varying(32),
    "regionname" character varying(128),
    "lastchecked" timestamp
) WITH (oids = false);


DROP TABLE IF EXISTS "azone_enable";
DROP SEQUENCE IF EXISTS azone_enable_id_seq;
CREATE SEQUENCE azone_enable_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."azone_enable" (
    "cid" integer NOT NULL,
    "regionname" character varying(128) NOT NULL,
    "id" integer DEFAULT nextval('azone_enable_id_seq') NOT NULL,
    CONSTRAINT "azone_enable_cid_name_key" UNIQUE ("cid", "regionname"),
    CONSTRAINT "azone_enable_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "configs";
DROP SEQUENCE IF EXISTS configs_id_seq;
CREATE SEQUENCE configs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."configs" (
    "id" integer DEFAULT nextval('configs_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "insttype_curgen" smallint DEFAULT '0' NOT NULL,
    "pass_lenght" integer DEFAULT '4' NOT NULL,
    "pass_expired" integer DEFAULT '0' NOT NULL,
    "pass_lowercase" integer DEFAULT '0' NOT NULL,
    "pass_number" integer DEFAULT '0' NOT NULL,
    "mail_address" character varying(255),
    "admin_active" integer DEFAULT '600' NOT NULL,
    "lock_failed" integer DEFAULT '5' NOT NULL,
    "instance_expires" integer DEFAULT '30' NOT NULL,
    "check_sys_admin" integer DEFAULT '1' NOT NULL,
    "check_manager" integer DEFAULT '1' NOT NULL,
    "email_instance" integer DEFAULT '0' NOT NULL,
    "days_apply" integer DEFAULT '0' NOT NULL,
    "timezone" character varying DEFAULT '9',
    "timezoneid" smallint DEFAULT '69'
) WITH (oids = false);


DROP TABLE IF EXISTS "cost";
CREATE TABLE "public"."cost" (
    "cid" integer,
    "lineitemtype" character varying(64),
    "usagestartdate" timestamp,
    "usageenddate" timestamp,
    "productcode" character varying(64),
    "usagetype1" character varying(64),
    "operation1" character varying(64),
    "resourceid" character varying(128),
    "usageamount" numeric,
    "currencycode" character varying(64),
    "unblendedcost" numeric,
    "blendedcost" numeric,
    "lineitemdescription" character varying(512),
    "instancetype" character varying(64),
    "location" character varying(128),
    "memory" character varying(64),
    "networkperformance" character varying(64),
    "normalizationsizefactor" numeric,
    "operatingsystem" character varying(64),
    "operation2" character varying(64),
    "physicalprocessor" character varying(64),
    "preinstalledsw" character varying(256),
    "processorarchitecture" character varying(64),
    "processorfeatures" character varying(64),
    "region" character varying(64),
    "servicename" character varying(128),
    "transfertype" character varying(128),
    "usagetype2" character varying(128),
    "publicondemandcost" numeric,
    "publicondemandrate" numeric,
    "term" character varying(64),
    "unit" character varying(128),
    "usageaccountid" character varying(64),
    "servicecode" character varying(128),
    "productfamily" character varying(128),
    "loaddate" timestamp,
    "ondemandusagecost" numeric,
    "riusagecost" numeric,
    "riflag" smallint DEFAULT '0'
) WITH (oids = false);

CREATE INDEX "idx_cost_1" ON "public"."cost" USING btree ("cid");

CREATE INDEX "idx_cost_2" ON "public"."cost" USING btree ("usageaccountid");


DROP TABLE IF EXISTS "cost_detail";
CREATE TABLE "public"."cost_detail" (
    "cid" integer,
    "ym" character varying(7),
    "lineitemtype" character varying(64),
    "region" character varying(64),
    "productcode" character varying(64),
    "productfamily" character varying(128),
    "lineitemdescription" character varying(512),
    "preinstalledsw" character varying(256),
    "operatingsystem" character varying(64),
    "unit" character varying(128),
    "unblendedcost" numeric,
    "blendedcost" numeric,
    "usageamount" numeric,
    "unblendedrate" numeric,
    "blendedrate" numeric,
    "usagetype" character varying(64),
    "org_unblendedcost" numeric,
    "org_blendedcost" numeric,
    "org_usageamount" numeric,
    "billingentity" character varying(128)
) WITH (oids = false);

CREATE INDEX "idx_cost_detail01" ON "public"."cost_detail" USING btree ("cid");

CREATE INDEX "idx_cost_detail02" ON "public"."cost_detail" USING btree ("ym");


DROP TABLE IF EXISTS "cost_ri";
CREATE TABLE "public"."cost_ri" (
    "accountname" character varying(64),
    "subscriptionid" character varying(256),
    "reservationid" character varying(256),
    "instancetype" character varying(64),
    "riutilization" numeric,
    "rihourspurchased" numeric,
    "rihoursused" numeric,
    "rihoursunused" numeric,
    "accountid" character varying(256),
    "startdate" timestamp,
    "enddate" timestamp,
    "numberofris" numeric,
    "scope" character varying(64),
    "region" character varying(64),
    "availabilityzone" character varying(64),
    "platform" character varying(64),
    "tenancy" character varying(64),
    "paymentoption" character varying(64),
    "offeringtype" character varying(64),
    "ondemandcostequivalent" numeric,
    "amortizedupfrontfees" numeric,
    "amortizedrecurringcharges" numeric,
    "effectivericost" numeric,
    "netsavings" numeric,
    "potentialsavings" numeric,
    "averageondemandrate" numeric,
    "totalassetvalue" numeric,
    "effectivehourlyrate" numeric,
    "upfrontfee" numeric,
    "hourlyrecurringfee" numeric,
    "ricostforunusedhours" numeric
) WITH (oids = false);


DROP TABLE IF EXISTS "cw_logs";
DROP SEQUENCE IF EXISTS cw_logs_id_seq;
CREATE SEQUENCE cw_logs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."cw_logs" (
    "id" integer DEFAULT nextval('cw_logs_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "stream" character varying(64) NOT NULL,
    "stream_time" timestamp NOT NULL,
    "event" text NOT NULL,
    "event_time" timestamp NOT NULL,
    "date_created" timestamp NOT NULL,
    "error_flag" smallint DEFAULT '0',
    "description" text
) WITH (oids = false);


DROP TABLE IF EXISTS "cw_rules";
DROP SEQUENCE IF EXISTS cw_rules_id_seq;
CREATE SEQUENCE cw_rules_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."cw_rules" (
    "id" integer DEFAULT nextval('cw_rules_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "name" character varying NOT NULL,
    "expr" character varying(512) NOT NULL,
    "region" character varying(32) NOT NULL,
    "state" smallint DEFAULT '1' NOT NULL,
    "notes" text,
    "status" integer DEFAULT '0',
    "dept" integer NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "dept";
DROP SEQUENCE IF EXISTS dept_id_seq;
CREATE SEQUENCE dept_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."dept" (
    "cid" integer,
    "dept" integer,
    "deptname" character varying(128) NOT NULL,
    "regdate" timestamp,
    "status" integer DEFAULT '0',
    "disporder" integer,
    "id" integer DEFAULT nextval('dept_id_seq') NOT NULL,
    CONSTRAINT "dept_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "ebs_log";
DROP SEQUENCE IF EXISTS ebs_log_id_seq;
CREATE SEQUENCE ebs_log_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."ebs_log" (
    "id" integer DEFAULT nextval('ebs_log_id_seq') NOT NULL,
    "instid" character varying(32) NOT NULL,
    "ebs_req_id" integer NOT NULL,
    "size" integer NOT NULL,
    "iops" integer NOT NULL,
    "zone" character varying(32) NOT NULL,
    "state" character varying(32) NOT NULL,
    "type" character varying(16) NOT NULL,
    "device" character varying(32) NOT NULL,
    "is_root" smallint NOT NULL,
    "encrypted" smallint NOT NULL,
    "del_term" smallint NOT NULL,
    "can_modify" smallint NOT NULL,
    "attached" smallint NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "ebs_req";
DROP SEQUENCE IF EXISTS ebs_req_id_seq;
CREATE SEQUENCE ebs_req_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."ebs_req" (
    "id" integer DEFAULT nextval('ebs_req_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "empid" integer NOT NULL,
    "instance_id" character varying(32) NOT NULL,
    "updated_at" timestamp NOT NULL,
    "status" integer DEFAULT '0' NOT NULL,
    "note" character varying(512)
) WITH (oids = false);


DROP TABLE IF EXISTS "ebs_req_detail";
DROP SEQUENCE IF EXISTS ebs_req_detail_id_seq;
CREATE SEQUENCE ebs_req_detail_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."ebs_req_detail" (
    "id" integer DEFAULT nextval('ebs_req_detail_id_seq') NOT NULL,
    "ebs_req_id" integer NOT NULL,
    "volume_id" character varying(32) NOT NULL,
    "volume_zone" character varying(32) NOT NULL,
    "volume_type" character varying(16) NOT NULL,
    "volume_size" integer NOT NULL,
    "volume_iops" integer DEFAULT '0',
    "device_name" character varying(16) NOT NULL,
    "state" character varying(16) NOT NULL,
    "attached" integer DEFAULT '0',
    "autoremove" integer DEFAULT '0',
    "change_type" character varying(16) NOT NULL,
    "status" integer DEFAULT '0' NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "emp";
DROP SEQUENCE IF EXISTS emp_id_seq;
CREATE SEQUENCE emp_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."emp" (
    "cid" integer,
    "empid" integer,
    "email" character varying(64),
    "pass" character varying(64),
    "dept" integer,
    "name" character varying(64),
    "memo1" character varying(128),
    "memo2" character varying(128),
    "regdate" timestamp,
    "lastlogin" timestamp,
    "status" integer,
    "regkey" character varying(32),
    "sysadmin" integer,
    "admin" integer,
    "disporder" integer,
    "infocount" integer,
    "wfcount" integer,
    "idsadmin" integer DEFAULT '0' NOT NULL,
    "id" integer DEFAULT nextval('emp_id_seq') NOT NULL,
    "email_confirmed" integer DEFAULT '0',
    "pw_reset_token" character varying(32),
    "pw_reset_token_time" timestamp,
    "login_initial" integer DEFAULT '0',
    "number_lock" integer DEFAULT '0',
    "email_lock" integer DEFAULT '0',
    "show_privacy" integer DEFAULT '1',
    CONSTRAINT "emp_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_emp" ON "public"."emp" USING btree ("cid");

CREATE INDEX "idx_emp2" ON "public"."emp" USING btree ("empid");


DROP TABLE IF EXISTS "emp_proj";
DROP SEQUENCE IF EXISTS emp_proj_id_seq;
CREATE SEQUENCE emp_proj_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."emp_proj" (
    "id" integer DEFAULT nextval('emp_proj_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "empid" integer NOT NULL,
    "pcode" character varying(128) NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "info";
DROP SEQUENCE IF EXISTS info_mid_seq;
CREATE SEQUENCE info_mid_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."info" (
    "rank" integer,
    "senddate" timestamp,
    "title" character varying(256),
    "message" text,
    "toall" integer,
    "mid" integer DEFAULT nextval('info_mid_seq') NOT NULL,
    "id_notif" integer,
    CONSTRAINT "info_pkey" PRIMARY KEY ("mid")
) WITH (oids = false);

CREATE INDEX "idx_info01" ON "public"."info" USING btree ("senddate");

CREATE INDEX "idx_info02" ON "public"."info" USING btree ("toall");


DROP TABLE IF EXISTS "info_ctl";
CREATE TABLE "public"."info_ctl" (
    "cid" integer,
    "uid" character varying(64),
    "mid" integer,
    "readdate" timestamp
) WITH (oids = false);

CREATE INDEX "idx_infoctl01" ON "public"."info_ctl" USING btree ("cid");

CREATE INDEX "idx_infoctl02" ON "public"."info_ctl" USING btree ("uid");


DROP TABLE IF EXISTS "inst_req";
DROP SEQUENCE IF EXISTS inst_req_id_seq;
CREATE SEQUENCE inst_req_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."inst_req" (
    "id" integer DEFAULT nextval('inst_req_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "dept" integer NOT NULL,
    "pcode" character varying(128) NOT NULL,
    "inst_ami" character varying(128) NOT NULL,
    "inst_type" character varying(32) NOT NULL,
    "inst_count" integer NOT NULL,
    "inst_region" character varying(128) NOT NULL,
    "security_groups" character varying(256),
    "using_purpose" character varying(512) NOT NULL,
    "using_from_date" date NOT NULL,
    "using_till_date" date NOT NULL,
    "shutdown_behavior" character varying(16) NOT NULL,
    "approved_status" smallint DEFAULT '0' NOT NULL,
    "req_date" timestamp NOT NULL,
    "empid" integer,
    "note" character varying(512),
    "inst_vpc" character varying(128),
    "inst_subnet" character varying(128),
    "expired_notified" smallint DEFAULT '0',
    "key_name" character varying(128),
    "inst_address" character varying(32),
    "status" smallint,
    "inst_region_text" character varying(128),
    "inst_vpc_text" character varying(128),
    "inst_subnet_text" character varying(128),
    "inst_ami_text" character varying(128),
    CONSTRAINT "inst_req_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "inst_req_bs";
DROP SEQUENCE IF EXISTS inst_req_bs_id_seq;
CREATE SEQUENCE inst_req_bs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."inst_req_bs" (
    "id" integer DEFAULT nextval('inst_req_bs_id_seq') NOT NULL,
    "inst_req_id" integer NOT NULL,
    "device_name" character varying(32) NOT NULL,
    "volumn_type" character varying(16) NOT NULL,
    "volumn_size" integer NOT NULL,
    "iops" integer,
    "encrypted" smallint DEFAULT '0',
    "auto_removal" smallint DEFAULT '1',
    "snapshot_id" character varying(32)
) WITH (oids = false);


DROP TABLE IF EXISTS "inst_req_change";
DROP SEQUENCE IF EXISTS inst_req_change_id_seq;
CREATE SEQUENCE inst_req_change_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."inst_req_change" (
    "id" integer DEFAULT nextval('inst_req_change_id_seq') NOT NULL,
    "inst_req_id" integer NOT NULL,
    "cid" integer NOT NULL,
    "empid" integer NOT NULL,
    "dept" integer,
    "pcode" character varying(128),
    "using_purpose" character varying(512),
    "using_from_date" date NOT NULL,
    "using_till_date" date NOT NULL,
    "inst_address" character varying(32)
) WITH (oids = false);


DROP TABLE IF EXISTS "instance";
DROP SEQUENCE IF EXISTS instance_tbl_id_seq;
CREATE SEQUENCE instance_tbl_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."instance" (
    "cid" integer,
    "id" character varying(128),
    "name" character varying(256),
    "type" character varying(64),
    "sg" character varying(128),
    "platform" character varying(128),
    "kernelid" character varying(128),
    "ramdiskid" character varying(128),
    "imageid" character varying(128),
    "state" character varying(128),
    "privatednsname" character varying(256),
    "publicdnsname" character varying(256),
    "launchtime" character varying(128),
    "availabilityzone" character varying(256),
    "privateip" character varying(64),
    "publicip" character varying(64),
    "rootdevicename" character varying(128),
    "lastchecked" timestamp,
    "pcode" character varying(128),
    "inst_req_id" integer,
    "elasticip" character varying(64),
    "tbl_id" integer DEFAULT nextval('instance_tbl_id_seq') NOT NULL,
    "interface_id" character varying(64),
    CONSTRAINT "idx_instance01" UNIQUE ("cid", "id")
) WITH (oids = false);

CREATE INDEX "idx_instance02" ON "public"."instance" USING btree ("cid");

CREATE INDEX "idx_instance03" ON "public"."instance" USING btree ("id");

CREATE INDEX "idx_instance04" ON "public"."instance" USING btree ("sg");


DROP TABLE IF EXISTS "instance_request";
CREATE TABLE "public"."instance_request" (
    "cid" integer,
    "reqid" integer,
    "instance_id" integer,
    "createdate" timestamp,
    "deletedate" timestamp
) WITH (oids = false);


DROP TABLE IF EXISTS "insttype";
DROP SEQUENCE IF EXISTS insttype_id_seq;
CREATE SEQUENCE insttype_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."insttype" (
    "curgen" character varying(3),
    "family" character varying(32),
    "name" character varying(32),
    "cpu" character varying(3),
    "memory" character varying(5),
    "storage" character varying(32),
    "ebsopt" character varying(3),
    "netperf" character varying(32),
    "cid" integer,
    "price" numeric DEFAULT '0',
    "region" character varying(128),
    "effdate" date,
    "id" integer DEFAULT nextval('insttype_id_seq') NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "insttype_enable";
DROP SEQUENCE IF EXISTS insttype_enable_id_seq;
CREATE SEQUENCE insttype_enable_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."insttype_enable" (
    "cid" integer NOT NULL,
    "name" character varying(32) NOT NULL,
    "id" integer DEFAULT nextval('insttype_enable_id_seq') NOT NULL,
    "region" character varying(128),
    CONSTRAINT "insttype_enable_cid_name_region" UNIQUE ("cid", "name", "region")
) WITH (oids = false);


DROP TABLE IF EXISTS "insttype_pricing";
DROP SEQUENCE IF EXISTS insttype_pricing_id_seq;
CREATE SEQUENCE insttype_pricing_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."insttype_pricing" (
    "cid" integer,
    "name" character varying(128),
    "region" character varying(128),
    "price" numeric,
    "id" integer DEFAULT nextval('insttype_pricing_id_seq') NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "insttype_temp";
DROP SEQUENCE IF EXISTS insttype_temp_id_seq;
CREATE SEQUENCE insttype_temp_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."insttype_temp" (
    "id" integer DEFAULT nextval('insttype_temp_id_seq') NOT NULL,
    "name" character varying(32),
    "region" character varying(128),
    "family" character varying(32),
    "cpu" character varying(3),
    "memory" integer,
    "storage" character varying(32),
    "os" character varying(64),
    "netperf" character varying(32),
    "curgen" character varying(3),
    "ebsopt" character varying(3),
    "termtype" character varying(64),
    "effdate" date
) WITH (oids = false);


DROP TABLE IF EXISTS "invoice";
CREATE TABLE "public"."invoice" (
    "cid" integer,
    "ym" character varying(6),
    "refno" character varying(11),
    "usageusd" numeric,
    "usagejpy" numeric,
    "taxusd" numeric,
    "taxjpy" numeric,
    "totalusd" numeric,
    "totaljpy" numeric,
    "rate" numeric,
    "calcdate" timestamp,
    "paid" integer,
    "filename" character varying(128),
    "refnoserial" integer,
    "blended_usageusd" numeric,
    "blended_usagejpy" numeric,
    "blended_taxusd" numeric,
    "blended_taxjpy" numeric,
    "blended_totalusd" numeric,
    "blended_totaljpy" numeric,
    "idsfee_usd" numeric,
    "idsfee_jpy" numeric,
    "creditusd" numeric,
    "creditjpy" numeric,
    "duedate" character varying(128),
    "address1" character varying(128),
    "address2" character varying(128),
    "address3" character varying(128),
    "costacctname" character varying(128),
    CONSTRAINT "inv_001" UNIQUE ("cid", "ym")
) WITH (oids = false);


DROP TABLE IF EXISTS "key_pair";
DROP SEQUENCE IF EXISTS key_pair_id_seq;
CREATE SEQUENCE key_pair_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."key_pair" (
    "id" integer DEFAULT nextval('key_pair_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "name_key" character varying NOT NULL,
    "file_key_pair" character varying(512) NOT NULL,
    "create_time" timestamp
) WITH (oids = false);


DROP TABLE IF EXISTS "logs";
CREATE TABLE "public"."logs" (
    "logdate" timestamp,
    "cid" integer,
    "email" character varying(64),
    "name" character varying(64),
    "act" text
) WITH (oids = false);


DROP TABLE IF EXISTS "obj";
DROP SEQUENCE IF EXISTS obj_id_seq;
CREATE SEQUENCE obj_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."obj" (
    "cid" integer NOT NULL,
    "name" character varying(128) NOT NULL,
    "ipaddr" character varying(15) NOT NULL,
    "submask" character varying(2) NOT NULL,
    "description" character varying(1024),
    "id" integer DEFAULT nextval('obj_id_seq') NOT NULL,
    "status" integer DEFAULT '0' NOT NULL,
    CONSTRAINT "obj_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "plan";
DROP SEQUENCE IF EXISTS plan_id_seq;
CREATE SEQUENCE plan_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."plan" (
    "id" integer DEFAULT nextval('plan_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "plan" character varying(10) NOT NULL,
    "startdate" timestamp,
    "enddate" timestamp,
    "startuid" character varying(64) NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "proj";
DROP SEQUENCE IF EXISTS proj_id_seq;
CREATE SEQUENCE proj_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."proj" (
    "cid" integer NOT NULL,
    "pcode" character varying(128) NOT NULL,
    "pname" character varying(256),
    "regdate" timestamp NOT NULL,
    "status" integer DEFAULT '0' NOT NULL,
    "disporder" integer,
    "memo" text,
    "dept" integer NOT NULL,
    "id" integer DEFAULT nextval('proj_id_seq') NOT NULL,
    CONSTRAINT "proj_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_proj01" ON "public"."proj" USING btree ("cid");

CREATE INDEX "idx_proj02" ON "public"."proj" USING btree ("disporder");


DROP TABLE IF EXISTS "public.cost";
CREATE TABLE "public"."public.cost" (
    "cid" integer,
    "lineitemtype" character varying(1024),
    "usagestartdate" timestamp,
    "usageenddate" timestamp,
    "productcode" character varying(1024),
    "li_usagetype" character varying(1024),
    "li_operation" character varying(1024),
    "resourceid" character varying(1024),
    "usageamount" numeric,
    "currencycode" character varying(32),
    "unblendedcost" numeric,
    "blendedcost" numeric,
    "lineitemdescription" character varying(1024),
    "instancetype" character varying(128),
    "location" character varying(128),
    "memory" character varying(128),
    "networkperformance" character varying(128),
    "normalizationsizefactor" numeric,
    "operatingsystem" character varying(1024),
    "pd_operation" character varying(1024),
    "physicalprocessor" character varying(1024),
    "preinstalledsw" character varying(1024),
    "processorarchitecture" character varying(1024),
    "processorfeatures" character varying(1024),
    "region" character varying(128),
    "servicename" character varying(1024),
    "transfertype" character varying(128),
    "pd_usagetype" character varying(128),
    "publicondemandcost" numeric,
    "publicondemandrate" numeric,
    "term" character varying(128),
    "unit" character varying(128),
    "usageaccountid" character varying(128),
    "servicecode" character varying(128),
    "productfamily" character varying(1024),
    "procdate" timestamp
) WITH (oids = false);


DROP TABLE IF EXISTS "rate";
CREATE TABLE "public"."rate" (
    "ym" character varying(7),
    "dy" numeric,
    CONSTRAINT "idx_rate1" UNIQUE ("ym")
) WITH (oids = false);


DROP TABLE IF EXISTS "request";
CREATE TABLE "public"."request" (
    "cid" integer,
    "reqid" integer,
    "from_uid" character varying(64),
    "to_uid" character varying(64),
    "dept" integer,
    "reqdate" timestamp,
    "reqauthdate" timestamp,
    "reqngdate" timestamp,
    "reqcanceldate" timestamp,
    "from_comment" text,
    "to_comment" text
) WITH (oids = false);


DROP TABLE IF EXISTS "request_detail";
CREATE TABLE "public"."request_detail" (
    "cid" integer,
    "reqid" integer,
    "name" character varying(256),
    "type" character varying(64),
    "sg" character varying(128),
    "platform" character varying(128),
    "kernelid" character varying(128),
    "ramdiskid" character varying(128),
    "imageid" character varying(128),
    "availabilityzone" character varying(256),
    "privateip" character varying(64),
    "publicip" character varying(64)
) WITH (oids = false);


DROP TABLE IF EXISTS "security_group";
DROP SEQUENCE IF EXISTS security_group_id_seq;
CREATE SEQUENCE security_group_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."security_group" (
    "id" integer DEFAULT nextval('security_group_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "name_sg" character varying(255) NOT NULL,
    "description_sg" character varying(255) NOT NULL,
    "vpc_sg" character varying(1024) NOT NULL,
    "create_time" timestamp,
    "inst_req_id" integer,
    "sg_id" character varying
) WITH (oids = false);


DROP TABLE IF EXISTS "sg";
DROP SEQUENCE IF EXISTS sg_tbl_id_seq;
CREATE SEQUENCE sg_tbl_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."sg" (
    "cid" integer,
    "id" character varying(128),
    "name" character varying(128),
    "lastchecked" timestamp,
    "description" character varying,
    "tbl_id" integer DEFAULT nextval('sg_tbl_id_seq') NOT NULL,
    "vpcid" character varying(32)
) WITH (oids = false);

CREATE INDEX "idx_sg01" ON "public"."sg" USING btree ("cid");

CREATE INDEX "idx_sg02" ON "public"."sg" USING btree ("id");


DROP TABLE IF EXISTS "sg_enable";
DROP SEQUENCE IF EXISTS sg_enable_id_seq;
CREATE SEQUENCE sg_enable_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."sg_enable" (
    "id" integer DEFAULT nextval('sg_enable_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "sg_id" character varying(128) NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "sg_inbound";
DROP SEQUENCE IF EXISTS sg_inbound_id_seq;
CREATE SEQUENCE sg_inbound_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."sg_inbound" (
    "id" integer DEFAULT nextval('sg_inbound_id_seq') NOT NULL,
    "id_security_group" integer NOT NULL,
    "inb_type" character varying(255) NOT NULL,
    "inb_protocol" character varying(64) NOT NULL,
    "inb_cidr_ip" character varying(64) NOT NULL,
    "inb_source" character varying(32) NOT NULL,
    "inb_description" character varying(1024) NOT NULL,
    "create_time" timestamp,
    "inb_formport_range" character varying(64),
    "inb_toport_range" character varying(64)
) WITH (oids = false);


DROP TABLE IF EXISTS "sg_ipperm";
DROP SEQUENCE IF EXISTS sg_ipperm_tbl_id_seq;
CREATE SEQUENCE sg_ipperm_tbl_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."sg_ipperm" (
    "cid" integer,
    "id" character varying(128),
    "protocol" character varying(64),
    "fromport" integer,
    "toport" integer,
    "iprange" character varying(64),
    "lastchecked" timestamp,
    "type" character varying(16),
    "tbl_id" integer DEFAULT nextval('sg_ipperm_tbl_id_seq') NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "sg_outbound";
DROP SEQUENCE IF EXISTS sg_outbound_id_seq;
CREATE SEQUENCE sg_outbound_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."sg_outbound" (
    "id" integer DEFAULT nextval('sg_outbound_id_seq') NOT NULL,
    "id_security_group" integer NOT NULL,
    "outb_type" character varying(255) NOT NULL,
    "outb_protocol" character varying(64) NOT NULL,
    "outb_cidr_ip" character varying(64) NOT NULL,
    "outb_destination" character varying(32) NOT NULL,
    "outb_description" character varying(1024) NOT NULL,
    "create_time" timestamp,
    "outb_formport_range" character varying(64),
    "outb_toport_range" character varying(64)
) WITH (oids = false);


DROP TABLE IF EXISTS "sg_temp";
DROP SEQUENCE IF EXISTS sg_temp_id_seq;
CREATE SEQUENCE sg_temp_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."sg_temp" (
    "id" integer DEFAULT nextval('sg_temp_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "sg_id" character varying(32),
    "sg_vpc" character varying(32),
    "sg_name" character varying(128),
    "sg_desc" character varying(128),
    "inst_req_id" integer NOT NULL,
    "is_created" smallint DEFAULT '0'
) WITH (oids = false);


DROP TABLE IF EXISTS "sg_temp_rules";
DROP SEQUENCE IF EXISTS sg_temp_rules_id_seq;
CREATE SEQUENCE sg_temp_rules_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."sg_temp_rules" (
    "id" integer DEFAULT nextval('sg_temp_rules_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "sg_temp_id" integer NOT NULL,
    "type" character varying(32) NOT NULL,
    "stype" character varying NOT NULL,
    "proto" character varying(32) NOT NULL,
    "range" character varying(32) NOT NULL,
    "source" character varying(32) NOT NULL,
    "target" character varying(64) NOT NULL,
    "desc" character varying(128),
    "pfrom" integer,
    "pto" integer
) WITH (oids = false);


DROP TABLE IF EXISTS "stat_cost_daily";
CREATE TABLE "public"."stat_cost_daily" (
    "cid" integer,
    "ymd" character varying(10),
    "servicecode" character varying(128),
    "ubcost" numeric,
    "bcost" numeric,
    "usagequantity" numeric,
    "currencycode" character varying(32),
    "estimated" boolean,
    "dept" integer,
    "pcode" character varying(128)
) WITH (oids = false);


DROP TABLE IF EXISTS "stat_ebs";
CREATE TABLE "public"."stat_ebs" (
    "cid" integer,
    "ym" character varying(7),
    "count" integer,
    "gib" numeric,
    "dept" integer,
    "pcode" character varying(128),
    CONSTRAINT "idx_stat_ebs1" UNIQUE ("cid", "ym")
) WITH (oids = false);


DROP TABLE IF EXISTS "stat_inst";
CREATE TABLE "public"."stat_inst" (
    "cid" integer,
    "ym" character varying(7),
    "count" integer,
    "instancetype" character varying(128),
    "dept" integer,
    "pcode" character varying(128),
    CONSTRAINT "idx_stat_inst1" UNIQUE ("cid", "ym", "instancetype")
) WITH (oids = false);


DROP TABLE IF EXISTS "stat_s3";
CREATE TABLE "public"."stat_s3" (
    "cid" integer,
    "ym" character varying(7),
    "count" integer,
    "gib" numeric,
    "dept" integer,
    "pcode" character varying(128),
    CONSTRAINT "idx_stat_s31" UNIQUE ("cid", "ym")
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_exchangerate";
DROP SEQUENCE IF EXISTS tbl_exchangerate_id_seq;
CREATE SEQUENCE tbl_exchangerate_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."tbl_exchangerate" (
    "id" integer DEFAULT nextval('tbl_exchangerate_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "empid" integer NOT NULL,
    "email" character varying(64),
    "num_rate" integer NOT NULL,
    "code_usd" character varying(16),
    "code_jpy" character varying(16),
    "count_rate" numeric,
    "status" integer DEFAULT '0' NOT NULL,
    "date_created" date NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_lambda";
DROP SEQUENCE IF EXISTS tbl_lambda_id_seq;
CREATE SEQUENCE tbl_lambda_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."tbl_lambda" (
    "id" integer DEFAULT nextval('tbl_lambda_id_seq') NOT NULL,
    "cid" integer,
    "pcode" character varying(256) NOT NULL,
    "inst_id" character varying(128) NOT NULL,
    "volumn_device" character varying(128) NOT NULL,
    "volumn_volid" character varying(128) NOT NULL,
    "volumn_size" integer NOT NULL,
    "volumn_voltype" character varying(32) NOT NULL,
    "time_gener" character varying(64) NOT NULL,
    "hours_gener" integer,
    "minute_gener" integer,
    "dai_gener" integer,
    "week_gener" character varying(32) NOT NULL,
    "day_gener" integer,
    "month_gener" integer,
    "num_gener" integer,
    "create_time" timestamp,
    "next_time" timestamp,
    "hours_dai_gener" integer,
    "minute_dai_gener" integer,
    "hours_week_gener" integer,
    "minute_week_gener" integer,
    "hours_month_gener" integer,
    "minute_month_gener" integer
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_logs";
DROP SEQUENCE IF EXISTS tbl_logs_id_seq;
CREATE SEQUENCE tbl_logs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."tbl_logs" (
    "id" integer DEFAULT nextval('tbl_logs_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "empid" integer NOT NULL,
    "category" character varying(16) NOT NULL,
    "action" character varying(16),
    "description" text NOT NULL,
    "error_flag" smallint DEFAULT '0' NOT NULL,
    "date_created" timestamp NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_notif";
DROP SEQUENCE IF EXISTS tbl_notif_id_seq;
CREATE SEQUENCE tbl_notif_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."tbl_notif" (
    "id" integer DEFAULT nextval('tbl_notif_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "empid" integer NOT NULL,
    "notif_title" character varying(128) NOT NULL,
    "notif_msg" text NOT NULL,
    "notif_time" timestamp NOT NULL,
    "publish_date" timestamp NOT NULL,
    "notif_status" integer DEFAULT '0' NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "tbl_roles";
DROP SEQUENCE IF EXISTS tbl_roles_id_seq;
CREATE SEQUENCE tbl_roles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."tbl_roles" (
    "id" integer DEFAULT nextval('tbl_roles_id_seq') NOT NULL,
    "cid" integer NOT NULL,
    "url" character varying(32) NOT NULL,
    "action" character varying(32) NOT NULL,
    "sysadmin" smallint NOT NULL,
    "sysadmin_role" smallint DEFAULT '0' NOT NULL,
    "manager" smallint NOT NULL,
    "manager_role" smallint DEFAULT '0' NOT NULL,
    "employee" smallint NOT NULL,
    "employee_role" smallint DEFAULT '0' NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."users" (
    "uid" character varying(64),
    "tel" character varying(64),
    "regkey" character varying(32),
    "regdate" timestamp,
    "cid" integer,
    "name" character varying(64),
    "status" integer,
    "dept" character varying(64),
    "companyname" character varying(128),
    "key" character varying(64),
    "secret" character varying(64),
    "position" character varying(64),
    "costacctid" character varying(64),
    "address1" character varying(128),
    "address2" character varying(128),
    "address3" character varying(128),
    "zip" character varying(10),
    "payday" integer,
    "id" integer DEFAULT nextval('users_id_seq') NOT NULL,
    "parent" character varying(128),
    "internaluse" character varying(128),
    "s3bucket" character varying(128),
    "parentacctid" character varying(128),
    "pending" character varying(128),
    "lastresult" character varying(128),
    "updated" character varying(128),
    "sumacctid" character varying(128),
    "discount" numeric DEFAULT '0',
    CONSTRAINT "idx_users1" UNIQUE ("cid"),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

TRUNCATE "users";
INSERT INTO "users" ("uid", "tel", "regkey", "regdate", "cid", "name", "status", "dept", "companyname", "key", "secret", "position", "costacctid", "address1", "address2", "address3", "zip", "payday", "id", "parent", "internaluse", "s3bucket", "parentacctid", "pending", "lastresult", "updated", "sumacctid", "discount") VALUES
('nakano@ids.co.jp',	'03-5484-7811',	'0f38f7c3427e7ff3a73f57ab87e5646d',	'2018-01-23 12:39:58.095743',	3,	'ご担当者(社内利用)',	'0',	NULL,	'株式会社アイディーエス',	'u4afSVeN/zCbinALgqDuOzklTiyhm9LE7bdDw+aOXveu/PCzYvWIG7kd/Jy2J/Ow',	'+dBG48Q45ctETcbc2WXM9PPHqoA0nA+CHVfP8MEG',	NULL,	'651737375868',	NULL,	NULL,	NULL,	NULL,	NULL,	3,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	56),
('rsv_sunny_kobunsha@sunnycloud.jp',	'03-5484-7811',	NULL,	'2018-04-03 15:12:10.666626',	6,	'ご担当者',	'0',	NULL,	'株式会社光文社',	'AKIAJ4UIR7HLGDF7IMXQ',	'0FFGy7uN7JmYGV9EVryMCdwDoXZM4VJ755vNQzhp',	NULL,	'627722188647',	NULL,	NULL,	NULL,	NULL,	NULL,	6,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	12),
('rsv_sunny_kobunsha_007@sunnycloud.jp',	'03-5484-7811',	'2018-01-23 12:39:58.095743	',	'2018-12-13 00:44:20.945349',	9,	'ICU AWS ACCOUNT',	'0',	NULL,	'Ti mi mi',	'AKIAJ4UIR7HLGDF7IMXQ',	'0FFGy7uN7JmYGV9EVryMCdwDoXZM4VJ755vNQzhp',	NULL,	'376365147650',	NULL,	NULL,	NULL,	NULL,	NULL,	10,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	23),
('sunny_guia@ids.co.jp',	'03-5484-7811',	NULL,	'2018-04-03 17:30:49.116397',	8,	'ご担当者',	'0',	NULL,	'株式会社NDT',	'AKIAI57OYNA5FEERDQHQ',	'k9aY7ItMUASyf+paQs9LO9JMF7xMrrHNmZ6VNiST',	NULL,	'298325701191',	NULL,	NULL,	NULL,	NULL,	NULL,	8,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0),
('rsv_sunny_ajis-group@sunnycloud.jp',	'03-5484-7811',	NULL,	'2018-04-03 17:29:55.216905',	7,	'ご担当者',	'0',	NULL,	'株式会社エイジス',	'AKIAI6Q6KO34EGIZ5YNQ',	'XNDUnIaKmbqg8/tbfruFJBnwBS5+/9T159QfbU3F',	NULL,	'260249930508',	NULL,	NULL,	NULL,	NULL,	NULL,	7,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	0);

DROP TABLE IF EXISTS "volumes";
CREATE TABLE "public"."volumes" (
    "cid" integer,
    "device" character varying(128),
    "instid" character varying(128),
    "state" character varying(32),
    "volid" character varying(128),
    "delonterm" integer,
    "azone" character varying(128),
    "attachtime" timestamp,
    "createtime" timestamp,
    "encrypt" integer,
    "size" numeric,
    "snapshotid" character varying(128),
    "state2" character varying(32),
    "iop" numeric,
    "tag_name" character varying(128),
    "voltype" character varying(32),
    "progress" character varying(12),
    "description" text,
    "stragetype" character varying(3)
) WITH (oids = false);

CREATE INDEX "idx_volumes01" ON "public"."volumes" USING btree ("cid");

CREATE INDEX "idx_volumes02" ON "public"."volumes" USING btree ("stragetype");

CREATE INDEX "idx_volumes03" ON "public"."volumes" USING btree ("volid");


-- 2018-12-13 11:15:46.467811+00
