-----------------------------------------------
-- *1 Only for own dept.
-- *2 Depends on the System Setting.
-- *3 Only for own dept. But only for Manager.
-- *4 Only for own account.
-- *5 Only for the assigned projects.
-----------------------------------------------
TRUNCATE tbl_roles;
INSERT INTO tbl_roles(cid,url,action,sysadmin,sysadmin_role,manager,manager_role,employee,employee_role) VALUES
(0, 'mst/dept.php', 'create', 1,0,0,0,0,0),
(0, 'mst/dept.php', 'read', 1,0,0,0,0,0),
(0, 'mst/dept.php', 'update', 1,0,0,0,0,0),
(0, 'mst/dept.php', 'delete', 1,0,0,0,0,0),

(0, 'mst/user.php', 'read', 1,0,1,1,0,0),
(0, 'mst/user.php', 'delete', 1,0,1,1,0,0),

(0, 'mst/userpj.php', 'read', 1,0,1,1,0,0),
(0, 'mst/userpj_change.php', 'read', 1,0,1,1,0,0),
(0, 'mst/userpj_change.php', 'update', 1,0,1,1,0,0),

(0, 'mst/user2.php', 'read', 1,0,1,1,0,0),
(0, 'mst/user2.php', 'create', 1,0,1,1,0,0),
(0, 'mst/user2.php', 'update', 1,0,1,1,0,0),
(0, 'mst/user2.php', 'change_permission', 1,0,1,3,0,0),

(0, 'mst/proj.php', 'read', 1,0,1,1,0,0),
(0, 'mst/proj.php', 'delete', 1,0,1,1,0,0),

(0, 'mst/proj2.php', 'read', 1,0,1,1,0,0),
(0, 'mst/proj2.php', 'create', 1,0,1,1,0,0),
(0, 'mst/proj2.php', 'update', 1,0,1,1,0,0),

(0, 'admin/obj.php', 'read', 1,0,1,1,0,0),
(0, 'admin/obj.php', 'delete', 1,0,1,1,0,0),

(0, 'admin/objproc.php', 'read', 1,0,1,1,0,0),
(0, 'admin/objproc.php', 'create', 1,0,1,1,0,0),
(0, 'admin/objproc.php', 'update', 1,0,1,1,0,0),

(0, 'mst/instowner.php', 'read', 1,0,1,1,0,0),
(0, 'mst/instowner.php', 'update', 1,0,1,1,0,0),

(0, 'admin/key.php', 'read', 1,0,0,0,0,0),
(0, 'admin/key.php', 'update', 1,0,0,0,0,0),

(0, 'admin/ami.php', 'read', 1,0,0,0,0,0),
(0, 'admin/ami.php', 'update', 1,0,0,0,0,0),

(0, 'admin/az.php', 'read', 1,0,0,0,0,0),
(0, 'admin/az.php', 'update', 1,0,0,0,0,0),

(0, 'admin/insttype.php', 'read', 1,0,0,0,0,0),
(0, 'admin/insttype.php', 'update', 1,0,0,0,0,0),

(0, 'admin/sg.php', 'read', 1,0,0,0,0,0),

(0, 'admin/cw_rules.php', 'read', 1,0,1,1,0,0),
(0, 'admin/cw_rules.php', 'update', 1,0,1,1,0,0),
(0, 'admin/cw_rules.php', 'delete', 1,0,1,1,0,0),

(0, 'admin/cw_rules_proc.php', 'create', 1,0,1,1,0,0),
(0, 'admin/cw_rules_proc.php', 'read', 1,0,1,1,0,0),
(0, 'admin/cw_rules_proc.php', 'update', 1,0,1,1,0,0),

(0, 'cons/profile.php', 'read', 1,0,0,0,0,0),
(0, 'cons/profile.php', 'update', 1,0,0,0,0,0),

(0, 'mst/changepw.php', 'read', 1,4,1,4,1,4),
(0, 'mst/changepw.php', 'update', 1,4,1,4,1,4),

(0, 'cons/info.php', 'read', 1,0,1,0,1,0),
(0, 'cons/info.php', 'update', 1,0,1,0,1,0),

(0, 'cons/request.php', 'read', 1,0,1,1,1,5),
(0, 'cons/request.php', 'remove', 1,0,1,1,1,5),
(0, 'cons/request.php', 'approve', 1,0,1,1,0,0),
(0, 'cons/request.php', 'reject', 1,0,1,1,0,0),
(0, 'cons/request.php', 'request_delete', 0,0,0,0,1,5),
(0, 'cons/request.php', 'approve_delete', 1,0,1,1,0,0),

(0, 'cons/inst.php', 'read', 1,0,1,1,1,5),
(0, 'cons/inst.php', 'start_stop', 1,0,1,1,1,5),

(0, 'cons/download.php', 'read', 1,0,1,0,1,0),

(0, 'cons/req_inst.php', 'read', 1,0,1,1,1,5),
(0, 'cons/req_inst2.php', 'read', 1,0,1,1,1,5),
(0, 'cons/req_inst3.php', 'read', 1,0,1,1,1,5),
(0, 'cons/security_group.php', 'read', 1,0,1,1,1,5),

(0, 'admin/log.php', 'read', 1,0,1,1,1,5),

(0, 'db/configuration.php', 'read', 1,0,0,0,0,0),
(0, 'db/configuration.php', 'update', 1,0,0,0,0,0),

(0, 'db/db.php', 'read', 1,0,1,1,1,5),

(0, 'admin/backup.php', 'read', 1,0,1,1,1,5),
(0, 'admin/backup.php', 'update', 1,0,1,1,1,5),

(0, 'admin/ebs.php', 'read', 1,0,1,1,1,5),
(0, 'admin/ebs.php', 'update', 1,0,1,1,1,5),

(0, 'mst/userpj.php', 'read', 1,0,1,1,0,0),

(0, 'mst/userpj_change.php', 'read', 1,0,1,1,0,0),
(0, 'mst/userpj_change.php', 'update', 1,0,1,1,0,0),

(0, 'cons/user_inst.php', 'read', 1,0,1,1,1,5),
(0, 'cons/user_inst.php', 'update', 1,0,1,1,1,5);
