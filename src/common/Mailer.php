<?php
namespace Common;

class Mailer
{
    private $transport;
    private $mailer;
    private $message;
    public function __construct()
    {
        $this->transport = (new \Swift_SmtpTransport(SMTP_HOST, SMTP_PORT));
        $this->mailer = new \Swift_Mailer($this->transport);
        $this->message = new \Swift_Message();
        $this->message->setFrom([
            SYSTEM_EMAIL => SYSTEM_EMAIL_NAME
        ]);
    }

    public function set($key, $value)
    {
        $key = ucfirst($key);
        $func = "set{$key}";
        $callable = array($this->message, $func);
        if (is_callable($callable)) {
            $this->message->$func($value);
            return $this;
        }
        throw new \Exception("Invalid setting key", 1);
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setBcc($conn, $mailTo)
    {
        $userInfo = \Akatsuki\Models\Emp::select('cid', 'dept')
            ->where('email', $mailTo)
            ->where('status', 0)
            ->first()->toArray();

        $sql = sprintf("
            SELECT DISTINCT 
                e.email,
                e.sysadmin,
                e.admin
            FROM
                emp e
            WHERE
                e.cid = $1
                AND e.status = 0
                AND e.email != $2
                AND (e.sysadmin = 1
                    OR (e.admin = 1
                        AND e.dept = $3
                    )
                )
        ");
        $r = pg_query_params($conn, $sql, [
            $userInfo['cid'],
            $mailTo,
            $userInfo['dept']
        ]);
        $userBccList = pg_fetch_all($r);

        $bccList = [];

        if ($userBccList) {
            $configs = \Akatsuki\Models\Configs::select('check_sys_admin', 'check_manager')
                ->where('cid', $userInfo['cid'])->first();
            foreach ($userBccList as $userBcc) {
                if ((int)$configs->check_sys_admin && (int)$userBcc['sysadmin']) {
                    array_push($bccList, $userBcc['email']);
                } elseif ((int)$configs->check_manager && (int)$userBcc['admin']) {
                    array_push($bccList, $userBcc['email']);
                }
            }

            if (!empty($bccList)) {
                $this->set('bcc', $bccList);
            }
        }
    }

    public function send()
    {
        try {
            return $this->mailer->send($this->message);
        } catch (\Exception $e) {
            return false;
        }
    }
}
