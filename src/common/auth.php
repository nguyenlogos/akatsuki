<?php

function setAuthInfo($conn)
{
    $loginInfo = "";

    // ログイン状態の取得
    if (req('sid')) {
        if (!exist_sid()) {
            $_SESSION = array();
        }
    }

    // その他（通常のアクセス）の場合
    if (isset($_SESSION["uid"]) && isset($_SESSION["cid"])) {
        // ログイン中の場合
        $loginInfo = 'ok';
    }

    return $loginInfo;
}

function req($key)
{
    return (isset($_REQUEST[$key]) ? $_REQUEST[$key] : '');
}

function exist_sid()
{
    $sid = session_id();

    return (!empty($sid) && file_exists(session_save_path() . DIRECTORY_SEPARATOR . 'sess_' . $sid)
        ? true : false);
}

function LoGinAuth($conn)
{
    if (!empty($_POST['email'] && !empty($_POST['password']))) {
        $auEmail = pg_escape_string($_POST['email']);
        $auPass = pg_escape_string($_POST['password']);
        $sql = sprintf('
                SELECT
                    e.email,
                    e.pass,
                    u.cid,
                    u.key,
                    u.secret,
                    e.sysadmin,
                    e.admin,
                    e.idsadmin,
                    u.costacctid,
                    e.infocount,
                    e.wfcount,
                    e.empid
                FROM
                    emp e
                INNER JOIN users u
                    ON u.cid = e.cid
                    AND u.status = 0
                WHERE
                    e.email = $1
                    AND e.pass = $2
                    AND e.status = 0
                    AND e.idsadmin = 1
                LIMIT 1
            ');
        $logs[] = $sql;
        $r = pg_query_params($conn, $sql, [
            $auEmail,
            md5($auPass)
        ]);
        $login = pg_fetch_assoc($r);

        if ($login['email'] != $auEmail && $login['pass'] != $auPass) {
            $error = 'パスワードまたはメールアドレスが間違っています。';
        } else {
            $login_font = [
                'uid_auth'      => $login['email'],
                'cid_auth'      => $login['cid'],
                'empid_auth'    => $login['empid'],
                'idsadmin_auth' => (int)$login['idsadmin'] === 1 ? 1 : 0
            ];
            $_SESSION['login_font'] = $login_font;
        }
    }
    raise_sql($logs, 'auth');

    return $error;
}

function login($conn)
{
    global $smarty;
    // ログイン状態の取得
    if (req('sid')) {
        if (!exist_sid()) {
            $_SESSION = array();
        }
    }

    if (!empty($_POST["Email"]) && !empty($_POST["pass"])) {
        // ユーザIDとパスワードが指定されている場合
        $usrEmail = pg_escape_string($_POST["Email"]);
        $usrPass = pg_escape_string($_POST["pass"]);
        $sql = sprintf('
                SELECT
                    e.email,
                    u.cid,
                    u.key,
                    u.secret,
                    e.sysadmin,
                    e.admin,
                    e.idsadmin,
                    u.costacctid,
                    e.infocount,
                    e.wfcount,
                    e.empid,
                    e.dept,
                    e.login_initial,
                    e.number_lock,
                    e.email_lock,
                    e.regdate,
                    u2.cid as is_cadmin,
                    e.email_confirmed
                FROM
                    emp e
                INNER JOIN users u
                    ON u.cid = e.cid
                    AND u.status = 0
                LEFT JOIN users u2
                    ON u2.cid = e.cid
                    AND u2.uid = e.email
                    AND u2.status = 0
                WHERE
                    e.email = $1
                    AND e.pass = $2
                    AND e.status = 0
                LIMIT 1
            ');
        $logs[] = $sql;
        $r = pg_query_params($conn, $sql, [
            $usrEmail,
            md5($usrPass)
        ]);
        if (!$r) {
            // パスワード間違い
            $loginInfo = 'ng';
        } else {
            $usrInfo = pg_fetch_assoc($r);
            if ($usrInfo['email_lock'] == 1) {
                $loginInfo = 'lock';
                return $loginInfo;
            }

            if ($usrInfo['email'] != $usrEmail && $usrInfo['pass'] != $usrPass) {
                $sql = 'SELECT cid,empid,number_lock,email_lock FROM emp WHERE email = $1';
                $data_email = pg_query_params($conn, $sql, [
                    $usrEmail
                ]);
                $row_email = pg_fetch_array($data_email, null, PGSQL_ASSOC);

                $sql = 'SELECT lock_failed FROM configs WHERE cid = ' . $row_email['cid'];
                $data = pg_query($conn, $sql);
                $row_lock_failed = pg_fetch_array($data, null, PGSQL_ASSOC);

                $asc_lock = $row_email['number_lock'] + 1;
                $sql = 'UPDATE emp SET number_lock = ' . $asc_lock . ' WHERE email = $1';
                pg_query_params($conn, $sql, [
                    $usrEmail
                ]);

                if ($row_lock_failed['lock_failed'] > 0
                    && ($row_lock_failed['lock_failed'] <= $row_email['number_lock'])) {
                    $sql = 'UPDATE emp SET email_lock = 1 WHERE email = $1';
                    pg_query_params($conn, $sql, [
                        $usrEmail
                    ]);
                    $loginInfo = 'lock';
                    return $loginInfo;
                }

                if ($row_email['email_lock'] == 1) {
                    $loginInfo = 'lock';
                } else {
                    $loginInfo = 'ng';
                }



                $logContents = [
                    'log_email' => [
                        'email'      => $usrEmail,
                        'number_log' => $asc_lock,
                    ]
                ];

                \Common\Logger::infoLoGin(
                    (int)$row_email['cid'],
                    $row_email['empid'],
                    json_encode($logContents),
                    LOG_TYPES['DB'],
                    LOG_ACTIONS['LOGIN_FAILED']
                );

                return $loginInfo;
            }

            //check pass expired

            $sql = 'SELECT pass_expired FROM configs WHERE cid = $1';
            $data = pg_query_params($conn, $sql, [
                $usrInfo['cid']
            ]);
            $row = pg_fetch_array($data, null, PGSQL_ASSOC);

            if ($row['pass_expired'] > 0) {
                $expire_date = strtotime($usrInfo['regdate'] . ' + '.$row['pass_expired'].' month');
                if (time() > $expire_date) {
                    $_SESSION["uid"] = $usrInfo['email'];
                    rd("reg/change_pass.php");
                }
            }

            $isCAdmin = !empty($usrInfo['is_cadmin']);
            if ($isCAdmin) {
                if (!(int)$usrInfo['email_confirmed']) {
                    return 'active';
                }
            } else {
                if ($usrInfo['login_initial'] == 0) {
                    $_SESSION["uid"] = $usrInfo['email'];
                    rd("reg/change_pass.php");
                }
            }
            {
                $_SESSION["uid"] = $usrInfo['email'];
                $_SESSION["cid"] = $usrInfo['cid'];
                $_SESSION["key"] = decryptIt($usrInfo['key']);
                $_SESSION["secret"] = $usrInfo['secret'];
                $_SESSION["region"] = "ap-northeast-1";
                $_SESSION["sysadmin"] = (int)$usrInfo['sysadmin'] === 1 ? 1 : 0;
                $_SESSION["admin"] = (int)$usrInfo['admin'] === 1 ? 1 : 0;
                $_SESSION["idsadmin"] = (int)$usrInfo['idsadmin'] === 1 ? 1 : 0;
                $_SESSION["accountid"] = $usrInfo['costacctid'];
                $_SESSION["empid"] = (int)$usrInfo['empid'];
                $_SESSION["dept"] = (int)$usrInfo['dept'];
                $_SESSION['ERR_AWS_API'] = 0;
                $_SESSION['roles'] = [];
                updateInfoCount($conn);
                updateCount($smarty);

            if (pg_fetch_result($r, 0, 8) == "") {
                $_SESSION["wfcount"] = 0;
            } else {
                $_SESSION["wfcount"] = pg_fetch_result($r, 0, 8);
            }
                // ログイン成功

                $sql = 'UPDATE emp SET lastlogin = now(),number_lock = 0 WHERE cid = $1 AND empid = $2';
                $logs[] = $sql;
                $r = pg_query_params($conn, $sql, [
                    $usrInfo['cid'],
                    $usrInfo['empid']
                ]);

                // 認証情報が設定されているかの確認
            if ($_SESSION["key"] == "" || $_SESSION["secret"] == "") {
                $_SESSION['ERR_AWS_KEY'] = 1;
                $loginInfo = 'ok yet';
            } else {
                $apiTestResult = \AwsServices\Aws::validateApi(
                    $_SESSION["key"],
                    $_SESSION["secret"],
                    $_SESSION["region"]
                );
                if ($apiTestResult) {
                    $loginInfo = 'ok';
                } else {
                    $_SESSION['ERR_AWS_KEY'] = 1;
                    $loginInfo = 'ok yet';
                }
            }
            }
        }
        raise_sql($logs, 'auth');
    } else {
        // ユーザIDまたはパスワードが指定されていない場合
        $loginInfo = '';
    }

    return $loginInfo;
}

function logout()
{
    // ログアウトリクエストの場合
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time() - 42000, '/');
    }
    if (session_status() !== PHP_SESSION_NONE) {
        session_destroy();
    }
}

function LogoutAuth()
{
    // ログアウトリクエストの場合
    unset($_SESSION['login_font']);
}

// infoテーブルのカウントからempテーブルの更新、SESSIONへの格納まで
// 負荷が高いので、ログイン時に1回のみ。基本は不要なため。
function updateInfoCount($conn)
{
    $sql = sprintf("SELECT count(a.mid)
                        FROM info a
                        LEFT JOIN tbl_notif t
                            ON t.id = a.id_notif
                        LEFT OUTER JOIN info_ctl b ON (a.mid = b.mid AND b.cid=" . $_SESSION["cid"] . "
                                AND b.uid='" . $_SESSION["uid"] . "')
                        WHERE b.readdate is null
                            AND (a.id_notif IS NULL
                                OR (a.id_notif IS NOT NULL
                                    AND (t.notif_time <= NOW() AND t.publish_date >= NOW())
                                )
                            )
                            AND (a.toall is not null OR (a.toall is null AND b.cid=" . $_SESSION["cid"] . "
                            AND b.uid='" . $_SESSION["uid"] . "'))");
    $logs[] = $sql;
    $r = pg_query($conn, $sql);
    $row = pg_fetch_row($r, 0);

    $sql = "UPDATE emp SET infocount = $1
                WHERE cid = $2 AND email = $3 AND status = $4";
    $logs[] = $sql;
    $r = pg_query_params($conn, $sql, [
        $row[0],
        $_SESSION["cid"],
        $_SESSION["uid"],
        0
    ]);

    $_SESSION["infocount"] = $row[0];
    raise_sql($logs, 'auth');
}

// infoテーブルなどから集計せずに、empテーブルから最新の件数を取得
// 毎アクセス実行されるため、軽い処理で実装。
function updateCount(&$smarty)
{
    updateWfTotalCount($smarty);
    $sql = sprintf("SELECT infocount, wfcount
                    FROM emp
                    WHERE cid = $1
                        AND email = $2
                        AND status = 0 ");
    $logs[] = $sql;
    $r = pg_query_params($smarty->_db, $sql, [
        $_SESSION["cid"],
        $_SESSION["uid"]
    ]);

    if (pg_num_rows($r) != 1) {
        $msg = "システムエラーが発生しました。(NOEMPREC)";
        $smarty->assign('msg', $msg);
        $smarty->assign('viewTemplate', 'error.tpl');
        $smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
        exit;
    }

    $row = pg_fetch_row($r, 0);

    $_SESSION["infocount"] = $row[0];
    $_SESSION["wfcount"] = $row[1];
    raise_sql($logs, 'auth');
}

function updateWfTotalCount($smarty = null)
{
    if (!$smarty) {
        global $smarty;
    }

    $conditions = [
        "i.approved_status != " . INST_REQ_DELETED,
        "i.cid = {$_SESSION["cid"]}"
    ];

    if ($_SESSION['sysadmin'] == '1' || $_SESSION['admin'] == '1') {
        $conditions[] = "i.approved_status != " . INST_REQ_REJECTED;
    }

    if ($_SESSION['sysadmin'] != '1') {
        $assignedProjects = \Akatsuki\Models\EmpProj::getListAssigned();
        $assignedProjects = array_map(function ($pcode) {
            return "'{$pcode}'";
        }, array_column($assignedProjects, 'pcode'));
        $assignedProjects = implode(',', $assignedProjects);
        !$assignedProjects  && $assignedProjects = "''";

        if ($_SESSION['admin']) {
            $condition = "(d.dept = {$_SESSION['dept']} OR i.pcode IN ($assignedProjects))";
        } else {
            $condition = "(i.pcode IN ($assignedProjects))";
            $conditions[] = "i.empid = " . $_SESSION['empid'];
            $conditions[] = "i.approved_status != " . INST_REQ_DELETE_PENDING;
        }
        $conditions[] = $condition;
    }
    $conditions[] = "(irc.id IS NOT NULL OR i.approved_status != " . INST_REQ_APPROVED . ")";
    $conditions = "WHERE " . implode(' AND ', $conditions);

    $sql = sprintf(
        "
        SELECT count(i.id)
        FROM
            inst_req i
        LEFT JOIN
            inst_req_change irc
            ON irc.cid = i.cid
            AND irc.inst_req_id = i.id
        LEFT JOIN
            emp e
            ON e.cid = i.cid
                AND e.empid = i.empid
                AND e.status = 0
        LEFT JOIN
            dept d
            ON d.cid = i.cid
                AND d.dept = i.dept
                AND d.status = 0
        $conditions
    "
    );
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $wfcount = (int)pg_fetch_result($r, 0, 0);

    // count ebs request
    $ebsReqCount = 0;
    $conditions = [
        "er.cid = {$_SESSION["cid"]}",
        "er.status = 0"
    ];

    if ($_SESSION['sysadmin'] != '1') {
        $conditions[] = "d.dept = {$_SESSION['dept']}";
        if ($_SESSION['admin'] != '1') {
            $conditions[] = "er.empid = {$_SESSION['empid']}";
        }
    }

    $conditions = "WHERE " . implode(" AND ", $conditions);

    $sql = "
        SELECT count(er.id)
        FROM
            ebs_req er
        INNER JOIN instance i
            ON i.cid = er.cid
            AND i.id = er.instance_id
        LEFT JOIN
            emp e
            ON e.cid = er.cid
                AND e.empid = er.empid
                AND e.status = 0
        INNER JOIN (
            SELECT erd.ebs_req_id
            FROM ebs_req_detail erd
            GROUP BY erd.ebs_req_id
        ) erd
            ON erd.ebs_req_id = er.id
        LEFT JOIN
            dept d
            ON d.cid = e.cid
                AND d.dept = e.dept
                AND d.status = 0
        {$conditions}
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $ebsReqCount = (int)pg_fetch_result($r, 0, 0);

    $wfcount += $ebsReqCount;

    $sql = '
            UPDATE emp
            SET wfcount = $1
            WHERE
                cid = $2
            AND
                empid = $3
        ';
    $logs[] = $sql;
    $result = pg_query_params($smarty->_db, $sql, [
        $wfcount,
        $_SESSION['cid'],
        $_SESSION['empid']
    ]);
    $_SESSION["wfcount"] = $wfcount;
    raise_sql($logs, 'auth');
}

function encryptIt($encode)
{
    // Set a random salt
    $salt = openssl_random_pseudo_bytes(16);
    $cryptKey = md5(CRYPT_KEY);
    $salted = '';
    $dx = '';
    // Salt the key(32) and iv(16) = 48
    while (strlen($salted) < 48) {
        $dx = hash('sha256', $dx.$cryptKey.$salt, true);
        $salted .= $dx;
    }

    $key = substr($salted, 0, 32);
    $iv  = substr($salted, 32, 16);

    $encrypted_data = openssl_encrypt($encode, 'AES-256-CBC', $key, true, $iv);
    return base64_encode($salt . $encrypted_data);
}

function decryptIt($decode)
{
    $data = base64_decode($decode);
    $salt = substr($data, 0, 16);
    $ct = substr($data, 16);
    $cryptKey = md5(CRYPT_KEY);
    $rounds = 3; // depends on key length
    $data00 = $cryptKey.$salt;
    $hash = array();
    $hash[0] = hash('sha256', $data00, true);
    $result = $hash[0];
    for ($i = 1; $i < $rounds; $i++) {
        $hash[$i] = hash('sha256', $hash[$i - 1].$data00, true);
        $result .= $hash[$i];
    }
    $key = substr($result, 0, 32);
    $iv  = substr($result, 32, 16);

    return openssl_decrypt($ct, 'AES-256-CBC', $key, true, $iv);
}

function permission_check($url = null, $cached = true)
{
    if ($url && $cached && !empty($_SESSION['roles'][$url])) {
        return $_SESSION['roles'][$url];
    }
    if ($url) {
        $url = trim($url, " /");
        $permissions = \Akatsuki\Models\TblRoles::where('url', $url)->get()->toArray();
    } else {
        $permissions = \Akatsuki\Models\TblRoles::orderBy('url')->get()->toArray();
    }

    $roles = [];

    $isAdmin = (int)$_SESSION["sysadmin"];
    $isManager = (int)$_SESSION["admin"];

    foreach ($permissions as $permission) {
        $url = $permission['url'];
        if (!array_key_exists($url, $roles)) {
            $roles[$url] = [];
        }

        $action = $permission["action"];
        $allowed = false;
        $condition = 0;
        if ($isAdmin) {
            $allowed = (int)$permission['sysadmin'];
        } else {
            if ($isManager) {
                $allowed = (int)$permission['manager'];
                $condition = $permission['manager_role'];
            } else {
                $allowed = (int)$permission['employee'];
                $condition = $permission['employee_role'];
            }
        }
        $roles[$url][$action] = [
            'allowed'   => $allowed,
            'condition' => $condition,
        ];
    }

    foreach ($roles as $url => $role) {
        $_SESSION['roles'][$url] = $role;
    }

    return $roles;
}
