<?php

namespace Common;

use Common\Logger;
use Illuminate\Database\Capsule\Manager as DB;

class BaseModelObserver
{
    public function updating($model)
    {
        DB::enableQueryLog();
    }

    public function creating($model)
    {
        DB::enableQueryLog();
    }

    public function deleting($model)
    {
        DB::enableQueryLog();
    }

    public function created($model)
    {
        $contents = $this->generateChanges($model, LOG_ACTIONS['INSERT']);
        BaseModel_sql(DB::getQueryLog(), 'insert_'.$model->getTable());
        Logger::info($contents, LOG_TYPES['DB'], LOG_ACTIONS['INSERT']);
    }

    public function updated($model)
    {
        $action = '';
        if (!empty($model->status) && $model->status == -1) {
            $action = LOG_ACTIONS['DELETE'];
        } else {
            $action = LOG_ACTIONS['UPDATE'];
        }

        $contents = $this->generateChanges($model, $action);
        BaseModel_sql(DB::getQueryLog(), 'update_'.$model->getTable());
        Logger::info($contents, LOG_TYPES['DB'], $action);
    }

    public function deleted($model)
    {
        $contents = $this->generateChanges($model, LOG_ACTIONS['DELETE']);
        BaseModel_sql(DB::getQueryLog(), 'deleted_'.$model->getTable());
        Logger::info($contents, LOG_TYPES['DB'], LOG_ACTIONS['DELETE']);
    }

    private function generateChanges($model, $mode = null)
    {
        $contents = [
            'tbl_id' => !empty($model->id) ? $model->id : null,
            'tbl_name' => $model->getTable(),
        ];

        $changes = $model->isDirty() ? $model->getDirty() : false;

        if ($changes) {
            $changedKeys = [];

            $oldData = $newData = null;
            if ($mode === LOG_ACTIONS['INSERT']) {
                $newData = $changes;
                $changedKeys = array_keys($newData);
            } elseif ($mode === LOG_ACTIONS['DELETE']) {
                $oldData = $model->toArray();
                $changedKeys = array_keys($oldData);
            } else {
                $newData = [];
                $oldData = [];
                $columns = array_merge($model->keys(), array_keys($changes));
                $changedKeys = $columns;
                foreach ($columns as $column) {
                    $newData[$column] = $model->$column;
                    $oldData[$column] = $model->getOriginal($column);
                }
            }

            $relations = $model->relations();
            $relationKeys = array_keys($relations);
            $relationKeys = array_intersect($relationKeys, $changedKeys);
            if (count($relationKeys)) {
                $classname = get_class($model);
                $oldModel = $classname::find($model->id)->first();
                foreach ($relationKeys as $key) {
                    $fn = "{$key}s";
                    $target = $relations[$key];
                    $mode !== LOG_ACTIONS['INSERT'] && $oldData[$key] = $oldModel->$fn->$target;
                    $mode !== LOG_ACTIONS['DELETE'] && $newData[$key] = $model->$fn->$target;
                }
            }
            $contents['changes'] = [
                'old' => $oldData,
                'new' => $newData
            ];
        }
        return json_encode($contents);
    }
}
