<?php
namespace Common;

class Logger
{
    private static function log($description, $category, $action = null, $error = false)
    {
        if (is_cli_env()) {
            $options = [
                "cid:",
                "empid:",
            ];
            $params = getopt(null, $options);
            $cid = $params['cid'];
            $empid = $params['empid'];
        } else {
            $cid = $_SESSION['cid'];
            $empid = $_SESSION['empid'];
        }
        $error = $error ? 1 : 0;
        $sql = "
            INSERT INTO tbl_logs(
                cid, empid, category, action,
                description, error_flag, date_created
            ) VALUES (
                $1, $2, $3, $4,
                $5, $6, $7
            );
        ";
        $bindings = [
            $cid, $empid, $category, $action,
            $description, $error, "now()"
        ];
        $conn = openConnection();

        return !!pg_query_params($conn, $sql, $bindings);
    }

    private static function logLoGin($cid, $empid, $description, $category, $action = null, $error = false)
    {
        $error = $error ? 1 : 0;
        $sql = "
            INSERT INTO tbl_logs(
                cid, empid, category, action,
                description, error_flag, date_created
            ) VALUES (
                $1, $2, $3, $4,
                $5, $6, $7
            );
        ";
        $bindings = [
            $cid, $empid, $category, $action,
            $description, $error, "now()"
        ];
        $conn = openConnection();
        return !!pg_query_params($conn, $sql, $bindings);
    }

    public static function info($description, $category, $action)
    {
        return self::log($description, $category, $action);
    }

    public static function infoLoGin($cid, $empid, $description, $category, $action)
    {
        return self::logLoGin($cid, $empid, $description, $category, $action);
    }

    public static function error($description, $category = null, $action = null)
    {
        if (empty($category)) {
            $category = LOG_TYPES['EX'];
        }
        return self::log($description, $category, $action, true);
    }
}
