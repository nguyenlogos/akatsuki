<?php
require_once(SMARTY_PLUGINS_DIR . 'shared.escape_special_chars.php');

function smarty_function_pulldown4notifystatus($params, &$smarty) {
	$idstr = isset($params["id"]) ? $params["id"] : "";
	$infoid = isset($params["infoid"]) ? (int)$params["infoid"] : 0;
	if ( $infoid > 0 ) {
		return;
	}
	$selectedStatus = isset($params["selected"]) ? smarty_function_escape_special_chars($params["selected"]) : "-1";
	$options = [
		"-1" => "全て表示",
		"0" => "未読のみ表示",
		"1" => "既読のみ表示"
	];
	$template = "";
	foreach ($options as $value => $displayText) {
		$selected = $selectedStatus == $value ? "selected" : "";
		$template .= "<option value='{$value}' {$selected}>{$displayText}</option>";
	}
	$template = "<select class='form-control' name='status' id='{$idstr}'>{$template}</select>";
	return $template;
}
