<?php

function smarty_function_pulldown4devicename($params, &$smarty)
{
    require_once(SMARTY_PLUGINS_DIR . 'shared.escape_special_chars.php');

    $name = "devname";
    $selected = "";
    $counter = "";
    $stat = "0";
    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'name':
            case 'counter':
            case 'stat':
                $$_key = (string) $_val;
                break;
            case 'selected':
                $$_key = smarty_function_escape_special_chars((string) $_val);
                break;
        }
    }
    $name = $name . $counter;

    $devList = array("sdb", "sdc", "sdd", "sde", "sdf", "sdg", "sdh", "sdi", "sdj", "sdk", "sdl");

    if ($stat == "0") {
        $retStr = "<select class='form-control two_devname' name='" . $name . "'>\n";
        foreach ($devList as $dev) {
            $retStr .= "<option value='/dev/" . $dev . "'";
            if ($selected == "/dev/".$dev) {
                $retStr .= " selected";
            }
            $retStr .= ">/dev/" . $dev . "\n";
        }
    } else {
        $retStr = $selected . "<input type='hidden' name='" . $name . "' value='" . $selected . "' />\n";
    }

    return $retStr;
}
