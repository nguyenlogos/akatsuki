<?php

function smarty_function_pulldownSG($params)
{
    $sg = '';
    $selected = "";
    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'selected':
                $$_key = (string) $_val;
                break;
        }
    }

    $type = [
        ['customtcp', 'Custom TCP Rule', 'TCP', '', 'tcp', '', ''],
        ['customudp', 'Custom UDP Rule', 'UDP', '', 'udp', '', ''],
        //['customicmpv6', 'Custom ICMP Rule - IPv6', 'IPV6 ICMP', 'All', 'icmpv6', '-1', '-1'],
        //['customprotocol', 'Custom Protocol', '', 'all', '', '0', '0'],
        ['alltcp', 'All TCP', 'TCP', '0-65535', 'tcp', '0', '65535'],
        ['alludp', 'All UDP', 'UDP', '0-65535', 'udp', '0', '65535'],
        ['allicmp', 'All ICMP - IPv4', 'ICMP', '0-65535', 'icmp', '-1', '-1'],
        ['allicmpv6', 'All ICMP - IPv6', 'IPV6 ICMP', 'All', 'icmpv6', '-1', '-1'],
        ['-1', 'All traffic', 'All', '0-65535', '-1', '0', '0'],
        ['ssh', 'SSH', 'TCP', '22', 'tcp', '22', '22'],
        ['smtp', 'SMTP', 'TCP', '25', 'tcp', '25', '25'],
        ['dns-udp', 'DNS (UDP)', 'UDP', '53', 'udp', '53', '53'],
        ['dns-tcp', 'DNS (TCP)', 'TCP', '53', 'tcp', '53', '53'],
        ['http', 'HTTP', 'TCP', '80', 'tcp' , '80', '80'],
        ['pop3', 'POP3', 'TCP', '110', 'tcp', '110', '110'],
        ['imap', 'IMAP', 'TCP', '143', 'tcp', '143', '143'],
        ['ldap', 'LDAP', 'TCP', '389', 'tcp', '389', '389'],
        ['https', 'HTTPS', 'TCP', '443', 'tcp', '443', '443'],
        ['smb', 'SMB', 'TCP', '445', 'tcp', '445', '445'],
        ['smtps', 'SMTPS', 'TCP', '465', 'tcp', '465', '465'],
        ['imaps', 'IMAPS', 'TCP', '993', 'tcp', '993', '993'],
        ['pop3s', 'POP3S', 'TCP', '995', 'tcp', '995', '995'],
        ['mssql', 'MS SQL', 'TCP', '1433', 'tcp', '1433', '1433'],
        ['nfs', 'NFS', 'TCP', '2049', 'tcp', '2049', '2049'],
        ['mysql', 'MYSQL/Aurora', 'TCP', '3306', 'tcp', '3306', '3306'],
        ['redshift', 'Redshift', 'TCP', '3389', 'tcp', '3389', '3389'],
        ['rdp', 'RDP', 'TCP', '5439', 'tcp', '5439', '5439'],
        ['postgreSQL', 'PostgreSQL', 'TCP', '5432', 'tcp', '5432', '5432'],
        ['oracle-rds', 'Oracle-RDS', 'TCP', '1521', 'tcp', '1521', '1521'],
        ['winrm-http', 'WinRM-HTTP', 'TCP', '5985', 'tcp', '5985', '5985'],
        ['winrm-https', 'WinRM-HTTPS', 'TCP', '5986', 'tcp', '5986', '5986'],
        ['egpu', 'Elastic GPU', 'TCP', '2007', 'tcp', '2007', '2007']
    ];

    if ($type) {
        $sg .= '<option value = "" > --Select Type--</option >';
        foreach ($type as $key => $value) {
            $sg .= '<option value = "'.$value[0].'"
                  data-col = "'.$value[2].'"
                  data-port = "'.$value[3].'"';
            if ($selected == $value[0]) {
                $sg .= " selected ";
            }
            if ($value[4] != '') {
                $sg .= 'data-protocol = "'.$value[4].'"';
            }
            if ($value[5] != '') {
                $sg .= 'data-fromport = "'.$value[5].'"';
            }
            if ($value[6] != '') {
                $sg .= 'data-toport = "'.$value[6].'"';
            }
            $sg .= '>' . $value[1] . '</option >';
        }
    }
    return $sg;
}
