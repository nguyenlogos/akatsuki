<?php

function smarty_function_pulldown4logtype($params, &$smarty)
{
    $selected = "";
    $name = "";
    foreach ($params as $_key => $_val) {
        $$_key = (string) $_val;
    }
    if (!$name) {
        $name = "logtype";
    }
    $retStr  = "";
    $retStr .= "<select class='form-control' name='{$name}'>\n";
    $retStr .= "<option value=''>-- ログ種別 --\n";

    $logTypes = LOG_TYPES;
    unset($logTypes['EX']);
    foreach ($logTypes as $type => $dispname) {
        $selectedType = $selected === $dispname ? "selected" : "";
        $retStr .= "<option value='{$dispname}' {$selectedType}>{$dispname}</option>\n";
    }
    $retStr .= "</select>\n";

    return $retStr;
}
