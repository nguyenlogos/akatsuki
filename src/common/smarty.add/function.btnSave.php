<?php

function smarty_function_btnSave($params, &$smarty)
{
    $class = "";
    $inline = "";
    foreach ($params as $key => $value) {
        $$key = $value;
    };
    $class || $class = 'btn-save';
    $btn = sprintf('
        <button type="button" name="button" class="btn btn-orange mb-4 %s">
            保存
        </button>
    ', $class);
    $inline = (int)$inline;
    if ($inline) {
        return "<span>$btn</span>";
    }
    return "<div class='btn-area'>$btn</div>";
}
