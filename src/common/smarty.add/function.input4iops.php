<?php

function smarty_function_input4iops($params, &$smarty)
{
    $name = "iops";
    $curval = "";
    $size = 3;
    $counter = "";
    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'name':
            case 'size':
            case 'curval':
            case 'counter':
                $$_key = (string) $_val;
                break;
        }
    }
    $name = $name . $counter;
    $class = 'iops' . $counter;

    $retStr = "<input class='form-control hidden_iops data_check two_iops' data-hidden id='".$class."' 
                type='number' min='1' max='10000000' name='" . $name . "' 
                value='" . $curval . "' size='" . $size . "' >\n";

    return $retStr;
}
