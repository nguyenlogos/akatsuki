<?php
use AwsServices\Ec2;

function smarty_function_pulldown4vpc($params, &$smarty)
{
    $name = $selected = "";
    foreach ($params as $_key => $_val) {
        $$_key = (string) $_val;
    }
    !$name && $name = "vpc";

    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1) {
        $vpcs = [];
    } else {
        $ec2Config = [
            'credentials' => [
                'key' => $_SESSION['key'],
                'secret' => $_SESSION['secret']
            ],
            'region' => 'ap-northeast-1', // Tokyo
            'version' => 'latest',
        ];
        $ec2Client = new Ec2($ec2Config);
        $vpcs = $ec2Client->describeVpcs();
    }

    $options = [
        "<option value=''>--- VPCを選択 ---"
    ];
    foreach ($vpcs as $vpc) {
        $vpcid = $vpc['VpcId'];
        $isSelected = $vpcid === $selected ? "selected" : "";
        $options[] = "
            <option value='{$vpcid}' {$isSelected}>
                {$vpcid}&nbsp;({$vpc['CidrBlock']})
            </option>
        ";
    }
    $options = implode("\n", $options);
    return "
        <select class='form-control' name='{$name}'>
            {$options}
        </select>
    ";
}
