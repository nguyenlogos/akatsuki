<?php

function smarty_function_pulldown4azone($params, &$smarty)
{
    $name = "azone";
    $selected = "";
    foreach ($params as $_key => $_val) {
        $$_key = (string)$_val;
    }

    $sql = "
        SELECT
            a.name,
            a.state,
            a.regionname
        FROM
            azone a
        INNER JOIN azone_enable ae
            ON ae.cid = a.cid
            AND ae.name = a.name
        WHERE
            a.cid = {$_SESSION["cid"]}
        ORDER BY
            a.regionname,
            a.name;
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $retStr = "<SELECT name='" . $name . "' class='form-control'>\n";
    $retStr .= "<OPTION value=''>--- データセンターを選択 ---\n";
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $azname = pg_fetch_result($r, $i, 0);
        $region = local_regionName(pg_fetch_result($r, $i, 2));
        $retStr .= "<OPTION value='" . $azname . "'";
        if ($selected == $azname) {
            $retStr .= " selected";
        }
        $retStr .= ">" . $azname . " (" . $region . ")\n";
    }
    $retStr .= "</SELECT>\n";
    raise_sql($logs, 'func_azone');

    return $retStr;
}
