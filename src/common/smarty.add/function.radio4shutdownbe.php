<?php

function smarty_function_radio4shutdownbe($params, &$smarty)
{
    $name = "shutdownbe";
    $selected = "";
    $behaviorList = array('stop', 'terminate');

    foreach ($params as $_key => $_val) {
        $$_key = (string)$_val;
    }

//  foreach ($params as $_key => $_val) {
//    switch ($_key) {
//      case 'name':
//        $$_key = (string) $_val;
//        break;
//      case 'selected':
//        $$_key = smarty_function_escape_special_chars((string) $_val);
//        break;
//    }
//  }

    $retStr = "";
    $retStr .= "<div class='block-radio'>";
    for ($i = 0; $i < count($behaviorList); $i++) {
        $retStr .= "<div class='form-check d-inline-block mr-5'>\n";
        $val = $behaviorList[$i];
        $check = $val == 'stop' ? 'checked' : '';
        $retStr .= "<input class='form-check-input' type='radio' value='" . $val . "' name='" . $name . "' ".$check." ";
        if ($selected == $val) {
            $retStr .= " checked";
        }
        $retStr .= ">\n";
        $retStr .= "<label class='form-check-label'>" . local_Shutdownbe($val) . "</label>\n";
        $retStr .= "</div>\n";
    }
    $retStr .= "</div>";
    return $retStr;
}
