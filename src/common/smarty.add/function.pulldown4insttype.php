<?php
function smarty_function_pulldown4insttype($params, &$smarty)
{
    $name = "insttype";
    $selected = "";
    $zone = "";
    $list = array();

    foreach ($params as $_key => $_val) {
        $$_key = (string)$_val;
    }

    $isCurGen = (int)\Akatsuki\Models\Configs::getConfig('insttype_curgen');
    if (empty($zone)) {
        $data = [];
    } else {
        $joinCondition = "
            LEFT JOIN
                insttype_pricing p
                ON p.cid = {$_SESSION["cid"]}
                AND p.name = a.name
                AND p.region = '{$zone}'
        ";
        if ($isCurGen === 0) {
            $sql = "
                SELECT
                    a.*, p.price
                FROM
                    insttype a
                INNER JOIN
                    insttype_enable b
                    ON a.name = b.name
                    AND b.cid = {$_SESSION["cid"]}
                $joinCondition
                ORDER BY
                    p.price
            ";
            $logs[] = $sql;
        } else {
            $sql = "
                SELECT
                    a.*, p.price
                FROM
                    insttype a
                $joinCondition
                WHERE
                    a.curgen = 'Yes'
                ORDER BY
                    p.price
            ";
            $logs[] = $sql;
        }
        $r = pg_query($smarty->_db, $sql);
        $data = pg_fetch_all($r);
        foreach ($data as $key => $value) {
            $item = [
                "text" => [
                    "name"   => $value['name'],
                    "cpu"    => "x{$value['cpu']} vCPU",
                    "memory" => "{$value['memory']} GiB",
                    "storage"=> $value['storage'],
                    "price"  => "{$value['price']} (USD/h)"
                ],
                "value" => $value['name'],
            ];
            $list[] = json_encode($item);
        }
    }
    return implode(',', $list);
}
