<?php

function smarty_function_tablelist($params, &$smarty)
{
    $tableClass = "";
    $tbodyClass = "";
    $scrollable = "";
    foreach ($params as $_key => $_val) {
        $$_key = $_val;
    }
    if (!is_array($data)) {
        return "";
    }
    if (count($data) === 0) {
        return "";
    }

    if (empty($headers)) {
        //
    }
    $htmlStr   = "";
    $headerStr = "";
    $bodyStr   = "";
    $actionBtn = "";
    $actionCount = !empty($actions) && is_array($actions) ? count($actions) : 0;
    if ($actionCount > 0) {
        foreach ($actions as $action) {
            $template = !empty($action['template']) ? $action['template'] : '';
            if (!$template) {
                continue;
            }
            $keys = array_keys($action);
            $vars = [];
            foreach ($keys as $key) {
                if (array_key_exists($key, $action)) {
                    $vars[$key] = $action[$key];
                } else {
                    $vars[$key] = "";
                }
            }
            $actionBtn .= strtr($template, $vars);
        }
        $actionBtn = sprintf("<td>%s</td>", $actionBtn);
    }
    foreach ($headers as $column => $header) {
        $class = $header["sortable"] ? "sort-header" : "";
        $width = !empty($header["width"]) ? (int)$header["width"] : 0;
        if (empty($header['disp_name'])) {
            $class .= " d-none";
        }
        $style = "";
        if ($width > 0) {
            $style .= "min-width: {$width}px;";
            $style .= "width: {$width}px;";
        }
        if ($style) {
            $style = "style=\"{$style}\"";
        }
        $disp_name = $header['disp_name'];
        empty($disp_name) && $disp_name = "&nbsp;";
        $headerStr .= "<th scope='col' class='{$class}' {$style}>{$disp_name}</th>";
    }

    $columns = array_keys($headers);
    foreach ($data as $obj) {
        $tds = "";
        foreach ($columns as $column) {
            $rawInput = !empty($headers[$column]['raw_input']) ? (int)$headers[$column]['raw_input'] : false;
            $dataAttr = !empty($headers[$column]['data_attr']) ? (int)$headers[$column]['data_attr'] : false;
            $value = array_key_exists($column, $obj) ? $obj[$column] : "";
            if ($value && !$rawInput) {
                $value = htmlspecialchars($value);
            };
            $className = isset($headers[$column]['class_name']) ? $headers[$column]['class_name'] : '';
            if (empty($headers[$column]['disp_name'])) {
                $className .= " d-none";
            }
            $className && $className = "class='{$className}'";
            $dataAttr = $dataAttr ? "data-item-{$column}='{$value}'" : "";
            $tds .= "<td {$className} {$dataAttr}>{$value}</td>";
        }
        $tds .= $actionBtn;
        $primaryId = !empty($obj['id']) ? $obj['id'] : 0;
        $bodyStr .= "<tr data-item-id='{$primaryId}'>{$tds}</tr>";
    }

    if ($actionBtn) {
        $btnWidth = '100px';
        $headerStr .= "<th style='width: {$btnWidth};min-width: {$btnWidth};'>操作</th>";
    }

    if ((int)$scrollable) {
        $tableClass .= " fixed-header";
        $tbodyClass .= " scrollable";
    }
    $htmlStr = "
        <table class='table table-striped sort-table sortable {$tableClass}'>
            <thead>
                <tr>{$headerStr}</td>
            </thead>
            <tbody class={$tbodyClass}>
                {$bodyStr}
            </tbody>
        </table>
    ";
    return $htmlStr;
}
