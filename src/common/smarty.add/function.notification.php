<?php

function smarty_function_notification($params, &$smarty)
{
    $infoMsgs = [];
    $errorMsgs = [];
    if (!empty($_SESSION['rd_info'])) {
        $rdInfo = $_SESSION['rd_info'];
        if (array_key_exists('err', $rdInfo) && array_key_exists('msg', $rdInfo)) {
            if ($rdInfo['err'] == 1) {
                $errorMsgs[] =  $rdInfo['msg'];
            } else {
                $infoMsgs[] = $rdInfo['msg'];
            }
        }
        $count = $_SESSION['rd_info']['count'];
        $_SESSION['rd_info'] = [
            'count' => $count
        ];
    }

    // if (!empty($_SESSION['ERR_AWS_API']) && $_SESSION['ERR_AWS_API'] == 1) {
    //     $errorMsgs[] = MESSAGES['ERR_AWS_API'];
    //     unset($_SESSION['ERR_AWS_API']);
    // }

    if (!empty($_SESSION['IS_NEW_AWS_KEY_OK'])) {
        unset($_SESSION['IS_NEW_AWS_KEY_OK']);
    } elseif (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1) {
        $errorMsgs[] = MESSAGES['ERR_AWS_KEY'];
    }

    $html = "";
    foreach ($infoMsgs as $msg) {
        $html .= "
            <div class='alert alert-normal'>
                <button type='button' class='close' data-dismiss='alert'>×</button>
                {$msg}
            </div>
        ";
    }

    foreach ($errorMsgs as $msg) {
        $html .= "
            <div class='alert alert-danger'>
                <button type='button' class='close' data-dismiss='alert'>×</button>
                {$msg}
            </div>
        ";
    }
    return $html;
}
