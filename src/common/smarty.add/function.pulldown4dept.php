<?php
function smarty_function_pulldown4dept($params, &$smarty)
{
    $selected = "";
    $cid = "";
    $all = "";
    $first = "";
    $second = "";
    $title = "--- グループを選択 ---";

    foreach ($params as $_key => $_val) {
        $$_key = (string)$_val;
    }
    empty($name) && $name = "deptid";

    $html  = "<select id='group' class='form-control validate_group' name='{$name}'>";
    $html .= "<option value=''>{$title}";
    $html .= "%s";
    $html .= "</select>";
    if (empty($_SESSION['roles']['mst/dept.php'])) {
        $roles = permission_check('mst/dept.php');
    } else {
        $roles = $_SESSION['roles']['mst/dept.php'];
    }

    if (empty($cid)) {
        $cid = $_SESSION["cid"];
    } else {
        $cid = (int)$cid;
    }

    $whereConditions = [
        'status = 0',
        'cid = $1',
    ];
    $bindingParams = [$cid];

    if (empty($roles['read']) || !$roles['read']['allowed']) {
        if ($_SESSION['dept'] == 0) {
            $html = sprintf($html, "");
            return $html;
        }
        $whereConditions[] = "dept = $2";
        $bindingParams[] = $_SESSION['dept'];
    }

    $whereConditions = implode(" AND ", $whereConditions);

    $sql = "select dept,deptname from dept where $whereConditions order by disporder;";
    $logs[] = $sql;
    raise_sql($logs, 'func_dept');
    $r = pg_query_params($smarty->_db, $sql, $bindingParams);

    $retStr  = "";
    if ($first != "") {
        $retStr .= "<OPTION value='-1'";
        if ($selected == -1) {
            $retStr .= " selected";
        }
        $retStr .= ">" . $first . "\n";
    }
    if ($second != "") {
        $retStr .= "<OPTION value='-2'";
        if ($selected == -2) {
            $retStr .= " selected";
        }
        $retStr .= ">" . $second . "\n";
    }

    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $dept = pg_fetch_result($r, $i, 0);
        $deptname = pg_fetch_result($r, $i, 1);
        $retStr .= "<OPTION value='" . $dept . "'";
        if ($selected == $dept) {
            $retStr .= " selected";
        }
        $retStr .= ">" . htmlspecialchars($deptname) . "\n";
    }

    $html = sprintf($html, $retStr);
    return $html;
}
