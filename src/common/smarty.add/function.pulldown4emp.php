<?php

function smarty_function_pulldown4emp($params, &$smarty)
{
    $selected = "";
    $name = "";
    foreach ($params as $_key => $_val) {
        $$_key = (string) $_val;
    }
    if (!$name) {
        $name = "emp";
    }
    $conditions = [
        "e.cid = {$_SESSION['cid']}"
    ];
    $conditions = "WHERE " . implode(" AND ", $conditions);
    $sql = "
        SELECT
            e.empid,
            e.name,
            e.email
        FROM
            emp e
        INNER JOIN
            users u
            ON
                u.cid = e.cid
        $conditions
        ORDER BY
            e.name
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $empList = pg_fetch_all($r);

    $retStr  = "";
    $retStr .= "<select class='form-control' name='{$name}'>\n";
    $retStr .= "<option value=''>--- ユーザーを選択 ---\n";

    foreach ($empList as $emp) {
        $selectedEmp = "";
        if ( $selected == $emp['empid'] ) {
            $selectedEmp = "selected";
        }
        $retStr .= "<option value='{$emp['empid']}' {$selectedEmp}>{$emp['name']}&nbsp;({$emp['email']})</option>\n";
    }
    $retStr .= "</select>\n";
    raise_sql($logs, 'func_emp');

    return $retStr;
}
