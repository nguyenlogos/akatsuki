<?php

function smarty_function_input4instnum($params, &$smarty)
{
  $name = "instnum";
  $curval = "";
  $size = 3;
  foreach ($params as $_key => $_val) {
    switch ($_key) {
      case 'name':
      case 'size':
      case 'curval':
        $$_key = (string) $_val;
        break;
    }
  }

  $retStr = "<input type='number' name='" . $name . "' min='0' max='5' class='form-control' value='" . $curval . "' size='" . $size . "'>\n";
  return $retStr;
}
?>
