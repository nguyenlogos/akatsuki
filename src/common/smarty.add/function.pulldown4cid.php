<?php
function smarty_function_pulldown4cid($params, &$smarty)
{
    $name = "scid";
    $selected = "";
    $onChange = "";
    $all = "";
    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'all':
            case 'name':
            case 'selected':
            case 'onchange':
                $$_key = (string) $_val;
                break;
        }
    }

    $conditions = "";
    if (empty($_SESSION['login_font'])) {
        $conditions = "WHERE cid = {$cid}";
    }

    $sql = "
        SELECT
            cid,
            companyname,
            dept,
            name,
            status
        FROM
            users
        $conditions
        ORDER BY
            companyname
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $retStr = "<SELECT class='form-control' name='" . $name . "'";
    if ($onChange == true) {
        $retStr .= " onChange='submit(this.form)'";
    }
    $retStr .= ">\n";
    if ($all) {
        $retStr .= "<OPTION value=''>すべて\n";
    }
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $cid = pg_fetch_result($r, $i, 0);
        $cname = pg_fetch_result($r, $i, 1);
        $dept = pg_fetch_result($r, $i, 2);
        $name = pg_fetch_result($r, $i, 3);
        $status = pg_fetch_result($r, $i, 4);
        $retStr .= "<OPTION value='" . $cid . "'";
        if ($selected == $cid) {
            $retStr .= " selected";
        }
        if ($status == 0) {
            $status = "利用中";
        } else {
            $status = "削除済";
        }
        $retStr .= ">" . $cname . " " . $dept ." " . $name . " (" . $status . ")\n";
    }
    $retStr .= "</SELECT>\n";
    raise_sql($logs, 'func_cid');

    return $retStr;
}
