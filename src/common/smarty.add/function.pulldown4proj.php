<?php

function smarty_function_pulldown4proj($params, &$smarty)
{
    $name       = "pcode";
    $selected   = "";
    $dept       = "";
    $cid        = "";
    $first      = "";
    $second     = "";
    $all        = "";

    foreach ($params as $_key => $_val) {
        $$_key = (string) $_val;
    }

    $retStr = "<SELECT id='proj-list' class='form-control' name='{$name}'>\n";

    $retStr .= "<OPTION value=''>すべて\n";

    if ( $first != "" ) {
        $retStr .= "<OPTION value='-1'>" . $first . "\n";
    }
    if ( $second != "" ) {
        $retStr .= "<OPTION value='-2'>" . $second . "\n";
    }

    if ( $dept ) {
        $whereConditions    = [];
        $bindingParams      = [];
        if ( $cid ) {
            $whereConditions[] = [
                "col" => "cid",
                "val" => $cid
            ];
        }
        if ( $dept ) {
            $whereConditions[] = [
                "col" => "dept",
                "val" => $dept
            ];
        }

        $whereStr = "status = 0";
        foreach ($whereConditions as $index => $condition) {
            $placeholder = $index + 1;
            $whereStr .= " AND {$condition['col']} = $$placeholder";
            $bindingParams[] = $condition['val'];
        }
        $sql    = "select pcode,pname from proj where $whereStr order by disporder;";
        $logs[] = $sql;
        $r      = pg_query_params($smarty->_db, $sql, $bindingParams);
        for ($i = 0; $i < pg_num_rows($r); $i++) {
            $pcode = pg_fetch_result($r, $i, 0);
            $pname = pg_fetch_result($r, $i, 1);
            $retStr .= "<OPTION value='" . htmlspecialchars($pcode) . "'";
            if($selected == $pcode) {
                $retStr .= " selected";
            }
            $retStr .= ">" . htmlspecialchars($pname) . "\n";
        }
    }

    $retStr .= "</SELECT>\n";
    raise_sql($logs, 'func_proj');

    return $retStr;
}