<?php
function smarty_function_pulldown4parent($params, &$smarty)
{
    $name = "pid";
    $selected = "";
    $onChange = "";
    $all = "";
    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'all':
            case 'name':
            case 'selected':
            case 'onchange':
                $$_key = (string) $_val;
                break;
        }
    }

    $sql = "
        SELECT
            b.costacctid,
            b.companyname,
            b.name
        FROM
            parent a,
            users b
        WHERE
            a.cid=b.cid
        ORDER BY
            b.companyname
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $retStr = "<SELECT class='form-control' name='" . $name . "'";
    if ($onChange == true) {
        $retStr .= " onChange='submit(this.form)'";
    }
    $retStr .= ">\n";
    if ($all) {
        $retStr .= "<OPTION value=''>すべて\n";
    }
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $acid = pg_fetch_result($r, $i, 0);
        $cname = pg_fetch_result($r, $i, 1);
        $name = pg_fetch_result($r, $i, 2);

        $retStr .= "<OPTION value='" . $acid . "'";
        if ($selected == $acid) {
            $retStr .= " selected";
        }
        $retStr .= ">" . $cname . " " . $acid  . "\n";
    }
    $retStr .= "</SELECT>\n";
    raise_sql($logs, 'func_cid');

    return $retStr;
}
