<?php

function smarty_function_pulldownListMyIp($params)
{
    $Ip = "";
    $selected = "";
    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'selected':
                $$_key = (string) $_val;
                break;
        }
    }

    $listIp = [
        ["my_ip", "My IP"],
        ["custom", "Custom"],
        ["anywhere", "Anywhere"]
    ];

    if ($listIp) {
        $Ip .= '<option value = "" > --Select Type--</option >';
        foreach ($listIp as $key => $value) {
            $Ip .= '<option value = "'.$value[0].'"';
            if ($selected == $value[0]) {
                $Ip .= " selected ";
            }
            $Ip .= '>' . $value[1] . '</option >';
        }
    }
    return $Ip;
}
