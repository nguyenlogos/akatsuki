<?php

function smarty_function_instDB($params, &$smarty)
{
    $reqRunningCount = $reqWarningCount = $reqOutdatedCount = $activeCount = $inactiveCount = 0;

    $info = !empty($params['info']) && is_array($params['info']) ? $params['info'] : [];
    $hideA = !empty($params['hideA']) ? (int)$params['hideA'] : 0;

    foreach ($info as $key => $value) {
        $$key = (int)$value;
    }

    $panelSettings_A = [
        [
            'panel'  => 'bg-info',
            'title'  => '申請(承認待ち)',
            'title2' => '件',
            'count'  => $reqRunningCount,
            'filter' => '',
            'footer' => 'フィルタに設定',
        ],
        [
            'panel'  => 'bg-danger',
            'title'  => '期限切れ(2週間以内)',
            'title2' => 'インスタンス数',
            'count'  => $reqWarningCount,
            'filter' => '',
            'footer' => 'フィルタに設定',
        ],
        [
            'panel'  => 'color-sunnyBlue',
            'title'  => '期限切れ',
            'title2' => 'インスタンス数',
            'count'  => $reqOutdatedCount,
            'filter' => '',
            'footer' => 'フィルタに設定',
        ],
    ];

    $panelSettings_B = [
        [
            'panel'  => 'bg-info',
            'title'  => '稼働中',
            'icon'   => 'fa-play',
            'title2' => 'インスタンス数',
            'h2class'=> 'active-count',
            'count'  => $activeCount,
            'filter' => 'running',
            'footer' => 'フィルタに設定',
        ],
        [
            'panel'  => 'bg-danger',
            'title'  => '停止済',
            'icon'   => 'fa-pause',
            'title2' => 'インスタンス数',
            'h2class'=> 'inactive-count',
            'count'  => $inactiveCount,
            'filter' => 'stopped',
            'footer' => 'フィルタに設定',
        ],
        [
            'panel'  => 'color-sunnyBlue',
            'title'  => '合計',
            'title2' => 'インスタンス数',
            'count'  => $activeCount + $inactiveCount,
            'filter' => 'all',
            'footer' => 'すべてを表示',
        ],
    ];

    $html_A = $html_B = [];
    if (!$hideA) {
        foreach ($panelSettings_A as $panel) {
            $html = generateOverviewTemplate($panel);
            $html_A[] = $html;
        }
    }
    foreach ($panelSettings_B as $panel) {
        $html = generateOverviewTemplate($panel);
        $html_B[] = $html;
    }

    $html_A = implode("", $html_A);
    $html_B = implode("", $html_B);
    if ($html_A) {
        $html_A = "
            <div class='row instance-overview nolink'>
                {$html_A}
            </div>
        ";
    }
    $html_B = "
        <div class='row instance-overview'>
            {$html_B}
        </div>
    ";
    return "
        $html_A
        $html_B
    ";
}

function generateOverviewTemplate($panel)
{
    $h2class = !empty($panel['h2class']) ? $panel['h2class'] : "";
    $icon = !empty($panel['icon']) ? $panel['icon'] : "";
    $html = "
        <article class=\"col-md-4 status-block\">
            <dev>
                <div class='block-contents {$panel['panel']}'>
                    <p>
                    <i class=\"fas {$icon}\"></i>{$panel['title']}
                    <br/>
                    <span class=\"txt-ss\">{$panel['title2']}</span>
                    </p>
                    <div class=\"instance-count {$h2class}\">{$panel['count']}</div>
                </div>
                <div class='block-footer {$panel['panel']}'>
                    <a href='javascript:void(0);' data-filter='{$panel['filter']}'>
                        <span>{$panel['footer']}</span>
                        <i class=\"fas fa-arrow-circle-right\"></i>
                    </a>
                </div>
            </dev>
        </article>
    ";
    return $html;
}
