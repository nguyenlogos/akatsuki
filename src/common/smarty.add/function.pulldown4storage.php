<?php

function smarty_function_pulldown4storage($params, &$smarty)
{
    require_once(SMARTY_PLUGINS_DIR . 'shared.escape_special_chars.php');

    $name = "storage";
    $selected = "";
    $counter = "";

    foreach ($params as $_key => $_val) {
        switch ($_key) {
            case 'name':
            case 'counter':
                $$_key = (string) $_val;
                break;
            case 'selected':
                $$_key = smarty_function_escape_special_chars((string) $_val);
                break;
        }
    }

    $name = $name . $counter;

    $retStr = "<select class='form-control target two_storage' name='" . $name . "'>\n";
    $retStr .= "<option value='standard'";
    if ($selected == "standard") {
        $retStr .= " selected";
    }
    $retStr .= ">" . local_DeviceType("standard") . "\n";

    $retStr .= "<option value='gp2'";
    if ($selected == "gp2") {
        $retStr .= " selected";
    }
    $retStr .= ">" . local_DeviceType("gp2") . "\n";

    $retStr .= "<option value='io1'";
    if ($selected == "io1") {
        $retStr .= " selected";
    }
    $retStr .= ">" . local_DeviceType("io1") . "\n";
    $retStr .= "</select>\n";

    return $retStr;
}
