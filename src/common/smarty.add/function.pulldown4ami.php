<?php

function smarty_function_pulldown4ami($params, &$smarty)
{
    $name = "ami";
    $selected = "";

    foreach ($params as $_key => $_val) {
        $$_key = (string)$_val;
    }

    $sql = "
        SELECT
            a.name,
            a.platform,
            a.architecture,
            a.imageid
        FROM
            ami a,
            ami_enable b
        WHERE
            a.cid = {$_SESSION["cid"]}
            AND b.cid = {$_SESSION["cid"]}
            AND a.imageid = b.imageid
        ORDER BY
            a.platform,
            a.imageid;
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $retStr = "<SELECT class='form-control' name='" . $name . "'>\n";
    $retStr .= "<OPTION value=''>--- AMIを選択 ---\n";
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $aminame = pg_fetch_result($r, $i, 0);
        $platform = pg_fetch_result($r, $i, 1);
        $archi = pg_fetch_result($r, $i, 2);
        $imageid = pg_fetch_result($r, $i, 3);
        $retStr .= "<OPTION value='" . $imageid . "'";
        if($selected == $imageid) {
            $retStr .= " selected";
        }
        $retStr .= ">" . $aminame . " " . $platform . " " . $archi . "(" . $imageid . " )\n";
    }
    $retStr .= "</SELECT>\n";
    raise_sql($logs, 'func_ami');

    return $retStr;
}