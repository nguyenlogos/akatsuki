<?php
use GuzzleHttp\Client as GuzzleClient;

function postreq($key)
{
    if (!isset($_POST[$key])) {
        return "";
    }

    if (is_array($_POST[$key])) {
        return $_POST[$key];
    }

    $tmpStr = trim($_POST[$key]);

    if (strlen(str_replace("　", "", $tmpStr)) == 0) {
        return "";
    }
    return ($tmpStr);
}

function getreq($key)
{
    if (!isset($_GET[$key])) {
        return "";
    }

    if (is_array($_GET[$key])) {
        return $_GET[$key];
    }

    $tmpStr = trim($_GET[$key]);

    if (strlen(str_replace("　", "", $tmpStr)) == 0) {
        return "";
    }
    return ($tmpStr);
}

function sesreq($key)
{
    if (!isset($_SESSION[$key])) {
        return;
    }
    if (is_array($_SESSION[$key])) {
        return $_SESSION[$key];
    }
    $tmpStr = trim($_SESSION[$key]);
    if (strlen(str_replace("　", "", $tmpStr)) == 0) {
        return "";
    }
    return ($tmpStr);
}

function infoScreen(&$smarty, $msg)
{
    $smarty->assign('msg', $msg);
    $smarty->assign('pageTitle', '');
    $smarty->assign('userID', '');
    $smarty->assign('viewTemplate', 'info.tpl');
    $smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
}

function errorScreen(&$smarty, $msg)
{
    $smarty->assign('msg', $msg);
    $smarty->assign('pageTitle', '');
    $smarty->assign('userID', '');
    $smarty->assign('viewTemplate', 'error.tpl');
    $smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
}

function errorScreenIds(&$smarty, $msg)
{
    $smarty->assign('msg', $msg);
    $smarty->assign('viewTemplate', 'managerids/error.tpl');
    $smarty->display(TEMPLATES_PATH . 'managerids.tpl');
}

function insertLog(&$smarty, $msg)
{
    $sql = "SELECT name FROM emp
            WHERE cid=" . $_SESSION["cid"] . " AND email='" . $_SESSION["uid"] . "' AND status=0";
    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r)!=1) {
        $sql = "INSERT INTO logs (logdate,cid,email,name,act)
                VALUES (now()," . $_SESSION["cid"] . ",'" . $_SESSION["uid"] . "',null,
                    'ログ書き込み時に氏名の取得に失敗しました。email:" . $_SESSION["uid"] . "')";
        $r = pg_query($smarty->_db, $sql);
        $name = "不明";
    } else {
        $name = pg_fetch_result($r, 0, 0);
    }

    $sql = "INSERT INTO logs (logdate,cid,email,name,act)
            VALUES (now()," . $_SESSION["cid"] . ",'" . $_SESSION["uid"] . "',
            '" . pg_escape_string($name) . "','" . pg_escape_string($msg) . "')";
    $r = pg_query($smarty->_db, $sql);
}

// ページネーション文字列の生成
function getPagenationStr($smarty, $recCount, $recPerPage, $curPage, $linkURL)
{
    // ページ総数は切り上げ
    $totalPage = ceil($recCount / $recPerPage);
    $curPage = (int)$curPage;
    $prevPage = $curPage - 1;
    $nextPage = $curPage + 1;
    if ($curPage <= 0) {
        $prevPage = $curPage = 1;
        $nextPage = $curPage + 1;
    } elseif ($curPage > $totalPage) {
        $nextPage = $curPage = $totalPage;
        $prevPage = $curPage - 1;
    }
    // $info = parse_url($linkURL);
    // $query = $info ? $info['query'] : '';
    // parse_str($query, $params);

    $firstPageDisabled = $curPage === 1 ? 'disabled' : '';
    $lastPageDisabled = $curPage >= $totalPage ? 'disabled' : '';
    $prevPageDisabled = $curPage === 1 ? 'disabled' : '';
    $nextPageDisabled = $curPage >= $totalPage ? 'disabled' : '';

    $urlFirstPage = $linkURL . "1";
    $urlPrevPage  = $linkURL . $prevPage;
    $urlNextPage  = $linkURL . $nextPage;
    $urlLastPage  = $linkURL . $totalPage;

    $btnFirst = "
        <li class='page-item {$firstPageDisabled}'>
            <a class='page-link' href='{$urlFirstPage}' aria-label='First'>
                <i class='fas fa-angle-double-left'></i>
            </a>
        </li>
    ";
    $btnLast = "
        <li class='page-item {$lastPageDisabled}'>
            <a class='page-link' href='{$urlLastPage}' aria-label='Last'>
                <i class='fas fa-angle-double-right'></i>
            </a>
        </li>
    ";
    $btnPrev = "
        <li class='page-item {$prevPageDisabled}'>
            <a class='page-link' href='{$urlPrevPage}' aria-label='Previous'>
                <i class='fas fa-angle-left'></i>
            </a>
        </li>
    ";
    $btnNext = "
        <li class='page-item {$nextPageDisabled}'>
            <a class='page-link' href='{$urlNextPage}' aria-label='Next'>
                <i class='fas fa-angle-right'></i>
            </a>
        </li>
    ";

    $pad = 3;
    $sub = 0;
    $from = $curPage - $pad;
    if ($from < 1) {
        $sub = abs($from) + 1;
        $from = 1;
    }
    $to = $curPage + $pad + $sub;
    if ($to > $totalPage) {
        $sub = $to - $totalPage;
        $to = $totalPage;
        $from -= $sub;
        if ($from < 1) {
            $from = 1;
        }
    }
    $buttons = [];
    $pages = range($from, $to);
    foreach ($pages as $page) {
        $pageActive = "";
        if ($page == $curPage) {
            $pageActive = "active disabled";
        }
        $urlPage = $linkURL . $page;
        $buttons[] = "
            <li class='page-item {$pageActive}'>
                <a class='page-link' href='{$urlPage}'>{$page}</a>
            </li>
        ";
    }
    $buttons = array_merge([$btnFirst, $btnPrev], $buttons, [$btnNext, $btnLast]);
    $buttons = implode("", $buttons);
    $buttons = "<ul class='pagination'>{$buttons}</ul>";

    return $buttons;
}

function getPagenationStrOld($smarty, $recCount, $recPerPage, $curPage, $linkURL)
{
    // ページ総数は切り上げ
    $pageAllCount = ceil($recCount / $recPerPage);

    // 現在のページ番号
    if ($curPage == "" || is_numeric($curPage) == false) {
        $curPage = 1;
    }
    // ページ番号がページ最大値を超    えている場合は最大値に。
    if ($curPage > $pageAllCount) {
        $curPage = $pageAllCount;
    }

    // ページネーション文字列の生成
    $pagenationStr = "";
    $pagenationStr .= "<ul class='pagination'>\n";
    $pagenationStr .= "  <li>\n";
    if ($curPage > 1) {
        $pagenationStr .= "    <a href='" . $linkURL . ($curPage - 1) . "' aria-label='前のページへ'>\n";
    }

    $pagenationStr .= "      <span aria-hidden='true'>≪</span>\n";

    if ($curPage > 1) {
        $pagenationStr .= "    </a>\n";
    }
    $pagenationStr .= "  </li>\n";

    $startPage = $curPage - 4;
    if ($startPage < 1) {
        $startPage = 1;
    }

    $endPage = $startPage + 9;
    if ($endPage > $pageAllCount) {
        $endPage = $pageAllCount;
        $startPage = $endPage - 9;
        if ($startPage < 1) {
            $startPage = 1;
        }
    }

    if ($startPage != 1) {
        $pagenationStr .= "<li class='disabled'><a href=''>...</a></li>\n";
    }

    for ($i = $startPage; $i <= $endPage; $i++) {
        $pagenationStr .= "<li";
        if ($curPage  == $i) {
            $pagenationStr .= " class='active'";
        }
        $pagenationStr .= "><a href='" . $linkURL . $i;
        $pagenationStr .= "'>" . $i . "</a></li>\n";
    }

    if (($i - 1) != $pageAllCount) {
        $pagenationStr .= "<li class='disabled'><a href=''>...</a></li>\n";
    }

    $pagenationStr .= "  <li>\n";
    if ($curPage != $pageAllCount) {
        $pagenationStr .= "    <a href='" . $linkURL . ($curPage + 1) . "' aria-label='次のページへ'>\n";
    }
    $pagenationStr .= "      <span aria-hidden='true'>≫</span>\n";
    if ($curPage != $pageAllCount) {
        $pagenationStr .= "    </a>\n";
    }

    $pagenationStr .= "</ul>\n";

    return($pagenationStr);
}

function dateCool($dateStr)
{
    if ($dateStr == "") {
        return ("未");
    }

    $tmpDate = strtotime($dateStr);

    $retStr = date("Y/m/d H:i", $tmpDate);

    return($retStr);
}

function setStrForDB($tmpStr, $flag)
{
    if ($tmpStr == "") {
        $retStr = "null";
    } else {
        $retStr = "'" . pg_escape_string($tmpStr) . "'";
    }

    if ($flag == true) {
        $retStr .= ",";
    }

    return ($retStr);
}

function getServiceShortName($longName)
{
    global $ServiceShortName;

    if (array_key_exists($longName, $ServiceShortName)) {
        return ($ServiceShortName[$longName]);
    }
    return ($longName);
}

function parseAjaxRequestParams()
{
    $params = (array)json_decode(file_get_contents('php://input'), true);
    if (count($params) === 0) {
        $params = $_POST;
    }
    return $params;
}

function sendAjaxResponse($object = null, $exit = true)
{
    header('Content-type:application/json;charset=utf-8');
    $json = false;

    if (is_array($object) || !empty($object)) {
        $json = json_encode($object);
    }

    $json === false && $json = '{}';
    echo $json;
    $exit && exit();
}

function getInfoInstanceOverview($smarty, $cid, $dept = null, $projs = [])
{
    // generate data for instance overview panel
    $instInfo = [
        'reqRunningCount'  => 0,
        'reqWarningCount'  => 0,
        'reqOutdatedCount' => 0,
        'activeCount'      => 0,
        'inactiveCount'    => 0,
    ];

    $conditions = [
        "(i.state != 'terminated')",
        "(i.state != 'shutting-down')",
        "(i.cid = $cid)"
    ];
    if (count($projs)) {
        if (count($projs)) {
            $projs = array_map(function ($pcode) {
                return "'{$pcode}'";
            }, $projs);
            $conditions[] = sprintf("(i.pcode IN (%s))", implode(",", $projs));
        }
    }
    if ($dept) {
        $conditions[] = "p.dept = {$dept}";
    }
    $conditions = "WHERE " . implode(' AND ', $conditions);
    $sql = "
        SELECT
            i.state, ir.approved_status, (ir.using_till_date::date + '0.9999 days'::interval) as using_till_date
        FROM
            instance i
        LEFT JOIN inst_req ir
            ON ir.id = i.inst_req_id
        LEFT OUTER JOIN proj p
            ON p.pcode = i.pcode
            AND p.cid = {$cid}
            AND p.status = 0
        $conditions
    ";
    $r = pg_query($smarty->_db, $sql);
    $instList = pg_fetch_all($r);
    if (!$instList) {
        return $instInfo;
    }
    foreach ($instList as $inst) {
        if ($inst['state'] === 'running' || $inst['state'] === 'pending') {
            $instInfo['activeCount']++;
        } elseif ($inst['state'] === 'stopped' || $inst['state'] === 'stopping') {
            $instInfo['inactiveCount']++;
        }

        if (!empty($inst['using_till_date'])) {
            $approvedStatus = (int)$inst['approved_status'];
            if ($approvedStatus === INST_REQ_APPROVED) {
                $expiredDate = strtotime($inst['using_till_date']);
                $timenow = time();
                if ($timenow > $expiredDate) {
                    $instInfo['reqOutdatedCount']++;
                } elseif ($expiredDate - $timenow < 86400 * 14) {
                    $instInfo['reqWarningCount']++;
                }
            }
        }
    }
    $instInfo['reqRunningCount'] = (int)$_SESSION['wfcount'];
    return $instInfo;
}

function getInstanceRequestInfo($smarty, $reqID)
{
    $reqID = (int)$reqID;
    $sql = "
        SELECT
            i.id,
            i.cid,
            i.empid,
            e.name AS uname,
            i.dept,
            d.deptname,
            i.pcode,
            p.pname,
            i.inst_ami,
            i.inst_type,
            i.inst_count,
            i.inst_region,
            i.inst_vpc,
            i.inst_subnet,
            i.inst_address,
            i.security_groups,
            i.shutdown_behavior,
            i.approved_status,
            i.using_purpose,
            i.using_from_date,
            i.using_till_date,
            i.note,
            i.key_name,
            irc.id AS change_state,
            irc.empid as empid_new,
            e1.name as uname_new,
            irc.dept as dept_new,
            d1.deptname as deptname_new,
            irc.pcode as pcode_new,
            p1.pname as pname_new,
            irc.using_purpose as using_purpose_new,
            irc.inst_address as inst_address_new,
            irc.using_from_date as using_from_date_new,
            irc.using_till_date as using_till_date_new
        FROM
            inst_req i
        LEFT JOIN
            inst_req_change irc
            ON irc.cid = i.cid
            AND irc.inst_req_id = i.id
        LEFT JOIN
            dept d
                ON d.dept = i.dept
        LEFT JOIN
            dept d1
                ON d1.dept = irc.dept
        LEFT JOIN
            proj p
                ON p.pcode = i.pcode
        LEFT JOIN
            proj p1
                ON p1.pcode = irc.pcode
        LEFT JOIN
            emp e
            ON e.cid = i.cid
            AND e.empid = i.empid
        LEFT JOIN
            emp e1
            ON e1.cid = irc.cid
            AND e1.empid = irc.empid
        WHERE
            i.id = {$reqID}
        LIMIT 1
    ";
    $r = pg_query($smarty->_db, $sql);
    if (!$r) {
        return null;
    }
    $instance = pg_fetch_all($r);
    if (!$instance) {
        return null;
    }
    $instance = array_pop($instance);

    $sql = '
        SELECT
            bs.device_name,
            bs.volumn_type,
            bs.volumn_size,
            bs.iops,
            bs.encrypted,
            bs.auto_removal,
            bs.snapshot_id
        FROM
            inst_req_bs bs
        WHERE
            bs.inst_req_id = $1
    ';
    $r = pg_query_params($smarty->_db, $sql, [
        $reqID
    ]);
    $blockStores = $r ? pg_fetch_all($r) : null;
    $instance['inst_bs'] = $blockStores ?: [];
    return $instance;
}

function getGroupManagers($smarty, $deptID, $cid = null)
{
    !$cid && $cid = $_SESSION['cid'];
    $sql = "
        SELECT
            *
        FROM
            emp
        WHERE
            dept = $1
            AND cid = $2
            AND admin = 1
            AND status = 0;
    ";
    $r = pg_query_params($smarty->_db, $sql, [
        $deptID,
        $cid,
    ]);
    $result = pg_fetch_all($r);
    return $result;
}

function getEc2Creds($smarty, $cid)
{
    $sql = "SELECT key, secret FROM users WHERE cid = $1 LIMIT 1";
    $r = pg_query_params($smarty->_db, $sql, [
        $cid
    ]);
    $creds = pg_fetch_assoc($r);
    if (!$creds) {
        return null;
    }
    return $creds;
}

function getEc2Client($smarty, $cid, $ec2Region)
{
    if (!$ec2Region) {
        return null;
    }
    $ec2Region = preg_replace('/(.+\d)[a-z]$/', '$1', $ec2Region);

    $creds = getEc2Creds($smarty, $cid);
    if (!$creds) {
        return null;
    }

    $clientConfigs = [
        'credentials' => [
            'key' => decryptIt($creds["key"]),
            'secret' => $creds["secret"]
        ],
        'region' => $ec2Region,
        'version' => AWS_SDK_VERSION,
    ];
    $ec2Client = new Aws\Ec2\Ec2Client($clientConfigs);
    return $ec2Client;
}

function openConnection()
{
    $conn = @pg_connect(DB_CONNECT_INFO);
    $status = @pg_connection_status($conn);
    if ($status !== PGSQL_CONNECTION_OK) {
        throw new Exception(MESSAGES['ERR_DB_CONNECT'], 1);
    }
    return $conn;
}

/**
 * @link https://gist.github.com/abtris/1437966
 */
function tracesFormater($exception, $toString = false)
{
    $traces = [];
    $count = 0;
    foreach ($exception->getTrace() as $frame) {
        $args = "";
        if (isset($frame['args'])) {
            $args = array();
            foreach ($frame['args'] as $arg) {
                if (is_string($arg)) {
                    $args[] = "'" . $arg . "'";
                } elseif (is_array($arg)) {
                    $args[] = "Array";
                } elseif (is_null($arg)) {
                    $args[] = 'NULL';
                } elseif (is_bool($arg)) {
                    $args[] = ($arg) ? "true" : "false";
                } elseif (is_object($arg)) {
                    $args[] = get_class($arg);
                } elseif (is_resource($arg)) {
                    $args[] = get_resource_type($arg);
                } else {
                    $args[] = $arg;
                }
            }
            $args = join(", ", $args);
        }
        $traces[] = sprintf(
            "#%s %s(%s): %s(%s)",
            $count,
            isset($frame['file']) ? $frame['file'] : 'unknown file',
            isset($frame['line']) ? $frame['line'] : 'unknown line',
            (isset($frame['class']))  ? $frame['class'].$frame['type'].$frame['function'] : $frame['function'],
            $args
        );
        $count++;
    }
    if ($toString) {
        return implode("\n\n", $traces);
    }

    return $traces;
}

if (!function_exists('array_rap')) {
    function array_rap(String $key, array $arr)
    {
        if (!array_key_exists($key, $arr)) {
            return null;
        }
        return $arr[$key];
    }
}

if (!function_exists('log_format')) {
    function log_format(array $data)
    {
        $data = htmlspecialchars(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        return preg_replace('~\\\/~', '/', $data);
    }
}

if (!function_exists('rd')) {
    function rd($path, $check_path = false)
    {
        $r = trim($_SERVER['REQUEST_URI'], " /");
        if ($r === $path) {
            return;
        }
        ob_end_clean();
        header("Location: ". ROOT_HOME . $path);
        exit();
    }
}

function raise_sql($message, $screen)
{
    if (is_cli_env()) {
        return;
    }
    $serror =
        "Env:       " . @$_SERVER['SERVER_NAME'] . "\r\n" .
        "timestamp: " . Date('m/d/Y H:i:s') . "\r\n" .
        "Screen:    " . @$_SERVER['REQUEST_URI'] . "\r\n".
        "Sql:\r\n";
    if (is_array($message)) {
        foreach ($message as $key => $value) {
            $serror .= $value."\r\n\r\n";
            $serror .= "------------------------------------";
            $serror .= "\r\n\r\n";
        }
    } else {
        $serror .= $message;
        $serror .= "\r\n\r\n";
    }
    $serror .= "=======================================================================================";
    $serror .= "\r\n\r\n";

    // write log sql
    $fhandle = fopen(LOGSQL_PATH.'/list_'.$screen.'_sql.txt', 'a');
    if ($fhandle) {
        fwrite($fhandle, $serror);
        fclose($fhandle);

        return $fhandle;
    }
}

function BaseModel_sql($message, $screen)
{
    if (is_cli_env()) {
        return;
    }
    $serror =
        "Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
        "timestamp: " . Date('m/d/Y H:i:s') . "\r\n" .
        "Screen:    " . $_SERVER['REQUEST_URI'] . "\r\n".
        "Sql:  \r\n";
    foreach ($message as $key => $value) {
        $serror .= $value['query']."\r\n\r\n";
        $serror .= "Parameter:  \r\n";
        foreach ($value['bindings'] as $array) {
            $serror .= $array."\r\n";
        }
        $serror .= "------------------------------------";
        $serror .= "\r\n\r\n";
    }
    $serror .= "=======================================================================================";
    $serror .= "\r\n\r\n";

    // write log sql
    $fhandle = fopen(LOGSQL_PATH.'/model_'.$screen.'_sql.txt', 'a');
    if ($fhandle) {
        fwrite($fhandle, $serror);
        fclose($fhandle);

        return $fhandle;
    }
}

function ctype_int($num_ber)
{
    return preg_match('/^[0-9]+(\.[0-9]{2})?$/', $num_ber) ? true : false;
}

function g_captcha_verify($g_captcha_response)
{
    if (!$g_captcha_response) {
        return false;
    }
    $client = new GuzzleClient();
    $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
        'form_params' => [
            'secret'   => G_RECAPTCHA_SECRET_KEY,
            'response' => $g_captcha_response,
        ]
    ]);
    $responseBody = $response->getBody();
    $response = json_decode((string) $responseBody, true);
    if (array_key_exists('success', $response) && $response['success'] == true) {
        return true;
    }
    return false;
}
/**
 * @link https://gist.github.com/raveren/5555297
 */
function random_string($type = 'alnum', $length = 32)
{
    switch ($type) {
        case 'alnum':
            $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
        case 'alpha':
            $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
        case 'hexdec':
            $pool = '0123456789abcdef';
            break;
        case 'numeric':
            $pool = '0123456789';
            break;
        case 'nozero':
            $pool = '123456789';
            break;
        case 'distinct':
            $pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
            break;
        default:
            $pool = (string) $type;
            break;
    }


    $crypto_rand_secure = function ($min, $max) {
        $range = $max - $min;
        if ($range < 0) {
            return $min;
        } // not so random...
        $log    = log($range, 2);
        $bytes  = (int) ($log / 8) + 1; // length in bytes
        $bits   = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    };

    $token = "";
    $max   = strlen($pool);
    for ($i = 0; $i < $length; $i++) {
        $token .= $pool[$crypto_rand_secure(0, $max)];
    }
    return $token;
}

function netmask($mask)
{
    return (long2ip(ip2long(MESSAGES['POLICY_NET_MASK'])
        << (32-$mask)));
}

function network($ip, $mask)
{
    return (long2ip((ip2long($ip))
        & (ip2long(netmask($mask)))));
}

function aws_handle_exception(\Aws\Exception\AwsException $e, $description = "")
{
    $message = $e->getAwsErrorMessage();
    if (is_cli_env()) {
        $GLOBALS['ERR_AWS_MSG'] = $message;
    } else {
        $_SESSION['ERR_AWS_API'] = 1;
        $_SESSION['ERR_AWS_MSG'] = $message;
        if ($message === AWS_INVALID_CREDENTIALS) {
            $_SESSION['ERR_AWS_KEY'] = 1;
        }
    }
    if ($description && $message) {
        $logContents = [
            'desc' => $description,
            'err_msg' => $message,
        ];
        \Common\Logger::error(json_encode($logContents), LOG_TYPES['AWS']);
    }
}

function aws_get_last_error()
{
    if (is_cli_env()) {
        if (!empty($_SESSION['ERR_AWS_MSG'])) {
            $message = $GLOBALS['ERR_AWS_MSG'];
            unset($GLOBALS['ERR_AWS_MSG']);
            return $message;
        }
    } elseif (!empty($_SESSION['ERR_AWS_MSG'])) {
        $message = $_SESSION['ERR_AWS_MSG'];
        $_SESSION['ERR_AWS_API'] = 0;
        $_SESSION['ERR_AWS_MSG'] = '';
        return $message;
    }
    return "";
}

function aws_rule_convert_timezone($expr, $timezone, $local = false)
{
    $expr = preg_replace('/cron\(([^)]+)\)/', '$1', $expr);
    $arr = explode(" ", $expr);
    if (count($arr) !== 6) {
        return false;
    }

    if ($arr[5] !== "*" || $arr[3] !== "*") {
        return false;
    }

    $sign = $local ? 1 : -1;

    list($hour, $minute) = explode(".", $timezone);
    $hour = (int)$hour;
    $minute = $minute ? (float)"0.{$minute}" * 60 : 0;

    if ($sign * $hour > 0) {
        $minute = (int)$arr[0] + (int)$minute;
    } else {
        $minute = (int)$arr[0] - (int)$minute;
    }
    $hour = (int)$arr[1] + $hour * $sign;

    if ($minute > 59) {
        $minute = $minute - 60;
        $hour = $hour + 1;
    } elseif ($minute < 0) {
        $minute = $minute + 60;
        $hour = $hour - 1;
    }
    if ($hour > 23) {
        $hour = $hour - 24;
    } elseif ($hour < 0) {
        $hour = $hour + 24;
    }
    $arr[0] = $minute;
    $arr[1] = $hour;
    $expr = sprintf("cron(%s)", implode(" ", $arr));

    return $expr;
}

function aws_rule_format_timezone($timezone)
{
    list($gmtHour, $gmtMin) = explode(".", $timezone);
    $gmtMin = (int)(floatval("0.{$gmtMin}") * 60);
    $gmtTime = sprintf("%02d:%02d", $gmtHour, $gmtMin);
    $gmtTime = floatval($timezone) >= 0 ? '+'.$gmtTime : '-'.$gmtTime;
    return "GMT".$gmtTime;
}

function validate_password_policy($password, $cid = null)
{
    if (empty($cid)) {
        $cid = sesreq("cid");
    }
    if (empty($cid)) {
        $configs = [
            'pass_number'    => SETTING_DEFAULT_PASSWORD_CHECK_NUMBER,
            'pass_lowercase' => SETTING_DEFAULT_PASSWORD_CHECK_CASE,
            'pass_lenght'    => SETTING_DEFAULT_PASSWORD_LENGTH,
        ];
    } else {
        $configs = \Akatsuki\Models\Configs::where("cid", $cid)->first();
        if (!$configs) {
            return [
                'err' => false
            ];
        }
        $configs = $configs->toArray();
    }
    $valid = true;
    $errors = [];
    if ($configs['pass_lowercase']) {
        if (!preg_match('/[a-z]/', $password)) {
            $errors[] = MESSAGES['ERR_PASSWORD_POLICY_LOWERCASE'];
        }
        if (!preg_match('/[A-Z]/', $password)) {
            $errors[] = MESSAGES['ERR_PASSWORD_POLICY_UPPERCASE'];
        }
    }
    if ($configs['pass_number']) {
        if (!preg_match('/[0-9]/', $password)) {
            $errors[] = MESSAGES['ERR_PASSWORD_POLICY_NUMBER'];
        }
    }
    $length = (int)$configs['pass_lenght'];
    if ($length === 0) {
        $length = SETTING_DEFAULT_PASSWORD_LENGTH;
    }
    if (mb_strlen($password) < $length) {
        $errors[] = str_replace('{length}', $length, MESSAGES['ERR_PASSWORD_POLICY_LENGTH']);
    }

    if (count($errors)) {
        return [
            'err'  => true,
            'msgs' => $errors,
        ];
    }
    return [
        'err' => false
    ];
}

function load_keypem($name)
{
    if (!$name) {
        return false;
    }
    $filepath = KEYPAIR_PATH . "/{$_SESSION['accountid']}/{$name}.pem";
    if (!is_readable($filepath)) {
        return false;
    }

    return $filepath;
}

// Validate follow yyyy/mm/dd format
function validate_date($request_input)
{
    $pattern = '/^[0-9]{4}\/[0-9]{2}\/[0-9]{2}$/';
    $result = preg_match($pattern, $request_input);
    if ($result) {
        $yyyy = substr($request_input, 0, 4);
        $mm = substr($request_input, 5, 2);
        $dd = substr($request_input, 8, 9);
        $result = checkdate($mm, $dd, $yyyy);
        if (checkdate($mm, $dd, $yyyy)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function is_cli_env()
{
    return php_sapi_name() === 'cli';
}

function get_ip_address()
{
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && validate_ip($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }

    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (validate_ip($ip)) {
                    return $ip;
                }
            }
        } else {
            if (validate_ip($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && validate_ip($_SERVER['HTTP_X_FORWARDED'])) {
        return $_SERVER['HTTP_X_FORWARDED'];
    }
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && validate_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && validate_ip($_SERVER['HTTP_FORWARDED_FOR'])) {
        return $_SERVER['HTTP_FORWARDED_FOR'];
    }
    if (!empty($_SERVER['HTTP_FORWARDED']) && validate_ip($_SERVER['HTTP_FORWARDED'])) {
        return $_SERVER['HTTP_FORWARDED'];
    }

    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
function validate_ip($ip)
{
    if (strtolower($ip) === 'unknown') {
        return false;
    }

    // generate ipv4 network address
    $ip = ip2long($ip);

    // if the ip is set and not equivalent to 255.255.255.255
    if ($ip !== false && $ip !== -1) {
        // make sure to get unsigned long representation of ip
        // due to discrepancies between 32 and 64 bit OSes and
        // signed numbers (ints default to signed in PHP)
        $ip = sprintf('%u', $ip);
        // do private network range checking
        if ($ip >= 0 && $ip <= 50331647) {
            return false;
        }
        if ($ip >= 167772160 && $ip <= 184549375) {
            return false;
        }
        if ($ip >= 2130706432 && $ip <= 2147483647) {
            return false;
        }
        if ($ip >= 2851995648 && $ip <= 2852061183) {
            return false;
        }
        if ($ip >= 2886729728 && $ip <= 2887778303) {
            return false;
        }
        if ($ip >= 3221225984 && $ip <= 3221226239) {
            return false;
        }
        if ($ip >= 3232235520 && $ip <= 3232301055) {
            return false;
        }
        if ($ip >= 4294967040) {
            return false;
        }
    }
    return true;
}
