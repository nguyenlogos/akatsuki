<?php
namespace Common;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;

class FileLogger extends Logger
{
    private $format = array(
        "date" => "Y-m-d H:i:s",
        "message" => "[%datetime%][%channel%][%level_name%] : %message% %context%\n\n",
    );

    public function __construct($name, array $handlers = array(), array $processors = array())
    {
        parent::__construct($name, $handlers, $processors);
        $logfile = sprintf("%s/%s.log", LOGS_PATH, date('Ymd'));
        $streamHandler = new StreamHandler($logfile);
        $streamHandler->setFormatter(new LineFormatter($this->format['message'], $this->format['date']));
        $this->pushHandler($streamHandler);
    }
}