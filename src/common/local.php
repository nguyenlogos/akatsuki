<?php

function local_regionName($regionName)
{
  // 地域名の日本語訳定義
    $localNames = array(
    "ap-northeast-1"  => "東京",
    "ap-northeast-2"  => "ソウル",
    "ap-south-1"      => "ムンバイ",
    "ap-southeast-1"  => "シンガポール",
    "ap-southeast-2"  => "シドニー",
    "ca-central-1"    => "カナダ",
    "eu-central-1"    => "フランクフルト",
    "eu-west-1"       => "アイルランド",
    "eu-west-2"       => "ロンドン",
    "eu-west-3"       => "パリ",
    "sa-east-1"       => "サンパウロ",
    "us-east-1"       => "バージニア北部",
    "us-east-2"       => "オハイオ",
    "us-west-1"       => "北カリフォルニア",
    "us-west-2"       => "オレゴン",
    );

    if (array_key_exists($regionName, $localNames)) {
        return($localNames[$regionName]);
    }
    return($regionName);
}

function local_instType($row)
{
    $localUsage = array( "Compute optimized" => "CPU重視",
    "Memory optimized" => "ﾒﾓﾘ重視",
    "GPU instances" => "GPU搭載",
    "Storage optimized" => "ｽﾄﾚｰｼﾞ重視",
    "General purpose" => "汎用",
    "Micro instances" => "最小構成" );

    $localNetwork = array( "Moderate" => "普通",
    "High" => "高速",
    "10 Gigabit" => "10ｷﾞｶﾞﾋﾞｯﾄ",
    "Low" => "低速",
    "Very Low" => "とても低速",
    "Low to Moderate" => "低速～普通" );

    if ($row[0] != "") {
        $row[0] = "利用可能";
    }
    if ($row[1] == "Yes") {
        $row[1] = "最新";
    } elseif ($row[1] == "No") {
        $row[1] = "";
    }

    if (array_key_exists($row[2], $localUsage)) {
        $row[2] = $localUsage[$row[2]];
    }

    if ($row[6] == "EBS only") {
        $row[6] = "EBSのみ";
    }

    if ($row[7] == "Yes") {
        $row[7] = "はい";
    } elseif ($row[7] == "No") {
        $row[7] = "いいえ";
    }

    if (array_key_exists($row[8], $localNetwork)) {
        $row[8] = $localNetwork[$row[8]];
    }

    return($row);
}

function local_Shutdownbe($name)
{

    if ($name == "stop") {
        return ("停止したまま保存(再起動可能)");
    }
    if ($name == "terminate") {
        return ("利用終了のため消去");
    }

    return($name);
}

function local_BlockDeviceMappings($listDev)
{
    if ($listDev["enc"] == false) {
        $listDev["enc"] = "いいえ";
    } else {
        $listDev["enc"] = "はい";
    }

    $listDev["devtype"] = local_DeviceType($listDev["devtype"]);

    if ($listDev["autodel"] == true) {
        $listDev["autodel"] = "はい";
    } else {
        $listDev["autodel"] = "いいえ";
    }

    return($listDev);
}

function local_DeviceType($devtype)
{
    if ($devtype == "standard") {
        return("磁気ﾃﾞｨｽｸ");
    }
    if ($devtype == "gp2") {
        return("汎用SSD");
    }
    if ($devtype == "io1") {
        return("IOPS提供型SSD");
    }

    return($devtype);
}
