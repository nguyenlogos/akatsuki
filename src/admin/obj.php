<?php
use Akatsuki\Models\Obj;

require_once(ROOT_PATH . "/if/updateSG.php");

$msg = "";
$errmsg = "";

\Akatsuki\Models\Sg::updateSecurityGroups();

$objid = (int)getreq("id");
$requestMethod = $_SERVER['REQUEST_METHOD'];
if ($requestMethod === 'POST' && $objid > 0) {
    $result = false;
    // DELETE
    $obj = Obj::find($objid);
    if ($obj) {
        $obj->status = -1;
        $result = $obj->save();
    }

    if ($result) {
        $msg = "削除しました。";
    } else {
        $errmsg = "失敗しました";
    }
}

// determine sort column
$sortIndex = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
$sortMap = [
    "a.name","a.name DESC",
    "a.ipaddr","a.ipaddr DESC",
    "a.submask","a.submask DESC",
    "b.sg_count","b.sg_count DESC",
];
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

$conditions = [
    "a.cid = {$_SESSION['cid']}",
    "a.status = 0"
];
$qObjName = htmlspecialchars(getreq('objname'));

if ($qObjName) {
    $qObjName = pg_escape_string($qObjName);
    $conditions[] = sprintf("UPPER(name) LIKE '%%%s%%'", strtoupper($qObjName));
}
$conditions = implode(' AND ', $conditions);
// count total record
$sql = "SELECT count(a.id) FROM obj a WHERE $conditions";
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);
$currentPage = getreq('p');
if ($currentPage <= 0) {
    $currentPage = 1;
}
$offset = ($currentPage -1) * ITEMS_PER_PAGE;
$paginationStr = '';
if ($dataCount > 0) {
    $paginationStr = getPagenationStr(
        $smarty,
        $dataCount,
        ITEMS_PER_PAGE,
        $currentPage,
        "./obj.php?sk=$sortIndex&p="
    );
}

// オブジェクトリストの取得
$sql = "
    SELECT
        a.id,
        a.name,
        a.ipaddr,
        a.submask,
        a.description,
        COALESCE(b.sg_count, 0) AS sg_count
    FROM obj a
    LEFT JOIN (
        SELECT
            tmp.id,
            COUNT(tmp.id) AS sg_count
        FROM (
            SELECT
                a.id,
                si.id AS sg_id
            FROM
                obj a
            LEFT JOIN
                sg_ipperm si
            ON
                si.cid = a.cid
            AND
                si.iprange = (a.ipaddr || '/' || a.submask)
            WHERE
                a.cid = {$_SESSION['cid']}
                AND si.cid IS NOT NULL
            GROUP BY a.id, si.id, si.cid
        ) tmp
        GROUP BY tmp.id
    ) b
    ON
        b.id = a.id
    WHERE
        $conditions
    ORDER BY $sortColumn
    LIMIT ".ITEMS_PER_PAGE." OFFSET {$offset}
";
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);

$objList = pg_fetch_all($r);
if (!$objList) {
    $objList = [];
}

$headerList = [
    'name' => [
        'disp_name' => 'オブジェクト名',
        'sortable'  => true
    ],
    'ipaddr' => [
        'disp_name' => 'IPアドレスレンジ',
        'sortable'  => true
    ],
    'submask' => [
        'disp_name' => 'サブネットマスク',
        'sortable'  => true
    ],
    'sg_count' => [
        'disp_name' => 'セキュリティグループ数',
        'sortable'  => true
    ],
    'description' => [
        'disp_name' => '説明',
        'class_name' => 'text-wrap',
        'sortable'  => false
    ]
];

$btnTemplate = '<button type="button" class="btn $btnClass" onclick="$onClick">$btnName</button>';

$actionList = [
    [
        'template' => $btnTemplate,
        '$btnName' => '変更',
        '$btnClass' => 'btn-blue btn-sm mr-3 mb-1',
        '$btnIcon' => 'fas fa-edit',
        '$onClick' => 'editRow(this);',
    ],
    [
        'template' => $btnTemplate,
        '$btnName' => '削除',
        '$btnClass' => 'btn-danger btn-sm mb-1',
        '$btnIcon' => 'fas fa-trash',
        '$onClick' => 'removeRow(this);'
    ]
];
raise_sql($logs, 'obj');

$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);

$smarty->assign('qObjName', htmlspecialchars($qObjName));
$smarty->assign('sortIndex', $sortIndex);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('objList', $objList);
$smarty->assign('headerList', $headerList);
$smarty->assign('actionList', $actionList);
$smarty->assign('paginationStr', $paginationStr);
$smarty->assign('viewTemplate', 'admin/obj.tpl');

// 以下は定番
$smarty->assign('pageTitle', 'オブジェクト管理');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
