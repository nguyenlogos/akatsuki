<?php
require_once('../common/CommonClass.class.php');
require_once('../common/define.php');
require_once('../common/auth.php');
require_once('../common/common.php');
// Include the SDK using the Composer autoloader
require_once '../aws/aws-autoloader.php';
require_once('../if/updateInst.php');
require_once('../if/updateSG.php');
require_once('../if/updateAMI.php');

$smarty = new MySmarty();
//共通情報をセット
$smarty->commonInfoSet();

// ログイン確認
if (setAuthInfo($smarty->_db) != 'ok') {
  errorScreen($smarty, "ログインしていません。");
  exit;
}

$msg = "";
if ($_POST["button"] == "更新" || $_GET["op"] == "1") {
  $config = array(
    'key'    => $_SESSION["key"],
    'secret' => $_SESSION["secret"],
    'region' => $_SESSION["region"],
  );
  $ret = updateAMI($_SESSION["cid"],$smarty, $config);
  if ($ret < 0) {
  // AMIリスト更新エラー
    if ($ret == -1) {
      $msg = "AMI情報の取得に失敗しました。\n[ユーザ設定]の[認証情報の設定]を確認してください。\n";
    } else {
      $msg = "AMI情報の取得に失敗しました。\nシステムエラーの可能性があるため、しばらく経ってから再度試してみてください。\n長時間、復旧しない場合はお問い合わせフォームよりご連絡ください。\n";
    }
  }
} 

// AMIリストの最終更新日時を取得
$sql = "select max(lastchecked) from ami where cid=" . $_SESSION["cid"];
$r = pg_query($smarty->_db, $sql);
if (pg_num_rows($r) == 0) {
   $lastUpdate = "（未）";
} else {
  $lastUpdate = pg_fetch_result($r, 0, 0);
}

// AMIリストの取得
$sql = "select pub, name, imageid, state, platform, architecture from ami where cid=" . $_SESSION["cid"] . " order by imageid;";
$r = pg_query($smarty->_db, $sql);

$amiList = "";
for ($i = 0; $i < pg_num_rows($r); $i++) {
    $row = pg_fetch_row($r, $i);
    if ($amiList != "") { $amiList .= ","; }
    if ($row[3] == 'available') {  $row[3] = "利用可能"; }
    else { $row[3] = "利用不可"; }

//    $row[7] = str_replace("-", "/", $row[7]);
//    $row[7] = str_replace("T", " ", $row[7]);
//    $row[7] = str_replace(".000Z", "", $row[7]);

    $amiList .= "['" . $row[0] . "','" . $row[1] . "','" . $row[2] . "','" . $row[3] . "','" .  $row[4] . "','" . $row[5] . "','" . $row[6] . "']";
}

$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('amiList', $amiList);
$smarty->assign('lastUpdate', $lastUpdate);
$smarty->assign('viewTemplate', 'admin/test.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
?>
