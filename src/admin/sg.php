<?php
use Akatsuki\Models\EmpProj;

$deptRoles = permission_check('mst/dept.php');
$pageRoles = permission_check('mst/userpj.php');

$hideDept = false;

$frmValues = [
    'sgname' => getreq('sgname'),
];

if (postreq('op') == '1') {
    \Akatsuki\Models\Sg::updateSecurityGroups();
    rd('admin/sg.php');
}

$whereConditions = [
    "sg.cid = {$_SESSION['cid']}",
];

if (!empty($frmValues['sgname'])) {
    $fmSgName = strtoupper(pg_escape_string($frmValues['sgname']));
    $whereConditions[] = "(upper(sg.name) LIKE '%{$fmSgName}%')";
}
$whereConditions = "WHERE " . implode(" AND ", $whereConditions);

$sql = "
    SELECT
        sg.id as sgid,
        sg.name as sgname,
        sg.vpcid,
        sgi.num_policy,
        COALESCE(sge.id, 0) AS checked
    FROM
        sg sg
    INNER JOIN
    (
        SELECT
            sgi.cid,
            sgi.id,
            COUNT(sgi.id) AS num_policy
        FROM
            sg_ipperm sgi
        WHERE
            sgi.cid = {$_SESSION['cid']}
        GROUP BY
            sgi.cid,
            sgi.id
    ) sgi
        ON sgi.cid = sg.cid
        AND sgi.id = sg.id
    LEFT JOIN
    (
        SELECT
            sge.id,
            sge.cid,
            sge.sg_id
        FROM
            sg_enable sge
        WHERE
            sge.cid = {$_SESSION['cid']}
    ) sge
        ON sge.cid = sg.cid
        AND sge.sg_id = sg.id
    {$whereConditions}
";

// 併せてSQLのソートキーを設定
$sortMap = [
    "sgid ASC", "sgid DESC",
    "sgname ASC", "sgname DESC",
    "vpcid ASC", "vpcid DESC",
    "sgi.num_policy ASC", "sgi.num_policy DESC",
];
$sortIndex  = (int)getreq("sk");
$sortColumn = "";

if ($sortIndex < 0) {
    $sortIndex = 0;
}

if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

list($sortColumn, $direction) = explode(" ", $sortColumn);

if ($sortColumn !== '') {
    $sql .= "
        ORDER BY
            {$sortColumn} {$direction}
    ";
}

$result = pg_query($smarty->_db, $sql);
$tableList = pg_fetch_all($result) ?: [];

$headerList = [
    'checked' => [
        'data_attr' => true,
        'disp_name' => '選択',
        'width'     => 75,
    ],
    'sgid' => [
        'data_attr' => true,
        'disp_name' => 'セキュリティグループ',
        'sortable'  => true,
        'width'     => 220,
    ],
    'sgname' => [
        'disp_name' => 'セキュリティグループ名',
        'sortable'  => true
    ],
    'vpcid' => [
        'disp_name' => 'ＶＰＣ',
        'sortable'  => true,
    ],
    'num_policy' => [
        'disp_name' => 'ポリシー設定数',
        'sortable'  => true,
        'width'     => 150,
    ]
];

$btnTemplate = '&nbsp;<button type="button" class="btn btn-change $btnClass">$btnName</button>';

$actionList = [];

$smarty->assign('tablelist', $tableList);
$smarty->assign('headerList', $headerList);
$smarty->assign('actionList', $actionList);
$smarty->assign('frmValues', $frmValues);
$smarty->assign('hideDept', $hideDept ? 'hide' : '');

$smarty->assign('pageTitle', 'セキュリティグループ管理');
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);

$smarty->assign('viewTemplate', 'admin/sg.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
