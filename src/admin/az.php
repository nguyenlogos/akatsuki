<?php
require_once(ROOT_PATH . '/if/updateAZ.php');

use Akatsuki\Models\AzoneEnable;

$cid = "";
$cid = (int)$_SESSION["cid"];

if ($_SERVER['REQUEST_METHOD'] === 'POST' && getreq('ajax') == '1') {
    $enabledList = postreq('enabled_list');
    $disabledList = postreq('disabled_list');
    $result = true;
    if ($enabledList && count($enabledList) > 0) {
        $result = AzoneEnable::enable($_SESSION['cid'], $enabledList);
    }
    if ($result && $disabledList && count($disabledList) > 0) {
        $result = AzoneEnable::disable($_SESSION['cid'], $disabledList);
    }
    echo json_encode([
        'result' => $result
    ]);
    exit;
}

// データセンターリストの最終更新日時を取得
$sql = sprintf("
    SELECT
        MAX(lastchecked)
    FROM
        azone
    WHERE
        cid = {$cid}
");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$lastUpdate = pg_fetch_result($r, 0, 0);
$forceReupdate = false;
if (empty($lastUpdate)) {
    $lastUpdate = "（未）";
    $forceReupdate = true;
} else {
    $updateInterval = time() - strtotime($lastUpdate);
    if ($updateInterval > AWS_UPDATE_INTERVAL) {
        $forceReupdate = true;
    }
}

// reupdate region list
$msg = "";
if ($forceReupdate || (int)getreq("op")) {
    $config = array(
        'credentials' => [
            'key'    => $_SESSION["key"],
            'secret' => $_SESSION["secret"],
        ],
        'region'  => $_SESSION["region"],
        'version' => 'latest'
    );
    $ret = updateAZ($cid, $smarty, $config);
}

$sortIndex = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
$sortMap = [
    "a.regionname","a.regionname DESC",
    "a.name","a.name DESC",
    "a.state","a.state DESC",
    "inst_count","inst_count DESC",
];
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}
// count total record
$sql = sprintf("SELECT count(*) FROM azone WHERE cid = {$cid}");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);
$currentPage = (int)getreq('p');
if ($currentPage <= 0) {
    $currentPage = 1;
}
$offset = ($currentPage -1) * DEFAULT_COUNTPAGE_EMP;
$paginationStr =  '';
if ($dataCount > 0) {
    $paginationStr = getPagenationStr(
        $smarty,
        $dataCount,
        DEFAULT_COUNTPAGE_EMP,
        $currentPage,
        "/admin/az.php?sk=$sortIndex&p="
    );
}

// データセンターリストの取得
$sql = sprintf("
    SELECT
        b.name,
        a.regionname,
        a.name,
        a.state,
        (
            SELECT
                COUNT(*)
            FROM
                instance
            WHERE
                cid = {$cid}
                AND availabilityzone = a.name
        ) AS inst_count
    FROM azone a
    LEFT JOIN azone_enable b
        ON b.cid = a.cid
        AND b.name = a.name
    WHERE
        a.cid = {$cid}
    ORDER BY
        $sortColumn
    LIMIT %d OFFSET %d
", DEFAULT_COUNTPAGE_EMP, $offset);
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$azList = "";
for ($i = 0; $i < pg_num_rows($r); $i++) {
    $row     = pg_fetch_row($r, $i);
    $checked = !empty($row[0]) ? "checked" : "";
    $azName  = sprintf('azname-%03d', $i);
    $row[0] = "
        <div class='form-check checkbox-simple' for='{$azName}'>
            <input
                type='checkbox'
                id='{$azName}'
                class='form-check-input'
                name='azone'
                data-zone-name='{$row[2]}'
                {$checked}>
            <label for='{$azName}'></label>
        </div>
    ";

    if ($row[3] == "available") {
        $row[3] = "正常";
    }
    $row[1] = local_regionName($row[1]);

    $azList .= "<tr><td>" . $row[0] . "</td><td>" . $row[1] . "</td><td>" . $row[2] . "</td>
                <td>" . $row[3] . "</td><td>" . $row[4] . "</td></tr>";
}

// 利用可能なデータセンターで、消えてしまったものを検出
$delazList = "";
$sql = sprintf("
    SELECT
        a.name
    FROM azone_enable a
    LEFT JOIN
        azone b
        ON a.cid = b.cid
        AND a.name = b.name
    WHERE
        a.cid = {$cid}
        AND b.name IS NULL
    ORDER BY
        a.name;
");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$result = pg_fetch_all($r);
if ($result) {
    $result     = array_column($result, 'name');
    $delazList  = implode(", ", $result);

    $delazListStr = implode(",", array_map(function ($name) {
        return "'{$name}'";
    }, $result));
    $sql = "
        DELETE FROM azone_enable
        WHERE
            cid = {$cid}
            AND name IN({$delazListStr});
    ";
    $logs[] = $sql;
    $result = pg_query($smarty->_db, $sql);
}
raise_sql($logs, 'az');

$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('azList', $azList);
$smarty->assign('delazList', $delazList);
$smarty->assign('lastUpdate', $lastUpdate);
$smarty->assign('viewTemplate', 'admin/az.tpl');
$smarty->assign('paginationStr', $paginationStr);

// 以下は定番
$smarty->assign('pageTitle', 'データセンター管理');
$smarty->assign('title', '');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
