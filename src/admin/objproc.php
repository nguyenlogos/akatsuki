<?php
use Akatsuki\Models\Obj;
use AwsServices\Ec2;

require_once(ROOT_PATH . "/if/updateSG.php");

$objid = (int)getreq('id');
$obj = new Obj();
$msg = "";
$errmsg = "";
$requestMethod = $_SERVER['REQUEST_METHOD'];
$cid = $_SESSION['cid'];
$config = array(
    'credentials' => [
        'key'    => $_SESSION["key"],
        'secret' => $_SESSION["secret"],
    ],
    'region'  => $_SESSION["region"],
    'version' => 'latest'
);

if ($requestMethod === 'POST') {
    $objname = htmlspecialchars(postreq("objname"));
    $objip   = htmlspecialchars(postreq("objip"));
    $objmask = (int)postreq("objmask");
    $objdesc = htmlspecialchars(postreq("objdesc"));
    $objip_old = htmlspecialchars(postreq("ipaddr_old"));
    $objmask_old = htmlspecialchars(postreq("submask_old"));
    $netmask = netmask($objmask);
    $network = network($objip, $objmask);

    if ($network != $objip) {
        $errmsg .= "Invalid network. See supported format  : ".$network.'/'.$objmask."<br />";
    }

    if (empty($objname)) {
        $errmsg .= "オブジェクト名は省略できません。<br />";
    }
    if (empty($objip)) {
        $errmsg .= MESSAGES['ERR_OBJ_IP_EMPTY'] . "<br />";
    } else {
        $isValid = filter_var($objip, FILTER_VALIDATE_IP);
        // $isValid = inet_pton($objip);
        if (!$isValid) {
            $errmsg .= MESSAGES['ERR_OBJ_IP_INVALID'] . "<br />";
        }
    }
    if ($objmask === '') {
        $errmsg .= MESSAGES['ERR_OBJ_SUBNET_EMPTY'] . "<br />";
    } else {
        $objmask = (int)$objmask;
        if ($objmask < 0 || $objmask > 32) {
            $errmsg .= MESSAGES['ERR_OBJ_SUBNET_INVALID'] . "<br />";
        }
    }

    $obj->setValues([
        'cid'         => $cid,
        'name'        => $objname,
        'ipaddr'      => $objip,
        'submask'     => $objmask,
        'description' => $objdesc,
    ]);
}
if ($objid > 0) {
    $tmp = Obj::where('id', $objid)
                ->where('status', 0)
                ->first();
    if (!$tmp) {
        throw new Exception(MESSAGES['ERR_DATA_NOT_FOUND'], 1);
    }
    $obj->id = $tmp->id;
    if ($requestMethod === 'POST' && !$errmsg) {
        $result = true;
        if ($tmp->ipaddr != $obj->ipaddr || $tmp->submask != $obj->submask) {
            $existingObj = Obj::where('cid', $obj->cid)
                ->where('ipaddr', $obj->ipaddr)
                ->where('submask', $obj->submask)
                ->where('status', 0)
                ->first();
            if ($existingObj && $existingObj->id !== $tmp->id) {
                $result = false;
                $errmsg = "すでに同じIPアドレスレンジとサブネットマスクが登録されています。";
            }
        }

        $ec2Client = Ec2::getInstance();
        $oldCIDR = $tmp->ipaddr . "/" . $tmp->submask;
        $newCIDR = $obj->ipaddr . "/" . $obj->submask;
        $result = $ec2Client->updateSecurityGroupCIDR($oldCIDR, $newCIDR);

        if ($result) {
            // UPDATE
            $tmp->setValues([
                'name'        => $obj->name,
                'ipaddr'      => $obj->ipaddr,
                'submask'     => $obj->submask,
                'description' => $obj->description,
            ]);
            $result = $tmp->save();
        }

        if ($result) {
            $msg = MESSAGES['INF_OBJ_UPDATE'];
            $obj = $tmp;
        } else {
            !$errmsg && $errmsg = MESSAGES['ERR_UPDATE'];
        }
    } elseif ($requestMethod === 'GET') {
        $obj = $tmp;
    }
    $smarty->assign('pageTitle', 'オブジェクトの変更');
} else {
    // CREATE
    if ($requestMethod === 'POST' && !$errmsg) {
        // check duplicate
        $sql = "
            SELECT count(*) FROM obj WHERE cid = $1 AND ipaddr = $2 AND submask = $3
        ";
        $tmpObj = Obj::where('cid', $cid)
            ->where('ipaddr', $objip)
            ->where('submask', $objmask)
            ->where('status', 0)
            ->first();
        if ($tmpObj) {
            $errmsg = "すでに同じIPアドレスレンジとサブネットマスクが登録されています。";
        } else {
            $result = $obj->save();
            if ($result) {
                $msg = MESSAGES['INF_OBJ_INSERT'];
                if ($tmpObj) {
                    $obj->id = $tmpObj->id;
                }
            } else {
                $errmsg = MESSAGES['ERR_INSERT'];
            }
        }
    }
    $smarty->assign('pageTitle', 'オブジェクトの新規登録');
}
if ($obj) {
    if (empty($obj->id)) {
        $obj->id = 0;
    }
    $obj = $obj->toArray();
    foreach (array_keys($obj) as $key) {
        $obj[$key] = $obj[$key];
    }
}

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);

$smarty->assign('obj', $obj);
$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'admin/objproc.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
