<?php
use Akatsuki\Models\CwRules;
use Akatsuki\Models\Configs;

$msg = "";
$errmsg = "";
$rulename = getreq("rule_name");
$timezone = Configs::getConfig('timezone');

// check permissions
$pageRoles = permission_check("admin/cw_rules.php", true);
$forDept = 0;

$ruleid = (int)getreq("id");
$requestMethod = $_SERVER['REQUEST_METHOD'];
if ($requestMethod === 'POST' && $ruleid > 0) {
    if (!$pageRoles['delete']['allowed'] || $pageRoles['delete']['condition'] == 1) {
        $forDept = $_SESSION['dept'];
    }
    $result = false;

    $rule = CwRules::find($ruleid);

    if (!$rule) {
        $errmsg = MESSAGES['ERR_DATA_NOT_FOUND'];
    } elseif ($rule->dept != $forDept) {
        rd("admin/cw_rules.php");
    }

    $result = $rule->deleteRule();

    if ($result) {
        $msg = MESSAGES['INF_DELETE'];
    } else {
        !$errmsg && $errmsg = MESSAGES['ERR_DELETE'];
    }
} else {
    if (!$pageRoles['read']['allowed'] || $pageRoles['read']['condition'] == 1) {
        $forDept = $_SESSION['dept'];
    }

    $regions = Akatsuki\Models\AzoneEnable::getAvailableRegion($_SESSION['cid']);
    foreach ($regions as $region) {
        $rules = CwRules::updateBackupRules($region);
    }
}

$formatCronExpr = function ($expr) {
    $expr = preg_replace('/cron\(([^)]+)\)/', '$1', $expr);
    $arr = explode(" ", $expr);
    if (count($arr) !== 6) {
        return false;
    }
    if ($arr[5] !== "*" || $arr[3] !== "*") {
        return false;
    }
    $str = "";
    if ($arr[4] !== '*' && $arr[4] !== '?') {
        $weeks = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thusday", "Friday", "Saturday"];
        $str .= "Weekly, on " . $weeks[(int)$arr[4]-1];
    } elseif ($arr[2] !== '*') {
        $str .= "Monthly, on day {$arr[2]} of the month";
    } elseif ($arr[1] !== '*') {
        $str .= "Daily";
    }
    $hour = (int)$arr[1];
    $minute = (int)$arr[0];
    if ($str) {
        $str .= sprintf(", at %02d:%02d", $hour, $minute);
    }
    return $str;
};

$sortMap = [
    "name ASC", "name DESC",
    "region ASC", "region DESC",
    "state ASC", "state DESC",
];

// アクティブなソートキーボタンを設定
// 併せてSQLのソートキーを設定
$sortIndex  = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}
list($sortColumn, $direction) = explode(" ", $sortColumn);

$itemList = CwRules::where("cid", $_SESSION['cid'])
        ->where("dept", $forDept)
        ->where("status", 0);
if ($rulename) {
    $namefm = strtoupper(pg_escape_string($rulename));
    $sql = "upper(name) LIKE '%{$namefm}%'";
    $itemList->whereRaw($sql);
}
$itemList = $itemList->orderBy($sortColumn, $direction)
        ->get()->toArray();
foreach ($itemList as &$item) {
    $item['expr'] = aws_rule_convert_timezone($item['expr'], $timezone, true);
    $exprText = $formatCronExpr($item['expr']);
    $item['expr_text'] = $exprText ?: '';
}
unset($item);
$paginationStr = '';

$headerList = [
    'name' => [
        'disp_name' => 'ルール名',
        'sortable'  => true,
        'data_attr' => true,
        'width'     => 200,
    ],
    'expr_text' => [
        'disp_name' => 'スケジュール',
        'sortable'  => false,
        'width'     => 200,
    ],
    'region' => [
        'disp_name' => 'エリア',
        'sortable'  => true,
        'data_attr' => true,
        'width'     => 150,
    ],
    'state' => [
        'disp_name' => 'ステータス',
        'sortable'  => true,
        'data_attr' => true,
        'width'     => 110,
    ],
    'notes' => [
        'disp_name' => '説明',
        'sortable'  => false
    ]
];
$btnTemplate = '<button type="button" class="btn $btnClass" onclick="$onClick">$btnName</button>';
$actionList = [
    [
        'template' => $btnTemplate,
        '$btnName' => '変更',
        '$btnClass'=> 'btn-blue btn-sm mr-3 mb-1',
        '$btnIcon' => 'fas fa-edit',
        '$onClick' => 'editRow(this);',
//        'width'    => 66,
    ],
    [
        'template' => $btnTemplate,
        '$btnName' => '削除',
        '$btnClass'=> 'btn-danger btn-sm mb-1',
        '$btnIcon' => 'fas fa-trash',
        '$onClick' => 'removeRow(this);',
//        'width'    => 66,
    ]
];

$smarty->assign('itemList', $itemList);
$smarty->assign('headerList', $headerList);
$smarty->assign('actionList', $actionList);
$smarty->assign('paginationStr', $paginationStr);
$smarty->assign('sortIndex', $sortIndex);
$smarty->assign('rulename', $rulename);

$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);
$smarty->assign('viewTemplate', 'admin/cw_rules.tpl');

// 以下は定番
$smarty->assign('pageTitle', 'バックアップ・ルール管理');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
