<?php
require_once('../common/CommonClass.class.php');
require_once('../common/define.php');
require_once('../common/auth.php');
require_once('../common/common.php');
// Include the SDK using the Composer autoloader
require_once '../aws/aws-autoloader.php';
require_once('../if/updateInst.php');
require_once('../if/updateSG.php');
require_once('../if/updateAMI.php');

$smarty = new MySmarty();

$amiid = postreq("ami");
$to = postreq("to");

if ($amiid == "" || $to == "" || ($to != 0 && $to != 1) || $_SESSION["cid"] == "") {
  echo -1;
  exit;
}

$sql = "delete from ami_enable where cid="  . $_SESSION["cid"] . " and imageid='" . $amiid . "'";
pg_query($smarty->_db, $sql);

if ($to) {
  $sql = "insert into ami_enable(cid, imageid) values(" . $_SESSION["cid"] . ",'" . $amiid . "')";
  pg_query($smarty->_db, $sql);
} // to falseの場合はレコードを作らない

echo 0;
exit;
?>
