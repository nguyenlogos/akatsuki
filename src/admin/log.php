<?php
use Akatsuki\Models\BaseModel;

if (getreq('l_category') === LOG_TYPES['CW']) {
    require_once("cw_log.php");
    exit();
}

$logTemplate = function ($model, $oldData, $newData) {
    $action      = "";
    $logTitle    = "";
    $dataChanges = [];
    $dataDisplay = [];

    $tableID = $model->id;
    $tableName = $model->getTable();
    if (!empty(TBL_NAMES[$tableName])) {
        $tableName = TBL_NAMES[$tableName];
    }
    $attributeLabels = $model->attributeLabels();

    if (!empty($oldData) && !empty($newData)) {
        $action = LOG_ACTIONS['UPDATE'];
        foreach (array_keys($oldData) as $key) {
            if ($oldData[$key] === $newData[$key]) {
                continue;
            }
            $dataChanges[$key] = sprintf("「%s」 → 「%s」", $oldData[$key], $newData[$key]);
        }
        $dataDisplay = $oldData;
    } elseif (!empty($oldData)) {
        $dataDisplay = $dataChanges = $oldData;
        $action = LOG_ACTIONS['DELETE'];
    } elseif (!empty($newData)) {
        $dataDisplay = $dataChanges = $newData;
        $action = LOG_ACTIONS['INSERT'];
    } else {
        $action = LOG_ACTIONS['DELETE'];
    }

    $logTitle = "";
    $dispKeys = [];
    foreach ($model->keys() as $key) {
        $keyName = $key === 'id' ? 'ＩＤ' : $attributeLabels[$key];
        $dispKeys[] = sprintf("%s：%s", $keyName, $dataDisplay[$key]);
    }
    $dispKeys = implode("、", $dispKeys);

    if ($action === LOG_ACTIONS['INSERT']) {
        $logTitle = sprintf("%s （%s） を追加しました。", $tableName, $dispKeys, $tableID);
    } elseif ($action === LOG_ACTIONS['UPDATE']) {
        $logTitle = sprintf("%s （%s） を変更しました。", $tableName, $dispKeys, $tableID);
    } elseif ($action === LOG_ACTIONS['DELETE']) {
        $logTitle = sprintf("%s （%s） を削除しました。", $tableName, $dispKeys, $tableID);
    }

    return [
        'title'   => $logTitle,
        'changes' => $dataChanges
    ];
};

$logMinimalize = function ($data, $attributeLabels) {
    if (empty($data)) {
        return [];
    }

    $output = [];
    foreach ($attributeLabels as $column => $label) {
        if (array_key_exists($column, $data)) {
            $output[$label] = $data[$column];
        }
    }

    unset($output['id']);
    return $output;
};

$frmValues = [
    'l_category' => pg_escape_string(getreq('l_category')),
    'l_dept' => (int)getreq('dept'),
    'l_from_date' => pg_escape_string(getreq('l_from_date')),
    'l_till_date' => pg_escape_string(getreq('l_till_date')),
];
$hideDept = false;
$deptRoles = permission_check("mst/dept.php");
if (!$deptRoles['read']['allowed'] || $deptRoles['read']['condition'] == 1) {
    $frmValues['l_dept'] = $_SESSION['dept'];
    $hideDept = true;
}
// determine sort column
$sortMap = [
    "date_created", "l.date_created DESC",
    "error_flag", "l.error_flag DESC",
    "category", "l.category DESC",
    "empname", "l.empname DESC",
];

$sortIndex = getreq("sk");
$sortColumn = "";
if ($sortIndex === "") {
    $sortIndex = 1;
}
$sortIndex = (int)$sortIndex;
if (!array_key_exists($sortIndex, $sortMap)) {
    $sortIndex = 1;
}
$sortColumn = $sortMap[$sortIndex];
$conditions = [
    "l.cid = {$_SESSION['cid']}"
];
$joinConditions = [
    "
    LEFT JOIN
        emp e
        ON
            e.cid = l.cid
        AND
            e.empid = l.empid
        AND
            e.status = 0
    "
];

if (!empty($frmValues['l_category']) && in_array($frmValues['l_category'], LOG_TYPES)) {
    $conditions[] = "l.category = '{$frmValues['l_category']}'";
} else {
    $frmValues['l_category'] = null;
}

if ($frmValues['l_dept'] > 0) {
    $joinConditions[] =
    "
    LEFT JOIN
        dept d
        ON
            d.cid = e.cid
        AND
            d.dept = e.dept
    ";
    $conditions[] = "d.dept = {$frmValues['l_dept']}";
    if ($hideDept) {
        $conditions[] = "e.empid = {$_SESSION['empid']}";
    }
} else {
    $frmValues['l_dept'] = null;
}

if ($logFromDate = strtotime($frmValues['l_from_date'])) {
    $conditions[] = sprintf("l.date_created::date >= '%s'", date('Y-m-d', $logFromDate));
} else {
    $frmValues['l_from_date'] = null;
}

if ($logTillDate = strtotime($frmValues['l_till_date'])) {
    $conditions[] = sprintf("l.date_created::date <= '%s'", date('Y-m-d', $logTillDate));
} else {
    $frmValues['l_till_date'] = null;
}

if (count($conditions)) {
    $conditions = "WHERE " . implode(' AND ', $conditions);
} else {
    $conditions = "";
}
$joinConditions = implode(" ", $joinConditions);

// count total record
$sql = "SELECT count(l.id) FROM tbl_logs l $joinConditions $conditions";
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);

$currentPage = getreq('p');
if ($currentPage <= 0) {
    $currentPage = 1;
}
$offset = ($currentPage -1) * ITEMS_PER_PAGE;
$paginationStr = '';
if ($dataCount > 0) {
    $params = $_GET;
    unset($params['r']);
    unset($params['p']);
    $params['p'] = '';
    $url = http_build_query($params);
    $paginationStr = getPagenationStr($smarty, $dataCount, ITEMS_PER_PAGE, $currentPage, "./log.php?{$url}");
}
// オブジェクトリストの取得
$sql = sprintf("
    SELECT
        l.id,
        l.cid,
        l.empid,
        e.name AS empname,
        l.category,
        l.action,
        l.error_flag,
        to_char(l.date_created,'YYYY/MM/DD HH24:MI:SS') AS date_created,
        l.description
    FROM tbl_logs l
    $joinConditions
    $conditions
    ORDER BY $sortColumn
    LIMIT %d OFFSET %d
", ITEMS_PER_PAGE, $offset);
$r = pg_query($smarty->_db, $sql);
$logList = pg_fetch_all($r) ?: [];

$generateLogDetails = function ($rowNumber, $data) {
    $btnShowHide = "
        <a href='' role='button'
            data-toggle='collapse'
            data-target='#rowInfo_{$rowNumber}'
            aria-expanded='false'
            aria-controls='rowInfo_{$rowNumber}'>詳細
        </a>
    ";
    if (is_array($data)) {
        $data = log_format($data);
    }
    $details = "
        <div id='rowInfo_{$rowNumber}' class='panel-collapse collapse'>
            <pre class='log-info'>{$data}</pre>
        </div>
    ";
    return $btnShowHide.$details;
};
foreach ($logList as $index => &$log) {
    $logType = $log['category'];
    $errFlag = (int)$log['error_flag'];
    $empname = $log['empname'];
    $desc    = json_decode($log['description'], true) ?: [];
    $tblName = !empty($desc['tbl_name']) ? $desc['tbl_name'] : '';
    $tblID   = !empty($desc['tbl_id']) ? (int)$desc['tbl_id'] : '';
    $action  = $log['action'];
    $msg = "";
    if ($logType === LOG_TYPES['DB'] ||
       ($logType === 'AWS' && ($tblName === 'inst_req' || $tblName === 'instance'))
    ) {
        $changes = !empty($desc['changes']) ? $desc['changes'] : null;
        $dataChanges = [];
        if (!$errFlag) {
            if (preg_match('/_enable$/', $tblName)) {
                $prefix = explode('_', $tblName)[0];
                $column = '';
                switch ($prefix) {
                    case 'ami':
                        $prefix = 'Ami';
                        $column = 'imageid';
                        break;
                    case 'azone':
                        $prefix = 'Azone';
                        $column = 'name';
                        break;
                    case 'insttype':
                        $prefix = 'Intance Type';
                        $column = 'name';
                        break;
                    default:
                        break;
                }
                $data = null;
                $state = null;
                if (!empty($changes['old'])) {
                    $state = 'disabled';
                    $data = $changes['old'];
                } else {
                    $state = 'enabled';
                    $data = $changes['new'];
                }
                $msg .= sprintf("%s <b>「%s」</b> has been <b>%s</b>.", $prefix, $data[$column], $state);
            } elseif ($action === LOG_ACTIONS['PWD_CHANGE']) {
                $msg .= "パスワードを変更しました。";
            } elseif ($action === LOG_ACTIONS['KEY_CHANGE']) {
                $msg .= "API認証情報を変更しました。";
            } elseif ($action === LOG_ACTIONS['LOGIN_FAILED']) {
                $dataChanges = !empty($desc['log_email']) ? $desc['log_email'] : [];
                $msg .= "ログイン失敗。(メールアドレス : ".$desc['log_email']['email'].")";
            } else {
                $model = BaseModel::getInstance($tblName);
                $model->id = $tblID;
                $attributeLabels = $model->attributeLabels();
                if ($action === LOG_ACTIONS['INST_REQ'] || $action === LOG_ACTIONS['INST_RUN']) {
                    $msgTemplate = "";
                    if ($action === LOG_ACTIONS['INST_REQ']) {
                        $dataChanges = !empty($desc['changes']['new']) ? $desc['changes']['new'] : [];
                        $blockStores = !empty($dataChanges['block_stores']) ? $dataChanges['block_stores'] : [];
                        $msg .= sprintf("<strong>「%s」</strong> has requested new instance(s).", $log['empname']);
                    } else {
                        $dataChanges = !empty($desc['inst_info']) ? $desc['inst_info'] : [];
                        $dataChanges['dept'] = $dataChanges['deptname'];
                        $blockStores = !empty($dataChanges['inst_bs']) ? $dataChanges['inst_bs'] : [];
                        $msg .= sprintf("新しいインスタンスが作成されました。");
                    }
                    if (count($blockStores)) {
                        $bsModel  = BaseModel::getInstance('inst_req_bs');
                        $bsLabels = $bsModel->attributeLabels();
                        foreach ($blockStores as &$bs) {
                            $bs = $logMinimalize($bs, $bsLabels);
                        }
                        unset($bs);
                    }
                    $dataChanges = $logMinimalize($dataChanges, $attributeLabels);
                    $dataChanges['ストレージの設定'] = $blockStores;
                } elseif ($action === LOG_ACTIONS['INST_REMOVE']) {
                    $instances = $desc['inst_info'];
                    $instIDs = [];
                    foreach ($instances as $instance) {
                        $instIDs[] = $instance['id'];
                        $dataChanges[] = $logMinimalize($instance, $attributeLabels);
                    }
                    $msg .= sprintf("インスタンス （インスタンスＩＤ：%s） が削除されました。", implode('、', $instIDs));
                } else {
                    $template = $logTemplate($model, $changes['old'], $changes['new']);
                    if (!empty($template)) {
                        $dataChanges = $template['changes'];
                        if ($tblName === 'users') {
                            $msg .= "契約窓口のプロファイルを変更しました。";
                        } elseif ($tblName === 'inst_req' && !empty($dataChanges['approved_status'])) {
                            $previouseStatus = $changes['old']['approved_status'];
                            $currentStatus   = $changes['new']['approved_status'];

                            $note = !empty($changes['new']['note']) ? $changes['new']['note'] : null;
                            switch ($currentStatus) {
                                case INST_REQ_DELETED:
                                    $msg .= sprintf("
                                        Instance request <strong>「id=%d」</strong> has been <b>deleted</b>.
                                    ", $tblID);
                                    break;
                                case INST_REQ_REJECTED:
                                    $msg .= sprintf("
                                        Instance request <strong>「id=%d」</strong> has been <b>rejected</b>.
                                    ", $tblID);
                                    break;
                                case INST_REQ_DELETE_PENDING:
                                    $msg .= sprintf("
                                        <strong>「%s」</strong> has a request to delete instance(s)
                                        <strong>「request_id=%d」</strong>.
                                    ", $log['empname'], $tblID);
                                    break;
                                default:
                                    break;
                            }
                            if (empty($msg)) {
                                $msg .= $template['title'];
                            } elseif (!empty($note)) {
                                $msg .= " ({$note})";
                            }
                            $dataChanges = [];
                        } else {
                            $msg .= $template['title'];
                        }
                        $dataChanges = $logMinimalize($dataChanges, $attributeLabels);
                    }
                }
            }
            if (count($dataChanges)) {
                $msg .= $generateLogDetails($index, $dataChanges);
            }
        }
    } elseif ($logType === LOG_TYPES['AWS']) {
        if (!$errFlag) {
            $instanceID    = !empty($desc['inst_id']) ? $desc['inst_id'] : '';
            $instanceInfo  = !empty($desc['inst_info']) ? $desc['inst_info'] : null;
            $instanceState = "";
            $msgTemplate   = "インスタンス （インスタンスＩＤ：%s） が%s。";
            switch ($action) {
                case LOG_ACTIONS['INST_START']:
                    $instanceState = '開始されました';
                    break;
                case LOG_ACTIONS['INST_STOP']:
                    $instanceState = '停止されました';
                    break;
                default:
                    break;
            }
            $msg .= sprintf($msgTemplate, $instanceID, $instanceState);
            if (!empty($instanceInfo)) {
                $msg .= $generateLogDetails($index, $instanceInfo);
            }
        }
    }
    if ($errFlag) {
        $log['category'] = 'エラー';
        $msg .= htmlspecialchars($desc['err_msg']);
        $traces = !empty($desc['traces']) ? $desc['traces'] : null;
        if ($traces) {
            $msg .= $generateLogDetails($index, $traces);
        }
    }
    $log['msg'] = $msg ?: htmlspecialchars($log['description']);
}
unset($log);

$headerList = [
    'id' => [
        'disp_name' => '',
        'sortable' => false,
    ],
    'date_created' => [
        'disp_name' => '発生日時',
        'sortable' => true,
        'width' => 145
    ],
    'category' => [
        'disp_name' => 'ログ種別',
        'sortable' => true,
        'width' => 100
    ],
    'empname' => [
        'disp_name' => '氏名',
        'sortable' => true,
        'width' => 150
    ],
    // 'action' => [
    //     'disp_name' => '作用',
    //     'sortable' => true
    // ],
    'msg' => [
        'disp_name' => '内容',
        'sortable' => false,
        'raw_input' => true
    ],
    'error_flag' => [
        'disp_name' => '',
        'sortable' => false,
        'class_name' => 'error-flag'
    ]
];
$smarty->assign('logList', $logList);
$smarty->assign('headerList', $headerList);

$smarty->assign('frmValues', $frmValues);
$smarty->assign('hideDept', $hideDept ? 'hide' : '');

$smarty->assign('sortIndex', $sortIndex);
$smarty->assign('msg', '');
$smarty->assign('errmsg', '');

$smarty->assign('paginationStr', $paginationStr);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);

// 以下は定番
$smarty->assign('title', '');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'admin/log.tpl');
$smarty->assign('pageIcon', 'glyphicon-eye-open');
$smarty->assign('pageTitle', '監査ログ');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
