<?php
use Akatsuki\Models\InsttypeEnable;
use Akatsuki\Models\InsttypePricing;

$updateCurgenSetting = function ($status) use ($smarty) {
    $cid = (int)$_SESSION["cid"];
    $sql = '
        UPDATE configs SET insttype_curgen = $1 WHERE cid = $2
    ';
    $logs[] = $sql;
    $result = pg_query_params($smarty->_db, $sql, [
        (int)$status,
        $cid
    ]);
    raise_sql($logs, 'insttype');

    return boolval($result);
};

$cid = "";
$cid = (int)$_SESSION["cid"];

if ($_SERVER['REQUEST_METHOD'] === 'POST' && getreq('ajax') == '1') {
    $result = true;
    $target = getreq('target');
    if ($target === 'curgen') {
        $usedCurgen = !!(int)postreq('curgen');
        $result = $updateCurgenSetting($usedCurgen);
    } else {
        $enabledList = postreq('enabled_list');
        $disabledList = postreq('disabled_list');
        if ($enabledList && count($enabledList) > 0) {
            $result = InsttypeEnable::enable($cid, $enabledList);
        }
        if ($result && $disabledList && count($disabledList) > 0) {
            $result = InsttypeEnable::disable($cid, $disabledList);
        }
    }
    echo json_encode([
        'result' => $result
    ]);
    exit;
}

$msg = "";

// determine sort column
$sortIndex  = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
$sortMap = [
    "a.curgen","a.curgen DESC",
    "a.family","a.family DESC",
    "a.name","a.name DESC",
    "a.cpu::numeric","a.cpu::numeric DESC",
    "a.memory::numeric","a.memory::numeric DESC",
    "a.storage","a.storage DESC",
    "a.ebsopt","a.ebsopt DESC",
    "a.netperf","a.netperf DESC",
    "p.price","p.price DESC",
];
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

// check conditions
$azoneErr = '';
$azone = pg_escape_string(getreq("azone")) ?: '';
$whereConditions = "";
// ログ件数の取得
$sql = "SELECT COUNT(a.*) FROM insttype a $whereConditions";
$logs[] = $sql;
$r   = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);

$sql = sprintf("
    SELECT
        b.name, -- use this to sort by checked status
        a.curgen,
        a.family,
        a.name,
        a.cpu,
        a.memory,
        a.storage,
        a.ebsopt,
        a.netperf,
        p.price,
        p.region
    FROM
        insttype a
        LEFT JOIN
            insttype_enable b
            ON b.name = a.name
            AND b.cid = {$cid}
        LEFT JOIN
            insttype_pricing p
            ON p.cid = {$cid}
            AND p.name = a.name
            AND p.region = '{$azone}'
            $whereConditions
    ORDER BY
            $sortColumn, p.price ASC
");
$logs[] = $sql;
$r      = pg_query($smarty->_db, $sql);
$itList = "";
$instTypeReupdate = (int)getreq("op");
$result = pg_fetch_all($r);
$list = array_column($result, 'name');
$nextToken = null;
$dataArr = [];
$getMaxByKey = function ($array, $key) {
    $max = $array[0][$key]->getTimestamp();
    $maxKey = 0;
    foreach ($array as $k => $value) {
        if ($value[$key]->getTimestamp() > $max) {
            $max = $value[$key]->getTimestamp();
            $maxKey = $k;
        }
    }
    return $array[$maxKey];
};
if (!$azone) {
    $azoneErr = "データがありません。";
} else {
    $client = \AwsServices\Ec2::getInstance($azone);
    do {
        $instTypePriceArr = $client->describeSpotPriceHistory($list, $azone, $nextToken);
        $resultSort = [];
        foreach ($instTypePriceArr as $value) {
            if (!is_array($resultSort[$value['InstanceType']])) {
                $resultSort[$value['InstanceType']] = array();
            }
            $resultSort[$value['InstanceType']][] = $value;
        }
        foreach ($resultSort as $info) {
            $maxKeyInst = $getMaxByKey($info, "Timestamp");
            $instTypePrice = $maxKeyInst['SpotPrice'];
            $dataArr[] = [
                'cid'    => $_SESSION['cid'],
                'name'   => $maxKeyInst['InstanceType'],
                'region' => $maxKeyInst['AvailabilityZone'],
                'price'  => $maxKeyInst['SpotPrice']
            ];
        }
    } while ($nextToken);

    if (count($dataArr)) {
        $result = InsttypePricing::beginTransaction(function () use ($dataArr, $azone) {
            InsttypePricing::where('cid', $_SESSION['cid'])
                ->where('region', $azone)->delete();
            return InsttypePricing::insertAll($dataArr, false);
        });
    }
    $r   = pg_query($smarty->_db, $sql);
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $row = pg_fetch_row($r, $i);
        if ($row[0] != "") {
            $row[0] = "利用可能";
        } else {
            $row[0] = "";
        }
        $row = local_instType($row);
        $checked = !empty($row[0]) ? "checked" : "";
        $curGen  = !empty($row[1]) ? "data-curgen='1'" : '';
        $instID  = sprintf('inst-%03d', $i);
        $row[9] = (($row[9] == null)||($row[9] == 0)) ? "利用不可 " : $row[9];
        $row[0] = "
            <div class='form-check checkbox-simple' for='{$instID}'>
                <input
                    type='checkbox'
                    id='{$instID}'
                    class='form-check-input'
                    name='ints'
                    data-name-id='{$row[3]}'
                    {$checked}
                    {$curGen}>
                <label for='{$instID}'></label>
            </div>";
        $itList .= "
            <tr>
                <td>{$row[0]}</td>
                <td>{$row[1]}</td>
                <td>{$row[2]}</td>
                <td>{$row[3]}</td>
                <td>{$row[4]}</td>
                <td>{$row[5]}</td>
                <td>{$row[6]}</td>
                <td>{$row[7]}</td>
                <td>{$row[8]}</td>
                <td>{$row[9]}</td>
            </tr>
        ";
    }
}
// 利用可能なデータセンターで、消えてしまったものを検出
$sql = "
    SELECT
        a.name,
        b.name
    FROM
        insttype_enable a
        LEFT JOIN
            insttype b
            ON a.name = b.name
    WHERE
        a.cid = {$cid}
        AND b.name IS NULL
    ORDER BY
        a.name
";
$logs[] = $sql;
$r         = pg_query($smarty->_db, $sql);
$delitList = "";
for ($i = 0; $i < pg_num_rows($r); $i++) {
    $row  = pg_fetch_row($r, $i);
    if ($delitList != "") {
        $delitList .= ",";
    }
    $delitList  .= $row[0];

    $sql = "delete from insttype_enable where cid = " . $cid . " and name = '" . $row[0] . "'";
    $logs[] = $sql;
    pg_query($smarty->_db, $sql);
}

$curgen = false;
$sql    = "
    SELECT insttype_curgen
    FROM configs
    WHERE cid = {$cid}
    LIMIT 1
";
$logs[] = $sql;
$result = pg_query($smarty->_db, $sql);
$result = pg_fetch_result($result, 0, 0);
if ($result === false) {
    $sql = "INSERT INTO configs(cid, insttype_curgen) VALUES({$cid}, 0)";
    $logs[] = $sql;
    $result = pg_query($smarty->_db, $sql);
} else {
    $curgen = !!(int)$result;
}
raise_sql($logs, 'insttype');


$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('itList', $itList);
$smarty->assign('delitList', $delitList);
$smarty->assign('viewTemplate', 'admin/insttype.tpl');
$smarty->assign('curgen', $curgen);
$smarty->assign('selectedAZ', $azone);
$smarty->assign('azoneErr', $azoneErr);
// 以下は定番
$smarty->assign('pageTitle', 'インスタンスタイプ管理');
$smarty->assign('title', '');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
