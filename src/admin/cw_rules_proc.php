<?php
use Akatsuki\Models\CwRules;
use Akatsuki\Models\Configs;

$timezone = Configs::getConfig('timezone');

$ruleid = (int)getreq('id');
$rule = new CwRules();

$msg = "";
$errmsg = "";
$inputReadonly = "";
// check permissions
$pageRoles = permission_check("admin/cw_rules_proc.php", true);
$forDept = 0;

$requestMethod = $_SERVER['REQUEST_METHOD'];
if ($requestMethod === 'POST') {
    $rule_name = postreq("rule_name");
    $rule_expr = postreq("rule_expr");
    $rule_region = postreq("rule_region");
    $rule_notes = postreq("rule_notes");

    // TODO: validate empty values

    $rule->setValues([
        'cid'    => $_SESSION['cid'],
        'name'   => $rule_name,
        'expr'   => $rule_expr,
        'notes'  => $rule_notes,
        'region' => $rule_region
    ]);
}

if ($ruleid > 0) {
    $inputReadonly = "readonly";

    $tmp = CwRules::where('id', $ruleid)->first();
    if (!$tmp) {
        rd("admin/cw_rules.php");
    }
    $rule->id = $tmp->id;
    if ($requestMethod === 'POST' && !$errmsg) {
        if (!$pageRoles['update']['allowed'] || $pageRoles['update']['condition'] == 1) {
            $forDept = $_SESSION['dept'];
        }
        if ($tmp->dept != $forDept) {
            rd("admin/cw_rules.php");
        }
        $tmp->setValues([
            'expr'   => $rule->expr,
            'notes'  => $rule->notes
        ]);
        $result = $tmp->putRule();

        if ($result) {
            $msg = MESSAGES['INF_UPDATE'];
            $rule = $tmp;
        } else {
            !$errmsg && $errmsg = MESSAGES['ERR_UPDATE'];
        }
    } elseif ($requestMethod === 'GET') {
        $rule = $tmp;
        $rule->expr = aws_rule_convert_timezone($rule->expr, $timezone, true);
        if (!$pageRoles['read']['allowed'] || $pageRoles['read']['condition'] == 1) {
            $forDept = $_SESSION['dept'];
        }
        if ($tmp->dept != $forDept) {
            rd("admin/cw_rules.php");
        }
    }

    $smarty->assign('pageTitle', 'バックアップ・ルールの変更');
} else {
    if ($requestMethod === 'POST') {
        if (!$pageRoles['create']['allowed']) {
            rd('src/403.php');
        }
        if (empty($rule->name)) {
            $errmsg .= "<br />ルール名は省略できません。";
        } elseif (preg_match('/[^\.\-_A-Za-z0-9]+/', $rule->name)) {
            $errmsg .= "Name should contains these one byte characters only: <br/>
                <p>dot (.), hyphen (-), underscore (_), characters (a-z, A-Z) and numbers (0-9)</p>";
        }
        if (empty($rule->region)) {
            $errmsg .= "<br />ルールのエリアは省略できません。";
        }

        if (!$errmsg) {
            $existingRule = CwRules::where('cid', $rule->cid)
                ->where('name', $rule->name)
                ->where('region', $rule->region)
                ->first();
            if ($existingRule) {
                $errmsg = "すでに同じルール名が登録されています。";
            }
        }

        if (!$errmsg) {
            if ($pageRoles['create']['condition'] == 1) {
                $forDept = $_SESSION['dept'];
            }
            $rule->state = 1;
            $rule->dept = $forDept;
            $result = $rule->putRule();
            if ($result) {
                $msg = MESSAGES['INF_INSERT'];
                $inputReadonly = "readonly";
            } else {
                !$errmsg && $errmsg = MESSAGES['ERR_INSERT'];
            }
        }
    }

    $smarty->assign('pageTitle', 'バックアップ・ルールの新規登録');
}
if ($rule) {
    if (empty($rule->id)) {
        $rule->id = 0;
    } else {
        $rule = $rule->toArray();
        foreach (array_keys($rule) as $key) {
            $rule[$key] = htmlspecialchars($rule[$key]);
        }
    }
}
if ($inputReadonly) {
    $smarty->assign('pageTitle', 'ルールの変更');
} else {
    $smarty->assign('pageTitle', 'ルールの新規登録');
}
$gmtTime = aws_rule_format_timezone($timezone);
$smarty->assign('rule', $rule);
$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);
$smarty->assign('inputReadonly', $inputReadonly);
$smarty->assign('timezone', $gmtTime);

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);

$smarty->assign('viewTemplate', 'admin/cw_rules_proc.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
