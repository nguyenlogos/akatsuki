<?php
require_once(ROOT_PATH . '/if/updateAMI.php');

use Akatsuki\Models\AmiEnable;

$msg = "";
$cid = "";
$cid = (int)$_SESSION["cid"];
$regionArr = [];

if ($_SERVER['REQUEST_METHOD'] === 'POST' && getreq('ajax') == '1') {
    $enabledList = postreq('enabled_list');
    $disabledList = postreq('disabled_list');
    $result = true;
    if ($enabledList && count($enabledList) > 0) {
        $result = AmiEnable::enable($_SESSION['cid'], $enabledList);
    }
    if ($result && $disabledList && count($disabledList) > 0) {
        $result = AmiEnable::disable($_SESSION['cid'], $disabledList);
    }
    echo json_encode([
        'result' => $result
    ]);
    exit;
}

$sql = sprintf("
    SELECT
        a.regionname, a.lastchecked
    FROM
        azone a
    INNER JOIN azone_enable ab
        ON ab.name = a.name
        AND ab.cid = a.cid
    WHERE
        a.cid = {$_SESSION['cid']}
    GROUP BY
        a.regionname, a.lastchecked
");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$regionList = pg_fetch_all($r) ?: [];

$lastUpdate    = "";
$forceReupdate = (int)getreq("op");
$config = array(
    'credentials' => [
        'key'    => $_SESSION["key"],
        'secret' => $_SESSION["secret"],
    ],
    'version' => 'latest'
);
foreach ($regionList as $region) {
    array_push($regionArr, $region['regionname']);
    $config['region'] = $region['regionname'];
    $ret = updateAMI($cid, $smarty, $config);
}

// determine sort column
$sortIndex = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
$sortMap = [
    "a.name","a.name DESC",
    "a.imageid","a.imageid DESC",
    "a.state","a.state DESC",
    "a.platform","a.platform DESC",
    "a.architecture","a.architecture DESC",
];
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

$conditions = [
    "a.cid = {$_SESSION['cid']}"
];
$azone = postreq('azone');
if (!empty($azone)) {
    $region = preg_replace('/(.+\d)[a-z]$/', '$1', $azone);
    $conditions[] = sprintf("region = '%s'", pg_escape_string($region));
} else {
    $conditions[] = sprintf("region IN ('%s')", implode("', '", $regionArr));
}
$conditions = "WHERE " . implode(' AND ', $conditions);

// count total record
$sql = sprintf("SELECT count(*) FROM ami a $conditions");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);
$currentPage = getreq('p');
if ($currentPage <= 0) {
    $currentPage = 1;
}
$offset = ($currentPage -1) * DEFAULT_COUNTPAGE_EMP;
$paginationStr = "";
if ($dataCount > 0) {
    $paginationStr = getPagenationStr(
        $smarty,
        $dataCount,
        DEFAULT_COUNTPAGE_EMP,
        $currentPage,
        "/admin/ami.php?sk=$sortIndex&p="
    );
}

// AMIリストの取得
$sql = sprintf("
    SELECT
        b.imageid,
        a.name,
        a.imageid,
        a.state,
        a.platform,
        a.architecture,
        a.region
    FROM
        ami a
        LEFT JOIN
            ami_enable b
            ON a.imageid = b.imageid
            AND a.cid = b.cid
    $conditions
    ORDER BY
        $sortColumn
    LIMIT %d OFFSET %d
", DEFAULT_COUNTPAGE_EMP, $offset);
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$result = pg_fetch_all($r);
$amiList = "";
for ($i = 0; $i < pg_num_rows($r); $i++) {
    $row     = pg_fetch_row($r, $i);
    $checked = !empty($row[0]) ? "checked" : "";
    $amiID   = sprintf('ami-%03d', $i);
    $row[0] = "
        <div class='form-check checkbox-simple' for='{$amiID}'>
            <input
                type='checkbox'
                id='{$amiID}'
                class='form-check-input'
                name='ami'
                data-image-id='{$row[2]}'
                {$checked}>
                <label for='{$amiID}'></label>
        </div>
    ";

    if ($row[3] == 'available') {
        $row[3] = "正常";
    } else {
        $row[3] = "異常";
    }
    $row[6] = local_regionName($row[6]);
//    $row[7] = str_replace("-", "/", $row[7]);
//    $row[7] = str_replace("T", " ", $row[7]);
//    $row[7] = str_replace(".000Z", "", $row[7]);

    $amiList .= "<tr><td>" . $row[0] . "</td><td>" .
        $row[1] . "</td><td>" . $row[2] . "</td><td>" .
        $row[3] . "</td><td>" .  $row[4] . "</td><td>" .
        $row[5] . "</td><td>" . $row[6] . "</td></tr>";
}

// 利用可能なAMIで、消えてしまったものを検出
$sql = sprintf("select a.imageid, b.imageid
                from ami_enable a
                left join ami b on a.cid=b.cid and a.imageid=b.imageid
                where a.cid = " . $cid . " and b.imageid is null order by a.imageid;");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$delamiList = "";
for ($i = 0; $i < pg_num_rows($r); $i++) {
    $row = pg_fetch_row($r, $i);
    if ($delamiList != "") {
        $delamiList .= ",";
    }
    $delamiList .= $row[0];

    $sql = "delete from ami_enable where cid = " . $cid . " and imageid = '" . $row[0] . "'";
    $logs[] = $sql;
    pg_query($smarty->_db, $sql);
}
raise_sql($logs, 'ami');

$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('amiList', $amiList);
$smarty->assign('delamiList', $delamiList);
$smarty->assign('lastUpdate', $lastUpdate);
$smarty->assign('paginationStr', $paginationStr);
$smarty->assign('selectedAZ', $azone);
$smarty->assign('viewTemplate', 'admin/ami.tpl');

// 以下は定番
$smarty->assign('pageTitle', 'AMI管理');
$smarty->assign('title', '');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
