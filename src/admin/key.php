<?php
require_once(ROOT_PATH . '/if/updateInst.php');
require_once(ROOT_PATH . '/if/updateAccountID.php');

use Akatsuki\Models\Users;

$encrypted = "";
$decrypted = "";
$msg = "";
$warning_msg = "";
$err = 0;
$newkey = htmlspecialchars(postreq("newkey"));
$newsecret = htmlspecialchars(postreq("newsecret"));

$button = postreq("button");
$mode = "";

if ($button === "認証テスト") {
    $mode = "TEST";
} else {
    if ($button === "保存") {
        $mode = "SAVE";
    }
}
if ($mode) {
    $encrypted = encryptIt($newkey);
    $decrypted = decryptIt($encrypted);

    $config = array(
        'credentials' => [
            'key'    => $newkey,
            'secret' => $newsecret
        ],
        'version'     => 'latest',
        'region'      => "ap-northeast-1"
    );
    $ret = updateInst($smarty, $config, $_SESSION["cid"], $mode);
    $apiTestResult = \AwsServices\Aws::validateApi($newkey, $newsecret, $_SESSION['region']);
    $ret = $apiTestResult ? 0 : -2;
    $ret2 = 0;
    if ($ret == 0 && $mode !== "TEST") {
        $ret2 = updateAccountID($smarty, $config, $_SESSION["cid"], "SAVE");
//        $rptClient = new \AwsServices\CostAndUsageReport($config);
//        if (count($rptClient->getRPT()) > 3) {
//            $warning_msg = "請求レポートの設定を追加できませんでした。<br />レポート設定は5つまでしか設定できません。<br />不要なレポートの設定を削除してください。";
//        } else {
//            $result = $rptClient->generateReportTemplates();
//        }
    }
    if ($ret < 0 || $ret2 < 0) {
        if ($ret == -1 || $ret2 == -1) {
            $msg = "認証に失敗しました。\n再度確認して入力してください。\n";
            $err = 1;
        } else {
            $msg = "システムエラーが発生した可能性があります。\nしばらく経ってから再度試してください。\n";
            $err = 1;
        }
    } else {
        if ($mode == "TEST") {
            $msg = "
                <p class='mb-0'>認証テストに成功しました。保存ボタンを押してください。</p>
                <p class='mb-0'>注意：まだ設定は保存されていません。</p>";
            $err = 1;
            $_SESSION['IS_NEW_AWS_KEY_OK'] = true;
        } else {
            // 成功 DBへ登録
            $user = Users::where('cid', $_SESSION['cid'])->first();
            if ($user) {
                $user->setValues([
                    'key'    => $encrypted,
                    'secret' => $newsecret
                ]);
                $r = $user->save();
            } else {
                $r = false;
            }
            if ($r) {
                $err = 0;
                $_SESSION["key"] = $newkey;
                $_SESSION["secret"] = $newsecret;
                $_SESSION['ERR_AWS_KEY'] = 0;
                $msg = "認証テストに成功しましたので、設定を保存しました。";
                $newkey = $newsecret = null;
            } else {
                $err = 1;
                $msg = MESSAGES['ERR_UPDATE'];
            }
        }
    }
}
if ($_SESSION["key"] != "" && $_SESSION["secret"] != "") {
    $smarty->assign('key', $_SESSION["key"]);
    $smarty->assign('secret', $_SESSION["secret"]);
} else {
    $smarty->assign('key', "未設定");
    $smarty->assign('secret', "未設定");
}
$smarty->assign('newkey', $newkey);
$smarty->assign('newsecret', $newsecret);
$smarty->assign('pageIcon', 'glyphicon-link');
$smarty->assign('pageTitle', 'AWS API認証情報の設定');
$smarty->assign('err', $err);
$smarty->assign('warning_msg', $warning_msg);
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('viewTemplate', 'admin/key.tpl');

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
