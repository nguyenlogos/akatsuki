<?php
use AwsServices\Ec2;
use Akatsuki\Models\Instance;

$msg = "";
$errmsg = "";
$frmValues = [
    'proj' => getreq("proj"),
    'inst' => getreq("inst"),
];

$ebsVolumes = [];

if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1) {
    $ebsVolumes = [];
} elseif ($frmValues['proj'] == '-1') {
    // load unmount volumes
    $ec2Client = Ec2::getInstance();
    $ebsVolumes = $ec2Client->getAvailableVolumes();
} elseif ($frmValues['proj']) {
    $instances = Instance::select(['id', 'availabilityzone AS area'])
        ->where('cid', $_SESSION['cid'])
        ->where('pcode', $frmValues['proj']);
    if ($frmValues['inst']) {
        $instances->where('id', $frmValues['inst']);
    }
    $instances = $instances->get()->toArray();
    if (count($instances)) {
        $instList = [];
        foreach ($instances as $instance) {
            $area = $instance['area'];
            if (empty($instList[$area])) {
                $instList[$area] = [];
            }
            $instList[$area][] = $instance['id'];
        };
        foreach ($instList as $area => $instances) {
            $ec2Client = Ec2::getInstance($area);
            $volumes = $ec2Client->describeVolumes([
                'Filters' => [
                    [
                        'Name'   => 'attachment.instance-id',
                        'Values' => $instances
                    ]
                ]
            ]);
            $ebsVolumes = array_merge($ebsVolumes, $volumes);
        }
    }
} else {
    $availableRegions = \Akatsuki\Models\AzoneEnable::getAvailableRegion();
    foreach ($availableRegions as $region) {
        $updateResult = \Akatsuki\Models\CwRules::updateBackupRules($region);
    }
}

$itemList = "";
foreach ($ebsVolumes as $volume) {
    $attachments = !empty($volume['Attachments']) ? $volume['Attachments'] : [];
    $tags = !empty($volume['Tags']) ? $volume['Tags'] : [];
    $tagsInfo = [];
    foreach ($tags as $tag) {
        $tagsInfo[$tag['Key']] = $tag['Value'];
    }
    $volumeName = !empty($tagsInfo['Name']) ? $tagsInfo['Name'] : "(未設定)";
    $volumeRule = !empty($tagsInfo['Rule']) ? $tagsInfo['Rule'] : "";
    $volumeGen = !empty($tagsInfo['Generation']) ? (int)$tagsInfo['Generation'] : 0;
    if ($volumeGen <= 0) {
        $volumeGen = 1;
    }

    $deviceName = "";
    if (count($attachments)) {
        $attachment = array_shift($attachments);
        $deviceName = $attachment['Device'];
    }
    $deviceLocation = $volume['AvailabilityZone'];

    $deptName = $item['deptname'];
    if (empty($deptName)) {
        $deptName = "(未設定)";
    }
    $dept = $item['dept'];
    $pcode = $item['pcode'];
    $itemList .= "
        <tr class='volume'>
            <td class='volume_id' data-volume-id='{$volume['VolumeId']}'>{$volume['VolumeId']}</td>
            <td>{$volumeName}</td>
            <td>{$deviceName}</td>
            <td class='region_id' data-region-id='{$deviceLocation}'>{$deviceLocation}</td>
            <td>{$volume['VolumeType']}</td>
            <td>{$volume['Size']}</td>
            <td class='volume_rule' data-dropdown='cw_rules' data-selected='{$volumeRule}'></td>
            <td>
                <input type='number' class='form-control w_75' min='1' name='volume_gen' value='{$volumeGen}' disabled>
            </td>
            <td>
                <button type='button' class='btn btn-save btn-blue'>保存</button>
            </td>
        </tr>";
}

$smarty->assign('pageIcon', 'glyphicon-inbox');
$smarty->assign('pageTitle', '自動バックアップ');

$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);

$smarty->assign('itemList', $itemList);
$smarty->assign('frmValues', $frmValues);
$smarty->assign('ec2region', $ec2Config['region']);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('viewTemplate', 'admin/backup.tpl');

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
