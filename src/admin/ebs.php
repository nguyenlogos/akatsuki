<?php
use AwsServices\Ec2;

$msg = "";
$err = "";
$frmValues = [
    'proj' => getreq("proj"),
    'inst' => getreq("inst"),
];

// $instances = \Akatsuki\Models\Instance::updateInstances();

$smarty->assign('frmValues', $frmValues);
$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);
$smarty->assign('viewTemplate', 'admin/ebs.tpl');

// 以下は定番
$smarty->assign('pageTitle', 'ディスク追加・変更の申請');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
