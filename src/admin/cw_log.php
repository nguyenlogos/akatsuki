<?php
use Akatsuki\Models\BaseModel;

$generateLogDetails = function ($rowNumber, $data) {
    $btnShowHide = "
        <a role='button'
            data-toggle='collapse'
            data-target='#rowInfo_{$rowNumber}'
            aria-expanded='false'
            aria-controls='rowInfo_{$rowNumber}'>Info
        </a>
    ";
    $details = "
        <div id='rowInfo_{$rowNumber}' class='panel-collapse collapse'>
            <pre class='log-info'>{$data}</pre>
        </div>
    ";
    return $btnShowHide.$details;
};

$frmValues = [
    'l_category'  => pg_escape_string(getreq('l_category')),
    'l_dept'      => (int)getreq('l_dept'),
    'l_from_date' => pg_escape_string(getreq('l_from_date')),
    'l_till_date' => pg_escape_string(getreq('l_till_date')),
];
$sortMap = [
    "event_time", "l.event_time DESC",
    "empname", "l.empname DESC",
];

$sortIndex = getreq("sk");
$sortColumn = "";
if ($sortIndex === "") {
    $sortIndex = 1;
}
$sortIndex = (int)$sortIndex;
if (!array_key_exists($sortIndex, $sortMap)) {
    $sortIndex = 1;
}
$sortColumn = $sortMap[$sortIndex];
$conditions = [
    "l.cid = {$_SESSION['cid']}"
];

if ($logFromDate = strtotime($frmValues['l_from_date'])) {
    $conditions[] = sprintf("l.event_time::date >= '%s'", date('Y-m-d', $logFromDate));
} else {
    $frmValues['l_from_date'] = null;
}

if ($logTillDate = strtotime($frmValues['l_till_date'])) {
    $conditions[] = sprintf("l.event_time::date <= '%s'", date('Y-m-d', $logTillDate));
} else {
    $frmValues['l_till_date'] = null;
}

if (count($conditions)) {
    $conditions = "WHERE " . implode(' AND ', $conditions);
} else {
    $conditions = "";
}

// count total record
$sql = "SELECT count(l.id) FROM cw_logs l $conditions";
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);

$currentPage = getreq('p');
if ($currentPage <= 0) {
    $currentPage = 1;
}
$offset = ($currentPage -1) * ITEMS_PER_PAGE;
$paginationStr = '';
if ($dataCount > 0) {
    $params = $_GET;
    unset($params['r']);
    unset($params['p']);
    $params['p'] = '';
    $url = http_build_query($params);
    $paginationStr = getPagenationStr($smarty, $dataCount, ITEMS_PER_PAGE, $currentPage, "./log.php?{$url}");
}
// オブジェクトリストの取得
$sql = sprintf("
    SELECT
        l.id,
        l.cid,
        l.event,
        to_char(l.event_time,'YYYY/MM/DD HH24:MI:SS') AS event_time,
        l.error_flag,
        l.description
    FROM cw_logs l
    $conditions
    ORDER BY $sortColumn
    LIMIT %d OFFSET %d
", ITEMS_PER_PAGE, $offset);
$r = pg_query($smarty->_db, $sql);
$logList = pg_fetch_all($r);
if (!$logList) {
    $logList = [];
}

foreach ($logList as $index => &$log) {
    $msg = "";
    if ((int)$log['error_flag'] === 1) {
        $log['category'] = 'エラー';
        $description = json_decode($log['description'], true);
        if (!$description) {
            continue;
        }
        $errorMessage = !empty($description['Traces']) ? $description['Traces'] : "";
        if (!$errorMessage) {
            continue;
        }
        $msg = $log['event'] . $generateLogDetails($index, $errorMessage);
        $log['event'] = $msg;
    } else {
        $log['category'] = LOG_TYPES['CW'];
    }
}
unset($log);

$headerList = [
    'id' => [
        'disp_name' => '',
        'sortable'  => false,
    ],
    'event_time'    => [
        'disp_name' => '日時',
        'sortable'  => true,
        'width'     => 145
    ],
    'category' => [
        'disp_name' => 'タイプ',
        'sortable'  => false,
        'width'     => 75
    ],
    'event' => [
        'disp_name' => 'アクション',
        'sortable'  => false,
        'raw_input' => true
    ]
];
$smarty->assign('logList', $logList);
$smarty->assign('headerList', $headerList);

$smarty->assign('frmValues', $frmValues);

$smarty->assign('sortIndex', $sortIndex);
$smarty->assign('msg', '');
$smarty->assign('errmsg', '');

$smarty->assign('paginationStr', $paginationStr);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);

// 以下は定番
$smarty->assign('title', '');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'admin/log.tpl');
$smarty->assign('pageIcon', 'glyphicon-eye-open');
$smarty->assign('pageTitle', '監査ログ');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
