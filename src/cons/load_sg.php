<?php
use Akatsuki\Models\SecurityGroup;

function CreateSG($smarty, $cid, $IdSGOld, $NameSG, $DescriptionSG, $VpcSG)
{
    $SecurityGroup = new SecurityGroup();
    if ($IdSGOld) {
        $sql = "SELECT * FROM sg WHERE cid = $1 AND id = $2";
        $data = pg_query_params($smarty->_db, $sql, [
            $cid,
            $IdSGOld
        ]);
        $ListSG = pg_fetch_array($data);
        $SecurityGroup->setValues([
            'cid'            => $_SESSION['cid'],
            'name_sg'        => $ListSG['name'],
            'description_sg' => $ListSG['description'],
            'vpc_sg'         => $VpcSG,
            'create_time'    => date("Y-m-d h:i:sa"),
            'sg_id'          => $IdSGOld,
        ]);
    } else {
        $SecurityGroup->setValues([
            'cid'            => $_SESSION['cid'],
            'name_sg'        => $NameSG,
            'description_sg' => $DescriptionSG,
            'vpc_sg'         => $VpcSG,
            'create_time'    => date("Y-m-d h:i:sa"),
            'sg_id'          => '',
        ]);
    }
    $SecurityGroup->save();

    return $SecurityGroup->id;
}

function UpdateSGLocal(
    $smarty,
    $cid,
    $SGId,
    $StatusTypeSG,
    $SecurityGroupId,
    $NameSG,
    $DescriptionSG,
    $VpcSG,
    $instReqId
) {
    $SecurityGroup = SecurityGroup::where("id", $SGId)
                                  ->where("cid", $cid)
                                  ->first();
    if (!$SecurityGroup) {
        $SecurityGroup = new SecurityGroup();
    }
    $sql = "SELECT * FROM sg WHERE cid = $1 AND id = $2";
    $data = pg_query_params($smarty->_db, $sql, [
        $cid,
        $SecurityGroupId
    ]);
    $list_sg = pg_fetch_array($data);
    $Name_SG = $StatusTypeSG == 'status_list' ? $list_sg['name'] : $NameSG;
    $Description_SG = $StatusTypeSG == 'status_list' ? $list_sg['description'] : $DescriptionSG;
    if ($Name_SG) {
        $SecurityGroup->setValues([
            'cid'            => $_SESSION['cid'],
            'name_sg'        => $Name_SG,
            'description_sg' => $Description_SG,
            'vpc_sg'         => $VpcSG,
            'create_time'    => date("Y-m-d h:i:sa"),
            'inst_req_id'    => (int)$instReqId,
            'sg_id'          => $SecurityGroupId,
        ]);
    }
    $result = $SecurityGroup->save();

    return $result;
}
