<?php
use Akatsuki\Models\InstReqBs;

require_once(COMMON_PATH . "/auth.php");
require_once(ROOT_PATH .'/if/updateAMI.php');

require_once("inst_req_loader.php");

$instReqId = (int)getreq("id");
//check session value
$dataarr = sesreq('arr_stepone') ?: null;
if (is_null($dataarr) || (!empty($dataarr['id']) && $dataarr['id'] != $instReqId)) {
    header("Location:/cons/req_inst.php");
    exit();
}
$step_two =  [];
$errMsg = "";
$err = "";
$smarty->assign('addNew', false);
$addDiskNum = $diskNum = 1;
$listDev = [];
$amiSize = getSizeAmi($_SESSION['arr_stepone']['ami']);
$showLastStepButton = false;
if ($instReqId > 0 || !empty($_SESSION['listDev'])) {
    $blockStores = sesreq('listDev') ?: [];
    if (count($blockStores) && $blockStores[0]['reqid'] == $instReqId) {
        // load from session data
        $step_two = $blockStores;
    } elseif ($instReqId > 0) {
        $instReqBs = InstReqBs::where("inst_req_id", $instReqId)->get();
        if ($instReqBs) {
            $instReqBs = $instReqBs->toArray();
            foreach ($instReqBs as $i => $bs) {
                $step_two[] = [
                    "reqid"     => $instReqId,
                    "no"        => $i,
                    "type"      => "EBS",
                    "device"    => $bs["device_name"],
                    "snapid"    => $bs["snapshot_id"],
                    "size"      => $bs["volumn_size"],
                    "orgsize"   => "",
                    "devtype"   => $bs["volumn_type"],
                    "iops"      => $bs["iops"],
                    "autodel"   => (int)$bs["auto_removal"],
                    "enc"       => (int)$bs["encrypted"],
                    "stat"      => 0
                ];
            }
        } else {
            $step_two = [];
        }
    }
    $_SESSION['listDev'] = $step_two;
}
$listDev = $step_two;
//---------------------------------------------
// ボタンが押された場合はlistDevをPOST値で上書き
//---------------------------------------------
if (postreq("add_disk")) {
    $listDev = [];
    $diskNum = postreq("diskNum");
    if (postreq("numberKeyDisks") != '') {
        $numberKeyDisks = explode(',', postreq("numberKeyDisks"));
        $numberKeyDisks[] = max($numberKeyDisks) + 1;
    } else {
        $numberKeyDisks[] = 0;
    }
    $addDiskNum = (int)postreq("addDiskNum") + 1;
    //$addDiskNum = !$_SESSION['listDev'] ? ($addDiskNum + 1) : ($addDiskNum - 1);
    //$i = empty($_SESSION['listDev']) ? 1 : 0;
    $no = 0;
    // 追加ﾃﾞｨｽｸ数の取得
    foreach ($numberKeyDisks as $i) {
        $item = array(
            "reqid"     => $instReqId,
            "no"        => $no,
            "type"      => "EBS",
            "device"    => postreq("devname" . $i),
            "snapid"    => "",
            "size"      => postreq("stsize" . $i),
            "orgsize"   => "",
            "devtype"   => postreq("devtype" . $i),
            "iops"      => postreq("iops" . $i),
            "autodel"   => (int)postreq("autodel" . $i),
            "enc"       => (int)postreq("enc" . $i),
            "stat"      => 0
        );
        $no++;
        $listDev[] = $item;
    }
    if (!$instReqId || count($step_two) === 0) {
        $listDev[$no - 1]['autodel'] = 1;
        $listDev[$no - 1]['enc'] = 1;
    }
} elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && count($listDev) === 0) {
    $listDev[] = [
        "reqid"     => $instReqId,
        "no"        => $i,
        "type"      => "EBS",
        "device"    => "",
        "snapid"    => "",
        "size"      => "",
        "orgsize"   => "",
        "devtype"   => "",
        "iops"      => "",
        "autodel"   => 1,
        "enc"       => 1,
        "stat"      => 0
    ];
    // ---------------------------------------
    // ボタンが押if ($list)なかった場合のみ、APIでAMI情報を取得する
    // -----------if ($list)-------------------------
    // AMIの詳細のif ($list)得
    // $amiDetail if ($list)etAMIdetail($vals["ami"]);
    // $listDev = array();
    // $diskNum = 0;
    // foreach ($amiDetail['BlockDeviceMappings'] as $devs) {
    //     if ($amiDetail["RootDeviceName"] == $devs["DeviceName"]) {
    //         $devs["VirtualName"] = "Root";
    //     }
    //     $listDev[] = array("no" => $diskNum,
    //         "type"      => $devs["VirtualName"],
    //         "device"    => $devs["DeviceName"],
    //         "snapid"    => $devs["Ebs"]["SnapshotId"],
    //         "size"      => $devs["Ebs"]["VolumeSize"],
    //         "orgsize"   => $devs["Ebs"]["VolumeSize"],
    //         "devtype"   => $devs["Ebs"]["VolumeType"],
    //         "iops"      => $devs["Ebs"]["Iops"],
    //         "autodel"   => $devs["Ebs"]["DeleteOnTermination"],
    //         "enc"       => $devs["Ebs"]["Encrypted"],
    //         "stat"      => 1);
    //     $listDev[count($listDev) - 1] = local_BlockDeviceMappings($listDev[count($listDev) - 1]);
    //     $diskNum++;
    // }
    // $_SESSION["listDev"] = $listDev;
}

//----------------------------------------
// 次の画面へボタンが押された場合 (最終確認)
//----------------------------------------
if (postreq("confim") || postreq("step4")) {
    $diskNum = postreq("diskNum");
    $addDiskNum = postreq("addDiskNum");
    $numberKeyDisks = explode(',', postreq('numberKeyDisks'));
    $listDev = [];
    $listType = [];
    // 追加ﾃﾞｨｽｸ数の取得
    // $addDiskNum = !$_SESSION['listDev'] ? ($addDiskNum + 1) : $addDiskNum;
    // $i = !$_SESSION['listDev'] ? 1 : 0;
    // $no = 0;
    $no = 0;

    foreach ($numberKeyDisks as $i) {
        $devName = postreq("devname" . $i);
        if (in_array($devName, $listType)) {
            $err = 'errDevName';
        }
        array_push($listType, $devName);

        if (postreq("stsize" . $i) != '') {
            $listDev[] = array(
                "reqid"     => $instReqId,
                "no"        => $no,
                "type"      => "EBS",
                "device"    => $devName,
                "snapid"    => "",
                "size"      => postreq("stsize" . $i),
                "orgsize"   => "",
                "devtype"   => postreq("devtype" . $i),
                "iops"      => postreq("iops" . $i),
                "autodel"   => postreq("autodel" . $i),
                "enc"       => postreq("enc" . $i),
                "stat"      => 0
            );
        }
        $no++;
    }

    if (!$err && count($listDev) > 0) {
        $_SESSION["listDev"] = $listDev;
        if ($instReqId) {
            if (postreq("step4")) {
                header("Location:/cons/req_inst3.php?id=".$instReqId."");
            } else {
                header("Location:/cons/security_group.php?id=".$instReqId."");
            }
        } else {
            if (postreq("step4")) {
                header("Location:/cons/req_inst3.php");
            } else {
                header("Location:/cons/security_group.php");
            }
        }
    } elseif (count($listDev) == 0) {
        $err = 'emptyListDev';
    }
}

if (!empty($_SESSION['inst_req'])) {
    $instReq = $_SESSION['inst_req'];
    if (!empty($instReq['sg']['sg_id']) || !empty($instReq['sg']['sg_name'])) {
        $showLastStepButton = true;
    }
} elseif (!empty($_SESSION['listDev'])) {
    $showLastStepButton = true;
}

//---------------------------------------
// ﾃﾞｨｽｸの追加ボタンが押された場合
//---------------------------------------
//if ((int)getreq("bk") == "2") {
//    if (postreq("add_disk")) {
//        // 新しいディスク情報入力域の表示
//        $smarty->assign('addNew', true);
//    }
//}

$smarty->assign('pageTitle', 'インスタンスの申請');
$smarty->assign('addDiskNum', $addDiskNum);
$smarty->assign('diskNum', $diskNum);
$smarty->assign('diskTotalNum', count($listDev));
$smarty->assign('listsseion', $step_two);
$smarty->assign('listDev', $listDev);
$smarty->assign('errMsg', $errMsg);
$smarty->assign('err', $err);
$smarty->assign('errSize', $errSize);
$smarty->assign('showLastStepButton', (int)$showLastStepButton);
$smarty->assign('link', $_SERVER['REQUEST_URI']);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('viewTemplate', 'cons/req_inst2.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('instReq', $instReqId);
$smarty->assign('amiSize', $amiSize);
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
