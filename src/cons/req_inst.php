<?php
use Akatsuki\Models\InstReq;
use Akatsuki\Models\SecurityGroup;

require_once(ROOT_PATH .'/if/updateAMI.php');
\Akatsuki\Models\Sg::updateSecurityGroups();

require_once "inst_req_loader.php";

$vals = '';
$htmls = '';
$showgroupErr = '';
$showgprojectErr = '';
$vpcErr = "";
$addressErr = "";
$subnetErr = "";
$azoneErr = '';
$amiErr = '';
$insttypeErr = '';
$usageErr = '';
$datetoErr = '';
$datefromErr = '';
$shutdownbeErr = '';
$instnumErr = '';
$total_dateErr = '';
$get_id_ins = '';
$check_dateErr = '';

// get session data step 1
$arr = [];
$errMsg = "";
$showgroup = $showgproject = $azone = $ami = $insttype = $usage = $dateto = $datefrom = $shutdownbe = $instnum = "";
$selectedVPC = "";
$selectedSubnet = "";
$selectedAddress = postreq("address");
$get_id_ins = (int)getreq('id');
$formActionUrl = "./req_inst.php";
$approvedStatus = false;
$instReq = null;
$deptRoles = permission_check("mst/dept.php");
$hideDept = false;
$dateto = postreq("dateto");
$datefrom = postreq("datefrom");
$showLastStepButton = false;

if (!$deptRoles['read']['allowed'] || $deptRoles['read']['condition'] == 1) {
    $showgroup = $_SESSION['dept'];
    $hideDept = true;
}
if ($get_id_ins > 0 || !empty($_SESSION['arr_stepone'])) {
    $sessionData = !empty($_SESSION['arr_stepone']) ? $_SESSION['arr_stepone'] : [];
    $sessionDataId = !empty($sessionData['id']) ? (int)$sessionData['id'] : 0;
    if ($get_id_ins > 0) {
        $instReq = \Akatsuki\Models\InstReq::where("id", $get_id_ins)
            ->where("empid", $_SESSION['empid'])
            ->first();
        if ($instReq) {
            if ($instReq->empid != $_SESSION['empid']) {
                rd("cons/req_inst.php");
            }
            $instReq = $instReq->toArray();
            $approvedStatus = $instReq['approved_status'] == INST_REQ_APPROVED;
            if ($approvedStatus) {
                $instReqChange = \Akatsuki\Models\InstReqChange::where("cid", $_SESSION['cid'])
                    ->where("inst_req_id", $instReq["id"])->first();
                if ($instReqChange) {
                    $editableFields = [
                        'dept',
                        'pcode',
                        'using_purpose',
                        'using_from_date',
                        'using_till_date',
                        'inst_address',
                    ];
                    foreach ($editableFields as $field) {
                        $instReq[$field] = $instReqChange->$field;
                    };
                }
            }

            $blockStores = sesreq('listDev') ?: [];
            if (count($blockStores) && $blockStores[0]['reqid'] == $get_id_ins) {
                // load from session data
                $step_two = $blockStores;
            } elseif ($get_id_ins > 0) {
                $instReqBs = \Akatsuki\Models\InstReqBs::where("inst_req_id", $get_id_ins)->get();
                if ($instReqBs) {
                    $instReqBs = $instReqBs->toArray();
                    foreach ($instReqBs as $i => $bs) {
                        $step_two[] = [
                            "reqid"     => $get_id_ins,
                            "no"        => $i,
                            "type"      => "EBS",
                            "device"    => $bs["device_name"],
                            "snapid"    => $bs["snapshot_id"],
                            "size"      => $bs["volumn_size"],
                            "orgsize"   => "",
                            "devtype"   => $bs["volumn_type"],
                            "iops"      => $bs["iops"],
                            "autodel"   => (int)$bs["auto_removal"],
                            "enc"       => (int)$bs["encrypted"],
                            "stat"      => 0
                        ];
                    }
                } else {
                    $step_two = [];
                }
            }
            $_SESSION['listDev'] = $step_two;
        } else {
            rd("cons/req_inst.php");
        }
        if (count($sessionData) && $sessionDataId == $get_id_ins) {
            $arr = $sessionData;
        } else {
            if ($instReq) {
                $arr = [
                    'id'            => $instReq['id'],
                    'showgroup'     => $instReq['dept'],
                    'showgproject'  => $instReq['pcode'],
                    'azone'         => $instReq['inst_region'],
                    'ami'           => $instReq['inst_ami'],
                    'vpc'           => $instReq['inst_vpc'],
                    'subnet'        => $instReq['inst_subnet'],
                    'insttype'      => $instReq['inst_type'],
                    'usage'         => $instReq['using_purpose'],
                    'dateto'        => date("Y/m/d", strtotime($instReq['using_till_date'])),
                    'datefrom'      => date("Y/m/d", strtotime($instReq['using_from_date'])),
                    'shutdownbe'    => $instReq['shutdown_behavior'],
                    'instnum'       => $instReq['inst_count'],
                    'address'       => $instReq['inst_address'],
                ];
            } else {
                $arr = [];
            }
        }
        if (count($arr)) {
            $formActionUrl .= "?id={$get_id_ins}";
        }
    } elseif ($sessionDataId > 0) {
        $arr = [];
    } else {
        $arr = $sessionData;
    }

    $_SESSION['arr_stepone'] = $arr;
    if (count($_SESSION['arr_stepone']) === 0) {
        $_SESSION['name_key_pair'] = '';
    }
}

// POSTで使う変数名の一覧
if (postreq("next_step") || postreq("step4")) {
    $curAmiSite = getSizeAmi($instReq['inst_ami']);
    $nextAmiSize = getSizeAmi(postreq("ami"));
    if (empty(postreq("dept"))) {
        $showgroupErr = "グループ：が入力されていません。";
    } else {
        $showgroup = postreq("dept");
        $arr['showgroup'] = $showgroup;
        $groupManagers = getGroupManagers($smarty, $showgroup);
        if (!$groupManagers) {
            $showgroupErr = MESSAGES['ERR_INST_REQ_INVALID_DEPT'];
        }
    }

    if (empty(postreq("proj"))) {
        $showgprojectErr = "プロジェクト";
    } else {
        $showgproject = postreq("proj");
    }

    if (empty(postreq("vpc"))) {
        $vpcErr = "VPC";
    } else {
        $selectedVPC = postreq("vpc");
    }

    if (empty(postreq("subnet"))) {
        $subnetErr = "サブネット";
    } else {
        $selectedSubnet = postreq("subnet");
    }

    if (empty(postreq("azone"))) {
        $azoneErr = "データセンター";
    } else {
        $azone = postreq("azone");
    }

    if (empty(postreq("ami"))) {
        $amiErr = "AMI";
    } else {
        $ami = postreq("ami");
    }

    if (empty(postreq("insttype"))) {
        $insttypeErr = "インスタンスタイプ";
    } else {
        $insttype = postreq("insttype");
    }

    if (empty(postreq("usage"))) {
        $usageErr = "用途が入力されていません。";
    } else {
        if (mb_strlen(postreq("usage")) > 512) {
            $usageErr = "170文字以内で入力してください。";
        }
        $usage = htmlspecialchars(postreq("usage"));
    }
    if (!validate_date($dateto)) {
            $datetoErr = "有効な日付(YYYY/MM/DD)を入力してください。";
    }

    if (!validate_date($datefrom)) {
            $datefromErr = "有効な日付(YYYY/MM/DD)を入力してください。";
    }

    if ((validate_date($dateto))&&(validate_date($datefrom))) {
        if (strtotime($datefrom) > strtotime($dateto)) {
            $check_dateErr = "利用終了日は利用開始日よりも大きいです。";
        }
    }

    if (empty(postreq("shutdownbe"))) {
        $shutdownbeErr = "シャットダウン時の後処理";
    } else {
        $shutdownbe = postreq("shutdownbe");
    }

    if (empty(postreq("instnum"))) {
        $instnumErr = "インスタンス個数";
    } else {
        $instnum = postreq("instnum");
    };

    if ($selectedAddress == 'new') {
        $client = \AwsServices\Ec2::getInstance();
        $elasticIp = $client->allocateAddress();
        $selectedAddress = $elasticIp['PublicIp'];
    }

    $arr = [
        'id'            => $get_id_ins,
        'showgroup'     => $showgroup,
        'showgproject'  => $showgproject,
        'azone'         => $azone,
        'ami'           => $ami,
        'vpc'           => $selectedVPC,
        'subnet'        => $selectedSubnet,
        'address'       => $selectedAddress,
        'insttype'      => $insttype,
        'usage'         => $usage,
        'dateto'        => $dateto,
        'datefrom'      => $datefrom,
        'shutdownbe'    => $shutdownbe,
        'instnum'       => $instnum
    ];

    if ($total_dateErr == ""
        && $arr['showgroup'] != ""
        && $arr['showgproject'] != ""
        && $arr['azone'] != ""
        && $arr['ami']
        && $arr['vpc']
        && $arr['subnet']
        && $arr['insttype']
        && $arr['usage']
        && $arr['dateto']
        && $arr['datefrom']
        && $arr['instnum']
        && $check_dateErr ==""
        && $datetoErr == ""
        && $datefromErr == "") {
        $_SESSION['arr_stepone'] = $arr;

        if ($instReq) {
            $status = (int)$instReq['approved_status'];
            if (in_array($status, INST_REQ_EDITABLE_STATES)) {
                if (postreq("step4") && $nextAmiSize <= $curAmiSite) {
                    header("Location:./req_inst3.php?id={$instReq['id']}");
                } else {
                    header("Location:./req_inst2.php?id={$instReq['id']}");
                }
            } else {
                header("Location:./req_inst3.php?id={$instReq['id']}");
            }
        } else {
            if (postreq("step4") && $nextAmiSize <= $curAmiSite) {
                header("Location:./req_inst3.php");
            } else {
                header("Location:./req_inst2.php");
            }
        }
    }
}
// check session value
$gr = $showgroup ? $showgroup : $arr['showgroup'];
$proj = $showgproject ? $showgproject : $arr['showgproject'];
$az = $azone ? $azone : $arr['azone'];
$am = $ami ? $ami : $arr['ami'];
$vpc = $selectedVPC ?: $arr['vpc'];
$subnet = $selectedSubnet ?: $arr['subnet'];
$ints = $insttype ? $insttype : $arr['insttype'];
$usag = $usage ? $usage : $arr['usage'];
if ($dateto ==='') {
    $dateto = $arr['dateto'];
}
if ($datefrom ==='') {
    $datefrom = $arr['datefrom'];
}
$downbe = $shutdownbe ? $shutdownbe : $arr['shutdownbe'];
$num = $instnum ? $instnum : $arr['instnum'];
$address = $selectedAddress ? $selectedAddress : $arr['address'];
//search filter group and project
if ($gr) {
    $cid = $_SESSION['cid'];
    $query = sprintf("
        SELECT pcode,pname
        FROM proj
        WHERE
            status = 0
            AND dept = $1
            AND cid = $2
    ");
    $logs[] = $query;
    $results = pg_query_params($smarty->_db, $query, [
        $gr,
        $cid
    ]);
    $htmls  = '';
    for ($i = 0; $i < pg_num_rows($results); $i++) {
        $dept = pg_fetch_result($results, $i, 0);
        $deptname = pg_fetch_result($results, $i, 1);
        $htmls .= "<option value='" . $dept . "'";
        if ($proj == $dept) {
            $htmls .= " selected";
        }
        $htmls .= ">" . $deptname . "\n";
    }
}

if (empty($_SESSION['arr_stepone'])) {
    $sql = "DELETE FROM security_group WHERE inst_req_id IS NULL OR inst_req_id = 0;";
    SecurityGroup::selectRaw($sql);
    unset($_SESSION['inst_req']);
    unset($_SESSION['listDev']);
}
if (!empty($_SESSION['inst_req'])) {
    $instReq = $_SESSION['inst_req'];
    if (!empty($instReq['sg']['sg_id']) || !empty($instReq['sg']['sg_name'])) {
        $showLastStepButton = true;
    }
} elseif (!empty($_SESSION['listDev'])) {
    $showLastStepButton = true;
}
raise_sql($logs, 'reg_inst');
$smarty->assign('pageTitle', 'インスタンス作成の申請');
$smarty->assign('vals', $vals);
$smarty->assign('htmls', $htmls);
$smarty->assign('showgroup', $gr);
$smarty->assign('hideDept', $hideDept ? 'hide' : '');
$smarty->assign('showgproject', $proj);
$smarty->assign('azone', $az);
$smarty->assign('ami', $am);
$smarty->assign('selectedVPC', $vpc);
$smarty->assign('selectedSubnet', $subnet);
$smarty->assign('selectedAddress', $address);
$smarty->assign('insttype', $ints);
$smarty->assign('usage', $usag);
$smarty->assign('dateto', $dateto);
$smarty->assign('datefrom', $datefrom);
$smarty->assign('check_dateErr', $check_dateErr);
$smarty->assign('shutdownbe', $downbe);
$smarty->assign('instnum', $num);
$smarty->assign('get_id_ins', $get_id_ins);
$smarty->assign('formActionUrl', $formActionUrl);
$smarty->assign('approvedStatus', (int)$approvedStatus);
$smarty->assign('showLastStepButton', (int)$showLastStepButton);
$smarty->assign('showgroupErr', $showgroupErr);
$smarty->assign('showgprojectErr', $showgprojectErr);
$smarty->assign('azoneErr', $azoneErr);
$smarty->assign('amiErr', $amiErr);
$smarty->assign('amiErrSize', $amiErrSize);
$smarty->assign('vpcErr', $vpcErr);
$smarty->assign('subnetErr', $subnetErr);
$smarty->assign('insttypeErr', $insttypeErr);
$smarty->assign('usageErr', $usageErr);
$smarty->assign('datetoErr', $datetoErr);
$smarty->assign('datefromErr', $datefromErr);
$smarty->assign('shutdownbeErr', $shutdownbeErr);
$smarty->assign('instnumErr', $instnumErr);
$smarty->assign('total_dateErr', $total_dateErr);
$smarty->assign('errMsg', $errMsg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('viewTemplate', 'cons/req_inst.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('instReq', $instReq);
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
