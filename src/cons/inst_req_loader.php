<?php
use Akatsuki\Models\Sg;
use Akatsuki\Models\InstReq;
use Akatsuki\Models\SgTemp;
use Akatsuki\Models\SgTempRules;

$method = $_REQUEST['REQUEST_METHOD'];

$init = function (Array $instReqInfo = array()) {
    return array_merge([
        'id' => 0,
        'vpc'=> '',
        'sg' => [
            'sg_id'       => '',
            'sg_name'     => '',
            'sg_desc'     => '',
            'sg_name_new' => '',
            'sg_desc_new' => '',
        ],
        'status' => INST_REQ_PENDING
    ], $instReqInfo);
};
$loadSg = function ($sgid) {
    return Sg::where('cid', $_SESSION['cid'])
        ->where('id', $sgid)->first();
};

$loadSgFromDB = function (Array &$instReqInfo, InstReq $instReq) use ($loadSg) {
    $info = $instReq['sg'];
    $sgTemp = \Akatsuki\Models\SgTemp::where('cid', $_SESSION['cid'])
        ->where('inst_req_id', $instReq->id)->first();
    if ($sgTemp) {
        if ($sgTemp->sg_id) {
            $sg = $loadSg($sgTemp->sg_id);
            if ($sg) {
                $info['sg_id'] = $sg->id;
                $info['sg_name'] = $sg->name;
                $info['sg_desc'] = $sg->description;
            } else {
                $sgTemp->sg_id = null;
                $sgTemp->saveWithoutEvents(function () use (&$sgTemp) {
                    return $sgTemp->save();
                });
            }
        } else {
            $sgRules = SgTempRules::where('cid', $_SESSION['cid'])
                ->where('sg_temp_id', $sgTemp->id)
                ->get()->toArray();
            $info['sg_name'] = $info['sg_name_new'] = $sgTemp->sg_name;
            $info['sg_desc'] = $info['sg_desc_new'] = $sgTemp->sg_desc;
            $info['sg_rules'] = $sgRules;
        }
        $info['sg_vpc'] = $sgTemp->sg_vpc;
        $instReqInfo['sg'] = $info;
    }
};

$loadSgFromSS = function (&$instReqInfo, $append = false) use ($loadSg) {
    $vpc = postreq('vpc');
    $sgInfo = $instReqInfo['sg'];
    if ($vpc) {
        if ($vpc !== $instReqInfo['vpc']) {
            $sgInfo['sg_vpc'] = $instReqInfo['vpc'] = $vpc;
            $sgInfo['sg_id'] = $sgInfo['sg_name'] = $sgInfo['sg_desc'] =
            $sgInfo['sg_name_new'] = $sgInfo['sg_desc_new'] = '';
            $sgInfo['sg_rules'] = [];
        }
    }
    if ($sgInfo['sg_id']) {
        if (empty($sgInfo['sg_name']) || empty($sgInfo['sg_desc'])) {
            $sg = $loadSg($sgInfo['sg_id']);
            if ($sg) {
                $sgInfo['sg_name'] = $sg->name;
                $sgInfo['sg_desc'] = $sg->description;
            } else {
                dd('not found');
            }
        }
    }
    $instReqInfo['sg'] = $sgInfo;
};

$reqid = (int)getreq('id');
if ($reqid < 0) {
    $reqid = 0;
}
$instReqInfo = sesreq('inst_req') ?: [];
if (!empty($instReqInfo) && $instReqInfo['id'] != $reqid) {
    $instReqInfo = $init();
} else {
    $instReqInfo = $init($instReqInfo);
}

if ($reqid > 0) {
    $instReq = InstReq::where('cid', $_SESSION['cid'])
        ->where('id', $reqid)->first();
    if (!$instReq) {
        unset($_SESSION['inst_req']);
        rd('cons/req_inst.php', true);
    }
    if ($instReqInfo['id'] == 0 || ($instReqInfo['id'] > 0 && $instReqInfo['id'] != $instReq->id)) {
        $instReqInfo = $init([
            'id'    => $instReq->id,
            'vpc'   => $instReq->inst_vpc,
            'status'=> $instReq->approved_status,
        ]);

        $loadSgFromDB($instReqInfo, $instReq);
    } else {
        $instReqInfo['id'] = $instReq->id;
        $loadSgFromSS($instReqInfo);
    }
} elseif ($instReqInfo['id'] && $instReqInfo['id'] != $reqid) {
    $loadSgFromSS($instReqInfo, true);
} else {
    $loadSgFromSS($instReqInfo);
}
$_SESSION['inst_req'] = $instReqInfo;
