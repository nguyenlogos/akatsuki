<?php
use Akatsuki\Models\Sg;
use Akatsuki\Models\SgTemp;
use Akatsuki\Models\SgTempRules;

require_once("inst_req_loader.php");

$msg = "";
$errmsg = "";
$instReq = sesreq('inst_req') ?: [];
if (count($instReq) === 0) {
    rd('cons/req_inst.php');
}
$frmValues = !empty($instReq['sg']) ? $instReq['sg'] : [];
$requestMethod = $_SERVER['REQUEST_METHOD'];

$findRuleByProtocol = function ($name, $protocol) {
    $match  = array_filter(AWS_SG_STYPES, function ($stype) use ($name, $protocol) {
        $match = false;
        if (!empty($stype['alias'])) {
            $match = $stype['alias'] == $protocol;
        } else {
            $match = $stype['protocol'] == $protocol;
        }
        return $match && $stype['name'] === $name;
    });
    if ($match) {
        return array_shift($match);
    }

    return null;
};

$checkDuplicateRule = function ($rule, $list) {
    $fields = ["proto","type","pfrom","pto","target"];
    $matches = array_filter($list, function ($item) use ($rule, $fields) {
        $match = true;
        foreach ($fields as $field) {
            if ($rule[$field] !== $item[$field]) {
                $match = false;
                break;
            }
        }
        return $match;
    });

    return count($matches) > 0;
};

$validate = function ($sgRules) use ($findRuleByProtocol, $checkDuplicateRule) {
    $fields = ["stype","proto","range","source","desc","type"];
    $batchData = [];
    foreach ($sgRules as $sgRule) {
        foreach ($fields as $field) {
            if (empty($sgRule[$field])) {
                $sgRule[$field] = "";
            }
            $data[$field] = $sgRule[$field];
        }
        $data["cid"] = $_SESSION["cid"];
        $data["sg_temp_id"] = $sgTemp->id;
        switch ($data["source"]) {
            case SG_IP_SOURCES["MYIP"]:
                $data["target"] = get_ip_address() . "/32";
                break;
            case SG_IP_SOURCES["ANYWHERE"]:
                $data["target"] = "0.0.0.0/32";
                break;
            default:
                $target = explode("/", $sgRule["target"], 2);
                if (count($target) === 1) {
                    $target = $target . "/32";
                } else {
                    $target = $sgRule["target"];
                }
                $data["target"] = $target;
                break;
        }
        $fromPort = $toPort = "";
        $stype = $findRuleByProtocol($data["stype"], $data["proto"]);
        if ($stype) {
            $fromPort = $stype["portfrom"];
            $toPort = $stype["portto"];
            $data['proto'] = $stype['protocol'];
        }
        if ($fromPort === "" || $toPort === "") {
            @list($fromPort, $toPort) = explode("-", $data['range'], 2);
            if (empty($toPort)) {
                $toPort = $fromPort;
            }
        }
        $data['pfrom'] = (int)$fromPort;
        $data['pto'] = (int)$toPort;

        $isDuplicate = $checkDuplicateRule($data, $batchData);
        if ($isDuplicate) {
            return false;
        } else {
            $batchData[] = $data;
        }
    };
    return $batchData;
};

if ($requestMethod === 'POST') {
    $frmValues['sg_id'] = postreq('sg_item_id') ?: '';
    if (!$frmValues['sg_id']) {
        $frmValues['sg_name'] = $frmValues['sg_name_new'] = postreq('sg_name_new');
        $frmValues['sg_desc'] = $frmValues['sg_desc_new'] = postreq('sg_desc_new');
        $list = [];
        foreach ($_POST as $key => $value) {
            $chunks = explode('_', $key);
            if (count($chunks) !== 4) {
                continue;
            }
            list(, $field, $type, $index) = $chunks;
            empty($list[$index]) && $list[$index] = [];
            $list[$index][$type][$field] = $value;
        }
        $sgRules = [];
        foreach (array_values($list) as $item) {
            $type = array_keys($item)[0];
            $rule = $item[$type];
            $rule['type'] = $type;
            $sgRules[] = $rule;
        }
        $frmValues['sg_rules'] = $sgRules;
    } else {
        // reset `sg_name` and `sg_desc` (will be loaded in inst_req_loader.php)
        $frmValues['sg_name'] = $frmValues['sg_desc'] = '';
    }
    $result = $validate($frmValues['sg_rules']);
    if ($result === false) {
        $errmsg = MESSAGES['ERR_AWS_DUPLICATE_SG_RULES'];
    } else {
        $frmValues['sg_rules_validated'] = $result;
        $_SESSION['inst_req']['sg'] = $frmValues;
        rd('cons/req_inst3.php?id='.$instReq['id']);
    }
}

$smarty->assign('pageTitle', 'セキュリティグループの作成');
$smarty->assign('err', $err);
$smarty->assign('errmsg', $errmsg);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);

$smarty->assign('frmValues', $frmValues);
$smarty->assign('CLIENTIP', get_ip_address());
$smarty->assign('sgRules', json_encode($frmValues['sg_rules']));
$smarty->assign('sgSTypes', json_encode(AWS_SG_STYPES));

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'cons/security_group.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
