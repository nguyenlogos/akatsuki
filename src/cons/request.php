<?php
use Akatsuki\Models\InstReq;
use Akatsuki\Models\InstReqChange;
use Common\Mailer;

$msg = "";
$errmsg = "";
$requestMethod = $_SERVER['REQUEST_METHOD'];

$frmValues = [
    'f_type' => getreq('f_type'),
    'f_approved' => getreq('f_approved') == 'on' ? 1 : 0,
];
if ($frmValues['f_type'] === 'ebs') {
    require_once('ebs_request.php');
    exit();
}
$emailConfig = \Akatsuki\Models\Configs::getConfig('email_instance');
$updateInstanceRequestStatus = function ($reqID, $status, $note = '') {
    $instReq = InstReq::find($reqID);
    if (!$instReq) {
        return false;
    }
    if ($instReq->approved_status === INST_REQ_APPROVE_PROCESSING) {
        return true;
    }
    $result = $instReq->changeStatus($status, $note, false);
    if ($result) {
        return $instReq;
    }
    return false;
};

if ($requestMethod === 'POST') {
    $requestParams = parseAjaxRequestParams();
    $action = !empty($requestParams['action']) ? $requestParams['action'] : '';
    $reqID = !empty($requestParams['id']) ? (int)$requestParams['id'] : 0;
    $note = !empty($requestParams['req_note']) ? $requestParams['req_note'] : '';

    $mailTo = $mailSubject = "";
    $requestInfoUrl = $reqID > 0 ? $_SERVER['HTTP_ORIGIN'] . "/cons/request.php?id={$reqID}" : "";
    $mailBody = $requestInfoUrl ? "Check your request info at: {$requestInfoUrl}" : "";
    $result = null;
    switch ($action) {
        case 'inst_approve':
            if ($reqID > 0) {
                $req = InstReq::find($reqID);
                if (!$req) {
                    $errmsg = MESSAGES['ERR_DATA_NOT_FOUND'];
                } else {
                    $status = $req->approved_status == INST_REQ_APPROVED ?
                        INST_REQ_APPROVED : INST_REQ_APPROVE_PROCESSING;
                    $result = $updateInstanceRequestStatus($reqID, $status);
                    if ($result) {
                        $msg = MESSAGES['INF_INST_REQ_PROCESSING'];
                    } else {
                        $errmsg = MESSAGES['ERR_INST_REQ_APPROVED'];
                    }
                }
            }
            break;
        case 'inst_reject':
            if ($reqID > 0 && $note) {
                $req = InstReq::find($reqID);
                if (!$req) {
                    $errmsg = MESSAGES['ERR_DATA_NOT_FOUND'];
                } else {
                    if ($req->approved_status == INST_REQ_APPROVED) {
                        InstReqChange::where("inst_req_id", $reqID)->delete();
                        $result = $updateInstanceRequestStatus($reqID, INST_REQ_APPROVED, $note);
                        updateWfTotalCount($smarty);
                    } else {
                        $result = $updateInstanceRequestStatus($reqID, INST_REQ_REJECTED, $note);
                    }
                    if ($result) {
                        $msg = MESSAGES['INF_INST_REQ_REJECTED'];
                        $mailSubject = "[Sunny View] Your instance request has been rejected.";
                    } else {
                        $errmsg = MESSAGES['ERR_INST_REQ_REJECTED'];
                    }
                }
            }
            break;
        case 'inst_delete':
        case 'inst_approve_delete':
            if ($reqID > 0) {
                $result = $updateInstanceRequestStatus($reqID, INST_REQ_DELETED);
                if ($result) {
                    $msg = MESSAGES['INF_INST_REQ_DELETED'];
                } else {
                    $errmsg = MESSAGES['ERR_INST_REQ_DELETED'];
                }
            }
            break;
        case 'inst_delete_request':
            if ($reqID > 0 && $note) {
                $result = $updateInstanceRequestStatus($reqID, INST_REQ_DELETE_PENDING, $note);
                if ($result) {
                    $msg = MESSAGES['INF_INST_REQ_DEL_REQUEST'];
                } else {
                    $errmsg = MESSAGES['ERR_INST_REQ_DEL_REQUEST'];
                }
            }
            break;
        case 'inst_reject_delete':
            $reqID = !empty($requestParams['id']) ? (int)$requestParams['id'] : 0;
            if ($reqID > 0 && $note) {
                $result = $updateInstanceRequestStatus($reqID, INST_REQ_APPROVED, $note);
                if ($result) {
                    $msg = MESSAGES['INF_INST_REQ_REJECTED'];
                } else {
                    $errmsg = MESSAGES['ERR_INST_REQ_REJECTED'];
                }
            }
            break;
        default:
            break;
    }
    if ($emailConfig && $mailSubject) {
        $emp = $result->empids()->first();
        if ($emp) {
            $mailTo = $emp->email;
            $mailer = new Mailer();
            $mailer
                ->set('subject', $mailSubject)
                ->set('to', $mailTo)
                ->set('body', $mailBody);
            $mailer->setBcc($smarty->_db, $mailTo);

            $mailer->send();
        }
    }
}
$reqID = (int)getreq('id');
if ($reqID > 0 && getreq('ajax') == '1') {
    $instance = getInstanceRequestInfo($smarty, $reqID);
    sendAjaxResponse($instance);
}

// determine sort column
$sortIndex = getreq("sk");
$sortColumn = "";
if ($sortIndex === '') {
    $sortIndex = 0;
} else {
    $sortIndex = (int)$sortIndex;
    if ($sortIndex < 0) {
        $sortIndex = 0;
    }
}
$sortMap = [
    "i.id DESC","i.id",
    "i.req_date DESC","i.req_date",
    "i.using_from_date DESC","i.using_from_date",
    "d.deptname DESC","d.deptname",
    "e.name DESC","e.name",
    "i.approved_status DESC","i.approved_status",
];
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

// general conditions
$deptRoles = permission_check("mst/dept.php");

$conditions = [
    "i.approved_status != " . INST_REQ_DELETED,
    "i.cid = {$_SESSION["cid"]}"
];

if ($_SESSION['sysadmin'] == '1' || $_SESSION['admin'] == '1') {
    $conditions[] = "i.approved_status != " . INST_REQ_REJECTED;
}
if ($_SESSION['sysadmin'] != '1') {
    $assignedProjects = \Akatsuki\Models\EmpProj::getListAssigned();
    $assignedProjects = array_map(function ($pcode) {
        return "'{$pcode}'";
    }, array_column($assignedProjects, 'pcode'));
    $assignedProjects = implode(',', $assignedProjects);
    !$assignedProjects  && $assignedProjects = "''";

    if ($_SESSION['admin']) {
        $condition = "(d.dept = {$_SESSION['dept']} OR i.pcode IN ($assignedProjects))";
    } else {
        $condition = "(i.pcode IN ($assignedProjects))";
        $conditions[] = "i.empid = " . $_SESSION['empid'];
        $conditions[] = "i.approved_status != " . INST_REQ_DELETE_PENDING;
    }
    $conditions[] = $condition;
}
if ($frmValues['f_approved'] == 1) {
    $conditions[] = "irc.id IS NULL";
    $conditions[] = "i.approved_status = " . INST_REQ_APPROVED;
} else {
    $conditions[] = "(irc.id IS NOT NULL OR i.approved_status != " . INST_REQ_APPROVED . ")";
}
$conditions = "WHERE " . implode(' AND ', $conditions);

// count total record
$sql = sprintf("SELECT count(*) FROM inst_req i $conditions");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);
$currentPage = getreq('p');
if ($currentPage <= 0) {
    $currentPage = 1;
}
$offset = ($currentPage -1) * ITEMS_PER_PAGE;
$paginationStr = '';
if ($dataCount > 0) {
    $paginationStr = getPagenationStr(
        $smarty,
        $dataCount,
        ITEMS_PER_PAGE,
        $currentPage,
        "./request.php?sk=$sortIndex&p="
    );
}
$sql = sprintf(
    "
    WITH gm AS (
        SELECT
            dept.dept
        FROM
            dept
        LEFT JOIN
            emp
            ON emp.dept = dept.dept
            AND emp.cid = dept.cid
            AND emp.status = 0
        WHERE
            dept.cid = {$_SESSION['cid']}
            AND dept.status = 0
            AND emp.dept IS NOT NULL
            AND emp.admin = 1
        GROUP BY
            dept.dept
    )
    SELECT
        i.id,
        i.cid,
        i.empid,
        e.name AS empname,
        d.deptname,
        to_char(i.req_date,'YYYY/MM/DD HH24:MI:SS') AS req_date,
        to_char(i.using_from_date,'YYYY/MM/DD') AS from_date,
        i.approved_status AS req_state,
        i.inst_type,
        i.inst_count,
        i.key_name,
        (
            CASE
                WHEN i.approved_status = ".INST_REQ_DELETE_PENDING." THEN '削除待ち'
                WHEN i.approved_status = ".INST_REQ_DELETED." THEN '削除済み'
                WHEN i.approved_status = ".INST_REQ_REJECTED." THEN '却下'
                WHEN i.approved_status = ".INST_REQ_PENDING." THEN '承認待ち'
                WHEN i.approved_status = ".INST_REQ_APPROVE_PROCESSING ." THEN 'starting'
                WHEN i.approved_status = ".INST_REQ_APPROVE_FAILED." THEN 'failed'
                WHEN irc.id IS NOT NULL THEN '承認待ち'
                WHEN i.approved_status = ".INST_REQ_APPROVED." THEN '承認済'
            END
        ) AS status_name,
        gm.dept AS group_manager,
        i.using_purpose,
        irc.id AS change_state,
        irc.empid as empid_new,
        irc.dept as dept_new,
        irc.pcode as pcode_new,
        irc.using_purpose as using_purpose_new,
        to_char(irc.using_from_date,'YYYY/MM/DD') AS from_date_new,
        irc.using_till_date as using_till_date_new,
        irc.inst_address as inst_address_new
    FROM
        inst_req i
    LEFT JOIN
        inst_req_change irc
        ON irc.cid = i.cid
        AND irc.inst_req_id = i.id
    LEFT JOIN
        emp e
        ON e.cid = i.cid
            AND e.empid = i.empid
            AND e.status = 0
    LEFT JOIN
        dept d
        ON d.cid = i.cid
            AND d.dept = i.dept
            AND d.status = 0
    LEFT JOIN
        gm
        ON gm.dept = i.dept
    $conditions
    ORDER BY
        $sortColumn
    LIMIT %d OFFSET %d
",
    ITEMS_PER_PAGE,
    $offset
);
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$reqList = pg_fetch_all($r);
if (!$reqList) {
    $reqList = [];
} else {
    $editableFields = [
        'empid',
        'dept',
        'pcode',
        'using_purpose',
        'from_date',
        'inst_address',
    ];
    foreach ($reqList as &$req) {
        $req['key_name_exists'] = 0;
        if ($req['key_name']) {
            $req['key_name_exists'] = (int)(boolean)load_keypem($req['key_name']);
        }
        if (!$req['change_state']) {
            continue;
        }
        foreach ($editableFields as $field) {
            $req[$field] = $req[$field.'_new'];
        };
    }
    unset($req);
}
$headerList = [
    'id' => [
        'disp_name' => '申請ID',
        'sortable'  => true
    ],
    'req_date' => [
        'disp_name' => '申請日時',
        'sortable'  => true
    ],
    'from_date' => [
        'disp_name' => '利用開始日',
        'sortable'  => true
    ],
    'deptname' => [
        'disp_name' => '所属ｸﾞﾙｰﾌﾟ',
        'sortable'  => true
    ],
    'empid' => [
        'disp_name' => '',
        'class_name'=> 'empid',
        'data_attr' => true,
    ],
    'empname' => [
        'disp_name' => '申請者',
        'sortable'  => true
    ],
    'req_state' => [
        'disp_name' => '',
        'sortable'  => false,
        'class_name'=> 'req-state'
    ],
    'change_state'  => [
        'disp_name' => '',
        'data_attr' => true
    ],
    'status_name'   => [
        'disp_name' => 'ｽﾃｰﾀｽ',
        'sortable'  => true,
    ],
    'key_name' => [
        'disp_name' => '',
        'class_name'=> 'key-name',
        'data_attr' => true,
    ],
    'key_name_exists' => [
        'disp_name' => '',
        'data_attr' => true,
    ]
];
$btnTemplate = '
    <button class="btn btn-sm mb-1 $btnClass btn-act w-100 hide">
        $btnName
    </button>';
$actionList = [
    [
        'template'  => $btnTemplate,
        '$btnName'  => '承認',
        '$btnClass' => 'btn-blue',
    ],
];
raise_sql($logs, 'request');

$smarty->assign('reqList', $reqList);
$smarty->assign('headerList', $headerList);
$smarty->assign('actionList', $actionList);
$smarty->assign('frmValues', $frmValues);

$smarty->assign('paginationStr', $paginationStr);
$smarty->assign('sortIndex', $sortIndex);

$smarty->assign('pageTitle', '申請一覧');
$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'cons/request.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
