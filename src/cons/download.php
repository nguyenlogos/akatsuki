<?php
$name = getreq("keyname");

$filepath = load_keypem($name);

if (!file_exists($filepath)) {
    rd("src/404.php");
}

$content = file_get_contents($filepath);
$content = decryptIt($content);
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename='" . $name . "'");

exit($content);
