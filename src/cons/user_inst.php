<?php
require_once(ROOT_PATH . "/if/updateEBS.php");

use Akatsuki\Models\Volumes;
use Akatsuki\Models\Instance;
use AwsServices\Ec2;
use Aws\Ec2\Ec2Client;

$listIns = '';
$listInss = '';
$listInstanceAws = '';
$getListInsAWS = '';
$cid = $_SESSION['cid'];

//list instance AWS
$sql = "SELECT * FROM proj WHERE cid = ".$cid." AND status = 0 ";
$aws = pg_query($smarty->_db, $sql);
$listInsAWS = pg_fetch_all($aws);

$updateData = function () {
    global $smarty;
    $config = array(
        'credentials' => [
            'key'     => $_SESSION["key"],
            'secret'  => $_SESSION["secret"],
        ],
        'region'      => $_SESSION["region"],
        'version'     => 'latest'
    );
    $ret = \Akatsuki\Models\Instance::updateInstances();
    $ret && $ret = updateEBS($_SESSION["cid"], $smarty, $config);
};

if ($_SERVER['REQUEST_METHOD'] === 'POST' && getreq('ajax') !== 'add') {
    $updateData();

    $pcode = pg_escape_string(postreq('pcodeID'));
    $pname = "";
    $conditions = [
        "(ins.cid = {$cid})",
        "(ins.state != 'terminated')",
        "(ins.state != 'shutting-down')",
    ];

    if ($_SESSION['sysadmin'] != '1') {
        if ($pcode === '未設定') {
            $conditions = [];
        } else {
            $listAssigned = [];
            if (!$_SESSION['admin']) {
                $listAssigned = \Akatsuki\Models\EmpProj::getListAssigned();
                $listAssigned = array_column($listAssigned, 'pcode');
                if (!in_array($pcode, $listAssigned)) {
                    $listAssigned = [];
                } else {
                    $listAssigned = [$pcode];
                }
            } else {
                $listAssigned = [$pcode];
            }
            if (!count($listAssigned)) {
                $conditions = [];
            } else {
                $conditions[] = "ins.pcode = '{$pcode}'";
            }
        }
    } else {
        if ($pcode == '未設定') {
            $conditions[] = "ins.pcode IS NULL";
        } else {
            $conditions[] = "ins.pcode = '{$pcode}'";
        }
    }

    if (count($conditions) === 0) {
        $listInstance = null;
    } else {
        $conditions = "WHERE " . implode(" AND ", $conditions);

        $sql = "
            SELECT
                ins.*
            FROM instance ins
            LEFT JOIN
                proj p
                ON
                    p.pcode = ins.pcode
                AND
                    p.cid = ins.cid
                AND
                    p.status = 0
            $conditions
            ORDER BY
                ins.availabilityzone
        ";

        $listInstance = Instance::selectRaw($sql);
    }

    $itemIndex = 1;
    $instStr = "";
    if ($listInstance) {
        foreach ($listInstance as $item) {
            $instReqBs = Volumes::where('cid', $cid)
                ->where('device', '!=', $item['rootdevicename'])
                ->where("instid", $item['id'])
                ->get()->toArray();
            $instStr .= "{";
            $instStr .= "
                id: \"{$item['id']}\",
                text: \"{$item['id']} - ({$item['availabilityzone']})\",
            ";
            $instStr .= '
                icons: {
                    folder_opened: "fa-notebook-cs",
                    folder_closed: "fa-notebook-cs",
                    file: "fa-notebook-cs"
                },
                items: [
            ';
            $instStr .= '{id:"' .($itemIndex++). '", text:"' . INSTANCE_STORAGE . '",';
            $instStr .= '
                icons: {
                    folder_opened: "fa-hdd-o",
                    folder_closed: "fa-hdd-o",
                    file: "fa-hdd-o"
                },
                items: [
            ';
            if ($instReqBs) {
                foreach ($instReqBs as $key) {
                    $instStr .= '{id:"'.$key['volid'].'", text:"' . $key['device'] . ' ';
                    $instStr .= 'サイズ:' . $key['size'] . 'GB タイプ:' . $key['voltype'] . '"},';
                }
            }
            $instStr .= ']},';

            $instStr .= '{id:"' .($itemIndex++). '", text:"' . INSTANCE_ELASTIC_IP . '",';
            $instStr .= '
                icons: {
                    folder_opened: "fa-share-alt",
                    folder_closed: "fa-share-alt",
                    file: "fa-share-alt"
                },
                items: [
            ';
            if ($item['elasticip']) {
                $instStr .= '{id:"' . $item['elasticip'] . '", text:"' . $item['elasticip'] . '"},';
            }
            $instStr .= ']},';

            $instStr .= ']},';
        }
    }
    if ($pcode) {
        $proj = \Akatsuki\Models\Proj::select('pname')
            ->where('cid', $_SESSION['cid'])
            ->where('pcode', $pcode)->first();
        $pname = $proj ? $proj->pname : '';
    }
    if (!$pname) {
        $pname = "未設定";
    }

    $instStr = trim($instStr, ", ");
    $rootText = "";
    if (getreq('ajax') == '2') {
        // !$pcode && $pcode = $pname;
        $inst  = "{id : ".($itemIndex++).", items:[";
        $inst .= "{id : '".($pcode++)."', text: '".$pname."',";
        $inst .= "
            icons: {
                folder_opened: \"fa-project-cs\",
                folder_closed: \"fa-project-cs\",
                file: \"fa-project-cs\"
            },
            items:[
        ";
        $inst .=  $instStr;
        $inst .= ']}';
        $inst .= ']}';
        $instStr = $inst;
    } elseif (!$instStr) {
        $instStr = "{id: 0, items:[]}";
    } else {
        $inst  = "{id : ".($itemIndex++).", items:[";
        $inst .=  $instStr;
        $inst .= ']}';
        $instStr = $inst;
    }
    $result = "var data = {$instStr}";

    sendAjaxResponse([
        'result' => $result
    ]);
}
$smarty->assign('listIns', $listIns ?: []);
$smarty->assign('listInss', $listInss ?: []);

if ($_SERVER['REQUEST_METHOD'] === 'POST' && getreq('ajax') == 'add') {
    $updateData();
    $data = postreq('data');
    $data = json_decode($data, true) ?: [];
    $pcode = !empty($data['pcode']) ? $data['pcode'] : '未設定';
    $listValue = !empty($data['listNew']) ? $data['listNew'] : [];
    $elasticIp = '';
    $instIp = '';
    $sgId = '';
    $subSrtInst = '';

    $result = '';

    $newPcodeList = array_keys($listValue);

    $instances = Instance::select('id')->where('cid', $cid)
        ->whereIn('id', $newPcodeList);
    if ($pcode == '未設定') {
        $instances->whereNull('pcode');
    } else {
        $instances->where('pcode', $pcode);
    }
    $instances = $instances->get()->toArray();
    $instIds = array_column($instances, 'id');
    if ($pcode == '未設定') {
        $removePcodeList = $changePcodeList = [];
    } else {
        $unchangePcodeList = array_intersect($newPcodeList, $instIds);
        $removePcodeList = array_diff($instIds, $unchangePcodeList);
        $changePcodeList = array_diff($newPcodeList, $unchangePcodeList);
    }

    foreach ($listValue as $instanceId => $specs) {
        $detachedList = [];
        $attachedList = [];
        $sql = "
            SELECT i.availabilityzone as zone, i.elasticip, v.volid, v.device, i.rootdevicename
            FROM instance i
            LEFT JOIN volumes v
            ON i.id = v.instid
                AND i.cid = v.cid
            WHERE i.id = '{$instanceId}'
                AND i.cid = {$_SESSION['cid']}
        ";

        $volumes = Volumes::selectRaw($sql);
        if (!$volumes || count($volumes) === 0) {
            sendAjaxResponse([
                'err' => 1,
                'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
            ]);
        }

        $instZone = $volumes[0]['zone'];
        $ec2Client = Ec2::getInstance($instZone);

        // check elastic ips
        $ips = $specs['ips'];
        $newElasticIp = array_pop($ips);
        $currentElasticIp = $volumes[0]['elasticip'];
        $ipListAttach = [];
        $ipListDetach = [];
        if ($newElasticIp) {
            $newElasticIp = explode("|", $newElasticIp['id'])[0];
            if ($currentElasticIp !== $newElasticIp) {
                $isAvailable = $ec2Client->checkAddressAvailability($newElasticIp);
                if (!$isAvailable) {
                    $ipListDetach[] = $newElasticIp;
                }
                $ipListAttach[] = $newElasticIp;
            }
        } else {
            if ($currentElasticIp) {
                $ipListDetach[] = $currentElasticIp;
            }
        }

        foreach ($ipListDetach as $ipAddress) {
            $result = $ec2Client->disassociateAddress($ipAddress);
            if (!$result) {
                sendAjaxResponse([
                    'err' => 1,
                    'msg' => aws_get_last_error() ?: MESSAGES['ERR_ADDRESS_DISASSOCIATE']
                ]);
            }
        }

        foreach ($ipListAttach as $ipAddress) {
            $result = $ec2Client->associateAddress($instanceId, $ipAddress);
            if (!$result) {
                sendAjaxResponse([
                    'err' => 1,
                    'msg' => aws_get_last_error() ?: MESSAGES['ERR_ADDRESS_ASSOCIATE']
                ]);
            }
        }

        // check ebs changes
        $newVolumes = array_map(function ($item) {
            $item['id'] = explode('|', $item['id'])[0];
            return $item;
        }, $specs['ebs']);

        $devicesMapping = [];
        foreach ($newVolumes as $volume) {
            $deviceName = explode(' ', $volume['text'])[0];
            $devicesMapping[$volume['id']] = trim($deviceName, ' ');
        }

        $currentVolumes = array_filter($volumes, function ($volume) {
            return $volume['device'] !== $volume['rootdevicename'];
        });

        $currentVolumeIds = array_column($currentVolumes, 'volid');
        $newVolumeIds = array_column($newVolumes, 'id');

        $listKeep = array_intersect($currentVolumeIds, $newVolumeIds);
        $listAttach = array_diff($newVolumeIds, $listKeep);
        $listDetach = array_diff($currentVolumeIds, $listKeep);

        if (count($listAttach)) {
            $volumeIds = Volumes::select('volid')->whereIn('volid', $listAttach)
                ->where('cid', $_SESSION['cid'])
                ->where('state', 'attached')
                ->groupBy('volid', 'instid')->get()->toArray();
            $volumeIds = array_column($volumeIds, 'volid');
            $listDetach = array_merge($listDetach, $volumeIds);
        }
        // detach volumes

        $matchedVolumes = array_intersect($listAttach, $listDetach);
        $waitForAvailable = count($matchedVolumes) > 0;

        foreach ($listDetach as $volid) {
            if (!$volid || in_array($volid, $detachedList)) {
                continue;
            }

            $result = $ec2Client->detachVolume($volid, $waitForAvailable);
            if (!$result) {
                sendAjaxResponse([
                    'err'    => 1,
                    'result' => aws_get_last_error() ?: MESSAGES['ERR_EBS_DETACH']
                ]);
            }
            $detachedList[] = $volid;
        }

        // attach volumes
        foreach ($listAttach as $volid) {
            if (in_array($volid, $attachedList)) {
                continue;
            }
            $deviceName = $devicesMapping[$volid];
            $result = $ec2Client->attachVolume($instanceId, $volid, $deviceName);
            if (!$result) {
                sendAjaxResponse([
                    'err' => 1,
                    'msg' => aws_get_last_error() ?: MESSAGES['ERR_EBS_ATTACH']
                ]);
            }
            $attachedList[] = $volid;
        }
    }

    if (count($changePcodeList)) {
        $instStr = implode(',', array_map(function ($instid) {
            return "'{$instid}'";
        }, $changePcodeList));
        $sql = "UPDATE instance SET pcode = '{$pcode}' WHERE id IN ($instStr)";
        $result = Instance::selectRaw($sql);
    }
    if (count($removePcodeList)) {
        $instStr = implode(',', array_map(function ($instid) {
            return "'{$instid}'";
        }, $removePcodeList));
        $sql = "UPDATE instance SET pcode = null WHERE id IN ($instStr)";
        $result = Instance::selectRaw($sql);
    }
    sendAjaxResponse([
        'err' => 0
    ]);
}


$smarty->assign('pageTitle', 'リソースの移動');

$smarty->assign('listInsRef', $listInsRef);
$smarty->assign('listInsAWS', $listInsAWS);
$smarty->assign('errmsg', $errmsg);
$smarty->assign('msg', $msg);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'cons/user_inst.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
