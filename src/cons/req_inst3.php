<?php
use Akatsuki\Models\InstReq;
use Akatsuki\Models\KeyPair;
use Akatsuki\Models\SecurityGroup;

require_once(ROOT_PATH . "/if/createKey.php");
require_once(ROOT_PATH . '/if/updateSG.php');
require_once('inst_req_loader.php');
$config = array(
    'credentials' => [
        'key'    => $_SESSION["key"],
        'secret' => $_SESSION["secret"],
    ],
    'region'  => $_SESSION["region"],
    'version' => 'latest'
);

$instReqInfo = sesreq('inst_req');
$sgInfo = $instReqInfo['sg'];
if (empty($sgInfo['sg_name']) || empty($sgInfo['sg_desc'])) {
    rd('cons/security_group.php?id='.$instReqInfo['id']);
}

$stepOneUrl = "./req_inst.php";
$formActionUrl = "./req_inst3.php";
$instReqId = (int)getreq("id");
$ebsReadOnly = false;
$ButtonSG = '';

//check session value
$dataarr = sesreq('arr_stepone') ?: null;
if (is_null($dataarr) || (!empty($dataarr['id']) && $dataarr['id'] != $instReqId)) {
    header("Location:/cons/req_inst.php");
    exit();
}

$instReq = $instReqId > 0 ? InstReq::where("id", $instReqId)
    ->where("empid", $_SESSION['empid'])
    ->first() : null;
if ($instReq) {
    $stepOneUrl .= "?id=" . $instReq->id;
    $formActionUrl .= "?id=" . $instReq->id;
}
$errMsg = "";
$err = "";
$smarty->assign('addNew', false);
$cid = ""; // メッセージ
$cid = (int)$_SESSION["cid"];
$checkKeyPair = false;

$config = array(
    'credentials' => [
        'key'    => $_SESSION["key"],
        'secret' => $_SESSION["secret"],
    ],
    'region'  => $_SESSION["region"],
    'version' => 'latest'
);
$listKey = ListKeyPair($config);

// Create key pair
if (isset($_POST['btn_key_pair'])) {
    $type = postreq('createKey');
    $key_pair = postreq('key_pair');
    $name_pair = "";
    $_SESSION['name_key_pair'] = "";
    if ($type === 'EXISTING_KEYPAIR') {
        $_SESSION['name_key_pair'] = $key_pair;
    } elseif ($type === 'NEW_KEYPAIR') {
        $name_pair = postreq('name_pair');
        ;
        if (empty($name_pair)) {
            $errMsgKey = "Keyname must not be empty.";
            $err = 1;
        } else {
            foreach ($listKey as $key) {
                if ($name_pair == $key) {
                    $errMsgKey = MESSAGES['KEYPAIR_ALREADY_EXISTS'];
                    $err = 1;
                }
            }
        }
    }

    if (!$err && $name_pair) {
        $create_key = CreateKeyPair($config, $name_pair);
        if (!$create_key) {
            $err = 1;
            $errMsgKey = MESSAGES['KEYPAIR_ALREADY_EXISTS'];
        } else {
            $pair = new KeyPair();
            $pair->setValues([
                'cid'           => $cid,
                'name_key'      => $name_pair,
                'file_key_pair' => $name_pair.'.pem',
                'create_time'   => date("Y-m-d h:i:sa"),
            ]);

            $result = $pair->save();
            if ($result) {
                $_SESSION['name_key_pair'] = $name_pair;
                $errMsg = MESSAGES['KEYPAIR_UPDATE_SUCCESS'];
                $err = 0;
            } else {
                $errMsg = MESSAGES['KEYPAIR_UPDATE_UNSUCCESSFUL'];
                $err = 1;
            }
        }
    }
}

$pair = KeyPair::where("name_key", $_SESSION['name_key_pair'])
               ->first();
$step_two = [];
// get session data step 1
$dataarr = $_SESSION['arr_stepone'];

$ebsEditable = !$instReq || in_array((int)$instReq->approved_status, INST_REQ_EDITABLE_STATES);
if ($ebsEditable) {
    $ButtonSG = "
        <a class='btn btn-orange unlock_sg'
            href='./security_group.php?id={$instReqId}'
            role='button'>セキュリティグループを変更
        </a>
    ";
}
if (!$ebsEditable && $dataarr) {
    $instReqId = (int)$dataarr['id'];
    $instReqBs = \Akatsuki\Models\InstReqBs::where("inst_req_id", $instReqId)->get();
    if ($instReqBs) {
        $instReqBs = $instReqBs->toArray();
        foreach ($instReqBs as $i => $bs) {
            $step_two[] = [
                "reqid"     => $instReqId,
                "no"        => $i,
                "type"      => "EBS",
                "device"    => $bs["device_name"],
                "snapid"    => $bs["snapshot_id"],
                "size"      => $bs["volumn_size"],
                "orgsize"   => "",
                "devtype"   => $bs["volumn_type"],
                "iops"      => $bs["iops"],
                "autodel"   => (int)$bs["auto_removal"],
                "enc"       => (int)$bs["encrypted"],
                "stat"      => 0
            ];
        }
    } else {
        $step_two = [];
    }
} else {
    $step_two = sesreq('listDev') ?: [];
}

$reture = $ebsEditable ?
    '<input type="submit" class="btn w_250 btn-blue" name="step2" value="ディスクの構成を変更"/>' : "";
if (postreq("step2")) {
    $numberKeyDisks = explode(',', postreq('numberKeyDisks'));
    $no = 0;
    foreach ($numberKeyDisks as $i) {
        $bsInfostep2[] = [
            "reqid"   => $instReqId,
            "no"      => $no,
            "type"    => "EBS",
            "device"  => postreq("devname".$i),
            "snapid"  => "",
            "size"    => postreq("stsize".$i),
            "orgsize" => "",
            "devtype" => postreq("devtype".$i),
            "iops"    => postreq("iops".$i),
            "autodel" => postreq("autodel".$i),
            "enc"     => postreq("enc".$i),
            "stat"    => 0
        ];
        $no++;
    }
    $_SESSION['listDev'] = $bsInfostep2;
    if ($instReq) {
        header("Location:/cons/req_inst2.php?bk=2&id=" . $instReqId);
    } else {
        header("Location:/cons/req_inst2.php?bk=2");
    }
}
$step = $_SESSION['listDev'] ? 'yes' : 'no';
$step_as = $_SESSION['listDevsss'] ? 'yes' : 'no';

// get name dept
$query = sprintf('SELECT deptname FROM dept WHERE cid = $1 and status = 0 and dept = $2');
$logs[] = $query;

$r = pg_query_params($smarty->_db, $query, [
    $cid,
    $dataarr['showgroup']
]);
$dt_dept = pg_fetch_array($r, null, PGSQL_ASSOC);

// get name project
$query = sprintf("SELECT pname FROM proj WHERE cid = $1 and status = 0 and pcode = $2");
$logs[] = $query;
$r = pg_query_params($smarty->_db, $query, [
    $cid,
    $dataarr["showgproject"]
]);
$dt_proj = pg_fetch_array($r, null, PGSQL_ASSOC);

// get name ami
$query = sprintf("SELECT name FROM ami WHERE cid = $1 and  imageid = $2");
$logs[] = $query;
$r = pg_query_params($smarty->_db, $query, [
    $cid,
    $dataarr["ami"]
]);
$dt_ami = pg_fetch_array($r, null, PGSQL_ASSOC);

//get SG
$IdSG = $_SESSION['IdSG'];

if ($IdSG) {
    $list_sg = SecurityGroup::where("id", $IdSG)
                            ->where("cid", $cid)
                            ->get();
    $list_sg = $list_sg->toArray();
}

if ($instReq) {
    $list_sg = SecurityGroup::where("inst_req_id", $instReq->id)
                            ->where("cid", $cid)
                            ->get();
    $list_sg = $list_sg->toArray();
    $checkKeyPair = true;
} else {
    $checkKeyPair = $_SESSION['name_key_pair'];
}


if ($step == 'yes' || $step_as == 'yes' || !empty($dataarr) || !empty($step_two)) {
    if (isset($_POST['btUPdate'])) {
        $instInfo = [
            "cid"               => $_SESSION['cid'],
            "empid"             => $_SESSION['empid'],
            "dept"              => $dataarr['showgroup'],
            "pcode"             => $dataarr['showgproject'],
            "inst_ami"          => $dataarr['ami'],
            "inst_vpc"          => $dataarr['vpc'],
            "inst_address"      => $dataarr['address'],
            "inst_subnet"       => $dataarr['subnet'],
            "inst_type"         => $dataarr['insttype'],
            "inst_count"        => $dataarr['instnum'],
            "inst_region"       => $dataarr['azone'],
            "security_groups"   => null,
            "using_purpose"     => $dataarr['usage'],
            "using_from_date"   => $dataarr['datefrom'],
            "using_till_date"   => $dataarr['dateto'],
            "shutdown_behavior" => $dataarr['shutdownbe'],
            "approved_status"   => 0,
            "req_date"          => "now()",
            "key_name"          => $instReq->key_name ? $instReq->key_name : $pair['name_key']
        ];

        $bsInfo = [];
        $listType = [];
        $numberKeyDisks = explode(',', postreq('numberKeyDisks'));
        foreach ($numberKeyDisks as $i) {
            $devName = postreq("devname" . $i);
            if (in_array($devName, $listType)) {
                $err = 'errDevName';
            }
            array_push($listType, $devName);
            $bsInfo[] = [
                "device_name"  => $devName,
                "volumn_type"  => postreq("devtype" . $i),
                "volumn_size"  => postreq("stsize" . $i),
                "iops"         => (int)postreq("iops" . $i),
                "encrypted"    => (int)postreq("enc" . $i),
                "auto_removal" => (int)postreq("autodel" . $i),
                "snapshot_id"  => null,
            ];
        }
        $sgInfo = $instReqInfo['sg'];
        if (!$err) {
            if ($instReq) {
                $instReq->updateInfo($instInfo, $bsInfo, $sgInfo);
            } else {
                $instReq = InstReq::register($instInfo, $bsInfo, $sgInfo);

                $SecurityGroup = SecurityGroup::where("id", $IdSG)
                    ->where("cid", $_SESSION['cid'])
                    ->first();
                if ($SecurityGroup) {
                    $SecurityGroup->setValues([
                        'inst_req_id' => $instReq->id,
                    ]);
                    $SecurityGroup->save();
                }
            }

            if ($instReq) {
                unset($_SESSION['inst_req']);
                unset($_SESSION['arr_stepone'], $_SESSION['listDev'], $_SESSION['name_key_pair'], $_SESSION['IdSG']);
                header("Location:/cons/request.php");
            }
        }
    } elseif (isset($_POST['btDelete'])) {
        unset(
            $_SESSION['arr_stepone'],
            $_SESSION['listDev'],
            $_SESSION['listDevsss'],
            $_SESSION['name_key_pair'],
            $_SESSION['IdSG']
        );
        header("Location:/cons/req_inst.php");
    }
} else {
    header("Location:/cons/req_inst.php");
}
raise_sql($logs, 'reg_inst3');

$smarty->assign('pageTitle', 'インスタンスの申請');

$smarty->assign('listDev', $step_two);
$smarty->assign('errMsg', $errMsg);
$smarty->assign('errMsgKey', $errMsgKey);
$smarty->assign('err', $err);
$smarty->assign('pair', $checkKeyPair);
$smarty->assign('list_sg', $list_sg);

$smarty->assign('dataarr', $dataarr);
$smarty->assign('reture', $reture);
$smarty->assign('dt_dept', $dt_dept);
$smarty->assign('dt_proj', $dt_proj);
$smarty->assign('dt_ami', $dt_ami);
$smarty->assign('listKey', $listKey);
$smarty->assign('ebsEditable', +$ebsEditable);
$smarty->assign('stepOneUrl', $stepOneUrl);
$smarty->assign('formActionUrl', $formActionUrl);
$smarty->assign('ButtonSG', $ButtonSG);

$smarty->assign('instReqInfo', $instReqInfo);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);

$smarty->assign('viewTemplate', 'cons/req_inst3.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
