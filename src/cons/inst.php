<?php
require_once(ROOT_PATH . "/if/updateEBS.php");
require_once("inst_function.php");

$msg = "";
$showgroup = "";
$showgproject = "";
$group = "";
$project = "";
$itemIndex = 0;
// インスタンスリストを更新
$config = array(
    'credentials'   => [
        'key'       => $_SESSION["key"],
        'secret'    => $_SESSION["secret"],
    ],
    'region'        => $_SESSION["region"],
    'version'       => 'latest'
);
$textStates = [
    'pending'  => '準備中...',
    'running'  => '稼働中',
    'stopping' => '停止中...',
    'stopped'  => '停止済',
];

$forceReupdate = (int)getreq('debug') ? (int)getreq('op') : true;

// reupdate ami list
$msg = "";
if ($forceReupdate) {
    $ret = \Akatsuki\Models\Instance::updateInstances();
    $ret && $ret = \Akatsuki\Models\Sg::updateSecurityGroups();
    $ret && $ret = updateEBS($_SESSION["cid"], $smarty, $config);
}

// filter by group and project
$hideDept = false; // hide dept selection from filter
$forDept = null; // used for filtering instance info
$forProjs = []; // used for filtering instance info
$listAssigned = [];

$deptRoles = permission_check("mst/dept.php", true);
$pageRoles = permission_check("cons/inst.php", true);
if (!$deptRoles['read']['allowed'] || $deptRoles['read']['condition'] == 1) {
    $forDept = $showgroup = $_SESSION['dept'];
    $hideDept = true;
} else {
    $showgroup = postreq("dept");
}

$showgproject = postreq("proj");
$conditions = [
    "(e.cid = {$_SESSION['cid']})",
    "(e.state != 'terminated')",
    "(e.state != 'shutting-down')",
];
if ($pageRoles['read']['condition'] == 5) {
    $listAssigned = \Akatsuki\Models\EmpProj::getListAssigned();
    $listAssigned = array_column($listAssigned, 'pcode');
    if ($showgproject) {
        $forProjs = array_intersect($listAssigned, [$showgproject]);
    } else {
        $forProjs = $listAssigned;
    }

    if (count($forProjs)) {
        $projs = array_map(function ($pcode) {
            return "'{$pcode}'";
        }, $forProjs);
        $conditions[] = sprintf("(e.pcode IN (%s))", implode(",", $projs));
    } else {
        $conditions = [];
    }
} elseif ($showgroup) {
    $conditions[] = sprintf("(f.dept = %d)", (int)$showgroup);
    if ($showgproject) {
        $fmPName = pg_escape_string($showgproject);
        $conditions[] = sprintf("(e.pcode = '%s')", $fmPName);
    }
}

$activeCount = 0;
$inactiveCount = 0;
$reqRunningCount = 0;
$reqWarningCount = 0;
$reqOutdatedCount = 0;
// TODO: remove unused variables if no longer use
$dataStr .= "{id: ".(++$itemIndex).",item:[";
$instGrp = [];
$totalCount = 0;
if (count($conditions) > 0) {
    $statusFilter = postreq("inst_filter");
    switch ($statusFilter) {
        case 'stopped':
            $conditions[] = "(e.state = 'stopped' OR e.state = 'stopping')";
            break;
        case 'running':
            $conditions[] = "(e.state = 'pending' OR e.state = 'running')";
            break;
        case 'req_warning':
            $conditions[] = "(e.using_till_date::date > now()
                AND e.using_till_date::date - now() <= '14 days'::interval)";
            break;
        case 'req_outdated':
            $conditions[] = "(e.using_till_date < now())";
            break;
        case 'req_running':
            $conditions[] = "(e.using_till_date::date > now()
                AND e.using_till_date::date - now() > '14 days'::interval)";
            break;
        default:
            break;
    }
    $conditions = "WHERE " . implode(' AND ', $conditions);
    $sql = "
        SELECT
            e.*,
            f.pname pname,
            f.dept
        FROM (
            SELECT
                c.*,
                d.device,
                d.size,
                d.volid,
                d.voltype,
                d.tag_name,
                d.snapshotid,
                d.createtime,
                d.attachtime,
                i.using_from_date,
                i.using_till_date
            FROM (
                SELECT
                    a.cid,
                    a.id instid,
                    a.name instname,
                    type,
                    platform,
                    state,
                    b.name sgname,
                    b.id sgid,
                    availabilityzone,
                    launchtime,
                    privatednsname,
                    publicdnsname,
                    privateip,
                    publicip,
                    kernelid,
                    ramdiskid,
                    imageid,
                    a.pcode,
                    a.inst_req_id
                FROM
                    instance a,
                    sg b
                WHERE
                    a.cid = {$_SESSION['cid']}
                    AND b.cid = {$_SESSION['cid']}
                    AND a.sg = b.id
                ORDER BY
                    a.name,
                    a.id
            ) c
            LEFT OUTER JOIN
                volumes d
                ON (c.instid = d.instid
                AND d.cid = {$_SESSION['cid']}
                AND d.stragetype = 'EBS')
            LEFT JOIN inst_req i
                ON i.id = c.inst_req_id
            ORDER BY
                c.instname,
                c.instid
        ) e
        LEFT OUTER JOIN
            proj f
            ON (e.pcode = f.pcode
            AND f.cid = {$_SESSION['cid']}
            AND f.status = 0)
        $conditions
        ORDER BY e.instid, f.pcode
    ";

    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $totalCount = pg_num_rows($r);
    for ($i = 0; $i < $totalCount; $i++) {
        $rowInst = pg_fetch_assoc($r, $i);
        if (empty($rowInst["pcode"])) {
            $rowInst["pcode"] = $rowInst["pname"] = "未設定";
        }

        $inst = createINSTStr(
            $smarty,
            $r,
            $rowInst,
            $i,
            $activeCount,
            $inactiveCount,
            $reqRunningCount,
            $reqWarningCount,
            $reqOutdatedCount
        );
        $pcode = $rowInst['pcode'];
        if (!array_key_exists($pcode, $instGrp)) {
            $instGrp[$pcode] = [
                'pname' => $rowInst['pname'],
                'insts' => []
            ];
        }
        $instGrp[$pcode]['insts'][] = $inst;
    }

    $instStr = "";
    foreach ($instGrp as $list) {
        $inst = implode('', $list['insts']);
        $inst = trim($inst, " ,");
        $instStr .= "{";
        $instStr .= "id : ".(++$itemIndex).", text: '".$list['pname']."',";
        $instStr .= "icons: {folder_opened: \"fa-project-cs\", folder_closed: \"fa-project-cs\"}, items:[";
        $instStr .=  $inst;
        $instStr .= ']},';
    }
    $dataStr .= $instStr;
}

$dataStr .= ']}';

if (count($conditions)) {
    if (count($listAssigned)) {
        $instInfo = getInfoInstanceOverview($smarty, $_SESSION['cid'], null, $listAssigned);
    } else {
        $instInfo = getInfoInstanceOverview($smarty, $_SESSION['cid'], $forDept);
    }
} else {
    $instInfo = [
        'reqRunningCount'  => 0,
        'reqWarningCount'  => 0,
        'reqOutdatedCount' => 0,
        'activeCount'      => 0,
        'inactiveCount'    => 0,
    ];
}

$frmValues = [
    'dept' => $showgroup,
    'proj' => $showgproject,
];
raise_sql($logs, 'inst');

$schar = "";
$smarty->assign('showgroup', $showgroup);
$smarty->assign('showgproject', $showgproject);

$smarty->assign('instInfo', $instInfo);
$smarty->assign('frmValues', $frmValues);
$smarty->assign('hideDept', $hideDept ? 'hide' : '');
$smarty->assign('totalCount', $totalCount);

$smarty->assign('pageTitle', 'インスタンス一覧');
$smarty->assign('msg', $msg);
$smarty->assign('schar', $schar);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('dataStr', $dataStr);
// $smarty->assign('dataStr', trim($dataStr, ","));
$smarty->assign('viewTemplate', 'cons/inst.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
