<?php
use Aws\Exception\AwsException as AwsException;

function createINSTStr(
    &$smarty,
    &$r,
    &$rowInst,
    &$i,
    &$activeCount,
    &$inactiveCount,
    &$reqRunningCount,
    &$reqWarningCount,
    &$reqOutdatedCount
) {
    global $textStates;
    global $itemIndex;
    $dataStr = "";

    if ($rowInst["instname"] == "") {
        $rowInst["instname"] = "未設定";
    }
    $instState = $rowInst["state"];
    $Result = $instState;
    $attr = sprintf('data-instid="%s" data-state="%s"', $rowInst['instid'], $instState);
    $rowInst["state"]  = "";
    $rowInst["state"] .= "<button disabled=\"disabled\" class=\"btn btn-state\" $attr>";
    $rowInst["state"] .= "    <i class=\"fas fa-cog spinner text-muted\"></i>";
    $rowInst["state"] .= "    <span class=\"text-muted\">点検中</span>";
    $rowInst["state"] .= "</button>";
    $rowInst["launchtime"] = str_replace("-", "/", $rowInst["launchtime"]);
    $rowInst["launchtime"] = str_replace("T", " ", $rowInst["launchtime"]);
    $rowInst["launchtime"] = str_replace("+00:00", "", $rowInst["launchtime"]);

    if (!empty($rowInst['using_till_date'])) {
        $expiredDate = strtotime($rowInst['using_till_date']);
        $timenow = time();
        if ($timenow > $expiredDate) {
            $reqOutdatedCount++;
        } elseif ($expiredDate - $timenow < 86400 * 14) {
            $reqWarningCount++;
        } else {
            $reqRunningCount++;
        }
    }

    // マシンタイトルノード
    $dataStr .= "{";
    $dataStr .= "id: ".(++$itemIndex).", text: '" . $rowInst["instname"] . " (ID:" . $rowInst["instid"] . ") " .
        $rowInst["state"] . "',";
    $dataStr .= "icons: {folder_opened: \"fa-notebook-cs\", folder_closed: \"fa-notebook-cs\"},";
    if ($instState == "停止") {
        $dataStr .= "backColor: 'whitesmoke',";
    }
    // マシン詳細ノード
    $dataStr .= "items:[";

    $dataStr .= "{";
    $dataStr .= "id: ".(++$itemIndex).", text: 'タイプ:" . $rowInst["type"] . " (" .
        $rowInst["platform"] . ") 起動:" . $rowInst["launchtime"] . "',";
    $dataStr .= "icons: {file: \"fa-type-cs\"},";
    $dataStr .= "},";

    $tmpStr = "";
    if ($rowInst["kernelid"] != "") {
        $tmpStr .= " Kernel ID:" . $rowInst["kernelid"];
    }
    if ($rowInst["ramdiskid"] != "") {
        $tmpStr .= " Ramdisk ID:" . $rowInst["ramdiskid"];
    }
    if ($rowInst["imageid"] != "") {
        $tmpStr .= " Image ID:" . $rowInst["imageid"];
    }

    if ($tmpStr != "") {
        $dataStr .= "{";
        $dataStr .= "id: ".(++$itemIndex).", text: '" . $tmpStr . "', icons: {file: \"fa-image-no\"}";
        $dataStr .= "},";
    }

    $dataStr .= "{";
    $dataStr .= "id: ".(++$itemIndex).", text: 'セキュリティグループ:" . $rowInst["sgname"] .
        " (" . $rowInst["sgid"] . ")',";
    $dataStr .= "icons: {folder_opened: \"fa-shield\", folder_closed: \"fa-shield\"},";
    $dataStr .= createSGStr($smarty, $rowInst["sgid"]);
    $dataStr .= "},";

    $dataStr .= "{";
    $dataStr .= "id: ".(++$itemIndex).", text: 'ネットワーク',";
    $dataStr .= "icons: {folder_opened: \"fa-network-cs\", folder_closed: \"fa-network-cs\"},";
    $dataStr .= "items: [";

    $dataStr .= "{";
    $dataStr .= "id: ".(++$itemIndex).", text:'データセンター:" . $rowInst["availabilityzone"] . "',";
    $dataStr .= "icons: {file: \"fa-database-no\"},";
    $dataStr .= "},";
    $dataStr .= "{";
    $dataStr .= "id: ".(++$itemIndex).", text: 'プライベートDNS:" . $rowInst["privatednsname"] . " (IPアドレス:" .
        $rowInst["privateip"] . ")',";
    $dataStr .= "icons: {file: \"fa-gear-no\"},";
    $dataStr .= "},";

    $dataStr .= "{";
    $dataStr .= "id: ".(++$itemIndex).", text: 'グローバルDNS:" . $rowInst["publicdnsname"] . " (IPアドレス:" .
        $rowInst["publicip"] . ")',";
    $dataStr .= "icons: {file: \"fa-globe-no\"},";
    $dataStr .= "},";
    $dataStr .= "]},";

    //
    $i++;
    $hddStr = "";
    $hddCount = 0;
    $ssCount = 0;

    if ($rowInst["device"] != "") {
        // 1つめのHDD
        $hddStr .= createHDDStr($rowInst, $smarty, $ssCount);
        $hddCount++;
    }
    for (; $i < pg_num_rows($r); $i++) {
        $rowTmp = pg_fetch_assoc($r, $i);
        if ($rowTmp["instid"] != $rowInst["instid"]) {
            $i--;
            break;
        }
        // HDD情報の追加
        $hddStr .= createHDDStr($rowTmp, $smarty, $ssCount);

        $hddCount++;
    }
    if ($hddCount != 0) {
        $dataStr .= "{";
        $dataStr .= "id: ".(++$itemIndex).", text: 'HDD x " . $hddCount . "(スナップショット x " . $ssCount . ")',";
        $dataStr .= "icons: {folder_opened: \"fa-hdd-o\", folder_closed: \"fa-hdd-o\"},";
        $dataStr .= "items: [";
        $dataStr .= $hddStr;
        $dataStr .= "]}";
    }

    //
    $dataStr .= ']},';

    return $dataStr;
}

function createHDDStr($arr, &$smarty, &$ssCount)
{
    global $itemIndex;
    $hddStr = "{";
    $hddStr .= "id: ".(++$itemIndex).", text: '" . $arr["device"] . " サイズ:" . $arr["size"] . "GB タイプ:" .
        $arr["voltype"] . " Name:" . $arr["tag_name"] . " (ID:" . $arr["volid"] . ")',";
    $hddStr .= "icons: {folder_opened: \"fa-save-no\", folder_closed: \"fa-save-no\"},";
    $hddStr .= "items: [";

    // ディスク詳細情報の追加
    $arr["createtime"] = str_replace("-", "/", $arr["createtime"]);
    $arr["attachtime"] = str_replace("-", "/", $arr["attachtime"]);
    $hddStr .= "{";
    $hddStr .= "id: ".(++$itemIndex).", text:'作成:" . $arr["createtime"] . " アタッチ:" . $arr["attachtime"] . "',";
    $hddStr .= "icons: {file: \"fa-calendar-no\"},";
    $hddStr .= "},";

    // スナップショットエントリーの追加
    $sql = sprintf("SELECT snapshotid, size, createtime, state, progress, description
                    FROM volumes
                    WHERE cid = $1
                        AND volid = $2
                        AND stragetype = $3
                    ORDER BY createtime DESC;");
    $logs[] = $sql;
    $r = pg_query_params($smarty->_db, $sql, [
        $_SESSION["cid"],
        $arr["volid"],
        'SS'
    ]);

    if (pg_num_rows($r) != 0) {
        $hddStr .= "{";
        $hddStr .= "id: ".(++$itemIndex).", text: 'スナップショット x " . pg_num_rows($r) . "',";
        $hddStr .= "items:[";
        for ($i = 0; $i < pg_num_rows($r); $i++) {
            $row = pg_fetch_assoc($r, $i);
            $row["createtime"] = str_replace("-", "/", $row["createtime"]);
            $hddStr .= "{";
            $hddStr .= "        id: ".(++$itemIndex).", text:'ID:" . $row["snapshotid"] . " サイズ:" .
                $row["size"] . "GB 作成:" . $row["createtime"] . " (";
            if ($row["state"] != "completed") {
                $hddStr .= " " . $row["progress"];
            } else {
                $row["state"] = "作成済";
            }
            $hddStr .= $row["state"] . ")',";
            $hddStr .= "},";
            $ssCount++;
        }
        $hddStr .= "]},";
    }
    $hddStr .= "]},";
    raise_sql($logs, 'inst');

    return ($hddStr);
}

function createSGStr(&$smarty, $sgid)
{
    global $itemIndex;
    $sql = sprintf("
        SELECT
            si.protocol,
            si.fromport,
            si.toport,
            si.iprange,
            o.name AS obj_name
        FROM
            sg_ipperm si
        LEFT JOIN obj o
        ON
            o.cid = si.cid
            AND (o.ipaddr || '/' || o.submask) = si.iprange
        WHERE
            si.cid = $1
            AND si.id = $2
        ORDER BY si.fromport
    ");
    $logs[] = $sql;
    $r = pg_query_params($smarty->_db, $sql, [
        $_SESSION["cid"],
        $sgid
    ]);
    if (pg_num_rows($r) <= 0) {
        return ("\n");
    }

    $retStr = "items: [";
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $arr = pg_fetch_assoc($r, $i);
        $retStr .= "{";
        $retStr .= "id: ".(++$itemIndex).", text: '" . $arr["protocol"] . " ポート:" . $arr["fromport"];
        if ($arr["fromport"] != $arr["toport"]) {
            $retStr .= "-" . $arr["toport"];
        }
        $retStr .= "',";//note
        $retStr .= "icons: {folder_opened: \"fa-share-alt\", folder_closed: \"fa-share-alt\"},";
        $retStr .= "items: [";
        $arrCur = $arr;
        for (; $i < pg_num_rows($r); $i++) {
            $arr = pg_fetch_assoc($r, $i);
            if ($arr["fromport"] == "") {
                $arr["fromport"] = 0;
            }
            if ($arrCur["protocol"] == $arr["protocol"] &&
                $arrCur["fromport"] == $arr["fromport"] && $arrCur["toport"] == $arr["toport"]) {
                $dispText = sprintf("%s %s", $arr["iprange"], empty($arr["obj_name"]) ?
                    "" : "({$arr['obj_name']})");
                $retStr .= "{";
                $retStr .= "id: ".(++$itemIndex).", text: '" . $dispText . "',";
                $retStr .= "icons: {file: \"fa-sitemap\"},";
                $retStr .= "},";
            } else {
                break;
            }
        }
        $retStr .= "],";//note
        $retStr .= "},";
        if ($i < pg_num_rows($r)) {
            $i--;
        }
    }
    $retStr .= "],";//note
    raise_sql($logs, 'inst');

    return ($retStr);
}

function getListINstance($smarty, $cid, $conditions)
{

    $sql = "
        SELECT
            e.*
        FROM (
            SELECT
                c.*,
                d.device,
                d.size,
                d.volid,
                d.voltype,
                d.tag_name,
                d.snapshotid,
                d.createtime,
                d.attachtime,
                i.using_from_date,
                i.using_till_date
            FROM (
                SELECT
                    a.cid,
                    a.id instid,
                    a.name instname,
                    type,
                    platform,
                    state,
                    b.name sgname,
                    b.id sgid,
                    availabilityzone,
                    launchtime,
                    privatednsname,
                    publicdnsname,
                    privateip,
                    publicip,
                    kernelid,
                    ramdiskid,
                    imageid,
                    a.pcode,
                    a.inst_req_id
                FROM
                    instance a,
                    sg b
                WHERE
                    a.cid = {$cid}
                    AND b.cid = {$cid}
                    AND a.sg = b.id
                ORDER BY
                    a.name,
                    a.id
            ) c
            LEFT OUTER JOIN
                volumes d
                ON (c.instid = d.instid
                AND d.cid = {$cid}
                AND d.stragetype = 'EBS')
            LEFT JOIN inst_req i
                ON i.id = c.inst_req_id
            ORDER BY
                c.instname,
                c.instid
        ) e
        $conditions
        ORDER BY e.instid
    ";
    $r = pg_query($smarty->_db, $sql);
    return $r;
}
