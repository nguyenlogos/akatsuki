<?php
$msg = "";
$err = 0;
$cid = sesreq("cid");
$uid = sesreq("uid");
$infoid = getreq("ID");
$currentPage = (int)getreq("p");
$CID_IN = $_SESSION["cid"];
$UID_IN = $_SESSION["uid"];

if ($currentPage <= 0) {
    $currentPage = 1;
}
$currentPageOffset = ($currentPage - 1) * DEFAULT_COUNTPAGE_EMP;
$paginationStr = "";
$selectedStatus = getreq("status");
if ($selectedStatus == '') {
    $selectedStatus = '0'; // default is unread
}
$htmlStr = '';
$recCount = 0;
$midokuCount = 0;
$paginationStr = '';

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('selectedStatus', $selectedStatus);
$smarty->assign('infoid', $infoid);
$smarty->assign('paginationStr', '');
if ($infoid == "") {
    $conditions = '';
    if ($selectedStatus == '0') {
        $conditions = "b.readdate IS NULL";
    } elseif ($selectedStatus == '1') {
        $conditions = "b.readdate IS NOT NULL";
    }

    if ($conditions) {
        $conditions = " AND {$conditions}";
    }

    $sqlTemplate = "
        SELECT %s
        FROM info a
            LEFT JOIN tbl_notif t 
                ON t.id = a.id_notif
            LEFT OUTER JOIN info_ctl b
                ON ( b.cid = {$cid}
                AND b.uid = '{$uid}'
                AND a.mid = b.mid)
        WHERE (
            ( a.toall IS NOT NULL )
            OR
            ( b.cid = {$cid} AND b.uid = '{$uid}')
        )
            AND (a.id_notif IS NULL
                OR (a.id_notif IS NOT NULL
                    AND (t.notif_time <= NOW() AND t.publish_date >= NOW())
                )
            )
        $conditions
    ";

    $orderBy = " ORDER BY a.senddate DESC ";

    // count total record (used for paging)
    $sql = sprintf($sqlTemplate, "count(*) AS num_rows");
    $r = pg_query($smarty->_db, $sql);
    $dataCount = (int)pg_fetch_result($r, 0, 0);

    // get data by page & offset
    $fields = "
        a.mid,
        a.RANK,
        a.senddate,
        a.title,
        a.message,
        a.toall,
        b.uid,
        b.readdate,
        t.notif_time
    ";

    $sql = sprintf($sqlTemplate, $fields);
    $sql .= $orderBy;
    $sql .= sprintf("LIMIT %d OFFSET %d", DEFAULT_COUNTPAGE_EMP, $currentPageOffset);
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $recCount = pg_num_rows($r);
    $htmlStr = " ";
    for ($i = 0; $i < $recCount; $i++) {
        // 0:a.mid, 1:a.rank, 2:a.senddate, 3:a.title, 4:a.message, 5:a.toall
        // 6:b.uid, 7:b.readdate

        $row = pg_fetch_assoc($r, $i);
        $opened = '';
        if (!empty($row['readdate'])) {
            $opened = "opened";
        }
        $rankedClass = '';
        switch ($row['rank']) {
            case '1':
                $rankedClass = 'check-circle text-info';
                break;
            case '2':
                $rankedClass = 'exclamation-triangle text-warning';
                break;
            default:
                $rankedClass = 'bell text-danger';
                break;
        }
        $sendDateFm = dateCool(!empty($row['notif_time']) ? $row['notif_time'] : $row['senddate']);
        $readDateFm = dateCool($row['readdate']);

        $htmlStr .= "
            <li>
                <a class='list-group-item {$opened}' href='javascript:void(0)'
                   onclick='showMessage({$row["mid"]}, this)'>
                    <p><i class=\"fas fa-{$rankedClass}\"></i>{$row["title"]}</p>
                    <ul class='no-disc-list'>
                        <li>投稿日時：{$sendDateFm}</li>
                        <li>開封日時：{$readDateFm}</li>
                    </ul>
                </a>
            </li>
        ";
    }
    if ($dataCount > 0) {
        $paginationStr = getPagenationStr(
            $smarty,
            $dataCount,
            DEFAULT_COUNTPAGE_EMP,
            $currentPage,
            "/cons/info.php?status={$selectedStatus}&p="
        );
    }
} else {
    // show notification details
    $ID = $infoid;
    if (is_numeric($ID) == false) {
        $msg = "パラメタに誤りがあります。<br />ログインしなおしてください。<br /><br /><a href='/'>ログイン画面</a>";
        logout();
        $smarty->assign('msg', $msg);
        $smarty->assign('viewTemplate', 'error.tpl');
        $smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
        exit;
    }

    $sql = "
        SELECT
            a.mid,
            a.RANK,
            a.senddate,
            a.title,
            a.message,
            a.toall,
            b.uid,
            b.readdate
        FROM info a
            LEFT JOIN info_ctl b
                ON (a.mid = b.mid
                AND b.cid = {$cid}
                AND b.uid = '{$uid}')
        WHERE
            a.mid = {$ID}
            AND
            (
                a.toall IS NOT NULL
                OR
                (
                    b.cid = {$cid} AND b.uid = '{$uid}'
                )
            )
        ORDER BY
            a.senddate DESC
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    if (pg_num_rows($r) !== 1) {
        $msg = "パラメタに誤りがあります。(ILID)<br />ログインしなおしてください。<br /><br /><a href='/'>ログイン画面</a>";
        logout();
        $smarty->assign('msg', $msg);
        $smarty->assign('viewTemplate', 'error.tpl');
        $smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
        exit;
    }

    $row = pg_fetch_row($r, 0);
    // 既読にする
    $sql = "BEGIN;";
    // $r = pg_query($smarty->_db, $sql);

    $sql = "SELECT readdate FROM info_ctl WHERE cid = $1 AND uid = $2 AND mid = $3";
    $logs[] = $sql;
    $r = pg_query_params($smarty->_db, $sql, [
        $CID_IN,
        $UID_IN,
        $ID
    ]);
    if (pg_num_rows($r) == 1) {
        $sql = "
            UPDATE info_ctl
            SET readdate = now()
            WHERE cid = " . $CID_IN . " AND uid = '" . $UID_IN . "' AND mid = " . $ID;
        $logs[] = $sql;
    } else {
        $sql = "
            INSERT INTO info_ctl (cid, uid, mid, readdate)
            VALUES (" . $CID_IN . ",'" . $UID_IN . "'," . $ID . ",now())
        ";
        $logs[] = $sql;
    }
    $r = pg_query($smarty->_db, $sql);

    $sql = "COMMIT;";
    $r = pg_query($smarty->_db, $sql);

    $titleHTML = "<i class=\"fas fa-";
    if ($row[1] == 1) {
        $titleHTML.="check-circle text-info";
    } elseif ($row[1] == 2) {
        $titleHTML.= "exclamation-triangle text-warning";
    } else {
        $titleHTML.= "bell text-danger";
    }
    $titleHTML .= "\" ></i >" . $row[3];
    $senddate = dateCool($row[2]);
    $msgBody = $row[4];
    if (getreq('ajax') == '1') {
        $str = "
            <p class='mb-1'>
                <span>配信日時：{$senddate}</span>
            </p>
            <div class='jumbotron'>
                <p class='h3'>
                    {$titleHTML}
                </p>
                <p class='mb-0'>{$msgBody}</p>
            </div>
        ";
        echo $str;
        exit();
    }
    $smarty->assign('titleHTML', $titleHTML);
    $smarty->assign('msgBody', $msgBody);
    $smarty->assign('readdate', dateCool($row[7]));
}

updateInfoCount($smarty->_db);
raise_sql($logs, 'info');

$smarty->disconnect();

$smarty->assign('htmlStr', $htmlStr);
$smarty->assign('midokuCount', $_SESSION['infocount']);

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('paginationStr', $paginationStr);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('pageIcon', 'glyphicon-comment');
$smarty->assign('pageTitle', 'お知らせ');
$smarty->assign('viewTemplate', 'cons/info.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
