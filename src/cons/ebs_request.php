<?php
use Akatsuki\Models\InstReq;
use Common\Mailer;

$msg = "";
$errmsg = "";
$frmValues = [
    'action' => postreq('action'),
    'reqid'  => (int)postreq('reqid'),
    'f_type' => getreq('f_type'),
    'f_approved' => getreq('f_approved') == 'on' ? 1 : 0,
];
// TODO: check permissions
$requestMethod = $_SERVER['REQUEST_METHOD'];
if ($requestMethod === 'POST') {
    $pageAction = '';
    $mailSubject = '';
    $mailBody = '';
    $emailConfig = \Akatsuki\Models\Configs::getConfig('email_instance');
    if ($frmValues['reqid'] > 0) {
        $request = \Akatsuki\Models\EbsReq::where('cid', $_SESSION['cid'])
            ->where('id', $frmValues['reqid'])->first();
        if (!$request) {
            $errmsg = MESSAGES['ERR_DATA_NOT_FOUND'];
        }
    };
    if (!$errmsg && $request) {
        switch ($frmValues['action']) {
            case 'ebs_reject':
                $request->status = EBS_REQ_REJECTED;
                $result = $request->saveWithoutEvents(function() use (&$request){
                    return $request->save();
                });
                if ($result) {
                    $mailSubject = '[Sunny View][EBS] Your EBS request has been rejected.';
                    $mailBody = '[EBS] Your EBS request has been rejected.';
                }
                break;
            case 'ebs_approve':
                $request->status = EBS_REQ_APPROVED;
                $result = $request->saveWithoutEvents(function() use (&$request){
                    return $request->save();
                });
                if ($result) {
                    $mailSubject = '[Sunny View][EBS] Your EBS request has been approved.';
                    $mailBody = '[EBS] Your EBS request has been approved.';
                }
                break;
            case 'ebs_delete':
                $result = $request->delete();
                break;
            default:
                break;
        }

        if ($emailConfig && $mailSubject) {
            $userInfo = \Akatsuki\Models\Emp::select('email')
                ->where("cid", $request->cid)
                ->where('empid', $request->empid)->first()->toArray();

            $mailer = new Mailer();
            $mailer
                ->set('subject', $mailSubject)
                ->set('to', $userInfo['email'])
                ->set('body', $mailBody);
            // 「管理者」または「マネージャ」のBCCメール
            $mailer->setBcc($smarty->_db, $userInfo['email']);

            $mailer->send();
        }

        $infoType = $frmValues['action'] === 'req_delete' ? 'DELETE' : 'UPDATE';
        if ($request) {
            $msg = MESSAGES['INF_'.$infoType];
        } else {
            $errmsg = MESSAGES['ERR_'.$infoType];
        }
    }
}
$deptRoles = permission_check("mst/dept.php");
$pageRoles = permission_check("cons/request.php");
$whereConditions = [
    "er.cid = {$_SESSION["cid"]}",
];
if ($frmValues['f_approved'] == 1) {
    $whereConditions[] = "er.status = 1";
} else {
    $whereConditions[] = "er.status = 0";
}
if (!$deptRoles['read']['allowed']) {
    $whereConditions[] = "d.dept = {$_SESSION['dept']}";
}

if (!$pageRoles['approve_delete']['allowed']) {
    $whereConditions[] = "er.empid = {$_SESSION['empid']}";
}
$whereConditions = implode(" AND ", $whereConditions);

$sql = "
    SELECT
        er.id,
        er.cid,
        er.empid,
        er.instance_id as instid,
        er.status,
        e.name AS empname,
        d.deptname,
        to_char(er.updated_at,'YYYY/MM/DD HH24:MI:SS') AS updated_at,
        i.pcode
    FROM
        ebs_req er
    INNER JOIN instance i
        ON i.cid = er.cid
        AND i.id = er.instance_id
    LEFT JOIN
        emp e
        ON e.cid = er.cid
            AND e.empid = er.empid
            AND e.status = 0
    INNER JOIN (
        SELECT erd.ebs_req_id
        FROM ebs_req_detail erd
        GROUP BY erd.ebs_req_id
    ) erd
        ON erd.ebs_req_id = er.id
    LEFT JOIN
        dept d
        ON d.cid = e.cid
            AND d.dept = e.dept
            AND d.status = 0
    WHERE {$whereConditions}
";

$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$reqList = pg_fetch_all($r);
if (!$reqList) {
    $reqList = [];
}
$headerList = [
    'id' => [
        'disp_name' => '申請ID',
        'data_attr' => true,
        'sortable'  => true
    ],
    'instid' => [
        'disp_name' => 'インスタンスID',
        'data_attr' => true
    ],
    'updated_at' => [
        'disp_name' => '申請日時',
        'sortable'  => true
    ],
    'deptname' => [
        'disp_name' => '所属ｸﾞﾙｰﾌﾟ',
        'sortable'  => true
    ],
    'empname' => [
        'disp_name' => '申請者',
        'sortable'  => true
    ],
    'status' => [
        'disp_name' => '',
        'data_attr' => true
    ],
    'pcode' => [
        'disp_name' => '',
        'data_attr' => true
    ],
];
$btnTemplate = '
    <button type="button" class="btn btn-sm mt-1 $btnClass btn-ebs">
        <i class="fas $btnIcon"></i>$btnName
    </button>';
$actionList = [
    [
        'template'  => $btnTemplate,
        '$btnName'  => '承認',
        '$btnClass' => 'btn-blue btn-approve',
        '$btnIcon'  => 'fa-thumbs-up',
        'width'     => 100,
    ],
    //[
    //    'template'  => $btnTemplate,
    //    '$btnName'  => '却下',
    //    '$btnClass' => 'btn-danger btn-reject',
    //    '$btnIcon'  => 'fa-thumbs-down',
    //    'width'     => 100,
    //]
];

$smarty->assign('reqList', $reqList);
$smarty->assign('headerList', $headerList);
$smarty->assign('actionList', $actionList);
$smarty->assign('frmValues', $frmValues);

$smarty->assign('paginationStr', $paginationStr);
$smarty->assign('sortIndex', $sortIndex);

$smarty->assign('pageTitle', '申請一覧');
$smarty->assign('msg', $msg);
$smarty->assign('errmsg', $errmsg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'cons/ebs_request.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
