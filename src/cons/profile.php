<?php
use Akatsuki\Models\Users;
use Akatsuki\Models\Emp;
use Akatsuki\Models\Plan;

$msg = "";
$err = 0;
$cid = "";
$cid = (int)$_SESSION["cid"];

if (postreq("button") == "保存") {
    // 必須事項が入力されているか確認
    $name = htmlspecialchars(postreq("name"));
    $company = htmlspecialchars(postreq("company"));
    $tel = htmlspecialchars(postreq("tel"));
    $dept = htmlspecialchars(postreq("dept"));
    $position = htmlspecialchars(postreq("position"));
    $uid = htmlspecialchars(postreq("uid"));
    $plan = postreq("plan");

    $errField = "";
    if ($name == "") {
        $errField .= "[お名前]";
    }
    if ($company == "") {
        $errField .= "[会社名]";
    }
    if ($tel == "") {
        $errField .= "[電話番号]";
    }
    if ($uid == "") {
        $errField .= "[メールアドレス]";
    }

    if ($errField != "") {
        $msg = $errField . "が入力されていません。";
        $err = 1;
    }

    if ($err == 1) {
        // エスケープはtpl側で実施
        $smarty->assign('name', $name);
        $smarty->assign('company', $company);
        $smarty->assign('dept', $dept);
        $smarty->assign('position', $position);
        $smarty->assign('tel', $tel);
        $smarty->assign('uid', $uid);

        $smarty->assign('err', $err);
        $smarty->assign('msg', $msg);
        $smarty->assign('userID', $_SESSION['uid']);
        $smarty->assign('pageIcon', 'glyphicon-user');
        $smarty->assign('pageTitle', 'ご契約窓口のプロファイル');
        $smarty->assign('viewTemplate', 'cons/profile.tpl');
        $smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
        exit;
    }

    $PlansUp = Plan::where('cid', $cid)
                   ->where('startuid', $uid)
                   ->first();
    if ($PlansUp) {
        $PlansUp->setValues([
            'plan'    => $plan == 1 ? 1 : 0,
            'enddate' => "now()",
        ]);
    } else {
        $PlansUp = new Plan();
        $PlansUp->setValues([
            'cid'         => $cid,
            'plan'        => $plan == 1 ? 1 : 0,
            'startdate'   => "now()",
            'startuid'    => $uid,
        ]);
    }

    $PlansUp->save();

    // 登録処理
    $user = Users::where('cid', $cid)->first();
    $checkUid = Users::where('uid', $uid)
        ->where('status', 0)
        ->first();
    $checkEmail = Emp::where('email', $uid)
        ->where('status', 0)
        ->first();
    if ($user && ($_SESSION['originuid'] == $uid || (!$checkUid && !$checkEmail))) {
        $user->setValues([
            'uid'         => $uid,
            'name'        => $name,
            'companyname' => $company,
            'dept'        => $dept ?: null,
            'position'    => $position  ?: null,
            'tel'         => $tel ?: null,
        ]);
        $r = $user->save();
        $_SESSION['originuid'] = $uid;
    } else {
        $r = false;
    }

    if ($r) {
        $check_lock = $plan ? 'checked' : '';
        $msg = "変更しました。";
        $err = 0;
    } else {
        $msg = MESSAGES['ERR_UPDATE'];
        $err = 1;
    }
} else {
// ボタンが押されていない場合
    $sql = "SELECT 
                u.name, u.tel, u.dept, u.companyname, u.position, u.uid, p.plan
            FROM users u
            LEFT JOIN plan p ON p.startuid = u.uid AND p.cid = ".$cid." 
            WHERE u.cid = ".$cid." AND u.status = 0";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r) != 1) {
        $msg = "システムエラーが発生しました。(NOT1U)";
        $err = 1;
    } else {
        $row = pg_fetch_row($r, 0);

        $check_lock = '';
        if ($row[6]  == 1) {
            $check_lock = 'checked';
        }

        $name = $row[0];
        $tel = $row[1];
        $dept = $row[2];
        $company = $row[3];
        $position = $row[4];
        $_SESSION['originuid'] = $uid = $row[5];
        $plan = $row[6];
    }
}
raise_sql($logs, 'profile');
$smarty->disconnect();

$smarty->assign('name', $name);
$smarty->assign('company', $company);
$smarty->assign('dept', $dept);
$smarty->assign('position', $position);
$smarty->assign('tel', $tel);
$smarty->assign('uid', $uid);
$smarty->assign('plan', $plan);
$smarty->assign('check_lock', $check_lock);

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('pageIcon', 'glyphicon-user');
$smarty->assign('pageTitle', 'ご契約窓口のプロファイル');
$smarty->assign('viewTemplate', 'cons/profile.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
