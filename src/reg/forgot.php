<?php
use Common\Mailer;
use Akatsuki\Models\Emp;

$requestMethod = $_SERVER['REQUEST_METHOD'];
if ($requestMethod === 'POST') {
    $email = postreq("email");
    $emp = Emp::where("email", $email)
            ->where("status", 0)
            ->first();
    if (empty($emp)) {
        errorScreen($smarty, "入力されたメールアドレスは登録されていません。");
        exit();
    }
    $resetToken = $emp->generateResetToken();

    $mailer = new Mailer();
    $mailSubject = "[SunnyView] パスワードの再設定";
    $mailBody = "パスワードを再設定するには、下記のリンクをクリックして画面に表示される手順を行ってください。\n\nhttp://52.68.30.203/reg/reset.php?k=" . $resetToken;
    $mailer
        ->set('subject', $mailSubject)
        ->set('to', $email)
        ->set('body', $mailBody);
    $result = $mailer->send();
    infoScreen($smarty, "メールを送信しました。<br />メールに記載されている手順に沿って、手続きを完了させてください。");
    exit();
}
$smarty->assign('viewTemplate', 'reg/forgot.tpl');
$smarty->display(TEMPLATES_PATH.'pagelayout.tpl');
