<?php
use Akatsuki\Models\Emp;

$sql = 'SELECT
          admin_active
        FROM configs';
$r = pg_query($smarty->_db, $sql);
$row = pg_fetch_array($r, null, PGSQL_ASSOC);

$regKey = getreq('k');
if (empty($regKey)) {
    errorScreen($smarty, '登録キーが不正です。(-1)');
    exit();
};

$emp = Emp::where('regkey', $regKey)
        ->where('email_confirmed', 0)
        ->where('status', 0)
        ->first();
if (!$emp) {
    errorScreen($smarty, '登録キーが不正です。(-2)');
    exit();
};

$regdate = strtotime($emp->regdate);
if (time() > $regdate + SETTING_DEFAULT_TOKEN_EXPIRATION) {
    errorScreen($smarty, '登録キーの有効期限が切れています。');
    exit();
}

$emp->setValues([
    'email_confirmed' => 1,
    'regkey' => null
]);
$result = $emp->save();
if ($result) {
    $emp->createConfigDefault();
}
infoScreen($smarty, 'メールアドレスの確認が完了しました。<br />ログイン画面からログインしてください。');
