<?php
defined("COMMON_PATH") OR define("COMMON_PATH", dirname(__DIR__) . "/common");
require_once(COMMON_PATH . "/CommonClass.class.php");
require_once(COMMON_PATH . "/common.php");

$smarty = new MySmarty(false);

//共通情報をセット
$smarty->commonInfoSet();

// ログイン情報の設定
// $loginInfo = setAuthInfo($smarty->_db);

$msg = "";
$err = 0;


if(getreq("mail") == "" || getreq("k") == "") {
  $msg = "エラーが発生しました。パラメタが不正です。";
  $err = 1;
} else {
  $sql = sprintf("SELECT uid
        FROM users
        WHERE uid = '" . pg_escape_string(getreq("mail")) . "'
            AND regkey = '" . pg_escape_string(getreq("k")) . "'
            AND status = 1");
    $logs[] = $sql;
  $r = pg_query($smarty->_db, $sql);
  if( pg_num_rows($r) <= 0) {
    $msg = "エラーが発生しました。再度、登録しなおしてみてください。(NOREC)";
    $err = 1;
  } else {
    // cidの取得
    $sql = sprintf("SELECT cid
                FROM users
                WHERE uid = '" . pg_escape_string(getreq("mail")) . "'
                    AND regkey = '" . pg_escape_string(getreq("k")) . "'
                    AND status = 1");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    if(pg_num_rows($r) < 1) {
      $msg = "エラーが発生しました。再度、登録しなおしてみてください。(NOCID)";
      $err = 1;
    } else {
      $cid = pg_fetch_result($r, 0, 0);

      $sql = "update users set status = 0 where uid = '" . pg_escape_string(getreq("mail")) . "' and regkey = '" . pg_escape_string(getreq("k")) . "' and status = 1";
      $logs[] = $sql;
      $r = pg_query($smarty->_db, $sql);

      $sql = "update emp set status = 0 where cid = " . $cid . " and email = '" . pg_escape_string(getreq("mail")) . "' and status = 1";
      $logs[] = $sql;
      $r = pg_query($smarty->_db, $sql);
      $msg = "登録が完了しました。<br />ログイン画面からログインしてください。";
      $err = 0;
    }
  }
}
raise_sql($logs, 'reg2');

$smarty->disconnect();
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);

$smarty->assign('viewTemplate', 'reg/reg2.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');