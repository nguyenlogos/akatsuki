<?php
use Common\Mailer;

$msg = "";
$cid = "";
$err = 0;
$name_err = "";
$tel_err = "";
$company_err = "";
$pass_err = "";
$policy_pass = "";
$pass_confirm_err = "";
$agree_err = "";
$email_err = "";
$email_confirm_err = "";
$requestMethod = $_SERVER['REQUEST_METHOD'];
$cid = (int)$_SESSION["cid"];

$sql = 'SELECT
          pass_lenght,
          pass_expired,
          pass_lowercase,
          pass_number
        FROM configs';
$r = pg_query($smarty->_db, $sql);
$row = pg_fetch_array($r, null, PGSQL_ASSOC);

if (!$cid) {
    if ($requestMethod === "POST") {
        $captchaResponse = postreq("g-recaptcha-response");
        if (!g_captcha_verify($captchaResponse)) {
            $err = 1;
            $msg = MESSAGES['ERR_CAPTCHA_VERIFY'];
        }
        // 必須事項が入力されているか確認
        $name = htmlspecialchars(trim(postreq("name")));
        $company = htmlspecialchars(trim(postreq("company")));
        $tel = postreq("tel");
        $email = postreq("email");
        $email2 = postreq("email2");
        $pass = postreq("pass");
        $pass2 = postreq("pass2");
        $agree = postreq("agree");
        $dept = htmlspecialchars(trim(postreq("dept")));
        $position = htmlspecialchars(trim(postreq("position")));

        // Check validate form sign up
        if (empty(trim($name))) {
            $name_err = "お名前は必須です。";
            $err = 1;
        }

        if (empty(trim($tel))) {
            $tel_err = "電話番号は必須です。";
            $err = 1;
        }

        if (empty(trim($company))) {
            $company_err = "会社名は必須です。";
            $err = 1;
        }

        if (empty(trim($email)) || empty(trim($email2))) {
            $email_err = "メールアドレスは必須です。";
            $err = 1;
        } elseif ($email !== $email2) {
            $email_confirm_err = "メールアドレス(再入力)が正しくありません。";
            $err = 1;
        }

        if (empty(trim($pass)) || empty(trim($pass2))) {
            $pass_err = "パスワードメールアドレスは必須です。";
            $err = 1;
        } elseif ($pass !== $pass2) {
            $pass_confirm_err = "パスワード(再入力)が正しくありません。";
            $err = 1;
        }

        if (empty(trim($agree))) {
            $agree_err = "利用規約への同意は必須です。";
            $err = 1;
        }
        // メールアドレスの重複チェック
        $user = \Akatsuki\Models\Users::where("uid", $email)->first();
        $emp = \Akatsuki\Models\Emp::where("email", $email)->first();
        $sql = sprintf("SELECT uid
            FROM users
            WHERE uid = $1 AND status = 0;");
        $logs[] = $sql;
        $r = pg_query_params($smarty->_db, $sql, [
            pg_escape_string($email),
        ]);
        if ($user || $emp) {
            if (($user && $user->status == 0) || ($emp && $emp->status == 0)) {
                $err = 1;
                $email_err = '指定されたメールアドレスは、すでに登録されています。';
            }
        }
        if (!$err) {
            $emp && $emp->delete();
            $user && $user->delete();
            // 登録手続き中の同じメールアドレスがある場合は、cid,regkeyを取得して削除
            $sql = sprintf("SELECT cid,regkey
                    FROM users
                    WHERE uid = $1 AND status = 1;");
            $logs[] = $sql;
            $r = pg_query_params($smarty->_db, $sql, [
                pg_escape_string($email),
            ]);
            if (pg_num_rows($r) > 0) {
                $cid = pg_fetch_result($r, 0, 0);
                $regkey = pg_fetch_result($r, 0, 1);

                $sql = "DELETE FROM users WHERE cid=" . $cid;
                $logs[] = $sql;
                $r = pg_query($smarty->_db, $sql);
                $sql = "DELETE FROM emp WHERE cid=" . $cid;
                $logs[] = $sql;
                $r = pg_query($smarty->_db, $sql);
                $sql = "DELETE FROM dept WHERE cid=" . $cid;
                $logs[] = $sql;
                $r = pg_query($smarty->_db, $sql);
            } else {
                // regkeyの決定
                $regkey = random_string();
                // cidの決定
                $sql = "select max(cid) from users;";
                $logs[] = $sql;
                $r = pg_query($smarty->_db, $sql);
                if (pg_num_rows($r) <= 0) {
                    $cid = 1;
                } else {
                    $cid = pg_fetch_result($r, 0, 0) + 1;
                }
            }

            // cidを念のためにempテーブルでも確認
            $sql = sprintf("select cid
                    from emp
                    where cid = " . $cid);
            $logs[] = $sql;
            $r = pg_query($smarty->_db, $sql);
            if (pg_num_rows($r) > 0) {
                // usersには登録されていないcidがempで使われていた場合
                $msg = "システムエラーが発生しました。(CID-INVALID cid:" . $cid . ")";
                $err = 1;
            } else {
                $validations = validate_password_policy($pass);
                if ($validations['err']) {
                    $policy_pass = implode("<br />", $validations['msgs']);
                    $err = 1;
                }

                if ($policy_pass == "" && $err == 0) {
                    // send email
                    $mailer = new Mailer();
                    $mailSubject = "【AWS管理コンソール】無料会員登録 確認メール";
                    $mailBody = "
                    無料会員登録をお申込みいただきましてありがとうございました。\n
                    下記URLをクリックして登録処理を完了させてください。\n\n
                    http://52.68.30.203/reg/confirm.php?k=" . $regkey . "\n\n
                    [重要]\n
                    お心当たりのない方は、申し訳ありませんが、上記をクリックせずに本メールを削除してください。\n\n
                    [お問い合わせ先] \n
                    株式会社アイディーエス\n
                    nakano@ids.co.jp\n
                    TEL.03-5484-7811";
                    $mailer
                        ->set('subject', $mailSubject)
                        ->set('to', $email)
                        ->set('body', $mailBody);
                    $result = $mailer->send();

                    // 登録処理
                    pg_query($smarty->_db, "BEGIN;");
                    $sql1 = "INSERT INTO users (uid,tel,regkey,regdate,cid,name,status,dept,companyname,position)
                        VALUES ('" . pg_escape_string($email) . "',
                        '" . pg_escape_string($tel) . "','" . $regkey . "',now(),
                        " . $cid . ",'" . pg_escape_string($name) . "',0,'" . pg_escape_string($dept) . "',
                        '" . pg_escape_string($company) . "','" . pg_escape_string($position) . "')";


                    if ($dept == "") {
                        $dept = "Default";
                    }
                    if ($dept != "") {
                        $sql2 = "INSERT INTO dept (cid, dept, deptname, regdate, status, disporder)
                        VALUES (" . $cid . ", 1,'" . pg_escape_string($dept) . "', now(),0,1)";
                        $r = pg_query($smarty->_db, $sql2);
                        $logs[] = $sql2;
                    }

                    $sql3 = "INSERT INTO emp (cid, empid, email, pass, dept, name,
                                        regkey, regdate, status, sysadmin, admin, disporder, login_initial)
                        VALUES (" . $cid . ",1,'" . pg_escape_string($email) . "',
                        '" . pg_escape_string(md5($pass)) . "',1,
                        '" . pg_escape_string($name) . "', '{$regkey}',now(),0,1,1,1,1)";
                    $r = true;
                    foreach ([$sql1, $sql3] as $sql) {
                        if (!$r) {
                            pg_query($smarty->_db, "ROLLBACK;");
                            break;
                        } else {
                            $r = pg_query($smarty->_db, $sql);
                            $logs[] = $sql;
                        }
                    }
                    if (!$r) {
                        pg_query($smarty->_db, "ROLLBACK;");
                    } else {
                        pg_query($smarty->_db, "COMMIT;");
                    }


                    if (!$r) {
                        $msg = MESSAGES['ERR_DELETE'];
                        $err = 1;
                    } else {
                        $msg = MESSAGES['POLICY_SIGN_UP'];
                        $err = 2;
                        insertLog(
                            $smarty,
                            "ユーザ情報「" . pg_escape_string($name) . "(ID:" . pg_escape_string($email) . ")」を削除しました。"
                        );
                    }
                }
            }
        }
    }
} else {
    rd("cons/info.php");
}

raise_sql($logs, 'reg');

$smarty->disconnect();
$smarty->assign('name', htmlspecialchars($name));
$smarty->assign('company', htmlspecialchars($company));
$smarty->assign('dept', htmlspecialchars($dept));
$smarty->assign('position', htmlspecialchars($position));
$smarty->assign('tel', htmlspecialchars($tel));
$smarty->assign('email', htmlspecialchars($email));
$smarty->assign('email2', htmlspecialchars($email2));
$smarty->assign('pass', htmlspecialchars($pass));
$smarty->assign('pass2', htmlspecialchars($pass2));
if ($agree != "") {
    $smarty->assign('agree', " checked");
} else {
    $smarty->assign('agree', "");
}
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('name_err', $name_err);
$smarty->assign('tel_err', $tel_err);
$smarty->assign('company_err', $company_err);
$smarty->assign('pass_err', $pass_err);
$smarty->assign('policy_pass', $policy_pass);
$smarty->assign('pass_confirm_err', $pass_confirm_err);
$smarty->assign('agree_err', $agree_err);
$smarty->assign('email_err', $email_err);
$smarty->assign('email_confirm_err', $email_confirm_err);
$smarty->assign('G_RECAPTCHA_SITE_KEY', G_RECAPTCHA_SITE_KEY);
$smarty->assign('pageTitle', 'サインアップ');
$smarty->assign('viewTemplate', 'reg/reg.tpl');
$smarty->display(TEMPLATES_PATH.'pagelayout.tpl');
