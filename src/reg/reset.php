<?php
use Akatsuki\Models\Emp;

$errmsg = "";

$token = getreq('k');
if (empty($token)) {
    errorScreen($smarty, 'Invalid token');
    exit();
};

$emp = Emp::where('pw_reset_token', $token)
        ->where('status', 0)
        ->first();
$pass_old = $emp->pass;

if (!$emp) {
    errorScreen($smarty, MESSAGES['ERR_PASSWORD_TOKEN_INVALID']);
    exit();
};
$expirationPeriod = (int)\Akatsuki\Models\Configs::getConfig("admin_active");
if (!$expirationPeriod) {
    $expirationPeriod = SETTING_DEFAULT_TOKEN_EXPIRATION;
}
if ($expirationPeriod > 0) {
    $tokenTime = strtotime($emp->pw_reset_token_time);
    if (time() > $tokenTime + $expirationPeriod) {
        errorScreen($smarty, MESSAGES['ERR_PASSWORD_TOKEN_EXPIRED']);
        exit();
    }
}

$requestMethod = $_SERVER['REQUEST_METHOD'];
if ($requestMethod === 'POST') {
    $password = postreq("pass");
    $passwordConfirmed = postreq("pass2");
    $pass_voice = md5($password);

    if (empty($password)) {
        $errmsg .= MESSAGES['ERR_PASSWORD_POLICY_EMPTY'];
    } elseif ($pass_voice == $pass_old) {
        $errmsg .= MESSAGES['ERR_PASSWORD_POLICY_DIFF'];
    } elseif ($password != $passwordConfirmed) {
        $errmsg = MESSAGES['ERR_PASSWORD_MISMATCHED'];
    } else {
        $validations = validate_password_policy($password, $emp->cid);
        if ($validations['err']) {
            $errmsg .= implode("<br />", $validations['msgs']);
        }
    }
    if (!$errmsg) {
        $result = $emp->resetPassword($password);
        infoScreen($smarty, "パスワードを変更しました。");
        exit();
    }
}
$smarty->assign('errmsg', $errmsg);
$smarty->assign('viewTemplate', 'reg/reset.tpl');
$smarty->display(TEMPLATES_PATH.'pagelayout.tpl');
