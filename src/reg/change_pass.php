<?php
use Akatsuki\Models\Emp;

$cid = '';
$email = '';
$errmsg = '';
$err = '';
$pass_old = '';
$cid = (int)$_SESSION["cid"];
$email = $_SESSION["uid"];

if (!$cid) {
    $emp = Emp::where('email', $email)
        ->where('status', 0)
        ->first();
    if ($emp) {
        if ($emp->login_initial == 0) {
            $errmsg = MESSAGES['POLICY_PASS_INITIAL'];
        } else {
            $pass_expired = (int)\Akatsuki\Models\Configs::getConfig('pass_expired', $emp->cid);
            if ($pass_expired > 0) {
                $regdate = strtotime($emp->regdate);
                if (time() > $regdate + $pass_expired) {
                    $errmsg = MESSAGES['ERR_PASSWORD_POLICY_EXPIRED'];
                }
            }
        }
    } else {
        rd('./');
    }

    $requestMethod = $_SERVER['REQUEST_METHOD'];
    if ($requestMethod === 'POST') {
        $password = postreq("pass");
        $passwordConfirmed = postreq("pass2");
        $pass_voice = md5($password);

        $errmsg = "";
        if (empty($password)) {
            $errmsg .= MESSAGES['ERR_PASSWORD_POLICY_EMPTY'];
        } elseif ($pass_voice == $emp->pass) {
            $errmsg .= MESSAGES['ERR_PASSWORD_POLICY_DIFF'];
        } elseif ($password != $passwordConfirmed) {
            $errmsg = MESSAGES['ERR_PASSWORD_MISMATCHED'];
        } else {
            $validations = validate_password_policy($password, $emp->cid);
            if ($validations['err']) {
                $errmsg .= implode("<br />", $validations['msgs']);
            }
        }
        if (!$emp) {
            rd('./');
        }
        if (!$errmsg) {
            $result = $emp->resetPassword($password);
            infoScreen($smarty, "". MESSAGES['INF_PASSWORD_UPDATE'] ."");
            exit();
        }
    }
} else {
    rd("cons/info.php");
}

$smarty->assign('errmsg', $errmsg);
$smarty->assign('viewTemplate', 'reg/change_pass.tpl');
$smarty->display(TEMPLATES_PATH.'pagelayout.tpl');
