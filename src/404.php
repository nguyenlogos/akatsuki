<?php
require_once('config/consts.php');

defined("COMMON_PATH") OR define("COMMON_PATH", SOURCE_PATH . "/common");

require_once(COMMON_PATH . "/CommonClass.class.php");
require_once(COMMON_PATH . "/common.php");

$smarty = new MySmarty();
$smarty->commonInfoSet();

$smarty->assign('viewTemplate', '404.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');