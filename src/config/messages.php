<?php
const MESSAGES = [
    'ERR_UNAUTHORIZED'          => "ログインしていません。",
    'ERR_DB_CONNECT'            => "データベースへの接続に失敗しました。",
    'ERR_PARAMS_MISSING'        => "パラメータが不足しています。",
    'ERR_METHOD_NOT_ALLOWED'    => "許可されていないメソッドです。",
    'ERR_DATA_NOT_FOUND'        => "データが見つかりません。",
    'ERR_PERMISSION_DENIED'     => "指定された操作は許可されていません。",
    'ERR_INVALID_EMAIL_ADDRESS' => "メールアドレスが不正です。",
    'ERR_UNKNOWN_ERROR'         => '不明なエラーが発生しました。',
    'ERR_DUPLICATE_ENTRY'       => 'すでに登録されています。',
    'ERR_CAPTCHA_VERIFY'        => 'Captcha "私はロボットではありません。"の認証に失敗しました。',
    'ERR_EC2_CLIENT'            => 'EC2クライアントの初期化に失敗しました。',
    'ERR_AWS_KEY'               => '<p class="mb-0">API認証情報が正しくない可能性があります。</p>
                                    <p class="mb-0">[設定]の[AWS API認証情報の設定] 画面から正しい値を設定してください。</p>',
    'ERR_AWS_API'               => 'AWS APIの実行に失敗しました。',
    'ERR_PASSWORD_TOKEN_INVALID'=> '不正なトークンです。',
    'ERR_PASSWORD_TOKEN_EXPIRED'=> '期限切れのトークンです。',
    'ERR_PASSWORD_MISMATCHED'   => '再入力したパスワードが一致していません。',
    'ERR_EBS_CREATE'            => 'EBSボリュームの作成に失敗しました。',
    'ERR_EBS_DETACH'            => 'EBSボリュームのデタッチに失敗しました。',
    'ERR_EBS_ATTACH'            => 'EBSボリュームのアタッチに失敗しました。',
    'ERR_ADDRESS_ASSOCIATE'     => 'Elastic IPアドレスの関連付けに失敗しました。',
    'ERR_ADDRESS_DISASSOCIATE'  => 'Elastic IPアドレスの関連付け解除に失敗しました。',
    'ERR_SG_NOT_EXIST'          => 'セキュリティグループが見つかりません。',
    'ERR_AWS_CREATE_SG'         => 'セキュリティグループの作成に失敗しました。',
    'ERR_AWS_DUPLICATE_SG'      => '同じ名前のセキュリティグループがすでに存在しています。',
    'ERR_AWS_DUPLICATE_SG_RULES'=> '同じルールのセキュリティグループがすでに存在しています。',

    'ERR_ELASTIC_IP_UNAVAILABLE'=> 'IPアドレスが利用可能ではありません。',

    'ERR_INST_ID_REQUIRED'      => "インスタンスIDが必要です。",
    'ERR_INST_START_STOP'       => "インスタンスの起動/停止に失敗しました。",
    'ERR_INST_UNAUTHORIZED'     => "このインスタンスへの変更を行う権限がありません。",

    'ERR_INST_REQ_INVALID_DEPT' => "このグループにはマネージャーが指定されていません。",
    'ERR_INST_REQ_START_FAILED' => "インスタンスの作成に失敗しました。",


    'ERR_OBJ_IP_INVALID'     => "IPアドレスが不正です。",
    'ERR_OBJ_IP_EMPTY'       => "IPアドレスレンジは省略できません。",
    'ERR_OBJ_SUBNET_INVALID' => "サブネットマスクが不正です。",
    'ERR_OBJ_SUBNET_EMPTY'   => "サブネットマスクは省略できません。",
    'INF_OBJ_INSERT'         => '新しいオブジェクトを登録しました。',
    'INF_OBJ_UPDATE'         => 'オブジェクト情報を変更しました。',

    'ERR_INSERT' => "失敗しました。",
    'ERR_UPDATE' => "失敗しました。",
    'ERR_DELETE' => "失敗しました。",
    'INF_INSERT' => "登録しました。",
    'INF_UPDATE' => "更新に成功しました。",
    'INF_DELETE' => "削除しました。",
    'INF_DELETE_EXCHANGERATE'   => "保存しました。",
    'FORMAT_PRICE_EXCHANGERATE' => "通貨の形式の誤りがありますので、再入力してください。",

    'INF_INST_REQ_APPROVED'     => "承認に成功しました。",
    'INF_INST_REQ_REJECTED'     => "更新に成功しました。",
    'INF_INST_REQ_DELETED'      => "削除しました。",
    'ERR_INST_REQ_APPROVED'     => "失敗しました。",
    'ERR_INST_REQ_REJECTED'     => "失敗しました。",
    'ERR_INST_REQ_DELETED'      => "失敗しました。",
    'INF_INST_REQ_DEL_REQUEST'  => "更新に成功しました。",
    'ERR_INST_REQ_DEL_REQUEST'  => "失敗",
    'INF_INST_REQ_PROCESSING'   => 'Your request is being processed.',

    'ERR_PASSWORD_POLICY_DIFF'      => "新しいパスワードは、これまでのパスワードと異なるものを指定してください。",
    'ERR_PASSWORD_POLICY_EMPTY'     => "パスワードは省略できません。",
    'ERR_PASSWORD_POLICY_NUMBER'    => "パスワードには1文字以上の数字が含まれている必要があります。",
    'ERR_PASSWORD_POLICY_UPPERCASE' => "パスワードには1文字以上の大文字アルファベットが含まれている必要があります。",
    'ERR_PASSWORD_POLICY_LOWERCASE' => "パスワードには1文字以上の小文字アルファベットが含まれている必要があります。",
    'ERR_PASSWORD_POLICY_LENGTH'    => "パスワードは{length}文字以上にする必要があります。",
    'ERR_PASSWORD_POLICY_EXPIRED'   => "パスワードの有効期限が切れました。パスワードを変更してください。",
    'INF_PASSWORD_UPDATE'           => "パスワードが変更されました。",

    'POLICY_PASS_INITIAL'       => "新しいパスワードを入力してください!",
    'POLICY_PASS_CONFIRMED'     => "パスワード(再入力)が正しくありません",
    'POLICY_SIGN_UP'            => "<p>登録はまだ完了していません。<br/>
                                        入力いただいたメールアドレス宛てに、確認メールを送信しました。<br/>
                                        メールの内容に記載された確認手順を実行してください。</p>

                                    <p class='mb-0'>しばらくたってもメールが届かない場合は、迷惑メールとして分類された可能性があります。<br/>
                                        お使いのメールソフトの迷惑メールフォルダなどをご確認ください。</p>",
    'POLICY_NET_MASK'           => "255.255.255.255",

    'KEYPAIR_ALREADY_EXISTS'      => '指定されたキーペア名はすでに使われています。',
    'KEYPAIR_CREATE_FAILED'       => 'キーペアの作成に失敗しました。',
    'KEYPAIR_UPDATE_SUCCESS'      => 'キーペアの更新に成功しました。',
    'KEYPAIR_UPDATE_UNSUCCESSFUL' => 'キーペアの更新に失敗しました。',

    'SECURITY_GROUP_ALREADY_EXISTS'       => '同じ名前のセキュリティグループが存在しています。',
    'SECURITY_GROUP_NAME_REQUIRED'        => 'セキュリティグループ名は省略できません。',
    'SECURITY_GROUP_DESCRIPTION_REQUIRED' => 'セキュリティグループの説明は省略できません。',
    'SECURITY_GROUP_REQUIRED'             => 'セキュリティグループは省略できません。',
    'SECURITY_GROUP_LIST_REQUIRED'        => 'セキュリティグループを選択してください。',
];
