<?php

const AWS_SG_STYPES = [
    [
        'name' => 'Custom TCP Rule',
        'protocol' => 'TCP',
        'portfrom' => '',
        'portto' => ''
    ],
    [
        'name' => 'Custom UDP Rule',
        'protocol' => 'UDP',
        'portfrom' => '',
        'portto' => ''
    ],
    [
        'name' => 'All TCP',
        'protocol' => 'TCP',
        'portfrom' => 0,
        'portto' => 65535
    ],
    [
        'name' => 'All UDP',
        'protocol' => 'UDP',
        'portfrom' => 0,
        'portto' => 65535
    ],
    [
        'name' => 'All ICMP - IPv4',
        'protocol' => 'icmp',
        'portfrom' => -1,
        'portto' => -1,
        'alias' => 'ICMP',
    ],
    [
        'name' => 'All ICMP - IPv6',
        'protocol' => 'icmpv6',
        'portfrom' => -1,
        'portto' => -1,
        'alias' => 'IPV6 ICMP',
    ],
    // [
    //     'name' => 'All traffic',
    //     'protocol' => '-1',
    //     'portfrom' => 0,
    //     'portto' => 65535
    // ],
    [
        'name' => 'SSH',
        'protocol' => 'TCP',
        'portfrom' => 22,
        'portto' => 22
    ],
    [
        'name' => 'SMTP',
        'protocol' => 'TCP',
        'portfrom' => 25,
        'portto' => 25
    ],
    [
        'name' => 'DNS (UDP)',
        'protocol' => 'UDP',
        'portfrom' => 53,
        'portto' => 53
    ],
    [
        'name' => 'DNS (TCP)',
        'protocol' => 'TCP',
        'portfrom' => 53,
        'portto' => 53
    ],
    [
        'name' => 'HTTP',
        'protocol' => 'TCP',
        'portfrom' => 80,
        'portto' => 80
    ],
    [
        'name' => 'POP3',
        'protocol' => 'TCP',
        'portfrom' => 110,
        'portto' => 110
    ],
    [
        'name' => 'IMAP',
        'protocol' => 'TCP',
        'portfrom' => 143,
        'portto' => 143
    ],
    [
        'name' => 'LDAP',
        'protocol' => 'TCP',
        'portfrom' => 389,
        'portto' => 389
    ],
    [
        'name' => 'HTTPS',
        'protocol' => 'TCP',
        'portfrom' => 443,
        'portto' => 443
    ],
    [
        'name' => 'SMB',
        'protocol' => 'TCP',
        'portfrom' => 445,
        'portto' => 445
    ],
    [
        'name' => 'SMTPS',
        'protocol' => 'TCP',
        'portfrom' => 465,
        'portto' => 465
    ],
    [
        'name' => 'IMAPS',
        'protocol' => 'TCP',
        'portfrom' => 993,
        'portto' => 993
    ],
    [
        'name' => 'POP3S',
        'protocol' => 'TCP',
        'portfrom' => 995,
        'portto' => 995
    ],
    [
        'name' => 'MS SQL',
        'protocol' => 'TCP',
        'portfrom' => 1443,
        'portto' => 1443
    ],
    [
        'name' => 'NFS',
        'protocol' => 'TCP',
        'portfrom' => 2049,
        'portto' => 2049
    ],
    [
        'name' => 'MYSQL/Aurora',
        'protocol' => 'TCP',
        'portfrom' => 3306,
        'portto' => 3306
    ],
    [
        'name' => 'Redshift',
        'protocol' => 'TCP',
        'portfrom' => 3389,
        'portto' => 3389
    ],
    [
        'name' => 'RDP',
        'protocol' => 'TCP',
        'portfrom' => 5439,
        'portto' => 5439
    ],
    [
        'name' => 'PostgreSQL',
        'protocol' => 'TCP',
        'portfrom' => 5432,
        'portto' => 5432
    ],
    [
        'name' => 'Oracle-RDS',
        'protocol' => 'TCP',
        'portfrom' => 1521,
        'portto' => 1521
    ],
    [
        'name' => 'WinRM-HTTP',
        'protocol' => 'TCP',
        'portfrom' => 5985,
        'portto' => 5985
    ],
    [
        'name' => 'WinRM-HTTPS',
        'protocol' => 'TCP',
        'portfrom' => 5986,
        'portto' => 5986
    ],
    [
        'name' => 'Elastic GPU',
        'protocol' => 'TCP',
        'portfrom' => 2007,
        'portto' => 2007
    ],
];
