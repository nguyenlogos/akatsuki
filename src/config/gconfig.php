<?php
date_default_timezone_set("Asia/Tokyo");
@session_save_path(ROOT_PATH . "/awsfw_session");
if (DEBUG_MODE) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}
$ServiceShortName = array(
    'AWS Cost Explorer' => 'Cost Explorer',
    'AWS Data Pipeline' => 'Data Pipeline',
    'Amazon Relational Database Service' => 'RDS',
    'Tax' => 'Tax',
    'AWS Lambda' => 'Lambda',
    'Amazon Elastic Block Store' => 'EBS',
    'AWS Key Management Service' => 'Key Mgmt. Service',
    'AWS CloudTrail' => 'CloudTrail',
    'AWS Storage Gateway' => 'Strage Gateway',
    'Amazon Simple Email Service' => 'SES',
    'Amazon Virtual Private Cloud' => 'VPC',
    'Amazon Elastic Load Balancing' => 'ELB',
    'Amazon Route 53' => 'Route 53',
    'Amazon Simple Storage Service' => 'S3',
    'AmazonCloudWatch' => 'CloudWatch',
    'Amazon Elastic Compute Cloud' => 'EC2',
    'Amazon Elastic Compute Cloud - Compute' => 'EC2',
    'AWS Config' => 'Config');

$ServiceColorTable1 = array(
    'Cost Explorer',
    'EC2',
    'S3',
    'Config',
    'AWSDataTransfer',
    'Data Pipeline',
    'Route 53',
    'RDS',
    'VPC',
    'Storage Gateway'
);
$ServiceColorTable2 = array(
    '#FAAC58',
    '#A9F5A9',
    '#A9A9F5',
    '#F6CEF5',
    '#F3F781',
    '#DF0101',
    '#FA5858',
    '#5858FA',
    '#FFFF00',
    '#E6E6E6'
);
$additionalColorTable = array(
    '#F6CEF5', '#F781F3', '#F8E0E0', '#FAAC58', '#BDBDBD',
    '#58FA58', '#F6E3CE', '#01DF01', '#F1F8E0', '#0101DF',
    '#E0E0F8', '#DF7401', '#6E6E6E'
);
