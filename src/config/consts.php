<?php
if (is_readable(__DIR__ . "/dbconfig.local.php")) {
    include_once("dbconfig.local.php");
} else {
    require_once("dbconfig.php");
}

if (is_readable(__DIR__ . "/config.local.php")) {
    include_once("config.local.php");
}

if (is_readable(__DIR__ . "/debug.local.php")) {
    include_once("debug.local.php");
}
if (!defined("DEBUG_MODE")) {
    define("DEBUG_MODE", false);
}
define("SERVICE_NAME", "Sunny View");
define("PAGE_TITLE", SERVICE_NAME . " - ");
define("SERVER_URL", 'http://52.68.30.203/');

define("DB_DRIVER", "pgsql");

define("SOURCE_PATH", dirname(__DIR__));
define("ROOT_PATH", dirname(SOURCE_PATH));
define("LIBS_PATH", ROOT_PATH . "/libs");
define("LOGS_PATH", ROOT_PATH . "/logs");
define("LOGSQL_PATH", ROOT_PATH . "/logsql");
define("KEYPAIR_PATH", ROOT_PATH . "/keypairs");
defined("COMMON_PATH") or define("COMMON_PATH", SOURCE_PATH . "/common");
define("ROOT_HOME", "/");
//DBへの接続情報
define("DB_CONNECT_INFO", sprintf(
    "host=%s port=%s dbname=%s user=%s password=%s",
    DB_SERVER,
    DB_PORT,
    DB_NAME,
    DB_USER,
    DB_PASSWORD
));
//全画面共通のテンプレート
define("PAGELAYOUT_TEMPLATE", LIBS_PATH . "/smarty/templates/pagelayout.tpl");
//templatesのパス
define("TEMPLATES_PATH", LIBS_PATH . "/smarty/templates/");
//templates_cのパス
define("TEMPLATES_C_PATH", LIBS_PATH . "/smarty/templates_c/");
//configsのパス
define("CONFIGS_PATH", LIBS_PATH . "/smarty/configs/");
//cacheのパス
define("CACHE_PATH", LIBS_PATH . "/smarty/cache/");

define("TMP_DIR", ROOT_PATH . "/batch/tmp/");

// 地域名の日本語訳定義
define("eu-west-1", "欧州(ｱｲﾙﾗﾝﾄﾞ)");
define("sa-east-1", "南米(ｻﾝﾊﾟｳﾛ)");
define("us-east-1", "米国東部(ﾊﾞｰｼﾞﾆｱ北部)");
define("ap-northeast-1", "ｱｼﾞｱﾊﾟｼﾌｨｯｸ(東京)");
define("us-west-1", "米国西部(北ｶﾘﾌｫﾙﾆｱ)");
define("us-west-2", "米国西部(ｵﾚｺﾞﾝ)");
define("ap-southeast-1", "ｱｼﾞｱﾊﾟｼﾌｨｯｸ(ｼﾝｶﾞﾎﾟｰﾙ)");
define("ap-southeast-2", "ｱｼﾞｱﾊﾟｼﾌｨｯｸ(ｼﾄﾞﾆｰ)");

defined("ITEMS_PER_PAGE") or define("ITEMS_PER_PAGE", 20);
define("DEFAULT_COUNTPAGE_DEPT", ITEMS_PER_PAGE);
define("DEFAULT_COUNTPAGE_EMP", ITEMS_PER_PAGE);
define("DEFAULT_COUNTPAGE_LOG", ITEMS_PER_PAGE);
define("DEFAULT_COUNTPAGE_PRJ", ITEMS_PER_PAGE);
define("DEFAULT_AWS_REGION", 'ap-northeast-1');

// UI
define("MOVE_DN", "glyphicon glyphicon-arrow-down");
define("MOVE_UP", "glyphicon glyphicon-arrow-up");

define("SORT_DN_ACTIVE", "glyphicon glyphicon-triangle-bottom");
define("SORT_UP_ACTIVE", "glyphicon glyphicon-triangle-top");
define("SORT_DN", "glyphicon glyphicon-chevron-down");
define("SORT_UP", "glyphicon glyphicon-chevron-up");

define("MAIL_FROM", "nakano@ids.co.jp");

defined("SUPERUSER_CID") or define("SUPERUSER_CID", 3);
define("CRYPT_KEY", "qJB0rGtIn5UB1xG03efyCp");
// 最悪の場合に採用するドルレート
define("SUPER_DEFAULT_RATE", 100);

define("INST_REQ_DELETE_PENDING", -3);
define("INST_REQ_DELETED", -2);
define("INST_REQ_REJECTED", -1);
define("INST_REQ_PENDING", 0);
define("INST_REQ_APPROVED", 1);
define("INST_REQ_APPROVE_PROCESSING", -4);
define("INST_REQ_APPROVE_FAILED", -5);

const INST_REQ_EDITABLE_STATES = [
    INST_REQ_PENDING,
    INST_REQ_REJECTED,
    INST_REQ_APPROVE_FAILED,
];

define("EBS_REQ_REJECTED", -1);
define("EBS_REQ_APPROVED", 1);
define("EBS_REQ_PENDING", 0);

define("INVOICE_PATH", ROOT_PATH . "/invoice/");
defined("AWS_UPDATE_INTERVAL") or define("AWS_UPDATE_INTERVAL", 86400);
defined("CW_LOGS_UPDATE_INTERVAL") or define("CW_LOGS_UPDATE_INTERVAL", 600);
define("AWS_SDK_VERSION", "2016-11-15");
define("PASSWORD_LENGTH", 8);

defined("G_RECAPTCHA_SITE_KEY") or define("G_RECAPTCHA_SITE_KEY", "6LdWjmQUAAAAAEpUyU4zrjg58vXzuoL7_drjuEBT");
defined("G_RECAPTCHA_SECRET_KEY") or define("G_RECAPTCHA_SECRET_KEY", "6LdWjmQUAAAAAAaE_ZyPfWh-PrYbS7qodt7V1nDh");
defined("SMTP_HOST") or define("SMTP_HOST", "127.0.0.1");
defined("SMTP_PORT") or define("SMTP_PORT", "25252");
defined("SYSTEM_EMAIL") or define("SYSTEM_EMAIL", "no-reply@ids.co.jp");
defined("SYSTEM_EMAIL_NAME") or define("SYSTEM_EMAIL_NAME", "SunnyView NO-REPLY");
defined("LAMBDA_EBS_BACKUP_FUNCTION_NAME") or define("LAMBDA_EBS_BACKUP_FUNCTION_NAME", "ids-ebs-backup");
defined("ERR_DATA_NOT_FOUND") or define("ERR_DATA_NOT_FOUND", "データが見つかりません。");

define("AWS_INVALID_CREDENTIALS", "AWS was not able to validate the provided access credentials");

define("SETTING_DEFAULT_PASSWORD_LENGTH", 4);       // 長さ
define("SETTING_DEFAULT_PASSWORD_EXPIRATION", 0);   // 有効期限(日数)
define("SETTING_DEFAULT_PASSWORD_CHECK_CASE", 0);   // 必ず大文字・小文字を使用
define("SETTING_DEFAULT_PASSWORD_CHECK_NUMBER", 0); // 必ず数字を使用
define("SETTING_DEFAULT_PASSWORD_LOCK", 5);         // アカウントロックする連続パスワード間違い (回数)

define("SETTING_DEFAULT_TOKEN_EXPIRATION", 600);    // パスワード変更などのURLの有効期限 (秒), admin_active
define("SETTING_DEFAULT_ADMIN_UNLOCK", 1);          // ロックされたアカウントの解除権限
define("SETTING_DEFAULT_SYSADMIN", 1);              // システム管理者
define("SETTING_DEFAULT_MANAGER", 1);               // マネージャー
define("SETTING_DEFAULT_INSTANCE_EXPIRATION", 30);  // インスタンスの期限前通知メール (日数)
define("SETTING_DEFAULT_INSTANCE_NOTIFICATION", 0); // インスタンスに関するメール通知 (起動・停止などについてメール通知)
define("INSTANCE_STORAGE", "Volume"); // List Volume on instance
define("INSTANCE_SG", "Security Group"); // List Security Group on instance
define("INSTANCE_ELASTIC_IP", "Elastic Ips"); // List Elastic Ips on instance
define("CHECK_64_STRLENGTH", 64); // check 64 varchar
define("CHECK_128_STRLENGTH", 128); // check 128 varchar


const LOG_TYPES = array(
    'AWS' => 'AWS',
    'DB'  => '設定',
    'EX'  => 'EX',
    'CW'  => 'CW',
);

const LOG_ACTIONS = [
    'INSERT' => 'INSERT',
    'UPDATE' => 'UPDATE',
    'DELETE' => 'DELETE',
    'INST_START'    => 'INST_START',
    'INST_STOP'     => 'INST_STOP',
    'INST_RUN'      => 'INST_RUN',
    'INST_REMOVE'   => 'INST_REMOVE',
    'INST_REQ'      => 'INST_REQ',
    'PWD_CHANGE'    => 'PWD_CHANGE',
    'KEY_CHANGE'    => 'KEY_CHANGE',
    'LOGIN_FAILED'  => 'LOGIN_FAILED',
];

const TBL_NAMES = [
    'dept' => 'グループ',
    'proj' => 'プロジェクト',
    'emp'  => 'ユーザー',
    'obj'  => 'オブジェクト',
    'cw_rules' => 'CWルール'
];

const SG_IP_SOURCES = [
    'MYIP'     => 'myip',
    'ANYWHERE' => 'anywhere',
    'CUSTOM'   => 'custom',
];

const AWS_INST_STATES = ['running', 'stopped', 'pending', 'stopping'];

require_once("gconfig.php");
require_once("sgstypes.php");