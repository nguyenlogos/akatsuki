<?php
namespace Akatsuki\Models;

class TblRoles extends BaseModel
{
    protected $table = 'tbl_roles';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }
}
