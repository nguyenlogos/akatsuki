<?php
namespace Akatsuki\Models;

class EmpProj extends BaseModel
{
    protected $table = 'emp_proj';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cid'   => 'cid',
            'empid' => 'empid',
            'pcode' => 'pcode',
        ];
    }

    public static function getListAssigned()
    {
        $sql = sprintf('
            SELECT
                p.*
            FROM
            (
                SELECT
                    ep.cid,
                    ep.pcode
                FROM
                    emp_proj ep
                WHERE
                    ep.cid = %s
                    AND ep.empid = %s
                GROUP BY
                    ep.cid,
                    ep.pcode
            ) ep
            INNER JOIN
                proj p
                ON p.cid = ep.cid
                AND p.pcode = ep.pcode;
        ', $_SESSION['cid'], $_SESSION['empid']);
        $list = self::selectRaw($sql);
        if ($list) {
            return $list;
        }
        return [];
    }
}
