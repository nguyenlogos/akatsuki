<?php
namespace Akatsuki\Models;

class Plan extends BaseModel
{
    protected $table = 'plan';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'plan' => '契約'
        ];
    }
}
