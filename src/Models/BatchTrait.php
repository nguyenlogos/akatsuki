<?php
namespace Akatsuki\Models;

use Illuminate\Database\Capsule\Manager as DB;

trait BatchTrait
{
    private static function getJoinColumn($table)
    {
        $joinColumn = '';
        switch ($table) {
            case 'ami_enable':
                $joinColumn = 'imageid';
                break;
            case 'azone_enable':
            case 'insttype_enable':
                $joinColumn = 'name';
                break;
            default:
                break;
        }
        if (!$joinColumn) {
            throw new \Exception("Columns does not exist.", 1);
        }

        return $joinColumn;
    }

    protected static function batchEnable($cid, $list = array())
    {
        $table = static::table();
        $joinColumn = self::getJoinColumn($table);
        $mainTable = explode("_", $table)[0];

        $cid = (int)$cid;
        $listStr = implode(',', array_map(function ($item) {
            $item = pg_escape_string($item);
            return "'{$item}'";
        }, $list));

        $joinConditions = [
            "b.{$joinColumn} = a.{$joinColumn}"
        ];
        $whereConditions = [
            "a.{$joinColumn} IN ($listStr)",
            "b.{$joinColumn} IS NULL"
        ];

        if ($table === 'insttype_enable') {
            $joinConditions[] = "b.cid = {$cid}";
        } else {
            $joinConditions[] = "b.cid = a.cid";
            $whereConditions[] = "a.cid = {$cid}";
        }

        $joinConditions = implode(' AND ', $joinConditions);
        $whereConditions = implode(' AND ', $whereConditions);

        $query = "
            SELECT a.{$joinColumn}
            FROM {$mainTable} a
            LEFT JOIN {$table} b
                ON {$joinConditions}
            WHERE {$whereConditions}
        ";
        // Illuminate\Database\PostgresConnection
        $list = DB::select($query);
        if (count($list) === 0) {
            return true;
        }
        $batchData = [];
        foreach ($list as $item) {
            $batchData[] = [
                "cid" => $cid,
                "{$joinColumn}" => $item->$joinColumn
            ];
        }

        return static::insertAll($batchData);
    }

    protected static function batchDisable($cid, $list = array())
    {
        $table = static::table();
        $joinColumn = self::getJoinColumn($table);

        $cid = (int)$cid;
        $listStr = implode(',', array_map(function ($item) {
            $item = pg_escape_string($item);
            return "'{$item}'";
        }, $list));

        $query = "
            SELECT id
            FROM {$table} a
            WHERE
                a.cid = {$cid}
                AND a.{$joinColumn} IN ($listStr)
        ";

        $list = DB::select($query);
        if (count($list) === 0) {
            return true;
        }

        $ids = [];
        foreach ($list as $item) {
            $ids[] = $item->id;
        }

        return static::deleteAll($ids);
    }
}
