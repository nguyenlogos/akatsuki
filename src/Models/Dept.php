<?php
namespace Akatsuki\Models;

class Dept extends BaseModel
{
    protected $table = 'dept';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'deptname'  => 'グループ名'
        ];
    }

    public function keys()
    {
        return [
            'deptname'
        ];
    }
}
