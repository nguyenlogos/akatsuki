<?php
namespace Akatsuki\Models;

class InstReqChange extends BaseModel
{
    protected $table = 'inst_req_change';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'inst_req_id' => 'inst_req_id',
            'cid' => 'cid',
            'empid' => 'empid',
            'dept' => 'dept',
            'pcode' => 'pcode',
            'using_purpose' => 'using_purpose',
            'inst_address' => 'inst_address',
        ];
    }
}
