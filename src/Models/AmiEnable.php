<?php
namespace Akatsuki\Models;

use Illuminate\Database\Capsule\Manager as DB;

use Common\Logger;

class AmiEnable extends BaseModel
{
    use BatchTrait;

    protected $table = 'ami_enable';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cid'     => 'cid',
            'imageid' => 'imageid',
            'id'      => 'id',
        ];
    }

    public static function enable($cid, $list = array())
    {
        return static::batchEnable($cid, $list);
    }

    public static function disable($cid, $list = array())
    {
        return static::batchDisable($cid, $list);
    }
}
