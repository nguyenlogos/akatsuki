<?php
namespace Akatsuki\Models;

use AwsServices\CWEvents;

class CwRules extends BaseModel
{
    protected $table = 'cw_rules';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name'   => 'ルール名',
            'expr'   => 'スケジュール',
            'region' => 'エリア',
            'state'  => '状態',
            'notes'  => '説明',
        ];
    }

    public function putRule()
    {
        if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY']) {
            return false;
        }
        $clientConfig = array(
            'credentials' => [
                'key'     => $_SESSION["key"],
                'secret'  => $_SESSION["secret"],
            ],
            'region'      => $this->region,
            'version'     => 'latest'
        );

        $timezone = Configs::getConfig('timezone');
        // convert from local timezone to aws timezone
        $exprLocal = $this->expr;
        $expr = aws_rule_convert_timezone($this->expr, $timezone);
        if (!$expr) {
            return false;
        }
        $client = new CWEvents($clientConfig);
        $ruleConfigs = [
            'Description' => $this->notes,
            'Name' => $this->name,
            'ScheduleExpression' => $expr,
            'State' => $this->state == 0 ? 'DISABLED' : 'ENABLED',
        ];

        $result = $client->putRule($ruleConfigs);
        $result && $result = $client->putBackupTargets($this->name);

        if ($result) {
            $this->expr = $expr;
            $result = $this->save();
            if ($result) {
                $this->expr = $exprLocal;
                return true;
            }
            return false;
        }

        return false;
    }

    public function deleteRule()
    {
        $clientConfig = array(
            'credentials' => [
                'key'     => $_SESSION["key"],
                'secret'  => $_SESSION["secret"],
            ],
            'region'      => $this->region,
            'version'     => 'latest'
        );

        $client = new CWEvents($clientConfig);
        $result = $client->deleteRule($this->name);
        if ($result) {
            $this->status = -1;
            return $this->save();
        }

        return false;
    }

    public static function updateBackupRules($region = null)
    {
        empty($region) && $region = $_SESSION['region'];
        $roles = permission_check("admin/cw_rules.php");
        if (!$roles['read']['allowed'] || $roles['read']['condition'] == 1) {
            $forDept = $_SESSION['dept'];
        } else {
            $forDept = 0;
        }
        $rules = self::select('name')->where('region', $region)->where('dept', $forDept)->get()->toArray();
        count($rules) && $rules = array_column($rules, 'name');

        $cwRules = (CWEvents::getInstance($region))->listRules();
        $batchData = [];
        foreach ($cwRules as $cwRule) {
            $expr = aws_rule_convert_timezone($cwRule['ScheduleExpression'], $timezone, true);
            if (!$expr) {
                continue;
            }

            if (in_array($cwRule['Name'], $rules)) {
                $dept = $forDept;
            } else {
                $dept = 0;
            }
            if ($dept != $forDept) {
                continue;
            }
            $batchData[] = [
                'cid'    => $_SESSION['cid'],
                'name'   => $cwRule['Name'],
                'expr'   => $cwRule['ScheduleExpression'],
                'state'  => $cwRule['State'] === 'DISABLED' ? 0 : 1,
                'notes'  => !empty($cwRule['Description']) ? $cwRule['Description'] : '',
                'region' => $region,
                'dept'   => $dept,
            ];
        }
        $rules = self::beginTransaction(function () use ($region, $forDept, $batchData) {
            self::where('region', $region)->where('dept', $forDept)->delete();
            return self::insertAll($batchData, false);
        });
        if ($rules) {
            return json_decode(json_encode($rules), true);
        }
        return [];
    }
}
