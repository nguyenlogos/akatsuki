<?php
namespace Akatsuki\Models;

class SgEnable extends BaseModel
{
    protected $table = 'sg_enable';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'cid' => 'cid',
            'sg_id' => 'sg_id',
        ];
    }

    public static function enable($cid, Array $sgIdList = array())
    {
        if (count($sgIdList)) {
            $batchData = array_map(function ($sgId) use ($cid) {
                return [
                    'cid'   => $cid,
                    'sg_id' => $sgId
                ];
            }, $sgIdList);
            $result = SgEnable::insertAll($batchData, false);
            return !!$result;
        }
        return true;
    }
}
