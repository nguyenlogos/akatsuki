<?php
namespace Akatsuki\Models;

class Volumes extends BaseModel
{
    protected $table = 'volumes';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'device'        => 'デバイス名',
            'volid'         => 'デバイスの種類',
            'size'          => 'サイズ',
            'iop'           => 'ＩＯＰＳ',
            'encrypt'       => '暗号化',
            'snapshotid '   => 'コピー元スナップショット',
        ];
    }
}
