<?php
namespace Akatsuki\Models;

class SecurityGroup extends BaseModel
{
    protected $table = 'security_group';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name_sg'           => 'Name security group',
            'description_sg'    => 'Description security group',
            'vpc_sg'            => 'ネットワーク'
        ];
    }

}
