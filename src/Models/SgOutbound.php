<?php
namespace Akatsuki\Models;

class SgOutbound extends BaseModel
{
    protected $table = 'sg_outbound';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }
}