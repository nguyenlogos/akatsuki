<?php

namespace Akatsuki\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\QueryException as DBException;
use Illuminate\Database\Capsule\Manager as DB;

use Common\Logger;

class BaseModel extends Model
{
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
        static::observe(new \Common\BaseModelObserver);
    }

    protected static function table()
    {
        return (new static)->getTable();
    }

    protected static function beginTransaction(\Closure $callback)
    {
        return DB::transaction(function () use ($callback) {
            return $callback();
        });
    }

    public function saveWithoutEvents(\Closure $callback)
    {
        $dispatcher = static::getEventDispatcher();
        static::unsetEventDispatcher();
        if (php_sapi_name() !== "cli") {
            DB::enableQueryLog();
            $return = $callback();
            $get_id = $this->id;
            if ($get_id) {
                $get_table = 'insert'.$this->getTable();
            } else {
                $get_table = 'update'.$this->getTable();
            }

            BaseModel_sql(DB::getQueryLog(), $get_table);
        } else {
            $return = $callback();
        }
        static::setEventDispatcher($dispatcher);

        return $return;
    }

    public function setValues(array $values = array())
    {
        foreach ($values as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * Specify main keys to display when generating log template
     */
    public function keys()
    {
        return [
            'id'
        ];
    }

    public function relations()
    {
        return [];
    }

    protected static function insertAll(array $items, $enableLogging = true)
    {
        if (count($items) === 0) {
            return true;
        }

        $insertedItems = null;

        DB::transaction(function () use ($items, &$insertedItems, $enableLogging) {
            $table = static::table();
            $columns = array_map(function ($column) {
                return '"'.$column.'"';
            }, array_keys($items[0]));
            $columns = implode(',', $columns);

            $values = [];
            foreach ($items as $item) {
                $data = array_map(function ($item) {
                    if (is_null($item)) {
                        return "NULL";
                    }
                    $item = pg_escape_string($item);
                    return "'{$item}'";
                }, array_values($item));
                $values[] = sprintf("(%s)", implode(',', $data));
            }
            $values = implode(',', $values);

            $query = "INSERT INTO {$table}({$columns}) VALUES {$values} RETURNING *;";
            raise_sql($query, 'insert'.$table);
            $logType = LOG_TYPES['DB'];
            $logAction = LOG_ACTIONS['INSERT'];

            $insertedItems = DB::select($query);
            if ($enableLogging) {
                foreach ($insertedItems as $item) {
                    $changes = [];
                    $contents = [
                        'tbl_id'   => $item->id,
                        'tbl_name' => $table,
                        'changes'  => [
                            'old'  => null,
                            'new'  => $item
                        ]
                    ];
                    Logger::info(json_encode($contents), $logType, $logAction);
                }
            }
        });

        return $insertedItems;
    }

    protected static function deleteAll(array $idList, $enableLogging = true)
    {
        if (count($idList) === 0) {
            return true;
        }
        $deletedItems = null;
        DB::transaction(function () use ($idList, &$deletedItems, $enableLogging) {
            $table = static::table();

            $idList = array_map(function ($id) {
                return (int)$id;
            }, $idList);

            $idList = implode(',', $idList);

            $query = "
                DELETE FROM {$table}
                WHERE id IN($idList) RETURNING *;
            ";
            raise_sql($query, 'delete'.$table);
            $logType = LOG_TYPES['DB'];
            $logAction = LOG_ACTIONS['DELETE'];

            $deletedItems = DB::select($query);
            if ($enableLogging) {
                foreach ($deletedItems as $item) {
                    $changes = [];
                    $contents = [
                        'tbl_id'   => $item->id,
                        'tbl_name' => $table,
                        'changes'  => [
                            'old'  => $item,
                            'new'  => null
                        ]
                    ];
                    Logger::info(json_encode($contents), $logType, $logAction);
                }
            }
        });

        return $deletedItems;
    }

    public static function getInstance($tblName)
    {
        $className = "\\Akatsuki\Models\\" . implode('', array_map(function ($part) {
            return ucwords($part);
        }, explode('_', $tblName)));
        if (!class_exists($className)) {
            return null;
        }

        return new $className();
    }

    public static function selectRaw($sql)
    {
        $result = DB::select($sql);

        if (!is_null($result)) {
            return json_decode(json_encode($result), true);
        }

        return null;
    }
}
