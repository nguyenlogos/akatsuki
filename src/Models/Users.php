<?php
namespace Akatsuki\Models;

class Users extends BaseModel
{
    protected $table = 'users';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'tel'         => '電話番号',
            'name'        => 'お名前',
            'dept'        => '部署名',
            'companyname' => '会社名',
            'position'    => '役職',
        ];
    }
}