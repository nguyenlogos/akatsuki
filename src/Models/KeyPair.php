<?php
namespace Akatsuki\Models;

class KeyPair extends BaseModel
{
    protected $table = 'key_pair';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name_key'  => 'Key Pair'
        ];
    }

    public function keys()
    {
        return [
            'name_key'
        ];
    }
}