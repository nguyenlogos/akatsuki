<?php
namespace Akatsuki\Models;

class EbsLog extends BaseModel
{
    protected $table = 'ebs_log';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }
}
