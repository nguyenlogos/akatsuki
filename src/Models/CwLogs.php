<?php
namespace Akatsuki\Models;

class CwLogs extends BaseModel
{
    protected $table = 'cw_logs';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    public static function insertLogs($cid, $stream, $events)
    {
        array_shift($events); // remove START message

        $infoReport = array_pop($events); // extract report info
        $infoMessage = preg_replace('/^REPORT /', '', $infoReport['message']);
        $infoMessage = trim($infoMessage);
        $infoDetails = preg_split('/\t/', $infoMessage);
        $description = [];
        foreach ($infoDetails as $info) {
            $info = explode(':', $info);
            $key = trim($info[0]);
            $val = trim($info[1]);
            $description[$key] = $val;
        }

        array_pop($events);   // remove END message
        $message = implode("", array_column($events, 'message'));
        $model = new self();
        $match = preg_match('/^=== BEGIN BACKUP ===\s(.+)=== END BACKUP ===$/s', $message, $matches);
        if ($match) {
            $model->error_flag = 0;
            $model->event = trim($matches[1]);
        } else {
            $model->error_flag = 1;
            $message = preg_replace('/=== BEGIN BACKUP ===/', '', $message);
            $message = preg_replace('/=== END BACKUP ===/', '', $message);
            $model->event = MESSAGES['ERR_UNKNOWN_ERROR'];
            $description['Traces'] = $message;
        }

        $model->setValues([
            'cid'          => $cid,
            'stream'       => $stream['logStreamName'],
            'stream_time'  => date('Y-m-d H:i:s', (int)$stream['lastEventTimestamp']/1000),
            'event_time'   => date('Y-m-d H:i:s', (int)$infoReport['timestamp']/1000),
            'description'  => json_encode($description),
            'date_created' => date('Y-m-d H:i:s'),
        ]);

        return $model->saveWithoutEvents(function () use ($model) {
            return $model->save();
        });
    }
}
