<?php
namespace Akatsuki\Models;

class EbsReqDetail extends BaseModel
{
    protected $table = 'ebs_req_detail';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }
}
