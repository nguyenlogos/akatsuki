<?php
namespace Akatsuki\Models;

class Configs extends BaseModel
{
    protected $table = 'configs';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    public static function getConfig($name, $cid = null)
    {
        if (empty($cid)) {
            $cid = $_SESSION['cid'];
        }
        $config = self::where('cid', $cid)->get()->first();
        if (!$config) {
            return false;
        }

        return $config->getAttribute($name);
    }
}
