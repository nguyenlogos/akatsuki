<?php
namespace Akatsuki\Models;

class SgTempRules extends BaseModel
{
    protected $table = 'sg_temp_rules';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    public function isEqualTo(SgTempRules $rule)
    {
        $isEqual =
            $this->type == $rule->type &&
            strcasecmp($this->proto, $rule->proto) == 0 &&
            $this->pfrom == $rule->pfrom &&
            $this->pto == $rule->pto &&
            $this->target == $rule->target;
        return $isEqual;
    }
}
