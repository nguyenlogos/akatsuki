<?php
namespace Akatsuki\Models;

use Illuminate\Support\Facades\Config;

class Emp extends BaseModel
{
    protected $table = 'emp';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name'      => '氏名',
            'email'     => 'メールアドレス',
            'dept'      => '所属グループ',
            'admin'     => 'マネージャー',
            'sysadmin'  => 'システム管理者',
            'idsadmin'  => 'ＩＤＳの管理者',
        ];
    }

    public function changePassword($newPassword)
    {
        if ($this->pass === $newPassword) {
            return true;
        }
        $this->pass = $newPassword;
        $result = $this->saveWithoutEvents(function () {
            return $this->save();
        });
        if ($result) {
            return \Common\Logger::info("", LOG_TYPES['DB'], LOG_ACTIONS['PWD_CHANGE']);
        }

        return $result;
    }

    public function resetPassword($newPassword)
    {
        $this->setValues([
            'pass'            => md5($newPassword),
            'pw_reset_token'  => null,
            'regdate'         => 'now()',
            'email_confirmed' => 1,
            'email_lock'      => 0,
            'number_lock'     => 0,
            'login_initial'   => 1,
        ]);
        return $this->saveWithoutEvents((function () {
            return $this->save();
        }));
    }

    public function generateResetToken()
    {
        $resetToken = random_string();
        $this->pw_reset_token = $resetToken;
        $this->pw_reset_token_time = 'now()';
        $result = $this->saveWithoutEvents(function () {
            return $this->save();
        });

        return $resetToken;
    }

    public function createConfigDefault()
    {
        Configs::where("cid", $this->cid)->delete();
        $config = new Configs();
        $config->cid = $this->cid;
        return $config->save();
    }

    public function keys()
    {
        return [
            'name'
        ];
    }

    public function relations()
    {
        return [
            'dept' => 'deptname'
        ];
    }

    public function depts()
    {
        return $this->belongsTo('Akatsuki\Models\Dept', 'dept', 'dept')
            ->where('cid', $this->cid);
    }
}
