<?php
namespace Akatsuki\Models;

use Illuminate\Database\Capsule\Manager as DB;

class AzoneEnable extends BaseModel
{
    use BatchTrait;

    protected $table = 'azone_enable';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cid'  => 'cid',
            'name' => 'name',
            'id'   => 'id',
        ];
    }

    public static function enable($cid, $list = array())
    {
        return static::batchEnable($cid, $list);
    }

    public static function disable($cid, $list = array())
    {
        return static::batchDisable($cid, $list);
    }

    public static function getAvailableRegion($cid = null)
    {
        if (empty($cid)) {
            $cid = $_SESSION['cid'];
        }
        $sql = "
            SELECT
                a.regionname
            FROM azone a
            INNER JOIN azone_enable b
                ON b.cid = a.cid
                AND b.name = a.name
            WHERE
                a.cid = {$cid}
            GROUP BY a.regionname
        ";
        $regions = DB::select($sql);

        return array_column($regions, 'regionname');
    }
}
