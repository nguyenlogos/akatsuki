<?php
namespace Akatsuki\Models;

class InsttypePricing extends BaseModel
{
    protected $table = 'insttype_pricing';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'    => 'id',
            'cid'   => 'cid',
            'name'  => 'name',
            'region'=> 'region',
            'price' => 'price'
        ];
    }
}
