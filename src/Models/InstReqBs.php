<?php
namespace Akatsuki\Models;

class InstReqBs extends BaseModel
{
    protected $table = 'inst_req_bs';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'device_name'   => 'デバイス名',
            'volumn_type'   => 'デバイスの種類',
            'volumn_size'   => 'サイズ',
            'iops'          => 'ＩＯＰＳ',
            'encrypted'     => '暗号化',
            'auto_removal'  => '自動削除',
            'snapshot_id'   => 'コピー元スナップショット',
        ];
    }
}