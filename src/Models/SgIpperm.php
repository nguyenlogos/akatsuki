<?php
namespace Akatsuki\Models;

class SgIpperm extends BaseModel
{
    protected $table = 'sg_ipperm';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'id',
            'cid'      => 'cid',
            'protocol' => 'protocol',
            'fromport' => 'fromport',
            'toport'   => 'toport',
            'iprange'  => 'iprange',
            'type'     => 'type',
        ];
    }
}
