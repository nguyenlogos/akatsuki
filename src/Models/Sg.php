<?php
namespace Akatsuki\Models;

use AwsServices\Ec2;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

class Sg extends BaseModel
{
    protected $table = 'sg';
    protected $primaryKey = 'tbl_id';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'id',
            'cid'         => 'cid',
            'vpcid'       => 'vpcid',
            'name'        => 'name',
            'lastchecked' => 'lastchecked',
            'description' => 'description',
        ];
    }

    public static function getListAvailable($vpc = null)
    {
        $whereConditions = [
            "sg.cid = {$_SESSION['cid']}",
            "sge.id > 0"
        ];
        if ($vpc) {
            $vpc = pg_escape_string($vpc);
            $whereConditions[] = "sg.vpcid = '{$vpc}'";
        }
        $whereConditions = "WHERE " .  implode(" AND ", $whereConditions);
        $sql = "
            SELECT
                sg.id,
                sg.name,
                sg.vpcid as vpc,
                sg.description as desc
            FROM
                sg sg
            LEFT JOIN
            (
                SELECT
                    sge.id,
                    sge.cid,
                    sge.sg_id
                FROM
                    sg_enable sge
                WHERE
                    sge.cid = {$_SESSION['cid']}
            ) sge
                ON sge.cid = sg.cid
                AND sge.sg_id = sg.id
            {$whereConditions}
        ";
        $list = self::selectRaw($sql);
        if ($list) {
            return $list;
        }
        return [];
    }

    public static function updateSecurityGroups($cid = null)
    {
        if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1) {
            return false;
        }
        empty($cid) && $cid = $_SESSION['cid'];
        $securityGroups = (Ec2::getInstance(null, $cid))->describeSecurityGroups();
        if (!$securityGroups) {
            return false;
        }
        Sg::where('cid', $cid)->delete();
        SgIpperm::where('cid', $cid)->delete();
        $sgList = [];
        foreach ($securityGroups as $securityGroup) {
            $sg = new self();
            $sg->setValues([
                'id'          => $securityGroup['GroupId'],
                'cid'         => $cid,
                'name'        => $securityGroup['GroupName'],
                'lastchecked' => 'now()',
                'description' => $securityGroup['Description'],
                'vpcid'       => $securityGroup['VpcId']
            ]);
            $result = $sg->saveWithoutEvents(function () use ($sg) {
                return $sg->save();
            });
            if (!$result) {
                return false;
            }
            $inbounds = $securityGroup['IpPermissions'];
            $outbounds = $securityGroup['IpPermissionsEgress'];
            $ipList = [];
            foreach ($inbounds as $rule) {
                $ipRanges = !empty($rule['IpRanges']) ? $rule['IpRanges'] : [];
                $fromPort = !empty($rule['FromPort']) ? (int)$rule['FromPort'] : 0;
                $toPort = !empty($rule['ToPort']) ? (int)$rule['ToPort'] : 0;
                foreach ($ipRanges as $ipRange) {
                    $ipList[] = [
                        'id'          => $sg->id,
                        'cid'         => $sg->cid,
                        'protocol'    => $rule['IpProtocol'],
                        'fromport'    => $fromPort,
                        'toport'      => $toPort,
                        'iprange'     => $ipRange['CidrIp'],
                        'lastchecked' => 'now()',
                        'type'        => 'ingress',
                    ];
                }
            }
            foreach ($outbounds as $rule) {
                $ipRanges = !empty($rule['IpRanges']) ? $rule['IpRanges'] : [];
                $fromPort = !empty($rule['FromPort']) ? (int)$rule['FromPort'] : 0;
                $toPort = !empty($rule['ToPort']) ? (int)$rule['ToPort'] : 0;
                foreach ($ipRanges as $ipRange) {
                    $ipList[] = [
                        'id'          => $sg->id,
                        'cid'         => $sg->cid,
                        'protocol'    => $rule['IpProtocol'],
                        'fromport'    => $fromPort,
                        'toport'      => $toPort,
                        'iprange'     => $ipRange['CidrIp'],
                        'lastchecked' => 'now()',
                        'type'        => 'egress',
                    ];
                }
            }
            if (count($ipList)) {
                $ipList = SgIpperm::insertAll($ipList, false);
                if ($ipList) {
                    $ipList = json_decode(json_encode($ipList), true);
                }
            }
            $sg->ipranges = $ipList;
            $sgList[] = $sg->toArray();
        }
        return $sgList;
    }
}
