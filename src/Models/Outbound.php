<?php
namespace Akatsuki\Models;

class Outbound extends BaseModel
{
    protected $table = 'sg_outbound';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'id',
            'id_security_group' => 'id_security_group',
            'outb_type'         => 'inb_type',
            'outb_protocol'     => 'inb_protocol',
            'outb_port_range'   => 'inb_port_range',
            'outb_cidr_ip'      => 'inb_cidr_ip',
            'outb_source'       => 'inb_source',
            'outb_description'  => 'inb_description',
            'create_time'       => 'create_time',
        ];
    }
}
