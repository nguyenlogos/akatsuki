<?php
namespace Akatsuki\Models;

use Common\Logger;

class InstReq extends BaseModel
{
    protected $table = 'inst_req';
    private $blockStores;

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'dept'              => 'グループ',
            'pcode'             => 'プロジェクト',
            'inst_ami'          => 'ＡＭＩ',
            'inst_vpc'          => 'ネットワーク',
            'inst_subnet'       => 'サブネット',
            'inst_address'      => '住所',
            'inst_type'         => 'インスタンスタイプ',
            'inst_count'        => 'インスタンス個数',
            'inst_region'       => 'データセンター',
            'security_groups'   => 'セキュリティグループ',
            'using_purpose'     => 'コメント',
            'shutdown_behavior' => 'シャットダウン時の後処理',
            'req_date'          => '申請日',
        ];
    }

    private function updateSgTemp($sgInfo)
    {
        if (count($sgInfo)) {
            $findRuleByProtocol = function ($name, $protocol) {
                $match  = array_filter(AWS_SG_STYPES, function ($stype) use ($name, $protocol) {
                    $match = false;
                    if (!empty($stype['alias'])) {
                        $match = $stype['alias'] == $protocol;
                    } else {
                        $match = $stype['protocol'] == $protocol;
                    }
                    return $match && $stype['name'] === $name;
                });
                if ($match) {
                    return array_shift($match);
                }

                return null;
            };

            $sgTemp = SgTemp::where('cid', $this->cid)
                ->where('inst_req_id', $this->id)
                ->first();
            if (!$sgTemp) {
                $sgTemp = new SgTemp();
            }
            if ($sgInfo['sg_id']) {
                $sgInfo['sg_name'] = $sgInfo['sg_desc'] = null;
            }

            $sgTemp->setValues([
                'cid'         => $this->cid,
                'sg_id'       => $sgInfo['sg_id'],
                'sg_vpc'      => $sgInfo['sg_vpc'],
                'sg_name'     => $sgInfo['sg_name'],
                'sg_desc'     => $sgInfo['sg_desc'],
                'inst_req_id' => $this->id,
            ]);

            $ret = $sgTemp->saveWithoutEvents(function () use (&$sgTemp) {
                return $sgTemp->save();
            });

            if ($ret) {
                return false;
            }

            SgTempRules::where('cid', $sgTemp->cid)
                ->where('sg_temp_id', $sgTemp->id)->delete();
            $sgRulesData = !empty($sgInfo['sg_rules_validated']) ? $sgInfo['sg_rules_validated'] : [];
            if (count($sgRulesData)) {
                SgTempRules::insertAll($sgRulesData, false);
            }
        }
    }

    public static function register($instInfo, $bsInfo = array(), $sgInfo = array())
    {
        $instReq = null;
        self::beginTransaction(function () use ($instInfo, $bsInfo, $sgInfo, &$instReq) {
            $instReq = new self();
            $instReq->setValues($instInfo);

            $result = $instReq->saveWithoutEvents(function () use (&$instReq) {
                return $instReq->save();
            });

            $ret = $instReq->updateSgTemp($sgInfo);
            if (!$ret) {
                return false;
            }
            if (count($bsInfo)) {
                $columns = array_keys((new InstReqBs)->attributeLabels());
                foreach ($columns as $column) {
                    foreach ($bsInfo as &$bs) {
                        if (!array_key_exists($column, $bs)) {
                            unset($bs[$column]);
                        }
                        $bs['inst_req_id'] = $instReq->id;
                    }
                }
                InstReqBs::insertAll($bsInfo, false);
            }

            $blockStores = json_decode(json_encode($result), true); // convert array of object to array
            $instReq->block_stores = $blockStores;
            $contents = [
                'tbl_id' => $instReq->id,
                'tbl_name' => $instReq->getTable(),
                'changes' => [
                    'old' => null,
                    'new' => $instReq->toArray()
                ]
            ];
            Logger::info(json_encode($contents), LOG_TYPES['AWS'], LOG_ACTIONS['INST_REQ']);
        });

        return $instReq;
    }

    public function updateInfo($instInfo, $bsInfo = array(), $sgInfo = array())
    {
        if ($this->approved_status == INST_REQ_APPROVED) {
            return $this->updateInfos($instInfo);
        }
        $instReq = $this;
        self::beginTransaction(function () use ($instInfo, $bsInfo, $sgInfo, &$instReq) {
            $instReq->setValues($instInfo);
            $instReq->note = '';
            $result = $instReq->saveWithoutEvents(function () use (&$instReq) {
                return $instReq->save();
            });

            $ret = $instReq->updateSgTemp($sgInfo);
            if ($ret) {
                return false;
            }
            if ($result && count($bsInfo)) {
                $columns = array_keys((new InstReqBs)->attributeLabels());
                InstReqBs::where("inst_req_id", $instReq->id)->delete();
                foreach ($columns as $column) {
                    foreach ($bsInfo as &$bs) {
                        if (!array_key_exists($column, $bs)) {
                            unset($bs[$column]);
                        }
                        $bs['inst_req_id'] = $instReq->id;
                    }
                }
                $result = InstReqBs::insertAll($bsInfo, false);
            }
            if (!$result) {
                $instReq = null;
            } else {
                $blockStores = json_decode(json_encode($result), true); // convert array of object to array
                $instReq->block_stores = $blockStores;
                $contents = [
                    'tbl_id'  => $instReq->id,
                    'tbl_name'=> $instReq->getTable(),
                    'changes' => [
                        'old' => null,
                        'new' => $instReq->toArray()
                    ]
                ];
                Logger::info(json_encode($contents), LOG_TYPES['AWS'], LOG_ACTIONS['INST_REQ']);
            }
        });

        return $instReq;
    }

    private function updateInfos($instInfo)
    {
        $changeRequest = InstReqChange::where('cid', $_SESSION['cid'])
            ->where('inst_req_id', $this->id)->first();
        if (!$changeRequest) {
            $changeRequest = new InstReqChange();
        }
        $editableFields = [
            'dept',
            'pcode',
            'using_purpose',
            'using_from_date',
            'using_till_date',
            'inst_address'
        ];
        $changeRequest->setValues([
            'inst_req_id'     => $this->id,
            'cid'             => $_SESSION['cid'],
            'empid'           => $_SESSION['empid'],
        ]);
        foreach ($editableFields as $field) {
            $changeRequest->setAttribute($field, $instInfo[$field]);
        }

        return $changeRequest->save();
    }

    public function changeStatus($newStatus, $note = null, $logging = true)
    {
        $oldStatus = $this->approved_status;
        if ($newStatus == $oldStatus && $note === $this->note) {
            return true;
        }
        $oldNote = $this->note;

        $this->setValues([
            'approved_status' => (int)$newStatus,
            'note' => $note,
        ]);

        $result = $this->saveWithoutEvents(function () {
            return $this->save();
        });

        if (!$result) {
            return false;
        }

        if (($oldStatus === INST_REQ_PENDING && $newStatus === INST_REQ_APPROVED) ||
           (($oldStatus === INST_REQ_APPROVED || $oldStatus === INST_REQ_DELETE_PENDING) &&
             $newStatus === INST_REQ_DELETED || $logging === false)) {
            return $result;
        }

        $logContents = [
            'tbl_name' => $this->getTable(),
            'tbl_id'   => $this->id,
            'changes'  => [
                'old' => [
                    'approved_status' => $oldStatus,
                    'note' => $oldNote,
                ],
                'new' => [
                    'approved_status' => $newStatus,
                    'note' => $note,
                ]
            ]
        ];
        Logger::info(json_encode($logContents), LOG_TYPES['AWS'], LOG_ACTIONS['UPDATE']);

        return $result;
    }

    public function empids()
    {
        return $this->belongsTo('Akatsuki\Models\Emp', 'empid', 'empid')
            ->where('cid', $this->cid);
    }
}
