<?php
namespace Akatsuki\Models;

class EbsReq extends BaseModel
{
    protected $table = 'ebs_req';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }
}
