<?php
namespace Akatsuki\Models;

use AwsServices\Ec2;

class Instance extends BaseModel
{
    protected $primaryKey = 'tbl_id';

    protected $table = 'instance';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'インスタンスＩＤ',
            'name'             => 'インスタンス名',
            'type'             => 'インスタンスタイプ',
            'platform'         => 'プラットフォーム',
            'sg'               => 'セキュリティグループ',
            'imageid'          => 'ＡＭＩ',
            'availabilityzone' => 'データセンター',
        ];
    }

    public static function updateInstances($cid = null, $region = null)
    {
        if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1) {
            return false;
        }
        empty($cid) && $cid = $_SESSION['cid'];
        empty($region) && $region = $_SESSION['region'];
        $ec2Client = Ec2::getInstance($region, $cid);
        $reservations = $ec2Client->describeInstances();
        $addressElasticIp = $ec2Client->describeAddresses();
        $ipList = array_column($addressElasticIp, 'PublicIp');
        if (!$reservations) {
            return false;
        }

        $instances = self::select('id', 'pcode', 'inst_req_id')->where('cid', $cid)->get()->toArray();
        $instanceMaps = [];
        foreach ($instances as $instance) {
            $instanceID = $instance['id'];
            unset($instance['id']);
            $instanceMaps[$instanceID] = $instance;
        }
        $optionalKeys = [
            'KernelId',
            'RamdiskId',
            'PrivateIpAddress',
            'PublicIpAddress',
        ];

        $batchData = [];
        foreach ($reservations as $reservation) {
            $instances = $reservation['Instances'];
            foreach ($instances as $instance) {
                if ($instance['State'] === 'shutting-down' || $instance['State'] === 'terminated') {
                    continue;
                }
                $instanceName = $instReqID = $securityGroupId = $pcode = null;
                $instanceID = $instance['InstanceId'];
                if (!empty($instance['Tags'])) {
                    foreach ($instance['Tags'] as $tag) {
                        if ($tag['Key'] == 'Name') {
                            $instanceName = $tag['Value'];
                            break;
                        }
                    }
                }

                if (array_key_exists($instanceID, $instanceMaps)) {
                    $info = $instanceMaps[$instanceID];
                    $instReqID = (int)$info['inst_req_id'];
                    $pcode = $info['pcode'];
                }

                $securityGroups = array_pop($instance['SecurityGroups']);
                if ($securityGroups) {
                    $securityGroupId = $securityGroups['GroupId'];
                }

                foreach ($optionalKeys as $key) {
                    if (!array_key_exists($key, $instance)) {
                        $instance[$key] = null;
                    }
                }

                $networkInterface = array_pop($instance['NetworkInterfaces']);
                if ($networkInterface) {
                    $networkInterfaceId = $networkInterface['NetworkInterfaceId'];
                } else {
                    $networkInterfaceId = null;
                }

                if (in_array($instance['PublicIpAddress'], $ipList)) {
                    $elasticIp = $instance['PublicIpAddress'];
                } else {
                    $elasticIp = null;
                }

                $batchData[] = [
                    'id'                => $instanceID,
                    'cid'               => (int)$cid,
                    'name'              => $instanceName,
                    'type'              => $instance['InstanceType'],
                    'sg'                => $securityGroupId,
                    'platform'          => $instance['Architecture'],
                    'kernelid'          => $instance['KernelId'],
                    'ramdiskid'         => $instance['RamdiskId'],
                    'imageid'           => $instance['ImageId'],
                    'state'             => $instance['State']['Name'],
                    'privatednsname'    => $instance['PrivateDnsName'],
                    'publicdnsname'     => $instance['PublicDnsName'],
                    'launchtime'        => $instance['LaunchTime'],
                    'availabilityzone'  => $instance['Placement']['AvailabilityZone'],
                    'privateip'         => $instance['PrivateIpAddress'],
                    'publicip'          => $instance['PublicIpAddress'],
                    'rootdevicename'    => $instance['RootDeviceName'],
                    'pcode'             => $pcode,
                    'inst_req_id'       => $instReqID,
                    'elasticip'         => $elasticIp,
                    'lastchecked'       => "NOW()",
                    'interface_id'      => $networkInterfaceId,
                ];
            }
        }

        $instances = self::beginTransaction(function () use ($cid, $batchData) {
            self::where('cid', $cid)->delete();
            return self::insertAll($batchData, false);
        });
        if ($instances) {
            return json_decode(json_encode($instances), true);
        }
        return false;
    }
}
