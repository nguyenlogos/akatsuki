<?php
namespace Akatsuki\Models;

class Obj extends BaseModel
{
    protected $table = 'obj';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name'        => 'オブジェクト名',
            'ipaddr'      => 'ＩＰアドレスレンジ',
            'submask'     => 'サブネットマスク',
            'description' => '説明'
        ];
    }

    public function keys()
    {
        return [
            'name'
        ];
    }
}
