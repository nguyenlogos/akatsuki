<?php
namespace Akatsuki\Models;

class SgInbound extends BaseModel
{
    protected $table = 'sg_inbound';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'id',
            'id_security_group' => 'id_security_group',
            'inb_type'          => 'inb_type',
            'inb_protocol'      => 'inb_protocol',
            'inb_port_range'    => 'inb_port_range',
            'inb_cidr_ip'       => 'inb_cidr_ip',
            'inb_source'        => 'inb_source',
            'inb_description'   => 'inb_description',
            'create_time'       => 'create_time',
        ];
    }
}
