<?php
namespace Akatsuki\Models;

class SgTemp extends BaseModel
{
    protected $table = 'sg_temp';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }
}
