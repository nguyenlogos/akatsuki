<?php
namespace Akatsuki\Models;

class InsttypeEnable extends BaseModel
{
    use BatchTrait;

    protected $table = 'insttype_enable';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cid' => 'cid',
            'name' => 'name',
            'id' => 'id',
        ];
    }

    public static function enable($cid, $list = array())
    {
        return static::batchEnable($cid, $list);
    }

    public static function disable($cid, $list = array())
    {
        return static::batchDisable($cid, $list);
    }
}
