<?php
namespace Akatsuki\Models;

class Proj extends BaseModel
{
    protected $table = 'proj';

    /**
     * Returns columns mapping
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'pcode'     => 'プロジェクトコード',
            'pname'     => 'プロジェクト名',
            'dept'      => '所属グループ',
        ];
    }

    public function keys() {
        return [
            'pcode'
        ];
    }

    public function relations() {
        return [
            'dept' => 'deptname'
        ];
    }

    public function depts()
    {
        return $this->belongsTo('Akatsuki\Models\Dept', 'dept', 'dept')
            ->where('cid', $this->cid);
    }
}