<?php

namespace App;

use Pimple\Container;

class Migration extends \Phpmig\Migration\Migration
{
    public function getContainer()
    {
        $container = new Container();
        $container['db'] = function () {
            $connectionStr = sprintf('pgsql:dbname=%s;host=%s', DB_NAME, DB_SERVER);
            $dbh = new \PDO($connectionStr, DB_USER, DB_PASSWORD);
            $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $dbh;
        };

        $container['phpmig.adapter'] = function ($c) {
            return new Adapter\PDO\SqlPgsql($c['db'], 'migrations', 'migrationschema');
        };

        $container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

        return $container;
    }

    public function exec($sql)
    {
        return $this->getContainer()['db']->exec($sql);
    }
}
