<?php
/**
 * This file is part of phpmig
 *
 * Copyright (c) 2011 Dave Marshall <dave.marshall@atstsolutuions.co.uk>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
require_once 'migdef.php';
require_once ROOT_DIR . '/vendor/autoload.php';

$dbconfig = __DIR__ . '/../config/dbconfig.local.php';
if (!is_readable($dbconfig)) {
    $dbconfig = __DIR__ . '/../config/dbconfig.php';
}
require_once $dbconfig;

$app = new Phpmig\Console\PhpmigApplication(PHPMIG_VERSION);
$app->run();
