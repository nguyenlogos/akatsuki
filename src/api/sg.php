<?php
$type = getreq('type');
$list = [];
switch ($type) {
    case 'obj':
        $list = \Akatsuki\Models\Obj::where('cid', $_SESSION['cid'])
            ->where('status', 0)->get()->toArray();
        break;
    case 'avail':
        $vpc = getreq('vpc');
        $list = \Akatsuki\Models\Sg::getListAvailable($vpc);
        break;
}
sendAjaxResponse($list);
