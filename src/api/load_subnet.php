<?php
use AwsServices\Ec2;

$vpcid = postreq("vpc");
if (!empty($vpcid)) {
    $selected = postreq("selected_subnet");
    $options = [];

    $ec2Config = [
        'credentials' => [
            'key' => $_SESSION['key'],
            'secret' => $_SESSION['secret']
        ],
        'region' => 'ap-northeast-1', // Tokyo
        'version' => 'latest',
    ];
    $ec2Client = new Ec2($ec2Config);
    $subnets = $ec2Client->describeSubnets([
        'Filters' => [
            [
                'Name' => 'vpc-id',
                'Values' => [$vpcid]
            ],
        ],
    ]);
    foreach ($subnets as $subnet) {
        $subnetId = $subnet['SubnetId'];
        $isSelected = $subnetId === $selected ? "selected" : "";
        $options[] = "
            <option value='{$subnetId}' {$isSelected}>
                {$subnetId}&nbsp;({$subnet['CidrBlock']})
            </option>
        ";
    }
    $options = implode("\n", $options);
    echo $options;
} else {
    echo "";
}
