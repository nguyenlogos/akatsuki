<?php
use Akatsuki\Models\AzoneEnable;

$regions = AzoneEnable::getAvailableRegion($_SESSION['cid']);
$regionList = [];
foreach ($regions as $region) {
    $regionList[] = [
        'name' => $region,
        'name_jp' => local_regionName($region)
    ];
}
sendAjaxResponse($regionList);
