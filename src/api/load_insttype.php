<?php
$isCurGen = (int)\Akatsuki\Models\Configs::getConfig('insttype_curgen');
$selectedAZ = postreq('azone');
if ($selectedAZ == '') {
    sendAjaxResponse([
        'data' => "[]",
        'selected' => ''
    ]);
}
if ($isCurGen === 0) {
    $sql = "
        SELECT
            a.*,
            p.price
        FROM
            insttype a
        INNER JOIN
            insttype_enable b
            ON a.name = b.name
            AND b.cid = {$_SESSION["cid"]}
        LEFT JOIN
            insttype_pricing p
            ON p.cid = {$_SESSION["cid"]}
            AND p.name = a.name
            AND p.region = '{$selectedAZ}'
        ORDER BY
            p.price
    ";
} else {
    $sql = "
        SELECT
            a.*,
            p.price
        FROM
            insttype a
        LEFT JOIN
            insttype_pricing p
            ON p.cid = {$_SESSION["cid"]}
            AND p.name = a.name
            AND p.region = '{$selectedAZ}'
        WHERE
            a.curgen = 'Yes'
        ORDER BY
            p.price
    ";
}
$data = \Akatsuki\Models\BaseModel::selectRaw($sql);

// $retStr = "<select class='form-control' name='" . $name . "'>\n";
// $retStr .= "<option value=''>--- インスタンスタイプを選択 ---</option>\n";

// $retStr = "";
$selected = postreq('selected_insttype');
$list = array();
foreach ($data as $key => $value) {
    if (empty($value['price'])) {
        $value['price'] = "N/A";
    }
    $item = [
        "text" => [
            "name"   => $value['name'],
            "cpu"    => "x{$value['cpu']} vCPU",
            "memory" => "{$value['memory']} GiB",
            "storage"=> $value['storage'],
            "price" => "{$value['price']} (USD/h)"
        ],
        "value" => $value['name'],
    ];
    $list[] = json_encode($item);
}
$list = implode(",", $list);

sendAjaxResponse([
    'data' => "[$list]",
    'selected' => $selected
]);
