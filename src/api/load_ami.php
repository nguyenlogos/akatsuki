<?php
$azone = postreq('azone');
if (empty($azone) || empty($_SESSION['cid'])) {
    exit();
}
$cid = $_SESSION['cid'];
$region = preg_replace('/(.+\d)[a-z]$/', '$1', $azone);
$selectedAMI = postreq('selected_ami');
$sql = "
    SELECT
        a.name,
        a.imageid,
        a.platform,
        a.architecture
    FROM
        ami a
        INNER JOIN
            ami_enable b
            ON a.imageid = b.imageid
            AND a.cid = b.cid
    WHERE
        a.cid = $1
        AND a.region = $2
";
$logs[] = $sql;
$r = pg_query_params($smarty->_db, $sql, [
    $cid,
    $region
]);
if (!$r || pg_num_rows($r) === 0) {
    exit();
}
$amiList = pg_fetch_all($r);
$htmlStr = "";
foreach ($amiList as $ami) {
    $selected = $selectedAMI === $ami['imageid'] ? "selected" : "";
    $dispName = "{$ami['name']} {$ami['platform']} {$ami['architecture']} {$ami['imageid']}";
    $htmlStr .= "<option value='{$ami['imageid']}' {$selected}>{$dispName}</option>\n";
}
echo $htmlStr;
raise_sql($logs, 'load_ami');
exit();
