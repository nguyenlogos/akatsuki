<?php
use Aws\Exception\AwsException as AwsException;
use Common\Logger;

$result = [
    'err'   => true,
    'msg'   => '',
    'state' => '',
];

$instid = postreq('instid');
$exectedState = postreq('state');
if ($instid && in_array($exectedState, AWS_INST_STATES)) {
    $instance = \Akatsuki\Models\Instance::where('cid', $_SESSION['cid'])
        ->where('id', $instid)
        ->first();
    if (!$instance) {
        $result['err'] = MESSAGES['ERR_DATA_NOT_FOUND'];
        sendAjaxResponse($result);
    }

    // check permission
    $instanceRoles = permission_check("cons/inst.php");
    $isInstanceGranted = false;
    if ($instanceRoles['start_stop']['allowed']) {
        $ssCondition = $instanceRoles['start_stop']['condition'];
        if ($ssCondition == 0) {
            $isInstanceGranted = true;
        } else {
            $isInstanceGranted = $row['dept'] == $_SESSION['dept'];
            if ($isInstanceGranted && $ssCondition == 5) {
                $assignedProjs = \Akatsuki\Models\EmpProj::getListAssigned();
                $isInstanceGranted = in_array($row['pcode'], $assignedProjs);
            }
        }
    }
    if (!$isInstanceGranted) {
        $result['msg'] = MESSAGES['ERR_INST_UNAUTHORIZED'];
        sendAjaxResponse($result);
    }

    $instRegion = $instance->availabilityzone;
    $ec2Client = \AwsServices\Ec2::getInstance($instRegion);
    $currentState = $ec2Client->checkInstanceState($instid);
    if (!$currentState) {
        $result['err'] = true;
        $msg = aws_get_last_error();
        if ($msg) {
            $result['msg'] = $msg;
        }
        sendAjaxResponse($result);
    } elseif ($currentState == 'running' || $currentState == 'stopped') {
        if ($exectedState === $currentState) {
            $result['err'] = false;
        } else {
            // logging
            $logAction = '';
            $logType = LOG_TYPES['AWS'];
            $logContents = [
                'inst_id' => $instid
            ];

            // start/stop instance
            if ($currentState === 'running') {
                $states = $ec2Client->stopInstances([$instid]);
                $logAction = LOG_ACTIONS['INST_STOP'];
            } else {
                $states = $ec2Client->startInstances([$instid]);
                $logAction = LOG_ACTIONS['INST_START'];
            }
            if (!$states) {
                $msg = aws_get_last_error();
                if ($msg) {
                    $result['msg'] = $msg;
                }
                sendAjaxResponse($result);
            }
            $currentState = $states[0]['CurrentState']['Name'];
            Logger::info(json_encode($logContents), $logType, $logAction);

            // send notification email
            if ($instance->inst_req_id) {
                $sql = "
                    SELECT e.email
                    FROM inst_req ir
                    INNER JOIN emp e
                        ON e.cid = ir.cid
                        AND e.empid = ir.empid
                    WHERE ir.cid = {$_SESSION['cid']}
                        AND ir.id = {$instance->inst_req_id}
                ";
                $info = \Akatsuki\Models\InstReq::selectRaw($sql);
                if (!empty($info['email'])) {
                    $mailTo = $info['email'];
                    $emailConfig = (int)\Akatsuki\Models\Configs::getConfig('email_instance');
                    if ($emailConfig == 1) {
                        $instanceState = $logAction == LOG_ACTIONS['INST_STOP'] ? "stopped" : "started";
                        $mailer = new \Common\Mailer();
                        $mailBody = sprintf("Instance <b>[%s]</b> has been %s", $instance->id, $instanceState);
                        $mailer
                            ->set('subject', "[Sunny View] Your instance has been been {$instanceState}.")
                            ->set('to', $mailTo)
                            ->set('body', $mailBody);
                        $mailResult = $mailer->send();
                    }
                }
            }
        }
    }
    $result['err'] = false;
    $result['state'] = $currentState;
} else {
    $result['err'] = MESSAGES['ERR_PARAMS_MISSING'];
}

sendAjaxResponse($result);
