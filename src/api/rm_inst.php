<?php
use Aws\Exception\AwsException as AwsException;
use AwsServices\Ec2;

use Common\Logger;
use Akatsuki\Models\Instance;

$result = [
    'result'  => false,
    'message' => ''
];

$requestMethod = $_SERVER['REQUEST_METHOD'];

$getInfoElasticIps = function ($ec2Client, $instanceIDs) {
    if (!is_array($instanceIDs) || count($instanceIDs) === 0) {
        return;
    }
    $ElasticIPs = [];
        $result = $ec2Client->describeAddresses([
            'Filters' => [
                [
                    'Name'   => 'instance-id',
                    'Values' => $instanceIDs,
                ],
            ],
        ])->toArray();
    foreach ($result['Addresses'] as $address) {
        $InstanceId = $address['InstanceId'];
        unset($address['InstanceId']);
        $ElasticIPs[$InstanceId] = $address;
    }
    return $ElasticIPs;
};

$releaseAddress = function ($ec2Client, $allocationId) {
    return $ec2Client->releaseAddress([
        "AllocationId" => $allocationId,
    ]);
};

$terminateInstances = function ($ec2Client, $instanceIDs) {
    if (!is_array($instanceIDs) || count($instanceIDs) === 0) {
        return;
    }
    return $ec2Client->terminateInstances([
        "InstanceIds" => $instanceIDs
    ]);
};

$disassociateAddress = function ($ec2Client, $publicIpAddress) {
    return $ec2Client->disassociateAddress([
        'PublicIp' => $publicIpAddress,
    ]);
};

$waitForAddressess = function ($ec2Client, $publicIps, $retryCount = 5) use (&$waitForAddressess) {
    $result = $ec2Client->describeAddresses([
        'Filters' => [
            [
                'Name'   => 'public-ip',
                'Values' => $publicIps,
            ],
        ],
    ])->toArray();
    $ElasticIPs = $result['Addresses'];
    $available = true;
    foreach ($ElasticIPs as $Ip) {
        if (!empty($Ip['AssociationId'])) {
            $available = false;
            break;
        }
    }
    if ($available) {
        return true;
    };
    if ($retryCount == 0) {
        return false;
    }
    sleep(3);
    return $waitForAddressess($ec2Client, $publicIps, --$retryCount);
};

$removeInstances = function ($smarty, $instanceIDs) {
    if (!is_array($instanceIDs) || count($instanceIDs) === 0) {
        return false;
    }
    $instanceIDs = array_map(function ($item) {
        $item = pg_escape_string($item);
        return "'{$item}'";
    }, $instanceIDs);
    $instanceIDs = implode(',', $instanceIDs);
    $sql = "
        DELETE FROM instance WHERE id IN({$instanceIDs});
    ";
    return !!pg_query($smarty->_db, $sql);
};

if ($requestMethod === 'POST') {
    $reqID = (int)postreq('id');
    if ($reqID > 0) {
        $sql = '
            SELECT e.email FROM inst_req ir
            LEFT JOIN emp e
                ON e.cid = ir.cid
                AND e.empid = ir.empid
            WHERE ir.id = $1
        ';
        $r = pg_query_params($smarty->_db, $sql, [
            $reqID
        ]);
        $instReq = pg_fetch_assoc($r);
        if (!$instReq) {
            sendAjaxResponse([
                'result'  => false,
                'message' => MESSAGES['ERR_DATA_NOT_FOUND']
            ]);
        }

        Instance::updateInstances();

        $instances = Instance::where('cid', $_SESSION['cid'])
            ->where('inst_req_id', $reqID)
            ->where('state', 'running')
            ->get()->toArray();
        if (count($instances) === 0) {
            sendAjaxResponse([
                'result'  => true,
                'message' => ''
            ]);
        };
        $instanceIDs = array_column($instances, 'id');
        $ec2Region   = $instances[0]['availabilityzone'];
        $ec2Client   = getEc2Client($smarty, $_SESSION['cid'], $ec2Region);

        $logType   = LOG_TYPES['AWS'];
        $logAction = LOG_ACTIONS['INST_REMOVE'];
        $logContents = [
            'tbl_mame'  => 'instance',
            'inst_info' => $instances
        ];
        try {
            $elasticIPs = $getInfoElasticIps($ec2Client, $instanceIDs);

            if ($elasticIPs) {
                $publicIPs = array_column($elasticIPs, 'PublicIp');
                foreach ($publicIPs as $publicIP) {
                    $disassociateAddress($ec2Client, $publicIP);
                }
                $done = $waitForAddressess($ec2Client, $publicIPs);
                if (!$done) {
                    $result['message'] = MESSAGES['ERR_ADDRESS_DISASSOCIATE'];
                    sendAjaxResponse($result);
                }
                foreach ($elasticIPs as $instanceID => $address) {
                    $releaseAddress($ec2Client, $address['AllocationId']);
                }
            }

            $terminateInstances($ec2Client, $instanceIDs);

            Logger::info(json_encode($logContents), $logType, $logAction);

            $emailConfig = \Akatsuki\Models\Configs::getConfig('email_instance');
            if ($emailConfig == 1) {
                // send notification email
                $mailer = new \Common\Mailer();
                $mailTo = $instReq['email'];
                $mailBody = sprintf("Instance(s) [%s] have been terminated.\n", implode(",", $instanceIDs));
                $mailer
                    ->set('subject', "[Sunny View] Your instance(s) have been terminated.")
                    ->set('to', $mailTo)
                    ->set('body', $mailBody);
                $mailResult = $mailer->send();
            }

            $removeInstances($smarty, $instanceIDs);

            $result['result'] = true;
        } catch (AwsException $e) {
            $result['message'] = $logContents['err_msg'] = $e->getAwsErrorMessage();
            Logger::error(json_encode($logContents), $logType, $logAction);
        }
    } else {
        $result['message'] = MESSAGES['ERR_PARAMS_MISSING'];
    }
} else {
    $result['message'] = MESSAGES['ERR_METHOD_NOT_ALLOWED'];
}
sendAjaxResponse($result);
