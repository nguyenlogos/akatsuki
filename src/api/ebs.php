<?php
use AwsServices\Ec2;
use Akatsuki\Models\Proj;
use Akatsuki\Models\Instance;
use Akatsuki\Models\EbsReq;
use Akatsuki\Models\EbsLog;
use Akatsuki\Models\EbsReqDetail;
use Aws\Exception\AwsException as AwsException;

$action = getreq('action');
$reqid = (int)getreq('reqid');
// TODO: check permissions
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $result = [
        'err' => 0,
        'msg' => ''
    ];
    if ($action === 'approve') {
        $requestId = (int)postreq('reqid');
        if ($requestId) {
            $sql = sprintf("
                SELECT
                    er.instance_id,
                    i.availabilityzone,
                    erd.*
                FROM
                    ebs_req er
                LEFT JOIN
                    ebs_req_detail erd
                    ON erd.ebs_req_id = er.id
                    AND erd.status = 0
                LEFT JOIN
                    instance i
                    ON i.id = er.instance_id
                    AND i.cid = er.cid
                WHERE
                    er.id = %s
                    AND er.cid = %s
                    AND er.status = 0
                ORDER BY
                    erd.change_type DESC
            ", $requestId, $_SESSION['cid']);
            $changes = EbsReqDetail::selectRaw($sql);
            if (!$changes) {
                sendAjaxResponse([
                    'err' => 1,
                    'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
                ]);
            }

            $instanceId = $changes[0]['instance_id'];
            $instanceZone = $changes[0]['availabilityzone'];
            $ec2Client = Ec2::getInstance($instanceZone);
            $instanceVolumes = $ec2Client->describeVolumes([
                'Filters' => [
                    [
                        'Name'   => 'attachment.instance-id',
                        'Values' => [$instanceId]
                    ],
                    [
                        'Name'   => 'attachment.status',
                        'Values' => ['attaching', 'attached']
                    ]
                ]
            ]);

            $usedDevices = [];
            $ebsDevices = []; // log device list to ebs_log
            $attachmentsInfo = [];
            foreach ($instanceVolumes as $ebsVolume) {
                $attachment = array_shift($ebsVolume['Attachments']);
                if (!$attachment) {
                    continue;
                }

                $device = $attachment['Device'];
                $delTerm = (int)$attachment['DeleteOnTermination'];
                $item = [
                    'instid'    => $ebsVolume['VolumeId'],
                    'device'    => $device,
                    'size'      => $ebsVolume['Size'],
                    'type'      => $ebsVolume['VolumeType'],
                    'iops'      => (int)$ebsVolume['Iops'],
                    'encrypted' => (int)$ebsVolume['Encrypted'],
                    'attached'  => 1,
                    'del_term'  => $delTerm,
                    'is_root'   => 0,
                    'state'     => $ebsVolume['State'],
                    'zone'      => $ebsVolume['AvailabilityZone'],
                    'can_modify'=> 1,
                    'ebs_req_id'=> $requestId
                ];
                $ebsDevices[] = $item;
                $attachmentsInfo[$ebsVolume['VolumeId']] = $attachment;
            }

            $availableVolumes = $ec2Client->getAvailableVolumes($instanceZone);
            $availableVolumeIds = [];
            foreach ($availableVolumes as $volume) {
                $availableVolumeIds[] = $volume['VolumeId'];
            }

            $listNew = [];
            $listAttach = [];
            $listDetach = [];
            $listModify = [];

            $ebsVolumes = array_merge($instanceVolumes, $availableVolumes);
            $checkDeviceName = function ($device, $attachmentsInfo) {
                $matchedInfo = null;
                foreach ($attachmentsInfo as $info) {
                    if ($info['Device'] === $device) {
                        $matchedInfo = $info;
                        break;
                    }
                }
                if ($matchedInfo) {
                    sendAjaxResponse([
                        'err' => 1,
                        'msg' => "Same device name has already been used. ({$device})"
                    ]);
                }
            };
            foreach ($changes as $volume) {
                if ($volume['change_type'] === 'detach') {
                    $attachmentInfo = array_key_exists($volume['volume_id'], $attachmentsInfo) ?
                        $attachmentsInfo[$volume['volume_id']] : null;
                    if ($attachmentInfo) {
                        $listDetach[] = $volume;
                        unset($attachmentsInfo[$volume['volume_id']]);
                    }
                } else {
                    if (!preg_match('/^vol-/', $volume['volume_id'])) {
                        $checkDeviceName($volume['device_name'], $attachmentsInfo);
                        $listNew[] = $volume;
                        $attachmentsInfo[$volume['id']] = [
                            'Device' => $volume['device_name']
                        ];
                    } else {
                        $matchedVolume = null;
                        foreach ($ebsVolumes as $ebsVolume) {
                            if ($ebsVolume['VolumeId'] === $volume['volume_id']) {
                                $matchedVolume = $ebsVolume;
                                break;
                            }
                        }
                        if (!$matchedVolume) {
                            $result['err'] = 1;
                            $result['msg'] = 'Volume ['.$volume['volume_id'].'] is not available.';
                            break;
                        }
                        if ($matchedVolume['Size'] != (int)$volume['volume_size']) {
                            $listModify[] = $volume;
                        }
                        if (count($matchedVolume['Attachments'])) {
                            $attachment = $matchedVolume['Attachments'][0];
                            if ($attachment['Device'] !== $volume['device_name']) { // device name will be changed
                                $checkDeviceName($volume['device_name'], $attachmentsInfo);
                                $listDetach[] = $volume;
                                $listAttach[] = $volume;
                                $attachmentsInfo[$volume['volume_id']]['Device'] = $volume['device_name'];
                            }
                        } else {
                            $checkDeviceName($volume['device_name'], $attachmentsInfo);
                            $listAttach[] = $volume;
                            $attachmentsInfo[$volume['volume_id']] = [
                                'Device' => $volume['device_name']
                            ];
                        }
                    }
                }
            }
            if ($result['err']) {
                sendAjaxResponse($result);
            }
            $ebsDevices = \Akatsuki\Models\BaseModel::beginTransaction(function () use ($ebsDevices) {
                EbsLog::where("ebs_req_id", $requestId)->delete();
                return EbsLog::insertAll($ebsDevices);
            });

            if (count($listModify)) {
                // TODO: check if volume id modifiable
                $promises = [];
                foreach ($listModify as $volume) {
                    $promises[$volume['volume_id']] =
                        $ec2Client->modifyVolumeAsync($volume['volume_id'], $volume['volume_size']);
                }
                try {
                    $promises = \GuzzleHttp\Promise\unwrap($promises);
                } catch (AwsException $e) {
                    aws_handle_exception($e, "[EC2] Modify Volumes");
                    $result['err'] = 1;
                    $result['msg'] = aws_get_last_error();
                    sendAjaxResponse($result);
                }
            }
            if (count($listNew)) {
                foreach ($listNew as $volume) {
                    $volumeConfigs = [
                        'AvailabilityZone' => $volume['availabilityzone'],
                        'Size'             => (int)$volume['volume_size'],
                        'VolumeType'       => $volume['volume_type'],
                    ];
                    if ($volume['volume_type'] === 'io1') {
                        $volumeConfigs['Iops'] = (int)$volume['volume_iops'];
                    }
                    $ebsVolume = $ec2Client->createVolume($volumeConfigs);
                    if ($ebsVolume) {
                        $ebsRequest = EbsReqDetail::where('id', $volume['id'])->first();
                        if ($ebsRequest) {
                            $ebsRequest->setValues([
                                'volume_id' => $ebsVolume['VolumeId'],
                                'state'     => $ebsVolume['State']
                            ]);
                            $ret = $ebsRequest->saveWithoutEvents(function () use (&$ebsRequest) {
                                return $ebsRequest->save();
                            });
                            if ($ret) {
                                $volume['volume_id'] = $ebsVolume['VolumeId'];
                                $volume['state'] = $ebsVolume['State'];
                                $listAttach[] = $volume;
                            }
                        } else {
                            sendAjaxResponse([
                                'err' => 1,
                                'msg' => MESSAGES['ERR_EBS_CREATE']
                            ]);
                        }
                    }
                }
            }
            if (count($listDetach)) {
                foreach ($listDetach as $volume) {
                    $response = $ec2Client->detachVolume($volume['volume_id']);
                    if (!$response) {
                        sendAjaxResponse([
                            'err' => 1,
                            'msg' => MESSAGES['ERR_EBS_DETACH']
                        ]);
                    }
                }
            }
            if (count($listAttach)) {
                foreach ($listAttach as $volume) {
                    $response = $ec2Client->attachVolume($instanceId, $volume['volume_id'], $volume['device_name']);
                    if (!$response) {
                        sendAjaxResponse([
                            'err' => 1,
                            'msg' => MESSAGES['ERR_EBS_ATTACH']
                        ]);
                    }
                }
            }
            $sql = "UPDATE ebs_req SET status = 1 WHERE id = {$requestId}";
            EbsReq::selectRaw($sql);
            $sql = "UPDATE ebs_req_detail SET status = 1 WHERE ebs_req_id = {$requestId}";
            EbsReqDetail::selectRaw($sql);

            sendAjaxResponse([
                'err' => 0
            ]);
        } else {
            sendAjaxResponse([
                'err' => 1,
                'msg' => MESSAGES['ERR_PARAMS_MISSING']
            ]);
        }
    } elseif ($action === 'update_backup_rule') {
        $volid = postreq('id');
        $rule = postreq('rule');
        $gen = empty($rule) ? 0 : (int)postreq('gen');
        $region = postreq('region');

        if (empty($volid) || empty($region)) {
            sendAjaxResponse([
                'err' => 1,
                'msg' => MESSAGES['ERR_PARAMS_MISSING']
            ]);
        }

        $result = Ec2::getInstance($region)->updateEbsBackupRule($volid, $rule, $gen);

        if ($result) {
            sendAjaxResponse([
                'err' => false
            ]);
        } else {
            sendAjaxResponse([
                'err' => true,
                'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
            ]);
        }
    } elseif ($action === 'change') {
        $instanceId = postreq('instance_id');
        $listChange = postreq('list_change') ?: [];
        $listDetach = postreq('list_detach') ?: [];

        if (empty($instanceId)) {
            sendAjaxResponse([
                'err' => 1,
                'msg' => MESSAGES['ERR_PARAMS_MISSING']
            ]);
        }

        if (!$listChange && !$listDetach) {
            if (count($listDetach) === 0) {
                $ebsReq = EbsReq::where('instance_id', $instanceId)
                    ->where('cid', $_SESSION['cid'])
                    ->where('empid', $_SESSION['empid'])
                    ->where('status', 0)
                    ->first();
                if ($ebsReq) {
                    EbsReqDetail::where('ebs_req_id', $ebsReq->id)->delete();
                }
            }
            sendAjaxResponse($result);
        }

        $data = [
            'change' => $listChange ?: [],
            'detach' => $listDetach ?: [],
        ];

        $ebsReq = EbsReq::where('instance_id', $instanceId)
                    ->where('cid', $_SESSION['cid'])
                    ->where('empid', $_SESSION['empid'])
                    ->where('status', 0)
                    ->first();
        if (!$ebsReq) {
            $ebsReq = new EbsReq();
            $ebsReq->setValues([
                "cid"         => $_SESSION['cid'],
                "empid"       => $_SESSION['empid'],
                "instance_id" => $instanceId,
                "updated_at"  => 'now()',
                "status"      => 0,
            ]);
            $result = $ebsReq->save();
            if (!$result) {
                sendAjaxResponse([
                    'err' => 1,
                    'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
                ]);
            }
        }

        $batchData = [];
        foreach ($data as $changeType => $volumes) {
            foreach ($volumes as $volume) {
                $batchData[] = [
                    'ebs_req_id'  => $ebsReq->id,
                    'volume_id'   => $volume['id'],
                    'volume_type' => $volume['type'],
                    'volume_zone' => $volume['zone'],
                    'volume_iops' => $volume['type'] == 'io1' ? (int)$volume['iops'] : 0,
                    'volume_size' => (int)$volume['size'],
                    'device_name' => $volume['device'],
                    'state'       => $volume['state'],
                    'attached'    => (int)filter_var($volume['attached'], FILTER_VALIDATE_BOOLEAN),
                    'autoremove'  => (int)filter_var($volume['del_term'], FILTER_VALIDATE_BOOLEAN),
                    'change_type' => $changeType
                ];
            }
        }

        if (count($batchData)) {
            $result = EbsReqDetail::beginTransaction(function () use ($ebsReq, $batchData) {
                EbsReqDetail::where('ebs_req_id', $ebsReq->id)->delete();
                $data = EbsReqDetail::insertAll($batchData, false);
                if ($data) {
                    $ebsReq->setValues([
                        'empid'      => $_SESSION['empid'],
                        'updated_at' => 'now()'
                    ]);
                    $result = $ebsReq->save();
                    if ($result) {
                        return $data;
                    }
                    return null;
                }
                return null;
            });

            if ($result) {
                sendAjaxResponse([
                    'err' => false
                ]);
            } else {
                sendAjaxResponse([
                    'err' => true,
                    'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
                ]);
            }
        } else {
            $result = EbsReqDetail::where('ebs_req_id', $ebsReq->id)->delete();
            $result && $result = $ebsReq->save();
            if (!$result) {
                sendAjaxResponse([
                    'err' => true,
                    'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
                ]);
            } else {
                sendAjaxResponse([
                    'err' => false
                ]);
            }
        }
    }
} elseif ($reqid > 0 && $action === 'info') {
    // view ebs request by id
    $sql = sprintf("
            SELECT
                er.*,
                e.name as empname,
                i.pcode,
                i.name as instance_name
            FROM
                ebs_req er
            LEFT JOIN
                instance i
                ON i.id = er.instance_id
                AND i.cid = er.cid
            LEFT JOIN
                emp e
                ON e.cid = er.cid
                AND e.empid = er.empid
            WHERE
                er.id = %s
                AND er.cid = %s
            LIMIT 1
    ", $reqid, $_SESSION['cid']);
    $request = EbsReq::selectRaw($sql);
    if ($request) {
        sendAjaxResponse([
            'err' => 0,
            'data' => array_shift($request)
        ]);
    }
    sendAjaxResponse([
        'err' => 1,
        'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
    ]);
} else {
    $itemList = [];
    $ebsVolumes = [];
    $ec2Client = null;
    $inst = null;
    $ebsStatus = 0;
    $reqid = (int)getreq('reqid');
    $ebsReq = null;
    if ($reqid) {
        $ebsReq = EbsReq::where('id', $reqid)->first();
        if (!$ebsReq) {
            sendAjaxResponse([
                'err' => true,
                'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
            ]);
        }
        $ebsStatus = (int)$ebsReq->status;
        $inst = $ebsReq->instance_id;
    } else {
        $inst = getreq('inst');
    }
    if ($ebsStatus === 1) {
        // load old instance device list by request id
        $sql = "
            SELECT
                el.instid as id,
                el.device,
                el.size,
                el.type,
                el.iops,
                el.encrypted,
                el.attached,
                el.del_term,
                el.is_root,
                el.state,
                el.zone,
                el.can_modify
            FROM ebs_log el
            INNER JOIN ebs_req er
                ON er.id = el.ebs_req_id
            WHERE ebs_req_id = {$reqid}
                AND er.status = 1;
        ";
        $ebsVolumes = \Akatsuki\Models\BaseModel::selectRaw($sql);
    } elseif ($inst) {
        // load ebs volumes by instances
        $instance = Instance::select("availabilityzone AS zone", "rootdevicename")
            ->where("cid", $_SESSION["cid"])
            ->where("id", $inst)
            ->first();
        if (!$inst) {
            sendAjaxResponse([
                'err' => true,
                'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
            ]);
        }
        $ec2Client = Ec2::getInstance($instance->zone);
        $ebsVolumes = $ec2Client->describeVolumes([
            'Filters' => [
                [
                    'Name'   => 'attachment.instance-id',
                    'Values' => [$inst]
                ],
                [
                    'Name'   => 'attachment.status',
                    'Values' => ['attaching', 'attached']
                ]
            ]
        ]);
    } elseif ($zone = getreq('zone')) {
        // load available ebs volumes by zone
        $ec2Client = Ec2::getInstance($zone);
        $ebsVolumes = $ec2Client->getAvailableVolumes($zone);
    }

    if ($ebsStatus === 1) {
        $itemList = $ebsVolumes;
    } else {
        foreach ($ebsVolumes as $volume) {
            $attachment = !empty($volume['Attachments']) && count($volume['Attachments']) > 0 ?
                array_shift($volume['Attachments']) : null;
            $volumeId = $volume['VolumeId'];
            $device = "";
            $delTerm = false;
            if ($attachment) {
                $device = $attachment['Device'];
                $delTerm = $attachment['DeleteOnTermination'];
            }
            $isAttached = $device ? true : false;
            $isRoot = $device && $instance->rootdevicename === $device;

            // modification status
            $canModify = false;
            $modifications = $ec2Client->describeVolumesModifications([$volumeId]);
            if ($modifications) {
                $info = array_shift($modifications);
                if ($info['ModificationState'] === 'completed') {
                    $endTime = (array)$info['EndTime'];
                    $endDate = $endTime['date'] . $endTime['timezone'];
                    $endDate = new \DateTime($endDate);
                    $endDate->setTimezone(new DateTimeZone(date_default_timezone_get()));
                    $interval = (new \DateTime)->getTimestamp() - $endDate->getTimestamp();
                    if ($interval > 21600) {
                        $canModify = true;
                    }
                }
            } else {
                $canModify = true;
            }

            $item = [
                'id'        => $volume['VolumeId'],
                'device'    => $device,
                'size'      => $volume['Size'],
                'type'      => $volume['VolumeType'],
                'iops'      => (int)$volume['Iops'],
                'encrypted' => $volume['Encrypted'],
                'attached'  => $isAttached,
                'del_term'  => $delTerm,
                'is_root'   => $isRoot,
                'state'     => $volume['State'],
                'zone'      => $volume['AvailabilityZone'],
                'can_modify'=> $canModify
            ];

            if ($item['is_root']) {
                array_unshift($itemList, $item);
            } else {
                $itemList[] = $item;
            }
        }
    }
    if ($action === 'change' && ($ebsReq || $inst)) {
        $data = [
            'list'        => $itemList,
            'list_change' => [],
            'list_detach' => [],
        ];
        if (count($itemList) > 0) {
            $sql = sprintf("
                SELECT
                    erd.*
                FROM
                    ebs_req er
                LEFT JOIN
                    ebs_req_detail erd
                    ON erd.ebs_req_id = er.id
                WHERE
                    er.id = '%s'
                    AND er.cid = %s
            ", $reqid, $_SESSION['cid']);
            $changes = EbsReqDetail::selectRaw($sql);
            $reupdateList = [];
            foreach ($changes as $volume) {
                $type = $volume['change_type'];
                if (!$type) {
                    continue;
                }
                if ($type === 'detach') {
                    $matchedVolume = null;
                    foreach ($itemList as $item) {
                        if ($item['id'] == $volume['volume_id'] && $item['device'] !== $volume['device_name']) {
                            $matchedVolume = $item;
                            break;
                        };
                    }
                    if ($matchedVolume) {
                        $volume['device_name'] = $matchedVolume['device'];
                        $reupdateList[] = [
                            'ebs_req_id' => $volume['ebs_req_id'],
                            'volume_id' => $matchedVolume['id'],
                            'device_name' => $matchedVolume['device'],
                        ];
                    }
                }
                $item = [
                    'id'        => $volume['volume_id'],
                    'device'    => $volume['device_name'],
                    'size'      => $volume['volume_size'],
                    'type'      => $volume['volume_type'],
                    'iops'      => (int)$volume['volume_iops'],
                    'encrypted' => false,
                    'attached'  => $volume['attached'],
                    'del_term'  => $volume['autoremove'],
                    'state'     => $volume['state'],
                    'zone'      => $volume['volume_zone'],
                ];
                $data['list_'.$type][] = $item;
            }
            if (count($reupdateList)) {
                foreach ($reupdateList as $item) {
                    $sql = sprintf("
                        UPDATE ebs_req_detail
                        SET device_name = '%s'
                        WHERE volume_id = '%s'
                        AND ebs_req_id = %s
                    ", $item['device_name'], $item['volume_id'], $item['ebs_req_id']);
                    EbsReqDetail::selectRaw($sql);
                }
            }
        }

        sendAjaxResponse([
            'err'  => false,
            'data' => $data
        ]);
    }

    sendAjaxResponse([
        'err'  => false,
        'data' => $itemList
    ]);
}
