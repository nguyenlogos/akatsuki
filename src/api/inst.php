<?php
use Akatsuki\Models\Instance;
use Akatsuki\Models\Proj;
use AwsServices\Ec2;

$params = parseAjaxRequestParams();
$action = getreq('action');
if ($action === 'update_info') {
    $instid = postreq('id');
    $dept = (int)postreq('dept');
    $proj = postreq('proj');
    $renameInstance = false;
    if (empty($instid) || ($dept > 0 && empty($proj))) {
        sendAjaxResponse([
            'err' => 1,
            'msg' => MESSAGES['ERR_PARAMS_MISSING']
        ]);
    }
    $instance = Instance::where('id', $instid)
        ->where('cid', $_SESSION['cid'])
        ->first();
    if (empty($instance)) {
        sendAjaxResponse([
            'err' => 1,
            'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
        ]);
    }
    if (empty($proj)) {
        $instance->pcode = null;
        $result = $instance->save();
        if ($result) {
            sendAjaxResponse([
                'err' => 0
            ]);
        }
        sendAjaxResponse([
            'err' => 1,
            'msg' => MESSAGES['ERR_UPDATE']
        ]);
    }

    $project = Proj::where('cid', $_SESSION['cid'])
                ->where('pcode', $proj)
                ->first();
    if (empty($project)) {
        sendAjaxResponse([
            'err' => false,
            'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
        ]);
    }

    $instances = Instance::select(['name', 'id'])
                    ->where('cid', $_SESSION['cid'])
                    ->where('pcode', $proj)
                    ->get();
    $nameList = [];
    foreach ($instances as $inst) {
        $nameList[$inst->name] = $inst->id;
    }

    $instanceName = "";
    $index = 1;
    do {
        $instanceName = sprintf("%s-%05d", $project->pname, $index++);
        if (array_key_exists($instanceName, $nameList)) {
            if ($nameList[$instanceName] === $instid) {
                break;
            }
            continue;
        }
        break;
    } while (true);

    $result = true;
    if ($instanceName !== $instance->name) {
        $clientConfig = array(
            'credentials' => [
                'key'     => $_SESSION["key"],
                'secret'  => $_SESSION["secret"],
            ],
            'region'      => $_SESSION["region"],
            'version'     => 'latest'
        );

        $ec2Client = new Ec2($clientConfig);
        $result = $ec2Client->renameInstance($instid, $instanceName);
    }

    if ($result) {
        $instance->setValues([
            'pcode' => $proj,
            'name' => $instanceName
        ]);
        $result = $instance->save();
    }
    if ($result) {
        sendAjaxResponse([
            'err' => false,
            'inst' => [
                'name' => $instanceName
            ]
        ]);
    } else {
        sendAjaxResponse([
            'err' => true,
            'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
        ]);
    }
} elseif ($action === 'ss') {
    require_once("inst_ss.php");
} else {
    $proj = getreq('proj');
    $insts = Instance::where('cid', $_SESSION['cid'])->where('state', '<>', 'terminated')
        ->where('state', '<>', 'shutting-down');
    if (!empty($proj)) {
        $insts->where('pcode', $proj);
    };
    $insts = $insts->get()->toArray();

    sendAjaxResponse($insts);
}
