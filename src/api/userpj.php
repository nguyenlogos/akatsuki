<?php
use Akatsuki\Models\EmpProj;
use Akatsuki\Models\Emp;

$action = getreq('action');
// TODO: check permissions
if ($action === 'update') {
    $pcode = postreq('pcode');

    if (empty($pcode)) {
        sendAjaxResponse([
            'err' => 1,
            'msg' => MESSAGES['ERR_PARAMS_MISSING']
        ]);
    }

    $checked = postreq('checked') ?: [];
    $unchecked = postreq('unchecked') ?: [];
    $result = EmpProj::beginTransaction(function () use ($pcode, $checked, $unchecked) {
        $removeList = array_merge($checked, $unchecked);
        if (count($removeList)) {
            $result = EmpProj::where('cid', $_SESSION['cid'])
                ->where('pcode', $pcode)
                ->whereIn('empid', $removeList)->delete();
        }
        $result = true;
        if (count($checked)) {
            $batchData = array_map(function ($empid) use ($pcode) {
                return [
                    'cid'   => (int)$_SESSION['cid'],
                    'pcode' => $pcode,
                    'empid' => (int)$empid
                ];
            }, $checked);
            $result = EmpProj::insertAll($batchData);
        }
        return $result;
    });
    if ($result) {
        sendAjaxResponse([
            'err' => false
        ]);
    } else {
        sendAjaxResponse([
            'err' => true,
            'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
        ]);
    }
}
