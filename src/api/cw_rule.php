<?php
use Akatsuki\Models\CwRules;

$action = getreq('action');
if ($action === 'update_state') {
    $name = postreq('name');
    $region = postreq('region');
    $state = (int)postreq('state');
    if (empty($name) || empty($region)) {
        sendAjaxResponse([
            'err' => 1,
            'msg' => MESSAGES['ERR_PARAMS_MISSING']
        ]);
    }

    $rule = CwRules::where('name', $name)
                ->where('region', $region)
                ->where('cid', $_SESSION['cid'])
                ->get()
                ->first();
    if (!$rule) {
        sendAjaxResponse([
            'err' => true,
            'msg' => MESSAGES['ERR_DATA_NOT_FOUND']
        ]);
    }
    $rule->state = $state;
    $result = $rule->putRule();

    if ($result) {
        sendAjaxResponse([
            'err' => false
        ]);
    } else {
        sendAjaxResponse([
            'err' => true,
            'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
        ]);
    }
} else {
    $regions = getreq('regions');
    $forDept = null;
    if ($_SESSION['sysadmin'] != '1') {
        $forDept = $_SESSION['dept'];
    }
    $action = '';
    $rules = CwRules::where('cid', $_SESSION['cid'])
        ->where('state', 1)
        ->where('status', 0);
    if ($forDept) {
        $rules->where('dept', $forDept);
    }
    if (!empty($regions)) {
        if (!is_array($regions)) {
            $regions = [$regions];
        }
        $regions = array_map(function ($region) {
            return preg_replace('/(.+\d)[a-z]$/', '$1', $region);
        }, $regions);

        $rules->whereIn('region', $regions);
    };
    $rules = $rules->orderBy('name', 'ASC')->get()->toArray();

    sendAjaxResponse($rules);
}
