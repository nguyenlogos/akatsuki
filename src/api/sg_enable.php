<?php
use Akatsuki\Models\SgEnable;

$action = getreq('action');
// TODO: check permissions
if ($action === 'update') {
    $checked = postreq('checked') ?: [];
    $unchecked = postreq('unchecked') ?: [];
    $result = SgEnable::beginTransaction(function () use ($checked, $unchecked) {
        $removeList = array_merge($checked, $unchecked);
        if (count($removeList)) {
            $result = SgEnable::where('cid', $_SESSION['cid'])
                ->whereIn('sg_id', $removeList)->delete();
        }
        $result = true;
        if (count($checked)) {
            SgEnable::enable($_SESSION['cid'], $checked);
        }
        return $result;
    });
    if ($result) {
        sendAjaxResponse([
            'err' => false
        ]);
    } else {
        sendAjaxResponse([
            'err' => true,
            'msg' => MESSAGES['ERR_UNKNOWN_ERROR']
        ]);
    }
}
