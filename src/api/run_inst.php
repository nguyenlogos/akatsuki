<?php

use Aws\Exception\AwsException as AwsException;

$result = [
    'result'  => false,
    'message' => MESSAGES['ERR_UNKNOWN_ERROR']
];
$requestMethod = $_SERVER['REQUEST_METHOD'];

if ($requestMethod === 'POST') {
    $reqID = (int)postreq('id');
    if ($reqID > 0) {
        $reqInfo = getInstanceRequestInfo($smarty, $reqID);
        if (!$reqInfo) {
            $result['message'] = MESSAGES['ERR_DATA_NOT_FOUND'];
            sendAjaxResponse($result);
        }

        $groupManagers = getGroupManagers($smarty, $reqInfo['dept']);
        if (!$groupManagers) {
            $result['message'] = MESSAGES['ERR_INST_REQ_INVALID_DEPT'];
            sendAjaxResponse($result);
        }

        $executionCmd = sprintf("nohup php %s/batch/approveInstance.php \
                --cid=%d \
                --empid=%d \
                --reqid=%d > /dev/null 2>&1 &
        ", ROOT_PATH, $_SESSION['cid'], $_SESSION['empid'], $reqID);
        $executionTest = sprintf("php %s/batch/approveInstance.php \
                --cid=%d \
                --empid=%d \
                --reqid=%d
        ", ROOT_PATH, $_SESSION['cid'], $_SESSION['empid'], $reqID);
        // $output = shell_exec($executionTest);
        $output = pclose(popen($executionCmd, 'r'));
        sendAjaxResponse([
            'result' => true,
            'message' => '',
        ]);
    } else {
        $result['message'] = MESSAGES['ERR_PARAMS_MISSING'];
    }
} else {
    $result['message'] = MESSAGES['ERR_METHOD_NOT_ALLOWED'];
}
sendAjaxResponse($result);
