<?php
use Akatsuki\Models\Proj;

$projs = [];

if (!$_SESSION['sysadmin'] && !$_SESSION['admin']) {
    $projs = \Akatsuki\Models\EmpProj::getListAssigned();
    sendAjaxResponse($projs);
}
$deptRoles = permission_check("mst/dept.php");
if (!$_SESSION['sysadmin']) {
    $dept = $_SESSION['dept'];
} else {
    $dept = (int)getreq('dept');
}


if ($dept > 0) {
    $projs = Proj::where('cid', $_SESSION['cid'])
        ->where('status', 0)
        ->where('dept', $dept)
        ->get()->toArray();
} elseif (getreq('all') == '1') {
    $projs = Proj::where('cid', $_SESSION['cid'])
        ->where('status', 0)
        ->get()->toArray();
}

sendAjaxResponse($projs);
