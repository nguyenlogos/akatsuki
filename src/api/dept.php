<?php
use Akatsuki\Models\Dept;

$roles = permission_check("mst/dept.php");
$depts = Dept::where('cid', $_SESSION['cid'])
            ->where('status', 0);
if (!$roles['read']['allowed'] || $roles['read']['condition'] == 1) {
    $depts->where('dept', $_SESSION['dept']);
}
$depts = $depts->get()->toArray();
sendAjaxResponse($depts);
