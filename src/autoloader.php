
<?php
$convert_autoloading_class = function ($className) {
    $dirsMapping = array(
        "Akatsuki\\Models\\" => SOURCE_PATH . "/Models/",
        "Common\\"           => SOURCE_PATH . "/common/",
        "AwsServices\\"      => ROOT_PATH . "/aws_services/",
    );
    $filepath = strtr($className, $dirsMapping);
    return "$filepath.php";
};

spl_autoload_register(function ($className) use ($convert_autoloading_class) {
    $filename = $convert_autoloading_class($className);
    if (is_readable($filename)) {
        require_once($filename);
    }
});
