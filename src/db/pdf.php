<?php
defined("COMMON_PATH") OR define("COMMON_PATH", dirname(__DIR__) . "/common");
require_once(COMMON_PATH . "/CommonClass.class.php");
require_once(COMMON_PATH . "/common.php");

$smarty = new MySmarty();

$YM = getreq("ym");
$cid = ""; // メッセージ
$cid = (int)$_SESSION["cid"];

$sql = sprintf("SELECT filename FROM invoice WHERE cid = $1 AND ym = $2");
$logs[] = $sql;
$r = pg_query_params($smarty->_db, $sql, [
    $cid,
    $YM
]);

if (pg_num_rows($r) != 1) {
  $msg = "該当する請求書ファイルが見つかりませんでした。(S1)";
}

$filename = pg_fetch_result($r, 0, 0);

header("Content-Type: application/force-download");
header("Content-Length: " . filesize(INVOICE_PATH . $filename));
header("Content-Disposition: attachment; filename='". $YM . ".pdf'");
readfile(INVOICE_PATH . $filename);

raise_sql($logs, 'pdf');
$smarty->disconnect();