<?php
defined("COMMON_PATH") OR define("COMMON_PATH", dirname(__DIR__) . "/common");
require_once(COMMON_PATH . "/CommonClass.class.php");
require_once(COMMON_PATH . "/common.php");

require_once(LIBS_PATH . "/tcpdf/tcpdf.php");
require_once(LIBS_PATH . "/tcpdf/fpdi.php");

$smarty = new MySmarty();

$cid = ""; // メッセージ
$cid = (int)$_SESSION["cid"];

$sql = sprintf("SELECT ym, totaljpy FROM invoice WHERE cid = $1 ORDER BY ym DESC");
$logs[] = $sql;
$r = pg_query_params($smarty->_db, $sql, [
    $cid
]);


$ym = array();
$ymstr = array();
$totaljpy = array();
for ($i = 0; $i < pg_num_rows($r);$i++) {
    $row = pg_fetch_row($r,$i);
    array_push($ymstr,substr($row[0], 0, 4) . "年" . substr($row[0], 4, 2) . "月分");
    array_push($ym,$row[0]);
    array_push($totaljpy,$row[1]);
}

raise_sql($logs, 'invoice');
$smarty->disconnect();

$smarty->assign('ymstr', $ymstr);
$smarty->assign('ym', $ym);
$smarty->assign('totaljpy', $totaljpy);
$smarty->assign('count', count($ym));
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('pageTitle', '請求書');
$smarty->assign('viewTemplate', 'db/invoice.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');