<?php

// 年月のプルダウン文字列生成
function getYMlist($Y, $M)
{
    $YMstr = "<select class='form-control' name='Y'>\n";

    $curY = date("Y");
    //$curM = date("m");

    for ($i = 2; $i >= 0; $i--) {
        $YMstr .= ("  <option value='" . ($curY - $i) . "'");
        if (($curY - $i) == $Y) {
            $YMstr .= " selected";
        }
        $YMstr .= (">" . ($curY - $i) . "</option>\n");
    }
    $YMstr .= "</select>\n<span class='d-inline-block'>年</span>\n<select class='form-control' name='M'>\n";
    for ($i = 0; $i < 12; $i++) {
        $YMstr .= ("  <option value='" . ($i + 1) . "'");
        if (($i+1) == $M) {
            $YMstr .= " selected";
        }
        $YMstr .= (">" . ($i + 1) . "</option>\n");
    }
    $YMstr .= "</select>\n<span class='d-inline-block'>月</span>\n";

    return ($YMstr);
}


// 1ヶ月コスト推移
// 指定された年月の「pcode毎の合計」を取得
// dataArr : データの一次元配列 各日のデータはカンマ区切り文字列
// labels : x軸のラベル　ここでは日
// pcodeArr : product codeのテーブル
function getGraphData(
    &$smarty,
    $Y,
    $M,
    $MonthEnd,
    &$labels,
    &$dataArr,
    &$pcodeArr,
    $rate1M,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
) {
    // グラフx軸ラベルの設定
    for ($i = 0,$j=0; $i < $MonthEnd; $i++) {
        if ($i != 0) {
            $labels .= ",";
        }
        $labels .= "'" . str_pad($i+1, 2, 0, STR_PAD_LEFT) . "'";
    }

    // 月間利用サービス一覧の取得
    $conditions = "";
//    if ($TARGET_DEPT) {
//        $conditions .= " AND %s.dept = {$TARGET_DEPT}";
//    }
//    if ($TARGET_PROJ) {
//        $conditions .= " AND %s.pcode = '{$TARGET_PROJ}'";
//    }

    $tablePrefix     = "sc";
    $whereConditions = sprintf($conditions, $tablePrefix, $tablePrefix);
    $sql = sprintf("
        SELECT
            sc.productcode,
            sum(sc.unblendedcost) aaa
        FROM
            cost_detail sc
        WHERE
            sc.cid = $TARGET_CID
            AND sc.lineitemtype != 'Tax'
            AND sc.ym = '{$Y}/{$M}'
            AND sc.unblendedcost != 0
            $whereConditions
        GROUP BY
            sc.productcode
        ORDER BY
            aaa DESC
    ");
    $logs[] = $sql;
    $data = [];
    $r = pg_query($smarty->_db, $sql);
    // dataテーブルを0で初期化
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        for ($j = 0; $j < $MonthEnd; $j++) {
            $data[$i][$j] = 0;
        }
        $row = pg_fetch_row($r, $i);
        $pcodeArr[$i] = getServiceShortName($row[0]);
    }
    // 日毎サービス毎のコスト取得

    $tablePrefix = "C";
    $whereConditions = sprintf($conditions, $tablePrefix, $tablePrefix);
    $sql = sprintf("
        SELECT
            ymd sd,
            ROUND(SUM(ubcost), 2),
            C.servicecode,
            no
        FROM
            stat_cost_daily C,
            (
                SELECT
                    ROW_NUMBER() OVER(
                        ORDER BY
                        SUM(ubcost) DESC
                    ) - 1 AS no,
                    servicecode,
                    SUM(ubcost) bbb
                FROM
                    stat_cost_daily
                WHERE
                    cid = '{$TARGET_CID}'
                    AND servicecode != 'Tax'
                    AND ymd >= '{$Y}/{$M}/01'
                    AND ymd < to_char(date '{$Y}/{$M}/01' + interval '1 month', 'YYYY/MM/DD')
                    AND ROUND(ubcost, 2) != 0
                GROUP BY
                    servicecode
                ORDER BY
                    bbb DESC
            )
            tmp
        WHERE
            C.cid = '{$TARGET_CID}'
            AND ymd >= '{$Y}/{$M}/01'
            AND ymd < to_char(date '{$Y}/{$M}/01' + interval '1 month','YYYY/MM/DD')
            AND C.servicecode != 'Tax'
            AND C.servicecode = tmp.servicecode
            AND ROUND(ubcost, 2) != 0
            $whereConditions
        GROUP BY
            sd, C.servicecode, no
        ORDER BY
            sd, no
    ");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    //     sd    |    sum     |   servicecode   | no
    // ------------+--------------+-----------------+----
    //  2017/12/12 | 1.4076000000 | AmazonEC2      |  0
    //  2017/12/12 |    0.010000 | AWSCostExplorer |  1
    //  2017/12/13 | 0.7296000000 | AmazonEC2      |  0
    //  2017/12/14 | 0.7296000000 | AmazonEC2      |  0
    //  2017/12/15 | 0.7296000000 | AmazonEC2      |  0

    // 1日から月末まで日ごとに処理
    // dataテーブルに利用料を設定
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $row = pg_fetch_row($r, $i);
        $d = str_replace($Y."/".$M."/", "", $row[0]) -1;
        $idx = $row[3];
        $data[$idx][$d] = round($row[1] * $rate1M, 0);
    }

    // dataテーブルをdataArrに展開

    $dataArr or $dataArr = [];
    for ($i=0; $i<count($data); $i++) {
        for ($j=0; $j<$MonthEnd; $j++) {
            if (!isset($dataArr[$i])) {
                $dataArr[$i] = "";
            }
            if ($j != 0) {
                $dataArr[$i] .= ",";
            }

            $dataArr[$i] .= $data[$i][$j];
        }
    }

    // 棒グラフ表示用文字列の生成
    $res = "";
    $colorIdx = 0;
    for ($i = 0; $i < count($dataArr); $i++) {
        if ($i != 0) {
            $res .= ",";
        }

        $res .= "{\n";
        $res .= "    label: '" . $pcodeArr[$i] . "',\n";
        $res .= "    lineTension: 0,\n";
        $res .= "    backgroundColor: '" . getGraphColor($colorIdx, $pcodeArr[$i]) . "',\n";
        $res .= "    pointStyle: 'rect',\n";
        $res .= "    data: [" . $dataArr[$i] . "]\n";
        $res .= "}";
    }
    raise_sql($logs, 'db');

    return($res);
}


// 指定された年月の明細の取得 table用タグ文字列を返す
// あわせてUsageとTaxそれぞれの合計値を返す
// recUsage : Usageの明細 tableタグ形式
// recTax   : Taxの明細 tableタグ形式
// usageTotal : Usage合計
// taxTotal : Tax合計

function getCostList(
    &$smarty,
    $Y,
    $M,
    &$recUsage,
    &$usageTotal,
    &$taxTotal,
    $rate1M,
    &$usageTotalJPY,
    &$taxTotalJPY,
    $TARGET_CID,
    $TARGET_AID
) {
    $usageTotal = 0;
    $taxTotal = 0;
    $usageTotalJPY = 0;
    $taxTotalJPY = 0;

    $sql = sprintf("
        SELECT
            ymd,
            servicecode,
            ubcost
        FROM
            stat_cost_daily
        WHERE
            cid = '{$TARGET_CID}'
            AND ymd >= '{$Y}/{$M}/01'
            AND ymd < to_char(date '{$Y}/{$M}/01' + interval '1 month', 'YYYY/MM/DD')
            AND ubcost != 0
            AND servicecode != 'Tax'
        ORDER BY
            ymd,
            servicecode;
    ");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

   
    for ($i = 0, $iMax = pg_num_rows($r); $i < $iMax; $i++) {
        $row = pg_fetch_row($r, $i);
        $recUsage .= "<tr><td>" . $row[0] . "</td>";
        $recUsage .= "<td>" . $row[1] . "</td>";
        $recUsage .= "<td>" . rtrim($row[2], '0') . "</td></tr>";
    }

    $sql = sprintf("SELECT sum(unblendedcost)
                    FROM cost_detail
                    WHERE cid = " . $TARGET_CID . "
                        AND ym = '" . $Y . "/" . $M . "'
                        AND lineitemtype != 'Tax'");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);
        $usageTotal = $row[0];
        $usageTotalJPY = ceil($row[0] * $rate1M);
    } else {
        $usageTotal = 0;
        $usageTotalJPY = 0;
    }

    $sql = sprintf("SELECT sum(unblendedcost)
                    FROM cost_detail
                    WHERE cid = " . $TARGET_CID . "
                        AND ym = '" . $Y . "/" . $M . "'
                        AND lineitemtype = 'Tax'");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);
        $taxTotal = $row[0];
        $taxTotalJPY = ceil($row[0] * $rate1M);
    } else {
        $taxTotal = 0;
        $taxTotalJPY = 0;
    }
    raise_sql($logs, 'db');
}


// グラフ用の色文字列を返す
function getGraphColor(&$idx, $pcode)
{
    global $ServiceColorTable1,$ServiceColorTable2,$additionalColorTable;

    for ($i=0; $i < count($ServiceColorTable1); $i++) {
        if ($ServiceColorTable1[$i] == $pcode) {
            return($ServiceColorTable2[$i]);
        }
    }

    $idx_fix = $idx % (count($additionalColorTable));

    $idx++;
    return($additionalColorTable[$idx_fix]);
}

// 円グラフ用のデータラベル(labels)、データ文字列(dataStr)、色文字列(colors)を
// 取得
function getDataSetsStr2(
    &$smarty,
    $Y,
    $M,
    &$labels,
    &$colors,
    $rate1M,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
) {
    $dataStr = "";
    $labels = "";
    $colors = "";

    $conditions = "";
//    if ($TARGET_DEPT) {
//        $conditions .= " AND dept = {$TARGET_DEPT}";
//    }
//    if ($TARGET_PROJ) {
//        $conditions .= " AND pcode = '{$TARGET_PROJ}'";
//    }

    $sql = sprintf("
        SELECT
            productcode,
            sum(unblendedcost) aaa
        FROM
            cost_detail
        WHERE
            cid = {$TARGET_CID}
            AND ym = '{$Y}/{$M}'
            AND lineitemtype != 'Tax'
            AND unblendedcost != 0
            $conditions
        GROUP BY
            productcode
        ORDER BY
            aaa DESC
    ");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $colorIdx = 0;
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        if ($i != 0) {
            $dataStr .= ",";
            $labels .= ",";
            $colors .= ",";
        }
        $row = pg_fetch_row($r, $i);
        $dataStr .= round($row[1] * $rate1M, 0);
        $labels .= "'" . getServiceShortName($row[0]) . "'";
        $colors .= "'" . getGraphColor($colorIdx, $row[0]) . "'";
    }
    raise_sql($logs, 'db');

    return($dataStr);
}

// 6ヵ月間ディスク容量グラフデータの取得
function getDiskAmount6M(&$smarty, $labels, $ymTable, $TARGET_CID, $TARGET_AID, $TARGET_DEPT, $TARGET_PROJ)
{
    // JOIN用の添付テーブルの作成
    $valueStr = "";
    for ($i = 0; $i < 6; $i++) {
        if ($i != 0) {
            $valueStr .= ",";
        }
        $valueStr .= "('" . $ymTable[$i] . "')";
    }

    $conditions = "";
    if ($TARGET_DEPT) {
        $conditions .= " AND dept = {$TARGET_DEPT}";
    }
    if ($TARGET_PROJ) {
        $conditions .= " AND pcode = '{$TARGET_PROJ}'";
    }

    // EBS 容量と個数の取得
    $sql = sprintf("
        SELECT
            tmp2.ccc,
            round(tmp.gib, 2),
            tmp.COUNT,
            tmp.YM
        FROM
        (
            SELECT
                YM,
                gib,
                COUNT
            FROM
                stat_ebs
            WHERE
                cid = {$TARGET_CID}
                AND YM >= '{$ymTable[0]}'
                AND YM <= '{$ymTable[5]}'
                $conditions
            ORDER BY
                YM
        ) tmp
        RIGHT OUTER JOIN
            (
                VALUES {$valueStr}
            ) AS tmp2 (ccc)
            ON tmp.YM = tmp2.ccc
        ORDER BY
            tmp2.ccc
    ");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $dataStr = "";
    $dataStr2 = "";
    for ($i = 0, $j=1; $i < 6; $i++) {
        $row = pg_fetch_row($r, $i);
        if ($row[1] == "") {
            $dataStr .= "0,";
        } else {
            $dataStr .= $row[1] . ",";
        }
        if ($row[2] == "") {
            $dataStr2 .= "0,";
        } else {
            $dataStr2 .= $row[2] . ",";
        }
    }

    // S3 容量と個数の取得
    $sql = sprintf("
        SELECT
            tmp2.ccc,
            round(tmp.gib, 2),
            tmp.COUNT,
            tmp.YM
        FROM
        (
            SELECT
                YM,
                gib,
                COUNT
            FROM
                stat_s3
            WHERE
                cid = {$TARGET_CID}
                AND YM >= '{$ymTable[0]}'
                AND YM <= '{$ymTable[5]}'
                $conditions
            ORDER BY
                YM
        ) tmp
        RIGHT OUTER JOIN
            (
                VALUES $valueStr
            ) AS tmp2 (ccc)
            ON tmp.YM = tmp2.ccc
        ORDER BY
            tmp2.ccc
    ");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $dataStr3 = "";
    $dataStr4 = "";
    for ($i = 0, $j=1; $i < 6; $i++) {
        $row = pg_fetch_row($r, $i);
        if ($row[1] == "") {
            $dataStr3 .= "0,";
        } else {
            $dataStr3 .= $row[1] . ",";
        }
        if ($row[2] == "") {
            $dataStr4 .= "0,";
        } else {
            $dataStr4 .= $row[2] . ",";
        }
    }

    $colorIdx = 0;
    $res = "";
    $res .= "{\n";
    $res .= "    label: 'EBSディスク容量(GiB)',\n";
    $res .= "    type: 'line',\n";
    $res .= "    lineTension: 0,\n";
    $res .= "    yAxisID: 'y1',\n";
    $res .= "    fill: false,\n";
    $res .= "    borderColor: '" . getGraphColor($colorIdx, "AmazonEC2") . "',\n";
    $res .= "    data: [" . $dataStr . "]\n";
    $res .= "},";
    $res .= "{\n";
    $res .= "    label: 'S3ディスク容量(GiB)',\n";
    $res .= "    type: 'line',\n";
    $res .= "    lineTension: 0,\n";
    $res .= "    yAxisID: 'y1',\n";
    $res .= "    fill: false,\n";
    $res .= "    borderColor: '" . getGraphColor($colorIdx, "AmazonS3") . "',\n";
    $res .= "    data: [" . $dataStr3 . "]\n";
    $res .= "},";
    $res .= "{\n";
    $res .= "    label: 'EBSディスク個数',\n";
    $res .= "    type: 'bar',\n";
    $res .= "    yAxisID: 'y2',\n";
    $tmp = getGraphColor($colorIdx, "");
    $res .= "    backgroundColor: '" . $tmp . "',\n";
    $res .= "    borderColor: '" . $tmp . "',\n";
    $res .= "    pointStyle: 'rect',\n";
    $res .= "    data: [" . $dataStr2 . "]\n";
    $res .= "},";
    $res .= "{\n";
    $res .= "    label: 'S3ディスク個数',\n";
    $res .= "    type: 'bar',\n";
    $res .= "    yAxisID: 'y2',\n";
    $tmp = getGraphColor($colorIdx, "");
    $res .= "    backgroundColor: '" . $tmp . "',\n";
    $res .= "    borderColor: '" . $tmp . "',\n";
    $res .= "    pointStyle: 'rect',\n";
    $res .= "    data: [" . $dataStr4 . "]\n";
    $res .= "},";
    raise_sql($logs, 'db');

    return ($res);
}

// 6ヵ月間のx軸ラベルの生成
function get6Mlist(&$labels, &$ymTable)
{
    // 現在年月までを表示
    $Y = date("Y");
    $M = date("m");

    // グラフx軸ラベルの設定
    $startDate = strtotime($Y . "-" . $M . "-01 -6 month");

    $labels = "";
    $ymTable = array();

    // X軸ラベル YYYY/MM x 6 の生成
    for ($i = 0; $i < 6; $i++) {
        if ($i != 0) {
            $labels .= ",";
        }
        $tmpDate = strtotime("+" . ($i+1) . " month", $startDate);
        $tmpY = date("Y", $tmpDate);
        $tmpM = date("m", $tmpDate);
        $labels .= "'" . $tmpY . "/" . str_pad($tmpM, 2, 0, STR_PAD_LEFT) . "'";
        array_push($ymTable, $tmpY . "/" . str_pad($tmpM, 2, 0, STR_PAD_LEFT));
    }
}

// 6ヵ月間インスタンス時間グラフデータの取得
function getInstanceCount6M(&$smarty, $labels, $ymTable, $TARGET_CID, $TARGET_AID, $TARGET_DEPT, $TARGET_PROJ)
{
    $conditions = "";
    if ($TARGET_DEPT) {
        $conditions .= " AND dept = {$TARGET_DEPT}";
    }
    if ($TARGET_PROJ) {
        $conditions .= " AND pcode = '{$TARGET_PROJ}'";
    }
    // 登場するインスタンス時間の取得
    $sql = "
        SELECT
            instancetype,
            SUM(COUNT) aaa
        FROM
            stat_inst
        WHERE
            cid = $TARGET_CID
            AND YM >= '{$ymTable[0]}'
            AND YM <= '{$ymTable[5]}'
            $conditions
        GROUP BY
            instancetype
        ORDER BY
            aaa DESC,
            instancetype
    ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $res = array();
    $instType = array();
    // 結果テーブルの初期化
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $row = pg_fetch_row($r, $i);
        array_push($instType, $row[0]);
        for ($j = 0; $j < 6; $j++) {
            $res[$j][$i] = 0;
        }
    }

    $sql = sprintf("
        SELECT
            ym,
            instancetype,
            COUNT
        FROM
            stat_inst
        WHERE
            cid = {$TARGET_CID}
            AND YM >= '{$ymTable[0]}'
            AND YM <= '{$ymTable[5]}'
            $conditions
        ORDER BY
            ym,
            instancetype
    ");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    // $resに結果を格納
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $row = pg_fetch_row($r, $i);
        $idx_type = array_search($row[1], $instType);
        $idx_ym = array_search($row[0], $ymTable);
        $res[$idx_ym][$idx_type] = $row[2];
    }

    $colorIdx = 0;
    $resStr = "";

    // Total の取得と表示
    $totalStr = "";
    for ($i=0; $i < 6; $i++) {
        $total = 0;
        for ($j=0; $j < count($instType); $j++) {
            $total += $res[$i][$j];
        }
        $totalStr .= $total . ",";
    }
    //  $resStr .= "{\n";
    //  $resStr .= "    label: '合計インスタンス稼働時間',\n";
    //  $resStr .= "    type: 'line',\n";
    //  $resStr .= "    lineTension: 0,\n";
    //  $resStr .= "    fill: false,\n";
    //  $resStr .= "    borderColor: '" . getGraphColor($colorIdx, "") . "',\n";
    //  $resStr .= "    data: [" . $totalStr . "]\n";
    //  $resStr .= "},";

    // 個別の積み上げグラフ
    for ($i = 0; $i < count($instType); $i++) {
        $dataStr = "";
        for ($j=0; $j < 6; $j++) {
            $dataStr .= $res[$j][$i] . ",";
        }
        $resStr .= "{\n";
        $resStr .= "    label: '" . $instType[$i] . "',\n";
        $resStr .= "    type: 'bar',\n";
        $resStr .= "    backgroundColor: '" . getGraphColor($colorIdx, "") . "',\n";
        $resStr .= "    pointStyle: 'rect',\n";
        $resStr .= "    data: [" . $dataStr . "]\n";
        $resStr .= "},";
    }
    raise_sql($logs, 'db');

    return ($resStr);
}

// 6ヵ月間グラフデータの取得
function getGraphData6M(
    &$smarty,
    $labels,
    &$dataArr,
    $ymTable,
    $rateArr,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
) {
    $pcodeArr = array();
    $conditions = "";
    if ($TARGET_DEPT) {
        $conditions .= " AND dept = {$TARGET_DEPT}";
    }
    if ($TARGET_PROJ) {
        $conditions .= " AND pcode = '{$TARGET_PROJ}'";
    }
    $sql = sprintf("
        SELECT
            productcode,
            sum(unblendedcost) aaa,
            ym
        FROM
            cost_detail
        WHERE
            cid = $TARGET_CID
            AND ym >= '{$ymTable[0]}'
            AND ym <= '{$ymTable[5]}'
            AND lineitemtype != 'Tax'
            AND unblendedcost != 0
            $conditions
        GROUP BY
            ym,
            productcode
        ORDER BY
            ym,
            aaa DESC,
            productcode
    ");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    // $result = pg_fetch_all($r);
    $data = array();
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $row = pg_fetch_row($r, $i);
        $row[0] = getServiceShortName($row[0]);
        $pidx = findArrIndex($row[0], $pcodeArr);
        $ymidx = findArrIndex($row[2], $ymTable);
        $rate = $rateArr[$row[2]];
        $data[$pidx][$ymidx] = round($row[1] * $rate, 0);
    }
    for ($i=0; $i< count($pcodeArr); $i++) {
        if (!isset($data[$i])) {
            $data[$i] = "";
        }
        if (!isset($dataArr[$i])) {
            $dataArr[$i] = "";
        }
        for ($j=0; $j<count($ymTable); $j++) {
            if (!isset($data[$i][$j]) || $data[$i][$j] == "") {
                $dataArr[$i] .= "0,";
            } else {
                $dataArr[$i] .= $data[$i][$j] . ",";
            }
        }
    }

    // 棒グラフ表示用文字列の生成
    $res = "";
    $rateMessage = "";
    $colorIdx = 0;
    $res .= "{\n";
    $res .= "    label: '為替レート(円/ドル)',\n";
    $res .= "    type: 'line',\n";
    $res .= "    lineTension: 0,\n";
    $res .= "    fill: false,\n";
    $res .= "    yAxisID: 'y2',\n";
    $res .= "    backgroundColor: '" . getGraphColor($colorIdx, "") . "',\n";
    $res .= "    data: [";
    for ($i = 0; $i < 6; $i++) {
        $res.= $rateArr[$ymTable[$i]] . ",";
    }
    $res .= "]\n";
    $res .= "},";

    for ($i = 0; $i < count($dataArr); $i++) {
        $res .= "{\n";
        $res .= "    label: '" . $pcodeArr[$i] . "',\n";
        $res .= "    type: 'bar',\n";
        $res .= "    yAxisID: 'y1',\n";
        $res .= "    backgroundColor: '" . getGraphColor($colorIdx, $pcodeArr[$i]) . "',\n";
        $res .= "    pointStyle: 'rect',\n";
        $res .= "    data: [" . $dataArr[$i] . "]\n";
        $res .= "},";
    }
    raise_sql($logs, 'db');

    return($res);
}

// レートテーブルの作成
function createRateArr(&$smarty, &$arr, $ymTable, &$rateMessage, $Y, $M, &$rate1M)
{
    $tmpRate = SUPER_DEFAULT_RATE;
    $rate1M = "";
    $target1M = $Y . "/" . str_pad($M, 2, 0, STR_PAD_LEFT);
    for ($i = 0; $i < count($ymTable); $i++) {
        $arr[$ymTable[$i]] = "";
    }

    $sql = sprintf("SELECT ym, dy
                    FROM rate
                    WHERE ym >= '" . $ymTable[0] . "'
                        AND ym <= '" . $ymTable[5] . "'
                    ORDER BY ym");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $row = pg_fetch_row($r, $i);
        $arr[$row[0]] = $row[1];
        $tmpRate = $row[1];
    }

    // 仮レートのチェックと代入
    for ($i = 0; $i < count($ymTable); $i++) {
        if ($arr[$ymTable[$i]] == "") {
            $rateMessage .= $ymTable[$i] .
                "の為替レートは未確定のため、前月または最新の確定レートを使って仮の料金を算出しています。<br />";
            $arr[$ymTable[$i]] = $tmpRate;
        }
        if ($ymTable[$i] == $target1M) {
            $rate1M = $arr[$ymTable[$i]];
        }
    }

    if ($rate1M == "") {
        // 1ヵ月グラフ用のレートを取得
        $sql = sprintf("SELECT dy
                        FROM rate
                        WHERE ym <='" . $Y . "/" . str_pad($M, 2, 0, STR_PAD_LEFT) . "'
                        ORDER BY ym desc");
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);

        if (pg_num_rows($r) == 0) {
            $rate1M = SUPER_DEFAULT_RATE;
            $rateMessage .= $Y . "/" . str_pad($M, 2, 0, STR_PAD_LEFT) .
                "の為替レートは未確定、または見つからなかったため、仮レート"
                . SUPER_DEFAULT_RATE . "円/ドルを使って仮の料金を算出しています。<br />";
        } else {
            $row = pg_fetch_row($r, 0);
            $rate1M = $row[0];
        }
    }
    raise_sql($logs, 'db');
}

// X軸ラベル用文字列の生成
// 年月 × n
function getXlabels($startDate, $monthCount, &$ymTable, &$labels)
{
    $ymTable = array();

    // X軸ラベル YYYY/MM x 6 の生成
    for ($i = 0; $i < $monthCount; $i++) {
        if ($i != 0) {
            $labels .= ",";
        }
        $tmpDate = strtotime("+" . ($i+1) . " month", $startDate);
        $tmpY = date("Y", $tmpDate);
        $tmpM = date("m", $tmpDate);
        $labels .= "'" . $tmpY . "/" . str_pad($tmpM, 2, 0, STR_PAD_LEFT) . "'";
        array_push($ymTable, $tmpY . "/" . str_pad($tmpM, 2, 0, STR_PAD_LEFT));
    }

    return($ymTable);
}

// 配列から指定されたitemを探してインデックスを返す。
// 見つからない場合は最後に追加して、そのインデックスを返す。
function findArrIndex($item, &$Arr)
{
    for ($i =0; $i < count($Arr); $i++) {
        if ($item == $Arr[$i]) {
            return($i);
        }
    }
    array_push($Arr, $item);
    return(count($Arr) - 1);
}
