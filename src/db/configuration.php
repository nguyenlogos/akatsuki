<?php
use Common\Mailer;
use Akatsuki\Models\Emp;

if ($_SESSION['sysadmin'] == 0) {
    errorScreen($smarty, "アクセスが拒否されました。");
    exit;
}

$cid = '';
$cid_reg = '';
$pass_length = '';
$pass_expired = '';
$pass_lowercase = '';
$pass_number = '';
$mail_address = '';
$admin_active = '';
$number_lock_failed = '';
$instance_expires = '';
//$check_sys_admin = '';
$check_manager = '';
$email_instance = '';
$days_apply = '';
$timezone = '';
$timezoneid = '';

$cid = (int)$_SESSION["cid"];
$sql = sprintf("SELECT * FROM configs WHERE cid = $1");
$r = pg_query_params(
    $smarty->_db,
    $sql,
    [
        $cid,
    ]
);
$result = pg_fetch_all($r);

$cid_reg = $result[0]['cid'];
$insttype_curgen = $result[0]['insttype_curgen'];
$pass_length = $result[0]['pass_lenght'];
$pass_expired = $result[0]['pass_expired'];
$pass_lowercase = $result[0]['pass_lowercase'];
$pass_number = $result[0]['pass_number'];
$mail_address = $result[0]['mail_address'];
$admin_active = $result[0]['admin_active'];
$number_lock_failed = $result[0]['lock_failed'];
$instance_expires = $result[0]['instance_expires'];
//$check_sys_admin = $result[0]['check_sys_admin'];
$check_manager = $result[0]['check_manager'];
$email_instance = $result[0]['email_instance'];
$days_apply = $result[0]['days_apply'];
$timezone = $result[0]['timezone'];
$timezoneid = $result[0]['timezoneid'];

if (isset($_POST['save_setting'])) {
    $cid = (int)$cid;
    $cid_reg = postreq('cid_reg');
    $pass_length = postreq('pass_lenght');
    $pass_expired = postreq('pass_expired');
    $pass_lowercase = postreq('pass_lowercase');
    $pass_number = postreq('pass_number');
    $mail_address = pg_escape_string(postreq('mail_address'));
    $admin_active = postreq('admin_active');
    $number_lock_failed = postreq('number_lock_failed');
    $instance_expires = postreq('instance_expires');
    //$check_sys_admin = postreq('check_sys_admin');
    $check_manager = postreq('check_manager');
    $email_instance = postreq('email_instance');
    $days_apply = postreq('days_apply');
    $timezone = pg_escape_string(postreq('timezone'));
    $timezoneid = postreq('timezoneid');

    $sql = 'SELECT cid, empid, email, id, number_lock, email_lock FROM emp';
    $r = pg_query($smarty->_db, $sql);
    $data = pg_fetch_all($r);

    if ($data) {
        foreach ($data as $key => $value) {
            if ($value['number_lock'] >= $number_lock_failed && $number_lock_failed > 0) {
                $emp = Emp::where('id', $value['id'])
                          ->first();
                $randomToken = $emp->generateResetToken();
                $emp->setValues([
                    'email_lock' => 1
                ]);
                $emp->save();
                if ($value['email_lock'] == 0) {
                    //send email
                    $sendMail = $value['email'];
                    $mailer = new Mailer();
                    $mailSubject = "[SunnyView] パスワードの再設定";
                    $mailBody = "パスワードを再設定するには、下記のリンクをクリックして画面に表示される手順を行ってください。\n\n
                                http://52.68.30.203/reg/change_pass.php?token=" . $randomToken;
                    $mailer
                        ->set('subject', $mailSubject)
                        ->set('to', $sendMail)
                        ->set('body', $mailBody);
                    $result = $mailer->send();
                }
            }
        }
    }

    if ($cid_reg > 0) {
        $sql = "UPDATE configs
                SET
                  pass_lenght = $2,
                  pass_expired = $3,
                  pass_lowercase = $4,
                  pass_number = $5,
                  mail_address = $6,
                  admin_active = $7,
                  lock_failed = $8,
                  instance_expires = $9,
                  check_sys_admin = $10,
                  check_manager = $11,
                  email_instance = $12,
                  days_apply = $13,
                  timezone = $14,
                  timezoneid = $15
                WHERE cid = $1 ";
        $result   = pg_query($smarty->_db, $sql);

        $result = pg_query_params($smarty->_db, $sql, [
            (int)$cid,
            (int)$pass_length,
            (int)$pass_expired,
            (int)$pass_lowercase,
            (int)$pass_number,
            $mail_address,
            (int)$admin_active,
            (int)$number_lock_failed,
            (int)$instance_expires,
            1,
            (int)$check_manager,
            (int)$email_instance,
            (int)$days_apply,
            $timezone,
            (int)$timezoneid,
        ]);

        if ($result) {
            $msg = MESSAGES['INF_UPDATE'];
            $err = 0;
        } else {
            $msg = MESSAGES['ERR_UPDATE'];
            $err = 1;
        }
    } else {
        $sql = sprintf("SELECT * FROM configs WHERE cid = $1");
        $r = pg_query_params(
            $smarty->_db,
            $sql,
            [
                $cid,
            ]
        );
        $result = pg_fetch_all($r);

        if ($cid == $result[0]['cid']) {
            $msg = MESSAGES['ERR_UPDATE'];
            $err = 1;
        } else {
            $sql = 'INSERT INTO "configs" ("cid",
                    "insttype_curgen",
                    "pass_lenght",
                    "pass_expired",
                    "pass_lowercase",
                    "pass_number",
                    "mail_address",
                    "admin_active",
                    "lock_failed",
                    "instance_expires",
                    "check_sys_admin",
                    "check_manager",
                    "email_instance",
                    "days_apply",
                    "timezone",
                    "timezoneid"
                    )';
            $sql .= " VALUES (
                '" . (int)$cid . "',
                '" . (int)$insttype_curgen . "',
                '" . (int)$pass_length . "',
                '" . (int)$pass_expired . "',
                '" . (int)$pass_lowercase . "',
                '" . (int)$pass_number . "',
                '" . $mail_address . "' ,
                '" . (int)$admin_active . "',
                '" . (int)$number_lock_failed . "',
                '" . (int)$instance_expires . "',
                '" . 1 . "',
                '" . (int)$check_manager . "',
                '" . (int)$email_instance . "',
                '" . (int)$days_apply . "',
                '" . $timezone . "',
                '" . (int)$timezoneid . "'
            );";

            $result   = pg_query($smarty->_db, $sql);

            if ($result) {
                $msg = MESSAGES['INF_UPDATE'];
                $err = 0;
            } else {
                $msg = MESSAGES['ERR_UPDATE'];
                $err = 1;
            }
        }
    }
}

$smarty->assign('cid_reg', $cid_reg);
$smarty->assign('pass_length', $pass_length);
$smarty->assign('pass_expired', $pass_expired);
$smarty->assign('pass_lowercase', $pass_lowercase);
$smarty->assign('pass_number', $pass_number);
$smarty->assign('mail_address', $mail_address);
$smarty->assign('admin_active', $admin_active);
$smarty->assign('number_lock_failed', $number_lock_failed);
$smarty->assign('instance_expires', $instance_expires);
//$smarty->assign('check_sys_admin', $check_sys_admin);
$smarty->assign('check_manager', $check_manager);
$smarty->assign('email_instance', $email_instance);
$smarty->assign('days_apply', $days_apply);
$smarty->assign('timezone', $timezone);
$smarty->assign('timezoneid', $timezoneid);

$smarty->assign('pageIcon', 'glyphicon glyphicon-wrench');
$smarty->assign('pageTitle', 'システム設定');
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('viewTemplate', 'db/configuration.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
