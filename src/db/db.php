<?php
require_once(ROOT_PATH . "/if/updateEBS.php");
require_once("db_function.php");

$TARGET_CID = null;
$TARGET_DEPT = null;
$TARGET_PROJ = null;

$listAssigned = [];

$TARGET_CID = $_SESSION["cid"];
$TARGET_AID = $_SESSION["accountid"];

// お知らせ件数のアップデート表示
updateCount($smarty);

$updateInstanceTable = function (&$msg) use ($smarty) {
    $config = array(
        'credentials'   => [
            'key'       => $_SESSION["key"],
            'secret'    => $_SESSION["secret"],
        ],
        'region'        => $_SESSION["region"],
        'version'       => 'latest'
    );
    $ret = \Akatsuki\Models\Instance::updateInstances();
    $ret && $ret = \Akatsuki\Models\Sg::updateSecurityGroups();
    $ret && $ret = updateEBS($_SESSION["cid"], $smarty, $config);
};

// check page permission
$deptRoles = permission_check("mst/dept.php");
$pageRoles = permission_check("db/db.php");
$hideDept = false;

if (!$deptRoles['read']['allowed']) {
    $TARGET_DEPT = $_SESSION['dept'];
} else {
    $TARGET_DEPT = postreq('dept');
}
$TARGET_PROJ = postreq('proj');

if ($pageRoles['read']['condition'] == 5) {
    $listAssigned = \Akatsuki\Models\EmpProj::getListAssigned();
    $listAssigned = array_column($listAssigned, 'pcode');
    if ($TARGET_PROJ) {
        if (!in_array($showgproject, $listAssigned)) {
            $listAssigned = [];
        }
    }

    if (count($listAssigned)) {
        $instInfo = getInfoInstanceOverview($smarty, $_SESSION['cid'], null, $listAssigned);
    } else {
        $instInfo = [
            'reqRunningCount'  => 0,
            'reqWarningCount'  => 0,
            'reqOutdatedCount' => 0,
            'activeCount'      => 0,
            'inactiveCount'    => 0,
        ];
    }
} else {
    $instInfo = getInfoInstanceOverview($smarty, $_SESSION['cid'], $TARGET_DEPT);
}

$msg = "";
$updateInstanceTable($msg);

$Y = postreq("Y");
$M = str_pad(postreq("M"), 2, 0, STR_PAD_LEFT);

if ($Y == "" || $M == "" || is_numeric($Y) == false || is_numeric($M) == false) {
    $Y = date("Y");
    $M = date("m");
}
$MonthEnd = date("t", strtotime($Y . "-" . $M . "-01"));

$recUsage = "";
$data = "";

// 6ヵ月のX軸ラベルとYYYY/MMの配列を生成
$labels6M = "";
$ymTable = array();
get6Mlist($labels6M, $ymTable);
// レートテーブルの作成
$rateArr = array();
$rateMessage = "";
createRateArr($smarty, $rateArr, $ymTable, $rateMessage, $Y, $M, $rate1M);

// 1Mグラフデータの取得と棒グラフ表示用datasets文字列の生成
$graphTitle="1ヵ月コスト推移(税別)";

$graphTitle2 = "";

$labels="";
$dataSetsStr = getGraphData(
    $smarty,
    $Y,
    $M,
    $MonthEnd,
    $labels,
    $dataArr,
    $pcodeArr,
    $rate1M,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
);

// 円グラフ 表示用datasets文字列の生成
$dataSetsStr2 = getDataSetsStr2(
    $smarty,
    $Y,
    $M,
    $labels2,
    $colors2,
    $rate1M,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
);

// 6Mグラフ用文字列の生成
$graphTitle6M = "6ヵ月コスト推移(税別)";

$dataSetsStr6M = getGraphData6M(
    $smarty,
    $labels6M,
    $dataArr6M,
    $ymTable,
    $rateArr,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
);

// 6Mインスタンス稼働数グラフ文字列の生成
$graphTitle6MInstances = "6ヵ月インスタンス稼働数(延べ)";
$dataSetsStr6MInstances = getInstanceCount6M(
    $smarty,
    $labels6M,
    $ymTable,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
);

// 6Mディスク容量推移グラフ文字列の生成
$graphTitle6MEBS = "6ヵ月ディスク容量推移";
$dataSetsStr6MEBS = getDiskAmount6M(
    $smarty,
    $labels6M,
    $ymTable,
    $TARGET_CID,
    $TARGET_AID,
    $TARGET_DEPT,
    $TARGET_PROJ
);

// 明細の取得
getCostList(
    $smarty,
    $Y,
    $M,
    $recUsage,
    $usageTotal,
    $taxTotal,
    $rate1M,
    $usageTotalJPY,
    $taxTotalJPY,
    $TARGET_CID,
    $TARGET_AID
);

$YMlist = getYMlist($Y, $M);

// generate data for instance request overview
$conditions = "WHERE i.approved_status = 0";
$sql = sprintf("
    SELECT
        i.id,
        u.name AS uname,
        d.deptname,
        to_char(i.req_date,'YYYY/MM/DD HH24:MI:SS') AS req_date,
        i.approved_status,
        i.inst_type,
        (
            CASE
                WHEN i.approved_status = 1 THEN 'Approved'
                WHEN i.approved_status = 0 THEN 'Pending'
            END
        ) AS approved_status
    FROM
        inst_req i
        LEFT JOIN
            users u
            ON u.cid = i.cid
        LEFT JOIN
            dept d
            ON d.cid = i.cid
            AND d.dept = i.dept
    $conditions
    ORDER BY
        i.req_date DESC
");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$reqList = pg_fetch_all($r);

if (!$reqList) {
    $reqList = [];
}

$reqListHeaders = [
    'uname' => [
        'disp_name' => '申請者',
        'sortable'  => false
    ],
    'deptname' => [
        'disp_name' => '所属グループ',
        'sortable'  => false
    ],
    'req_date' => [
        'disp_name' => '申請日',
        'sortable'  => false
    ],
    'approved_status' => [
        'disp_name' => 'ステータス',
        'sortable'  => false
    ],
    'inst_type' => [
        'disp_name' => 'インスタンスタイプ',
        'sortable'  => false
    ],
    'using_purpose' => [
        'disp_name' => 'コメント',
        'sortable'  => false
    ],
];
raise_sql($logs, 'db');
$smarty->assign('CID', $_SESSION["cid"]);

$smarty->assign('SCID', $TARGET_CID);
$smarty->assign('DEPT', $TARGET_DEPT);
$smarty->assign('PROJ', $TARGET_PROJ);
$smarty->assign('hideDept', $hideDept ? 'hide' : '');

$smarty->assign('rate1M', $rate1M);
$smarty->assign('rateMessage', $rateMessage);
$smarty->assign('YMlist', $YMlist);
$smarty->assign('labels', $labels);
$smarty->assign('labels2', $labels2);
$smarty->assign('labels6M', $labels6M);
$smarty->assign('colors2', $colors2);
$smarty->assign('dataSetsStr', $dataSetsStr);
$smarty->assign('dataSetsStr2', $dataSetsStr2);
$smarty->assign('dataSetsStr6M', $dataSetsStr6M);
$smarty->assign('dataSetsStr6MInstances', $dataSetsStr6MInstances);
$smarty->assign('dataSetsStr6MEBS', $dataSetsStr6MEBS);
$smarty->assign('pcodeArr', $pcodeArr);
$smarty->assign('graphTitle', $graphTitle);
$smarty->assign('graphTitle2', $graphTitle2);
$smarty->assign('graphTitle6M', $graphTitle6M);
$smarty->assign('graphTitle6MInstances', $graphTitle6MInstances);
$smarty->assign('graphTitle6MEBS', $graphTitle6MEBS);
$smarty->assign('recs', $recUsage);
$smarty->assign('usageTotal', $usageTotal);
$smarty->assign('taxTotal', $taxTotal);
$smarty->assign('usageTotalJPY', $usageTotalJPY);
$smarty->assign('taxTotalJPY', $taxTotalJPY);

$smarty->assign('pageIcon', 'fa-dashboard');
$smarty->assign('pageTitle', 'ダッシュボード');
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('instInfo', $instInfo);
$smarty->assign('reqList', $reqList);
$smarty->assign('reqListHeaders', $reqListHeaders);

$smarty->assign('viewTemplate', 'db/db.tpl');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
