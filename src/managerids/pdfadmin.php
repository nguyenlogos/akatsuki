<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}
$acid = getreq("id");
$YM = getreq("ym");
$down_all = getreq("slug");

$sql = sprintf("SELECT filename FROM invoice2 WHERE costacctid = '" . $acid . "' AND ym = '" . $YM . "'");
$logs[] = $sql;
$r   = pg_query($smarty->_db, $sql);

if (pg_num_rows($r) != 1) {
    $msg = "該当する請求書ファイルが見つかりませんでした。(S1)";
}
if ($down_all) {
    $filename = "".$down_all."_download_all.pdf";
} else {
    $filename = pg_fetch_result($r, 0, 0);
}

header("Content-Type: application/force-download");
header("Content-Length: " . filesize(INVOICE_PATH . $filename));
if ($down_all) {
    header("Content-Disposition: attachment; filename='".$down_all."_download_all.pdf'");
} else {
    header("Content-Disposition: attachment; filename='" . $YM . ".pdf'");
}
readfile(INVOICE_PATH . $filename);

raise_sql($logs, 'pdfadmin_manager');

$smarty->disconnect();
