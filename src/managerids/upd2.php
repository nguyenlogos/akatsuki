<?php
require_once(ROOT_PATH . '/if/updateInst.php');
require_once(ROOT_PATH . '/if/updateAccountID.php');

if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}

$msg = "";
$err = 0;
$key = "";
$newkey = "";
$secret = "";
$newsecret = "";
$encrypted = "";
$mode = "";

$button = postreq("button");
if ($button == "認証テスト") {
    $mode = "TEST";
} elseif ($button == "保存") {
    $mode = "SAVE";
}

if ($mode != "") {
    $cid = postreq("id");
    $company = htmlspecialchars(postreq("company"));
    $key = htmlspecialchars(postreq("key"));
    $secret = htmlspecialchars(postreq("secret"));
    $newkey = htmlspecialchars(postreq("newkey"));
    $newsecret = htmlspecialchars(postreq("newsecret"));
    if ($cid == "") {
        errorScreen($smarty, "システムエラーが発生しました。(NOID)");
        exit;
    }

    $config = array(
        'credentials' => [
            'key'    => $newkey,
            'secret' => $newsecret
        ],
        'version' => 'latest',
        'region' => "ap-northeast-1"
    );

    $_SESSION['ERR_AWS_KEY'] = "";
    $ret = updateInst($smarty, $config, $cid, $mode);
    $ret2 = 0;
    if ($ret == 0 && $mode !== "TEST") {
        $ret2 = updateAccountID($smarty, $config, $cid, $mode);
    }

    if ($ret < 0 || $ret2 < 0) {
        $msg = "認証に失敗しました。<br />再度確認して入力してください。(" . $ret . "," . $ret2 . ")";
        $err = 1;
    } else {
        if ($mode == "TEST") {
            $msg = "認証テストに成功しました。<br />保存ボタンを押してください。<br /><br />注意：まだ設定は保存されていません。";
            $err = 1;
        } else {
            // 成功 DBへ登録
            $encrypted = encryptIt($newkey);
            $sql = "update users set key = '" . pg_escape_string($encrypted) . "', 
                        secret = '" . pg_escape_string($newsecret) . "' 
                    where cid = " . pg_escape_string($cid);
            $logs[] = $sql;
            pg_query($smarty->_db, $sql);

            $err = 0;
            $msg = "認証テストに成功しましたので、設定を保存しました。";
        }
    }
} else {
    $cid = getreq("id");
    if ($cid == "") {
        errorScreen($smarty, "システムエラーが発生しました。(NOID)");
        exit;
    }

    // 初期アクセス
    $sql = "SELECT companyname, status,secret,key FROM users WHERE cid = " . pg_escape_string($cid);
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r) != 1) {
        errorScreen($smarty, "システムエラーが発生しました。(NOREC)");
        exit;
    }
    $company = pg_fetch_result($r, 0, 0);
    $status = pg_fetch_result($r, 0, 1);
    $secret = pg_fetch_result($r, 0, 2);
    $key = pg_fetch_result($r, 0, 3);

    if ($status == -1) {
        $msg = "警告：すでに削除された企業です。";
    }

    if ($secret != "") {
        $smarty->assign('key', decryptIt($key));
        $smarty->assign('secret', $secret);
    } else {
        $smarty->assign('key', "未設定");
        $smarty->assign('secret', "未設定");
    }
}
raise_sql($logs, 'upd2_manager');

$smarty->assign('key', decryptIt($key));
$smarty->assign('secret', $secret);
$smarty->assign('newkey', $newkey);
$smarty->assign('newsecret', $newsecret);
$smarty->assign('pageTitle', 'AWS API認証情報の設定');
$smarty->assign('err', $err);
$smarty->assign('cid', $cid);
$smarty->assign('company', $company);
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('viewTemplate', 'managerids/upd2.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'managerids.tpl');
