<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}

// 初期化
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$updateMessage = "";
$name = $company = $dept = $position = $tel = $email = $email2 = "";
$position_err = $dept_err = $name_err = $tel_err = $company_err = $email_err = $email_config_err = "";
$payday_err = "";
$cid = "";
$button = postreq("btn");
// ボタンが押されたときの処理

if ($button == "新規登録" || $button == "保存") {
    if ($button == "新規登録") {
        $cmd = "new";
    } else {
        $cmd = "update";
    }

    $cid = postreq("cid");
    $name = postreq("name");
    $company = postreq("company");
    $dept = postreq("dept");
    $position = postreq("position");
    $tel = postreq("tel");
    $email = postreq("email");
    $email2 = postreq("email2");
    $payday = postreq("payday");

    // Check validate form sign up
    if (empty($name)) {
        $msg .= "氏名は省略できません。<br />";
        $err = 1;
    } elseif (mb_strlen($name) > CHECK_64_STRLENGTH) {
        $msg .= "64文字まで が入力されていません。<br />";
        $err = 1;
    }

    if (empty($company)) {
        $msg .= "会社名は省略できません。<br />";
        $err = 1;
    } elseif (mb_strlen($company) > CHECK_128_STRLENGTH) {
        $msg .= "128文字まで が入力されていません。<br />";
        $err = 1;
    }

    if (mb_strlen($dept) > CHECK_64_STRLENGTH) {
        $msg .= "64文字まで が入力されていません。<br />";
        $err = 1;
    }
    
    if (mb_strlen($position) > CHECK_64_STRLENGTH) {
        $msg .= "64文字まで が入力されていません。<br />";
        $err = 1;
    }

    if (empty($tel)) {
        $msg .= "電話番号は省略できません。<br />";
        $err = 1;
    } elseif (mb_strlen($tel) > CHECK_64_STRLENGTH) {
        $msg .= "64文字まで が入力されていません。<br />";
        $err = 1;
    }

    if (empty($email)) {
        $msg .= "メールアドレスは省略できません。<br />";
        $err = 1;
    } elseif (mb_strlen($email) > CHECK_64_STRLENGTH) {
        $msg .= "64文字まで が入力されていません。<br />";
        $err = 1;
    }

    if ($email != $email2) {
        $msg .= "再入力されたメールアドレスが異なります。<br />";
        $err = 1;
    } elseif (mb_strlen($email2) > CHECK_64_STRLENGTH) {
        $msg .= "64文字まで が入力されていません。<br />";
        $err = 1;
    }

    if (empty($payday)) {
        $msg .= "支払い条件は省略できません。<br />";
        $err = 1;
    }

    if ($err != 1) {
        //// トランザクションの開始
        //$r = pg_query($smarty->_db, "BEGIN");
        //
        //// CIDの決定。削除ずみのものを含めてmaxを取得
        if ($cmd == "new") {
            $sql = sprintf("SELECT max(cid) FROM users");
            $logs[] = $sql;
            $r = pg_query($smarty->_db, $sql);
            $row = pg_fetch_row($r, 0);
            // レコードが1件もなければ IDに1を設定。
            if ($row[0] == "") {
                $cid = 1;
            } else {
                $cid = $row[0] + 1;
            }
        }

        // PostgreSQL用に文字列をエスケープ
        $name = pg_escape_string($name);
        $company = pg_escape_string($company);
        $dept = pg_escape_string($dept);
        $position = pg_escape_string($position);
        $tel = pg_escape_string($tel);
        $email = pg_escape_string($email);
        $payday = pg_escape_string($payday);

        // メールアドレスの重複チェック
        $sql = sprintf("SELECT email FROM users WHERE email = $1 and cid != $2 ");
        $logs[] = $sql;
        $r = pg_query_params($smarty->_db, $sql, [
            $email,
            $cid
        ]);
        //レコードが1件でもあればエラー
        if (pg_num_rows($r) > 0) {
            $msg = "すでに同じメールアドレスが登録されています。";
            $err = 1;
        } else {
            // エラーチェック終了。登録・保存処理
            if ($cmd == "new") {
                $sql = "INSERT INTO users (cid, name, companyname, dept, position, tel, uid, regdate, status, payday) 
                        VALUES(" . $cid . ",'" . $name . "', '" . $company . "',";
                if ($dept == "") {
                    $sql .= "null,";
                } else {
                    $sql .= "'" . $dept . "',";
                }
                if ($position == "") {
                    $sql .= "null,";
                } else {
                    $sql .= "'" . $position . "',";
                }
                $sql .= "'" . $tel . "','" . $email . "',now(),0," . $payday . ")";
                $r = pg_query($smarty->_db, $sql);
                if (!$r) {
                    $msg = MESSAGES['ERR_DELETE'];
                    $err = 1;
                } else {
                    $msg = "新しい企業を登録しました。<br /><br /><a href='/managerids/etp.php'>戻る</a>";
                    $err = 0;
                }
                insertLog($smarty, "新しい企業情報を登録しました。CID:" . $cid . " " . $company . " " . $name);
            } else {
                $sql = "UPDATE users SET name = '" . $name . "',companyname = '" . $company . "',";
                if ($dept == "") {
                    $sql .= "dept = null,";
                } else {
                    $sql .= "dept = '" . $dept . "',";
                }
                if ($position == "") {
                    $sql .= "position = null,";
                } else {
                    $sql .= "position = '" . $position . "',";
                }
                $sql .= "tel = '" . $tel . "',uid = '" . $email . "',payday = " . $payday . "  WHERE cid = " . $cid;
                $r = pg_query($smarty->_db, $sql);
                if (!$r) {
                    $msg = MESSAGES['ERR_DELETE'];
                    $err = 1;
                } else {
                    $msg = "企業情報を変更しました。<br /><br /><a href='/managerids/etp.php'>戻る</a>";
                    $err = 0;
                }
                insertLog($smarty, "企業情報を変更しました。CID:" . $cid . " " . $company . " " . $dept . " 
                            " . $name . " " . $position . " " . $tel . " " . $email);
            }
        } // if
    } //else
} else {
    // 初期GETアクセスの場合

    // 処理内容の確認
    $cmd=getreq("cmd");
    if ($cmd == "update") {
        $cid = getreq("id");
        if ($cid == "") {
            errorScreenIds($smarty, "GETデータが不正です。(NOCID)");
            exit;
        }
        $sql = sprintf("SELECT name,tel, dept, companyname, position, uid, status, payday 
                        FROM users 
                        WHERE cid = " . $cid);
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);

        if (pg_num_rows($r) != 1) {
            errorScreenIds($smarty, "GETデータが不正です。(CIDNOTF)");
            exit;
        }
        $row      = pg_fetch_row($r, 0);
        $name     = $row[0];
        $company  = $row[3];
        $dept     = $row[2];
        $position = $row[4];
        $tel      = $row[1];
        $email2   = $email = $row[5];
        $status   = $row[6];
        $payday   = $row[7];
    }
}

raise_sql($logs, 'upd_manager');
$smarty->assign('cid', htmlspecialchars($cid));
$smarty->assign('name', htmlspecialchars($name));
$smarty->assign('company', htmlspecialchars($company));
$smarty->assign('dept', htmlspecialchars($dept));
$smarty->assign('position', htmlspecialchars($position));
$smarty->assign('tel', htmlspecialchars($tel));
$smarty->assign('email', htmlspecialchars($email));
$smarty->assign('email2', htmlspecialchars($email2));
$smarty->assign('payday', htmlspecialchars($payday));

$smarty->assign('paydayOptions', array(
    30 => '翌月末',
    40 => '翌々月10日',
    60 => '翌々月末'
    ));

$smarty->assign('name_err', $name_err);
$smarty->assign('company_err', $company_err);
$smarty->assign('tel_err', $tel_err);
$smarty->assign('email_err', $email_err);
$smarty->assign('email_config_err', $email_config_err);
$smarty->assign('payday_err', $payday_err);

if ($cmd == "new") {
    $smarty->assign('pageTitle', '契約企業の新規登録');
    $smarty->assign('submitButton', '新規登録');
} else {
    $smarty->assign('pageTitle', '契約企業情報の変更');
    $smarty->assign('submitButton', '保存');
}

$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'managerids/upd.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'managerids.tpl');

//function updateMessageEMP(&$smarty, $uid, $newDeptName, $newEmail, $newDeptID, $newName, $newSysadmin, $newAdmin)
//{
//    $msg = "";
//    $cid_auth = $_SESSION['login_font']["cid_auth"];
//    $sql = sprintf("SELECT a.email,a.dept,b.deptname,a.name,a.sysadmin,a.admin
//            FROM emp a, dept b
//            WHERE a.dept = b.dept
//                AND a.status = 0
//                AND b.status = 0
//                AND a.cid = " . $cid_auth . "
//                AND b.cid = " . $cid_auth. "
//                AND empid = " . $uid);
//    $logs[] = $sql;
//    $r = pg_query($smarty->_db, $sql);
//
//    if (pg_num_rows($r) != 1) {
//        $msg = "ユーザ情報の変更ログを出力する際にレコードが見つかりませんでした。";
//        $oldName = "不明";
//        $oldEmail = "不明";
//        $oldDeptName = "不明";
//        $oldDeptID = "不明";
//        $oldSysadmin = "不明";
//        $oldAdmin = "不明";
//    } else {
//        $msg = "ユーザ情報を変更しました。";
//        $row = pg_fetch_row($r, 0);
//        $oldName = $row[3];
//        $oldEmail = $row[0];
//        $oldDeptName = $row[2];
//        $oldDeptID = $row[1];
//        $oldSysadmin = $row[4];
//        $oldAdmin = $row[5];
//    }
//
//    if ($oldName != $newName) {
//        $msg.= "氏名「" . $oldName . "」→「" . $newName . "」 ";
//    }
//    if ($oldEmail != $newEmail) {
//        $msg.= "メールアドレス「" . $oldEmail . "」→「" . $newEmail . "」 ";
//    }
//    if ($oldDeptID != $newDeptID) {
//        $msg.= "所属グループID「" . $oldDeptID . "」→「" . $newDeptID . "」 ";
//    }
//    if ($oldDeptName != $newDeptName) {
//        $msg.= "所属グループ名「" . $oldDeptName . "」→「" . $newDeptName . "」 ";
//    }
//    if ($oldSysadmin != $newSysadmin) {
//        if ($oldSysadmin==1) {
//            $oldSysadmin="on";
//        } else {
//            $oldSysadmin="off";
//        }
//        if ($newSysadmin==1) {
//            $newSysadmin="on";
//        } else {
//            $newSysadmin="off";
//        }
//        $msg.= "システム管理者「" . $oldSysadmin . "」→「" . $newSysadmin . "」 ";
//    }
//    if ($oldAdmin != $newAdmin) {
//        if ($oldAdmin==1) {
//            $oldAdmin="on";
//        } else {
//            $oldAdmin="off";
//        }
//        if ($newAdmin==1) {
//            $newAdmin="on";
//        } else {
//            $newAdmin="off";
//        }
//        $msg.= "マネージャー「" . $oldAdmin . "」→「" . $newAdmin . "」 ";
//    }
//
//    if ($msg == "ユーザ情報を変更しました。") {
//        // 変更なし
//        return ("");
//    }
//    raise_sql($logs, 'upd');
//
//    return($msg);
//}
