<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}

$msg = $err = $num_rate = $usd_rate = $jpy_rate = $date_rate = $ym = $get_data = "";
$err        = 0;
$num_rate   = htmlspecialchars(postreq("num_rate"));
$date_rate  = postreq("date_rate");
$ym = getreq('ym');

// ソートキーボタンの設定
$sortMap = [
    "ym DESC", "ym",
    "dy", "dy DESC",
];

// 併せてSQLのソートキーを設定
$sortIndex  = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

$buton = postreq('button');
if ($buton == "削除") {
    $id   = postreq('title');
    $sql  = "DELETE FROM rate WHERE ym = '".$id."'";
    $logs[] = $sql;
    $date = pg_query($smarty->_db, $sql);

    if (!$date) {
        $msg = MESSAGES['ERR_DELETE'];
        $err = 1;
    } else {
        $msg = MESSAGES['INF_DELETE_EXCHANGERATE'];
        $err = 0;
    }
}

if (isset($_POST['currency'])) {
    if (!ctype_int($num_rate) >= 1) {
        $msg = MESSAGES['FORMAT_PRICE_EXCHANGERATE'];
        $err = 1;
    } else {
        // insert exhange rate
        $sql    = 'INSERT INTO rate
                ("ym", "dy")';
        $sql    .= "VALUES
              ('".pg_escape_string($date_rate)."', '".pg_escape_string($num_rate)."')";
        $logs[] = $sql;
        $insert = pg_query($smarty->_db, $sql);
        if (!$insert) {
            $msg = MESSAGES['ERR_DELETE'];
            $err = 1;
        } else {
            $msg = MESSAGES['INF_DELETE_EXCHANGERATE'];
            $err = 0;
        }
    }
} else {
    $ym_edit = $buttom_update = "";
    $buttom_update = postreq('update');
    $ym_edit = postreq('ym_edit');
    if ($buttom_update) {
        if (!ctype_int($num_rate) >= 1) {
            $msg = MESSAGES['FORMAT_PRICE_EXCHANGERATE'];
            $err = 1;
        } else {
            //update exhange rate
            $sql    = "UPDATE rate SET
                    ym = '".pg_escape_string($date_rate)."',
                    dy = '".pg_escape_string($num_rate)."'
                WHERE ym = '".$ym_edit."'";
            $logs[] = $sql;
            $update = pg_query($smarty->_db, $sql);
            if (!$update) {
                $msg = MESSAGES['ERR_DELETE'];
                $err = 1;
            } else {
                $msg = MESSAGES['INF_DELETE_EXCHANGERATE'];
                $err = 0;
            }
        }
    }
}

if ($ym) {
    $get_data = $ym;
}

$sql = "SELECT * FROM rate WHERE ym = '$get_data'";
$r = pg_query($smarty->_db, $sql);
$data_edit = pg_fetch_all($r);

// 現在のページ番号
$p = (int)getreq("p");
if ($p <= 0) {
    $p = 1;
}

// ページ総数の取得
$count = "
    SELECT
        COUNT(ym)
    FROM
        rate";

$r_count = pg_query($smarty->_db, $count);
$dataCount = (int)pg_fetch_result($r_count, 0, 0);
if ($dataCount > 0) {
    $paginationStr = getPagenationStrOld(
        $smarty,
        $dataCount,
        DEFAULT_COUNTPAGE_PRJ,
        $p,
        "./exchangerate.php?sk={$sortIndex}&p="
    );
}

// データテーブルの生成
$pOffset = ($p - 1) * ITEMS_PER_PAGE;
$result = "SELECT *
           FROM rate
           ORDER BY  {$sortColumn} LIMIT ".ITEMS_PER_PAGE." OFFSET ".$pOffset." ";
$r = pg_query($smarty->_db, $result);
$data = pg_fetch_all($r);

raise_sql($logs, 'exchangerate');
$smarty->assign('num_rate', $num_rate);
$smarty->assign('date_rate', $date_rate);
$smarty->assign('data', $data);
$smarty->assign('data_edit', $data_edit);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('paginationStr', $paginationStr);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);

$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('viewTemplate', 'managerids/rate/exchange_rate.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
if ($data_edit) {
    $smarty->assign('pageTitle', 'レート情報変更');
} else {
    $smarty->assign('pageTitle', '為替レート');
}
$smarty->display(TEMPLATES_PATH . '/managerids.tpl');
