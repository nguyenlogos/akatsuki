<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}
// 初期化
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$updateMessage = "";
$name = $company = $dept = $position = $tel = $email = $email2 = "";
$position_err = $dept_err = $name_err = $tel_err = $company_err = "";
$email_err = $email_config_err = $internaluse = $howtosend = "";
$payday_err = "";
$acid = "";
$button = postreq("btn");

// ボタンが押されたときの処理
if ($button == "新規登録" || $button == "保存") {
    if ($button == "新規登録") {
        $cmd = "new";
    } else {
        $cmd = "update";
    }

    $acid = postreq("acid");
    $old_acid = postreq("old_acid");
    $name = postreq("name");
    $company = postreq("company");
    $dept = postreq("dept");
    $position = postreq("position");
    $tel = postreq("tel");
    $email = postreq("email");
    $email2 = postreq("email2");
    $payday = postreq("payday");
    $internaluse = postreq("internaluse");
    $howtosend = postreq("howtosend");

    if ($cmd=="update" && empty($old_acid)) {
        $msg .= "システムエラー：old_acidが取得できませんでした。<br />";
        $err = 1;
    }

    if ($cmd=="update" && $old_acid != $acid) {
        // ACIDが変更された場合、請求書が存在しているか確認
        $sql = "
            SELECT costacctid
                FROM invoice2
                WHERE costacctid = '" . $old_acid . "'
        ";
        $r = pg_query($smarty->_db, $sql);
        if (pg_num_rows($r) != 0) {
            $msg .= "請求書が存在しているため、AWS IDを変更できません。<br />";
            $err = 1;
        }
    }

    if (empty($acid)) {
        $msg .= "AWS IDは省略できません。<br />";
        $err = 1;
    }

    if (mb_strlen($name) > CHECK_64_STRLENGTH) {
        $msg .= "64文字までしか入力できません。<br />";
        $err = 1;
    }

    if (empty($company)) {
        $msg .= "会社名は省略できません。<br />";
        $err = 1;
    } elseif (mb_strlen($company) > CHECK_128_STRLENGTH) {
        $msg .= "64文字までしか入力できません。<br />";
        $err = 1;
    }

    if (mb_strlen($dept) > CHECK_64_STRLENGTH) {
        $msg .= "64文字までしか入力できません。<br />";
        $err = 1;
    }
    
    if (mb_strlen($position) > CHECK_64_STRLENGTH) {
        $msg .= "64文字までしか入力できません。<br />";
        $err = 1;
    }

    if (mb_strlen($tel) > CHECK_64_STRLENGTH) {
        $msg .= "64文字までしか入力できません。<br />";
        $err = 1;
    }

    if (mb_strlen($email) > CHECK_64_STRLENGTH) {
        $msg .= "64文字までしか入力できません。<br />";
        $err = 1;
    }

    if ($email != $email2) {
        $msg .= "再入力されたメールアドレスが異なります。<br />";
        $err = 1;
    } elseif (mb_strlen($email2) > CHECK_64_STRLENGTH) {
        $msg .= "64文字までしか入力できません。<br />";
        $err = 1;
    }

    if (empty($payday)) {
        $msg .= "支払い条件は省略できません。<br />";
        $err = 1;
    }

    if (empty($internaluse)) {
        $msg .= "社内/社外利用は省略できません。<br />";
        $err = 1;
    }

    if (empty($howtosend)) {
        $msg .= "請求書発送方法は省略できません。<br />";
        $err = 1;
    }


    if ($err != 1) {
        // PostgreSQL用に文字列をエスケープ
        $acid = pg_escape_string($acid);
        $name = pg_escape_string($name);
        $company = pg_escape_string($company);
        $dept = pg_escape_string($dept);
        $position = pg_escape_string($position);
        $tel = pg_escape_string($tel);
        $email = pg_escape_string($email);
        $payday = pg_escape_string($payday);
        $howtosend = pg_escape_string($howtosend);

        if ($internaluse == 1) {
            $internaluse = "true";
        } else {
            $internaluse = "false";
        }

        // エラーチェック終了。登録・保存処理
        if ($cmd == "new") {
            $sql = "
                INSERT 
                    INTO invoice_addr 
                    (
                        costacctid, 
                        name, 
                        companyname, 
                        dept, 
                        position, 
                        tel, 
                        email,
                        payday, 
                        internaluse, 
                        howtosend, 
                        updated
                    ) 
                    VALUES
                    (
                         '" . $acid . "',
           ";

            if ($name == "") {
                $sql .= "null,";
            } else {
                $sql .= "'" . $name . "',";
            }

            $sql .= "
                         '" . $company . "',
           ";

            if ($dept == "") {
                $sql .= "null,";
            } else {
                $sql .= "'" . $dept . "',";
            }

            if ($position == "") {
                $sql .= "null,";
            } else {
                $sql .= "'" . $position . "',";
            }

            if ($tel == "") {
                $sql .= "null,";
            } else {
                $sql .= "'" . $tel . "',";
            }

            if ($email == "") {
                $sql .= "null,";
            } else {
                $sql .= "'" . $email . "',";
            }

            $sql .= "
               '" . $payday . "',
                " . $internaluse . ",
           ";

            $sql .= "
               '" . $howtosend . "',
               now(),
           )
           ";

            $r = pg_query($smarty->_db, $sql);

            $msg = "新しい企業を登録しました。<br /><br />
               <a href='/managerids/etp.php'>戻る</a>";
            $err = 0;
            insertLog(
                $smarty,
                "新しい企業情報を登録しました。ACCTID:" . $acid
                . " " . $company . " " . $name
            );
        } else {
            $sql = "
                UPDATE 
                    invoice_addr 
                    SET costacctid='" . $acid . "',
                        name = '" . $name . "',
                        companyname = '" . $company . "',
            ";
            if ($dept == "") {
                $sql .= "dept = null,";
            } else {
                $sql .= "dept = '" . $dept . "',";
            }

            if ($position == "") {
                $sql .= "position = null,";
            } else {
                $sql .= "position = '" . $position . "',";
            }

            $sql .= "
                     tel = '" . $tel . "',
                     email = '" . $email . "',
                     payday = " . $payday . ",
                     internaluse = " . $internaluse . ",
                     updated = now()
                WHERE costacctid = '" . $old_acid . "'
            ";
            $r = pg_query($smarty->_db, $sql);

            $msg = "企業情報を変更しました。<br /><br /><a href='/managerids/invoice.php'>戻る</a>";
            $err = 0;
            insertLog($smarty, "企業情報を変更しました。ACID:" . $acid . " "
                . $company . " " . $dept . " " . $name . " " . $position
                . " " . $tel . " " . $email);
        }
    }
} else {
    // 初期GETアクセスの場合

    // 処理内容の確認
    $cmd=getreq("cmd");
    if ($cmd == "update") {
        $acid = getreq("id");
        if ($acid == "") {
            errorScreenIds($smarty, "GETデータが不正です。(NOACCTID)");
            exit;
        }
        $sql = "
            SELECT
                name,
                tel, 
                dept, 
                companyname, 
                position, 
                payday, 
                internaluse,
                email,
                howtosend
            FROM invoice_addr 
            WHERE 
               costacctid = '" . $acid . "'
        ";
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);

        if (pg_num_rows($r) != 1) {
            errorScreenIds($smarty, "GETデータが不正です。(CIDNOTF)");
            exit;
        }
        $row      = pg_fetch_row($r, 0);
        $name     = $row[0];
        $tel      = $row[1];
        $dept     = $row[2];
        $company  = $row[3];
        $position = $row[4];
        $payday   = $row[5];
        $internaluse = $row[6];
        $email2   = $email = $row[7];
        $howtosend = $row[8];

        if ($internaluse == 't') {
            $internaluse = 1;
        } else {
            $internaluse = 2;
        }
    }
}

raise_sql($logs, 'upd_manager');
$smarty->assign('acid', htmlspecialchars($acid));
$smarty->assign('name', htmlspecialchars($name));
$smarty->assign('company', htmlspecialchars($company));
$smarty->assign('dept', htmlspecialchars($dept));
$smarty->assign('position', htmlspecialchars($position));
$smarty->assign('tel', htmlspecialchars($tel));
$smarty->assign('email', htmlspecialchars($email));
$smarty->assign('email2', htmlspecialchars($email2));
$smarty->assign('payday', htmlspecialchars($payday));
$smarty->assign('internaluse', htmlspecialchars($internaluse));
$smarty->assign('howtosend', htmlspecialchars($howtosend));

$smarty->assign('paydayOptions', array(
    30 => '翌月末',
    40 => '翌々月10日',
    60 => '翌々月末'
    ));

$smarty->assign('howtosendOptions', array(
    'PAPER' => '郵送',
    'HAND' => '手渡し',
    'MAIL' => 'メール送信'
    ));

$smarty->assign('internaluseOptions', array(
    1 => '社内',
    2 => '社外'
    ));

$smarty->assign('name_err', $name_err);
$smarty->assign('company_err', $company_err);
$smarty->assign('tel_err', $tel_err);
$smarty->assign('email_err', $email_err);
$smarty->assign('email_config_err', $email_config_err);
$smarty->assign('payday_err', $payday_err);
$smarty->assign('internaluse_err', $internaluse_err);
$smarty->assign('howtosend_err', $howtosend_err);

if ($cmd == "new") {
    $smarty->assign('pageTitle', '請求先情報の新規登録');
    $smarty->assign('submitButton', '新規登録');
} else {
    $smarty->assign('pageTitle', '請求先情報の変更');
    $smarty->assign('submitButton', '保存');
}

$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'managerids/upd_invoice.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'managerids.tpl');
