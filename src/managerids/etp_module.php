<?php

function getChargedDays(
    $smarty,
    $cid,
    $curY,
    $curM,
    $nextY,
    $nextM
) {

    $sql = "
        SELECT
            plan,
            startdate,
            enddate,
            startuid
        FROM
            plan
        WHERE
            cid=" . $cid . "
            AND startdate < '" . $nextY . "-" . $nextM . "-01 00:00:00'
            AND enddate > '" . $curY . "-" . $curM . "-01 00:00:00'
        ORDER BY
            startdate
    ";

    $r = pg_query($smarty->_db, $sql);

    // 有料プランの契約がある場合は、当該月の有料プラン日数を計算する
    // なお、当該月に複数回、契約・解約を繰り返しすパターンがあるので注意

    $chargeDays = 0;

    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $row = pg_fetch_row($r, $i);

        if ($row[1] < ($curY . "-" . $curM . "-01")) {
            $row[1] = $curY . "-" . $curM . "-01";
        }
        if ($row[2] >= ($nextY . "-" . $nextM . "-01")) {
            $row[2] = date('Y-m-d', strtotime($nextY . "-" . $nextM
              . "-01 00:00:00 -1 days"));
        }
        if ($row[1] >= ($curY . "-" . $curM . "-01")
            && $row[2] < ($nextY . "-" . $nextM . "-01") ) {
            $chargeDays = $chargeDays + ( strtotime($row[2]) - strtotime($row[1]) ) / (60 * 60 * 24) + 1;
        }
    }
    return ($chargeDays);
}

function getFeeTableStr(
    $u_usd,
    $u_jpy,
    $c_usd,
    $c_jpy,
    $t_usd,
    $t_jpy,
    $i_usd,
    $i_jpy,
    $to_usd,
    $to_jpy,
    $flag
) {
    $userList = "<table class='table'>";
    $userList .= "<tr><td>利用料</td><td align='right'>$" . number_format(
        $u_usd,
        2
    )."</td><td align='right'>" . number_format($u_jpy)."円</td></tr>\n";

    $userList .= "<tr><td>クレジット</td><td align='right'>$" . number_format(
        $c_usd,
        2
    )."</td><td align='right'>" . number_format($c_jpy)."円</td></tr>\n";

    $userList .= "<tr><td>消費税</td><td align='right'>$" . number_format(
        $t_usd,
        2
    )."</td><td align='right'>" . number_format($t_jpy)."円</td></tr>\n";

//    $userList .= "<tr><td>IDS (5%)</td><td align='right'>$" . number_format(
//        $i_usd,
//        2
//    ) . "</td><td align='right'>" . number_format($i_jpy)."円</td></tr>\n";

    $userList .= "<tr><td>";
    if ($flag) {
        $userList .= "小　計";
    } else {
        $userList .= "<b>合　計</b>";
    }
    $userList .= "</td><td align='right'>$" . number_format(
        $to_usd,
        2
    )."</td><td align='right'>" . number_format($to_jpy) . "円</b></td></tr></table>\n";
    return($userList);
}
