<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}

// 初期化
$err = 0;
$errDateTo = $errDateFrom = $title_err = $notif_title = "";
$notif_name = $notif_date_from = $notif_date_to = $notif_icon = $uid = "";
$msg = ""; // メッセージ
$button = postreq("button");
$cmd = "";
$cmd = getreq("cmd");
$cid_auth = $_SESSION['login_font']["cid_auth"];
$empid_auth = $_SESSION['login_font']["empid_auth"];

if ($cmd == "new") {
    $smarty->assign('submitButton', '新規登録');
} else {
    $smarty->assign('submitButton', '保存');
}

$uid = (int)postreq("uid");
$notif_title = htmlspecialchars(postreq('notif_title'));
$notif_name = htmlspecialchars(postreq('notif_name'));
$notif_date_from = postreq('date_rate_from');
$notif_date_to = postreq('date_rate_to');
$notif_icon = postreq('notif_icon');

if ($button == "新規登録" || $button == "保存") {
    if ($button == "新規登録") {
        $cmd = "new";
    } else {
        $cmd = "update";
    }
    if (strtotime("$notif_date_from") > strtotime("$notif_date_to")) {
        $msg = "終了日は開始日よりも大きいです。";
        $err = 0;
    } elseif ($cmd == "new") {
        // Validate yyyy/mm/dd
        if (!validate_date($notif_date_from)) {
            $errDateFrom = "有効な日付(YYYY/MM/DD)を入力してください。";
            $err = 1;
        }
        if (!validate_date($notif_date_to)) {
            $errDateTo = "有効な日付(YYYY/MM/DD)を入力してください。";
            $err = 1;
        }

        if ($notif_title == "") {
            $title_err = "タイトル";
            $err = 1;
        } elseif (mb_strlen($notif_title) > CHECK_128_STRLENGTH) {
            $title_err = "128文字まで";
            $err = 1;
        }
        if ($err == 0) {
            // insert notification
            $sql = 'INSERT INTO tbl_notif
            ("cid", "empid", "notif_title", "notif_msg",
            "notif_time", "publish_date", "notif_status")';
            $sql .= "VALUES
          ('".pg_escape_string($cid_auth)."', '".pg_escape_string($empid_auth)."',
          '".pg_escape_string($notif_title)."', '".pg_escape_string($notif_name)."',
          '".pg_escape_string($notif_date_from)."',
          '".pg_escape_string($notif_date_to)."', '".pg_escape_string($notif_icon)."') RETURNING id";
            $insert = pg_query($smarty->_db, $sql);
            $insert_row = pg_fetch_row($insert);
            $insert_id = $insert_row[0];
            $logs[] = $sql;

            $sql2 = 'INSERT INTO info
          ("rank", "senddate", "title", "message", "toall", "id_notif")';
            $sql2 .= "VALUES
          ('".pg_escape_string($notif_icon)."', 'NOW()',
          '".pg_escape_string($notif_title)."',
          '".pg_escape_string($notif_name)."',
          '1', '".pg_escape_string($insert_id)."')";
            $logs[] = $sql2;
            $insert2 = pg_query($smarty->_db, $sql2);
            if (!$insert && !$insert2) {
                $msg = MESSAGES['ERR_INSERT'];
                $err = 0;
            } else {
                $msg = MESSAGES['INF_UPDATE'];
                $err = 1;
            }
        }
    } else {
        // Validate yyyy/mm/dd
        if (!validate_date($notif_date_from)) {
            $errDateFrom = "有効な日付(YYYY/MM/DD)を入力してください。";
            $err = 1;
        }
        if (!validate_date($notif_date_to)) {
            $errDateTo = "有効な日付(YYYY/MM/DD)を入力してください。";
            $err = 1;
        }
        if ($notif_title == "") {
            $title_err = "タイトル";
            $err = 1;
        } elseif (mb_strlen($notif_title) > CHECK_128_STRLENGTH) {
            $title_err = "128文字まで";
            $err = 1;
        }
        if ($err == 0) {
            $sql    = "UPDATE tbl_notif SET
                    notif_title = '".pg_escape_string($notif_title)."',
                    notif_msg = '".pg_escape_string($notif_name)."',
                    notif_time = '".pg_escape_string($notif_date_from)."',
                    publish_date = '".pg_escape_string($notif_date_to)."',
                    notif_status = '".pg_escape_string($notif_icon)."'
                    WHERE id = '".pg_escape_string((int)getreq("id"))."'";

            $logs[] = $sql;
            $update      = pg_query($smarty->_db, $sql);

            $sql2    = "UPDATE info SET
                    rank = '".pg_escape_string($notif_icon)."',
                    senddate = 'NOW()',
                    title = '".pg_escape_string($notif_title)."',
                    message = '".pg_escape_string($notif_name)."'
                    WHERE id_notif = '".pg_escape_string((int)getreq("id"))."'";
            $logs[]      = $sql2;

            $update_info = pg_query($smarty->_db, $sql2);

            if (!$update && !$update_info) {
                $msg = MESSAGES['ERR_INSERT'];
                $err = 0;
            } else {
                $msg = MESSAGES['INF_UPDATE'];
                $err = 1;
            }
        }
    }
} else {
    if ($cmd == "update") {
        $uid = (int)getreq("id");

        if ($uid <= 0) {
            errorScreenIds($smarty, "GETデータが不正です。(NOID)");
            exit;
        }

        $sql = sprintf(
            "SELECT
                        notif_title,
                        notif_msg,
                        notif_time,
                        publish_date,
                        notif_status
                    FROM tbl_notif
                    WHERE id = " . $uid ." "
        );
        $logs[] = $sql;
        $r   = pg_query($smarty->_db, $sql);

        if (pg_num_rows($r) != 1) {
            errorScreenIds($smarty, "GETデータが不正です。(IDNOTF)");
            exit;
        }

        $row = pg_fetch_row($r, 0);
        $notif_title = $row[0];
        $notif_name = $row[1];
        $notif_date_from = date("Y/m/d", strtotime($row[2]));
        $notif_date_to = date("Y/m/d", strtotime($row[3]));
        $notif_icon = (int)$row[4];

        $smarty->assign('uid', getreq("id"));
        $smarty->assign('notif_icon', $notif_icon);
    }
}
raise_sql($logs, 'addnotification_manager');

$smarty->assign('notif_title', $notif_title);
$smarty->assign('errDateFrom', $errDateFrom);
$smarty->assign('errDateTo', $errDateTo);
$smarty->assign('notif_name', $notif_name);
$smarty->assign('notif_date_from', $notif_date_from);
$smarty->assign('notif_date_to', $notif_date_to);
$smarty->assign('uid', $uid);

$smarty->assign('title_err', $title_err);
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'managerids/noti/addnotication.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('pageTitle', 'お知らせ');
$smarty->display(TEMPLATES_PATH . '/managerids.tpl');
