<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}

$err = $msg = $buton = "";
$err = 0;
$buton = postreq("button");

$sql = sprintf("SELECT id, notif_title, notif_msg, notif_time, notif_status, publish_date
        FROM tbl_notif
        ORDER BY publish_date DESC ");
$logs[] = $sql;
$r    = pg_query($smarty->_db, $sql);
$data = pg_fetch_all($r);

if ($buton == "削除") {
    $id = postreq('title');
    $sql = "DELETE FROM tbl_notif WHERE id = '".$id."'";
    $logs[] = $sql;
    $date = pg_query($smarty->_db, $sql);
    
    if (!$date) {
        $msg = MESSAGES['ERR_DELETE'];
        $err = 1;
    } else {
        $msg = MESSAGES['INF_DELETE'];
        $err = 0;
    }
}
raise_sql($logs, 'notification_manager');

$smarty->assign('data', $data);

$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'managerids/noti/notification.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('pageTitle', 'お知らせ');
$smarty->display(TEMPLATES_PATH . '/managerids.tpl');
