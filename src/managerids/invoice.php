<?php
include_once('etp_module.php');

if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}

// 分離ボタンが押されたとき
if (postreq("btn") == "分離" && postreq("actid") != "") {
    $sql = "UPDATE
        invoice_addr
        SET parentacctid is null,
            updated = now()
        WHERE costacctid='" . $postreq("actid") . "'";
    $r      = pg_query($smarty->_db, $sql);
    echo $sql;
}


$TARGET_PID = (int)getreq("pid");
if ($TARGET_PID <= 0) {
    $TARGET_PID = "";
}
$TARGET_PFROM = (int)getreq("price_from");
if ($TARGET_PFROM <= 0) {
    $TARGET_PFROM = "";
}
$TARGET_PTO = (int)getreq("price_to");
if ($TARGET_PTO <= 0) {
    $TARGET_PTO = "";
}

// 初期化
$err    = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg    = ""; // メッセージ
$cmd = getreq("cmd");

// ボタンが押されたときの処理
if ($cmd == "re") {
    // 請求書再生成処理
    if (getreq("id") == "") {
        // IDが空の場合はシステムエラー
        errorScreen($smarty, "システムエラーが発生しました。GETデータが不正です。");
        exit;
    }
    $msg = "請求書を再生成しました。";
    $err = 0;
}

// 年月プルダウンの表示

$curYM  = getreq("ym");
$sql    = "SELECT DISTINCT(ym) FROM invoice2 ORDER BY ym DESC limit 100";
$logs[] = $sql;
$r      = pg_query($smarty->_db, $sql);
if (pg_num_rows($r) != 0) {
    if ($curYM == "") {
        $curYM = pg_fetch_result($r, 0, 0);
    }
    $YMlist = "<select class='form-control' name='ym'>\n";
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $val    = pg_fetch_result($r, $i, 0);
        $YMlist .= "  <option value='".$val."'";
        if ($val == $curYM) {
            $YMlist .= " selected";
        }
        $YMlist .= ">".substr($val, 0, 4)."年".substr($val, 4, 2)."月"."</option>\n";
    }
    $YMlist .= "</select>\n";
    // レートの取得
    $sql    = "
        SELECT
            rate
            FROM invoice2
            WHERE ym = '".$curYM."'
                AND internaluse = false
                AND rate is not null
            ORDER BY rate DESC
            limit 1
    ";
    $logs[] = $sql;
    $r      = pg_query($smarty->_db, $sql);
    $rate   = pg_fetch_result($r, 0, 0);
} else {
    $curYM = date("Ym");
    $rate  = 0;
}

// 年と月に分割
    $curY = substr($curYM, 0, 4);
    $curM = substr($curYM, 4, 2);
    $curT = date("t", strtotime($curY . "-" . $curM . "-01"));
    $nextY = date("Y", strtotime($curY . "-" . $curM . "-01 + 1 month"));
    $nextM = date("m", strtotime($curY . "-" . $curM . "-01 + 1 month"));

// ソートキーボタンの設定
$sktable  = array(
    "a.costacctid",
    "a.costacctid DESC",
    "a.companyname, a.name",
    "a.companyname DESC, a.name DESC",
    "a.calcdate",
    "a.calcdate DESC",
    "b.totalusd NULLS FIRST",
    "b.totalusd DESC NULLS LAST",
);
$schar[0] = $schar[2] = $schar[4] = $schar[6] = SORT_DN;
$schar[1] = $schar[3] = $schar[5] = $schar[7] = SORT_UP;

// アクティブなソートキーボタンを設定
// 併せてSQLのソートキーを設定
$sk = getreq("sk");
if ($sk == "") {
    $sk = 0;
}
if ($sk >= 0 && $sk <= 7) {
    $skStr = $sktable[$sk];
    if (($sk % 2) == 0) {
        $schar[$sk] = SORT_DN_ACTIVE;
    } else {
        $schar[$sk] = SORT_UP_ACTIVE;
    }
} else {
    $skStr = "a.costacctid";
}

// filter conditions
$conditions = "";
if ($TARGET_PID > 0) {
    $conditions .= " AND a.payeracctid = '{$TARGET_PID}'";
}
if ($TARGET_PFROM > 0) {
    $conditions .= " AND a.totalusd >= {$TARGET_PFROM}";
}
if ($TARGET_PTO > 0) {
    $conditions .= " AND a.totalusd <= {$TARGET_PTO}";
}

// ページ総数の取得
$sql       = "
    SELECT count(costacctid) FROM invoice2 a
    WHERE
        ym='" . $curYM . "'
        $conditions
";
$logs[]    = $sql;
$r         = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);

// ページネーション文字列の生成
$curPage = (int)getreq("p");
if ($curPage <= 0) {
    $curPage = 1;
}
$curPageOffset = ($curPage - 1) * DEFAULT_COUNTPAGE_DEPT;
$pagenationStr = getPagenationStrOld(
    $smarty,
    $dataCount,
    DEFAULT_COUNTPAGE_DEPT,
    $curPage,
    "./invoice.php?sk={$sortIndex}&pid=" . $TARGET_PID . "&p="
);

$sql = "
    SELECT
        SUM(a.usageusd),
        SUM(a.usagejpy),
        SUM(a.creditusd),
        SUM(a.creditjpy),
        SUM(a.taxusd),
        SUM(a.taxjpy),
        SUM(a.totalusd),
        SUM(a.totaljpy)
    FROM
        invoice2 a
    WHERE
        a.ym = '$curYM'
        $conditions
    ";
$logs[]  = $sql;
$r       = pg_query($smarty->_db, $sql);
$arrCost = pg_fetch_row($r, 0);

// データテーブルの生成
$sql    = sprintf(
    "SELECT list.*, p.cnt
            FROM
        (
        SELECT
            a.costacctid costid,
            a.companyname,
            a.name,
            a.usageusd,
            a.usagejpy,
            a.taxusd,
            a.taxjpy,
            a.creditusd,
            a.creditjpy,
            a.totalusd,
            a.totaljpy,
            a.idsfee_usd,
            a.idsfee_jpy,
            a.paid,
            a.refnoserial,
            b.updated,
            a.calcdate,
            b.parentacctid,
            a.internaluse
        FROM
            invoice2 a,
            invoice_addr b
        WHERE
            a.costacctid = b.costacctid
            AND ym='" . $curYM . "'
            AND (
                b.parentacctid is null
                OR a.refnoserial is NOT null
            )
            " . $conditions . "
        ORDER BY " . $skStr . "
        LIMIT %d OFFSET %d
        ) AS list
        LEFT OUTER JOIN
        (
            SELECT parentacctid, count(parentacctid) cnt FROM invoice_addr
               GROUP BY parentacctid
               ORDER BY parentacctid
        ) AS p
        ON list.costid = p.parentacctid
    ",
    DEFAULT_COUNTPAGE_DEPT,
    $curPageOffset
);
$logs[] = $sql;
$r      = pg_query($smarty->_db, $sql);

$userList = "";
for ($i = 0; $i < pg_num_rows($r); $i++) {
    $row = pg_fetch_row($r, $i);
    $row[1] = $row[1] == "" ? '会社名:(未設定)': $row[1];
    $row[2] = $row[2] == "" ? '担当者名:(未設定)': $row[2];

    $userList .= "<tr><td>";
    $userList .= "AWS ID:" . $row[0] . "<br />";
    $userList .= $row[1] . "<br />" . $row[2]  . "<br />";
    if ($row[15] != "" && $row[15] > $row[16]) {
        // 請求書作成後に住所等が変更になった
        $userList .= "<br /><font color='red'>基本情報変更あり</font>";
    }
    if ($row[17] != "") {
        // 合算設定前に生成された請求書
        $userList .= "<br /><font color='red'>合算設定前の請求書あり</font>";
    }
    $userList .= "</td><td>";

    $u_usd = $row[3];
    $u_jpy = $row[4];
    $c_usd = $row[7];
    $c_jpy = $row[8];
    $t_usd = $row[5];
    $t_jpy = $row[6];
    $i_usd = $row[11];
    $i_jpy = $row[12];
    $to_usd = $row[9];
    $to_jpy = $row[10];

    if ($row[19] != "" && $row[19] != 0) {
        $userList .= getFeeTableStr(
            $u_usd,
            $u_jpy,
            $c_usd,
            $c_jpy,
            $t_usd,
            $t_jpy,
            $i_usd,
            $i_jpy,
            $to_usd,
            $to_jpy,
            true
        );
    } else {
        $userList .= getFeeTableStr(
            $u_usd,
            $u_jpy,
            $c_usd,
            $c_jpy,
            $t_usd,
            $t_jpy,
            $i_usd,
            $i_jpy,
            $to_usd,
            $to_jpy,
            false
        );
    }
    if ($row[19] != "" && $row[19] != 0) {
        $sql = "
            SELECT
                b.usageusd,
                b.usagejpy,
                b.creditusd,
                b.creditjpy,
                b.taxusd,
                b.taxjpy,
                b.idsfee_usd,
                b.idsfee_jpy,
                b.totalusd,
                b.totaljpy,
                a.costacctid
            FROM
                (
                    SELECT
                        costacctid
                        FROM
                            invoice_addr
                        WHERE
                            parentacctid='" . $row[0] . "'
                        ORDER BY
                            costacctid
                ) AS a,
                invoice2 b
            WHERE
                a.costacctid = b.costacctid
                AND b.refnoserial is NULL
            ORDER BY a.costacctid
        ";
        $r_child = pg_query($smarty->_db, $sql);
        for ($child_idx = 0; $child_idx < pg_num_rows($r_child); $child_idx++) {
            $row2 = pg_fetch_row($r_child, $child_idx);
            $userList .= "<form METHOD='POST' ACTION='{$curURI}'>";
            $userList .= "AWS ID: " . $row2[10]
                . "<input type='hidden' name='actid' value='{$row2[10]}' />"
                . " <input type='submit' class='btn btn-primary bt-sign' name='btn' value='分離' />"
                . "</form>";
            $userList .= getFeeTableStr(
                $row2[0],
                $row2[1],
                $row2[2],
                $row2[3],
                $row2[4],
                $row2[5],
                $row2[6],
                $row2[7],
                $row2[8],
                $row2[9],
                true
            );
            $u_usd += $row2[0];
            $u_jpy += $row2[1];
            $c_usd += $row2[2];
            $c_jpy += $row2[3];
            $t_usd += $row2[4];
            $t_jpy += $row2[5];
            $i_usd += $row2[6];
            $i_jpy += $row2[7];
            $to_usd += $row2[8];
            $to_jpy += $row2[9];
        }
        $userList .= "合算合計<br />";
        $userList .= getFeeTableStr(
            $u_usd,
            $u_jpy,
            $c_usd,
            $c_jpy,
            $t_usd,
            $t_jpy,
            $i_usd,
            $i_jpy,
            $to_usd,
            $to_jpy,
            false
        );
    }
    $userList .= "</td>\n";

    // 変更ボタン
    $userList .= "<td>
        <a class='btn btn-primary' href='./upd_invoice.php?cmd=update&id=" .
            $row[0]."' role='button'>変更</a>
        <a class='btn btn-primary' href='./upd_invoice2.php?id=" .
            $row[0]."' role='button'>合算請求の設定</a><br /><br />
        <button type='button' class='btn btn-info' data-toggle='collapse' data-target='#detail{$row[0]}'>
            <i class='glyphicon glyphicon-exclamation-sign'>
            </i> 明細の表示/非表示
        </button><br />
    ";
    if ($row[14] != "") {
        $userList .= "<a href='./pdfadmin.php?id=" . $row[0] .
            "&ym=".$curYM."'>請求書ダウンロード</a><br />";
        $userList .= "請求書番号:C".substr($curYM, 2, 4).str_pad(
            $row[14],
            5,
            0,
            STR_PAD_LEFT
        ) ;
    } else {
        $userList .= "請求書なし";
    }
    $userList .= "<br />算出/生成:" . dateCool($row[16]);
    if ($row[18] == 't') {
        $userList .= "<br /><br /><font color='red'>社内利用</font><br />";
    }
    $userList .= "</td></tr>\n";

    // 請求明細の表示
    $sql = "SELECT
                productcode,
                region,
                usagetype,
                lineitemdescription,
                sum(usageamount),
                sum(unblendedcost),
                unit,
                billingentity
                FROM
                    cost_detail
                WHERE
                    cid=" . $row[0] . "
                    AND ym='" . $curY . "/" . $curM . "'
                    AND (
                        lineitemtype = 'Usage'
                        OR lineitemtype = 'Credit'
                        OR lineitemtype = 'Fee'
                        OR lineitemtype = 'RIFee'
                    )
                    AND (
                        usageamount != 0
                        OR unblendedcost != 0
                    )
                GROUP BY
                    billingentity,
                    productcode,
                    region,
                    usagetype,
                    lineitemdescription,
                    unit
                ORDER BY
                    billingentity DESC,
                    productcode,
                    region,
                    usagetype,
                    lineitemdescription;
    ";
    $r2     = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r2) > 0) {
        $userList .= "<tr><td colspan='5'>\n";
        $userList .= "
        <button type='button' class='btn btn-info' data-toggle='collapse' data-target='#detail{$row[0]}'>
            <i class='glyphicon glyphicon-exclamation-sign'>
            </i> 明細の表示/非表示
        </button>
        <br />
        <br />
        <div id='detail{$row[0]}' class='collapse'>
            <div class='container col-xs-12'>
        ";

        $curBillingEntity = $curService = $curRegion = $curUsagetype = "";
        $tblSW = false;

        for ($j =0; $j < pg_num_rows($r2); $j++) {
            $row2 = pg_fetch_row($r2, $j);
            $row2[1] = local_regionName($row2[1]);

            if ($curBillingEntity != $row2[7]) {
                if ($tableSW == true) {
                    $userList .= "</table><br />\n";
                    $tableSW = false;
                }
                $userList .= "<b>" . $row2[7] . "</b><br />\n";
                $curBillingEntity = $row2[7];
                $curService = $curRegion = $curUsagetype = "";
            }

            if ($curService != $row2[0]) {
                if ($tableSW == true) {
                    $userList .= "</table><br />\n";
                    $tableSW = false;
                }
                $userList .= "    サービス名：<b>" . $row2[0] . "</b><br />\n";
                $curService = $row2[0];
                $curRegion = $curUsagetype = "";
            }

            if ($curRegion != $row2[1]) {
                if ($tableSW == true) {
                    $userList .= "</table><br />\n";
                    $tableSW = false;
                }
                $userList .= "            リージョン名：<b>" . $row2[1] . "</b><br />\n";
                $curRegion = $row2[1];
                $userList .= "
                <table class='table table-hover'>
                    <tr>
                    <th class='col-xs-7'>詳細</th>
                    <th class='col-xs-3'>利用量</th>
                    <th class='col-xs-2'>USD</th>
                    </tr>
                ";
                $tableSW = true;
                $curUsagetype = "";
            }

            if ($curUsagetype != $row2[2]) {
                if ($curPfamily == 'Request') {
                    $userList .= "
                        <tr>
                        <td colspan='3'><b>
                    ";
                    $userList .= $curService . " " . $row2[2];
                    $userList .= "
                        </b></td>
                        </tr>
                    ";
                }
                $curUsagetype = $row2[2];
            }

            $userList .= "
                    <tr>
                    <td>" . $row2[3] . "</td>
                    <td align='right'>";
            switch ($row2[6]) {
                case 'Requests':
                case 'Events':
                    $row2[4] = number_format($row2[4]);
                    break;
                case 'Alarms':
                case 'Metrics':
                    $row2[4] = number_format($row2[4], 3);
                    break;
            }
            $userList .= $row2[4] . " " . $row2[6] . "</td>
                    <td align='right'>$" . number_format($row2[5], 2) . "</td>
                    </tr>\n";
        }
        if ($tableSW == true) {
            $userList .= "</table><br />\n";
            $tableSW = false;
        }
        $userList .= "</div>
            </div></td></tr>\n";
    }
}
raise_sql($logs, 'etp_manager');

$smarty->assign('schar', $schar);

$smarty->assign('pagenationStr', $pagenationStr);

$smarty->assign('k', htmlspecialchars(postreq("k")));
$smarty->assign('pageTitle', '請求処理');
$smarty->assign('dataCount', $dataCount);

$smarty->assign('usageusd', number_format($arrCost[0], 2));
$smarty->assign('usagejpy', number_format($arrCost[1]));
$smarty->assign('creditusd', number_format($arrCost[2], 2));
$smarty->assign('creditjpy', number_format($arrCost[3]));
$smarty->assign('taxusd', number_format($arrCost[4], 2));
$smarty->assign('taxjpy', number_format($arrCost[5]));
$smarty->assign('totalusd', number_format($arrCost[6], 2));
$smarty->assign('totaljpy', number_format($arrCost[7]));

$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('YMlist', $YMlist);
$smarty->assign('rate', $rate);
$smarty->assign('ymStr', substr($curYM, 0, 4)."年".substr($curYM, 4, 2)."月");
$smarty->assign('curYM', $curYM);
$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('userList', $userList);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'managerids/invoice.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('TARGET_PID', $TARGET_PID);
$smarty->assign('TARGET_PFROM', $TARGET_PFROM);
$smarty->assign('TARGET_PTO', $TARGET_PTO);
$smarty->display(TEMPLATES_PATH.'managerids.tpl');
