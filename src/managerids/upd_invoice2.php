<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}
// 初期化
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$updateMessage = "";
$name = $company = $dept = $position = $tel = $email = $email2 = "";
$position_err = $dept_err = $name_err = $tel_err = $company_err = "";
$email_err = $email_config_err = $internaluse = $howtosend = "";
$payday_err = "";
$acid = "";
$button = postreq("btn");

// ボタンが押されたときの処理
if ($button == "保存") {
    $acid = postreq("acid");
    $linkto = postreq("linkto");

    if (empty($acid)) {
        $msg .= "システムエラー：acidが取得できませんでした。<br />";
        $err = 1;
    }

    if ($acid == $linkto) {
        $msg .= "エラー：自分のAWS IDに合算することはできません。<br />";
        $err = 1;
    }

    if ($linkto != "") {
        // linktoが指定された場合は、存在するかチェック
        $sql = "
            SELECT costacctid
                FROM invoice_addr
                WHERE costacctid = '" . pg_escape_string($linkto) . "'
        ";
        $r = pg_query($smarty->_db, $sql);
        if (pg_num_rows($r) == 0) {
            $msg .= "指定されたAWS IDは登録されていません。先に合算先の情報を登録してください。<br />";
            $err = 1;
        }
    }

    if ($err != 1) {
        $sql = "
            UPDATE 
                invoice_addr 
                SET 
                    parentacctid = 
        ";
        if ($linkto != "") {
            $sql .= "'" . pg_escape_string($linkto) . "'";
        } else {
            $sql .= "null";
        }
        $sql .= "
                WHERE costacctid = '" . pg_escape_string($acid) . "'
        ";

        $r = pg_query($smarty->_db, $sql);

        $msg = "保存しました。<br /><br />
               <a href='/managerids/invoice.php'>戻る</a>";
        $err = 0;
    }
} else {
    // 初期GETアクセスの場合
    $acid = getreq("id");

    $sql = "
        SELECT
            parentacctid
            FROM invoice_addr 
            WHERE 
               costacctid = '" . pg_escape_string($acid) . "'
        ";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r) != 1) {
        errorScreenIds($smarty, "GETデータが不正です。(CIDNOTF)");
        exit;
    }

    $row      = pg_fetch_row($r, 0);
    $linkto     = $row[0];
}

raise_sql($logs, 'upd_manager');
$smarty->assign('acid', htmlspecialchars($acid));
$smarty->assign('linkto', htmlspecialchars($linkto));

$smarty->assign('pageTitle', '合算請求の設定');

$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'managerids/upd_invoice2.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'managerids.tpl');
