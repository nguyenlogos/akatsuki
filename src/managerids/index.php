<?php
$msg = '';
$result = "";
if (!empty($_SESSION['login_font'])) {
    rd("managerids/etp.php");
}
if (isset($_POST['bt_login'])) {
    $result = LoGinAuth($smarty->_db);
    if (empty($result)) {
        header("Location: " . ROOT_HOME . "managerids/etp.php");
    }
}
$smarty->assign('error', $result);
$smarty->assign('viewTemplate', 'managerids/index.tpl');
$smarty->display(TEMPLATES_PATH . 'managerids.tpl');
