<?php
defined("COMMON_PATH") or define("COMMON_PATH", dirname(__DIR__) . "/../common");
require_once(COMMON_PATH . "/CommonClass.class.php");
require_once(COMMON_PATH . "/common.php");
//require_once(LIBS_PATH . "../phpmailer/class.phpmailer.php");

$smarty = new MySmarty(false);

if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}
$purifier = new HTMLPurifier();
$msg = "";
$err = 0;
$name = $company = $tel = $dept = $position = $uid = "";
$cid_auth = $_SESSION['login_font']["cid_auth"];

if (postreq("button") == "変更") {
    // 必須事項が入力されているか確認
    $name     = htmlspecialchars(postreq("name"));
    $company  = htmlspecialchars(postreq("company"));
    $tel      = postreq("tel");
    $dept     = htmlspecialchars(postreq("dept"));
    $position = htmlspecialchars(postreq("position"));
    $uid      = postreq("uid");
    
    $errField = "";
    if ($name == "") {
        $errField .= "[お名前]";
    }
    if ($company == "") {
        $errField .= "[会社名]";
    }
    if ($tel == "") {
        $errField .= "[電話番号]";
    }
    if ($uid == "") {
        $errField .= "[メールアドレス]";
    }
    
    if ($errField != "") {
        $msg = $errField . "が入力されていません。";
        $err = 1;
    }
    
    if ($err == 1) {
        // エスケープはtpl側で実施
        $smarty->assign('name', $name);
        $smarty->assign('company', $company);
        $smarty->assign('dept', $dept);
        $smarty->assign('position', $position);
        $smarty->assign('tel', $tel);
        $smarty->assign('uid', $uid);
        
        $smarty->assign('err', $err);
        $smarty->assign('msg', $msg);
        $smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
        $smarty->assign('viewTemplate', 'managerids/profile/profile.tpl');
        $smarty->display(TEMPLATES_PATH . '/managerids.tpl');
        exit;
    }
    
    
    // ランダム文字列の生成
    
    $sql = "BEGIN";
    $r   = pg_query($smarty->_db, $sql);
    
    // 登録処理
    $sql = "UPDATE users SET uid = '" . pg_escape_string($uid) . "', name = '" . pg_escape_string($name) . "', 
                    companyname = '" . pg_escape_string($company) . "',dept = ";
    $logs[] = $sql;
    if ($dept == "") {
        $sql .= "null,";
    } else {
        $sql .= "'" . pg_escape_string($dept) . "',";
    }
    $sql .= "position=";
    if ($position == "") {
        $sql .= "null,";
    } else {
        $sql .= "'" . pg_escape_string($position) . "',";
    }
    $sql .= "tel = '" . pg_escape_string($tel) . "' WHERE cid = " . $cid_auth;
    $r    = pg_query($smarty->_db, $sql);
    
    $sql = "COMMIT;";
    $r   = pg_query($smarty->_db, $sql);
    
    $msg = "変更しました。";
    $err = 0;
} else {
    // ボタンが押されていない場合
    $sql = sprintf("SELECT name, tel, dept, companyname, position,uid 
                    FROM users 
                    WHERE cid = " . $cid_auth . " AND status = 0");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    
    if (pg_num_rows($r) != 1) {
        $msg = "システムエラーが発生しました。(NOT1U)";
        $err = 1;
    } else {
        $row        = pg_fetch_row($r, 0);
        $name       = $row[0];
        $tel        = $row[1];
        $dept       = $row[2];
        $company    = $row[3];
        $position   = $row[4];
        $uid        = $row[5];
    }
}

raise_sql($logs, 'profile_manager');
$smarty->disconnect();

$smarty->assign('name', $name);
$smarty->assign('company', $company);
$smarty->assign('dept', $dept);
$smarty->assign('position', $position);
$smarty->assign('tel', $tel);
$smarty->assign('uid', $uid);

$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('viewTemplate', 'managerids/profile/profile.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . '/managerids.tpl');
