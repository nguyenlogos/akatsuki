<?php
if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}

$msg = "";
$err = 0;
$p1_err = $p2_err = $p3_err = "";
$p1  = "";
$p2  = "";
$p3  = "";
$button   = postreq("button");
$cid_auth = $_SESSION['login_font']["cid_auth"];
$uid_auth = $_SESSION['login_font']["uid_auth"];


$p1 = htmlspecialchars(postreq("p1"));
$p2 = htmlspecialchars(postreq("p2"));
$p3 = htmlspecialchars(postreq("p3"));

// Check validate form changepw
if (mb_strlen($p1) > CHECK_64_STRLENGTH) {
    $err = 1;
}
if (mb_strlen($p2) > CHECK_64_STRLENGTH) {
    $err = 1;
}
if (mb_strlen($p3) > CHECK_64_STRLENGTH) {
    $err = 1;
}

if (($button == "変更")&&($err != 1)) {
    
    $sql = sprintf("select empid
                    from emp
                    where cid = $1
                        and email = $2
                        and pass = $3
                        and status = 0");
    $logs[] = $sql;
    $r   = pg_query_params($smarty->_db, $sql, [
        $cid_auth,
        $uid_auth,
        pg_escape_string(md5($p1))
    ]);

    /*commetn*/
    if (pg_num_rows($r) == 0) {
        $msg = "現在のパスワードが正しくありません。\n";
        $err = 1;
        $p1 = "";
    } else {
        if ($p2 != $p3) {
            $msg = "再入力したパスワードが一致していません。\n";
            $err = 1;
            $p2 = "";
            $p3 = "";
        } else {
            $sql = "update emp set pass='" . pg_escape_string(md5($p2)) . "'
                    where cid = " . $cid_auth . "
                        and email = '" . $uid_auth . "'
                        and pass = '" . pg_escape_string(md5($p1)) . "'
                        and status = 0";
            $logs[] = $sql;
            pg_query($smarty->_db, $sql);
            $msg = "パスワードを変更しました。ログインしなおしてください。\n";
            $err = 0;
            LogoutAuth();
        }
    }
}
raise_sql($logs, 'changepw_manager');

$smarty->assign('p1', $p1);
$smarty->assign('p2', $p2);
$smarty->assign('p3', $p3);
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('viewTemplate', 'managerids/profile/changepw.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . '/managerids.tpl');
