<?php
include_once('etp_module.php');

if ($_SESSION['login_font']['idsadmin_auth'] == 0) {
    errorScreenIds($smarty, "アクセスが拒否されました。");
    exit;
}
$TARGET_CID = (int)getreq("cid");
if ($TARGET_CID <= 0) {
    $TARGET_CID = "";
}
$TARGET_PFROM = (int)getreq("price_from");
if ($TARGET_PFROM <= 0) {
    $TARGET_PFROM = "";
}
$TARGET_PTO = (int)getreq("price_to");
if ($TARGET_PTO <= 0) {
    $TARGET_PTO = "";
}

// 初期化
$err    = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg    = ""; // メッセージ
$cmd = getreq("cmd");

// ボタンが押されたときの処理
if ($cmd == "delete") {
    // 削除処理
    if (getreq("id") == "") {
        // IDが空の場合はシステムエラー
        errorScreen($smarty, "システムエラーが発生しました。GETデータが不正です。");
        exit;
    }
    // PostgreSQL用にIDをエスケープ
    $target_cid = pg_escape_string(getreq("id"));

    $sql    = "UPDATE users SET status = -1 WHERE cid = ".$target_cid." AND status = 0";
    $logs[] = $sql;
    $r      = pg_query($smarty->_db, $sql);

    $sql    = "UPDATE emp SET status = -1 WHERE cid = ".$target_cid." AND status = 0";
    $logs[] = $sql;
    $r      = pg_query($smarty->_db, $sql);

    $sql    = "UPDATE dept SET status = -1 WHERE cid = ".$target_cid." AND status = 0";
    $logs[] = $sql;
    $r      = pg_query($smarty->_db, $sql);

    $sql    = "UPDATE proj SET status = -1 WHERE cid = ".$target_cid." AND status = 0";
    $logs[] = $sql;
    $r      = pg_query($smarty->_db, $sql);

    $msg = "削除しました。";
    $err = 0;
}

// 年月プルダウンの表示

$curYM  = getreq("ym");
$sql    = "SELECT DISTINCT(ym) FROM invoice ORDER BY ym DESC limit 100";
$logs[] = $sql;
$r      = pg_query($smarty->_db, $sql);
if (pg_num_rows($r) != 0) {
    if ($curYM == "") {
        $curYM = pg_fetch_result($r, 0, 0);
    }
    $YMlist = "<select class='form-control' name='ym'>\n";
    for ($i = 0; $i < pg_num_rows($r); $i++) {
        $val    = pg_fetch_result($r, $i, 0);
        $YMlist .= "  <option value='".$val."'";
        if ($val == $curYM) {
            $YMlist .= " selected";
        }
        $YMlist .= ">".substr($val, 0, 4)."年".substr($val, 4, 2)."月"."</option>\n";
    }
    $YMlist .= "</select>\n";
    // レートの取得
    $sql    = "SELECT rate FROM invoice WHERE ym = '".$curYM."' limit 1";
    $logs[] = $sql;
    $r      = pg_query($smarty->_db, $sql);
    $rate   = pg_fetch_result($r, 0, 0);
} else {
    $curYM = date("Ym");
    $rate  = 0;
}

// 年と月に分割
    $curY = substr($curYM, 0, 4);
    $curM = substr($curYM, 4, 2);
    $curT = date("t", strtotime($curY . "-" . $curM . "-01"));
    $nextY = date("Y", strtotime($curY . "-" . $curM . "-01 + 1 month"));
    $nextM = date("m", strtotime($curY . "-" . $curM . "-01 + 1 month"));

// ソートキーボタンの設定
$sktable  = array(
    "a.cid",
    "a.cid DESC",
    "a.companyname,a.dept,a.name,a.uid",
    "a.companyname DESC,a.dept DESC NULLS LAST,a.name DESC,a.uid DESC",
    "a.status DESC",
    "a.status",
    "b.usagejpy NULLS FIRST",
    "b.usagejpy DESC NULLS LAST",
);
$schar[0] = $schar[2] = $schar[4] = $schar[6] = SORT_DN;
$schar[1] = $schar[3] = $schar[5] = $schar[7] = SORT_UP;

// アクティブなソートキーボタンを設定
// 併せてSQLのソートキーを設定
$sk = getreq("sk");
if ($sk == "") {
    $sk = 0;
}
if ($sk >= 0 && $sk <= 7) {
    $skStr = $sktable[$sk];
    if (($sk % 2) == 0) {
        $schar[$sk] = SORT_DN_ACTIVE;
    } else {
        $schar[$sk] = SORT_UP_ACTIVE;
    }
} else {
    $skStr = "a.cid";
}

// filter conditions
$conditions = "";
if ($TARGET_CID > 0) {
    $conditions .= " AND a.cid = {$TARGET_CID}";
}
if ($TARGET_PFROM > 0) {
    $conditions .= " AND b.totaljpy >= {$TARGET_PFROM}";
}
if ($TARGET_PTO > 0) {
    $conditions .= " AND b.totaljpy <= {$TARGET_PTO}";
}

// ページ総数の取得
if ($TARGET_CID > 0) {
    $dataCount = 1;
} else {
    $sql       = "
        SELECT count(cid) FROM users
        WHERE
            status = 0
            $conditions
    ";
    $logs[]    = $sql;
    $r         = pg_query($smarty->_db, $sql);
    $dataCount = (int)pg_fetch_result($r, 0, 0);
}
// ページネーション文字列の生成
$curPage = (int)getreq("p");
if ($curPage <= 0) {
    $curPage = 1;
}
$curPageOffset = ($curPage - 1) * DEFAULT_COUNTPAGE_DEPT;
//$urlParams     = [
//    'sk'         => $sk,
//    'ym'         => $curYM,
//    'cid'        => $TARGET_CID,
//    'price_from' => $TARGET_PFROM,
//    'price_to'   => $TARGET_PTO,
//    'p'          => '',
//];
//$url = http_build_query($urlParams);
$pagenationStr = getPagenationStrOld(
    $smarty,
    $dataCount,
    DEFAULT_COUNTPAGE_DEPT,
    $curPage,
    "./etp.php?sk={$sortIndex}&p="
);

$sql = "
    SELECT
        SUM(b.usageusd),
        SUM(b.usagejpy),
        SUM(b.taxusd),
        SUM(b.taxjpy),
        SUM(b.totalusd),
        SUM(b.totaljpy)
    FROM
        invoice b
    WHERE
        b.ym = '$curYM'
        $conditions
    ";
$logs[]  = $sql;
$r       = pg_query($smarty->_db, $sql);
$arrCost = pg_fetch_row($r, 0);
// データテーブルの生成

$sql    = sprintf(
    "
    SELECT
        a.cid,
        a.companyname,
        'TEMP',
        b.usageusd,
        b.usagejpy,
        b.taxusd,
        b.taxjpy,
        b.rate,
        b.paid,
        b.refnoserial,
        a.dept,
        a.name,
        a.uid,
        b.blended_usageusd,
        b.blended_usagejpy,
        b.blended_taxusd,
        b.blended_taxjpy,
        b.blended_totalusd,
        b.blended_totaljpy,
        b.idsfee_usd,
        b.idsfee_jpy,
        p.plan,
        a.costacctid
    FROM
        users a
    LEFT OUTER JOIN
        (SELECT *
	 FROM
             invoice
         WHERE
             ym = '{$curYM}') AS b
        ON a.cid = b.cid
    LEFT JOIN plan p ON p.startuid = a.uid
    WHERE
        a.status=0
        $conditions
    ORDER BY {$skStr}
    LIMIT %d OFFSET %d
    ",
    DEFAULT_COUNTPAGE_DEPT,
    $curPageOffset
);

$logs[] = $sql;
$r      = pg_query($smarty->_db, $sql);
$userList = "";
for ($i = 0; $i < pg_num_rows($r); $i++) {
    $row = pg_fetch_row($r, $i);

    $chargeDays = getChargedDays($smarty, $row[0], $curY, $curM, $nextY, $nextM);

    if ($chargeDays == 0) {
        $row[2] = "無料";
    } else {
        $row[2] = "有料：" . $chargeDays . "日間";
    }

    $userList .= "<tr><td>" . $row[0] . "</td>";
    $userList .= "<td>" . $row[1] . "<br />" . $row[10] . "<br />" .
        $row[11] . "<br />" . $row[12] . "<br />AWSアカウントID : " .
        $row[22] . "</td>";
    $userList .= "<td>" . $row[2] . "</td>";
    if ($row[3] == "") {
        $userList .= "<td>&nbsp;</td>";
    } else {
        $userList .= "<td>請求書番号:C".substr($curYM, 2, 4).str_pad(
            $row[9],
            5,
            0,
            STR_PAD_LEFT
        ) . "<br />";

        $userList .= "利用料 $" . number_format(
            $row[3],
            2
        )." / " . number_format($row[4])."円<br />\n";
        $userList .= "消費税 $" . number_format(
            $row[5],
            2
        )." / " . number_format($row[6])."円<br />\n";
        $userList .= "IDS (5%) $" . number_format(
            $row[19],
            2
        ) . " / " . number_format($row[20])."円<br />\n";
        $userList .= "合　計 $" . number_format(
            $row[3] + $row[5] + $row[19],
            2
        )." / " . number_format($row[4] + $row[6] + $row[20]) . "円<br /></td>\n";
    }
    $charges = $row[21] == 1 ? 'No charges': 'Charges';
    // 変更ボタン
    $userList .= "<td>
        <p><a class='btn btn-primary' href='./upd.php?cmd=update&id=" .
            $row[0]."' role='button'>変更</a>
        <a class='btn btn-primary' href='./etp.php?cmd=delete&id=".$row[0]."'
          role='button' onClick='return confDelete()'>削除</a></p>
        <p><a class='btn btn-warning' href='./upd2.php?id=".$row[0]."'
          role='button'>API設定</a>
           <span class='btn btn-success'>".$charges."</span>
        </p>";

    if ($row[3] != "") {
        $userList .= "<a href='./pdfadmin.php?id=" . $row[0] .
            "&ym=".$curYM."'>請求書ダウンロード</a>";
    } else {
        $userList .= "請求書なし<br />";
    }
    $userList .= "</td></tr>\n";

    // 請求明細の表示
    $sql = "SELECT
                productcode,
                region,
                usagetype,
                lineitemdescription,
                sum(usageamount),
                sum(unblendedcost),
                unit,
                billingentity
                FROM
                    cost_detail
                WHERE
                    cid=" . $row[0] . "
                    AND ym='" . $curY . "/" . $curM . "'
                    AND (
                        lineitemtype = 'Usage'
                        OR lineitemtype = 'Credit'
                        OR lineitemtype = 'Fee'
                        OR lineitemtype = 'RIFee'
                    )
                    AND (
                        usageamount != 0
                        OR unblendedcost != 0
                    )
                GROUP BY
                    billingentity,
                    productcode,
                    region,
                    usagetype,
                    lineitemdescription,
                    unit
                ORDER BY
                    billingentity DESC,
                    productcode,
                    region,
                    usagetype,
                    lineitemdescription;
    ";
    $r2     = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r2) > 0) {
        $userList .= "<tr><td colspan='5'>\n";
        $userList .= "
        <button type='button' class='btn btn-info' data-toggle='collapse' data-target='#detail{$row[0]}'>
            <i class='glyphicon glyphicon-exclamation-sign'>
            </i> 明細の表示/非表示
        </button>
        <br />
        <br />
        <div id='detail{$row[0]}' class='collapse'>
            <div class='container col-xs-12'>
        ";

        $curBillingEntity = $curService = $curRegion = $curUsagetype = "";
        $tblSW = false;

        for ($j =0; $j < pg_num_rows($r2); $j++) {
            $row2 = pg_fetch_row($r2, $j);
            $row2[1] = local_regionName($row2[1]);

            if ($curBillingEntity != $row2[7]) {
                if ($tableSW == true) {
                    $userList .= "</table><br />\n";
                    $tableSW = false;
                }
                $userList .= "<b>" . $row2[7] . "</b><br />\n";
                $curBillingEntity = $row2[7];
                $curService = $curRegion = $curUsagetype = "";
            }

            if ($curService != $row2[0]) {
                if ($tableSW == true) {
                    $userList .= "</table><br />\n";
                    $tableSW = false;
                }
                $userList .= "    サービス名：<b>" . $row2[0] . "</b><br />\n";
                $curService = $row2[0];
                $curRegion = $curUsagetype = "";
            }

            if ($curRegion != $row2[1]) {
                if ($tableSW == true) {
                    $userList .= "</table><br />\n";
                    $tableSW = false;
                }
                $userList .= "            リージョン名：<b>" . $row2[1] . "</b><br />\n";
                $curRegion = $row2[1];
                $userList .= "
                <table class='table table-hover'>
                    <tr>
                    <th class='col-xs-7'>詳細</th>
                    <th class='col-xs-3'>利用量</th>
                    <th class='col-xs-2'>USD</th>
                    </tr>
                ";
                $tableSW = true;
                $curUsagetype = "";
            }

            if ($curUsagetype != $row2[2]) {
                if ($curPfamily == 'Request') {
                    $userList .= "
                        <tr>
                        <td colspan='3'><b>
                    ";
                    $userList .= $curService . " " . $row2[2];
                    $userList .= "
                        </b></td>
                        </tr>
                    ";
                }
                $curUsagetype = $row2[2];
            }

            $userList .= "
                    <tr>
                    <td>" . $row2[3] . "</td>
                    <td align='right'>";
            switch ($row2[6]) {
                case 'Requests':
                case 'Events':
                    $row2[4] = number_format($row2[4]);
                    break;
                case 'Alarms':
                case 'Metrics':
                    $row2[4] = number_format($row2[4], 3);
                    break;
            }
            $userList .= $row2[4] . " " . $row2[6] . "</td>
                    <td align='right'>$" . number_format($row2[5], 2) . "</td>
                    </tr>\n";
        }
        if ($tableSW == true) {
            $userList .= "</table><br />\n";
            $tableSW = false;
        }
        $userList .= "</div>
            </div></td></tr>\n";
    }
}
raise_sql($logs, 'etp_manager');

$smarty->assign('schar', $schar);

$smarty->assign('pagenationStr', $pagenationStr);

$smarty->assign('k', htmlspecialchars(postreq("k")));
$smarty->assign('pageTitle', '契約企業管理');
$smarty->assign('dataCount', $dataCount);

$smarty->assign('usageusd', number_format($arrCost[0], 2));
$smarty->assign('usagejpy', number_format($arrCost[1]));
$smarty->assign('taxusd', number_format($arrCost[2], 2));
$smarty->assign('taxjpy', number_format($arrCost[3]));
$smarty->assign('totalusd', number_format($arrCost[4], 2));
$smarty->assign('totaljpy', number_format($arrCost[5]));

$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('YMlist', $YMlist);
$smarty->assign('rate', $rate);
$smarty->assign('ymStr', substr($curYM, 0, 4)."年".substr($curYM, 4, 2)."月");
$smarty->assign('curYM', $curYM);
$smarty->assign('userID', $_SESSION['login_font']['uid_auth']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('userList', $userList);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'managerids/etp.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('TARGET_CID', $TARGET_CID);
$smarty->assign('TARGET_PFROM', $TARGET_PFROM);
$smarty->assign('TARGET_PTO', $TARGET_PTO);
$smarty->display(TEMPLATES_PATH.'managerids.tpl');
