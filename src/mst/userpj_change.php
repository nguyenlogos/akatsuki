<?php
use Akatsuki\Models\EmpProj;

$hideDept = false;

$frmValues = [
    'dept'  => getreq('dept'),
    'proj'  => getreq('proj'),
    'uname' => getreq('uname'),
];

if (empty($frmValues['proj'])) {
    rd('mst/userpj.php');
}
$deptRoles = permission_check("mst/dept.php");
if (!$deptRoles['read']['allowed']) {
    $frmValues['dept'] = $_SESSION['dept'];
    $hideDept = true;
}

// determine sort column
$sortIndex = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
$sortMap = [
    "e.name","e.name DESC",
    "e.email","e.email DESC",
    "d.deptname","d.deptname DESC",
];
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

$whereConditions = [
    "e.cid = {$_SESSION['cid']}",
    "e.status = 0",
];
if (!empty($frmValues['dept'])) {
    $whereConditions[] = "e.dept = " . (int)$frmValues['dept'];
}
if (!empty($frmValues['uname'])) {
    $fmUname = strtoupper(pg_escape_string($frmValues['uname']));
    $whereConditions[] = "upper(e.name) LIKE '%%{$fmUname}%%'";
}
$whereConditions = implode(" AND ", $whereConditions);
$sql = sprintf("
    SELECT
        e.empid,
        e.name as empname,
        e.email as empmail,
        ep.id,
        ep.pcode,
        p.pname,
        (
            CASE
                WHEN ep.pcode IS NULL THEN '0'
                ELSE '1'
            END
        ) AS epcode,
        d.dept as deptid,
        d.deptname
    FROM
        emp e
    LEFT JOIN
    (
        SELECT
            ep.id,
            ep.cid,
            ep.pcode,
            ep.empid
        FROM
            emp_proj ep
        WHERE
            ep.cid = {$_SESSION['cid']}
            AND ep.pcode = '%s'
    ) ep
        ON e.cid = ep.cid
        AND e.empid = ep.empid
    LEFT JOIN
        proj p
        ON p.cid = ep.cid
        AND p.pcode = ep.pcode
        AND p.status = 0
    LEFT JOIN
        dept d
        ON d.cid = e.cid
        AND d.dept = e.dept
        AND d.status = 0
    WHERE {$whereConditions}
    ORDER BY {$sortColumn}
", pg_escape_string($frmValues['proj']));
$result = pg_query($smarty->_db, $sql);
$tableList = pg_fetch_all($result);

$headerList = [
    'epcode' => [
        'data_attr' => true,
        'disp_name' => '選択',
        'width'     => 75,
    ],
    'empid' => [
        'data_attr' => true,
        'disp_name' => '',
    ],
    'empname' => [
        'disp_name' => 'ユーザー名',
        'sortable'  => true
    ],
    'empmail' => [
        'disp_name' => 'メールアドレス',
        'sortable'  => true
    ],
    'deptname' => [
        'disp_name' => 'グループ名',
        'sortable'  => true
    ],
];

$smarty->assign('tablelist', $tableList);
$smarty->assign('headerList', $headerList);
$smarty->assign('actionList', $actionList);
$smarty->assign('frmValues', $frmValues);
$smarty->assign('hideDept', $hideDept ? 'hide' : '');

$smarty->assign('pageTitle', 'プロジェクト・ユーザー管理');
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);

$smarty->assign('viewTemplate', 'mst/userpj_change.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
