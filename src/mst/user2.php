<?php
use Akatsuki\Models\Emp;
use Common\Mailer;

require_once(COMMON_PATH . '/auth.php');

$allErrorCheck = function (
    $smarty,
    $uid,
    $name,
    $email,
    $dept,
    $sysadmin,
    $admin,
    &$msg,
    $cmd,
    &$newDeptName
) {
    $cid = (int)$_SESSION["cid"];
    if ($name == "" || $email == "" || $dept == "") {
        // 空の場合はエラー
        $msg = "氏名、メールアドレス、所属グループは省略できません。";

        return (1);
    } else {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $msg = MESSAGES['ERR_INVALID_EMAIL_ADDRESS'];

            return (1);
        }
    }

    if ($cmd == "update" && ($uid == "" || is_numeric($uid) == false)) {
        // IDが空の場合はシステムエラー
        errorScreen($smarty, "システムエラーが発生しました。POSTデータが不正です。(ID)");
        exit;
    }
    // メールアドレスの重複チェック
    $sql = sprintf("
            SELECT email
            FROM emp
            WHERE cid = " . $cid . " and status = 0 and email = '" . pg_escape_string($email) . "'");
    $logs[] = $sql;
    if ($cmd == "update") {
        $sql .= " and empid != " . $uid;
    }
    $r = pg_query($smarty->_db, $sql);
    // レコードが1件でもあればエラー
    if (pg_num_rows($r) > 0) {
        $msg = "すでに同じメールアドレスが登録されています。";

        return (1);
    }

    // 変更の場合、システム管理者が最低一名存在するかチェック
    if ($cmd == "update" && $sysadmin != "on") {
        $sql = sprintf("
            SELECT email
            FROM emp
            WHERE cid = " . $cid . " and status = 0 and sysadmin = 1 and empid != " . $uid);
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);

        if (pg_num_rows($r) <= 0) {
            $msg = "システム管理者が最低1名は必要なため、削除できません。";

            return (1);
        }
    }

    // deptが存在するかのチェック
    $sql = sprintf("SELECT deptname FROM dept WHERE cid = " . $cid . " and dept = " . $dept . " and status = 0");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    if (pg_num_rows($r) <= 0) {
        errorScreen($smarty, "システムエラーが発生しました。POSTデータが不正で>す。(DEPT)");
        exit;
    }
    // 監査ログ用に所属ブループ名を取得
    $newDeptName = pg_fetch_result($r, 0, 0);

    raise_sql($logs, 'user2');

    return (0);
};

$getID = function ($smarty, &$uid, &$dispNum) {
    $cid = (int)$_SESSION["cid"];
    // IDの決定。削除ずみのものを含めてmaxを取得
    $sql = sprintf("SELECT max(empid) FROM emp WHERE cid = " . $cid);
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $row = pg_fetch_row($r, 0);
    // レコードが1件もなければ IDに1を設定。
    if ($row[0] == "") {
        $uid = 1;
    } else {
        $uid = $row[0] + 1;
    }

    // 表示順の決定。削除済みのものを除いてmaxを取得
    $sql = sprintf("SELECT max(disporder) FROM emp WHERE cid = " . $cid . " and status = 0");
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $row = pg_fetch_row($r, 0);
    if ($row[0] == "") {
        $dispNum = 1;
    } else {
        $dispNum = $row[0] + 1;
    }
    raise_sql($logs, 'user2');
};

// 初期化
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$name = $email = $dept = $sysadmin = $admin;
$cid = (int)$_SESSION["cid"];
$button = postreq("button");
$uid = (int)getreq("id");
$uid < 0 && $uid = 0;
$cmd = $uid === 0 ? "new" : "update";
$requestMethod = $_SERVER['REQUEST_METHOD'];
$deptRoles = permission_check("mst/dept.php");
$pageRoles = permission_check("mst/user2.php");

$toggleLockPermission = 0;
$configs = \Akatsuki\Models\Configs::select('check_sys_admin', 'check_manager')
    ->where('cid', $_SESSION['cid'])->first();
if ($configs) {
    if ($_SESSION['sysadmin'] && (int)$configs->check_sys_admin ||
        $_SESSION['admin'] && (int)$configs->check_manager) {
        $toggleLockPermission = true;
    }
}

$sql = 'SELECT
          pass_lenght,
          pass_expired,
          pass_lowercase,
          pass_number
        FROM configs';
$r = pg_query($smarty->_db, $sql);
$row = pg_fetch_array($r, null, PGSQL_ASSOC);

// ボタンが押されたときの処理
if ($requestMethod === "POST") {
    $name = htmlspecialchars(postreq("name"));
    $email = htmlspecialchars(postreq("email"));
    $dept = postreq("group");
    $sysadmin = postreq("sysadmin");
    $admin = postreq("admin");
    $pass = postreq("pass");
    $pass2 = postreq("pass2");
    $lock = postreq("lock");
    $empid = 0;
    // エラーチェック
    $err = $allErrorCheck(
        $smarty,
        $uid,
        $name,
        $email,
        $dept,
        $sysadmin,
        $admin,
        $msg,
        $cmd,
        $newDeptName
    );
    if ($err == 0) {
        // トランザクションの開始

        if ($uid === 0) {
            $getID($smarty, $empid, $dispNum);
        }

        // PostgreSQL用に文字列をエスケープ
        $name = pg_escape_string($name);
        $email = pg_escape_string($email);
        $dept = pg_escape_string($dept); // dept id
        $isSysAdmin = $sysadmin == "on" ? 1 : 0;
        $isAdmin = $admin == "on" ? 1 : 0;
        $updateMessage = "";
        if ($uid === 0) {
            $r = false;
            $emp = null;
            if (empty($pass)) {
                $msg .= "Invalid password";
            } else {
                $emp = Emp::where('email', $email)
                ->where('status', 0)
                ->first();
            }
            if ($emp) {
                $msg .= "すでに同じメールアドレスが登録されています。";
            } elseif (empty($pass)) {
                $msg .= MESSAGES['ERR_PASSWORD_POLICY_EMPTY'];
            } elseif ($pass != $pass2) {
                $msg = MESSAGES['ERR_PASSWORD_MISMATCHED'];
            } else {
                $validations = validate_password_policy($pass);
                if ($validations['err']) {
                    $msg .= implode("<br />", $validations['msgs']);
                } else {
                    $r = true;
                }
            }
            if ($msg == "") {
                $emp = new Emp();
                $randomStr = md5(rand());
                $emp->setValues([
                    'cid'           => $cid,
                    'empid'         => $empid,
                    'email'         => $email,
                    'pass'          => md5($pass),
                    'dept'          => $dept,
                    'name'          => $name,
                    'memo1'         => null,
                    'memo2'         => null,
                    'regdate'       => 'now()',
                    'lastlogin'     => null,
                    'status'        => 0,
                    'regkey'        => $randomStr,
                    'sysadmin'      => $isSysAdmin,
                    'admin'         => $isAdmin,
                    'disporder'     => $dispNum,
                    'email_lock'    => (int)$lock,
                    'login_initial' => 0,
                ]);

                $r = $emp->save();
                if ($r) {
                    $uid = $emp->empid;
                }
            }
        } else {
            $emp = Emp::where("cid", $cid)
                        ->where("empid", $uid)
                        ->where("status", 0)
                        ->first();
            if ($emp) {
                $r = true;
                if ($emp->email != $email) {
                    $tmpEmp = Emp::where('email', $email)
                        ->where('status', 0)->first();
                    if ($tmpEmp) {
                        $r = false;
                        $msg = "すでに同じメールアドレスが登録されています。";
                    }
                }

                // ユーザーのロック/アンロックを編集するときにメールを送信します。
                if ($emp->email_lock != (int)$lock) {
                    $token = $emp->generateResetToken();

                    $mailer = new Mailer();

                    if ((int)$lock == 0) {
                        $mailSubject = "[SunnyView] パスワードの再設定";
                        $mailBody = "パスワードを再設定するには、下記のリンクをクリックして画面に表示される手順を行ってください。\n\n
                                http://52.68.30.203/reg/change_pass.php?token=" . $token;
                    } else {
                        $mailSubject = "[SunnyView] Account Locked";
                        $mailBody = "Your account has been locked.";
                    }

                    $mailer
                        ->set('subject', $mailSubject)
                        ->set('to', $email)
                        ->set('body', $mailBody);
                    // 「管理者」または「マネージャ」のBCCメール
                    $mailer->setBcc($smarty->_db, $email);

                    $mailer->send();
                }

                if ($r) {
                    $emp->setValues([
                        'email'         => $email,
                        'dept'          => $dept,
                        'name'          => $name,
                        'sysadmin'      => $isSysAdmin,
                        'admin'         => $isAdmin,
                        'email_lock'    => (int)$lock,
                    ]);
                    $r = $emp->save();
                }
            } else {
                $r = false;
            }
        }
        // トランザクションの終了
        if (!$r) {
            $err = 1;
            !$msg && $msg = MESSAGES['ERR_UPDATE'];
        } else {
            if ($cmd == "new") {
                // メッセージの設定
                $msg = "新しいユーザーを登録しました。";
            } else {
                $msg = "ユーザー情報を変更しました。";
                if ($_SESSION['empid'] == $uid) {
                    $_SESSION['sysadmin'] = $isSysAdmin;
                    $_SESSION['admin'] = $isAdmin;
                    $_SESSION['roles'] = [];
                    $_SESSION['dept'] = $dept;
                }
            }
        }
    }
    $sysadmin = $sysadmin == "on" ? "checked" : "";
    $admin = $admin == "on" ? "checked" : "";
} elseif ($uid > 0) {
    // 初期GETアクセスの場合
    $sql = sprintf("SELECT
                        name,
                        email,
                        dept,
                        sysadmin,
                        admin,
                        idsadmin,
                        email_lock,
                        empid
                    FROM emp
                    WHERE
                        cid = $1
                        AND empid = $2
                        AND status = 0");
    $logs[] = $sql;
    $r = pg_query_params($smarty->_db, $sql, [
        $cid,
        $uid
    ]);
    if (pg_num_rows($r) != 1) {
        errorScreen($smarty, "GETデータが不正です。(IDNOTF)");
        exit;
    }
    // $row = pg_fetch_row($r, 0);
    $row = pg_fetch_assoc($r, 0);

    $editable = true;
    if ($row['empid'] != $_SESSION['empid']) {
        if ($_SESSION['sysadmin'] == 1) {
            if ($row['sysadmin'] == 1) {
                $editable = false;
            }
        } elseif ($_SESSION['admin'] == 1) {
            if ($row['admin'] == 1) {
                $editable = false;
            }
        }
    }
    if (!$editable) {
        rd("mst/user.php");
    }

    $name = $row['name'];
    $email = $row['email'];
    $dept = $row['dept'];
    $sysadmin = $row['sysadmin'] == 1 ? "checked" : "";
    $admin = $row['admin'] == 1 ? "checked" : "";
    $lock = $row['email_lock'];

    $smarty->assign('uid', getreq("id"));
}

$smarty->assign('name', $name);
$smarty->assign('email', $email);
$smarty->assign('dept', $dept);
$smarty->assign('pass', $pass);
$smarty->assign('pass2', $pass2);
$smarty->assign('sysadmin', $sysadmin);
$smarty->assign('admin', $admin);
$smarty->assign('lock', $lock);
$smarty->assign('toggleLockPermission', +$toggleLockPermission);
$smarty->assign('isSystemAdmin', +$_SESSION['sysadmin']);
if ($uid === 0) {
    $smarty->assign('pageTitle', 'ユーザーの新規登録');
    $smarty->assign('submitButton', '新規登録');
} else {
    $smarty->assign('pageTitle', 'ユーザー情報の変更');
    $smarty->assign('submitButton', '保存');
}
raise_sql($logs, 'user2');
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('frmAction', htmlspecialchars($cmd));
$smarty->assign('uid', htmlspecialchars($uid));
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'mst/user2.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
