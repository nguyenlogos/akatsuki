<?php
use Akatsuki\Models\EmpProj;

$deptRoles = permission_check('mst/dept.php');
$pageRoles = permission_check('mst/userpj.php');

$hideDept = false;

$frmValues = [
    'dept'  => getreq('dept'),
    'pname' => getreq('pname'),
];

if (!$deptRoles['read']['allowed']) {
    $frmValues['dept'] = $_SESSION['dept'];
    $hideDept = true;
}

// ソートキーボタンの設定
$sortMap = [
    "p.pcode", "p.pcode DESC",
    "p.pname", "p.pname DESC",
    "d.deptname", "d.deptname DESC",
    "empnum", "empnum DESC",
];

// アクティブなソートキーボタンを設定
// 併せてSQLのソートキーを設定
$sortIndex = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

$whereConditions = [
    "p.cid = {$_SESSION['cid']}",
    "p.status = 0",
];
if (!empty($frmValues['dept'])) {
    $whereConditions[] = "p.dept = " . pg_escape_string($frmValues['dept']);
}
if (!empty($frmValues['pname'])) {
    $fmPname = strtoupper(pg_escape_string($frmValues['pname']));
    $whereConditions[] = "(upper(p.pname) LIKE '%{$fmPname}%' OR upper(p.pcode) LIKE '%{$fmPname}%')";
}
$whereConditions = implode(" AND ", $whereConditions);

$sql = "
    SELECT
        p.pcode,
        p.pname,
        d.deptname,
        COALESCE(tmp.empnum, 0) AS empnum
    FROM
        proj p
    LEFT JOIN
    (
        SELECT
            ep.cid,
            ep.pcode,
            COUNT(ep.id) AS empnum
        FROM
            emp_proj ep
        WHERE
            ep.cid = {$_SESSION['cid']}
        GROUP BY
            ep.cid, ep.pcode
    ) tmp
        ON tmp.cid = p.cid
        AND tmp.pcode = p.pcode
    LEFT JOIN
        dept d
        ON d.cid = p.cid
        AND d.dept = p.dept
        AND d.status = 0
    WHERE
        {$whereConditions}
    ORDER BY
        {$sortColumn}
";
$result = pg_query($smarty->_db, $sql);
$tableList = pg_fetch_all($result) ?: [];

$headerList = [
    'pcode' => [
        'data_attr' => true,
        'disp_name' => 'プロジェクトID',
        'sortable'  => true,
        'width'     => 140
    ],
    'pname' => [
        'disp_name' => 'プロジェクト名',
        'sortable'  => true
    ],
    'deptname' => [
        'disp_name' => 'グループ名',
        'sortable'  => true,
        'width'     => 250
    ],
    'empnum' => [
        'disp_name' => 'ユーザー数',
        'sortable'  => true,
        'width'     => 110
    ],
];

$btnTemplate = '<button type="button" class="btn btn-change $btnClass">$btnName</button>';

$actionList = [
    [
        'template'  => $btnTemplate,
        '$btnName'  => '変更',
        '$btnClass' => 'btn-blue btn-sm',
        'width'     => 70
    ],
];

$smarty->assign('tablelist', $tableList);
$smarty->assign('headerList', $headerList);
$smarty->assign('actionList', $actionList);
$smarty->assign('frmValues', $frmValues);
$smarty->assign('hideDept', $hideDept ? 'hide' : '');

$smarty->assign('pageTitle', 'プロジェクト・ユーザー管理');
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);

$smarty->assign('viewTemplate', 'mst/userpj.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
