<?php
require_onceCOMMON_PATH.'/auth.php';

use Akatsuki\Models\Proj;
use Akatsuki\Models\BaseModel;
use Akatsuki\Models\Instance;

// 初期化
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$cid = (int)$_SESSION["cid"];
$pcode = $oldpcode = $pname = $dept = $memo = $uid = $projID = "";
$button = postreq("button");

// ボタンが押されたときの処理
if ($button == "新規登録" || $button == "保存") {
    if ($button == "新規登録") {
        $cmd = "new";
    } else {
        $cmd = "update";
    }

    $pcode = htmlspecialchars(postreq("pcode"));
    $oldpcode = htmlspecialchars(postreq("oldpcode"));
    $pname = htmlspecialchars(postreq("pname"));
    $dept = (int)postreq("dept");
    $memo = htmlspecialchars(postreq("memo"));

    // エラーチェック
    if ($pcode == "" || $pname == "" || $dept == "") {
        // 空の場合はエラー
        $msg = "プロジェクトID、プロジェクト名、所属グループは省略できません。";
        $err = 1;
    } elseif ($oldpcode == "" && $cmd == "update") {
        // 更新時に古いpcodeが無かった場合
        errorScreen($smarty, "データが不正です。(OLDPCODE_NF)");
        exit;
    }
    //Check max length
    if (mb_strlen($pcode) > 128) {
        $msg = "128 文字以内で入力してください。";
        $err = 1;
    }
    if (mb_strlen($pname) > 256) {
        $msg = "256 文字以内で入力してください。";
        $err = 1;
    }
    // 登録・更新処理の開始
    if ($err == 0) {
        // トランザクションの開始
        // $r = pg_query($smarty->_db, "BEGIN");
        if ($cmd == "new") {
            // 表示順の決定。削除済みのものを除いてmaxを取得
            $sql = sprintf("SELECT max(disporder) FROM proj WHERE cid = " . $cid . " and status = 0");
            $logs[] = $sql;
            $r = pg_query($smarty->_db, $sql);
            $row = pg_fetch_row($r, 0);
            if ($row[0] == "") {
                $dispNum = 1;
            } else {
                $dispNum = $row[0] + 1;
            }
        }

        // PostgreSQL用に文字列をエスケープ
        $pcode = pg_escape_string($pcode);
        $pname = pg_escape_string($pname);
        $memo = pg_escape_string($memo);
        $updateMessage = "";
        // deptが存在するかのチェック
        $sql = sprintf("SELECT deptname FROM dept WHERE cid = $1 and dept = $2 and status = 0");
        $logs[] = $sql;
        $r = pg_query_params(
            $smarty->_db,
            $sql,
            [
            $cid,
            pg_escape_string($dept)
            ]
        );
        if (pg_num_rows($r) <= 0) {
            errorScreen($smarty, "システムエラーが発生しました。POSTデータが不正です。(DEPT)");
            exit;
        }
        $oldDeptName = pg_fetch_result($r, 0, 0);
        // エラーチェック終了。登録・保存処理
        if ($cmd == "new") {
            $proj = Proj::where('pcode', $pcode)
                ->where('cid', $_SESSION['cid'])
                ->where('status', 0)
                ->first();
            if ($proj) {
                $r = false;
                $msg = "指定されたプロジェクトコードは、すでに使われています。";
            } else {
                $proj = new Proj();
                $proj->setValues([
                    'cid'       => $cid,
                    'pcode'     => $pcode,
                    'pname'     => $pname,
                    'regdate'   => 'now()',
                    'status'    => 0,
                    'disporder' => $dispNum,
                    'memo'      => $memo,
                    'dept'      => $dept,
                ]);
                $r = $proj->save();
            }
        } else {
            $projID = (int)getreq("id");
            $proj = Proj::where('id', $projID)
                        ->where('status', 0)
                        ->first();
            if ($proj) {
                $r = true;
                if (htmlspecialchars($proj->pcode) != $pcode) {
                    $tmpProj = Proj::where('pcode', $pcode)
                        ->where('cid', $_SESSION['cid'])
                        ->where('status', 0)
                        ->first();
                    if ($tmpProj) {
                        $r = false;
                        $msg = "指定されたプロジェクトコードは、すでに使われています。";
                    }
                }
                if ($r) {
                    $r = Proj::beginTransaction(function () use ($proj, $pcode, $pname, $memo, $dept) {
                        $oldpcode = $proj->pcode;
                        $proj->setValues([
                            'pcode' => $pcode,
                            'pname' => $pname,
                            'memo'  => $memo,
                            'dept'  => $dept
                        ]);
                        $r = $proj->save();
                        if ($r) {
                            $affectedTables = [
                                'emp_proj',
                                'inst_req',
                                'inst_req_change',
                                'instance',
                                'stat_cost_daily',
                                'stat_ebs',
                                'stat_inst',
                                'stat_s3',
                                'tbl_lambda',
                            ];
                            foreach ($affectedTables as $table) {
                                $sql = sprintf("
                                    UPDATE %s SET pcode = '%s' WHERE pcode = '%s' AND cid = %s
                                ", $table, $pcode, $oldpcode, $_SESSION['cid']);
                                BaseModel::selectRaw($sql);
                            }
                            return true;
                        }
                        return $r;
                    });
                }
            } else {
                $r = false;
            }

            // 紐づけられたインスタンスのpcodeも更新
            if ($r && $pcode != $oldpcode) {
                $sql = "
                    UPDATE instance
                    SET pcode = '" . pg_escape_string($pcode) . "' WHERE cid = " . $cid . "AND pcode = '" .
                    pg_escape_string($oldpcode) . "'";
                $logs[] = $sql;
                $r = pg_query($smarty->_db, $sql);
            }
        }

        if (!$r) {
            $err = 1;
            !$msg && $msg = $cmd === "new" ? MESSAGES['ERR_INSERT'] : MESSAGES['ERR_UPDATE'];
        } elseif ($_POST["button"] == "新規登録") {
            $msg = "新しいプロジェクトを登録しました。";
        } else {
            $msg = "プロジェクト情報を変更しました。";
        }
    }
} else {
    // 初期GETアクセスの場合

    // 処理内    容の確認
    $cmd = getreq("cmd");
    if ($cmd == "update") {
        $projID = (int)getreq("id");
        if (!($projID > 0)) {
            errorScreen($smarty, "GETデータが不正です。(NOPID)");
            exit;
        }
        $sql = sprintf("SELECT pcode, pname, dept, memo FROM proj WHERE cid = $1 AND id = $2 AND status = 0");
        $logs[] = $sql;
        $r = pg_query_params($smarty->_db, $sql, [
            $cid,
            $projID
        ]);

        if (pg_num_rows($r) != 1) {
            rd('mst/proj.php');
            exit;
        }

        $row = pg_fetch_row($r, 0);
        $pageRoles = permission_check("mst/user2.php", true);
        if ($pageRoles['read']['condition'] == 1 && $_SESSION['dept'] != $row[2]) {
            rd('mst/proj.php');
        }

        $pcode = $row[0];
        $pname = $row[1];
        $dept = $row[2];
        $memo = $row[3];
    }
}
raise_sql($logs, 'proj2');

$smarty->assign('pcode', $pcode);
$smarty->assign('projID', (int)getreq('id'));
$smarty->assign('oldpcode', $oldpcode);
$smarty->assign('pname', $pname);
$smarty->assign('memo', $memo);
$smarty->assign('dept', $dept);

if ($cmd == "new") {
    $smarty->assign('pageTitle', 'プロジェクトの新規登録');
    $smarty->assign('submitButton', '新規登録');
} else {
    $smarty->assign('pageTitle', 'プロジェクト情報の変更');
    $smarty->assign('submitButton', '保存');
}

$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('uid', $uid);
$smarty->assign('cmd', $cmd);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'mst/proj2.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
