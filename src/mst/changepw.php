<?php
use Akatsuki\Models\Emp;

$msg = "";
$err = 0;
$p1 = "";
$p2 = "";
$p3 = "";
$policy_pass = "";
$button = postreq("button");

$sql = 'SELECT
          pass_lenght,
          pass_expired,
          pass_lowercase,
          pass_number
        FROM configs';
$r = pg_query($smarty->_db, $sql);
$row = pg_fetch_array($r, null, PGSQL_ASSOC);

if ($button == "変更") {
    $p1 = postreq("p1");
    $p2 = postreq("p2");
    $p3 = postreq("p3");


    $emp = Emp::where('cid', $_SESSION["cid"])
                ->where('email', $_SESSION["uid"])
                ->where('pass', md5($p1))
                ->where('status', 0)
                ->first();
    $pass_old = $emp->pass;
    $pass_voice = md5($p2);
    if (!$emp) {
        $msg = "現在のパスワードが正しくありません。";
        $err = 1;
        $p1 = "";
    } else {
        if ($pass_voice == $pass_old) {
            $policy_pass .= MESSAGES['ERR_PASSWORD_POLICY_DIFF'];
            $err = 1;
        } elseif ($p2 != $p3) {
            $msg = MESSAGES['ERR_PASSWORD_MISMATCHED'];
            $err = 1;
        } else {
            $validations = validate_password_policy($p2);
            if ($validations['err']) {
                $err = 1;
                $policy_pass .= implode("<br />", $validations['msgs']);
            }
        }
        if ($policy_pass == "" && $err == 0) {
            $r = $emp->changePassword(md5($p2));
            if (!$r) {
                $msg = MESSAGES['ERR_UPDATE'];
                $err = 1;
            } else {
                // $msg = "パスワードを変更しました。ログインしなおしてください。\n";
                $msg = '
                    パスワードを変更しました。ログインしなおしてください <br/>
                    <br>
                    <a href="/">ログイン画面</a>
                ';
                $err = 0;
                logout();
            }
        }
    }
}

$smarty->assign('p1', $p1);
$smarty->assign('p2', $p2);
$smarty->assign('p3', $p3);
$smarty->assign('policy_pass', $policy_pass);
$smarty->assign('pageIcon', 'glyphicon-lock');
$smarty->assign('pageTitle', 'パスワードの変更');
$smarty->assign('err', $err);
$smarty->assign('msg', $msg);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('viewTemplate', 'mst/changepw.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
