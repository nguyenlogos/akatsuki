<?php
use Common\Mailer;
use Akatsuki\Models\Emp;

// 初期化
$UnActive = '';
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$cid = (int)$_SESSION["cid"];
$user_name = htmlspecialchars(getreq('user_name')); // key ユーザ名
$show_dept = htmlspecialchars(getreq('showdept')); // key select box
$showdept = htmlspecialchars(getreq("showdept"));
$roles = permission_check("mst/user.php", true);

$toggleLockPermission = 0;
$configs = \Akatsuki\Models\Configs::select('check_sys_admin', 'check_manager')
    ->where('cid', $_SESSION['cid'])->first();
if ($configs) {
    if ($_SESSION['sysadmin'] && (int)$configs->check_sys_admin ||
        $_SESSION['admin'] && (int)$configs->check_manager) {
        $toggleLockPermission = true;
        // $sql = sprintf("SELECT lock_failed FROM configs WHERE cid = $1");
        // $r = pg_query_params(
        //     $smarty->_db,
        //     $sql,
        //     [
        //         $cid,
        //     ]
        // );
        // $configRow = pg_fetch_row($r);
        // if ($configRow[0] > 0) {
        //     $toggleLockPermission = true;
        // } else {
        //     $toggleLockPermission = 0;
        // }
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && getreq('lock') == '1') {
    $CheckId = postreq('check_id');
    $CheckLockEmail = postreq('check_val');
    $CheckEmail = postreq('check_email');

    $emp = Emp::where("id", $CheckId)
                ->where("cid", $cid)
                ->where("status", 0)
                ->first();
    $token = $emp->generateResetToken();

    $mailer = new Mailer();

    if ($CheckLockEmail == 0) {
        $mailSubject = "[SunnyView] パスワードの再設定";
        $mailBody = "パスワードを再設定するには、下記のリンクをクリックして画面に表示される手順を行ってください。\n\n
                                http://52.68.30.203/reg/reset.php?k=" . $token;
    } else {
        $mailSubject = "[SunnyView] Account Locked";
        $mailBody = "Your account has been locked.";
    }

    $mailer
        ->set('subject', $mailSubject)
        ->set('to', $CheckEmail)
        ->set('body', $mailBody);
    // 「管理者」または「マネージャ」のBCCメール
    $mailer->setBcc($smarty->_db, $CheckEmail);

    $mailer->send();

    if ($emp) {
        $result = true;
        if ($result) {
            $emp->setValues([
                'number_lock'     => 0,
                'email_lock'      => (int)$CheckLockEmail,
                'pw_reset_token'  => $token,
            ]);
            $result = $emp->save();
        }
    } else {
        $result = false;
    }

    echo json_encode([
        'result' => $result
    ]);

    exit;
}

if ($showdept == "") {
    $sql = sprintf("SELECT dept FROM dept WHERE status = 0 ORDER BY disporder");
    $logs[] = $sql;
    $rss = pg_query($smarty->_db, $sql);
    for ($i = 0; $i < pg_num_rows($rss); $i++) {
        $row = pg_fetch_row($rss, $i);
        $showdept .= $row[0] . ',';
    }
}

$button = postreq("button");

// ボタンが押されたときの処理
if ($button == "削除") {
    // 削除処理
    if (postreq("uid2") == "") {
        // IDが空の場合はシステムエラー
        errorScreen($smarty, "システムエラーが発生しました。POSTデータが不正です。");
        exit;
    }
    // PostgreSQL用にIDをエスケープ
    $userID = (int)postreq("uid2");
    // システム管理者が最低1名は必要
    $sql = sprintf("SELECT empid
                    FROM emp
                    WHERE cid = $1 AND empid <> $2 AND status = 0 AND sysadmin = 1");
    $r = pg_query_params($smarty->_db, $sql, [
        $cid,
        $userID
    ]);
    $logs[] = $sql;
    if (pg_num_rows($r) <= 0) {
        $msg = "システム管理者が最低1名は必要なため、削除できません。";
        $err = 1;
    } else {
        if ($userID == $_SESSION['empid']) {
            $msg = "User cannot be deleted";
            $err = 1;
        } else {
            if (!$_SESSION['sysadmin'] && !$_SESSION['admin']) {
                $msg = MESSAGES['ERR_PERMISSION_DENIED'];
                $err = 1;
            } else {
                // 更新するuserのdisporderを取得
                $sql = sprintf("SELECT disporder,email,name
                                FROM emp
                                WHERE cid = $1 AND empid = $2 and status = 0");
                $r = pg_query_params($smarty->_db, $sql, [
                    $cid,
                    $userID
                ]);
                $logs[] = $sql;
                $row = pg_fetch_row($r, 0);
                $disporder = $row[0];
                $email = htmlspecialchars($row[1]);
                $name = htmlspecialchars($row[2]);

                // statusの更新
                $emp = Emp::where("cid", $cid)
                            ->where("empid", $userID)
                            ->where("status", 0)
                            ->first();

                if ($emp) {
                    $emp->status = -1;
                    $r = $emp->save();
                } else {
                    $r = false;
                }
                // 表示順の洗い替え
                // 削除した表示順以降のレコードのみ
                $sql = "
                    UPDATE emp set disporder = disporder - 1
                    WHERE cid = " . $cid . " AND disporder > " . $disporder . " AND status = 0
                ";
                $logs[] = $sql;
                $r && $r = pg_query($smarty->_db, $sql);
                if ($r) {
                    $msg = "削除しました。";
                    $err = 0;
                    insertLog(
                        $smarty,
                        "ユーザ情報「" . pg_escape_string($name) . "(ID:" . pg_escape_string($email) . ")」を削除しました。"
                    );
                } else {
                    $msg = MESSAGES['ERR_DELETE'];
                    $err = 1;
                }
            }
        }
    }
}
// ソートキーボタンの設定
$sktable = array(
    "emp.disporder",
    "emp.disporder DESC",
    "emp.name",
    "emp.name DESC",
    "dept.dept",
    "dept.dept DESC",
    "emp.lastlogin",
    "emp.lastlogin DESC",
);
// determine sort column
$sortIndex = (int)getreq("sk");
$skStr = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
if (array_key_exists($sortIndex, $sktable)) {
    $skStr = $sktable[$sortIndex];
} else {
    $skStr = $sktable[0];
}

$conditions = [
    "emp.cid = {$cid}",
    "emp.status = 0"
];

if (!empty($user_name)) {
    $fmUname = pg_escape_string($user_name);
    $conditions[] = sprintf("upper(emp.name) LIKE '%%%s%%'", strtoupper($fmUname));
}

if ($roles['read']['condition'] == 1) {
    $conditions[] = sprintf("emp.dept = %d", $_SESSION['dept']);
} elseif (!empty($show_dept)) {
    $conditions[] = sprintf("emp.dept = '%s'", (int)$show_dept);
}
$conditions = "WHERE " . implode(' AND ', $conditions);
$orderConditions = '';
$limitConditions = '';
$paginationStr = '';

// データテーブルの生成
$p = (int)getreq("p");
if ($p <= 0) {
    $p = 1;
}
$sqlTemplate = "
SELECT
    %s
FROM
    emp
INNER JOIN
    dept
    ON dept.cid = emp.cid
    AND dept.dept = emp.dept
    AND dept.status = 0
";
$sql = sprintf($sqlTemplate, 'count(emp.cid) AS num_row');
$sql .= $conditions;
//count マネージャーが存在していないグループがあります。
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);
if ($dataCount > 0) {
    $paginationStr = getPagenationStr(
        $smarty,
        $dataCount,
        DEFAULT_COUNTPAGE_EMP,
        $p,
        "./user.php?&showdept={$show_dept}&user_name={$user_name}&sk={$sortIndex}&p="
    );
}
$tmpTable = "
    SELECT
        d.dept
    FROM
        emp e
        INNER JOIN
            dept d
            ON d.dept = e.dept
            AND d.cid = e.cid
    WHERE
        d.cid = {$_SESSION['cid']}
        AND d.status = 0
        AND e.admin = 1
        AND e.status = 0
    GROUP BY
        d.dept
";
$columns = [
    "emp.empid",
    "emp.name",
    "dept.deptname",
    "to_char(emp.lastlogin, 'YYYY/MM/DD HH24:MI:SS') as lastlogin",
    "emp.disporder",
    "emp.sysadmin",
    "emp.admin",
    "tmp.dept AS has_mgr",
    "emp.number_lock",
    "emp.email_lock",
    "emp.id",
    "emp.email"
];
$orderConditions = " ORDER BY {$skStr}";

$pOffset = ($p - 1) * ITEMS_PER_PAGE;
$limitConditions = sprintf(" LIMIT %d OFFSET %d", ITEMS_PER_PAGE, $pOffset);
$sqlTemplate = "
    WITH tmp AS ($tmpTable)
        $sqlTemplate
    LEFT JOIN tmp
    ON tmp.dept = emp.dept
";
$sql = sprintf($sqlTemplate, implode(',', $columns));
$sql .= $conditions;
$sql .= $orderConditions;
$sql .= $limitConditions;
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);

$userList = "";
$k = 1;
$managerCount = 0;
for ($i = 0; $i < pg_num_rows($r); $i++) {
    // $row = pg_fetch_row($r, $i);
    $row = pg_fetch_assoc($r, $i);
    $dd[] = $row['empid'];
    if ($sortIndex == 1) {
        $id_row  = $row['empid'];
        $row_sub = $dataCount--;
    } elseif ($sortIndex) {
        $row_sub = $k++;
    } elseif ($show_dept) {
        $id_row  = $row['disporder'];
        $row_sub = $k++;
    } else {
        $id_row  = $row['empid'];
        $row_sub = $k++;
    }
    // 表示順
    $userList .= "<tr id=" . $id_row . " data-class=" . $row['disporder'] . "><td>" . $row_sub . "</td>";
    // ユーザ名
    $uname = htmlspecialchars($row['name']);
    if ($_SESSION['empid'] == $row['empid']) {
        $uname .= " <sup>（*）</sup>";
    }
    $userList .= "<td>" . $uname . "</td>";
    $userList .= "<td>" . htmlspecialchars($row['deptname']);

    if (empty($row['has_mgr'])) {
        $managerCount++;
        $userList .= "
            <a href='#'
                data-toggle='tooltip'
                data-placement='top'
                title='マネージャーが存在していないグループがあります。!'><i class='fas fa-exclamation-circle'></i>
            </a>
        ";
    }
    $userList .= "</td>";
    // SYSAD ADMIN
    $userList .= "<td>";
    if ($row['sysadmin'] == 1) {
        $userList .= "<span class='badge badge-pill badge-orange mb-2'>Admin</span> ";
    }
    if ($row['admin'] == 1) {
        $userList .= "<span class='badge badge-pill badge-blue'>Mgr.</span> ";
    }
    $userList .= "</td>";

    // 最終ログイン日時
    $check_lock = '';
    if ($row['email_lock']  == 1) {
        $check_lock = 'checked';
    }

    $btnPermissions = true;
    if ($row['empid'] != $_SESSION['empid']) {
        if ($_SESSION['sysadmin'] == 1) {
            if ($row['sysadmin'] == 1) {
                $btnPermissions = false;
            }
        } elseif ($_SESSION['admin'] == 1) {
            if ($row['admin'] == 1) {
                $btnPermissions = false;
            }
        }
    }
    // 変更削除ボタン
    $disabled = $row['empid'] == $_SESSION['empid'] ? "disabled" : "";

    if ($toggleLockPermission) {
        $userList .= "<td class='text-center'><code>" . $row['number_lock'] . "</code></td>";
        if (($btnPermissions && empty($disabled))
            || $_SESSION['sysadmin'] == 1
            || ($_SESSION['admin'] == 1 && $configs->check_manager == 1)
        ) {
            $userList .= "
                <td>
                    <div class='toggle'>
                        <input type='checkbox' onclick='changeValueCheckbox(this)'
                            class='check_lock' id='check" . $row['empid'] . "'
                            data-lockid='" . $row['id'] . "'
                            data-email='" . $row['email'] . "'
                            value='" . $row['email_lock'] . "' " . $check_lock . ">
                        <label for='check" . $row['empid'] . "'></label>
                    </div>
                </td>
            ";
        } else {
            $userList .= "<td></td>";
        }
    }
    $userList .= "<td>" . $row['lastlogin'] . "</td>";

    if ($btnPermissions) {
        $userList .= "
            <td class='text-center'>
                <a class='btn btn-blue btn-sm mb-1' href='./user2.php?id=" . $row['empid'] . "' role='button'>変更</a>
                <button type='button'
                    class='btn btn-danger btn-sm mb-1'
                    data-toggle='modal'
                    data-target='#deleteData'
                    data-dataid='" . $row['empid'] . "'
                    data-name='" . $row['name'] . "' {$disabled}>削除
                </button>
            </td>";
    } else {
        $userList .= "<td></td>";
    }
    $userList .= "</tr>\n";
}

//drap and drop user
$event = (isset($_POST["page_id_array"]) ? $_POST["page_id_array"] : null);
$event_class = (isset($_POST["page_class_array"]) ? $_POST["page_class_array"] : null);
if ($show_dept) {
    $m = 1;
    for ($i = 0; $i < count($event_class); $i++) {
        $query = "
     UPDATE emp
     SET disporder = '" . $_POST["page_class_array"][$m++] . "'
     WHERE cid = " . $cid . " AND empid = '" . $dd[$i] . "' AND status = 0";
        pg_query($smarty->_db, $query);
    }
} else {
    for ($i = 0; $i < count($event); $i++) {
        $query = "
     UPDATE emp
     SET disporder = '" . $i . "'
     WHERE cid = " . $cid . " AND empid = '" . $_POST["page_id_array"][$i] . "' AND status = 0";
        pg_query($smarty->_db, $query);
    }
}

raise_sql($logs, 'user');
$hideDept = $roles['read']['condition'] == 1;
$smarty->assign('showdept', $showdept);
$smarty->assign('show_dept', $show_dept);
$smarty->assign('toggleLockPermission', $toggleLockPermission ? '' : 'hide');
$smarty->assign('user_name', htmlspecialchars($user_name));
$smarty->assign('hide_dept', (int)$hideDept);

$smarty->assign('paginationStr', $paginationStr);

$smarty->assign('k', htmlspecialchars(postreq("k")));
$smarty->assign('pageTitle', 'ユーザー管理');
$smarty->assign('dataCount', $dataCount);
$smarty->assign('managerCount', $managerCount);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('userList', $userList);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'mst/user.tpl');

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
