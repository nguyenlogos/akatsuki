<?php
use Akatsuki\Models\Proj;
use Illuminate\Database\Capsule\Manager as DB;
use AwsServices\Ec2;

require_once(ROOT_PATH . "/if/updateEBS.php");

$config = array(
    'credentials'   => [
        'key'       => $_SESSION["key"],
        'secret'    => $_SESSION["secret"],
    ],
    'region'        => $_SESSION["region"],
    'version'       => 'latest'
);
updateEBS($_SESSION['cid'], $smarty, $config);

// 初期化
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$cid = "";
$cid = (int)$_SESSION["cid"];
$paginationStr = '';
$button = postreq("button");

// check page permission
$pageRoles = permission_check("mst/proj.php", true);
$showDept = $pageRoles['read']['condition'] != 1;

//drag and drop dept
$event = (isset($_POST["page_id_array"]) ? $_POST["page_id_array"] : null);
for ($i=0; $i<count($event); $i++) {
    $query = "
        UPDATE proj
        SET disporder = '".$i."'
        WHERE cid = " . $cid . " AND pcode = '".$_POST["page_id_array"][$i]."' AND status = 0";
    pg_query($smarty->_db, $query);
}

// ボタンが押されたときの処理
if ($button == "削除") {
    // 削除処理
    if (postreq("pcode2") == "") {
        // IDが空の場合はシステムエラー
        errorScreen($smarty, "システムエラーが発生しました。POSTデータが不正です。");
        exit;
    }
    // PostgreSQL用にIDをエスケープ
    $pcode = pg_escape_string(htmlspecialchars($_POST["pcode2"]));

    // 更新するuserのdisporderを取得
    $sql = sprintf(
        "SELECT disporder,pname
                  FROM proj
                  WHERE cid = $1 AND id = $2 and status = 0"
    );
    $r = pg_query_params($smarty->_db, $sql, [
        $cid,
        $pcode
    ]);
    $logs[] = $sql;
    $row = pg_fetch_row($r, 0);
    $disporder = $row[0];
    $pname = $row[1];

    $r = false;
    $proj = Proj::where('id', $pcode)
                ->first();
    if ($proj) {
        $proj->status = -1;
        $r = $proj->save();
    }
    $sql = "
        UPDATE proj
        set disporder = disporder - 1
        WHERE cid = " . $cid . "
        AND disporder > " . $disporder . "
        AND status = 0;
    ";

    $logs[] = $sql;
    $r && $r = pg_query($smarty->_db, $sql);
    // 表示順の洗い替え
    // 削除した表示順以降のレコードのみ
    if (!$r) {
        $msg = MESSAGES['ERR_UPDATE'];
        $err = 1;
    } else {
        $msg = "削除しました。";
        $err = 0;
    }
} elseif ($_SERVER["REQUEST_METHOD"] == "POST") {
    // deptの切り替え
    $dept = postreq("dept");
    if ($dept == -1) {
        $dept = "";
    }
} // 各種ボタンの処理終わり

// ソートキーボタンの設定
$sortMap = [
    "p.disporder", "p.disporder DESC",
    "p.pcode", "p.pcode DESC",
    "p.pname", "p.pname DESC",
    "d.deptname", "d.deptname DESC",
];

// アクティブなソートキーボタンを設定
// 併せてSQLのソートキーを設定
$sortIndex = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

$conditions = [
    "p.cid = {$cid}",
    "p.status = 0",
];

$proj_name = htmlspecialchars(getreq('proj'));
$show_dept = (int)getreq('dept');

if (!$showDept) {
    $conditions[] = sprintf("p.dept = %d", $_SESSION['dept']);
} elseif (!empty($show_dept)) {
    $conditions[] = sprintf("p.dept = %d", (int)$show_dept);
}

if ($proj_name) {
    $fmPName = pg_escape_string($proj_name);
    $fmPName = strtoupper($fmPName);
    $conditions[] = sprintf("(upper(p.pcode) LIKE '%%%s%%' OR upper(p.pname) LIKE '%%%s%%')", $fmPName, $fmPName);
}

$conditions = "WHERE " . implode(' AND ', $conditions);
// ページ総数の取得
$sql = "
    SELECT
        COUNT(p.pcode)
    FROM
        proj p $conditions
";
$r = pg_query($smarty->_db, $sql);
$dataCount = (int)pg_fetch_result($r, 0, 0);
// 現在のページ番号
$p = (int)getreq("p");
if ($p <= 0) {
    $p = 1;
}

$proj_name = htmlspecialchars($proj_name);
if ($dataCount > 0) {
    $paginationStr = getPagenationStr(
        $smarty,
        $dataCount,
        DEFAULT_COUNTPAGE_PRJ,
        $p,
        "./proj.php?dept={$show_dept}&proj={$proj_name}&sk={$sortIndex}&p="
    );
}

// データテーブルの生成
$pOffset = ($p - 1) * ITEMS_PER_PAGE;
$sql = "
    SELECT
        p.pcode,
        p.pname,
        d.deptname,
        p.disporder,
        p.id
    FROM
        proj p
        LEFT JOIN
            dept d
            ON d.cid = p.cid
            AND d.dept = p.dept
            AND d.status = 0 {$conditions}
    ORDER BY
        {$sortColumn} LIMIT ".ITEMS_PER_PAGE." OFFSET ".$pOffset.";
";
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$items = pg_fetch_all($r);

// instances and volumes mapping
$ret = \Akatsuki\Models\Instance::updateInstances();
$instAppovedStt = INST_REQ_APPROVED;
$sql = "
    SELECT
        i.pcode,
        i.id
    FROM
        instance i
    WHERE
        i.cid = {$_SESSION['cid']}
        AND i.pcode IS NOT NULL
        AND i.state NOT IN ('shutting-down', 'terminated')
    ORDER BY
        i.pcode
";
$r = pg_query($smarty->_db, $sql);
$instData = pg_fetch_all($r);
$pjMaps = [];
$ec2Client = Ec2::getInstance();

foreach ($instData as $data) {
    $pcode = $data['pcode'];
    unset($data['pcode']);
    $instanceVolumes = $ec2Client->describeVolumes([
        'Filters' => [
            [
                'Name' => 'attachment.instance-id',
                'Values' => [$data['id']],
            ],
        ],
    ]);
    $data['num_volumes'] = count($instanceVolumes);
    $pjMaps[$pcode] || $pjMaps[$pcode] = [];
    $pjMaps[$pcode][] = $data;
}
$projList = "";
foreach ($items as $proj) {
    $pid = $proj['id'];
    $pcode = htmlspecialchars($proj['pcode']);
    $pname = htmlspecialchars($proj['pname']);
    $deptname = htmlspecialchars($proj['deptname']);
    $dipsorder = htmlspecialchars($proj['disporder']);
    $numInstances = $numVolumes = 0;
    if (array_key_exists($pcode, $pjMaps)) {
        $pjInfo = $pjMaps[$pcode];
        $numInstances = count($pjInfo);
        $numVolumes = array_sum(array_column($pjInfo, "num_volumes"));
    }
    // 表示順
    $projList .= "<tr id=".$pcode."><td>" . $dipsorder . "</td>";
    // 上下矢印
    // プロジェクトID
    $projList .= "<td class='text-wrap'>" . $pcode . "</td>";
    // プロジェクト名
    $projList .= "<td>" . $pname . "</td>";
    // グループ名
    $projList .= "<td>" . $deptname . "</td>";
    $projList .= "<td align='center'>" . $numInstances . "</td>";
    $projList .= "<td align='center'>" . $numVolumes . "</td>";
    // 変更削除ボタン
    $projList .= "
    <td class='text-center'>
        <a class='btn btn-blue btn-sm mb-1'
            href='./proj2.php?cmd=update&id=" . $pid . "'
            role='button'>変更
        </a>
        <button type='button'
            class='btn btn-danger btn-sm mb-1'
            data-toggle='modal'
            data-target='#deleteData'
            data-dataid='" . $pcode . "'
            data-id='" . $pid . "'
            data-name='" . $pname . "'>削除
        </button>
    </td>";
    $projList .= "</tr>\n";
}
raise_sql($logs, 'proj');
$smarty->assign('proj', $proj_name);
$smarty->assign('dept', $show_dept);
$smarty->assign('showDept', (int)$showDept);

$smarty->assign('paginationStr', $paginationStr);

$smarty->assign('k', htmlspecialchars(postreq("k")));
$smarty->assign('pageTitle', 'プロジェクト管理');
$smarty->assign('dataCount', $dataCount);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('projList', $projList);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'mst/proj.tpl');

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
