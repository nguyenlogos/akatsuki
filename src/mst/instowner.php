<?php
require_once(ROOT_PATH . '/if/updateInst.php');
require_once(ROOT_PATH . '/if/updateSG.php');

$msg = "";
$showgroup = "";
$showgproject = "";

$deptRoles = permission_check("mst/dept.php", true);
$pageRoles = permission_check("mst/instowner.php", true);
$hideDept = false;

// \Akatsuki\Models\Instance::updateInstances();

// ソートキーボタンの設定
$sortMap = [
    "ins.name", "ins.name DESC",
    "ins.type", "ins.type DESC",
    "ins.state", "ins.state DESC",
    "de.deptname", "de.deptname DESC",
    "pr.pname", "pr.pname DESC",
];

// アクティブなソートキーボタンを設定
// 併せてSQLのソートキーを設定
$sortIndex  = (int)getreq("sk");
$sortColumn = "";
if ($sortIndex < 0) {
    $sortIndex = 0;
}
if (array_key_exists($sortIndex, $sortMap)) {
    $sortColumn = $sortMap[$sortIndex];
} else {
    $sortColumn = $sortMap[0];
}

if (!$deptRoles['read']['allowed'] || $deptRoles['read']['condition'] == 1) {
    $showgroup = $_SESSION['dept'];
    $hideDept = true;
} else {
    $showgroup = postreq("dept");
}

$showgproject = pg_escape_string(postreq("proj"));
$conditions = [
    "ins.cid = {$_SESSION['cid']}",
    "ins.state != 'terminated'",
    "ins.state != 'shutting-down'",
];

if ($showgroup) {
    $conditions[] = sprintf("(de.dept = %d)", (int)$showgroup);
    if ($showgproject) {
        $conditions[] = sprintf("pr.pcode = '%s'", $showgproject);
    }
}
$conditions = implode(" AND ", $conditions);
$sql = "
    SELECT
        ins.id,
        ins.name,
        ins.type,
        ins.platform,
        ins.state,
        de.deptname,
        ins.availabilityzone,
        ins.launchtime,
        de.dept,
        pr.pcode,
        pr.pname
    FROM instance ins
    LEFT JOIN proj pr
        ON
            ins.pcode = pr.pcode
        AND
            pr.status = 0
    LEFT JOIN dept de
        ON
            pr.dept = de.dept
        AND
            pr.cid = de.cid
        AND
            de.status = 0
    WHERE
        {$conditions}
    ORDER BY
        {$sortColumn};
";
$logs[] = $sql;
// インスタンスリストの取得

$r = pg_query($smarty->_db, $sql);

$instList = "";
$items = $r ? pg_fetch_all($r) : [];
foreach ($items as $item) {
    $instState = $item['state'];
    if ($instState == 'running') {
        $instState = "稼働中";
    } elseif ($instState == 'stopped') {
        $instState = "停止";
    }

    $instLaunchTime = $item['launchtime'];
    $instLaunchTime = str_replace("-", "/", $instLaunchTime);
    $instLaunchTime = str_replace("T", " ", $instLaunchTime);
    $instLaunchTime = str_replace("+00:00", "", $instLaunchTime);

    $instName = $item['name'];
    if (empty($instName)) {
        $instName = "(未設定)";
    }
    $deptName = $item['deptname'];
    if (empty($deptName)) {
        $deptName = "(未設定)";
    }
    $dept = $item['dept'];
    $pcode = $item['pcode'];
    $instList .= "
        <tr>
        <td class='inst_name'>{$instName}<br />({$item['id']})</td>
            <td class='inst_id d-none' data-instid='{$item['id']}'></td>
            <td>{$item['type']}</td>
            <td>{$instState}</td>
            <td class='inst_dept' data-dropdown='dept' data-selected='{$dept}'></td>
            <td class='inst_proj' data-dropdown='proj' data-selected-from='{$dept}' data-selected='{$pcode}'></td>
            <td>
                <button type='button' class='btn btn-save btn-blue'>保存</button>
            </td>
        </tr>";
}
raise_sql($logs, 'instowner');
$infocount = isset($_SESSION["infocount"]) ? $_SESSION["infocount"] : "";
$frmValues = [
    'dept' => $showgroup,
    'proj' => $showgproject,
];

$smarty->assign('pageTitle', 'プロジェクト・リソース管理');
$smarty->assign('msg', $msg);
$smarty->assign('dept', $dept);
$smarty->assign('pcode', $pcode);
$smarty->assign('schar', $schar);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $infocount);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('instList', $instList);
$smarty->assign('showgroup', $showgroup);
$smarty->assign('frmValues', $frmValues);
$smarty->assign('hideDept', $hideDept ? "hide" : "");

$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->assign('viewTemplate', 'mst/instowner.tpl');

$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
