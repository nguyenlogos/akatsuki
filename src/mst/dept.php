<?php
use Akatsuki\Models\Dept;

$button = postreq("button");
// 初期化
$err = 0;  // エラーコード 0: msgをINFORMATIONで表示。1: エラーで表示
$msg = ""; // メッセージ
$cid = "";
$cid = (int)$_SESSION["cid"];
$paginationStr = "";
//drap and drop dept
$event  = (isset($_POST["page_id_array"]) ? $_POST["page_id_array"] : null);
for ($i=0; $i<count($event); $i++) {
    $query = "
     UPDATE dept
     SET disporder = '".$i."'
     WHERE cid = " . $cid . " AND dept = '".$_POST["page_id_array"][$i]."' AND status = 0";
    pg_query($smarty->_db, $query);
}

// ボタンが押されたときの処理
if ($button == "新規登録") {
    // 新規登録
    $newDeptName = htmlspecialchars(postreq("newDeptName"));
    if ($newDeptName == "") {
        // グループ名が空の場合はエラー
        $msg = "グループ名は省略できません。";
        $err = 1;
    } else {
        // グループの新規登録
        // トランザクションの開始
        // IDの決定。削除ずみのものを含めてmaxを取得
        $sql = sprintf("SELECT max(dept) FROM dept WHERE cid = " . $cid) ;
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);
        $row = pg_fetch_row($r, 0);
        // レコードが1件もなければ IDに1を設定。
        if ($row[0] == "") {
            $deptNum = 1;
        } else {
            $deptNum = $row[0] + 1;
        }

        // 表示順の決定。削除済みのものを除いてmaxを取得
        $sql = sprintf("SELECT max(disporder) FROM dept WHERE cid = " . $cid . " and status = 0");
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);
        $row = pg_fetch_row($r, 0);
        if ($row[0] == "") {
            $dispNum = 1;
        } else {
            $dispNum = $row[0] + 1;
        }

        // 新しいグループのINSERT
        // PostgreSQL用に文字列をエスケープ
        $newDeptName = pg_escape_string($newDeptName);
        $dept = Dept::where('cid', $cid)
            ->where('deptname', $newDeptName)
            ->where('status', 0)
            ->first();
        if ($dept) {
            $msg = "指定されたグループ名は、すでに使われています。";
            $err = 1;
        } else {
            $dept = new Dept();
            $dept->setValues([
                'cid'       => $cid,
                'dept'      => $deptNum,
                'deptname'  => $newDeptName,
                'regdate'   => "now()",
                'status'    => 0,
                'disporder' => $dispNum
            ]);
            $r = $dept->save();
            // トランザクションの終了

            if (!$r) {
                $msg = MESSAGES['ERR_INSERT'];
                $err = 1;
            } else {
                // メッセージの設定
                $msg = "新しいグループを登録しました。";
                $err = 0;
            }
        }
    }
} elseif ($button == "保存") {
    // グループ名の変更
    if (postreq("gid2") == "") {
        // IDが空の場合はシステムエラー
        errorScreen($smarty, "システムエラーが発生しました。POSTデータが不正です。");
        exit;
    }
    if (postreq("newName") == "") {
        // グループ名が空の場合はエラー
        $msg = "グループ名は省略できません。";
        $err = 1;
    } else {
        // 変更処理
        // PostgreSQL用に文字列をエスケープ
        $newName = pg_escape_string(htmlspecialchars(postreq("newName")));
        $deptID  = (int)postreq("gid2");

        // 古いグループ名の取得
        $sql = sprintf("SELECT deptname FROM dept WHERE cid = $1 AND dept = $2 and status = 0");
        $logs[] = $sql;
        $r = pg_query_params($smarty->_db, $sql, [
            $cid,
            $deptID
        ]);

        if (pg_num_rows($r) != 1) {
            $msg = "グループ名の変更に失敗しました。対象レコードが見つかりませんでした。";
            $err = 1;
        } else {
            $oldName = pg_escape_string(pg_fetch_result($r, 0, 0));
            $dept = Dept::where("cid", $cid)
                        ->where("dept", $deptID)
                        ->where("status", 0)
                        ->first();
            if ($dept) {
                $r = true;
                if ($dept->deptname != $newName) {
                    $tmpDept = Dept::where('cid', $cid)
                        ->where('deptname', $newName)
                        ->where('status', 0)
                        ->first();
                    if ($tmpDept) {
                        $r = false;
                        $msg = "指定されたグループ名は、すでに使われています。";
                    } else {
                        $dept->setValues([
                            "deptname" => $newName,
                        ]);
                        $r = $dept->save();
                    }
                }
            } else {
                $r = false;
            }
            if ($r) {
                $msg = "変更しました。";
                $err = 0;
            } else {
                !$msg && $msg = MESSAGES['ERR_UPDATE'];
                $err = 1;
            }
        }
    }
} elseif ($button == "削除") {
    // 削除処理
    if (postreq("gid2") == "") {
        // IDが空の場合はシステムエラー
        errorScreen($smarty, "システムエラーが発生しました。POSTデータが不正です。");
        exit;
    }
    // PostgreSQL用にIDをエスケープ
    $deptID  = (int)postreq("gid2");
    // 所属ユーザがいる場合はエラー
    $sql = sprintf("SELECT empid
                  FROM emp
                  WHERE cid = $1 AND dept = $2 and status = 0");
    $logs[] = $sql;
    $r = pg_query_params($smarty->_db, $sql, [
        $cid,
        $deptID
    ]);
    if (pg_num_rows($r) > 0) {
        $msg = "ユーザが所属しているグループは削除できません。";
        $err = 1;
    } else {
        // 更新するdeptのdisporderを取得
        $sql = sprintf("SELECT disporder,deptname
                      FROM dept
                      WHERE cid = $1 AND dept = $2 and status = 0");
        $logs[] = $sql;
        $r = pg_query_params($smarty->_db, $sql, [
            $cid,
            $deptID
        ]);
        $row = pg_fetch_row($r, 0);
        $disporder = $row[0];
        $deptName = $row[1];
        // statusの更新
        $dept = Dept::where("cid", $cid)
                    ->where("dept", $deptID)
                    ->where("status", 0)
                    ->first();
        if ($dept) {
            $dept->setValues([
                "status" => -1,
            ]);
            $r = $dept->save();
        } else {
            $r = false;
        }


        // 表示順の洗い替え
        // 削除した表示順以降のレコードのみ
        $sql = "
            UPDATE dept
            SET disporder = disporder - 1
            WHERE cid = " . $cid . " AND disporder > " . $disporder . " AND status = 0";
        $logs[] = $sql;
        $r && $r = pg_query($smarty->_db, $sql);
        if (!$r) {
            $msg = MESSAGES['ERR_DELETE'];
            $err = 1;
        } else {
            $msg = "削除しました。";
            $err = 0;
        }
    }
}

// ソートキーボタンの設定

$sktable = array(
    "dept", "dept DESC", "regdate", "regdate DESC"
);
$schar[0] = $schar[2] = $schar[4] = $schar[6] = SORT_DN;
$schar[1] = $schar[3] = $schar[5] = $schar[7] = SORT_UP;

// アクティブなソートキーボタンを設定
// 併せてSQLのソートキーを設定
$sk = getreq("sk");
if ($sk == "") {
    $sk = 0;
}
if ($sk >= 0 && $sk <= 7) {
    $skStr = $sktable[$sk];
    if (($sk % 2) == 0) {
        $schar[$sk] = SORT_DN_ACTIVE;
    } else {
        $schar[$sk] = SORT_UP_ACTIVE;
    }
} else {
    $skStr = "disporder";
}

// ページ総数の取得
$sql = sprintf("SELECT count(dept) from dept where cid = " . $cid . " and status = 0");
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$row = pg_fetch_row($r, 0);

$dataCount = (int)$row[0];
$curPage = (int)getreq("p");
if ($curPage <= 0) {
    $curPage = 1;
}
// ページネーション文字列の生成
if ($dataCount > 0) {
    $paginationStr = getPagenationStr(
        $smarty,
        $dataCount,
        ITEMS_PER_PAGE,
        $curPage,
        "./dept.php?sk=" . $sk . "&p="
    );
}

// データテーブルの生成
$pOffset = ($curPage - 1) * ITEMS_PER_PAGE;
$sql = sprintf("
SELECT
    d.dept,
    d.deptname,
    to_char(d.regdate, 'YYYY/MM/DD HH24:MI:SS'),
    d.disporder,
    e.num_emp
FROM
    dept d
    LEFT OUTER JOIN
    (
        SELECT
            dept,
            COUNT(dept) AS num_emp
        FROM
            emp
        WHERE
            cid = {$cid}
            AND status = 0
        GROUP BY
            dept
    ) e
    ON (d.dept = e.dept)
WHERE
    d.cid = {$cid}
    AND d.status = 0
ORDER BY
    $skStr
LIMIT %d OFFSET %d
", ITEMS_PER_PAGE, $pOffset);
$logs[] = $sql;
$r = pg_query($smarty->_db, $sql);
$deptList = "";
$count_row = pg_num_rows($r);
for ($i = 0; $i < $count_row; $i++) {
    $row = pg_fetch_row($r, $i);

    // 件数前処理
    if (empty($row[4])) {
        $recCount = 0;
    } else {
        $recCount = (int)$row[4];
    }
    $aa = htmlspecialchars($row[1]);


    // 表示順
    $deptList .= "<tr id=".$row[0]."><td>" . ($i + 1) . "</td>";
    // 上下矢印
    // グループID
    // $deptList .= "<td>" . $row[0] . "</td>";
    // グループ名
    $deptList .= "<td class='text-wrap'>" . $aa . "</td>";
    // 登録更新日時
    $deptList .= "<td>" . $row[2] . " <span class='badge badge-small badge-pill badge-orange'>" . $recCount .
        "</span></td>";
    // 変更削除ボタン
    $disabled = $recCount != 0 ? "disabled" : "";
    $deptList .= "
        <td class='text-center'>
            <button type='button'
                class='btn btn-blue btn-sm'
                data-toggle='modal'
                data-target='#changeData'
                data-dataid='" . $row[0] . "'
                data-name='" . $aa . "'>変更
            </button>
            <button type='button'
                class='btn btn-danger btn-sm'
                data-toggle='modal'
                data-target='#deleteData'
                data-dataid='" . $row[0] . "'
                data-name='" . $aa . "'
                {$disabled}>削除
            </button>
        </td>
    ";
    $deptList .= "</tr>\n";
}
raise_sql($logs, 'dept');
$smarty->assign('paginationStr', $paginationStr);

$smarty->assign('k', htmlspecialchars(postreq('k')));
$smarty->assign('pageTitle', 'グループ管理');
$smarty->assign('dataCount', $dataCount);
$smarty->assign('msg', $msg);
$smarty->assign('err', $err);
$smarty->assign('userID', $_SESSION['uid']);
$smarty->assign('infocount', $_SESSION["infocount"]);
$smarty->assign('wfcount', $_SESSION["wfcount"]);
$smarty->assign('deptList', $deptList);
$smarty->assign('curURI', $_SERVER["REQUEST_URI"]);
$smarty->assign('viewTemplate', 'mst/dept.tpl');
$smarty->assign('metaKeyword', '');
$smarty->assign('description', '');
$smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
