<?php
require_once('config/consts.php');
require_once(SOURCE_PATH .'/common/CommonClass.class.php');
require_once(SOURCE_PATH .'/common/auth.php');

logout();
header("Location: /");
exit;
