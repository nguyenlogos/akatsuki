<?php
ob_start();
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once '../src/config/consts.php';
require_once '../src/config/messages.php';
require_once COMMON_PATH . "/CommonClass.class.php";
require_once COMMON_PATH . "/common.php";
require_once COMMON_PATH . "/local.php";

require_once 'autoloader.php';
require_once '../vendor/autoload.php';

$fileLogger = new \Common\FileLogger('general');

set_error_handler(function ($errno, $errstr, $errfile, $errline) use ($fileLogger) {
    $caller = "";
    switch ($errno) {
        case E_USER_WARNING:
            $caller = "warn";
            break;
        case E_USER_NOTICE:
            $caller = "notice";
            break;
    }
    if ($caller) {
        $fileLogger->$caller($errstr, [
            "File" => $errfile,
            "Line" => $errline,
        ]);
    }
});

register_shutdown_function(function () use ($fileLogger) {
    $lastError = error_get_last();
    if ($lastError) {
        $errFlag = (int)$lastError['type'];
        $error = $errFlag & E_ERROR;
        $userError = $errFlag & E_USER_ERROR;
        if ($error || $userError) {
            $rdCount = 0;
            $message = $lastError['message'];
            if ($message === AWS_INVALID_CREDENTIALS) {
                $_SESSION['ERR_AWS_KEY'] = 1;
            }
            if (!empty($_SESSION['rd_info'])) {
                $rdCount = (int)$_SESSION['rd_info']['count'];
                if ($rdCount > 5) {
                    exit();
                } else {
                    $rdCount++;
                }
            }
            $_SESSION['rd_info'] = [
                'err'   => 1,
                'msg'   => MESSAGES['ERR_UNKNOWN_ERROR'],
                'count' => $rdCount
            ];
            $fileLogger->crit($message, [
                "File" => $lastError['file'],
                "Line" => $lastError['line'],
            ]);
            $requestURI = trim($_SERVER['REQUEST_URI'], "/");
            rd($requestURI);
        }
    }
});

new Akatsuki\Models\Database();

$smarty = new MySmarty();
$smarty->commonInfoSet();
$authInfo = setAuthInfo($smarty->_db);
$r = getreq('r');
if (!$r) {
    $requestURI = $_SERVER['REQUEST_URI'];
    $requestURI = trim($requestURI, '/');
    if (preg_match('/^(?:reg|managerids)/', $requestURI)) {
        rd("{$requestURI}/");
    }
} else {
    $r = trim($r, "/ ");
}
if ($r && $r !== 'index.php') {
    if ($r == 'managerids') {
        rd("managerids/index.php");
    }

    $checkAuth = true;
    if (preg_match('/\/$/', $r)) {
        $r .= "index.php";
    }

    if (preg_match('/^(?:reg|managerids|api)\//', $r)) {
        $checkAuth = false;
    }
    $fullpath = __DIR__ . "/$r";
    if (is_readable($fullpath)) {
        if ($checkAuth && $authInfo !== 'ok') {
            errorScreen($smarty, MESSAGES['ERR_UNAUTHORIZED']);
            exit();
        }
        if ($checkAuth) {
            $allRoles = permission_check();
            $roles = $allRoles[$r];

            if (empty($roles['read']) || !$roles['read']['allowed']) {
                rd("src/403.php");
            }
            $stateRoles = [];
            foreach ($allRoles as $url => $roles) {
                $stateRoles[$url]['visible'] = $roles['read']['allowed'] ? '' : 'hide';
            }

            $smarty->assign('STATE_ROLES', $stateRoles);
            updateWfTotalCount($smarty);
        }
        include_once($fullpath);
    } else {
        rd("src/404.php");
    }
} elseif ($authInfo == 'ok') {
    // Redirect to info page if user has already logged in
    // Caution: Don't redirect to db/db.php or cons/inst.php, because it may cause problems on /cons/request.php
    rd("cons/info.php");
} else {
    $msg = '';
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $result = login($smarty->_db);
        if ($result == 'ok yet') {
            rd("admin/key.php");
        }

        if ($result == 'ok') {
            rd("db/db.php");
        }

        if ($result == 'ng') {
            $msg = "パスワードまたはメールアドレスが間違っています。";
        } elseif ($result == 'lock') {
            $email = pg_escape_string($_POST["Email"]);
            $lock = '<p>アカウントはロックされています。</p>
                     <p><a href="/reg/forgot.php">パスワードの再設定</a>を行ってください。</p>
                     <p>または御社の管理者またはマネージャーにお問合せください。</p>';

            $sql = sprintf('
                SELECT
                    cid,
                    dept,
                    email
                FROM
                    emp
                WHERE
                    email = $1
            ');

            $r = pg_query_params($smarty->_db, $sql, [
                $email
            ]);
            $rowUser = pg_fetch_all($r);

            $mailer = new \Common\Mailer();
            $mailSubject = "[SunnyView] アカウントがロックされました。";
            $mailBody = "ログインに連続して失敗したため、アカウントがロックされました。\n\n
パスワードの再設定を行ってください。\n
または御社の管理者またはマネージャーにお問合せください。";
            $mailer
                ->set('subject', $mailSubject)
                ->set('to', $rowUser[0]['email'])
                ->set('body', $mailBody);
            // 「管理者」または「マネージャ」のBCCメール
            $mailer->setBcc($smarty->_db, $rowUser[0]['email']);

            $mailer->send();
        } elseif ($result == 'active') {
            $active = '<p>新規登録時にお送りした登録確認メールをご確認ください。<br />アカウントがアクティベートされていません。</p>';
        } else {
            $msg = "";
        }
    }
    $smarty->assign('active', $active);
    $smarty->assign('lock', $lock);
    $smarty->assign('msg', $msg);
    $smarty->assign('viewTemplate', 'index.tpl');
    $smarty->display(TEMPLATES_PATH . 'pagelayout.tpl');
}
