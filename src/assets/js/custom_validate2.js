$(document).ready(function () {
    $('.btn-validate').click(function (event) {
        //Add validation rule for dynamically generated Size (GiB) fields
        $('.quantity').each(function () {
            $(this).rules(
                "add",
                {
                    required: true,
                    digits: true,
                    messages: {
                        required: "サイズ(GiB) が入力されていません",
                    }
                }
            );
        });
        //Add validation rule for dynamically generated IOPS fields
        $('.data_check').each(function () {
            $(this).rules(
                "add",
                {
                    required: true,
                    digits: true,
                    messages: {
                        required: "IOPS が入力されていません",
                    }
                }
            );
        });
    });
    $("#orderform").validate();

    //check select option value Device type
    $('.target').each(function () {
        $(this).on('change',function () {
            var value = $(this).find(":selected").val();
            if (value === 'io1') {
                $(this).parents('.seson').find('input[data-hidden]').removeClass('hidden_iops');
            } else {
                $(this).parents('.seson').find('input[data-hidden]').addClass('hidden_iops');
            }

        });
    })

    function forceNumeric()
    {
        var $input = $(this);
        $input.val($input.val().replace(/[^\d]+/g,''));
    }
    $('form').on('propertychange input', 'input[type="number"]', forceNumeric);
    (function(){
        $.validator.addMethod('phonecheck', function(value, element) {
            return this.optional(element) || /^(\(\d{1,6}\)|\d{1,6})[-]{1}(\(\d{1,6}\)|\d{1,6})[-]{1}(\(\d{1,6}\)|\d{1,6})$/i.test(value);
        }, '電話番号が間違っています。');

        $('#reg_form_2').validate({
            ignore: ".ignore",
            rules: {
                name: {
                    maxlength: 64
                },
                tel: {
                    maxlength: 64,
                    phonecheck: true
                },
                company: {
                    maxlength: 128
                },
                dept: {
                    maxlength: 64
                },
                position: {
                    maxlength: 64
                },
                email: {
                    maxlength: 64,
                    email: true,
                },
                email2: {
                    equalTo: "#email",
                },
                p1: {
                    required: true,
                    maxlength: 64,
                },
                p2: {
                    required: true,
                    maxlength: 64,
                },
                p3: {
                    required: true,
                    maxlength: 64,
                },
                agree: {
                    required: true,
                },
                group: {
                    required: true,
                },
                hiddenRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                // name: "名前は必須です。",
                name: {
                    required: "氏名は省略できません。",
                    maxlength: "64 文字までしか入力できません。",
                },
                company: {
                    required: "会社名は省略できません。",
                    maxlength: "128 文字までしか入力できません。",
                },
                dept: {

                    maxlength: "64 文字までしか入力できません。",
                },
                position: {

                    maxlength: "64 文字までしか入力できません。",
                },
                tel: {
                    required:"電話番号は必須です。",
                    maxlength: "64 文字までしか入力できません。",
                },
                // email: "メールアドレスの形式が不正です。",
                email: {
                    required: "メールアドレスは省略できません。",
                    maxlength: "64 文字までしか入力できません。",
                },
                email2: {
                    required: "再入力されたメールアドレスが異なります。",
                    maxlength: "64 文字までしか入力できません。",
                    equalTo: "同じメールアドレスをもう一度入力してください。",
                },
                pass: {
                    required: "パスワードは省略できません。",
                    minlength: "パスワードは6文字以上で入力してください。"
                },
                pass2: {
                    required: "パスワード(再入力)は省略できません。",
                    equalTo: "同じパスワードをもう一度入力してください。",
                },
                p1: {
                    required: "パスワードは必須です。",
                    maxlength: "64 文字までしか入力できません。",
                },
                p2: {
                    required: "パスワードは必須です。",
                    maxlength: "64 文字までしか入力できません。",
                },
                p3: {
                    required: "パスワード(再入力)は必須です。",
                    equalTo: "同じパスワードをもう一度入力してください。",
                },
                notif_title: {
                    required: "が入力されていません。",
                },
                agree: "利用規約への同意は必須です。",
                hiddenRecaptcha: "キャプチャは必須です。",
                group: "所属グループは省略できません。",
            }
        });
    })();
});

// //check number phone
// $(window).load(function () {
//     var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-######"}];
//     $('.textbox').inputmask({
//         mask: phones,
//         greedy: false,
//         definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
// });

//price format
(function ($) {
  /******************
   * Unmask Function *
   *******************/
    $.fn.unmask = function () {

        var field = $(this).val();
        var result = "";

        for (var f in field) {
            if (!isNaN(field[f]) || field[f] == "-") {
                result += field[f];
            }
        }

        return result;
    };


    /*
    * Translated default messages for the jQuery validation plugin.
    * Locale: JA
    * https://github.com/satooshi/jquery-validation-ja-util/tree/master/jquery-validation/localization
    */
    $.extend($.validator.messages, {
        required: "このフィールドは必須です。",
        remote: "このフィールドを修正してください。",
        email: "有効なEメールアドレスを入力してください。",
        url: "有効なURLを入力してください。",
        date: "有効な日付を入力してください。",
        dateISO: "有効な日付（ISO）を入力してください。",
        number: "有効な数字を入力してください。",
        digits: "数字のみを入力してください。",
        creditcard: "有効なクレジットカード番号を入力してください。",
        equalTo: "同じ値をもう一度入力してください。",
        extension: "有効な拡張子を含む値を入力してください。",
        maxlength: $.validator.format("{0} 文字以内で入力してください。"),
        minlength: $.validator.format("{0} 文字以上で入力してください。"),
        rangelength: $.validator.format("{0} 文字から {1} 文字までの値を入力してください。"),
        range: $.validator.format("{0} から {1} までの値を入力してください。"),
        step: $.validator.format("{0} の倍数を入力してください。"),
        max: $.validator.format("{0} 以下の値を入力してください。"),
        min: $.validator.format("{0} 以上の値を入力してください。"),

        maxWords: $.validator.format("{0}単語以内で入力してください。"),
        minWords: $.validator.format("{0}単語以上で入力してください。"),
        rangeWords: $.validator.format("{0}単語から{1}単語以内で入力してください。"),
        letterswithbasicpunc: "Letters or punctuation only please",
        alphanumeric: "英数字またはアンダースコアで入力してください。",
        lettersonly: "英字で入力してください。",
        nowhitespace: "空白は入力できません。",
        //ziprange: "Your ZIP-code must be in the range 902xx-xxxx to 905-xx-xxxx",
        //zipcodeUS: "The specified US ZIP Code is invalid",
        integer: "整数で入力してください。",
        //vinUS: "The specified vehicle identification number (VIN) is invalid.",
        //dateITA: "Please enter a correct date",
        //dateNL: "Vul hier een geldige datum in.",
        time: "00:00から23:59までの時間を入力してください。",
        time12h: "00:00 amから12:00 pmまでの時間を入力してください。",
        //phoneUS: "Please specify a valid phone number",
        //phoneUK: 'Please specify a valid phone number',
        //mobileUK: 'Please specify a valid mobile number',
        //phonesUK: 'Please specify a valid uk phone number',
        //postcodeUK: 'Please specify a valid postcode',
        strippedminlength: $.validator.format("{0}文字以上で入力してください。"),
        email2: $.validator.messages.email,
        url2: $.validator.messages.url,
        creditcardtypes: "有効なクレジットカード番号を入力してください。",
        ipv4: "有効なIP v4アドレスを入力してください。",
        ipv6: "有効なIP v6アドレスを入力してください。",
        pattern: "不正な形式です。",
        require_from_group: $.validator.format("これらの項目は{0}文字以上で入力してください。"),
        skip_or_fill_minimum: $.validator.format("これらの項目は空欄または{0}文字以上で入力してください。"),
        accept: $.validator.format("有効なmimeタイプのファイルを指定してください。"),
        extension: $.validator.format("有効な拡張子のファイルを指定してください。"),
        ip_range_address: "Please input ip by format x.x.x.x/y (x = 0~255, y = 0~32)",
        port_range: "Please input correct port range",
        unique: "Please input unique security group rules type"
    });
})(jQuery);
