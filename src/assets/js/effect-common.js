
// menu opener

$(document).ready(function(){
  $('.switching-button').click(function () {
    $('body').toggleClass('small-menu-contents');
  });
});

// button toggle

$(document).ready(function(){
  $('.btn-toggle').click(function () {
    $('.btn-toggle').toggleClass('status-on');
  });
});

// sub-nav button

$(document).ready(function(){
  $('.sub-nav-button').click(function () {
    $('header').toggleClass('status-on');
  });
});