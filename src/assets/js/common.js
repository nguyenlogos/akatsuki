function confDialog(strTitle, strComment, func1)
{

    // ダイアログのメッセージを設定
    $("#dialog").html(strComment);

    // ダイアログを作成
    $("#dialog").dialog({
        modal: true,
        title: strTitle,
        width: 450,
        height: 200,
        resizable: false,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
                func1();
            },
            "キャンセル": function () {
                $(this).dialog("close");
            }
        }
    });
}

function msgDialog(strTitle, strComment)
{
    // ダイアログのメッセージを設定
    $("#dialog").html(strComment);

    // ダイアログを作成
    $("#dialog").dialog({
        modal: true,
        title: strTitle,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
}

$('document').ready(function () {
    if ( window.toastr ) {
        window.toastr.options = {
            "positionClass": "toast-bottom-right",
            "timeOut": "3000"
        };
    }
    var dialog = function () {
        var state = -1;
        var gcallback;
        var _options = {};
        var $btnOK, $btnCancel, $btnConfirm;
        var $modalContent, $modalTitle, $modalFooter;
        var $modal = $('#iModal');
        if ( $modal.length > 0 ) {
            $modalContent = $modal.find('modal-body > div');
            $modalTitle = $modal.find('modal-title');
            $modalFooter = $modal.find('modal-footer');
            $btnOK = $modal.find('.btn-ok');
            $btnCancel = $modal.find('.btn-cancel');
            $btnConfirm = $modal.find('.btn-confirm');
        } else {
            $modalContent = $('<div class="responsive-vertical scrollable">');
            $modalTitle = $('<h4 class="modal-title">');
            $modalFooter = $('<div class="modal-footer text-right">');
            $btnOK = $('<button type="button" class="btn btn-blue btn-ok">')
                .text('ＯＫ');
            $btnCancel = $('<button type="button" class="btn btn-secondary btn-cancel" data-dismiss="modal">')
                .text('キャンセル')
                .click(function () {
                    state = -1;
                    if ( typeof gcallback === 'function' ) {
                        gcallback(state);
                    }
                });
            $btnConfirm = $('<button type="button" class="btn btn-orange btn-confirm">')
                .text('はい')
                .on('click', function () {
                    state = 1;
                    if ( typeof gcallback === 'function' ) {
                        gcallback(state);
                    }
                });
            var $btnClose = $('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>');
            $modal =
                $('<div class="modal fade" id="iModal" tabindex="-1" role="dialog" aria-hidden="true">').append(
                    $('<div class="modal-dialog modal-dialog-centered" role="document">').append(
                        $('<div class="modal-content">').append(
                            $('<div class="modal-header">').append(
                                $modalTitle,
                                $btnClose,
                            ),
                            $('<div class="modal-body">').append(
                                $modalContent
                            ),
                            $modalFooter.append($btnCancel, $btnConfirm, $btnOK)
                        )
                    )
                );
        }

        function updateConfig(options, type)
        {
            options || (options = {});
            var title = options.title || '確認';
            $modalTitle.html(title);
            var buttons = options.buttons || [];
            $modalFooter.show().find('.btn-custom').remove();
            $btnOK.hide();
            $btnCancel.hide();
            $btnConfirm.hide();
            if ( buttons.length === 0 ) {
                if ( type === 'info' ) {
                    $modalFooter.hide();
                } else if ( type === 'confirm' ) {
                    $btnCancel.show();
                    $btnConfirm.show();
                }
            } else {
                buttons.forEach(function (button) {
                    $button = $(button.button);
                    var callback = button.callback;
                    if ( typeof callback === 'string' ) {
                        callback = window.util.stringToFunction(callback);
                    }
                    if ( typeof callback === 'function' ) {
                        $button.on('click', callback);
                    };
                    $modalFooter.append($button.addClass('btn-custom'));
                });
            }
            var modalClass = options.modalClass || '';
            if ( modalClass !== _options.modalClass ) {
                _options.modalClass = modalClass;
                modalClass = 'modal-dialog modal-dialog-centered ' + modalClass;
                $modal.find('.modal-dialog').removeClass().addClass(modalClass);
            }
            delete options.title;
            delete options.buttons;
            delete options.modalClass;
            $modal.options = options;
        }

        function closeModal()
        {
            var prevent_close = $modal.options.prevent_close;
            if (!prevent_close) {
                $modal.modal('hide');
            }
        };

        $modal.confirmDialog = function (msg, options, callback) {
            state = -1;
            gcallback = undefined;
            updateConfig(options, 'confirm');
            $modalContent.html(msg);
            $modal.modal('show');
            gcallback = callback;
        };

        $modal.infoDialog = function (msg, options, callback) {
            state = -1;
            gcallback = undefined;
            updateConfig(options, 'info');
            $modalContent.html(msg);
            $btnCancel.hide();
            $btnOK.hide();
            $btnConfirm.hide();
            $modal.modal('show');
            gcallback = callback;
        };

        $modal.close = function () {
            closeModal();
        };

        $modal.setState = function (state) {
            var dialogOptions = $modal.options;
            switch (state) {
                case 'no-close':
                    dialogOptions.prevent_close = true;
                    break;
                default:
                    dialogOptions.prevent_close = false;
                    break;
            }
        };

        $modal.on('hide.bs.modal', function (e) {
            var prevent_close = $modal.options.prevent_close;
            if (prevent_close) {
                e.preventDefault();
            }
        });
        $btnClose.on('click', closeModal);
        $btnCancel.on('click', closeModal);
        $btnOK.on('click', closeModal);
        return $modal;
    }
    var util = function () {
        this.extractUrlParam = function (search, urlStr) {
            if ( !search ) {
                return "";
            }
            if ( !urlStr ) {
                urlStr = window.location.href;
            }
            var url = new URL(urlStr);
            return url.searchParams.get(search);
        }

        // TODO: replace by new function
        this.replaceUrlParam = function (url, paramName, paramValue) {
            var searchParams = new URLSearchParams(url);
            searchParams.set(paramName,paramValue);
            return "?" + searchParams.toString()
        }

        this.setParamsURL = function(params) {
            params || (params = {});
            var searchParams = new URLSearchParams(window.location.search);
            for (key in params) {
                var value = params[key];
                if (typeof value !== 'undefined') {
                    searchParams.set(key, value);
                } else {
                    searchParams.delete(key);
                }
            }
            searchParams = searchParams.toString();
            if (searchParams) {
                var newUrl = window.location.pathname + "?" + searchParams.toString();
            } else {
                var newUrl = window.location.pathname;
            }
            window.history.replaceState({}, '', newUrl);
        }

        this.getParamUrl = function(key) {
            var searchParams = new URLSearchParams(window.location.search);
            return searchParams.get(key);
        }

        this.parseJSON = function (jsonStr) {
            var jsonData;
            try {
                jsonData = JSON.parse(jsonStr);
            } catch (err) {
            }
            return jsonData;
        }

        this.readFromClipboard = function (e) {
            if ( e.type === 'paste' ) {
                return e.originalEvent.clipboardData.getData('text/plain');
            }
            return '';
        }

        this.stringToFunction = function (str) {
            if ( typeof str !== 'string' ) {
                return;
            }
            var fn = undefined;
            var parts = str.split('.');
            if ( parts.length === 1 ) {
                fn = window[str];
            } else {
                var obj = parts.shift();
                obj = window[obj];
                var isCallable = !!obj;
                if ( isCallable ) {
                    for (var i = 0, length = parts.length; i < length; ++i) {
                        if ( !obj[parts[i]] ) {
                            isCallable = false;
                            break;
                        } else {
                            obj = obj[parts[i]];
                        }
                    };
                    if ( isCallable ) {
                        fn = obj;
                    }
                }
            }
            return fn;
        }

        this.ajax = function(url, data, type) {
            var deferred = $.Deferred();
            $.ajax({
                'url': url,
                'type' : (type || 'get').toUpperCase(),
                'data' : data || {},
                'dataType' : 'json',
                'success': function(data) {
                    deferred.resolve(data);
                },
                'error': function(error) {
                    deferred.reject(error);
                }
            });
            return deferred.promise();
        }

        this.ajaxPost = function(url, data) {
            return window.util.ajax(url, data, 'post');
        }

        this.ajaxAll = function(requests) {
            return $.when.apply($, requests);
        }
    };
    window.util = new util();
    window.dialog = new dialog();
    (function () {
        function sort(sortValue)
        {
            var newUrlParams = window.util.replaceUrlParam(window.location.search, 'sk', sortValue);
            window.location.href = window.location.pathname + newUrlParams;
        };
        var currentSortValue = window.util.extractUrlParam('sk');
        var forceReloadState = false;
        if ( !currentSortValue ) {
            var $inputSK = $('input[name="sk"]');
            if ( $inputSK.length ) {
                currentSortValue = $inputSK.val();
                forceReloadState = true;
            }
        }
        if ( !currentSortValue || +currentSortValue < 0 ) {
            currentSortValue = 0;
        } else {
            currentSortValue = +currentSortValue;
        }

        if ( currentSortValue > 0 || forceReloadState ) {
            var newUrlParams = window.util.replaceUrlParam(window.location.search, 'sk', currentSortValue);
            var newUrl = window.location.pathname + newUrlParams;
            window.history.replaceState({}, '', newUrl);
        }


        $('table.sortable th.sort-header').each(function (index) {
            var $this = $(this);
            if ( currentSortValue < 0 ) {
                currentSortValue = 0;
            }
            $this.on('click', function () {
                var sortValue;
                var currentSortValue = +window.util.extractUrlParam('sk') || 0;
                if ( currentSortValue < 0 ) {
                    currentSortValue = 0;
                }
                if ( currentSortValue === (index * 2) || currentSortValue === (index * 2 + 1)) {
                    var sortType = currentSortValue % 2 === 0 ? 1 : 0;
                    sortValue = index * 2 + sortType;
                } else {
                    sortValue = index * 2;
                }
                sort(sortValue);
            }).addClass('header').removeClass('sort-header').append('<i class="fas fa-angle-down"></i>');
            if ( currentSortValue === (index * 2) ) {
                $this.addClass('headerSortUp on').find('.fas').remove();
                $this.append('<i class="fas fa-angle-up"></i>');
            } else if ( currentSortValue === (index * 2 + 1) ) {
                $this.addClass('headerSortDown on').find('.fas').remove();
                $this.append('<i class="fas fa-angle-down"></i>');
                // $this.addClass('headerSortUp on').find('.fas').removeClass('fa-angle-down').addClass('fa-angle-up');
            }
        });
    })();

    (function () {
        var $sortable = $('#fixed-drop-table');
        if ( $sortable.length > 0 ) {
            var $tbody = $sortable.find('tbody');
            var columnIndex = $sortable.find('thead th.column-index').index() + 1;
            var $sortColumns = null;
            var sortable_tables = {
                sorting_field_table: function () {
                    $tbody.sortable({
                        helper: sortable_tables.fixWidthHelper,
                        placeholder : "state-highlight",
                        update  : function (event, ui) {
                            var page_id_array = new Array();
                            var page_class_array = new Array();
                            $('#fixed-drop-table tr').each(function () {
                                page_id_array.push($(this).attr("id"));
                                page_class_array.push($(this).attr("data-class"));
                            });
                            $.ajax({
                                url: location.pathname + location.search,
                                method:"POST",
                                data:{page_id_array:page_id_array,page_class_array:page_class_array},
                                success:function (data) {
                                    if ( columnIndex ) {
                                        $tds = $tbody.find('tr > td:nth-child('+columnIndex+')');
                                        var num_rows = $tds.length;
                                        if ( num_rows > 0 ) {
                                            var currentSortValue = +window.util.extractUrlParam('sk') || 0;
                                            if ( currentSortValue !== 1 ) {
                                                num_rows = -1;
                                            }
                                            $tds.each(function (index, td) {
                                                index = Math.abs(num_rows - index);
                                                $(td).text(index);
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }).disableSelection();
                },

                fixWidthHelper: function (e, ui) {
                    ui.children().each(function () {
                        $(this).width($(this).width());
                    });
                    return ui;
                }
            };
            sortable_tables.sorting_field_table();
        }
    })();

    (function () {
        $('#update').click(function () {
            var success = true;
            var listDev = new Array();
            $('.quantity').each(function() {
                if (!$(this).val()) {
                    success = false;
                    return false;
                }
            });

            $('.two_devname').each(function() {
                var devName = $(this).find('option:selected').val();
                if ($.inArray(devName, listDev) != -1) {
                    success = false;
                    return false;
                }
                listDev.push(devName);
            });

            if (success) {
                toastr.success("記録を更新しました！");
            }
        })
    })();

    (function () {
        var max_height = 300;
        var $header = $('tbody.scrollable');
        var height = $header.height();
        if ( height > max_height ) {
            $header.height(max_height);
        }
    })();

    (function () {
        var ALLOWED_KEYS = [
            35,  // Home
            36,  // End
            37,  // Arrow left
            38,  // Arrow up
            39,  // Arrow right
            40,  // Arrow down,
            16,  // Shift
            17,  // Ctrl,
            8,   // Backspace,
            9,   //.Tab
            45,  // Insert,
            46,  // Delete,
            144, // Numlock
            20,  // Capslock
            13,  // Enter,
            18,  // Alt
        ];
        function isShortcutKeys(e)
        {
            var result = false;
            var allowedKeys = ['a', 'c', 'x', 'v', 'z', 'y'];
            if ( e.ctrlKey == true ) {
                if ( allowedKeys.indexOf(e.key.toLowerCase()) > -1 ) {
                    result = true;
                }
            }
            return result;
        };
        function byteTrim(string, maxbytes)
        {
            var strlength = string.length;
            var totalBytes = 0;
            var finalStr  = '';
            for (var i = 0; i < strlength; ++i) {
                var char = string.charAt(i);
                var bytes = utf8.encode(char).length;
                if ( totalBytes + bytes <= maxbytes ) {
                    totalBytes += bytes;
                    finalStr += char;
                } else {
                    break;
                }
            }
            return finalStr;
        };

        function appendText(input, string)
        {
            string || (string = '');
            var startPos = input.selectionStart;
            var endPos = input.selectionEnd;
            var value = input.value;
            var chunkStart = value.substring(0, startPos);
            var chunkEnd = value.substring(endPos, value.length);
            var value = chunkStart + string + chunkEnd;
            return value;
        };
        $('input[maxbytes], textarea[maxbytes]').on('keydown paste blur', function (e) {
            var value = this.value;
            var clipboard = '';
            var startPos = this.selectionStart;
            var endPos = this.selectionEnd;
            var isShortKeys = isShortcutKeys(e);
            if ( e.type === 'paste' ) {
                e.preventDefault();
                clipboard = window.util.readFromClipboard(e);
                value = appendText(this, clipboard);
            }
            var maxbytes = +this.getAttribute('maxbytes');
            var bytes = utf8.encode(value).length;
            if (bytes >= maxbytes) {
                var keyCode = e.which || e.keyCode;
                if (!isShortKeys && startPos == endPos && ALLOWED_KEYS.indexOf(keyCode) === - 1) {
                    e.preventDefault();
                }

                if (bytes > maxbytes) {
                    var newValue = byteTrim(value, maxbytes);
                    this.value = newValue;
                    this.setSelectionRange(startPos, endPos);
                } else {
                    this.value = value;
                }
            } else {
                this.value = value;
            }
            if (clipboard) {
                startPos = endPos = startPos + clipboard.length
                this.setSelectionRange(startPos, endPos);
            }
        });

        $('input.no-typing').on('keydown', function(e){
            var keyCode = e.which || e.keyCode;
            if (!isShortcutKeys(e) && ALLOWED_KEYS.indexOf(keyCode) === - 1) {
                e.preventDefault();
            }
        });

        $('.date-input').bind('keyup blur',function(){
            var node = $(this);
            node.val(node.val().replace(/[^0-9-/]/g,'') ); }
        );
    })();

    (function(){
        $('select[name="timezone"]').change(function(){
            var timezone = $(this).val();
            var timezoneid = $(this).find("option:selected").attr("timezoneid");
            $('input[name="timezone"]').val(timezone);
            $('input[name="timezoneid"]').val(timezoneid);
        });
        function loadDepts() {
            return window.util.ajax('/api/dept.php');
        };

        function loadProjs(dept) {
            return window.util.ajax('/api/proj.php', {
                dept : dept
            });
        };

        function generateProjsTemplate(projs, options) {
            options || (options = {})
            var selected = options.selected;
            var selname = options.name || 'proj';
            projs && projs.length || (projs = []);

            var $optionDefault =
                $('<option>').attr('value', '').text('--- プロジェクトを選択 ---');
            var $select = $('<select>')
                .attr('name', selname)
                .addClass('form-control')
                .append($optionDefault);
            if (typeof options.onChange === 'function') {
                $select.change(options.onChange);
            }
            projs.forEach(function(proj){
                var text = proj.pname;
                if (proj.pcode) {
                    text += ' (' +proj.pcode+ ')';
                }
                var $option =
                    $('<option>')
                        .attr('value', proj.pcode)
                        .text(text)
                if (selected && selected == proj.pcode) {
                    $option.attr('selected', 'selected');
                }
                $select.append($option);
            });
            return $select;
        };

        function generateDeptsTemplate(depts, selected, onchangeCallback) {
            depts && depts.length || (depts = [])
            var $optionDefault =
                $('<option>').attr('value', '').text('--- グループを選択 ---');
            var $select =
                $('<select>')
                    .attr('name', 'dept')
                    .addClass('form-control')
                    .append($optionDefault);
            if (typeof onchangeCallback === 'function') {
                $select.change(function(){
                    onchangeCallback(this.value);
                })
            }
            depts.forEach(function(dept){
                var $option =
                    $('<option>')
                        .attr('value', dept.dept)
                        .text(dept.deptname)
                if (selected && selected == dept.dept) {
                    $option.attr('selected', 'selected');
                }
                $select.append($option);
            });
            return $select;
        };

        var $sel_depts = $('[data-dropdown-source="dept"]');
        if ($sel_depts.length) {
            loadDepts().then(function(depts){
                $sel_depts.each(function(i, el){
                    var $sel_dept = $(el);
                    var targetID = $sel_dept.data('dropdownTarget');
                    var onchange = $sel_dept.data('onchange');
                    var onchangeCallback = undefined;

                    var $sel_proj = $(targetID+'[data-dropdown-source="proj"]');
                    var selectedDept = $sel_dept.data('selected');
                    var disabled = $sel_dept.data('disabled');
                    var selectedProj = undefined;

                    if (onchange) {
                        onchange = window.util.stringToFunction(onchange);
                    }

                    if ($sel_proj.length > 0) {
                        var selectedProj = $sel_proj.data('selected');
                        onchangeCallback = function(dept) {
                            if (dept) {
                                loadProjs(dept).then(function(projs){
                                    var $projsTemplate = generateProjsTemplate(projs, {
                                        selected : selectedProj
                                    });
                                    $sel_proj.empty().append($projsTemplate);
                                    selectedProj = undefined;
                                });
                            } else {
                                var $projsTemplate = generateProjsTemplate();
                                $sel_proj.empty().append($projsTemplate);
                                selectedProj = undefined;
                            }
                            if (typeof onchange === 'function') {
                                onchange(dept);
                            }
                        };
                        // initialize project template
                        var $projsTemplate =  generateProjsTemplate();
                        if (selectedProj) {
                            $projsTemplate.attr('disabled', 'disabled');
                        }
                        $projsTemplate.appendTo($sel_proj);
                    } else {
                        onchangeCallback = onchange;
                    }

                    var $deptsTemplate = generateDeptsTemplate(depts, selectedDept, onchangeCallback);
                    if (disabled) {
                        $deptsTemplate.attr('disabled', 'disabled');
                    }
                    $sel_dept.empty().append($deptsTemplate);
                    if (selectedDept) {
                        if (typeof onchangeCallback === 'function') {
                            onchangeCallback(selectedDept);
                        }
                    }
                });
            });
        }
        window.generateProjsTemplate = generateProjsTemplate;
    })();
});

function changeValueCheckbox(element)
{
    if (element.checked) {
        element.value='1';
    } else {
        element.value='0';
    }
}

function changeValueCheckboxEnc(element)
{
    if (element.checked) {
        element.value='1';
    } else {
        element.value='0';
    }
}
function isNumeric(value) {
    return /^\d+$/.test(value);
}
