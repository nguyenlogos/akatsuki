;(function($){
    'user strict';
    var units = {
        minute : {
            range : _.range(0,60),
            jptext : '分',
        },
        hour : {
            range : _.range(0,24),
            jptext : '時',
        },
        day : {
            range : _.range(1, 32),
            jptext : '日',
        }
    };
    var dtpicker = {};
    for (const unit in units) {
        var $wrapper = $('<div>');
        var $options = $('<select class="form-control d-inline-block w_65">').attr('id', 'dtp-'+unit);
        var range = units[unit].range;
        var jptext = units[unit].jptext;
        range.forEach(function(value){
            var $option = $('<option>').val(value).text(value);
            $options.append($option);
        });
        $wrapper.append($options);
        $wrapper.append(
            $('<div class="d-inline-block ml-1">').text(jptext)
        );
        dtpicker[unit] = {
            '$options' : $wrapper,
            'text' : jptext
        };
    }
    window.dtpicker = dtpicker;
})(jQuery);