
$('select.time_list').on('change',function(){
    var parentLine = $(this).closest('.list_hdd');
    var timeType = '.' + $(this).val();
    parentLine.find('.cron_display').hide();
    parentLine.find(timeType).show();
});

$(".my_section").ready( function () {
  for (var i = 0; i < 60; i++) {
    if (i < 24) {
      $(".hours").each(function () {
        $(this).append(timeSelectOption(i));
      });
    }
    $(".minutes").each(function () {
      $(this).append(timeSelectOption(i));
    });
  }
})

function displayTimeUnit(unit)
{
  if (unit.toString().length == 1) {
    return + unit;
  }
  return unit;
}

function timeSelectOption(i)
{
  return "<option value='" + i + "'>" + displayTimeUnit(i) + "</option>";
}

$("#cron_week").ready( function () {
  var days = [
    {text: "None", val: ""},
    {text: "月", val: "0"},
    {text: "火", val: "1"},
    {text: "水", val: "2"},
    {text: "木", val: "3"},
    {text: "金", val: "4"},
    {text: "土", val: "5"},
    {text: "日", val: "6"}
  ];
  $(".week-days").each(function () {
    fillOptions(this, days);
  });
})

function fillOptions(elements, options) {
  for (var i = 0; i < options.length; i++)
    $(elements).append("<option value='" + options[i].val + "'>" + options[i].text + "</option>");
}

$(document).ready(function () {
  var rowCount = $('.mgT .list_hdd').length;
  $('#count_unmount').val(rowCount);
});