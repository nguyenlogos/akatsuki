$(document).ready(function () {
    $('.btn-validate').click(function (event) {
        //Add validation rule for dynamically generated Size (GiB) fields
        $('.quantity').each(function () {
            $(this).rules(
                "add",
                {
                    required: true,
                    digits: true,
                    messages: {
                        required: "サイズ(GiB) が入力されていません",
                    }
                }
            );
        });
        //Add validation rule for dynamically generated IOPS fields
        $('.data_check').each(function () {
            $(this).rules(
                "add",
                {
                    required: true,
                    digits: true,
                    messages: {
                        required: "IOPS が入力されていません",
                    }
                }
            );
        });
    });
    $("#orderform").validate();

    //check select option value Device type
    $('.target').each(function () {
        $(this).on('change',function () {
            var value = $(this).find(":selected").val();
            if (value === 'io1') {
                $(this).parents('.seson').find('input[data-hidden]').removeClass('hidden_iops');
            } else {
                $(this).parents('.seson').find('input[data-hidden]').addClass('hidden_iops');
            }

        });
    })

    function forceNumeric()
    {
        var $input = $(this);
        $input.val($input.val().replace(/[^\d]+/g,''));
    }
    $('form').on('propertychange input', 'input[type="number"]', forceNumeric);
    (function(){
        $.validator.addMethod('phonecheck', function(value, element) {
            return this.optional(element) || /^(\(\d{1,6}\)|\d{1,6})[-]{1}(\(\d{1,6}\)|\d{1,6})[-]{1}(\(\d{1,6}\)|\d{1,6})$/i.test(value);
        }, '電話番号が間違っています。');

        $('#reg_form').validate({
            ignore: ".ignore",
            rules: {
                name: {
                    required: true,
                    maxlength: 64
                },
                tel: {
                    required: true,
                    maxlength: 64,
                    phonecheck: true
                },
                company: {
                    required: true,
                    maxlength: 128
                },
                dept: {
                    maxlength: 64
                },
                position: {
                    maxlength: 64
                },
                email: {
                    required: true,
                    maxlength: 64,
                    email: true,
                },
                email2: {
                    required: true,
                    equalTo: "#email",
                },
                pass: {
                    required: true
                },
                pass2: {
                    required: true,
                    equalTo: "#password",
                },
                p1: {
                    required: true,
                    maxlength: 64,
                },
                p2: {
                    required: true,
                    maxlength: 64,
                },
                p3: {
                    required: true,
                    maxlength: 64,
                },
                agree: {
                    required: true,
                },
                group: {
                    required: true,
                },
                hiddenRecaptcha: {
                    required: function () {
                        if (grecaptcha.getResponse() == '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            },
            messages: {
                // name: "名前は必須です。",
                name: {
                    required: "氏名は省略できません。",
                    maxlength: "64 文字まで が入力されていません。",
                },
                company: {
                    required: "会社名は省略できません。",
                    maxlength: "128 文字まで が入力されていません。",
                },
                dept: {

                    maxlength: "64 文字まで が入力されていません。",
                },
                position: {

                    maxlength: "64 文字まで が入力されていません。",
                },
                tel: {
                    required:"電話番号は必須です。",
                    maxlength: "64 文字まで が入力されていません。",
                },
                // email: "メールアドレスの形式が不正です。",
                email: {
                    required: "メールアドレスは省略できません。",
                    maxlength: "64 文字まで が入力されていません。",
                },
                email2: {
                    required: "再入力されたメールアドレスが異なります。",
                    maxlength: "64 文字まで が入力されていません。",
                    equalTo: "同じメールアドレスをもう一度入力してください。",
                },
                pass: {
                    required: "パスワードは省略できません。",
                    minlength: "パスワードは６文字以上で入力してください。"
                },
                pass2: {
                    required: "パスワード(再入力)は省略できません。",
                    equalTo: "同じパスワードをもう一度入力してください。",
                },
                p1: {
                    required: "パスワードは必須です。",
                    maxlength: "64 文字まで が入力されていません。",
                },
                p2: {
                    required: "パスワードは必須です。",
                    maxlength: "64 文字まで が入力されていません。",
                },
                p3: {
                    required: "パスワード(再入力)は必須です。",
                    equalTo: "同じパスワードをもう一度入力してください。",
                },
                notif_title: {
                    required: "が入力されていません。",
                },
                agree: "利用規約への同意は必須です。",
                hiddenRecaptcha: "キャプチャは必須です。",
                group: "所属グループは省略できません。",
            }
        });
    })();
});

// //check number phone
// $(window).load(function () {
//     var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-######"}];
//     $('.textbox').inputmask({
//         mask: phones,
//         greedy: false,
//         definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
// });

//price format
(function ($) {
  /******************
   * Unmask Function *
   *******************/
    $.fn.unmask = function () {

        var field = $(this).val();
        var result = "";

        for (var f in field) {
            if (!isNaN(field[f]) || field[f] == "-") {
                result += field[f];
            }
        }

        return result;
    };

    $.fn.cidr = function(cidr) {
        var g_cidr = cidr + '';
        function ip2int(ip) {
            var ip = ip.match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
            if(ip) {
                return (+ip[1]<<24) + (+ip[2]<<16) + (+ip[3]<<8) + (+ip[4]);
            }
            return 0;
        }

        function int2ip(int) {
            var part1 = int & 255;
            var part2 = ((int >> 8) & 255);
            var part3 = ((int >> 16) & 255);
            var part4 = ((int >> 24) & 255);

            return part4 + "." + part3 + "." + part2 + "." + part1;
        }

        function getbase(ip, subnet) {
            return int2ip(ip2int(ip) & ip2int(unmask(subnet)));
        }

        function unmask(subnet) {
            return int2ip(ip2int('255.255.255.255') << (32-subnet));
        }

        function validate(cidr) {
            var matches = cidr.match(/^((?:[0-9]{1,3}\.){3}[0-9]{1,3})(?:\/([0-9]|[1-2][0-9]|3[0-2]))?$/);
            if (!matches) {
                return false;
            }
            if (!matches) {
                return false;
            }
            var ip = matches[1],
                mask = parseInt(matches[2]);
            if (isNaN(mask)) {
                mask = 32;
            }
            if (mask < 0 || mask > 32) {
                return false;
            }
            var isValid = true;
            ip.split('.').forEach(v => {
                v = parseInt(v);
                if (isValid && (v < 0 || v > 255)) {
                    isValid = false;
                }
            });
            if (isValid) {
                return {
                    ip : ip,
                    mask : mask
                };
            }

            return false;
        }
        return {
            check : function () {
                var info = validate(g_cidr);
                if (!info) {
                    return false;
                }
                if (info.mask === 0 && info.ip !== '0.0.0.0') {
                    return false;
                }
                var network = getbase(info.ip, info.mask)

                return network === info.ip;
            },
        }
    };

    /*
    * Translated default messages for the jQuery validation plugin.
    * Locale: JA
    * https://github.com/satooshi/jquery-validation-ja-util/tree/master/jquery-validation/localization
    */
    $.extend($.validator.messages, {
        required: "このフィールドは必須です。",
        remote: "このフィールドを修正してください。",
        email: "有効なEメールアドレスを入力してください。",
        url: "有効なURLを入力してください。",
        date: "有効な日付を入力してください。",
        dateISO: "有効な日付（ISO）を入力してください。",
        number: "有効な数字を入力してください。",
        digits: "数字のみを入力してください。",
        creditcard: "有効なクレジットカード番号を入力してください。",
        equalTo: "同じ値をもう一度入力してください。",
        extension: "有効な拡張子を含む値を入力してください。",
        maxlength: $.validator.format("{0} 文字以内で入力してください。"),
        minlength: $.validator.format("{0} 文字以上で入力してください。"),
        rangelength: $.validator.format("{0} 文字から {1} 文字までの値を入力してください。"),
        range: $.validator.format("{0} から {1} までの値を入力してください。"),
        step: $.validator.format("{0} の倍数を入力してください。"),
        max: $.validator.format("{0} 以下の値を入力してください。"),
        min: $.validator.format("{0} 以上の値を入力してください。"),

        maxWords: $.validator.format("{0}単語以内で入力してください。"),
        minWords: $.validator.format("{0}単語以上で入力してください。"),
        rangeWords: $.validator.format("{0}単語から{1}単語以内で入力してください。"),
        letterswithbasicpunc: "Letters or punctuation only please",
        alphanumeric: "英数字またはアンダースコアで入力してください。",
        lettersonly: "英字で入力してください。",
        nowhitespace: "空白は入力できません。",
        //ziprange: "Your ZIP-code must be in the range 902xx-xxxx to 905-xx-xxxx",
        //zipcodeUS: "The specified US ZIP Code is invalid",
        integer: "整数で入力してください。",
        //vinUS: "The specified vehicle identification number (VIN) is invalid.",
        //dateITA: "Please enter a correct date",
        //dateNL: "Vul hier een geldige datum in.",
        time: "00:00から23:59までの時間を入力してください。",
        time12h: "00:00 amから12:00 pmまでの時間を入力してください。",
        //phoneUS: "Please specify a valid phone number",
        //phoneUK: 'Please specify a valid phone number',
        //mobileUK: 'Please specify a valid mobile number',
        //phonesUK: 'Please specify a valid uk phone number',
        //postcodeUK: 'Please specify a valid postcode',
        strippedminlength: $.validator.format("{0}文字以上で入力してください。"),
        email2: $.validator.messages.email,
        url2: $.validator.messages.url,
        creditcardtypes: "有効なクレジットカード番号を入力してください。",
        ipv4: "有効なIP v4アドレスを入力してください。",
        ipv6: "有効なIP v6アドレスを入力してください。",
        pattern: "不正な形式です。",
        require_from_group: $.validator.format("これらの項目は{0}文字以上で入力してください。"),
        skip_or_fill_minimum: $.validator.format("これらの項目は空欄または{0}文字以上で入力してください。"),
        accept: $.validator.format("有効なmimeタイプのファイルを指定してください。"),
        extension: $.validator.format("有効な拡張子のファイルを指定してください。"),
        ip_range_address: "Please input ip by format x.x.x.x/y (x = 0~255, y = 0~32)",
        port_range: "Please input correct port range",
        unique: "Please input unique security group rules type",
        compare_size: $.validator.format("EBS is smaller than AMI")
    });
    $.validator.addClassRules({
        phoneCheck: {
            phonecheck: true
        }
    });

    $.validator.addMethod("portrange", function(value, element) {
        if (this.optional(element) || element.getAttribute('readonly')) {
            return true;
        }
        if (value.match(/[^\d-]/)) {
            return false;
        }
        var parts = value.split('-').map(v => parseInt(v));
        var len = parts.length;
        if (len > 2) {
            return false;
        }
        if (len === 2) {
            if (parts[0] > parts[1]) {
                return false;
            }
        }
        var isValid =  true;
        parts.forEach(port => {
            if (isNaN(port) || port < 0 || port > 65535) {
                isValid = false;
                return;
            }
        });

        return isValid;
    }, "Invalid port range (must be from 0-65535)");

    $.validator.addMethod('cidr', function(value, element) {
        value = value.trim();
        var isRequired = $(element).data('required');
        if (!value && isRequired) {
            return false;
        }
        if (this.optional(element)) {
            return true;
        }

        return $(this).cidr(value).check();
    }, "Invalid IP/CIDR format");
    $.validator.addClassRules({
        'dhxcombo_input' : {
            required: true
        }
    });
})(jQuery);
