<?php

use App\Migration;

class DropTableCost extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = 'DROP TABLE "cost";';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'ALTER TABLE "cost";';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
