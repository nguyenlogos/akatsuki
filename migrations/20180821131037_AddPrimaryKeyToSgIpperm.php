<?php

use App\Migration;

class AddPrimaryKeyToSgIpperm extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg_ipperm"
            ADD "tbl_id" serial NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg_ipperm"
            DROP "tbl_id";
        ';
        $this->exec($sql);
    }
}
