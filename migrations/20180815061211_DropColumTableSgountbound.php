<?php

use App\Migration;

class DropColumTableSgountbound extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg_outbound"
            DROP "outb_port_range",
            ADD "outb_formport_range" character varying(64) NULL,
            ADD "outb_toport_range" character varying(64) NULL
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg_outbound"
            DROP "outb_port_range",
            DROP "outb_formport_range",
            DROP "outb_toport_range";
        ';
        $this->exec($sql);
    }
}
