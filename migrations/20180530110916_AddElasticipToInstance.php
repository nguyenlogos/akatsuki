<?php

use App\Migration;

class AddElasticipToInstance extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "instance"
            ADD "elasticip" character varying(64) NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "instance"
            DROP "elasticip";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
