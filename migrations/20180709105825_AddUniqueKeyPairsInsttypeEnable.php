<?php

use App\Migration;

class AddUniqueKeyPairsInsttypeEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "insttype_enable"
            ADD UNIQUE (cid, name)
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "insttype_enable"
            DROP CONSTRAINT "insttype_enable_cid_name_key";
        ';
        $this->exec($sql);
    }
}
