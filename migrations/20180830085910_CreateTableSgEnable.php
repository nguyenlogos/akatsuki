<?php

use App\Migration;

class CreateTableSgEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "sg_enable" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "sg_id" character varying(128) NOT NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "sg_enable";';
        $this->exec($sql);
    }
}
