<?php

use App\Migration;

class CreateTableCwRules extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "cw_rules" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "name" character varying NOT NULL,
                "expr" character varying(512) NOT NULL,
                "region" character varying(32) NOT NULL,
                "state" smallint NOT NULL DEFAULT 1,
                "notes" text NULL,
                "status" integer NULL DEFAULT 0
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "cw_rules";';
        $this->exec($sql);
    }
}