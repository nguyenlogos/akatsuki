<?php

use App\Migration;

class AddColumIdsadminEmp extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "emp" ADD COLUMN "idsadmin" integer NOT NULL DEFAULT 0;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "emp"
            DROP "idsadmin";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
