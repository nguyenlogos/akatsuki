<?php

use App\Migration;

class AddEmpidAndNoteToInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ADD "empid" integer NULL,
            ADD "note" character varying(512) NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "inst_req"
            DROP "empid",
            DROP "note";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
