<?php

use App\Migration;

class CreateTableInsttypePricing extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE insttype_pricing (
                id integer,
                cid integer,
                name character varying(128),
                region character varying(128),
                price numeric
            );
            ALTER TABLE "insttype_pricing"
            ADD CONSTRAINT "insttype_pricing_cid_name_region" UNIQUE ("cid", "name", "region");
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "insttype_pricing"';
        $this->exec($sql);
    }
}
