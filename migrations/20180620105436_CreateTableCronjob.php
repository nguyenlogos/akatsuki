<?php

use App\Migration;

class CreateTableCronjob extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "tbl_lambda" (
                "id" serial NOT NULL,
                "cid" integer NULL,
                "pcode" character varying(256) NOT NULL,
                "inst_id" character varying(128) NOT NULL,
                "volumn_device" character varying(128) NOT NULL,
                "volumn_volid" character varying(128) NOT NULL,
                "volumn_size" integer NOT NULL,
                "volumn_voltype" character varying(32) NOT NULL,
                "time_gener" character varying(64) NOT NULL,
                "hours_gener" integer NULL,
                "minute_gener" integer NULL,
                "dai_gener" integer NULL,
                "week_gener" character varying(32) NOT NULL,
                "day_gener" integer NULL,
                "month_gener" integer NULL,
                "num_gener" integer NULL,
                "create_time" timestamp NULL,
                "next_time" timestamp NULL
            );
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "tbl_lambda";';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
