<?php

use App\Migration;

class AddIdForObj extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "obj"
                ADD "id" serial NOT NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "obj"
                DROP "id";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
