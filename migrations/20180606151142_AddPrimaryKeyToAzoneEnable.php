<?php

use App\Migration;

class AddPrimaryKeyToAzoneEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "azone_enable"
            ADD "id" serial NOT NULL,
            ADD PRIMARY KEY (id);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "azone_enable"
            DROP "id";
        ';
        $this->exec($sql);
    }
}
