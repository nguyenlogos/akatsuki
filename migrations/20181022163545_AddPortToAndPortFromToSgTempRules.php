<?php

use App\Migration;

class AddPortToAndPortFromToSgTempRules extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg_temp_rules"
            ADD "pfrom" integer NULL,
            ADD "pto" integer NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg_temp_rules"
            DROP "pfrom",
            DROP "pto";
        ';
        $this->exec($sql);
    }
}
