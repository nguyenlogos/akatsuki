<?php

use App\Migration;

class AddInstReqIdToInstance extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "instance"
            ADD "inst_req_id" integer NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "instance"
            DROP "inst_req_id";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
