<?php

use App\Migration;

class CreateTableEbsReqDetail extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "ebs_req_detail" (
                "id" serial NOT NULL,
                "ebs_req_id" integer NOT NULL,
                "volume_id" character varying(32) NOT NULL,
                "volume_zone" character varying(32) NOT NULL,
                "volume_type" character varying(16) NOT NULL,
                "volume_size" integer NOT NULL,
                "volume_iops" integer NULL DEFAULT 0,
                "device_name" character varying(16) NOT NULL,
                "state" character varying(16) NOT NULL,
                "attached" integer NULL DEFAULT 0,
                "autoremove" integer NULL DEFAULT 0,
                "change_type" character varying(16) NOT NULL,
                "status" integer NOT NULL DEFAULT 0
            )
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "ebs_req_detail";';
        $this->exec($sql);
    }
}
