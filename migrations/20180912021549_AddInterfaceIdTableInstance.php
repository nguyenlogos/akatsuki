<?php

use App\Migration;

class AddInterfaceIdTableInstance extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "instance"
            ADD "interface_id" character varying(64) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'ALTER TABLE "instance" DROP "interface_id";';
        $this->exec($sql);
    }
}
