<?php

use App\Migration;

class DropDefaultNullProj extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "proj"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" SET NOT NULL,
            ALTER "pcode" DROP DEFAULT,
            ALTER "pcode" SET NOT NULL,
            ALTER "regdate" DROP DEFAULT,
            ALTER "regdate" SET NOT NULL,
            ALTER "status" SET DEFAULT 0,
            ALTER "status" SET NOT NULL,
            ALTER "dept" DROP DEFAULT,
            ALTER "dept" SET NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "proj"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" DROP NOT NULL,
            ALTER "pcode" DROP DEFAULT,
            ALTER "pcode" DROP NOT NULL,
            ALTER "regdate" DROP DEFAULT,
            ALTER "regdate" DROP NOT NULL,
            ALTER "status" DROP DEFAULT,
            ALTER "status" DROP NOT NULL,
            ALTER "dept" DROP DEFAULT,
            ALTER "dept" DROP NOT NULL;
        ';
        $this->exec($sql);
    }
}
