<?php

use App\Migration;

class CreateTableInbountSecurityGroup extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "sg_inbound" (
                "id" serial NOT NULL,
                "id_security_group"  integer NOT NULL,
                "inb_type" character varying(255) NOT NULL,
                "inb_protocol" character varying(64) NOT NULL,
                "inb_port_range" character varying(64) NOT NULL,
                "inb_cidr_ip" character varying(64) NOT NULL,
                "inb_source" character varying(32) NOT NULL,
                "inb_description" character varying(1024) NOT NULL,
                "create_time" timestamp
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "security_group";';
        $this->exec($sql);
    }
}
