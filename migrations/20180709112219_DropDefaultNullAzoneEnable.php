<?php

use App\Migration;

class DropDefaultNullAzoneEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "azone_enable"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" SET NOT NULL,
            ALTER "name" DROP DEFAULT,
            ALTER "name" SET NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "azone_enable"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" DROP NOT NULL,
            ALTER "name" DROP DEFAULT,
            ALTER "name" DROP NOT NULL;
        ';
        $this->exec($sql);
    }
}
