<?php

use App\Migration;

class AddColumTableSG extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg"
            ADD "description" character varying NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg"
            DROP "description";
        ';
        $this->exec($sql);
    }
}
