<?php

use App\Migration;

class AddTypeToSgIpperm extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg_ipperm"
            ADD "type" character varying(16) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg_ipperm"
            DROP "type";
        ';
        $this->exec($sql);
    }
}
