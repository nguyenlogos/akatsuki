<?php

use App\Migration;

class CreateTableExchangeRate extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "tbl_exchangerate" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "empid" integer NOT NULL,
                "email" character varying(64) NULL,
                "num_rate" integer NOT NULL,
                "code_usd" character varying(16) NULL,
                "code_jpy" character varying(16) NULL,
                "count_rate" numeric NULL,
                "status" integer NOT NULL DEFAULT 0,
                "date_created" date NOT NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            DROP TABLE "tbl_exchangerate";
        ';
        $this->exec($sql);
    }
}
