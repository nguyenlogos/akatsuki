<?php

use App\Migration;

class AllowNullForSecurityGroupsInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ALTER "security_groups" DROP NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ALTER "security_groups" SET NOT NULL;
        ';
        $this->exec($sql);
    }
}