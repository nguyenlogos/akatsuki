<?php

use App\Migration;

class AddTableEmp extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
                ALTER TABLE "emp"
                ADD "login_initial" integer NULL DEFAULT 0,
                ADD "number_lock" integer NULL DEFAULT 0,
                ADD "email_lock" integer NULL DEFAULT 0,
                ADD "email_unlock" integer NULL DEFAULT 0;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
                ALTER TABLE "emp"
                DROP "login_initial",
                DROP "number_lock",
                DROP "email_lock",
                DROP "email_unlock";
        ';
        $this->exec($sql);
    }
}
