<?php

use App\Migration;

class AddTimezoneToConfigs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "configs"
            ADD "timezone" character varying NULL,
            ADD "timezoneid" smallint NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "configs"
            DROP "timezone",
            DROP "timezoneid";
        ';
        $this->exec($sql);
    }
}
