<?php

use App\Migration;

class SetPrimaryKeyForInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ADD PRIMARY KEY (id);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "inst_req"
            DROP constraint inst_req_pkey;
        ';
        $this->exec($sql);
    }
}
