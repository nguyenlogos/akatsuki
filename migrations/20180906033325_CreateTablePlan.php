<?php

use App\Migration;

class CreateTablePlan extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "plan" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "plan" character varying(10) NOT NULL,
                "startdate" timestamp,
                "enddate" timestamp,
                "startuid" character varying(64) NOT NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "plan";';
        $this->exec($sql);
    }
}
