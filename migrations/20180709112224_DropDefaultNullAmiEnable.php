<?php

use App\Migration;

class DropDefaultNullAmiEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "ami_enable"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" SET NOT NULL,
            ALTER "imageid" DROP DEFAULT,
            ALTER "imageid" SET NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "ami_enable"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" DROP NOT NULL,
            ALTER "imageid" DROP DEFAULT,
            ALTER "imageid" DROP NOT NULL;
        ';
        $this->exec($sql);
    }
}
