<?php

use App\Migration;

class AddColumTableIvoice extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE invoice
                ADD idsfee_usd NUMERIC,
                ADD idsfee_jpy NUMERIC;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE invoice
                DROP idsfee_usd,
                DROP idsfee_jpy;
        ';
        $this->exec($sql);
    }
}
