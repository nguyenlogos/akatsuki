<?php

use App\Migration;

class AddRegionToInsttypeEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
        ALTER TABLE "insttype_enable"
        ADD "region" character varying(128) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "insttype_enable"
            DROP "region";
        ';
        $this->exec($sql);
    }
}
