<?php

use App\Migration;

class RemoveUnusedColumnsFromUsers extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "users"
            DROP "costcalc",
            DROP "costid",
            DROP "costtype",
            DROP "coststatus",
            DROP "costmsg";
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
