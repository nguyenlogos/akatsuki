<?php

use App\Migration;

class AddPCodeToCostService extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "cost_service"
            ADD "dept" integer NULL,
            ADD "pcode" character varying(128) NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "cost_service"
            DROP "dept",
            DROP "pcode";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
