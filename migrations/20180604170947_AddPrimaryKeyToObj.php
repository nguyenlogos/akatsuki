<?php

use App\Migration;

class AddPrimaryKeyToObj extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "obj"
            ADD PRIMARY KEY (id);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "obj"
            DROP constraint obj_pkey;
        ';
        $this->exec($sql);
    }
}
