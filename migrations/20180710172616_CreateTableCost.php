<?php

use App\Migration;

class CreateTableCost extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE cost (
                cid integer,
                lineitemtype character varying(64),
                usagestartdate timestamp without time zone,
                usageenddate timestamp without time zone,
                productcode character varying(64),
                usagetype1 character varying(64),
                operation1 character varying(64),
                resourceid character varying(128),
                usageamount numeric,
                currencycode character varying(64),
                unblendedcost numeric,
                blendedcost numeric,
                lineitemdescription character varying(512),
                instancetype character varying(64),
                location character varying(128),
                memory character varying(64),
                networkperformance character varying(64),
                normalizationsizefactor numeric,
                operatingsystem character varying(64),
                operation2 character varying(64),
                physicalprocessor character varying(64),
                preinstalledsw character varying(256),
                processorarchitecture character varying(64),
                processorfeatures character varying(64),
                region character varying(64),
                servicename character varying(128),
                transfertype character varying(128),
                usagetype2 character varying(128),
                publicondemandcost numeric,
                publicondemandrate numeric,
                term character varying(64),
                unit character varying(128),
                usageaccountid character varying(64),
                servicecode character varying(128),
                productfamily character varying(128),
                loaddate timestamp
            );
            CREATE INDEX idx_cost_1 ON cost USING btree (cid);
            CREATE INDEX idx_cost_2 ON cost USING btree (usageaccountid);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "cost";';
        $this->exec($sql);
    }
}
