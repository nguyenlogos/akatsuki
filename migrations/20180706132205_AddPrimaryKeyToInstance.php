<?php

use App\Migration;

class AddPrimaryKeyToInstance extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "instance"
            ADD "tbl_id" serial NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "instance"
            DROP "tbl_id";
        ';
        $this->exec($sql);
    }
}
