<?php

use App\Migration;

class CreateTableTblLogs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "tbl_logs" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "empid" integer NOT NULL,
                "category" character varying(16) NOT NULL,
                "action" character varying(16) NULL,
                "description" text NOT NULL,
                "error_flag" smallint NOT NULL DEFAULT 0,
                "date_created" timestamp NOT NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            DROP TABLE "tbl_logs";
        ';
        $this->exec($sql);
    }
}