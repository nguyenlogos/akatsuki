<?php

use App\Migration;

class CreateTableCwLogs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "cw_logs" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "stream" character varying(64) NOT NULL,
                "stream_time" timestamp NOT NULL,
                "event" text NOT NULL,
                "event_time" timestamp NOT NULL,
                "date_created" timestamp NOT NULL,
                "error_flag" smallint NULL DEFAULT 0,
                "description" text NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "cw_logs";';
        $this->exec($sql);
    }
}
