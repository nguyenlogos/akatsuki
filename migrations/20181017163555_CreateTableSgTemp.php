<?php

use App\Migration;

class CreateTableSgTemp extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql  = '
            CREATE TABLE "sg_temp" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "sg_id" character varying(32) NULL,
                "sg_vpc" character varying(32) NULL,
                "sg_name" character varying(128) NULL,
                "sg_desc" character varying(128) NULL,
                "inst_req_id" integer NOT NULL,
                "is_created" smallint NULL DEFAULT 0
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            DROP TABLE "sg_temp";
        ';
        $this->exec($sql);
    }
}
