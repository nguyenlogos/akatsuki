<?php

use App\Migration;

class DropDefaultNullObj extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "obj"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" SET NOT NULL,
            ALTER "name" DROP DEFAULT,
            ALTER "name" SET NOT NULL,
            ALTER "ipaddr" DROP DEFAULT,
            ALTER "ipaddr" SET NOT NULL,
            ALTER "submask" DROP DEFAULT,
            ALTER "submask" SET NOT NULL;

        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "obj"
            ALTER "cid" DROP DEFAULT,
            ALTER "cid" DROP NOT NULL,
            ALTER "name" DROP DEFAULT,
            ALTER "name" DROP NOT NULL,
            ALTER "ipaddr" DROP DEFAULT,
            ALTER "ipaddr" DROP NOT NULL,
            ALTER "submask" DROP DEFAULT,
            ALTER "submask" DROP NOT NULL;
        ';
        $this->exec($sql);
    }
}
