<?php

use App\Migration;

class AddExpiredNotifiedToInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ADD "expired_notified" smallint NULL DEFAULT 0;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "inst_req"
            DROP "expired_notified";
        ';
        $this->exec($sql);
    }
}
