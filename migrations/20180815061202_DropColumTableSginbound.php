<?php

use App\Migration;

class DropColumTableSginbound extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg_inbound"
            DROP "inb_port_range",
            ADD "inb_formport_range" character varying(64) NULL,
            ADD "inb_toport_range" character varying(64) NULL
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg_inbound"
            DROP "inb_port_range",
            DROP "inb_formport_range",
            DROP "inb_toport_range";
        ';
        $this->exec($sql);
    }
}
