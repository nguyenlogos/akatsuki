<?php

use App\Migration;

class CreateTableSecurityGroup extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "security_group" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "name_sg" character varying(255) NOT NULL,
                "description_sg" character varying(255) NOT NULL,
                "vpc_sg" character varying(1024) NOT NULL,
                "create_time" timestamp
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "security_group";';
        $this->exec($sql);
    }
}
