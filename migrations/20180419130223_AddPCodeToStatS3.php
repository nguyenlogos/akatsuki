<?php

use App\Migration;

class AddPCodeToStatS3 extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "stat_s3"
            ADD "dept" integer NULL,
            ADD "pcode" character varying(128) NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "stat_s3"
            DROP "dept",
            DROP "pcode";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}