<?php

use App\Migration;

class AddPrimaryKeyToProj extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "proj"
            ADD "id" serial NOT NULL,
            ADD PRIMARY KEY (id);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "proj"
            DROP "id";
        ';
        $this->exec($sql);
    }
}
