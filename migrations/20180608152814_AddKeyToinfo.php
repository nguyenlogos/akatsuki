<?php

use App\Migration;


class AddKeyToinfo extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "info"
            DROP "mid";
        ';
        $sql .= '
            ALTER TABLE "info"
            ADD "mid" serial NOT NULL,
            ADD "id_notif" integer NULL,
            ADD PRIMARY KEY (mid);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
           ALTER TABLE "info"
            DROP "mid";
        ';
        $this->exec($sql);
    }
}
