<?php

use App\Migration;

class AddVpcIdToSg extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg"
            ADD "vpcid" character varying(32) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg"
            DROP "vpcid";
        ';
        $this->exec($sql);
    }
}
