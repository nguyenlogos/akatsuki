<?php

use App\Migration;

class CreateTableKeyPair extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "key_pair" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "name_key" character varying NOT NULL,
                "file_key_pair" character varying(512) NOT NULL,
                "create_time" timestamp
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "key_pair";';
        $this->exec($sql);
    }
}
