<?php

use App\Migration;

class AddPrimaryKeyToAmiEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "ami_enable"
            ADD "id" serial NOT NULL,
            ADD PRIMARY KEY (id);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "ami_enable"
            DROP "id";
        ';
        $this->exec($sql);
    }
}
