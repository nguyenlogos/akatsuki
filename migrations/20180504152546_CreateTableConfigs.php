<?php

use App\Migration;

class CreateTableConfigs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "configs" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "insttype_curgen" smallint NOT NULL DEFAULT 0
            );
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "configs";';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
