<?php

use App\Migration;

class AddPriceToInsttype extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "insttype"
            ADD "price" numeric NULL DEFAULT 0;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'ALTER TABLE "insttype" DROP "price";';
        $this->exec($sql);
    }
}
