<?php

use App\Migration;

class CreateTableInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "inst_req" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "dept" integer NOT NULL,
                "pcode" character varying(128) NOT NULL,
                "inst_ami" character varying(128) NOT NULL,
                "inst_type" character varying(32) NOT NULL,
                "inst_count" integer NOT NULL,
                "inst_region" character varying(128) NOT NULL,
                "security_groups" character varying(256) NOT NULL,
                "using_purpose" character varying(512) NOT NULL,
                "using_from_date" date NOT NULL,
                "using_till_date" date NOT NULL,
                "shutdown_behavior" character varying(16) NOT NULL,
                "approved_status" smallint NOT NULL DEFAULT 0,
                "req_date" timestamp NOT NULL
            );
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "inst_req";';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
