<?php

use App\Migration;

class CreateTableInstReqChange extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "inst_req_change" (
                "id" serial NOT NULL,
                "inst_req_id" integer NOT NULL,
                "cid" integer NOT NULL,
                "empid" integer NOT NULL,
                "dept" integer NULL,
                "pcode" character varying(128) NULL,
                "using_purpose" character varying(512) NULL,
                "using_from_date" date NOT NULL,
                "using_till_date" date NOT NULL,
                "inst_address" character varying(32) NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "inst_req_change"';
        $this->exec($sql);
    }
}
