<?php

use App\Migration;

class DropDefaultNullDept extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "dept"
            ALTER "deptname" DROP DEFAULT,
            ALTER "deptname" SET NOT NULL,
            ALTER "status" SET DEFAULT 0,
            ALTER "status" DROP NOT NULL
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "dept"
            ALTER "deptname" DROP DEFAULT,
            ALTER "deptname" DROP NOT NULL,
            ALTER "status" DROP DEFAULT,
            ALTER "status" DROP NOT NULL;
        ';
        $this->exec($sql);
    }
}
