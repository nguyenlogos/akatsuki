<?php

use App\Migration;

class RemoveUnusedTables extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            DROP TABLE cost_insttype;
            DROP TABLE cost_service;
            DROP TABLE stat_cost;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
