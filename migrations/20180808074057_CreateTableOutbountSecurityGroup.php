<?php

use App\Migration;


class CreateTableOutbountSecurityGroup extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "sg_outbound" (
                "id" serial NOT NULL,
                "id_security_group"  integer NOT NULL,
                "outb_type" character varying(255) NOT NULL,
                "outb_protocol" character varying(64) NOT NULL,
                "outb_port_range" character varying(64) NOT NULL,
                "outb_cidr_ip" character varying(64) NOT NULL,
                "outb_destination" character varying(32) NOT NULL,
                "outb_description" character varying(1024) NOT NULL,
                "create_time" timestamp
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "sg_outbound";';
        $this->exec($sql);
    }
}
