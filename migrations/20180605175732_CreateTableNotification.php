<?php

use App\Migration;

class CreateTableNotification extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "tbl_notif" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "empid" integer NOT NULL,
                "notif_title" character varying(128) NOT NULL,
                "notif_msg" text NOT NULL,
                "notif_time" timestamp NOT NULL,
                "publish_date" timestamp NOT NULL,
                "notif_status" integer NOT NULL DEFAULT 0
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            DROP TABLE "tbl_notif";
        ';
        $this->exec($sql);
    }
}
