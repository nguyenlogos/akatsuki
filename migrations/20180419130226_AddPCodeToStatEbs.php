<?php

use App\Migration;

class AddPCodeToStatEbs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "stat_ebs"
            ADD "dept" integer NULL,
            ADD "pcode" character varying(128) NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "stat_ebs"
            DROP "dept",
            DROP "pcode";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}