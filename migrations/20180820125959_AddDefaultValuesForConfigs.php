<?php

use App\Migration;

class AddDefaultValuesForConfigs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "configs"
            ALTER "pass_lenght" TYPE integer,
            ALTER "pass_lenght" SET DEFAULT 4,
            ALTER "pass_lenght" SET NOT NULL,
            ALTER "admin_active" TYPE integer,
            ALTER "admin_active" SET DEFAULT 600,
            ALTER "admin_active" SET NOT NULL,
            ALTER "lock_failed" TYPE integer,
            ALTER "lock_failed" SET DEFAULT 5,
            ALTER "lock_failed" SET NOT NULL,
            ALTER "instance_expires" TYPE integer,
            ALTER "instance_expires" SET DEFAULT 30,
            ALTER "instance_expires" SET NOT NULL,
            ALTER "check_sys_admin" TYPE integer,
            ALTER "check_sys_admin" SET DEFAULT 1,
            ALTER "check_sys_admin" SET NOT NULL,
            ALTER "check_manager" TYPE integer,
            ALTER "check_manager" SET DEFAULT 1,
            ALTER "check_manager" SET NOT NULL,
            ALTER "timezone" TYPE character varying,
            ALTER "timezone" SET DEFAULT 9,
            ALTER "timezone" DROP NOT NULL,
            ALTER "timezoneid" TYPE smallint,
            ALTER "timezoneid" SET DEFAULT 69,
            ALTER "timezoneid" DROP NOT NULL;
        ';
        $result = $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {

    }
}
