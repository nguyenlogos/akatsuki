<?php

use App\Migration;

class AddInstVpcToInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ADD "inst_vpc" character varying(128) NULL,
            ADD "inst_subnet" character varying(128) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "inst_req"
            DROP "inst_vpc",
            DROP "inst_subnet";
        ';
        $this->exec($sql);
    }
}
