<?php

use App\Migration;

class AddPrimaryKeyToSg extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "sg"
            ADD "tbl_id" serial NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "sg"
            DROP "tbl_id";
        ';
        $this->exec($sql);
    }
}
