<?php

use App\Migration;

class AddPrimaryKeyToDept extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "dept"
            ADD "id" serial NOT NULL,
            ADD PRIMARY KEY (id);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "dept"
            DROP "id";
        ';
        $this->exec($sql);
    }
}
