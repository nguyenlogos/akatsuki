<?php

use App\Migration;

class AddUniqueKeyPairsAzoneEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "azone_enable"
            ADD UNIQUE (cid, name)
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "azone_enable"
            DROP CONSTRAINT "azone_enable_cid_name_key";
        ';
        $this->exec($sql);
    }
}
