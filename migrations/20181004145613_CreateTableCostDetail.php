<?php

use App\Migration;

class CreateTableCostDetail extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE cost_detail (
                cid integer,
                ym character varying(7),
                lineitemtype character varying(64),
                region character varying(64),
                productcode character varying(64),
                productfamily character varying(128),
                lineitemdescription character varying(512),
                preinstalledsw character varying(256),
                operatingsystem character varying(64),
                unit character varying(128),
                unblendedcost numeric,
                blendedcost numeric,
                usageamount numeric,
                unblendedrate numeric,
                blendedrate numeric,
                usagetype character varying(64),
                org_unblendedcost numeric,
                org_blendedcost numeric,
                org_usageamount numeric,
                billingentity character varying(128)
            );
            CREATE INDEX idx_cost_detail01 ON public.cost_detail USING btree (cid);
            CREATE INDEX idx_cost_detail02 ON public.cost_detail USING btree (ym);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            DROP TABLE "cost_detail";
        ';
        $this->exec($sql);
    }
}
