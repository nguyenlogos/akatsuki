<?php

use App\Migration;

class CreateTableInstReqBs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "inst_req_bs" (
                "id" serial NOT NULL,
                "inst_req_id" integer NOT NULL,
                "device_name" character varying(32) NOT NULL,
                "volumn_type" character varying(16) NOT NULL,
                "volumn_size" integer NOT NULL,
                "iops" integer NULL,
                "encrypted" smallint NULL DEFAULT 0,
                "auto_removal"  smallint NULL DEFAULT 1,
                "snapshot_id" character varying(32) NULL
            );
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "inst_req_bs";';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
