<?php

use App\Migration;

class AddBlendedColumsToInvoice extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE invoice
                ADD blended_usageusd NUMERIC,
                ADD blended_usagejpy NUMERIC,
                ADD blended_taxusd NUMERIC,
                ADD blended_taxjpy NUMERIC,
                ADD blended_totalusd NUMERIC,
                ADD blended_totaljpy NUMERIC;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE invoice
                DROP blended_usageusd,
                DROP blended_usagejpy,
                DROP blended_taxusd,
                DROP blended_taxjpy,
                DROP blended_totalusd,
                DROP blended_totaljpy;
        ';
        $this->exec($sql);
    }
}
