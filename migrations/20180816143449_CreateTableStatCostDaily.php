<?php

use App\Migration;

class CreateTableStatCostDaily extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE stat_cost_daily (
                cid integer,
                ymd character varying(10),
                servicecode character varying(128),
                ubcost numeric,
                bcost numeric,
                usagequantity numeric,
                currencycode character varying(32),
                estimated boolean,
                dept integer,
                pcode character varying(128)
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "stat_cost_daily";';
        $this->exec($sql);
    }
}
