<?php

use App\Migration;

class AddIpAddressToInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ADD "inst_address" character varying(32) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "inst_req"
            DROP "inst_address";
        ';
        $this->exec($sql);
    }
}
