<?php

use App\Migration;

class CreateTablePublicCost extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "public.cost" (
                  cid integer,
                  lineitemtype character varying(1024),
                  usagestartdate timestamp without time zone,
                  usageenddate timestamp without time zone,
                  productcode character varying(1024),
                  li_usagetype character varying(1024),
                  li_operation character varying(1024),
                  resourceid character varying(1024),
                  usageamount numeric,
                  currencycode character varying(32),
                  unblendedcost numeric,
                  blendedcost numeric,
                  lineitemdescription character varying(1024),
                  instancetype character varying(128),
                  location character varying(128),
                  memory character varying(128),
                  networkperformance character varying(128),
                  normalizationsizefactor numeric,
                  operatingsystem character varying(1024),
                  pd_operation character varying(1024),
                  physicalprocessor character varying(1024),
                  preinstalledsw character varying(1024),
                  processorarchitecture character varying(1024),
                  processorfeatures character varying(1024),
                  region character varying(128),
                  servicename character varying(1024),
                  transfertype character varying(128),
                  pd_usagetype character varying(128),
                  publicondemandcost numeric,
                  publicondemandrate numeric,
                  term character varying(128),
                  unit character varying(128),
                  usageaccountid character varying(128),
                  servicecode character varying(128),
                  productfamily character varying(1024),
                  procdate timestamp without time zone
                );
            ';

        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "public.cost";';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
