<?php

use App\Migration;

class AddColumtableLamda extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "tbl_lambda"
            ADD "hours_dai_gener" integer NULL,
            ADD "minute_dai_gener" integer NULL,
            ADD "hours_week_gener" integer NULL,
            ADD "minute_week_gener" integer NULL,
            ADD "hours_month_gener" integer NULL,
            ADD "minute_month_gener" integer NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "tbl_lambda"
            DROP "hours_dai_gener",
            DROP "minute_dai_gener",
            DROP "hours_week_gener",
            DROP "minute_week_gener",
            DROP "hours_month_gener",
            DROP "minute_month_gener";
        ';
        $this->exec($sql);
    }
}
