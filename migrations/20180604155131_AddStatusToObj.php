<?php

use App\Migration;

class AddStatusToObj extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "obj"
            ADD "status" integer NOT NULL DEFAULT 0;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "obj"
            DROP "status";
        ';
        $this->exec($sql);
    }
}
