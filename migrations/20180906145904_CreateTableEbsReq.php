<?php

use App\Migration;

class CreateTableEbsReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "ebs_req" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "empid" integer NOT NULL,
                "instance_id" character varying(32) NOT NULL,
                "updated_at" timestamp NOT NULL,
                "status" integer NOT NULL DEFAULT 0
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "ebs_req";';
        $this->exec($sql);
    }
}
