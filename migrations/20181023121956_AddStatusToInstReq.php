<?php

use App\Migration;

class AddStatusToInstReq extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ADD "status" smallint NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "inst_req"
            DROP "status";
        ';
        $this->exec($sql);
    }
}
