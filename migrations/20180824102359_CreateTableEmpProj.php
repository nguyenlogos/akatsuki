<?php

use App\Migration;

class CreateTableEmpProj extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "emp_proj" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "empid" integer NOT NULL,
                "pcode" character varying(128) NOT NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "emp_proj";';
        $this->exec($sql);
    }
}
