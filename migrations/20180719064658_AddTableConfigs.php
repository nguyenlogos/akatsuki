<?php

use App\Migration;

class AddTableConfigs extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "configs"
            ADD "pass_lenght" integer NOT NULL DEFAULT 0,
            ADD "pass_expired" integer NOT NULL DEFAULT 0,
            ADD "pass_lowercase" integer NOT NULL DEFAULT 0,
            ADD "pass_number" integer NOT NULL DEFAULT 0,
            ADD "mail_address" character varying(255) NULL,
            ADD "admin_active" integer NOT NULL DEFAULT 0,
            ADD "lock_failed" integer NOT NULL DEFAULT 0,
            ADD "instance_expires" integer NOT NULL DEFAULT 0,
            ADD "check_sys_admin" integer NOT NULL DEFAULT 0,
            ADD "check_manager" integer NOT NULL DEFAULT 0,
            ADD "email_instance" integer NOT NULL DEFAULT 0,
            ADD "days_apply" integer NOT NULL DEFAULT 0;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "configs"
            DROP "id";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
