<?php

use App\Migration;

class AddKeyNameTableInstReg extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "inst_req"
            ADD "key_name" character varying(128) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
                ALTER TABLE "inst_req"
                DROP "key_name";
        ';
        $this->exec($sql);
    }
}
