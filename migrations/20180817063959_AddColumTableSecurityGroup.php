<?php

use App\Migration;

class AddColumTableSecurityGroup extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "security_group"
            ADD "inst_req_id" integer NULL,
            ADD "sg_id" character varying NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "security_group"
            DROP "inst_req_id",
            DROP "sg_id";
        ';
        $this->exec($sql);
    }
}
