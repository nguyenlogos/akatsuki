<?php

use App\Migration;

class CreateTableEbsLog extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "ebs_log" (
                "id" serial NOT NULL,
                "instid" character varying(32) NOT NULL,
                "ebs_req_id" integer NOT NULL,
                "size" integer NOT NULL,
                "iops" integer NOT NULL,
                "zone" character varying(32) NOT NULL,
                "state" character varying(32) NOT NULL,
                "type" character varying(16) NOT NULL,
                "device" character varying(32) NOT NULL,
                "is_root" smallint NOT NULL,
                "encrypted" smallint NOT NULL,
                "del_term" smallint NOT NULL,
                "can_modify" smallint NOT NULL,
                "attached" smallint NOT NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE ebs_log';
        $this->exec($sql);
    }
}
