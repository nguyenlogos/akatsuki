<?php

use App\Migration;

class AddEmailConfirmedToEmp extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "emp"
            ADD "email_confirmed" integer NULL DEFAULT 0;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "emp"
            DROP "email_confirmed";
        ';
        $this->exec($sql);
    }
}
