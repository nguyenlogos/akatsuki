<?php

use App\Migration;

class CreateTableSgTempRules extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "sg_temp_rules" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "sg_temp_id" integer NOT NULL,
                "type" character varying(32) NOT NULL,
                "stype" character varying NOT NULL,
                "proto" character varying(32) NOT NULL,
                "range" character varying(32) NOT NULL,
                "source" character varying(32) NOT NULL,
                "target" character varying(64) NOT NULL,
                "desc" character varying(128) NULL
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            DROP TABLE "sg_temp_rules";
        ';
        $this->exec($sql);
    }
}
