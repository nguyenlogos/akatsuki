<?php

use App\Migration;

class AddDeptToCwRules extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "cw_rules"
            ADD "dept" integer NOT NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'ALTER TABLE "cw_rules" DROP "dept";';
        $this->exec($sql);
    }
}
