<?php

use App\Migration;

class DeleteColumTableEmp extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
                ALTER TABLE "emp"
                DROP "email_unlock";
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
                ALTER TABLE "emp"
                DROP "email_unlock";
        ';
        $this->exec($sql);
    }
}
