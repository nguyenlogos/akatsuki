<?php

use App\Migration;

class AddPrimaryKeyToEmp extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "emp"
            ADD "id" serial NOT NULL,
            ADD PRIMARY KEY (id);
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "emp"
            DROP "id";
        ';
        $this->exec($sql);
    }
}
