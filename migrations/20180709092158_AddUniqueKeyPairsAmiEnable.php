<?php

use App\Migration;

class AddUniqueKeyPairsAmiEnable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "ami_enable"
            ADD UNIQUE (cid, imageid)
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "ami_enable"
            DROP CONSTRAINT "ami_enable_cid_imageid_key";
        ';
        $this->exec($sql);
    }
}
