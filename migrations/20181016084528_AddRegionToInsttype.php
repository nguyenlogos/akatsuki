<?php

use App\Migration;

class AddRegionToInsttype extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "insttype"
            ADD "region" character varying(128) NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "insttype"
            DROP "region";
        ';
        $this->exec($sql);
    }
}
