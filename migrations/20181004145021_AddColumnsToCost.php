<?php

use App\Migration;

class AddColumnsToCost extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER table "cost"
            ADD unblendedrate numeric NULL,
            ADD blendedrate numeric NULL
        ';
        // $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER table "cost"
            DROP unblendedrate,
            DROP blendedrate
        ';
        // $this->exec($sql);
    }
}
