<?php

use App\Migration;

class AddPwResetTokenToEmp extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "emp"
            ADD "pw_reset_token" character varying(32) NULL,
            ADD "pw_reset_token_time" timestamp NULL;
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "emp"
            DROP "pw_reset_token",
            DROP "pw_reset_token_time";
        ';
        $this->exec($sql);
    }
}
