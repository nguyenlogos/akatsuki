<?php

use App\Migration;

class AddRegionToAmi extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            ALTER TABLE "ami"
            ADD "region" character varying(128) NULL;
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = '
            ALTER TABLE "ami"
            DROP "region";
        ';
        $container = $this->getContainer();
        $container['db']->exec($sql);
    }
}
