<?php

use App\Migration;

class CreateTableTblRoles extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $sql = '
            CREATE TABLE "tbl_roles" (
                "id" serial NOT NULL,
                "cid" integer NOT NULL,
                "url" character varying(32) NOT NULL,
                "action" character varying(32) NOT NULL,
                "sysadmin" smallint NOT NULL,
                "sysadmin_role" smallint NOT NULL DEFAULT 0,
                "manager" smallint NOT NULL,
                "manager_role" smallint NOT NULL DEFAULT 0,
                "employee" smallint NOT NULL,
                "employee_role" smallint NOT NULL DEFAULT 0
            );
        ';
        $this->exec($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $sql = 'DROP TABLE "tbl_roles"';
        $this->exec($sql);
    }
}
