<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\Ec2\Ec2Client;
use Common\Logger;
use Akatsuki\Models\SgTemp;
use Akatsuki\Models\SgTempRules;

class Ec2 extends Aws
{
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $this->client = new Ec2Client($this->getConfig());
    }

    public function describeVpcs($params = array())
    {
        try {
            $result = $this->client->describeVpcs($params);
            if (!empty($result['Vpcs'])) {
                return $result['Vpcs'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe VPCs');
            return [];
        }
    }

    public function describeSubnets($params = array())
    {
        try {
            $result = $this->client->describeSubnets($params);
            if (!empty($result['Subnets'])) {
                return $result['Subnets'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe Subnets');
            return [];
        }
    }

    public function describeVolumesModifications($volumeIds)
    {
        try {
            $result = $this->client->describeVolumesModifications([
                'VolumeIds' => $volumeIds
            ]);
            if (!empty($result['VolumesModifications'])) {
                return $result['VolumesModifications'];
            }
            return false;
        } catch (AwsException $e) {
            return false;
        }
    }

    public function runInstances($instanceConfigs)
    {
        try {
            $result = $this->client->runInstances($instanceConfigs);
            if (!empty($result['Instances'])) {
                return $result;
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Run Instances');
            return false;
        }
    }

    public function renameInstance($instanceID, $instanceName)
    {
        try {
            $result = $this->client->createTags([
                'Resources' => [
                    $instanceID
                ],
                'Tags' => [
                    [
                        'Key'   => 'Name',
                        'Value' => $instanceName
                    ]
                ]
            ]);
            return true;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Rename Instance');
            return false;
        }
    }

    public function updateEbsBackupRule($volumeID, $ruleName, $generaion)
    {
        try {
            $result = $this->client->createTags([
                'Resources' => [
                    $volumeID
                ],
                'Tags' => [
                    [
                        'Key'   => 'Rule',
                        'Value' => $ruleName
                    ],
                    [
                        'Key'   => 'Generation',
                        'Value' => $generaion
                    ],
                ]
            ]);
            return true;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Update Ebs Backup Rule');
            return false;
        }
    }

    public function describeInstances($params = array())
    {
        try {
            $result = $this->client->describeInstances($params);
            if (!empty($result['Reservations'])) {
                return $result['Reservations'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe Instances');
            return [];
        }
    }

    public function describeVolumes($params = array())
    {
        try {
            $result = $this->client->describeVolumes($params);
            if (!empty($result['Volumes'])) {
                return $result['Volumes'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe Volumes');
            return [];
        }
    }

    public function createSecurityGroup($params = array())
    {
        try {
            $result = $this->client->createSecurityGroup($params);
            if (!empty($result['GroupId'])) {
                return $result['GroupId'];
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Create Security Group');
            return false;
        }
    }

    /**
     * Update inbound/outbound of an existing security group
     *
     * @param String $sgid
     * @param Array $rules array list of SgTempRules
     * @return Boolean true|false
     */
    public function updateSecurityGroupRules($sgid, Array $rules)
    {
        $result = $this->describeSecurityGroups([
            'GroupIds' => [$sgid]
        ]);
        if (!$result) {
            return false;
        }

        $checkRuleExists = function ($targetRule, Array $listRules) {
            $matches = array_filter($listRules, function ($rule) use ($targetRule) {
                return $targetRule->isEqualTo($rule);
            });
            return count($matches) > 0;
        };

        $convertIpPermission = function ($ipPermission, $type) {
            $protocol = strtoupper($ipPermission['IpProtocol']);
            if ($protocol == '-1') {
                return null;
            }
            $ipRange = array_shift($ipPermission['IpRanges']);
            $target = $ipRange ? $ipRange['CidrIp'] : '';

            $fromPort = $ipPermission['FromPort'];
            $toPort = $ipPermission['ToPort'];

            $sgTempRule = new SgTempRules();
            $sgTempRule->setValues([
                'type'  => $type,
                'proto' => $protocol,
                'pfrom' => (int)$ipPermission['FromPort'],
                'pto'   => (int)$ipPermission['ToPort'],
                'target'=> $target,
            ]);

            return $sgTempRule;
        };

        $sg = array_shift($result);

        $listRuleCurrent = [];
        if (!empty($sg['IpPermissions'])) {
            foreach ($sg['IpPermissions'] as $ipPermission) {
                $rule = $convertIpPermission($ipPermission, 'inbound');
                $rule && $listRuleCurrent[] = $rule;
            }
        }
        if (!empty($sg['IpPermissionsEgress'])) {
            foreach ($sg['IpPermissionsEgress'] as $ipPermission) {
                $rule = $convertIpPermission($ipPermission, 'outbound');
                $rule && $listRuleCurrent[] = $rule;
            }
        }

        // list append
        $listRules = [];
        foreach ($rules as $rule) {
            if (!($rule instanceof SgTempRules)) {
                continue;
            }

            // TODO: check existing
            $isExisting = $checkRuleExists($rule, $listRuleCurrent);
            if ($isExisting) {
                continue;
            }

            $configs = [
                'GroupId' => $sgid,
                'IpPermissions' => [
                    [
                        'FromPort'   => (int)$rule->pfrom,
                        'IpProtocol' => $rule->proto,
                        'IpRanges'   => [
                            [
                                'CidrIp' => $rule->target,
                                'Description' => $rule->desc,
                            ],
                        ],
                        'ToPort' => (int)$rule->pto,
                    ],
                ]
            ];
            $configs['type'] = $rule->type;
            $listRules[] = $configs;
        }

        if (count($listRules)) {
            try {
                foreach ($listRules as $index => $configs) {
                    $type = $configs['type'];
                    unset($configs['type']);
                    if ($type === 'inbound') {
                        $promises[$index] = $this->client->authorizeSecurityGroupIngressAsync($configs);
                    } else {
                        $promises[$index] = $this->client->authorizeSecurityGroupEgressAsync($configs);
                    }
                }
                $promises = \GuzzleHttp\Promise\unwrap($promises);
            } catch (AwsException $e) {
                aws_handle_exception($e, '[EC2] Authorize Security Group Ingress/Egress');
                return false;
            }
        }

        return true;
    }

    public function getAvailableVolumes($zone = null)
    {
        $filters = [
            [
                'Name'   => 'status',
                'Values' => ['available']
            ]
        ];
        if ($zone) {
            $filters[] = [
                'Name'   => 'availability-zone',
                'Values' => [$zone]
            ];
        }

        return $this->describeVolumes([
            'Filters' => $filters
        ]);
    }

    public function createVolume($volumeSettings)
    {
        try {
            $ebsVolume = $this->client->createVolume($volumeSettings)->toArray();
            if ($ebsVolume) {
                $result = $this->waitForVolume($ebsVolume['VolumeId'], 'available');
                if ($result) {
                    $ebsVolume['State'] = 'available';
                    return $ebsVolume;
                }
                return false;
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Create Volume');
            return false;
        }
    }

    public function attachVolume($instanceId, $volumeId, $device)
    {
        try {
            $result = $this->client->attachVolume([
                'Device'     => $device,
                'InstanceId' => $instanceId,
                'VolumeId'   => $volumeId
            ]);

            if ($result) {
                return $this->waitForVolume($volumeId, 'in-use');
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Attach Volume');
            return false;
        }
    }

    public function detachVolume($volumeId, $waitForAvailable = true)
    {
        try {
            $result = $this->client->detachVolume([
                'VolumeId' => $volumeId
            ]);

            if ($result) {
                if ($waitForAvailable) {
                    return $this->waitForVolume($volumeId, 'available', 5);
                }
                return true;
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Detach Volume');
            return false;
        }
    }

    public function modifyVolume($volumeId, $size = null, $type = null, $iops = null)
    {
        try {
            $volumeConfigs = [
                'VolumeId' => $volumeId
            ];
            $size > 0 && $volumeConfigs['Size'] = $size;
            $type && $volumeConfigs['VolumeType'] = $type;
            $iops && $volumeConfigs['Iops'] = $iops;
            $result = $this->client->modifyVolume($volumeConfigs);
            return $result;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Modify Volume');
            return false;
        }
    }

    public function modifyVolumeAsync($volumeId, $size = null, $type = null, $iops = null)
    {
        try {
            $volumeConfigs = [
                'VolumeId' => $volumeId
            ];
            $size > 0 && $volumeConfigs['Size'] = $size;
            $type && $volumeConfigs['VolumeType'] = $type;
            $iops && $volumeConfigs['Iops'] = $iops;
            $result = $this->client->modifyVolumeAsync($volumeConfigs);
            return $result;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Modify Volume');
            return false;
        }
    }

    private function waitForVolume($volumeId, $expectedState, $retryCount = 3)
    {
        if ($retryCount === 0) {
            return false;
        }
        sleep(2);
        try {
            $currentState = null;
            $result = $this->client->describeVolumes([
                'VolumeIds' => [$volumeId]
            ]);
            if (!empty($result['Volumes'])) {
                $volume = array_shift($result['Volumes']);
                $currentState = $volume['State'];
            }
            if ($currentState && $currentState === $expectedState) {
                return true;
            }
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe Volumes');
            return false;
        }

        return $this->waitForVolume($volumeId, $expectedState, --$retryCount);
    }

    public function describeSecurityGroups($params = array())
    {
        try {
            $result = $this->client->describeSecurityGroups($params);
            if (!empty($result['SecurityGroups'])) {
                return $result['SecurityGroups'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe Security Groups');
            return false;
        }
    }

    public function describeAddresses($configs = array())
    {
        try {
            $result = $this->client->describeAddresses($configs);
            if (!empty($result['Addresses'])) {
                return $result['Addresses'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe Addresses');
            return false;
        }
    }

    public function updateSecurityGroupCIDR($oldCIDR, $newCIDR)
    {
        if ($oldCIDR === $newCIDR) {
            return true;
        }
        @list($oldIP, $oldSubnet) = explode("/", $oldCIDR);
        @list($newIP, $newSubnet) = explode("/", $newCIDR);
        $oldIPAdrress = $oldCIDR[0];
        $securityGroups = \Akatsuki\Models\Sg::updateSecurityGroups();
        $ipRanges = [];
        foreach ($securityGroups as $sg) {
            $ipRanges = array_merge($ipRanges, $sg['ipranges']);
        }

        $sourceList = [];
        $targetList = [];
        foreach ($ipRanges as $ipRange) {
            @list($ip, $subnet) = explode("/", $ipRange['iprange']);
            if ($ip === $oldIP) {
                $sourceList[] = $ipRange;
            } elseif ($ip === $newIP) {
                $targetList[] = $ipRange;
            }
        }
        unset($ipRanges);

        $delList = [];
        $addList = [];

        $inRange = function ($ipRanges, $ipRange) {
            $match = false;
            foreach ($ipRanges as $range) {
                if ($ipRange['id'] === $range['id'] && $ipRange['type'] === $range['type'] &&
                    $ipRange['protocol'] === $range['protocol'] && $ipRange['iprange'] === $range['iprange']
                ) {
                    $match = true;
                    break;
                }
            }

            return $match;
        };

        foreach ($sourceList as $index => $ipRange) {
            @list($ip, $subnet) = explode("/", $ipRange['iprange']);
            if ($oldIP === $newIP) {
                if ($subnet === $oldSubnet) {
                    $delList[] = $ipRange;
                    $ipRangeNew = ["iprange" => $newCIDR] + $ipRange;
                    $match = $inRange($sourceList, $ipRangeNew);
                    if (!$match) {
                        $addList[] = $ipRangeNew;
                    }
                }
            } else {
                $delList[] = $ipRange;
                $cidrNew = $subnet === $oldSubnet ? $newCIDR : "{$newIP}/{$subnet}";
                $ipRangeNew = ["iprange" => $cidrNew] + $ipRange;
                $match = $inRange($targetList, $ipRangeNew);
                if (!$match) {
                    $addList[] = $ipRangeNew;
                }
            }
        }

        try {
            if (count($delList)) {
                $promises = [];
                foreach ($delList as $ipRange) {
                    $configs = [
                        'GroupId' => $ipRange['id'],
                        'IpPermissions' => [
                            [
                                'FromPort'   => $ipRange['fromport'],
                                'IpProtocol' => $ipRange['protocol'],
                                'IpRanges'   => [
                                    [
                                        'CidrIp' => $ipRange['iprange']
                                    ],
                                ],
                                'ToPort' => $ipRange['fromport'],
                            ],
                        ]
                    ];
                    if ($ipRange['type'] === 'ingress') {
                        $promises[$index] = $this->client->revokeSecurityGroupIngressAsync($configs);
                    } elseif ($ipRange['type'] === 'egress') {
                        $promises[$index] = $this->client->revokeSecurityGroupEgressAsync($configs);
                    }
                }
                $promises = \GuzzleHttp\Promise\unwrap($promises);
            }

            if (count($addList)) {
                $promises = [];
                foreach ($addList as $ipRange) {
                    $configs = [
                        'GroupId' => $ipRange['id'],
                        'IpPermissions' => [
                            [
                                'FromPort'   => $ipRange['fromport'],
                                'IpProtocol' => $ipRange['protocol'],
                                'IpRanges'   => [
                                    [
                                        'CidrIp' => $ipRange['iprange']
                                    ],
                                ],
                                'ToPort' => $ipRange['fromport'],
                            ],
                        ]
                    ];
                    if ($ipRange['type'] === 'ingress') {
                        $promises[$index] = $this->client->authorizeSecurityGroupIngressAsync($configs);
                    } elseif ($ipRange['type'] === 'egress') {
                        $promises[$index] = $this->client->authorizeSecurityGroupEgressAsync($configs);
                    }
                }
                $promises = \GuzzleHttp\Promise\unwrap($promises);
            }

            return true;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Update Security Groups Ingress/Egress');
            return false;
        }
    }

    public function checkAddressAvailability($publicIpAddress)
    {
        try {
            $ips = $this->describeAddresses([
                'Filters' => [
                    [
                        'Name'   => 'public-ip',
                        'Values' => [$publicIpAddress]
                    ]
                ]
            ]);
            $addressInfo = $ips ? array_shift($ips) : null;
            if (!$addressInfo || !empty($addressInfo['AssociationId'])) {
                return false;
            }

            return true;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Check Address Availability');
            return false;
        }
    }

    public function allocateAddress($publicIpAddress = '')
    {
        try {
            $addressInfo = [];
            $addressInfo['Domain'] = 'vpc';
            if (!empty($publicIpAddress)) {
                $addressInfo['PublicIp'] = $publicIpAddress;
            }
            $result = $this->client->allocateAddress($addressInfo);
            if ($result) {
                return $result->toArray();
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Allocate Address');
            return false;
        }
    }

    public function associateAddress($instanceId, $publicIpAddress)
    {
        try {
            $result = $this->waitForInstance($instanceId, 'running,stopped', 30);
            $result && $result = $this->client->associateAddress([
                'InstanceId' => $instanceId,
                'PublicIp'   => $publicIpAddress,
            ]);

            if ($result) {
                return $result->toArray();
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Associate address');
            return false;
        }
    }


    public function disassociateAddress($publicIpAddress)
    {
        try {
            $result = $this->client->disassociateAddress([
                'PublicIp'   => $publicIpAddress,
            ]);

            return true;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Disassociate address');
            return false;
        }
    }

    private function waitForInstance($instanceId, $expectedState, $retryCount = 3)
    {
        if ($retryCount === 0) {
            return false;
        }
        sleep(2);
        try {
            $currentState = null;
            $result = $this->client->describeInstances([
                'InstanceIds' => [$instanceId],
                'Filters' => [
                    [
                        'Name' => 'instance-state-name',
                        'Values' => explode(',', $expectedState),
                    ]
                ],
            ]);
            if (!empty($result['Reservations']) && count($result['Reservations'])) {
                return true;
            }
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Describe Volumes');
            return false;
        }

        return $this->waitForInstance($instanceId, $expectedState, --$retryCount);
    }

    public function modifySecurityGroup($sgId, $InstanceFaceId)
    {
        try {
            $result = $this->client->modifyNetworkInterfaceAttribute([
                'Groups' => [
                    $sgId
                ],
                'NetworkInterfaceId' => ''.$InstanceFaceId.'',
            ]);
            if ($result) {
                return $result->toArray();
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Modify security group');
            return false;
        }
    }

    public function describeSpotPriceHistory($instTypeList, $region, &$nextToken = null)
    {
        try {
            $result = $this->client->describeSpotPriceHistory([
                'InstanceTypes' => $instTypeList,
                'ProductDescriptions' => [
                    'Linux/UNIX (Amazon VPC)',
                ],
                'AvailabilityZone' => $region,
            ]);
            if ($result) {
                $newNextToken = $result['NextToken'];
                if (!$newNextToken || $newNextToken === $nextToken) {
                    $nextToken = null;
                }
                return $result['SpotPriceHistory'];
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Instance type pricing');
            return false;
        }
    }

    public function checkInstanceState($instid)
    {
        try {
            $result = $this->client->describeInstances([
                'InstanceIds' => [$instid],
            ]);
            if (!empty($result['Reservations']) && count($result['Reservations'])) {
                $instance = $result['Reservations'][0]['Instances'][0];
                return $instance['State']['Name'];
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Instance type pricing');
            return false;
        }
    }

    public function startInstances(Array $instids)
    {
        try {
            $result = $this->client->startInstances([
                'InstanceIds' => $instids,
            ]);
            if (!empty($result['StartingInstances'])) {
                return $result['StartingInstances'];
            }

            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Start Instances');
            return false;
        }
    }

    public function stopInstances(Array $instids)
    {
        try {
            $result = $this->client->stopInstances([
                'InstanceIds' => $instids,
            ]);

            if (!empty($result['StoppingInstances'])) {
                return $result['StoppingInstances'];
            }

            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[EC2] Stop Instances');
            return false;
        }
    }
}
