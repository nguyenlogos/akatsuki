<?php
namespace AwsServices;

class Aws
{
    protected $client;
    private $clientConfig;

    public function __construct(array &$clientConfig = array())
    {
        $result = true;
        if (empty($clientConfig['credentials'])) {
            $result = false;
        } else {
            $credentials = $clientConfig['credentials'];
            if (empty($credentials['key']) || empty($credentials['secret'])) {
                $result = false;
            }
        }

        if (empty($clientConfig['region'])) {
            $result = false;
        } else {
            $clientConfig['region'] = preg_replace('/(.+\d)[a-z]$/', '$1', $clientConfig['region']);
        }

        if (!$result) {
            throw new \Exception("Invalid credentials", 1);
        }

        if (empty($clientConfig['version'])) {
            $clientConfig['version'] = 'latest';
        }
        $this->clientConfig = $clientConfig;
    }

    public function getConfig()
    {
        return $this->clientConfig;
    }

    public static function validateApi($key, $secret, $region)
    {
        $config = [
            'credentials'   => [
                'key'       => $key,
                'secret'    => $secret,
            ],
            'region'        => $region,
            'version'       => 'latest'
        ];
        try {
            $client = new \Aws\Ec2\Ec2Client($config);
            return $client->describeAccountAttributes();
        } catch (\Aws\Exception\AwsException $e) {
            return false;
        }
    }

    public static function getInstance($region = null, $cid = null)
    {
        $credentials = [
            'key'    => '',
            'secret' => '',
        ];
        $awsregion = '';
        if (php_sapi_name() === "cli") {
            if (empty($cid)) {
                return false;
            }
            $info = \Akatsuki\Models\Users::where('cid', $cid)->where('status', 0)->first();
            if (!$info) {
                return false;
            }
            $credentials['key'] = decryptIt($info['key']);
            $credentials['secret'] = $info['secret'];
            $awsregion = $region ?: DEFAULT_AWS_REGION;
        } else {
            $credentials['key'] = $_SESSION['key'];
            $credentials['secret'] = $_SESSION['secret'];
            $awsregion = $region ?: $_SESSION['region'];
            ;
        }

        try {
            $clientConfig = array(
                'credentials'   => [
                    'key'       => $credentials['key'],
                    'secret'    => $credentials['secret'],
                ],
                'region'        => $awsregion,
                'version'       => 'latest'
            );
            return new static($clientConfig);
        } catch (\Exception $e) {
            return false;
        }
    }
}
