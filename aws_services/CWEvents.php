<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\CloudWatchEvents\CloudWatchEventsClient;
use Common\Logger;

class CWEvents extends Aws
{
    private $lambdaClient;
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $this->client = new CloudWatchEventsClient($this->getConfig());
    }

    // TODO: remove
    public function putEbsBackupRules($configs)
    {
        if (!$this->lambdaClient) {
            $this->lambdaClient = new Lambda($this->getConfig());
        }
        $backupLambdaArn = $this->lambdaClient->getFunctionArn('ids-ebs-backup');
        if (!$backupLambdaArn) {
            $backupLambdaArn = $this->lambdaClient->createEbsBackupFunction('ids-ebs-backup');
        }

        if (!$backupLambdaArn) {
            return false;
        }

        foreach ($configs as $volumeID => $config) {
            if (empty($config['expr']) || empty($config['copy']) || (int)$config['copy'] <= 0) {
                continue;
            }
            $ruleName = 'ebs-backup-' . str_replace('vol-', '', $volumeID);
            $ruleArn = $this->putRule([
                'Name' => $ruleName,
                'ScheduleExpression' => "cron({$config['expr']})",
            ]);
            if (empty($ruleArn)) {
                return false;
            }

            $targets = $this->listTargets($ruleName);
            $targetID = $targetInput = [];
            if (count($targets)) {
                $cwTarget = array_shift($targets);
                $targetID = $cwTarget['Id'];
                $targetInput = json_decode($cwTarget['Input'], true);
            } else {
                $targetID = 'Id' . time();
            }

            $targetInput['id'] = $volumeID;
            $targetInput['copy'] = (int)$config['copy'];
            $cwTarget = [
                'Arn'   => $backupLambdaArn,
                'Id'    => $targetID,
                'Input' => json_encode($targetInput)
            ];
            array_unshift($targets, $cwTarget);
            $this->putTargets($ruleName, $targets);
        }
        return true;
    }

    public function putRule($configs)
    {
        try {
            $result = $this->client->putRule($configs);
            return $result['RuleArn'];
        } catch (AwsException $e) {
            aws_handle_exception($e, "[Cloudwatch] Put Rule");
            return null;
        }
    }

    public function deleteRule($name)
    {
        try {
            $targets = $this->listTargets($name);
            if (count($targets)) {
                $targetIds = array_column($targets, 'Id');
                $result = $this->client->removeTargets([
                    'Ids'  => $targetIds,
                    'Rule' => $name
                ]);
            }
            $result && $result = $this->client->deleteRule([
                'Name' => $name
            ]);
            return !!$result;
        } catch (AwsException $e) {
            aws_handle_exception($e, "[Cloudwatch] Delete Rule");
            return false;
        }
    }

    public function listTargets($ruleName, $limit = 0, &$nextToken = null)
    {
        try {
            $configs = [
                'Rule' => $ruleName
            ];
            if ($limit > 0) {
                $configs['Limit'] = $limit;
            }
            if (!empty($nextToken)) {
                $configs['NextToken'] = $nextToken;
            }
            $result = $this->client->listTargetsByRule($configs);
            $nextToken = !empty($result['NextToken']) ? $result['NextToken'] : null;

            if (!empty($result['Targets'])) {
                return $result['Targets'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, "[Cloudwatch] List Targets By Rule");
            return [];
        }
    }

    public function listRules($namePrefix = '', $limit = 0, &$nextToken = null)
    {
        try {
            $configs = [];
            if (!empty($namePrefix)) {
                $configs['NamePrefix'] = $namePrefix;
            }
            if ($limit > 0) {
                $configs['Limit'] = $limit;
            }
            if (!empty($nextToken)) {
                $configs['NextToken'] = $nextToken;
            }
            $result = $this->client->listRules($configs);
            $nextToken = !empty($result['NextToken']) ? $result['NextToken'] : null;

            if (!empty($result['Rules'])) {
                return $result['Rules'];
            }
            return [];
        } catch (AwsException $e) {
            aws_handle_exception($e, '[Cloudwatch] List Rules');
            return [];
        }
    }

    public function putTargets($ruleName, $targets)
    {
        try {
            return $this->client->putTargets([
                'Rule'    => $ruleName,
                'Targets' => $targets
            ]);
        } catch (AwsException $e) {
            aws_handle_exception($e, '[Cloudwatch] Put Targets');
            return null;
        }
    }

    public function putBackupTargets($ruleName)
    {
        $backupLambdaArn = "";
        if (!empty($_SESSION['EBS_BACKUP_FUNCTION_ARN'])) {
            $backupLambdaArn = $_SESSION['EBS_BACKUP_FUNCTION_ARN'];
        } else {
            if (!$this->lambdaClient) {
                $this->lambdaClient = new Lambda($this->getConfig());
            }
            $backupLambdaArn = $this->lambdaClient->getFunctionArn(LAMBDA_EBS_BACKUP_FUNCTION_NAME);
            if (!$backupLambdaArn) {
                $backupLambdaArn = $this->lambdaClient->createEbsBackupFunction(LAMBDA_EBS_BACKUP_FUNCTION_NAME);
            }
        }
        if (!$backupLambdaArn) {
            return false;
        }

        $targets = $this->listTargets($ruleName);
        $targetID = '';
        $params = [];

        if (count($targets)) {
            $target = array_shift($targets);
            $targetID = $target['Id'];
            $params = json_decode($target['Input'], true);
        } else {
            $targetID = 'Id' . time();
        }

        $params['Name'] = $ruleName;

        $myTarget = [
            'Arn'   => $backupLambdaArn,
            'Id'    => $targetID,
            'Input' => json_encode($params)
        ];
        array_unshift($targets, $myTarget);
        return $this->putTargets($ruleName, $targets);
    }
}
