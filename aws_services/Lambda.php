<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\Lambda\LambdaClient;
use Common\Logger;

class Lambda extends Aws
{
    private $iamClient;
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $this->client = new LambdaClient($this->getConfig());
    }

    public function createEbsBackupFunction($functionName)
    {
        $filePath = ROOT_PATH . "/aws_lambda/functions/lambda-{$functionName}.zip";
        if (!is_readable($filePath)) {
            return false;
        }
        if (!$this->iamClient) {
            $this->iamClient = new IAM($this->getConfig());
        }
        $backupRoleArn = $this->iamClient->getRoleArn('ids-ebs-lambda-worker');
        if (empty($backupRoleArn)) {
            $backupRoleArn = $this->iamClient->createEbsBackupRole('ids-ebs-lambda-worker');
        }
        if (!$backupRoleArn) {
            return false;
        }
        try {
            $contents = file_get_contents($filePath);
            $result = $this->client->createFunction([
                'Code' => [
                    'ZipFile'  => $contents
                ],
                'Description'  => 'Lambda ebs-backup function created automatically by IDS system.',
                'FunctionName' => $functionName,
                'Handler'      => 'lambda_function.lambda_handler',
                'MemorySize'   => 128,
                'Publish'      => true,
                'Role'         => $backupRoleArn,
                'Runtime'      => 'python2.7',
                'Timeout'      => 120
            ]);
            if (!empty($result['FunctionArn'])) {
                return $result['FunctionArn'];
            }
            return false;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[Lambda] Create Function');
            return false;
        }
    }

    public function getFunctionArn($functionName)
    {
        try {
            $result = $this->client->getFunction([
                'FunctionName' => $functionName
            ]);
            if (!empty($result['Configuration'])) {
                return $result['Configuration']['FunctionArn'];
            }
            return null;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[Lambda] Get Function Arn');
            return null;
        }
    }
}
