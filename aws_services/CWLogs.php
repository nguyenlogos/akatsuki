<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Common\Logger;

class CWLogs extends Aws
{
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $this->client = new CloudWatchLogsClient($this->getConfig());
    }

    public function getLogStreams($logGroupName, $limit = 50, &$nextToken = null)
    {
        $config = [
            'limit'        => $limit,
            'orderBy'      => 'LastEventTime',
            'descending'   => true,
            'logGroupName' => $logGroupName,
        ];
        if (!empty($nextToken)) {
            $config['nextToken'] = $nextToken;
        }
        $result = $this->client->describeLogStreams($config);
        if (!empty($result['logStreams'])) {
            if (!empty($result['nextToken'])) {
                $nextToken = $result['nextToken'];
            } else {
                $nextToken = null;
            }
            return $result['logStreams'];
        }
        return null;
    }

    public function getLogEvents($logGroupName, $logStreamName, &$nextToken = null)
    {
        $config = [
            'logGroupName'  => $logGroupName,
            'logStreamName' => $logStreamName
        ];
        if (!empty($nextToken)) {
            $config['nextToken'] = $nextToken;
        }
        $result = $this->client->getLogEvents($config);
        $nextToken = $result['nextForwardToken'];
        if (!empty($result['events'])) {
            return $result['events'];
        }
        return null;
    }
}
