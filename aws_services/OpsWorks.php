<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\OpsWorks\OpsWorksClient;
use Common\Logger;

class OpsWorks extends Aws
{
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $this->client = new OpsWorksClient($this->getConfig());
    }

    public function describeElasticIps()
    {
        try {
            return $result = $this->client->describeElasticIps();
        } catch (AwsException $e) {
            aws_handle_exception($e, '[OpsWorks] Describe ElasticIps');
            return false;
        }
    }
}
