<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\CostandUsageReportService\CostandUsageReportServiceClient as CostReportClient;
use Common\Logger;

class CostAndUsageReport extends Aws
{
    private $s3Client;
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $clientConfig['region'] = 'us-east-1'; // only "US East" supports "Cost and Usage Report" Service
        $this->client = new CostReportClient($clientConfig);
    }

    public function generateReportTemplates()
    {
        try {
            // $currentRPT = $this->getRPT();
            // $numRPT = count($currentRPT);
            return $this->generateRPTs();
            /*
            if ($numRPT === 0) {
                $this->generateRPTs();
            } else {
                $rptByTimeUnit = [];
                foreach ($currentRPT as $rpt) {
                    $timeUnit = $rpt['TimeUnit'];
                    if (!array_key_exists($timeUnit, $rptByTimeUnit)) {
                        $rptByTimeUnit[$timeUnit] = [];
                    }
                    $rptByTimeUnit[$timeUnit][] = $rpt;
                }

                $missingTimeUnit = null;
                $rptTemplate = null;
                if (count(array_keys($rptByTimeUnit)) >= 2) {
                    return true;
                }
                if (!array_key_exists('DAILY', $rptByTimeUnit)) {
                    $rptTemplate = $rptByTimeUnit['HOURLY'][0];
                    $missingTimeUnit = 'DAILY';
                } else {
                    $rptTemplate = $rptByTimeUnit['DAILY'][0];
                    $missingTimeUnit = 'HOURLY';
                }
                // since "describeReportDefinitions" return wrong location for S3Bucket, we must retrieve it manually
                $rptConfig = [
                    'ReportName' => $missingTimeUnit . "_REPORT_" . time(),
                    'TimeUnit'   => $missingTimeUnit,
                    'S3Region'   => $this->getS3Location($rptTemplate['S3Bucket']),
                ];
                $otherOptions = [
                    'Compression',
                    'Format',
                    'S3Bucket',
                    'S3Prefix',
                    'AdditionalArtifacts',
                    'AdditionalSchemaElements',
                ];
                foreach ($otherOptions as $option) {
                    $rptConfig[$option] = $rptTemplate[$option];
                }

                return !!$this->createRPT($rptConfig);
            }
            */
        } catch (AwsException $e) {
            aws_handle_exception($e, '[Billing] Generate Report Templates');
            return false;
        }
    }

    private function generateRPTs()
    {
        if (!$this->s3Client) {
            $this->s3Client = new S3($this->getConfig());
        }
        $bucketName = 'ids-report-bucket-' . time();
        $s3Config = [
            'Bucket' => $bucketName
        ];

        $s3Bucket = $this->s3Client->createBucket($s3Config);
        if (!$s3Bucket) {
            return false;
        }

        $bucketPolicy = [
            "Version" => "2012-10-17",
            "Statement" => [
                [
                    "Effect" => "Allow",
                    "Principal" => [
                        "AWS" => "arn:aws:iam::386209384616:root"
                    ],
                    "Action" => [
                        "s3:GetBucketAcl",
                        "s3:GetBucketPolicy"
                    ],
                    "Resource" => "arn:aws:s3:::{$bucketName}"
                ],
                [
                    "Effect" => "Allow",
                    "Principal" => [
                        "AWS" => "arn:aws:iam::386209384616:root"
                    ],
                    "Action" => "s3:PutObject",
                    "Resource" => "arn:aws:s3:::{$bucketName}/*"
                ]
            ]
        ];
        $this->s3Client->updatePolicy($bucketName, $bucketPolicy);

        $rptConfig = [
            'AdditionalArtifacts' => [],
            'AdditionalSchemaElements' => [
                "RESOURCES"
            ],
            'Compression' => 'GZIP',
            'Format'      => 'textORcsv',
            'ReportName'  => '',
            'S3Bucket'    => $bucketName,
            'S3Prefix'    => 'rep',
            'S3Region'    => $this->getConfig()['region'],
            'TimeUnit'    => '',
        ];

        $result = true;
        foreach (['HOURLY', 'DAILY'] as $timeUnit) {
            $rptConfig['ReportName'] = $timeUnit . "_REPORT_" . time();
            $rptConfig['TimeUnit'] = $timeUnit;

            $result && $result = $this->createRPT($rptConfig);
        }

        return $result;
    }

    private function createRPT($rptConfig = array())
    {
        return $this->client->putReportDefinition([
            'ReportDefinition' => $rptConfig
        ]);
    }

    public function getRPT()
    {
        $result = $this->client->describeReportDefinitions();
        if (!empty($result['ReportDefinitions'])) {
            return $result['ReportDefinitions'];
        }

        return [];
    }

    private function getS3Location($s3Bucket)
    {
        if (!$this->s3Client) {
            $this->s3Client = new S3($this->getConfig());
        }

        return $this->s3Client->getLocation($s3Bucket);
    }
}
