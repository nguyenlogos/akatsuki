<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\Iam\IamClient;
use Common\Logger;

class IAM extends Aws
{
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $this->client = new IamClient($this->getConfig());
    }

    public function getRoleArn($roleName)
    {
        try {
            $result = $this->client->getRole([
                'RoleName' => $roleName
            ]);
            if (!empty($result['Role'])) {
                return $result['Role']['Arn'];
            }
            return null;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[IAM] Get Role Arn');
            return null;
        }
    }

    public function createEbsBackupRole($roleName)
    {
        $defaultPolicies = [
            "Version"   => "2012-10-17",
            "Statement" => [
                [
                    "Effect"      => "Allow",
                    "Principal"   => [
                        "Service" => [
                            "ec2.amazonaws.com",
                            "lambda.amazonaws.com",
                        ]
                    ],
                    "Action" => [
                        "sts:AssumeRole"
                    ]
                ]
            ]
        ];
        $policies = [
            "Version"   => "2012-10-17",
            "Statement" => [
                [
                    "Effect" => "Allow",
                    "Action" => [
                        "ec2:CreateSnapshot",
                        "ec2:CreateTags",
                        "ec2:DeleteSnapshot",
                        "ec2:Describe*",
                        "ec2:ModifySnapshotAttribute",
                        "ec2:ResetSnapshotAttribute"
                    ],
                    "Resource" => "*"
                ],
                [
                    "Effect"   => "Allow",
                    "Action"   => "logs:*",
                    "Resource" => "arn:aws:logs:*:*:*"
                ]
            ]
        ];
        try {
            $result = $this->client->createRole([
                'AssumeRolePolicyDocument' => json_encode($defaultPolicies),
                'Description'  => 'Role for lambda ebs backup (created by IDS System)',
                'Path'         => '/ids/lambda/',
                'RoleName'     => $roleName,
            ]);
            if (empty($result['Role'])) {
                return null;
            }
            $roleArn = $result['Role']['Arn'];
            $result = $this->client->putRolePolicy([
                'PolicyDocument' => json_encode($policies),
                'PolicyName'     => 'ids_lambda_autobackup_' . time(),
                'RoleName'       => $roleName
            ]);
            return $roleArn;
        } catch (AwsException $e) {
            aws_handle_exception($e, '[IAM] Create Role & Put Role Policy');
            return false;
        }
    }
}
