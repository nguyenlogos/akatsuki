<?php
namespace AwsServices;

use Aws\Exception\AwsException as AwsException;
use Aws\S3\S3Client;

class S3 extends Aws
{
    public function __construct($clientConfig = array())
    {
        parent::__construct($clientConfig);
        $this->client = new S3Client($this->getConfig());
    }

    public function createBucket($bucketConfig)
    {
        $bucketName = !empty($bucketConfig['Bucket']) ? $bucketConfig['Bucket'] : null;
        if (empty($bucketName)) {
            throw new \Exception("Failed to create s3 bucket [bucket name is required]", 1);
        }

        $s3Location = empty($bucketConfig['CreateBucketConfiguration']['LocationConstraint']) ?
            $this->getConfig()['region'] : $bucketConfig['CreateBucketConfiguration']['LocationConstraint'];
        $s3Config = [
            'ACL'    => empty($bucketConfig['ACL']) ? 'private' : $bucketConfig['ACL'],
            'Bucket' => $bucketName,
            'CreateBucketConfiguration' => [
                'LocationConstraint'    => $s3Location,
            ]
        ];
        $result = $this->client->createBucket($s3Config);
        if (!empty($result['Location'])) {
            return $result['Location'];
        }

        return null;
    }

    public function updatePolicy($bucketName, $bucketPolicy)
    {
        try {
            return $this->client->putBucketPolicy([
                'Bucket' => $bucketName,
                'Policy' => is_array($bucketPolicy) ? json_encode($bucketPolicy) : $bucketPolicy,
            ]);
        } catch (AwsException $e) {
            aws_handle_exception($e, '[S3] Update Policy');
            return null;
        }
    }

    public function getLocation($s3Bucket)
    {
        try {
            $result = $this->client->getBucketLocation([
                'Bucket' => $s3Bucket
            ]);
            if (!empty($result['LocationConstraint'])) {
                return $result['LocationConstraint'];
            }
        } catch (AwsException $e) {
            aws_handle_exception($e, '[S3] Get Bucket Location');
            return null;
        }

        return null;
    }
}
