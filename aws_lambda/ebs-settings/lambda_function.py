import boto3
from pprint import pprint

from datetime import datetime
from libs.croniter import croniter

def lambda_handler(event, context):
    DT_FORMAT = '%Y-%m-%d %H:%M:%S'

    ec2_region = event['ec2_region'] if 'ec2_region' in event else ""
    ebs_settings = event['ebs_settings'] if 'ebs_settings' in event else []
    if not ec2_region or len(ebs_settings) == 0:
        return False
    ec2_resource = boto3.resource('ec2', region_name=ec2_region)

    now = datetime.now()

    volume_ids = ebs_settings.keys()
    volumes = list(ec2_resource.volumes.filter(VolumeIds=volume_ids).all())
    for volume in volumes:
        settings = ebs_settings[volume.id]
        if 'time' not in settings:
            continue
        iter = croniter(settings['time'], now)
        tags = [{
            'Key' : 'next',
            'Value' : iter.get_next(datetime).strftime(DT_FORMAT)
        }]
        for key in settings:
            tags.append({
                'Key': key,
                'Value': str(settings[key])
            })
        volume.create_tags(Tags=tags)
        print('Settings for volume['+volume.id+'] has been updated.')
    return True
