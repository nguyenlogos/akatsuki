import ec2

from datetime import datetime
from libs.croniter import croniter
from pprint import pprint

def backup(ec2_region):
    # connect to region
    ec2_client = ec2.get_client(ec2_region)
    ec2_resource = ec2.get_resource(ec2_region)
    # list all ebs volumes
    ebs_volumes = ec2_resource.volumes.filter(Filters=[
        {
            'Name' : 'status',
            'Values' : [
                'in-use'
            ]
        }
    ])

    DT_FORMAT = '%Y-%m-%d %H:%M:%S'

    for ebs_volume in ebs_volumes:
        # skip backup if settings were not found
        tags = ebs_volume.tags
        if not tags:
            continue
        tnext = None
        tintv = None
        for tag in tags:
            if tag['Key'] != 'next':
                if tag['Key'] == 'time':
                    tintv = tag['Value']
                continue
            tnext = datetime.strptime(tag['Value'], DT_FORMAT)
        if not tnext or not tintv:
            continue
        tnow = datetime.now()
        if tnow < tnext:
            continue
        ebs_volume.create_snapshot(
            Description="Ebs snapshot created by Lambda autobackup"
        )
        print('Snapshot for volume['+ebs_volume.id+'] has been created.')

        # set next time update
        iter = croniter(tintv, tnext)
        tnext = iter.get_next(datetime)
        ebs_volume.create_tags(Tags=[
            {
                'Key' : 'next',
                'Value' : tnext.strftime(DT_FORMAT)
            }
        ])
