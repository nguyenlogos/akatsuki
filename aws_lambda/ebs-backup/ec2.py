import boto3ap-northeast-1

def list_regions():
    ec2_client = boto3.client('ec2')
    return ec2_client.describe_regions().get('Regions', [])

def get_client(ec2_region):
    return boto3.client('ec2', region_name=ec2_region)

def get_resource(ec2_region):
    return boto3.resource('ec2', region_name=ec2_region)
