import ec2
import ebs

def lambda_handler(event, context):
    regions = ec2.list_regions()
    for region in regions:
        ebs.prune(region['RegionName'])
