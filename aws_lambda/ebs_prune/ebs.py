import ec2
import re

from datetime import datetime
from pprint import pprint

def prune(ec2_region):
    # connect to region
    ec2_client = ec2.get_client(ec2_region)
    ec2_resource = ec2.get_resource(ec2_region)
    # list all ebs volumes
    ebs_volumes = ec2.describe_volumes(ec2_client)

    for ebs_volume in ebs_volumes:
        # skip if settings were not found
        if 'Tags' not in ebs_volume:
            continue
        num_copies = -1
        for tag in ebs_volume['Tags']:
            if tag['Key'] == 'copy':
                num_copies = int(tag['Value'])
                break
        if num_copies <= 0:
            continue
        snapshots = ec2.describe_snapshots(ec2_client, ebs_volume['VolumeId'])
        snapshots = filter(lambda ss:re.match(r'^(?!Created by CreateImage).*', ss['Description']), snapshots)
        num_snaps = len(snapshots)
        if num_snaps == 0:
            continue
        num_remove = num_snaps - num_copies
        if num_remove <= 0:
            continue
        snapshots = sorted(snapshots, key=lambda ss:ss['StartTime'])
        ebs_snapshots = snapshots[:num_remove]
        if len(ebs_snapshots) == 0:
            continue
        for ebs_snapshot in ebs_snapshots:
            snapshot = ec2_resource.Snapshot(ebs_snapshot['SnapshotId'])
            snapshot.delete()
            print('Snapshot['+snapshot.id+'] has been deleted.')

def prune_old(ec2_region):
    ec2_client = ec2.get_client(ec2_region)
    ec2_resource = ec2.get_resource(ec2_region)
    instances = ec2_resource.instances.all()
    for instance in instances:
        volumes = instance.volumes.all()
        for volume in volumes:
            tags = volume.tags
            if not tags:
                continue
            num_copies = -1
            for tag in tags:
                if tag['Key'] == 'copy':
                    num_copies = int(tag['Value'])
                    break
            if num_copies <= 0:
                continue
            snapshots = ec2.describe_snapshots(ec2_client, volume.id)
            snapshots = filter(lambda ss:re.match(r'^(?!Created by CreateImage).*', ss['Description']), snapshots)
            num_snaps = len(snapshots)
            if num_snaps == 0:
                continue
            num_remove = num_snaps - num_copies
            if num_remove <= 0:
                continue
            snapshots = sorted(snapshots, key=lambda ss:ss['StartTime'])
            ebs_snapshots = snapshots[:num_remove]
            if len(ebs_snapshots) == 0:
                continue
            for ebs_snapshot in ebs_snapshots:
                snapshot = ec2_resource.Snapshot(ebs_snapshot['SnapshotId'])
                snapshot.delete()
                print('Snapshot['+snapshot.id+'] has been deleted.')
