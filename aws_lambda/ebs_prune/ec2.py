import boto3

def describe_volumes(ec2_client):
    return ec2_client.describe_volumes(
        Filters=[
        {
            'Name': 'status',
            'Values': ['in-use']
        }
    ]).get('Volumes', [])

def describe_snapshots(ec2_client, volume_id):
    return ec2_client.describe_snapshots(
        Filters=[
        {
            'Name': 'volume-id',
            'Values': [volume_id]

        }
    ]).get('Snapshots', [])

def list_regions():
    ec2_client = boto3.client('ec2')
    return ec2_client.describe_regions().get('Regions', [])

def get_client(ec2_region):
    return boto3.client('ec2', region_name=ec2_region)

def get_resource(ec2_region):
    return boto3.resource('ec2', region_name=ec2_region)
