<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="{$metaKeyword}" />
  <meta name="description" content="{$description}" />
  <title>{$PAGE_TITLE}{$pageTitle}</title>

  <link rel="icon" href="favicon.ico" type="image/x-icon">

  <link href="https://fonts.googleapis.com/earlyaccess/notosansjapanese.css" rel="stylesheet" />
  <link href="/src/assets/css/normalize.css" rel="stylesheet">
  <script src="/src/assets/js/jquery.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/jquery-ui.js" type="text/javascript"></script>
  <script src="https://selectize.github.io/selectize.js/js/selectize.js"></script>
  {*<script src="/src/assets/js/bootstrap.min.js" type="text/javascript"></script>*}
  <script src="/src/assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/bootstrap-v4.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/toastr.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/common.js" type="text/javascript"></script>
  <script src="/src/assets/js/utf8.js" type="text/javascript"></script>
  <script src="/src/assets/js/lodash.js" type="text/javascript"></script>
  <script src="/src/assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="/src/assets/js/bootstrap-datepicker.ja.js" type="text/javascript"></script>
  <script src="/src/assets/js/jquery.validate.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/custom_validate.js" type="text/javascript"></script>
  <script src="/src/assets/js/jquery.inputmask.bundle.js" type="text/javascript"></script>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script src="/src/assets/js/popper.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/effect-common.js" type="text/javascript"></script>
  <link href="/src/assets/css/fontawesome.v5.css" rel="stylesheet">
  <link href="/src/assets/css/toastr.min.css" rel="stylesheet" type="text/css" />
  <link href="/src/assets/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
  <link href="/src/assets/css/bootstrap-v4.min.css" rel="stylesheet" type="text/css" />
  <link href="/src/assets/css/default.css" rel="stylesheet" type="text/css" />
  <link href="/src/assets/css/style.css" rel="stylesheet" type="text/css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css" rel="stylesheet" type="text/css" />
  <script src="/src/assets/js/dhtmlx/dhtmlx.js" type="text/javascript"></script>
  <link href="/src/assets/css/dhtmlx/dhtmlx.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="wrapper">
  <!-- 0-1 page-header -->
  <header>
    <!-- site-logo -->
    <div class="logo"><a href="{if $userID != ""}/db/db.php{else}/{/if}"><img src="/src/assets/images/sunnyview-logo.png" alt="Sunny View" /></a></div>
    <!-- /site-logo -->
    <!-- header-nav -->
    <div class="sp sub-nav-button"><span>-</span></div>
    <div class="sub-nav">
      {if $userID != ""}
      <ul>
        <li><a href="/db/db.php">ダッシュボード</a></li>
        <li class="dropdown">
          <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">設定</a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li class="{$STATE_ROLES['mst/dept.php']['visible']} dropdown-item"><a href="/mst/dept.php">グループ管理</a></li>
            <li class="{$STATE_ROLES['mst/user.php']['visible']} dropdown-item"><a href="/mst/user.php">ユーザー管理</a></li>
            <li class="{$STATE_ROLES['mst/proj.php']['visible']} dropdown-item"><a href="/mst/proj.php">プロジェクト管理</a></li>
            <li class="{$STATE_ROLES['mst/instowner.php']['visible']} dropdown-item"><a href="/mst/instowner.php">プロジェクト・リソース管理</a></li>
            <li class="{$STATE_ROLES['mst/userpj.php']['visible']} dropdown-item"><a href="/mst/userpj.php">プロジェクト・ユーザー管理</a></li>
            <li class="{$STATE_ROLES['admin/key.php']['visible']} dropdown-item"><a href="/admin/key.php"><i class="fas fa-link"></i> AWS API認証情報の設定</a></li>
            <li class="{$STATE_ROLES['admin/ami.php']['visible']} dropdown-item"><a href="/admin/ami.php">AMI管理</a></li>
            <li class="{$STATE_ROLES['admin/az.php']['visible']} dropdown-item"><a href="/admin/az.php">データセンター管理</a></li>
            <li class="{$STATE_ROLES['admin/insttype.php']['visible']} dropdown-item"><a href="/admin/insttype.php">インスタンスタイプ管理</a></li>
            <li class="{$STATE_ROLES['admin/obj.php']['visible']} dropdown-item"><a href="/admin/obj.php">オブジェクト管理</a></li>
            <li class="{$STATE_ROLES['admin/sg.php']['visible']} dropdown-item"><a href="/admin/sg.php">セキュリティグループ管理</a></li>
            <li class="{$STATE_ROLES['admin/cw_rules.php']['visible']} dropdown-item"><a href="/admin/cw_rules.php"><i class="fas fa-clock"></i> バックアップ・ルール管理</a></li>
            <li class="{$STATE_ROLES['cons/profile.php']['visible']} dropdown-item"><a href="/cons/profile.php"><i class="fas fa-user"></i> プロファイル</a></li>
            <li class="{$STATE_ROLES['mst/changepw.php']['visible']} dropdown-item"><a href="/mst/changepw.php"><i class="fas fa-lock"></i> パスワードの変更</a></li>
          </ul>
        </li>
        <li><a href="#">ヘルプ</a></li>
        <li><a href="/src/logout.php">ログアウト</a></li>
      </ul>
      {/if}
    </div>
    <!-- /header-nav -->
  </header>
  <!-- / 0-1 page-header -->
  <!-- 0-2 side-manu -->
  {if $userID != ""}
  <nav class="side-manu">
    <!-- user-ID -->
    <div class="menu-user-id">ユーザーID: {$userID|escape}</div>
    <!-- /user-ID -->
    <!-- menu -->
    <ul>
      <li>
        <a href="/cons/info.php?status=-1">
          <i class="fas fa-bell fa-fw"></i>
          <span>お知らせ</span>
          <span class="badge badge-small badge-pill badge-orange">{$infocount}</span>
        </a>
      </li>
      <li>
        <a href="/cons/request.php">
          <i class="fas fa-list fa-fw"></i>
          <span>申請一覧</span>
          <span class="badge badge-small badge-pill badge-orange">{$wfcount|default}</span>
        </a>
      </li>
      <li><a href="/cons/inst.php">
          <i class="fas fa-folder fa-fw"></i>
          <span>インスタンス一覧</span>
        </a>
      </li>
      <li>
        <a href="/cons/req_inst.php">
          <i class="fas fa-edit fa-fw"></i>
          <span>インスタンス作成の申請</span>
        </a>
      </li>
      <li>
        <a href="/admin/ebs.php">
          <i class="fab fa-gitter fa-fw" aria-hidden="true"></i>
          <span>ディスク追加・変更の申請</span>
        </a>
      </li>
      <li>
        <a href="/cons/user_inst.php">
          <i class="fas fa-random fa-fw"></i>
          <span>リソースの移動</span>
        </a>
      </li>
      <li>
        <a href="/admin/log.php">
          <i class="fas fa-eye fa-fw"></i>
          <span>監査ログ</span>
        </a>
      </li>
      <li>
        <a href="/admin/backup.php">
          <i class="fas fa-inbox fa-fw"></i>
          <span>自動バックアップ</span>
        </a>
      </li>
      <li class="{$STATE_ROLES['db/configuration.php']['visible']}">
        <a href="/db/configuration.php">
          <i class="fas fa-cog fa-fw"></i>
          <span>システム設定</span>
        </a>
      </li>
    </ul>
    <!-- /menu -->
    <!-- switching-button -->
    <div class="switching-button">
      <p><i class="fas fa-angle-double-right"></i></p>
    </div>
    <!-- /switching-button -->
  </nav>
  <!-- / 0-2 side-manu -->
  {/if}
  <!-- 0-3 contents -->
  <main{if $userID == ""} class="main-full-width"{/if}>
    <div class="container-fluid">
      <section class="container">
        {if $pageTitle}<h1>{$pageTitle}</h1>{/if}
        {notification}

        {include file=$viewTemplate}
      </section>
    </div>
  </main>
  <!-- / 0-3 contents -->
  <!-- 0-4 page-footer -->
  <footer>
    <p class="text-center">Copyright (c) 2018 IDS Corp. All Rights Reserved.</p>
  </footer>
  <!-- / 0-4 page-footer -->
</div>
</body>
</html>
