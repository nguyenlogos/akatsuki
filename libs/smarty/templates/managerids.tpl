<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <script src="/src/assets/js/jquery.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/jquery-ui.js" type="text/javascript"></script>
  <script src="/src/assets/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/toastr.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/common.js" type="text/javascript"></script>
  <script src="/src/assets/js/utf8.js" type="text/javascript"></script>
  <script src="/src/assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="/src/assets/js/bootstrap-datepicker.ja.js" type="text/javascript" charset="UTF-8"></script>
  <script src="/src/assets/js/jquery.validate.min.js" type="text/javascript"></script>
  <script src="/src/assets/js/custom_validate.js" type="text/javascript"></script>
  <script src="/src/assets/js/custom_validate2.js" type="text/javascript"></script>
  <script src="/src/assets/js/jquery.inputmask.bundle.js" type="text/javascript"></script>

  <link href="/src/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="/src/assets/css/toastr.min.css" rel="stylesheet" type="text/css" />
  <link href="/src/assets/css/style.css" rel="stylesheet" type="text/css" />
  <link href="/src/assets/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />

  <meta name="keywords" content="{$metaKeyword}" />
  <meta name="description" content="{$description}" />
  <title>AWS API認証情報の設定</title>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/managerids/">Sunny-CON</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        {if $userID != ''}
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="glyphicon glyphicon-cog"></i> 設定
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="/managerids/profile/changepw.php"><i class="glyphicon glyphicon-lock"></i> パスワードの変更</a></li>
            </ul>
          </li>
            <li><a href="/managerids/logout.php"><i class="glyphicon glyphicon-remove"></i> ログアウト</a></li>
        {/if}
      </ul>
    </div>
  </div>
</nav>
<div class="container-fluid" style="min-height: 935px;">
  <div class="row">
	  {if $userID != ""}
        <div class="col-xs-2 bd-sidebar">
          <ul class="nav nav-sidebar" id="sticky-sidebar">
            <li>ユーザID : {$userID|escape}</li>
            <li  class="active"><a href="/managerids/etp.php"><i class="glyphicon glyphicon-list-alt"></i> 契約企業管理</a></li>
            <li  class="active"><a href="/managerids/invoice.php"><i class="glyphicon glyphicon-list-alt"></i> 請求処理</a></li>
            <li>
              <a href="/managerids/noti/notification.php"><i class="glyphicon glyphicon-comment"></i> お知らせ</a>
            </li>
            <li><a href="/managerids/rate/exchangerate.php"><i class="glyphicon glyphicon-yen"></i> 為替レート</a></li>
          </ul>
        </div>
	  {/if}

      <div class="col-xs-8 col-xs-offset-3 main">
        {if $pageTitle}<h1 class="page-header">{$pageTitle}</h1>{/if}
        <div class="row placeholders">
            {include file=$viewTemplate}
        </div>
      </div>

  </div>
</div>

<footer id="main_footer">
  <p class="text-center">Copyright (c) 2018 IDS Corp. All Rights Reserved.</p>
</footer>

</body>
</html>

