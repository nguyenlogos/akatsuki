<style type="text/css">
    img {
        max-width: 100%;
        width: auto;
    }
</style>
<div class="text-center">
    <p><img src="/src/assets/images/404.png" alt="404 エラー!"/></p>
    {*<h1>404 エラー!</h1>*}
    <p>探しているページを見つけませんでした。</p>
    <p class="mt_30">
        <a href="/" class="btn btn-orange">&raquo; ログイン画面</a>
    </p>
</div>
