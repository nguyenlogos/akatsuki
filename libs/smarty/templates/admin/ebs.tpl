{if $msg != ''}
    <div id="message">
        <div class="alert alert-dismissible alert-info">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    </div>
{/if}
{if $errmsg != ''}
    <div id="message">
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    </div>
{/if}
<div class="container-editable">
ディスクの追加・変更を行うインスタンスを選択して、[表示]ボタンを押してください。<br /><br />
    <form>
        <div class="form-row">
            <div class="form-group col-md-6 my-1" data-dropdown="proj" data-selected="{$frmValues['proj']}"></div>
            <div class="form-group col-md-5 my-1" data-dropdown="inst" data-selected="{$frmValues['inst']}"></div>
            <div class="form-group col-md-1 my-1">
                <button type="button" class="btn btn-orange btn-filter">表示</button>
            </div>
        </div>
    </form>
    <form name="frm-ebs-volumes">
        <div class="table-responsive" id="tbl-volumes"></div>
    </form>
    <div class="hdd-choice hide">
        <div class="form-row">
            <div class="form-group col-md-10 my-1">
                <div class="form-row">
                    <div class="form-group col-md-4 my-1">
                        <select name="sel-hdd" class="form-control">
                            <option value="">--- ディスクの追加 ---</option>
                            <option value="existing">既存のディスクを接続</option>
                            <option value="new">新しいディスクを追加</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4 my-1" data-dropdown="dhdd" style="display: none";></div>
                    <div class="form-group col-md-2 my-1">
                        <button type="button" class="btn btn-green btn-add-hdd"><i class="fas fa-plus-circle"></i>追加</button>
                    </div>
                </div>
            </div>
            <div class="form-group col-md-2 my-1 text-right">
                <button type="button" class="btn btn-blue btn-save" disabled>保存</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var $sel_proj = generateProjsTemplate();
        var $sel_inst = generateIntsTemplate();

        var $container_proj = $('[data-dropdown="proj"]');
        var $container_inst = $('[data-dropdown="inst"]');

        var g_selected_proj = $container_proj.data('selected');
        var g_selected_inst = $container_inst.data('selected');
        var g_instance_zone = '';
        var g_hddOriginalInfos = {};

        $container_proj.empty().append($sel_proj);
        $container_inst.empty().append($sel_inst);

        var EBS_SIZE_CONFIG = {
            io1 : {
                minSize : 4,
                maxSize : 16384,
                minIops : 100,
                maxIops : 32000,
            },
            gp2 : {
                minSize : 1,
                maxSize : 16384
            },
            standard : {
                minSize : 1,
                maxSize : 1024
            },
        };

        var TABLE_HEADERS = {
            volid : {
                width: 175,
                text: 'ボリュームID',
            },
            device : {
                width: 130,
                text: 'デバイス名',
            },
            size : {
                width: 100,
                text: 'サイズ(GiB)',
            },
            type : {
                width: 130,
                text: 'ボリュームタイプ',
            },
            iops : {
                width: 100,
                text: 'IOPS',
            },
            del_term : {
                text: '自動削除',
                invisible : true,
            },
            encrypted : {
                text: '暗号化',
                invisible : true,
            },
            state : {
                text: '状態',
            },
            action : {
                width: 40,
                text: '',
            },
        }

        function loadProjs(dept) {
            return window.util.ajax('/api/proj.php?all=1', {
                'dept' : dept
            });
        }

        function loadInsts(proj) {
            return window.util.ajax('/api/inst.php', {
                'proj' : proj
            });
        }

        function loadEbsVolumes(inst) {
            return window.util.ajax('/api/ebs.php?action=change', {
                'inst' : inst
            });
        }

        function loadAvailableEbsVolumes(zone) {
            return window.util.ajax('/api/ebs.php', {
                'zone' : zone
            });
        }

        function generateProjsTemplate(projs, selected, onchangeCallback) {
            if (!projs || !projs.length) {
                projs = [];
            }
            var $optionDefault =
                $('<option>').attr('value', '').text('--- プロジェクトを選択 ---');
            var $select = $('<select>')
                    .attr('name', 'proj')
                    .addClass('form-control')
                    .append($optionDefault);
                if (typeof onchangeCallback === 'function') {
                    $select.change(function(){
                        onchangeCallback(this.value);
                    })
                };
            projs.forEach(function(proj){
                var $option =
                    $('<option>')
                        .attr('value', proj.pcode)
                        .text(proj.pname + ' (' +proj.pcode+ ')');
                if (selected && selected == proj.pcode) {
                    $option.attr('selected', 'selected');
                }
                $select.append($option);
            });
            return $select;
        }

        function generateIntsTemplate(insts, selected, onchangeCallback) {
            if (!insts || !insts.length) {
                insts = [];
            }
            var $optionDefault =
                $('<option>').attr('value', '').text('--- インスタンスを選択 ---');
            var $select = $('<select>')
                    .attr('name', 'inst')
                    .addClass('form-control')
                    .append($optionDefault);
            if (typeof onchangeCallback === 'function') {
                $select.change(function(){
                    onchangeCallback(this.value);
                })
            }
            insts.forEach(function(inst){
                var $option =
                    $('<option>')
                        .attr('value', inst.id)
                        .attr('data-zone', inst.availabilityzone)
                        .text(inst.name + ' (' +inst.id+ ')')
                    if (selected && selected == inst.id) {
                        $option.attr('selected', 'selected');
                    }
                $select.append($option);
            });
            return $select;
        }

        function generateVolumesTemplate(volumes, selected, onchangeCallback) {
            if (!volumes || !volumes.length) {
                volumes = [];
            }
            var $optionDefault =
                $('<option>').attr('value', '').text('--- ディスクを選択 ---');
            var $select = $('<select>')
                    .attr('name', 'dhdd')
                    .addClass('form-control')
                    .append($optionDefault);
            if (typeof onchangeCallback === 'function') {
                $select.change(function(){
                    onchangeCallback(this.value);
                })
            }
            volumes.forEach(function(volume){
                var $option =
                    $('<option>')
                        .attr('value', volume.id)
                        .text(volume.id + ' (' +volume.size+ ' GB)')
                    if (selected && selected == volume.id) {
                        $option.attr('selected', 'selected');
                    }
                $select.append($option);
            });
            return $select;
        }

        function generateDevicesTemplate(selected) {
            var listDevices = _.map('defghijklmnopqrstuvwxyz'.split(''), function(device){
                return '/dev/sd' + device;
            });
            var $select = $('<select class="form-control ebs_device">');
            listDevices.forEach(function(device){
                var $option = $('<option>').attr('value', device).text(device);
                $select.append($option);
            });
            if (selected) {
                $selecteOption = $select.find('[value="'+selected+'"]');
                if ($selecteOption.length > 0) {
                    $selecteOption.attr('selected', 'selected');
                } else {
                    $('<option>').attr({
                        value : selected,
                        selected : 'selected',
                    }).text(selected).appendTo($select);
                }
            }
            return $select;
        }

        function generateDeviceTypesTemplate(selected) {
            var $select = $(`
                <select class="form-control ebs_device_type">
                    <option value="standard">磁気ディスク</option>
                    <option value="gp2">汎用SSD</option>
                    <option value="io1">IOPS提供型SSD</option>
                </select>
            `);
            if (selected) {
                $selecteOption = $select.find('[value="' + selected + '"]');
                if ($selecteOption.length > 0) {
                    $selecteOption.attr('selected', 'selected');
                } else {
                    $('<option>').attr({
                        value : selected,
                        selected : 'selected',
                    }).text(selected).appendTo($select);
                }
            }
            return $select;
        }

        loadProjs().then(function(projs){
            var onchangeCallback = function(selected, initial) {
                g_selected_proj = selected || undefined;
                if (!initial) {
                    g_selected_inst = undefined;
                }
                window.util.setParamsURL({
                    proj : selected,
                    inst : g_selected_inst
                });
                $sel_inst.attr('disabled', 'disabled').removeClass('error');
                if (!g_selected_proj) {
                    $sel_inst = generateIntsTemplate();
                    $container_inst.empty().append($sel_inst);
                } else {
                    loadInsts(g_selected_proj).then(function(insts){
                        if (g_selected_inst) {
                            var result = _.find(insts, function(inst){
                                return inst.id == g_selected_inst;
                            });
                            if (!result) {
                                window.util.setParamsURL({
                                    inst : undefined
                                });
                            }
                        }
                        $sel_inst = generateIntsTemplate(insts, g_selected_inst, function(selected){
                            window.util.setParamsURL({
                                inst : selected
                            });
                            g_selected_inst = selected;
                            if (!selected) {
                                $sel_inst.addClass('error');
                            } else {
                                $sel_inst.removeClass('error');
                            }
                        });
                        $container_inst.empty().append($sel_inst);
                        var init = window.util.getParamUrl('init');
                        if (+init === 1) {
                            $btnFilter.trigger('click');
                        }
                    });
                }
            };
            $sel_proj = generateProjsTemplate(projs, g_selected_proj, onchangeCallback);
            $container_proj.empty().append($sel_proj);
            if (g_selected_proj) {
                onchangeCallback(g_selected_proj, true);
            }
        });

        var $btnSave = $('.btn-save');
        var $btnFilter = $('.btn-filter');
        var $frmEbsVolumes = $('form[name="frm-ebs-volumes"]');
        var $container_editable = $('.container-editable');

        var $hddChoice = $('.hdd-choice');
        var $sel_hdd = $hddChoice.find('[name="sel-hdd"]');
        var $sel_dhdd = generateVolumesTemplate();
        var $container_dhdd = $('[data-dropdown="dhdd"]');

        var $table = $('<table class="table table-hover sort-table sortable">');

        var $tr = $('<tr>');
        for (var field in TABLE_HEADERS) {
            var setttings = TABLE_HEADERS[field];
            var $th = $('<th>').addClass(field).text(setttings.text);
            if (setttings.width > 0) {
                $th.css({
                    // 'width' : setttings.width,
                    'min-width' : setttings.width,
                });
            }
            if (setttings.invisible) {
                $th.addClass('hide');
            }
            $th.appendTo($tr);
        }
        $('<thead>').append($tr).appendTo($table);
        var $tbody = $('<tbody>');
        var g_tableData = []; // all ebs volumes of selected instance
        var g_dhdd = []; // all unmounted ebs volumes
        var g_is_dirty = false; // table is dirty or not
        var g_ebs_del_term = false;

        $table.append($tbody).hide().appendTo('#tbl-volumes');

        function resetState() {
            g_is_dirty = false;
            g_ebs_del_term = false;
            g_dhdd = [];
            g_tableData = [];
            $sel_hdd.val("");
            $sel_dhdd.val("");
            $table.hide();
            $hddChoice.addClass('hide');
            $container_dhdd.hide();
            $btnSave.attr('disabled', 'disabled');
        };

        function generateRowTemplate(volume, onRemoveCallback) {
            var $deviceTemplate = generateDevicesTemplate(volume.device);
            var $btnDelete = "";

            if (!volume.is_root) {
                $btnDelete = $('<button type="button" class="btn btn-sm btn-danger">').text("削除")
                $btnDelete.click(function(){
                    if (typeof onRemoveCallback === 'function') {
                        var $targetRow = $(this).closest('tr');
                        onRemoveCallback($targetRow);
                    }
                });
            } else {
                g_ebs_del_term = volume.del_term;
            }

            var $row = $('<tr>').attr('data-item-id', volume.id);
            var volid = '',
                state = '';
            if (volume.id.match(/^vol-/)) {
                volid = volume.id;
                state = volume.state;
            }
            var inputs = {
                $volid : $('<span>').text(volid),
                $device : generateDevicesTemplate(volume.device)
                    .attr({
                        'name' : 'ebs_device_'+volume.id,
                        'disabled' : volume.is_root
                    }),
                $size :
                    $('<input>').attr({
                        type : "number",
                        class : "form-control",
                        name : "ebs_size_"+volume.id,
                        value : volume.size,
                        required : "required"
                    }),
                $type : generateDeviceTypesTemplate(volume.type)
                    .attr('name', 'ebs_type_'+volume.id),
                $iops :
                    $('<input>').attr({
                        type : "number",
                        class : "form-control",
                        name : "ebs_iops_"+volume.id,
                        value : volume.iops,
                    }),
                $del_term :
                    $('<input>').attr({
                        type : "checkbox",
                        name : "ebs_del_term"+volume.id,
                        disabled  : 'disabled'
                    }),
                $encrypted :
                    $('<input>').attr({
                        type : "checkbox",
                        name : "ebs_encrypted"+volume.id,
                    }),
                $state : $('<span>').text(state),
            };
            var elements = {
                $volid : $('<td class="device">').append(inputs.$volid),
                $device : $('<td class="device">').append(inputs.$device),
                $size : $('<td class="size">').append(inputs.$size),
                $type : $('<td class="type">').append(inputs.$type),
                $iops : $('<td class="iops">').append(inputs.$iops),
                $del_term : $('<td class="del_term">').append(inputs.$del_term),
                $encrypted : $('<td class="encrypted">').append(inputs.$encrypted),
                $state : $('<td class="state">').append(inputs.$state),
                $action : $('<td class="action">').append($btnDelete),
            };
            if (g_ebs_del_term) {
                inputs.$del_term.attr({
                    checked : 'checked'
                });
            }

            if (!volume.editable) {
                var editableFields = ['$device'];
                if (volume.can_modify) {
                    editableFields.push('$size');
                }
                var nonEditableInputs = _.differenceWith(Object.keys(inputs), editableFields);
                nonEditableInputs.forEach(function(key){
                    inputs[key].attr('disabled', 'disabled');
                });
                editableFields.forEach(function(key){
                    inputs[key].change(function(){
                        updateRowData($row, volume);
                    });
                });
            } else {
                inputs.$type.change(function(){
                    var selected = $(this).val();
                    var config = EBS_SIZE_CONFIG[selected];
                    var minSize = g_hddOriginalInfos[volume.id] ? +g_hddOriginalInfos[volume.id].size : config.minSize;
                    inputs.$size.attr({
                        min : minSize,
                        max : config.maxSize,
                    });
                    if (selected !== 'io1') {
                        inputs.$iops.removeAttr('required').hide();
                    } else {
                        var value = +inputs.$iops.val();
                        if (value < config.minIops) {
                            value = config.mineIops;
                        } else if (value > config.maxIops) {
                            value = config.maxIops;
                        }
                        inputs.$iops.attr({
                            min : config.minIops,
                            max : config.maxIops,
                            required : 'required',
                        }).val(value).show();
                    }
                    updateRowData($row, volume);
                });
                var editableInputs = _.differenceWith(Object.keys(inputs), ['$type', '$sel_term']);
                editableInputs.forEach(function(key){
                    inputs[key].change(function(){
                        updateRowData($row, volume);
                    });
                });
            }

            var selected = inputs.$type.val();
            var config = EBS_SIZE_CONFIG[selected];
            var minSize = g_hddOriginalInfos[volume.id] ? +g_hddOriginalInfos[volume.id].size : config.minSize;
            inputs.$size.attr({
                min : minSize,
                max : config.maxSize,
            });

            if (volume.encrypted) {
                inputs.$encrypted.attr('checked', 'checked');
            }
            if (volume.type !== 'io1') {
                inputs.$iops.hide();
            } else {
                inputs.$iops.attr('required', 'required').show();
            }

            for (var field in TABLE_HEADERS) {
                if (TABLE_HEADERS[field].invisible) {
                    elements['$'+field].addClass('hide');
                }
            }

            $row.append(
                elements.$volid,
                elements.$device,
                elements.$size,
                elements.$type,
                elements.$iops,
                elements.$del_term,
                elements.$encrypted,
                elements.$state,
                elements.$action,
            );
            return $row;
        };

        function generateVolumesTable(volumes) {
            $tbody.empty();
            if (volumes.length === 0) {
                $tbody.append(`
                    <tr>
                        <td colspan="9" class="text-center">データがありません。</td>
                    </tr>
                `);
                $hddChoice.addClass('hide');
            } else {
                volumes.forEach(function(volume){
                    var $row = generateRowTemplate(volume, function($target){
                        removeTableRow($target, volume)
                    });
                    $tbody.append($row);
                });
                $hddChoice.removeClass('hide');
            }
        };

        function addNewTableRow(volume) {
            if ($btnSave.is_saving) {
                return;
            }

            g_tableData.push(volume);

            var $newRowTemplate = generateRowTemplate(volume, function($target){
                removeTableRow($target, volume);
            });
            $table.append($newRowTemplate);
            updateRowData($newRowTemplate, volume);

            g_is_dirty = true;
            $btnSave.removeAttr('disabled');
        };

        function removeTableRow($row, volume) {
            if ($btnSave.is_saving) {
                return;
            }

            g_tableData = _.differenceWith(g_tableData, [volume], _.isEqual);
            if (volume.id.match(/^vol-/)) {
                g_dhdd = _.unionWith(g_dhdd, [volume], _.isEqual);
            }

            $sel_dhdd = generateVolumesTemplate(g_dhdd);
            $container_dhdd.empty().append($sel_dhdd);

            $row.remove();
            g_is_dirty = true;
            $btnSave.removeAttr('disabled');
        };

        function updateRowData($row, volume) {
            if (!volume.id.match(/^vol-/)) {
                volume.type = $row.find('td.type > select').val();
                volume.encrypted = $row.find('td.encrypted > input').is(':checked');
                if (volume.type === 'io1') {
                    volume.iops = $row.find('td.iops > input').val();
                }
            } else {
                if (!volume.device_og) {
                    volume.device_og = volume.device;
                }
                if (!volume.size_og) {
                    volume.size_og = volume.size;
                }
            }
            volume.device = $row.find('td.device > select').val();
            volume.size = +$row.find('td.size > input').val();

            g_is_dirty = true;
            $btnSave.removeAttr('disabled');
        };

        function generateTableData() {
            return {
                list_change : _.filter(g_tableData, function(item){
                    var changed = !item.attached;
                    if (!changed && item.id.match(/^vol-/)) {
                        if ((item.size_og && item.size_og != item.size) ||
                            (item.device_og && item.device_og != item.device)
                        ) {
                            changed = true;
                        }
                    }
                    return changed;
                }),
                list_detach: _.filter(g_dhdd, function(item){
                    return item.attached;
                })
            }
        };

        function degenerateTableData(data) {
            g_hddOriginalInfos = {};
            g_tableData.forEach(function(hdd){
                g_hddOriginalInfos[hdd.id] = {
                    size : hdd.size
                };
            });
            g_dhdd.forEach(function(hdd){
                g_hddOriginalInfos[hdd.id] = {
                    size : hdd.size
                };
            });
            g_tableData = _.uniqBy(g_tableData.concat(data.list_change), 'id');
            g_tableData = _.differenceWith(g_tableData, data.list_detach, function(a, b){
                return a.id == b.id;
            });
            // TODO: check if volume exists
            g_dhdd = _.differenceWith(g_dhdd, g_tableData, function(a, b){
                return a.id == b.id;
            });
            g_dhdd =  _.unionWith(g_dhdd, data.list_detach, function(a, b){
                return a.id == b.id;
            });

            g_tableData.forEach(function(item){
                if (!item.id.match(/^vol-/)) {
                    item.editable = true;
                    item.can_modify = true;
                    item.size = +item.size;
                } else {
                    var volume = _.find(data.list_change, ['id', item.id]);
                    if (volume) {
                        item.size = +volume.size;
                        item.device = volume.device;
                    }
                }
            });

            generateVolumesTable(g_tableData);
            $table.show();
        };

        function toggleReadOnly() {
            if ($container_editable.readonly) {
                g_is_dirty && $btnSave.removeAttr('disabled');
                $btnSave.is_saving = false;
                $container_editable.css({
                    'z-index' : ''
                });
            } else {
                $btnSave.is_saving = true;
                $btnSave.attr('disabled', 'disabled');
                $container_editable.css('z-index', -1000);
            }
            $container_editable.readonly = !$container_editable.readonly;
        }

        $sel_hdd.change(function(){
            var selected = $(this).val();
            if (selected === 'existing') {
                $sel_dhdd = generateVolumesTemplate(g_dhdd);
                $container_dhdd.empty().append($sel_dhdd).show();
            } else {
                $container_dhdd.hide();
            }
        });

        $btnFilter.click(function(e){
            if (!g_selected_inst && !g_selected_proj) {
                return;
            }
            if ($btnSave.is_saving) {
                return;
            }
            if (g_selected_proj && g_selected_proj != -1 && !g_selected_inst) {
                $sel_inst.addClass('error');
            } else {
                resetState();
                g_instance_zone = $sel_inst.find("option:selected").data("zone");
                $btnFilter.attr('disabled', 'disabled');
                var tableData = undefined;
                var q1 = loadEbsVolumes(g_selected_inst).then(function(response){
                    if (response && response.data) {
                        tableData = response.data;
                        g_tableData = tableData.list;
                    }
                });
                var q2 = loadAvailableEbsVolumes(g_instance_zone).then(function(response){
                    g_dhdd = response.data;
                });
                window.util.ajaxAll([q1, q2]).then(()=>{}).always(function(){
                    $btnFilter.removeAttr('disabled');
                    tableData && degenerateTableData(tableData);
                });
            }
        });

        $('.btn-add-hdd').click(function(){
            var selected = $sel_hdd.val();
            if (!selected) {
                return;
            }
            if (selected === 'existing') {
                var selected_hdd = $sel_dhdd.val();
                if (selected_hdd) {
                    var index = _.findIndex(g_dhdd, ['id', selected_hdd]);
                    var selected = index > -1 ? g_dhdd.splice(index, 1) : undefined;
                    if (selected) {
                        $sel_dhdd = generateVolumesTemplate(g_dhdd);
                        $container_dhdd.empty().append($sel_dhdd);
                        addNewTableRow(selected[0]);
                    }
                } else {
                    return;
                }
            } else if (selected === 'new') {
                var volumes = _.filter(g_tableData, function(volume){
                    return volume.editable === true;
                });
                var volume = _.maxBy(volumes, function(volume){
                    return !volume.editable ? 0 : volume.id;
                });
                var id = volume ? +volume.id+ 1 : 1;
                var volume = {
                    id : id + "",
                    del_term : g_ebs_del_term,
                    device : "",
                    encrypted : false,
                    attached : false,
                    iops : 0,
                    is_root : false,
                    size : 1,
                    type : "",
                    state : "",
                    zone : g_instance_zone,
                    editable : true,
                    can_modify : true,
                };
                addNewTableRow(volume);
            }
        });

        $btnSave.click(function(){
            $(this).focus();
            var isValid = $frmEbsVolumes.valid();
            if (isValid) {
                var devices = _.map(g_tableData, 'device');
                if (_.uniq(devices).length !== devices.length) {
                    toastr.error('Cannot use same device name on different volumes.');
                    return;
                }
                $btnSave.is_saving = true;
                toggleReadOnly();

                var table_data = generateTableData();
                table_data['instance_id'] = g_selected_inst;
                var errmsg = '不明なエラーが発生しました。';
                window.util.ajaxPost('/api/ebs.php?action=change', table_data).then(function(response){
                    if (response) {
                        if (!response.err) {
                            errmsg = '';
                        } else if (response.msg) {
                            errmsg = response.msg;
                        }
                    }
                }).always(function(){
                    $btnSave.is_saving = false;
                    if (errmsg) {
                        toastr.error(errmsg);
                    } else {
                        toastr.success('更新に成功しました。');
                        g_is_dirty = false;
                    }
                    toggleReadOnly();
                });
            }
        });
    })
</script>
