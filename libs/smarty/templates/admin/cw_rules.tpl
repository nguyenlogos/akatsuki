{if $msg != ''}
    <div id="message">
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    </div>
{/if}
{if $errmsg != ''}
    <div id="message">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    </div>
{/if}
<div class="row">
    <div class="col-md">
        <p>自動バックアップ機能で利用するバックアップ実行頻度を、エリアごとに最大100件まで定義することができます。</p>
    </div>
</div>
<form method="get">
    <div class="form-row">
        <div class="form-group col-md-6 col-lg-8 my-1">
            <input type="text"
                   class="form-control"
                   name="rule_name"
                   value="{$rulename}"
                   placeholder="ルール名"
                   max-bytes="64">
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1">
            <button type="submit" class="btn btn-orange btn-filter">検索</button>
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1 text-right">
            <a href="./cw_rules_proc.php">
                <button type="button" name="button" class="btn btn-green" value="更新">
                    <i class="fas fa-plus-circle"></i> 新規登録
                </button>
            </a>
        </div>

    </div>
    <input type="hidden" name="sk" value="{$sortIndex}">
</form>

{if ($itemList|@count) == 0}
<div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{else}
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="table-responsive">
    {tablelist headers=$headerList data=$itemList actions=$actionList}
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
{/if}
<script>
    function editRow(target) {
        var $tr = $(target).closest('tr');
        var itemID = +$tr.attr('data-item-id');
        if (itemID > 0) {
            window.location.href = "./cw_rules_proc.php?id=" + itemID;
        }
    }
    function removeRow(target) {
        var $tr = $(target).closest('tr');
        var objId = +$tr.attr('data-item-id');
        if ( objId > 0 ) {
            dialog.confirmDialog(function(){
                var tds  = $tr.find('td');
                var name = tds[0].innerText;
                var cron = tds[1].innerText;
                var zone = tds[2].innerText;

                var content = '';
                content += '<p>バックアップ・ルールを削除します。</p>';
                content += `
                    <form name="frmDel" method="post" action="./cw_rules.php?id=`+objId+`">
                        <div class="form-group">
                            <label>ルール名</label>
                            <p>`+name+`</p>
                        </div>
                        <div class="form-group">
                            <label>スケジュール</label>
                            <p>`+cron+`</p>
                        </div>
                        <div class="form-group">
                            <label>エリア</label>
                            <p>`+zone+`</p>
                        </div>
                    </form>
                `;
                return content;
            }, {
                title : 'バックアップ・ルールの削除'
            }, function(state){
                if (state === 1) {
                    var $frm = $('[name="frmDel"]');
                    $frm.submit();
                }
            })
        }
    }

    $(document).ready(function(){
        function updateCWRuleState(name, region, state) {
            var deferred = $.Deferred();
            $.ajax({
                'url': '/api/cw_rule.php?action=update_state',
                'dataType' : 'json',
                'type' : 'POST',
                'data' : {
                    name : name,
                    region : region,
                    state : +state
                },
                'success': function(data) {
                    deferred.resolve(data);
                },
                'error': function(error) {
                    deferred.reject(error);
                }
            });

            return deferred.promise();
        }

        $('td[data-item-state]').each(function(index, item){
            var $item = $(item);
            var state = +$item.data('itemState');
            var itemID = "item-state-"+index;
            var $input =
                $('<input type="checkbox" name="item-state">')
                    .attr('id', itemID);
            if (state === 1) {
                $input.attr('checked', 'checked');
            } else {
                $input.removeAttr('checked');
            }
            $input.change(function(){
                var $this = $(this);
                var $tr = $this.closest('tr');

                var name = $tr.find('[data-item-name]').data('itemName');
                var region = $tr.find('[data-item-region]').data('itemRegion');
                var isChecked = $this.is(':checked');

                var msg = '', errmsg = '';

                $this.attr('disabled', 'disabled');
                updateCWRuleState(name, region, isChecked).then(function(response){
                    if (response) {
                        if (!response.err) {
                            msg = '更新に成功しました。';
                        }
                        else if (response.msg) {
                            errmsg  = response.msg;
                        }
                        else {
                            errmsg = '不明なエラーが発生しました。';
                        }
                    }
                    else {
                        errmsg = '不明なエラーが発生しました。';
                    }
                }, function(response){
                    errmsg = '不明なエラーが発生しました。';
                }).always(function(){
                    if (errmsg) {
                        toastr.error(errmsg);
                        if (isChecked) {
                            $this.removeProp('checked');
                        } else {
                            $this.prop('checked', 'checked');
                        }
                    }
                    else if (msg) {
                        toastr.success(msg);
                    }
                    $this.removeProp('disabled');
                });
            });
            $item.empty().append(
                $('<div class="toggle">').append(
                    $input,
                    $('<label>').attr('for', itemID)
                )
            )
        });
    });
</script>
