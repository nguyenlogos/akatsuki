<div class="row">
    <div class="col-md">
        <p>利用するインスタンスタイプのチェックをONにして[保存]ボタンを選択してください。<br />
            [最新]をONにすると、常に最新世代のインスタンスタイプのみ利用可能にします。</p>
    </div>
</div>

<div class="row">
    <div class="col-md">
        {if $msg != ""}<p>{$msg}</p>{/if}
    </div>
</div>
<!-- BEGIN REGION SELECT -->
<div>
    <div>
        <form method="get">
            <div class="form-row">
                <div class="form-group col-xs-9 col-md-6 col-lg-6 my-1">
                    {pulldown4azone selected=$selectedAZ}
                </div>
                <div class="form-group col-sm-3 col-md-2 col-lg-1 my-1 text-right">
                    <button type="submit" name="btnFilter" class="btn btn-orange">検索</button>
                </div>
            </div>
        </form>
    </div>
    <div class="row float-right">
        <div class="col-md text-right">
            {if $itList != ""}
            <div class="form-check d-inline-block align-text-bottom">
                <input id="checkLatest" type="checkbox" name="latest" class="form-check-input" {($curgen) ? 'checked' : ''}>
                <label for="checkLatest" class="custom-control-description"><span>最新</span></label>
            </div>
            {btnSave class="btn-save2" inline="1"}
            {/if}
        </div>
    </div>
</div>
<!-- END REGION SELECT -->
<!-- BEGIN TABLE DATA -->
<div class="loading-overlay" style="display: none;"><div class="overlay-content"></div></div>
{if $itList != ""}
    <div class="table-responsive">
        <table class="table table-striped sort-table sortable">
            <colgroup>
                <col width="50">
                <col width="80">
                <col width="">
                <col width="">
                <col width="90">
                <col width="120">
                <col width="140">
                <col width="110">
                <col width="125">
                <col width="135">
            </colgroup>
            <thead>
            <tr>
                <th>利用</th>
                <th class="sort-header">世代</th>
                <th class="sort-header">用途</th>
                <th class="sort-header">名前</th>
                <th class="sort-header">vCPU数</th>
                <th class="sort-header">メモリ(GiB)</th>
                <th class="sort-header">内部ｽﾄﾚｰｼﾞ(GB)</th>
                <th class="sort-header">EBS最適化</th>
                <th class="sort-header">ネットワーク</th>
                <th class="sort-header">1時間の費用($)</th>
            </tr>
            </thead>
            <tbody id="userData">
            {$itList}
            </tbody>
        </table>
    </div>
<!-- END TABLE DATA -->
<div class="col-md text-left">
    {btnSave}
</div>
{else}
    <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{/if}
<script type="text/javascript">
        //AJAX FOR NOTIECE SAVE BUTTON
    $(document).ready(function(){
        $('.btn-save').on('click', function(){
            var $this = $(this);
            var enabled_list = [];
            var disabled_list = [];
            $('input[name="ints"]').each(function(index, input){
                var imageid = $(input).attr('data-name-id');
                if ( $(input).is(':checked') ) {
                    enabled_list.push(imageid);
                }
                else {
                    disabled_list.push(imageid);
                }
            });
            $.ajax({
                'url' : '/admin/insttype.php?ajax=1',
                'type' : 'post',
                'data' : {
                    'enabled_list' : enabled_list,
                    'disabled_list' : disabled_list
                },
                'success' : function(response){
                    var jsonData;
                    try {
                        jsonData = JSON.parse(response);
                    } catch (err) {}
                    finally {
                        if ( jsonData && jsonData.result ) {
                            toastr.success("更新に成功しました。");
                        }
                        else {
                            toastr.error("失敗しました。");
                            $this.prop('checked', !checked);
                        }
                    }
                },
                'error' : function(){
                    toastr.error("失敗しました。");
                    $this.prop('checked', !checked);
                }
            });
        });

        //AJAX FOR SAVE BUTTON
        $('.btn-save2').on('click', function(){
            var $target = $('input[name="latest"]');
            var checked = $target.is(':checked');
            $.ajax({
                'url' : '/admin/insttype.php?target=curgen&ajax=1',
                'type' : 'post',
                'data' : {
                    'curgen' : +checked
                },
                'success' : function(response){
                    var jsonData;
                    try {
                        jsonData = JSON.parse(response);
                    } catch (err) {}
                    finally {
                        if ( jsonData && jsonData.result ) {
                            toastr.success("更新に成功しました。");
                        }
                        else {
                            toastr.error("失敗しました。");
                        }
                    }
                },
                'error' : function(){
                    toastr.error("失敗しました。");
                }
            });
        });
    });
</script>
