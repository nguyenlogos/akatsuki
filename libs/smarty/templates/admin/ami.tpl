{if $msg != ""}
    <div class="row">
        <div class="col-md">
            {$msg}
        </div>
    </div>
{/if}

<div class="block-content mt-5 mb-5">
    <p>利用するAMIのチェックをONにして[保存]ボタンを選択してください。<br />
        なお、データセンター管理でONにしたデータセンターのAMIのみ表示しています。</p>
</div>

<form method="post">
    <div class="form-row">
        <div class="form-group col-xs-9 col-md-6 col-lg-6 my-1">
            {pulldown4azone selected=$selectedAZ}
        </div>
        <div class="form-group col-sm-3 col-md-2 col-lg-1 my-1 text-right">
            <button type="submit" name="btnFilter" class="btn btn-orange">検索</button>
        </div>
    </div>
</form>
<div class="row mt-5">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
{if $amiList != ""}
    <div class="table-responsive">
        <table class="table table-striped sort-table sortable">
            <thead>
            <tr>
                <th>利用可能</th>
                <th class="sort-header">AMI名</th>
                <th class="sort-header">AMI ID</th>
                <th class="sort-header">ステータス</th>
                <th class="sort-header">プラットフォーム</th>
                <th class="sort-header">アーキテクチャ</th>
                <th class="sort-header">地域名</th>
            </tr>
            </thead>
            {$amiList}
        </table>
    </div>
    {btnSave}
{else}
    <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{/if}

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.btn-save').on('click', function(){
            var enabled_list = [];
            var disabled_list = [];
            $('input[name="ami"]').each(function(index, input){
                var imageid = $(input).attr('data-image-id');
                if ( $(input).is(':checked') ) {
                    enabled_list.push(imageid);
                }
                else {
                    disabled_list.push(imageid);
                }
            });
            $.ajax({
                'url' : '/admin/ami.php?ajax=1',
                'type' : 'post',
                'data' : {
                    'enabled_list' : enabled_list,
                    'disabled_list' : disabled_list
                },
                'success' : function(response){
                    var jsonData;
                    try {
                        jsonData = JSON.parse(response);
                    } catch (err) {}
                    finally {
                        if ( jsonData && jsonData.result ) {
                            toastr.success("更新に成功しました。");
                        }
                        else {
                            toastr.error("失敗しました。");
                        }
                    }
                },
                'error' : function(){
                    toastr.error("失敗しました。");
                }
            });
        });
    });
</script>
