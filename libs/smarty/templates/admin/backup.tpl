{if $msg != ''}
    <div id="message">
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    </div>
{/if}
{if $errmsg != ''}
    <div id="message">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    </div>
{/if}
<form method="GET" name="frm-backup">
    <div class="form-row">
        <div class="form-group col-md-6 col-lg-6 my-1" data-dropdown="proj" data-selected="{$frmValues['proj']}">
        </div>
        <div class="form-group col-md-5 col-lg-5 my-1" data-dropdown="inst" data-selected="{$frmValues['inst']}">
        </div>
        <div class="form-group col-md-1 col-lg-1 my-1 text-right">
            <button type="button" class="btn btn-orange btn-filter" value="更新">
                検索
            </button>
        </div>

    </div>
    <input type="hidden" name="sk" value="{$sortIndex}">
</form>
{if $itemList != ""}
    <div class="table-responsive">
        <table class="table table-striped sort-table sortable">
            <thead>
            <tr>
                <th>ID</th>
                <th>名</th>
                <th>デバイス</th>
                <th>エリア</th>
                <th>タイプ</th>
                <th>サイズ （GB）</th>
                <th>ルール</th>
                <th>世代</th>
                <th>アクション</th>
            </tr>
            </thead>
            <tbody>
            {$itemList}
            </tbody>
        </table>
    </div>
{else}
<div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{/if}
<script>
    $(document).ready(function(){
        var $sel_proj = generateProjsTemplate();
        var $sel_inst = generateIntsTemplate();

        var $container_proj = $('[data-dropdown="proj"]');
        var $container_inst = $('[data-dropdown="inst"]');

        var g_selected_proj = $container_proj.data('selected');
        var g_selected_inst = $container_inst.data('selected');

        var g_ec2_region = '{$ec2region}' + '';

        $container_proj.empty().append($sel_proj);
        $container_inst.empty().append($sel_inst);

        function loadProjs(dept) {
            return window.util.ajax('/api/proj.php?all=1', {
                dept : dept
            });
        }

        function loadRules(regions) {
            return window.util.ajax('/api/cw_rule.php', {
                regions : regions
            });
        }

        function loadInsts(proj) {
            return window.util.ajax('/api/inst.php', {
                proj : proj
            });
        }

        function generateProjsTemplate(projs, selected, onchangeCallback) {
            if (!projs || !projs.length) {
                projs = [];
            }
            var $optionDefault =
                $('<option>').attr('value', '').text('--- プロジェクトを選択 ---');
            var $select = $('<select>')
                    .attr('name', 'proj')
                    .addClass('form-control')
                    .append($optionDefault);
                if (typeof onchangeCallback === 'function') {
                    $select.change(function(){
                        onchangeCallback(this.value);
                    })
                }
            var $optionUnmount =
                $('<option>')
                    .attr('value', -1)
                    .text('アンマウント');
            if (selected && selected == -1) {
                $optionUnmount.attr('selected', 'selected');
            }
            $select.append($optionUnmount);
            projs.forEach(function(proj){
                var $option =
                    $('<option>')
                        .attr('value', proj.pcode)
                        .text(proj.pname + ' (' +proj.pcode+ ')');
                if (selected && selected == proj.pcode) {
                    $option.attr('selected', 'selected');
                }
                $select.append($option);
            });
            return $select;
        }

        function generateIntsTemplate(insts, selected, onchangeCallback) {
            if (!insts || !insts.length) {
                insts = [];
            }
            var $optionDefault =
                $('<option>').attr('value', '').text('--- インスタンスを選択 ---');
            var $select = $('<select>')
                    .attr('name', 'inst')
                    .addClass('form-control')
                    .append($optionDefault);
            if (typeof onchangeCallback === 'function') {
                $select.change(function(){
                    onchangeCallback(this.value);
                })
            }
            insts.forEach(function(inst){
                var $option =
                    $('<option>')
                        .attr('value', inst.id)
                        .text(inst.id + ' (' +inst.name+ ')')
                    if (selected && selected == inst.id) {
                        $option.attr('selected', 'selected');
                    }
                $select.append($option);
            });
            return $select;
        }

        function generateRulesTemplate(rules, selected, onchangeCallback) {
            if (!rules || !rules.length) {
                rules = [];
            }
            var $optionDefault =
                $('<option>').attr('value', '').text('--- ルールを選択 ---');
            var $select = $('<select>')
                    .attr('name', 'cw_rule')
                    .addClass('form-control')
                    .append($optionDefault);
            if (typeof onchangeCallback === 'function') {
                $select.change(function(){
                    onchangeCallback(this.value);
                })
            }
            rules.forEach(function(rule){
                var $option =
                    $('<option>')
                        .attr('value', rule.name)
                        .text(rule.name)
                    if (selected && selected == rule.name) {
                        $option.attr('selected', 'selected');
                    }
                $select.append($option);
            });
            return $select;
        }

        loadProjs().then(function(projs){
            var onchangeCallback = function(selected, initial) {
                g_selected_proj = selected;
                if (!selected) {
                    $sel_proj.addClass('error');
                } else {
                    $sel_proj.removeClass('error');
                }
                if (!initial) {
                    $sel_inst = generateIntsTemplate();
                    g_selected_inst = undefined;
                }
                if (selected <= 0) {
                    $sel_inst.attr('disabled', 'disabled').removeClass('error');
                    $container_inst.empty().append($sel_inst);
                } else {
                    loadInsts(selected).then(function(insts){
                        $sel_inst = generateIntsTemplate(insts, g_selected_inst, function(selected){
                            g_selected_inst = selected;
                        });
                        if (g_selected_inst) {
                            var inst = _.find(insts, {
                                "id" : g_selected_inst
                            });
                            if (!inst) {
                                g_selected_inst = undefined;
                            }
                        }
                        $container_inst.empty().append($sel_inst);
                    });
                }
            };
            $sel_proj = generateProjsTemplate(projs, g_selected_proj, onchangeCallback);
            $container_proj.empty().append($sel_proj);
            if (g_selected_proj) {
                onchangeCallback(g_selected_proj, true);
            }
        });

        var regions = [];
        $('tr.volume > td.region_id').each(function(i, el){
            var regionId = $(el).data('regionId');
            regionId = regionId.replace(/(-\d+)\w+$/, '$1');
            if (regions.indexOf(regionId) === -1) {
                regions.push(regionId);
            }
        });

        loadRules(regions).then(function(rules){
            $('[data-dropdown="cw_rules"]').each(function(index, element){
                var $element = $(element);
                var selected = $element.data('selected');
                var $tr = $(this).closest('tr');
                var $inp_gen = $tr.find('input[name="volume_gen"]');
                var $template = generateRulesTemplate(rules, selected, function(selected){
                    if (selected) {
                        $inp_gen.removeAttr('disabled');
                    } else {
                        $inp_gen.attr('disabled', 'disabled');
                    }
                });
                $element.append($template);
                if (selected) {
                    var rule = _.find(rules, {
                        "name" : selected
                    });
                    if (rule) {
                        $inp_gen.removeAttr('disabled');
                    }
                }
            });
        });

        $('.btn-filter').on('click', function(e){
            if (!g_selected_proj) {
                return;
            }
            $(this).attr('disabled', 'disabled');
            $('form[name="frm-backup"]').submit();
        });

        (function(){
            $('input[name="unmount"]').change(function(){
                var isChecked = $(this).is(':checked');
                if (isChecked) {
                    $sel_inst.attr('disabled', 'disabled');
                    $sel_proj.attr('disabled', 'disabled');
                } else {
                    $sel_inst.removeAttr('disabled');
                    $sel_proj.removeAttr('disabled');
                }
            });

            $('.btn-save').each(function(index, btn){
                $(btn).click(function(){
                    var $this =  $(this);
                    var $row = $(this).closest('tr');
                    var $sel_rule = $row.find('.volume_rule > select');
                    var $inp_gen = $row.find('input[name="volume_gen"]');

                    var newRuleValue = $sel_rule.val();
                    var newGenValue = $inp_gen.val();
                    var volumeID = $row.find('.volume_id').data('volumeId');
                    var regionID = $row.find('.region_id').data('regionId');

                    $errmsg = $msg = '';
                    $this.prop('disabled', 'disabled');
                    updateBackupRules(volumeID, regionID, newRuleValue, newGenValue).then(function(response){
                        if (response) {
                            if (!response.err) {
                                $msg = '更新に成功しました。';
                            }
                            else if (response.msg) {
                                $errmsg  = response.msg;
                            }
                            else {
                                $errmsg = '不明なエラーが発生しました。';
                            }
                        }
                        else {
                            $errmsg = '不明なエラーが発生しました。';
                        }
                    }, function(response){
                        $errmsg = '不明なエラーが発生しました。';
                    }).always(function(){
                        if ($errmsg) {
                            toastr.error($errmsg)
                        }
                        else if ($msg) {
                            toastr.success($msg);
                        }
                        $this.removeProp('disabled');
                    });
                });
            });

            function updateBackupRules(volumeID, regionID, rule, gen) {
                var deferred = $.Deferred();
                $.ajax({
                    'url': '/api/ebs.php?action=update_backup_rule',
                    'type' : 'POST',
                    'dataType' : 'json',
                    'data' : {
                        'id' : volumeID,
                        'rule' : rule,
                        'gen' : gen,
                        'region' : regionID
                    },
                    'success': function(data) {
                        deferred.resolve(data);
                    },
                    'error': function(error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise();
            }
        })();
    })
</script>