{if $msg || $errmsg}
    <div class="alert alert-{($msg == '') ? 'danger' : 'normal'}">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {if $msg}
            {$msg}
        {else}
            {$errmsg}
        {/if}
    </div>
{/if}
<form method="post" action="objproc.php?id={$obj['id']}" class="form-box title-form-01 mt-5" id="reg_form">
    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="obj_name">オブジェクト名</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input type="text" class="form-control objname" name="objname" value="{$obj['name']}" placeholder="128文字まで" maxlength="128" required />
        </div>
    </div>

    <div class="form-group row">
        <label for="obj_ip" class="col-md-3 col-form-label">IPアドレスレンジ</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input id="obj_ip" type="text" class="form-control objip" name="objip" value="{$obj['ipaddr']}" placeholder="15文字まで" maxlength="15" required />
        </div>
    </div>

    <div class="form-group row">
        <label for="obj_mask" class="col-md-3 col-form-label">サブネットマスク</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input id="obj_mask" type="number" min="0" max="32" class="form-control objmask" name="objmask" value="{$obj['submask']}" placeholder="" required/>
        </div>
    </div>

    <div class="form-group row">
        <label for="obj_desc" class="col-md-3 col-form-label">説明</label>
        <div class="col-md-1"></div>
        <div class="col-md-7">
            <textarea name="objdesc" id="obj_desc" class="form-control" placeholder="1024文字まで" maxlength="1024">{$obj['description']}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-4"></div>
        <div class="col-md-7">
            <a href="./obj.php" class="btn btn-secondary mr-3" role="button">戻る</a>
            <input type="hidden" name="ipaddr_old" value="{$obj['ipaddr']}">
            <input type="hidden" name="submask_old" value="{$obj['submask']}">
            <button type="submit" name="button" class="btn btn-orange bt-sign" value="">
                保存
            </button>
        </div>
    </div>
</form>
<script>
    $(document).ready(function(){
        var id = '{$obj["id"]}' + '';
        var newParams = util.replaceUrlParam(window.location.search, 'id', id);
        var newUrl = window.location.pathname + newParams;
        window.history.replaceState({}, '', newUrl);
    });
    $('form').validate({
        rules: {
            objname: {
                maxlength: 128
            },
            objip: {
                maxlength: 15
            },
            objdesc: {
                maxlength: 1024
            }
        }
    });
</script>