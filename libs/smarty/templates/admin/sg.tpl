{if $msg != ''}
    <div id="message">
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    </div>
{/if}
{if $errmsg != ''}
    <div id="message">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    </div>
{/if}

<form id="frmReload" method="POST"></form>
<form>
    <div class="form-row">
        <div class="form-group col-md-6 col-lg-8 my-1">
            <input type="text"
                   name="sgname"
                   class="form-control"
                   placeholder="セキュリティグループ名"
                   value="{$frmValues['sgname']}"
                   max-bytes="128">
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1">
            <button type="submit" class="btn btn-orange btn-filter">検索</button>
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1 text-right">
            <button form="frmReload" type="submit" name="op" class="btn btn-blue pull-right" value="1">
                <i class="fas fa-sync-alt"></i> 更新
            </button>
        </div>

    </div>
</form>

{if ($tablelist|@count) == 0}
<div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{else}
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<div class="table-responsive">
    {tablelist headers=$headerList data=$tablelist actions=$actionList}
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="btn-area">
    <button type="button" name="button" class="btn btn-orange btn-save" value="保存" disabled="">
        保存
    </button>
</div>
{/if}

<script>
    $(document).ready(function(){
        var g_changed = false;
        var $btnSave = $('.btn-save');

        $('td[data-item-checked]').each(function(i, el){
            var $td = $(el);
            var sgid = $td.closest('tr').find('td[data-item-sgid]').data('itemSgid');
            var checked = +$td.data('itemChecked');
            var $chkbox = $('<input type="checkbox" name="sg_checked">')
                .attr('data-sgid', sgid)
                .change(function(){
                    if (!g_changed) {
                        g_changed = true;
                        $btnSave.removeProp('disabled');
                    }
                });
            if (checked) {
                $chkbox.prop('checked', 'checked');
            }
            var $label = '<label></label>';
            var $wrapper = $('<div class="form-check checkbox-simple">').append($chkbox, $label);

            $td.empty().append($wrapper);
        });
        $('thead > tr > th:first').addClass('text-center');
        $btnSave.click(function(){
            if ($btnSave.isSaving) {
                return false;
            }
            var checkedList = [];
            var uncheckedList = [];

            $('input[name="sg_checked"]').each(function(i, el){
                if (el.checked) {
                    checkedList.push($(el).data('sgid'));
                } else {
                    uncheckedList.push($(el).data('sgid'));
                }
            });
            var errmsg = '不明なエラーが発生しました。';
            $btnSave.isSaving = true;
            $btnSave.prop('disabled', 'disabled');

            window.util.ajaxPost('/api/sg_enable.php?action=update', {
                'checked' : checkedList,
                'unchecked' : uncheckedList
            }).then(function(response){
                if (response && !response.err) {
                    errmsg = '';
                }
            }).always(function(){
                if (errmsg) {
                    toastr.error(errmsg);
                    $btnSave.removeProp('disabled');
                } else {
                    toastr.success('更新に成功しました。');
                    g_changed = false;
                }
                $btnSave.isSaving = false;
            });
        });
    });
</script>