{if $warning_msg}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$warning_msg}
    </div>
{/if}
{if $msg != ""}
    {if $err == 0}
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {else}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {/if}
{/if}
<div class="block-content mt-5">
    <p>本サービスでは、Amazon社が提供するAPIを使って機能を実現しています。<br />
        そのため、API接続のための認証情報が必要です。</p>

    <p>以下に認証情報を入力して、[保存]ボタンを押してください。</p>
</div>
<form method="POST" action="/admin/key.php" class="form-box title-form-01 mt-5">
    <div class="form-group row">
        <label for="inputAccessKey" class="col-md-3 col-form-label">Access Key Id:</label>
        <div class="col-md-8">
            <input type="text" class="form-control" placeholder="Access Key Id" name="newkey" value="{$newkey|escape}" required />
        </div>
    </div>
    <div class="form-group row">
        <label for="inputSecretAcessKey" class="col-md-3 col-form-label">Secret Access Key:</label>
        <div class="col-md-8">
            <input type="text" class="form-control" placeholder="Secret Access Key" name="newsecret" value="{$newsecret|escape}" required />
        </div>
    </div>
    <div class="form-group row">
        <label for="inputSecretAcessKey" class="col-md-3 col-form-label">&nbsp;</label>
        <div class="col-md-8">
            <button type="button" class="btn btn-info btn-block" data-toggle="collapse" data-target="#detail"><i class="fas fa-info-circle"></i> 現在の認証情報の表示/非表示</button>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-3"></div>
        <div class="col-md-4">
            <button type="submit" name="button" class="btn btn-blue btn-block" value="認証テスト"> 認証テスト</button>
        </div>
        <div class="col-md-4">
            <button type="submit" name="button" class="btn btn-orange btn-block" value="保存">保存</button>
        </div>
    </div>
    <div id="detail" class="collapse">
        <div class="form-group row mt-0">
            <label for="inputAccessKey" class="col-md-3 col-form-label">Access Key Id:</label>
            <div class="col-md-8">
                <input type="text" class="form-control" value="{$key|escape}" readonly />
            </div>
        </div>
        <div class="form-group row">
            <label for="inputSecretAcessKey" class="col-md-3 col-form-label">Secret Access Key:</label>
            <div class="col-md-8">
                <input type="text" class="form-control" value="{$secret|escape}" readonly />
            </div>
        </div>
    </div>
</form>
