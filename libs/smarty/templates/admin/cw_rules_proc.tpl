{if $msg || $errmsg}
    <div class="alert alert-{($msg == '') ? 'danger' : 'normal'}">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {if $msg}
            {$msg}
        {else}
            {$errmsg}
        {/if}
    </div>
{/if}
<form method="post" id="frm_expr" action="./cw_rules_proc.php?id={$rule['id']}" class="form-box title-form-01">
    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="rule_name">ルール名</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input id="rule_name"
                   type="text"
                   class="form-control"
                   name="rule_name"
                   value="{$rule['name']}"
                   placeholder="64文字まで"
                   maxlength="64"
                   required="required" {$inputReadonly} />
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label">エリア</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <div class="rule_region" data-selected="{$rule['region']}" data-readonly="{$inputReadonly}"></div>
        </div>
    </div>

    <div class="form-group row expr_cron_options d-none">
        <label class="col-md-3 col-form-label">スケジュール ({$timezone})</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <div class="expr_options">
                <div class="expr_cron_interval expr_cron d-inline-block">
                    <select class="form-control">
                        <option value="d">毎日</option>
                        <option value="w">毎週</option>
                        <option value="m">毎月</option>
                    </select>
                </div>
                <div class="expr_cron_weekday expr_cron d-none">
                    <select class="form-control">
                        <option value="1">日曜日</option>
                        <option value="2">月曜日</option>
                        <option value="3">火曜日</option>
                        <option value="4">水曜日</option>
                        <option value="5">木曜日</option>
                        <option value="6">金曜日</option>
                        <option value="7">土曜日</option>
                    </select>
                </div>
                <div class="expr_cron_day d-none"></div>
                <div class="expr_cron_hour d-inline-block"></div>
                <div class="expr_cron_minute d-inline-block"></div>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="rule_notes">説明</label>
        <div class="col-md-1"></div>
        <div class="col-md-7">
            <textarea name="rule_notes" id="rule_notes" rows="4" class="form-control" placeholder="512文字まで" maxlength="512">{$rule['notes']}</textarea>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-4"></div>
        <div class="col-md-7">
            <a href="./cw_rules.php" class="btn btn-secondary mr-3" role="button">戻る</a>
            <button type="button" name="button" class="btn btn-orange bt-sign btn-save">保存</button>
        </div>

    </div>
    <div class="form-group">
        <input id="rule_expr"
               type="hidden"
               class="form-control"
               name="rule_expr"
               value="{$rule['expr']}"
               required readonly/>
    </div>
</form>

<script src="/src/assets/js/custom.dtpicker.js"></script>
<script>
    $(document).ready(function(){
        var id = '{$rule["id"]}' + '';
        var newParams = util.replaceUrlParam(window.location.search, 'id', id);
        var newUrl = window.location.pathname + newParams;
        window.history.replaceState({}, '', newUrl);

        var CronGenerator = function(){
            var unitData = {
                minute : [],
                hour   : [],
                day    : [], // day of month
                month  : [],
                weekday: [], // day of week
                year   : []
            };

            function rebuildExpr() {
                var expr = [];
                var data = [];
                if (unitData.day.length === 0 && unitData.weekday.length === 0 || unitData.day.length > 0) {
                    unitData.weekday = ['?'];
                } else {
                    unitData.day = ['?'];
                }
                for (var key in unitData) {
                    var values = unitData[key];
                    if (values.length === 0) {
                        expr.push("*");
                    } else {
                        values = values.sort(function(x, y){
                            return +x > +y ? 1 : -1;
                        });
                        expr.push(values.join(","));
                    }
                }
                return expr.join(" ");
            }

            return {
                set : function(unit, values) {
                    if (!Array.isArray(values)) {
                        values = [values];
                    }
                    values = values.filter(function(value){
                        return value && value >= 0;
                    });
                    if (values.length) {
                        unitData[unit] = _.union(unitData[unit], values);
                        if (unit === 'day') {
                            unitData.weekday = [];
                        } else if (unit === 'weekday') {
                            unitData.day = [];
                        }
                    }
                },
                unset : function(unit, value) {
                    _.pull(unitData[unit], value);
                },
                export : function() {
                    return "cron("+rebuildExpr()+")";
                }
            };
        };

        (function(){
            var $rule_region = $('.rule_region');
            $rule_region.empty().append(generateRegionsTemplate());

            var $expr_cron_weekday = $('.expr_cron_weekday');
            var $expr_cron_day = $('.expr_cron_day');
            var $expr_cron_hour = $('.expr_cron_hour');
            var $expr_cron_minute = $('.expr_cron_minute');

            $expr_cron_minute.append(dtpicker.minute.$options);
            $expr_cron_hour.append(dtpicker.hour.$options);
            $expr_cron_day.append(dtpicker.day.$options);


            var $sel_interval = $('.expr_cron_interval > select');

            var $sel_day = $expr_cron_day.find('select');
            var $sel_weekday = $expr_cron_weekday.find('select');
            var $sel_hour = $expr_cron_hour.find('select');
            var $sel_minute = $expr_cron_minute.find('select');

            var $rule_expr = $('#rule_expr');
            var $frm_expr = $('#frm_expr');

            $sel_interval.change(function(){
                var interval = $(this).val();
                if (interval === 'w') {
                    $expr_cron_weekday.removeClass('d-none').addClass('d-inline-block');
                }
                else {
                    $expr_cron_weekday.addClass('d-none').removeClass('d-inline-block');
                }
                if (interval === 'm') {
                    $expr_cron_day.removeClass('d-none').addClass('d-inline-block');
                }
                else {
                    $expr_cron_day.addClass('d-none').removeClass('d-inline-block');
                }
            });

            var expr = '{$rule["expr"]}' + '';
            if (expr) {
                expr = expr.replace(/cron\(([^)]+)\)/, '$1');
                expr = expr.split(' ');
                if (expr.length === 6) {
                    expr = _.zipObject(['minute', 'hour', 'day', 'month', 'weekday', 'year'], expr);
                    expr = _.pickBy(expr, function(value){
                        return +value > 0;
                    });
                } else {
                    expr = '';
                }
            }

            if (expr) {
                if (expr.day) {
                    $sel_day.val(expr.day);
                    $sel_interval.val('m');
                    $expr_cron_day.removeClass('d-none').addClass('d-inline-block');
                } else if (expr.weekday) {
                    $sel_weekday.val(expr.weekday);
                    $sel_interval.val('w');
                    $expr_cron_weekday.removeClass('d-none').addClass('d-inline-block');
                }
                if (expr.hour) {
                    $sel_hour.val(expr.hour);
                }
                if (expr.minute) {
                    $sel_minute.val(expr.minute);
                }
            }

            $('.btn-save').on('click', function(){
                var cron = new CronGenerator();
                var interval = $sel_interval.val();
                var requiredFields = ['hour', 'minute'];
                if (interval === 'w') {
                    requiredFields.push('weekday');
                } else if(interval === 'm') {
                    requiredFields.push('day');
                }
                requiredFields.forEach(function(field){
                    var $select = $('.expr_cron_'+field);
                    $select = $select.find('select');

                    cron.set(field, $select.val());
                });
                $rule_expr.val(cron.export());
                if ($frm_expr.valid()) {
                    $(this).attr('disabled', 'disabled');
                    $frm_expr.submit();
                }
            });

            function loadRegions() {
                var deferred = $.Deferred();
                $.ajax({
                    'url': '/api/region.php',
                    'dataType' : 'json',
                    'success': function(data) {
                        deferred.resolve(data);
                    },
                    'error': function(error) {
                        deferred.reject(error);
                    }
                });

                return deferred.promise();
            }

            function generateRegionsTemplate(regions, selected, onchangeCallback) {
                if (!regions || !regions.length) {
                    regions = [];
                }
                var $optionDefault =
                    $('<option>').attr('value', '').text('--- エリアを選択 ---');
                var $select = $('<select>')
                        .attr('name', 'rule_region')
                        .attr('required', 'required')
                        .addClass('form-control')
                        .append($optionDefault);
                    if (typeof onchangeCallback === 'function') {
                        $select.change(function(){
                            onchangeCallback(this.value);
                        })
                    };
                regions.forEach(function(region){
                    var $option =
                        $('<option>')
                            .attr('value', region.name)
                            .text(region.name_jp + ' (' +region.name+ ')');
                    if (selected && selected == region.name) {
                        $option.attr('selected', 'selected');
                    }
                    $select.append($option);
                });
                return $select;
            }

            loadRegions().then(function(regions){
                var selected_region = $rule_region.data('selected');
                var $template = generateRegionsTemplate(regions, selected_region);
                $rule_region.empty().append($template);
            }).always(function(){
                var readonly = $rule_region.data('readonly');
                if (readonly) {
                    $rule_region.find('select').attr('disabled', 'disabled');
                }
            });
        })();

        $('.expr_cron_options').removeClass('d-none');
    });
    $('form').validate({
        rules: {
            rule_name: {
                maxlength: 64
            },
            rule_notes: {
                maxlength: 512
            }
        }
    });
</script>
