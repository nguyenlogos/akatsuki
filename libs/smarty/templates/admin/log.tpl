<div id="message">
    {if $msg != ''}
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    {/if}
    {if $errmsg != ''}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    {/if}
</div>
<div class="block-content">
    <p>すべての操作・変更に関するログが確認できます。</p>
</div>
<form method="get">
    <div class="form-row">
        <div class="form-group col-md-2 col-lg-2 my-1">
            {pulldown4logtype name="l_category" selected=$frmValues['l_category']}
        </div>
        <div class="form-group col-md-2 col-lg-2 my-1 {$hideDept}">
            <div data-dropdown-source="dept"
                 data-dropdown-target="#proj"
                 data-selected="{$frmValues['l_dept']}"></div>
        </div>
        <div class="form-group input-group col-md-3 col-lg-3 my-1 {$hideDept}">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
            </div>
            <input
                    id="l_from_date"
                    name="l_from_date"
                    class="form-control date-input"
                    placeholder="開始年月日"
                    value="{$frmValues['l_from_date']}"
                    autocomplete="off"
            />
        </div>
        <div class="form-group input-group col-md-3 col-lg-3 my-1 {$hideDept}">
            <div class="input-group-prepend">
                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
            </div>
            <input
                    id="l_till_date"
                    name="l_till_date"
                    class="form-control date-input"
                    placeholder="終了年月日"
                    value="{$frmValues['l_till_date']}"
                    autocomplete="off"
            />
        </div>

        <div class="form-group col-md-1 col-lg-1 my-1 text-right">
            <button type="submit" class="btn btn-orange" value="検索">
                検索
            </button>
        </div>

    </div>

    <input type="hidden" name="sk" value="{$sortIndex}">
</form>

{if ($logList|@count) == 0}
    <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{else}
    <div class="row">
        <div class="col-md">
            {$paginationStr}
        </div>
    </div>
    <div class="table-responsive">
        {tablelist headers=$headerList data=$logList}
    </div>
    <div class="row">
        <div class="col-md">
            {$paginationStr}
        </div>
    </div>
{/if}

<script type="text/javascript">
    $(document).ready(function(){
        $('input#l_from_date, input#l_till_date').datepicker({
            'language' : 'ja',
            'endDate' : 'now',
            'format' : 'yyyy/mm/dd',
            'autoclose' : true,
            'todayHighlight' : true,
        });
        $('[data-toggle="collapse"]').click(function() {
            $('.collapse.in').collapse('hide')
        });
    });
</script>
