{if $msg != ""}
<div class="row mt-5">
    <div class="col-md">
        {$msg}
    </div>
</div>
{/if}

<div class="row mt-5">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
{if $azList != ""}
    <div class="row">
        <div class="col-md">
            <div class="table-responsive">
                <table  class="table table-striped sort-table sortable">
                    <colgroup>
                        <col width="80">
                        <col width="">
                        <col width="">
                        <col width="150">
                        <col width="200">
                    </colgroup>
                    <thead>
                    <tr class="table-header">
                        <th>利用可能</th>
                        <th class="sort-header">地域名</th>
                        <th class="sort-header">データセンター名</th>
                        <th class="sort-header">ステータス</th>
                        <th class="sort-header">現在のインスタンス数</th>
                    </tr>
                    </thead>
                    {$azList}
                </table>
            </div>
            {btnSave}
        </div>
    </div>

{else}
    <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{/if}
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

{if $delazList !== ""}
    <div class="alert alert-danger">
        警告:データセンター{$delazList}は利用可能に設定していましたが、最新のデータセンターのリストに存在しません。<br />
        設定を解除しました。
    </div>
{/if}
<script type="text/javascript">
    $(document).ready(function(){
        $('.btn-save').on('click', function(){
            var enabled_list = [];
            var disabled_list = [];
            $('input[name="azone"]').each(function(index, input){
                var zone_name = $(input).attr('data-zone-name');
                if ( $(input).is(':checked') ) {
                    enabled_list.push(zone_name);
                }
                else {
                    disabled_list.push(zone_name);
                }
            });
            $.ajax({
                'url' : '/admin/az.php?ajax=1',
                'type' : 'post',
                'data' : {
                    'enabled_list' : enabled_list,
                    'disabled_list' : disabled_list
                },
                'success' : function(response){
                    var jsonData;
                    try {
                        jsonData = JSON.parse(response);
                    } catch (err) {}
                    finally {
                        if ( jsonData && jsonData.result ) {
                            toastr.success("更新に成功しました。");
                        }
                        else {
                            toastr.error("失敗しました。");
                        }
                    }
                },
                'error' : function(){
                    toastr.error("失敗しました。");
                }
            })
        });
    });
</script>
