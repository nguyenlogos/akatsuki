<style type="text/css">
    @media only screen and (min-width: 1235px) {
        .header {
            white-space: nowrap;
        }
    }
    .table th:nth-last-child(2) {
        width: 260px;
    }
</style>
{if $msg != ''}
    <div id="message">
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    </div>
{/if}
{if $errmsg != ''}
    <div id="message">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    </div>
{/if}

<div class="block-content mt-5">
    <p>オブジェクト名を登録して、IPアドレス(レンジ)を抽象化することができます。<br />変更を行った場合は、すべてのセキュリティグループの通信設定に即座に反映されます。</p>
</div>

<form method="get">
    <div class="form-row">
        <div class="form-group col-md-6 col-lg-8 my-1">
            <input type="text" name="objname" class="form-control" placeholder="オブジェクト名" value="{$qObjName}">
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1 text-left">
            <button type="submit" class="btn btn-orange mr-3">検索</button>
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1 text-right">
            <a href="./objproc.php" class="btn btn-green pull-right" role="button">
                <i class="fas fa-plus-circle"></i>
                新規登録
            </a>
        </div>
    </div>
    <input type="hidden" name="sk" value="{$sortIndex}">
</form>

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="table-responsive">
    {tablelist headers=$headerList data=$objList actions=$actionList}
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

{if count($objList) == 0}
    <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{/if}

<script>
    function editRow(target) {
        var $tr = $(target).closest('tr');
        var objId = +$tr.attr('data-item-id');
        if ( objId > 0 ) {
            newUrlParams = window.util.replaceUrlParam(window.location.search, 'id', objId);
            window.location.href = "./objproc.php" + newUrlParams;
        }
        else {
            //
        }
    }
    function removeRow(target) {
        var $tr = $(target).closest('tr');
        var objId = +$tr.attr('data-item-id');
        if ( objId > 0 ) {
            dialog.confirmDialog(function(){
                var tds  = $tr.find('td');
                var name = tds[0].innerText;
                var ip   = tds[1].innerText;
                var mask = tds[2].innerText;

                var content = '';
                content += '<p>オブジェクトを削除します。</p>';
                content += `
                    <form name="frmDel" method="post" action="./obj.php?id=`+objId+`">
                        <div class="form-group">
                            <label>オブジェクト名</label>
                            <p>`+name+`</p>
                        </div>
                        <div class="form-group">
                            <label>IPアドレスレンジ</label>
                            <p>`+ip+`/`+mask+`</p>
                        </div>
                    </form>
                `;
                return content;
            }, {
                title : 'プオブジェクトの削除'
            }, function(state){
                if ( state === 1 ) {
                    var $frm = $('[name="frmDel"]');
                    $frm.submit();
                }
            })
        }
    }
</script>
