{if $msg != "" && $err == 0}
<div class="alert alert-dismissible alert-info">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
</div>
{else}
合算請求の設定を行います。<br />
合算先のAWS ID(数字12桁)を入力してください。<br />
合算設定を外す場合は、空白にして[変更]ボタンを押してください。<br />
<br />
{if $msg != "" && $err == 1}
<div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
</div>
{/if}
<form METHOD="POST" ACTION="{$curURI}" class="form-horizontal col-sm-8" id="reg_form_2">
    <input type="hidden" name="acid" value="{$acid}" />
    <div class="form-group">
        <label>AWS ID</label>
        <input type="text" class="form-control {if $acid_err}error{/if}" id="linkto" name="linkto" value="{$linkto}" maxlength="12" placeholder="数字12桁" />
    </div>

    <div class="form-group">
        <a href="./invoice.php" class="btn btn-default" role="button">キャンセル</a>
        <input type="submit" class="btn btn-primary bt-sign" name="btn" value="保存" />
    </div>
</form>
{/if}
