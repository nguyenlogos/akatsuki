{if $msg != "" && $err == 0}
<div class="alert alert-dismissible alert-info">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
</div>
{else}
下記フォームに必要事項を入力して、[{$submitButton}]ボタンを押してください。<br />
<br />
{if $msg != "" && $err == 1}
<div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
</div>
{/if}
<form METHOD="POST" ACTION="{$curURI}" class="form-horizontal col-sm-8" id="reg_form_2">
    <div class="form-group">
        <label>AWS ID</label>
        <input type="text" class="form-control {if $acid_err}error{/if}" id="acid" name="acid" value="{$acid}" maxlength="12" placeholder="数字12桁" required/>
        <input type="hidden" name="old_acid" value="{$acid}" />
    </div>
    <div class="form-group">
        <label>氏名</label>
        <input type="text" class="form-control {if $name_err}error{/if}" id="name" name="name" value="{$name}" maxlength="64" placeholder="64文字まで" />
    </div>
    <div class="form-group">
        <label>会社名 <font color="red">*</font></label>
        <input type="text" class="form-control {if $company_err}error{/if}" id="company" name="company" value="{$company}" maxlength="128" placeholder="128文字まで" required/>
    </div>
    <div class="form-group">
        <label>部署名(任意)</label>
        <input type="text" name="dept" class="form-control {if $dept_err}error{/if}" maxlength="64" placeholder="64文字まで" value="{$dept}" />
    </div>
    <div class="form-group">
        <label>役職(任意)</label>
        <input type="text" name="position" class="form-control {if $position_err}error{/if}" maxlength="64" placeholder="64文字まで" value="{$position}" />
    </div>
    <div class="form-group">
        <label>電話番号(任意)</label>
        <input type="text" name="tel" class="form-control {if $position_err}error{/if}" maxlength="15" placeholder="-(ハイフン)で区切ってください。15文字まで" value="{$tel}" />
    </div>
    <div class="form-group">
        <label>メールアドレス</label>
        <input type="email" name="email" id="email" maxlength="64" placeholder="64文字まで" class="form-control {if $email_err}error{/if}" value="{$email}" />
    </div>
    <div class="form-group">
        <label>メールアドレス(再入力)</label>
        <input type="email" name="email2" id="confirm_email" maxlength="64" placeholder="64文字まで" class="form-control {if $email_config_err}error{/if}" value="{$email2}"/>
    </div>
    <div class="form-group">
        <label>支払い条件 <font color="red">*</font></label>
        <select name="payday" class="form-control" required>
            <option value="">-----支払い条件-----</option>
            {html_options  options=$paydayOptions selected=$payday}
        </select>
    </div>
    <div class="form-group">
        <label>発送方法 <font color="red">*</font></label>
        <select name="howtosend" class="form-control" required>
            <option value="">-----発送方法-----</option>
            {html_options  options=$howtosendOptions selected=$howtosend}
        </select>
    </div>
    <div class="form-group">
        <label>社内/社外利用 <font color="red">*</font></label>
        <select name="internaluse" class="form-control" required>
            <option value="">-----社内/社外利用-----</option>
            {html_options  options=$internaluseOptions selected=$internaluse}
        </select>
        <p>AWS利用料の請求書を発行しない場合は「社内」を選択してください。</p>
    </div>
    <div class="form-group">
        <a href="./invoice.php" class="btn btn-default" role="button">キャンセル</a>
        <input type="submit" class="btn btn-primary bt-sign" name="btn" value="{$submitButton}" />
    </div>
</form>
{/if}
