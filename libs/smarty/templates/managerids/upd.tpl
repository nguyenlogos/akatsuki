{if $msg != "" && $err == 0}
<div class="alert alert-dismissible alert-info">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
</div>
{else}
下記フォームに必要事項を入力して、[{$submitButton}]ボタンを押してください。<br />
<br />
{if $msg != "" && $err == 1}
<div class="alert alert-dismissible alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
</div>
{/if}
<form METHOD="POST" ACTION="{$curURI}" class="form-horizontal col-sm-8" id="reg_form">
    <div class="form-group">
        <label>ID</label>
        <p>{$cid}</p>
        <input type="hidden" name="cid" VALUE="{$cid}" />
    </div>
    <div class="form-group">
        <label>氏名 <font color="red">*</font></label>
        <input type="text" class="form-control {if $name_err}error{/if}" id="name" name="name" value="{$name}" maxlength="64" placeholder="64文字まで"  required/>
    </div>
    <div class="form-group">
        <label>会社名 <font color="red">*</font></label>
        <input type="text" class="form-control {if $company_err}error{/if}" id="company" name="company" value="{$company}" maxlength="128" placeholder="128文字まで" required/>
    </div>
    <div class="form-group">
        <label>部署名(任意)</label>
        <input type="text" name="dept" class="form-control {if $dept_err}error{/if}" maxlength="64" placeholder="64文字まで" value="{$dept}" />
    </div>
    <div class="form-group">
        <label>役職(任意)</label>
        <input type="text" name="position" class="form-control {if $position_err}error{/if}" maxlength="64" placeholder="64文字まで" value="{$position}" />
    </div>
    <div class="form-group">
        <label>電話番号 <font color="red">*</font></label>
        <input type="text" name="tel" id='phone' maxlength="15" class="form-control textbox {if $tel_err}error{/if}" value="{$tel}"/>
    </div>
    <div class="form-group">
        <label>メールアドレス <font color="red">*</font></label>
        <input type="email" name="email" id="email" maxlength="64" placeholder="64文字まで" class="form-control {if $email_err}error{/if}" value="{$email}" required/>
    </div>
    <div class="form-group">
        <label>メールアドレス(再入力) <font color="red">*</font></label>
        <input type="email" name="email2" id="confirm_email" maxlength="64" placeholder="64文字まで" class="form-control {if $email_config_err}error{/if}" value="{$email2}"/>
    </div>
    <div class="form-group">
        <label>支払い条件 <font color="red">*</font></label>
        <select name="payday" class="form-control">
            <option>-----支払い条件-----</option>
            {html_options  options=$paydayOptions selected=$payday}
        </select>

    </div>
    <div class="form-group">
        <a href="./etp.php" class="btn btn-default" role="button">キャンセル</a>
        <input type="submit" class="btn btn-primary bt-sign" name="btn" value="{$submitButton}" />
    </div>
</form>
{/if}
