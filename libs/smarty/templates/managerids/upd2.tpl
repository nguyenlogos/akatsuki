{if $msg != "" && $err == 0}
<div class="alert alert-dismissible alert-info">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
    <br />
    <br />
    <a href="/managerids/etp.php">戻る</a>
</div>
{else}
<div class="col-xs-10 col-lg-8">
<form method="POST" action="/managerids/upd2.php" class="form-horizontal">
    {if $msg != "" && $err == 1}
    <div class="alert alert-dismissible alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$msg}
    </div>
    {/if}
    <div class="row">
        ID:{$cid} 会社名：{$company} の認証情報を編集します。<br />
        <br />
        以下に認証情報を入力して、[保存]ボタンを押してください。<br /><br />
    </div>
    <div class="form-group">
        <div class="row">
            <label for="inputAccessKey" class="col-xs-3 control-label">Access Key Id:</label>
            <div class="col-xs-6 no-padding-right">
                <input type="text" class="form-control" placeholder="Access Key Id" name="newkey" value="{$newkey|escape}" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <label for="inputSecretAcessKey" class="col-xs-3 control-label">Secret Access Key:</label>
            <div class="col-xs-6 no-padding-right">
                <input type="text" class="form-control" placeholder="Secret Access Key" name="newsecret" value="{$newsecret|escape}" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <label for="inputSecretAcessKey" class="col-xs-3 control-label">&nbsp;</label>
            <div class="col-xs-6 no-padding-right">
                <button type="button" class="btn btn-info btn-block" data-toggle="collapse" data-target="#detail"><i class="glyphicon glyphicon-exclamation-sign"></i> 現在の認証情報の表示/非表示</button>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <label class="col-xs-3 control-label">&nbsp;</label>
            <div class="col-xs-2 no-padding-right">
                <button type="submit" name="button" class="btn btn-primary btn-block" value="認証テスト" /> 認証テスト</button>
            </div>
            <div class="col-xs-2 no-padding-right">
                <button type="submit" name="button" class="btn btn-primary btn-block" value="保存" />
                    <i class="glyphicon glyphicon-ok"></i> 保存
                </button>
            </div>
            <div class="col-xs-2 no-padding-right">
                <a class='btn btn-default btn-block' href='/managerids/etp.php' role='button'>戻る</a>
            </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="id" value="{$cid}" />
        <input type="hidden" name="company" value="{$company}" />
        <input type="hidden" name="key" value="{$key}" />
        <input type="hidden" name="secret" value="{$secret}" />
    </div>
    <div id="detail" class="collapse">
        <div class="form-group">
            <div class="row">
                <label for="inputAccessKey" class="col-xs-3 control-label">Access Key Id:</label>
                <div class="col-xs-6 no-padding-right">
                    <input type="text" class="form-control" value="{$key|escape}" readonly />
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <label for="inputSecretAcessKey" class="col-xs-3 control-label">Secret Access Key:</label>
                <div class="col-xs-6 no-padding-right">
                    <input type="text" class="form-control" value="{$secret|escape}" readonly />
                </div>
            </div>
        </div>
    </div>
</form>
</div>
{/if}

