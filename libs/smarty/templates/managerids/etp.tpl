<form method="POST" action="{$curURI}">
    {if $msg != "" && $err == 1}
        <div class="alert alert-dismissible alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {elseif $msg != "" && $err == 0}
        <div class="alert alert-dismissible alert-info">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {/if}
</form>


<div class="col-xs-12">
    <div class="col-xs-12 panel-group">
        <div class="row">
            <p>{$dataCount}件、登録されています。</p>
            <p><a href="./upd.php?cmd=new" class="btn btn-success" role="button"> <i class="glyphicon glyphicon-plus-sign"></i> 新規登録</a></p>
        </div>
    </div>

    <form METHOD="GET" ACTION="{$curURI}" class="panel-group">
        <div class="row">
            <div class="col-xs-3">
                <div class="form-group">
                    {$YMlist}
                </div>
            </div>
            <div class="col-xs-9">
                <div class="form-group">
                    {pulldown4cid name=cid selected=$TARGET_CID all="true"}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3">
                <div class="form-group">
                    <input
                            type="number"
                            name="price_from"
                            class="form-control"
                            placeholder="利用料(FROM)"
                            value="{$TARGET_PFROM}"
                            min=0
                            max=999999999999>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="form-group">
                    <input
                            type="number"
                            name="price_to"
                            class="form-control"
                            placeholder="利用料(TO)"
                            value="{$TARGET_PTO}"
                            min=0
                            max=999999999999>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">検索</button>
                </div>
            </div>
        </div>
    </form>

    <div class="col-xs-12 col-padding-left">
        <div class="row">
            <div class="col-xs-6 well">
                <label>合計</label>
                <p><label>利用料 :</label> {$usageusd} USD / {$usagejpy} JPY</p>
                <p><label>消費税 :</label> {$taxusd} USD / {$taxjpy} JPY</p>
                <p><label>合計 :</label> {$totalusd} USD / {$totaljpy} JPY</p>
            </div>
        </div>
    </div>

    <div class="col-xs-10">
        <div class="row">
            {$pagenationStr}
        </div>
    </div>
    <div class="col-xs-2">
        <div class="row">
            <a href="./pdfadmin.php?slug={$curYM}" class="btn btn-info">全てダウンロード</a>
        </div>
    </div>

    <table class="table table-hover sortable">
        <tr>
            <th class="sort-header">ID</th>
            <th class="sort-header">会社名
                <br />部署名、氏名、メールアドレス 他
            </th>
            <th class="sort-header">プラン</th>
            <th class="sort-header">利用料({$ymStr}分)
                <br />
                レート:{$rate}JPY/USD
            </th>
            <th>操作</th>
        </tr>
        {$userList}
    </table>

    <div class="col-xs-12">
        <div class="row">
            {$pagenationStr}
        </div>
    </div>

</div>

<script>
    $(document).ready(function() {
        $('a[data-sort-value]').on('click', function(){
            var sortValue = +$(this).attr('data-sort-value');
            var newUrlParams = util.replaceUrlParam(window.location.search, 'sk', sortValue);
            window.location.href = window.location.pathname + newUrlParams;
        });
    })
    function confDelete() {
        return window.confirm('削除します。よろしいですか？');
    }
</script>
