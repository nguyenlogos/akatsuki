{if $error != ""}
    <div class="col-xs-6 col-xs-offset-1">
        <div class="row">
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {$error}
            </div>
        </div>
    </div>
{/if}
<div class="col-xs-offset-2">
    <div class="modal-login">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">管理者ログイン</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="メールアドレス" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="パスワード" required>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="bt_login" class="btn btn-success btn-block login-btn">ログイン</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
