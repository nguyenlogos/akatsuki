<div class="col-xs-offset-2">
    <div class="col-sm-9">
		{if $msg != "" && $err == 0}
            <div class="alert alert-dismissible alert-info">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <p>{$msg}</p>
                <p><a href="./profile.php">キャンセル</a></p>
            </div>
		{else}
			{if $msg != "" && $err == 1}
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
					{$msg}
                </div>
			{/if}
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>ご契約窓口のプロファイル</h3>
                        <p>ご契約窓口のご担当者様のプロファイルを編集して、[変更]ボタンを押してください。</p>
                        <p>ご契約に関して等、弊社よりご連絡・お問い合わせをさせていただく場合がございます。 </p>
                    </div>
                    <div class="form-top-right">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="" method="post" class="registration-form" id="reg_form">
                        <div class="form-group">
                            <label>お名前</label>
                            <input type="text" class="form-control" name="name" value="{$name|escape}" required/>
                        </div>

                        <div class="form-group">
                            <label>会社名</label>
                            <input type="text" name="company" class="form-control" value="{$company|escape}" required/>
                        </div>

                        <div class="form-group">
                            <label>部署名(任意)</label>
                            <input type="text" name="dept" class="form-control" value="{$dept|escape}" />
                        </div>

                        <div class="form-group">
                            <label>役職(任意)</label>
                            <input type="text" name="position" class="form-control" value="{$position|escape}" />
                        </div>

                        <div class="form-group">
                            <label>電話番号</label>
                            <input type="text" name="tel" id='textbox' maxlength="15" class="form-control" value="{$tel|escape}" required />
                        </div>

                        <div class="form-group">
                            <label>メールアドレス</label>
                            <input type="email" name="uid" class="form-control" id="email" value="{$uid|escape}" required />
                        </div>

                        <a href="/managerids/etp.php" class="btn btn-default" role="button">キャンセル</a>
                        <input type="submit" name="button" class="btn btn-primary bt-sign" value="変更" />
                    </form>
                </div>
            </div>
		{/if}
    </div>
</div>
