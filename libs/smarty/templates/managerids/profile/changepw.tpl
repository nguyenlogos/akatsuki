<div class="col-xs-offset-3">
    <div class="col-sm-6">
        {if $msg != "" && $err == 0}
            <div class="alert alert-dismissible alert-info">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <p>{$msg}</p>
                <p><a href="/managerids/">キャンセル</a></p>
            </div>
        {else}
            {if $msg != "" && $err == 1}
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {$msg}
                </div>
            {/if}
            <div class="form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>パスワードの変更</h3>
                        <p>ログイン用のパスワードを変更します。</p>
                        <p>以下の情報を入力して、[変更]ボタンを押してください。</p>
                    </div>
                    <div class="form-top-right">
                        <i class="glyphicon glyphicon-lock"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="" method="post" class="registration-form" id="reg_form">
                        <div class="form-group">
                            <label>現在のパスワード <font color="red">*</font></label>
                            <input type="password" maxlength="64" name="p1" value="{$p1|escape}" placeholder="現在のパスワードを入力してください" class="form-first-name form-control" required>
                        </div>
                        <div class="form-group">
                            <label>新しいパスワード <font color="red">*</font></label>
                            <input type="password" maxlength="64" name="p2" value="{$p2|escape}"  placeholder="新しいパスワードを入力してください" class="form-last-name form-control" required>
                        </div>
                        <div class="form-group">
                            <label>新しいパスワード(再度) <font color="red">*</font></label>
                            <input type="password" maxlength="64" name="p3" value="{$p3|escape}" placeholder="新しいパスワード(再度)を入力してください" class="form-email form-control" required>
                        </div>
                        <button type="submit" name="button" class="btn btn-primary" value="変更"><i class="glyphicon glyphicon-ok"></i> 変更</button>
                    </form>
                </div>
            </div>
        {/if}

    </div>
</div>
