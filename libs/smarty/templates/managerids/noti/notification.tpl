<script type="text/javascript">
    $(function() {
        $('#deleteData').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var recipient1 = button.data('dataid');
            var recipient2 = button.data('name');
            var recipient3 = button.data('datefrom');
            var recipient4 = button.data('dateto');
            var modal = $(this);

            modal.find('.uid').text(recipient1);
            modal.find('.title').val(recipient1);
            modal.find('.content').text(recipient2);
            modal.find('.datefrom').text(recipient3);
            modal.find('.dateto').text(recipient4);
        });
    });

</script>

<div  class="col-xs-12">
    <div class="col-xs-12">
        <div class="row">
            <p class="pull-left"><a href="./addnotification.php?cmd=new" class="btn btn-success" role="button"> <i class="glyphicon glyphicon-plus-sign"></i> 新規登録</a></p>
        </div>
        <div class="row">
			{if $msg != "" && $err == 1}
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p>{$msg}</p>

                </div>
			{elseif $msg != "" && $err == 0}
                <div class="alert alert-dismissible alert-info">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p>{$msg}</p><br/>
                    <p><a href="./notification.php">戻る</a></p>
                </div>
			{/if}
        </div>
    </div>

	{if $msg == "" && $err == 0}
        {if $data}
            <table class="table table-hover mgT">
                <thead>
                <tr>
                    <th>タイトル</th>
                    <th>コンテンツ</th>
                    <th>アイコン</th>
                    <th>利用開始日</th>
                    <th>利用終了日</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {foreach from=$data item=value}
                    <tr>
                        <td>{$value['notif_title']}</td>
                        <td>{$value['notif_msg']}</td>
                        <td>
                          {if $value['notif_status'] eq '1'}
                            <i class="glyphicon glyphicon-ok-sign text-info"></i> 情報
                          {elseif $value['notif_status'] eq '2'}
                            <i class="glyphicon glyphicon-warning-sign text-warning"></i> 警告
                          {else}
                            <i class="glyphicon glyphicon-bell text-danger"></i> 危険
                          {/if}
                        </td>
                        <td>{date("Y/m/d",strtotime($value['notif_time']))}</td>
                        <td>{date("Y/m/d",strtotime($value['publish_date']))}</td>
                        <td>
                            <a class='btn btn-primary' href='./addnotification.php?cmd=update&id={$value['id']}' role='button'>変更</a>
                            <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#deleteData' data-dataid=' {$value['id']}' data-name='{$value['notif_title']}' data-datefrom='{$value['notif_time']}' data-dateto='{$value['publish_date']}'>削除</button>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {else}
            <div class="row">
                <div class='col-xs-12 text-info-nodata'>データがありません。</div>
            </div>
        {/if}
	{/if}

    <div class="modal" tabindex="-1" role="dialog" id="deleteData">
        <div class="modal-dialog">
            <div class="modal-content" style="padding:10px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"> お知らせ削除</h4>
                </div>
                <form action="{$curURI}" method="POST">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>タイトル</label><p class="uid"></p>
                            <input type="hidden" class="form-control title" name="title" />
                        </div>
                        <div class="form-group">
                            <label>コンテンツ</label><p class="content"></p>
                        </div>
                        <div class="form-group">
                            <label>利用開始日</label><p class="datefrom"></p>
                        </div>
                        <div class="form-group">
                            <label>利用終了日</label><p class="dateto"></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                        <button type="submit" name="button" class="btn btn-primary" value="削除"><i class="glyphicon glyphicon-remove"></i> 削除</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>