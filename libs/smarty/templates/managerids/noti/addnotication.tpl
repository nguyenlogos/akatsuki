<div class="col-xs-12">
	{if $msg != "" && $err == 1}
	<div class="alert alert-dismissible alert-info">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<p>{$msg}</p><br/>
		<p><a href="./notification.php">戻る</a></p>
	</div>
	{else}
	{if $msg != "" && $err == 0}
		<div class="alert alert-dismissible alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<p>{$msg}</p>
		</div>
	{/if}
		<div class="row">
			<form method="POST" action="{$curURI}" class="form-horizontal col-xs-12">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<div class="form-group {if $title_err}error_sub{/if}">
								<label>タイトル <font color="red">*</font></label>
								<input type="text" class="form-control" name="notif_title" maxlength="128" placeholder="128文字まで"  value="{$notif_title}"/>
                {if $title_err}<font color="red"> {$title_err} が入力されていません。</font>{/if}
							</div>
						</div>
						<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label>コンテンツ</label>
								<textarea type="text" class="form-control" maxlength="225" placeholder="225文字まで" name="notif_name">{$notif_name}</textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>利用開始日 <font color="red">*</font></label>
								<div class="input-group date form_datetime">
									<input type="text" name="date_rate_from" class="form-control date-input" placeholder="YYYY/MM/DD" autocomplete="off" value="{$notif_date_from}">
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
								{if $errDateFrom}<font color="red">{$errDateFrom}</font>{/if}
							</div>
						</div>
						<div class="col-xs-1"></div>
						<div class="col-xs-12 col-sm-3 col-md-3">
							<div class="form-group">
								<label>利用終了日 <font color="red">*</font></label>
								<div class="input-group date form_datetime">
									<input type="text" name="date_rate_to" class="form-control date-input" placeholder="YYYY/MM/DD" autocomplete="off" value="{$notif_date_to}">
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
								{if $errDateTo}<font color="red">{$errDateTo}</font>{/if}
							</div>
						</div>
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">

                <div class="form-check">
                  <input type="radio" class="form-check-input" name="notif_icon" value="1" checked {if $notif_icon == 1}checked{/if}>
                  <label class="form-check-label"><i class="glyphicon glyphicon-ok-sign text-info"></i> 情報</label>
                </div>

                <div class="form-check">
                  <input type="radio" class="form-check-input" name="notif_icon" value="2" {if $notif_icon == 2}checked{/if}>
                  <label class="form-check-label"><i class="glyphicon glyphicon-warning-sign text-warning"></i> 警告</label>
                </div>

                <div class="form-check">
                  <input type="radio" class="form-check-input" name="notif_icon" value="3" {if $notif_icon == 3}checked{/if}>
                  <label class="form-check-label"><i class="glyphicon glyphicon-bell text-danger"></i> 危険</label>
                </div>
              </div>

            </div>
						<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<a href="./notification.php" class="btn btn-default" role="button">キャンセル</a>
								<button type="submit" name="button" class="btn btn-primary" value="{$submitButton}"><i class="glyphicon glyphicon-ok"></i> {$submitButton}</button>
							</div>
						</div>
						<input type="hidden" name="uid" value="{$uid}" />
					</div>
				</div>
			</form>
		</div>
	{/if}
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.input-group.date').datepicker({
            format: 'yyyy/mm/dd',
            calendarWeeks: true,
            todayHighlight: true,
            autoclose: true,
            language: "ja"
        });
    })

</script>