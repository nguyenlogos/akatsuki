<style>
  .bg-er {
      margin-left: -15%;
  }
  .bg-er img { margin: 30px 0 30px;}
  a { color: #705AB2; text-decoration: none;}
  .bg-er h1 { color: #000; font-size: 48px; margin: 0;}
  .bg-er p { color: #000; font-size: 18px; margin: 6px 0 0 0;}
</style>
<div class="text-center bg-er">
  <img src="/assets/images/notfound.png"/>
  <h1>404 エラー!</h1>
  <p>探しているページを見つけませんでした。</p>
  <p style="margin-top:45px">
    <a href="/managerids/">&raquo; ログイン画面</a>
  </p>
</div>