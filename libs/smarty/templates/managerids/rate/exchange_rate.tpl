<script type="text/javascript">
    $(function() {
        $('#deleteData').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var recipient1 = button.data('dataid');
            var recipient2 = button.data('name');
            var modal = $(this);

            modal.find('.uid').text(recipient1);
            modal.find('.title').val(recipient1);
            modal.find('.content').text(recipient2);
        });
    });

</script>
<div class="col-xs-12">
    <div class="row">
		{if $msg != "" && $err == 1}
            <div class="alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <p>{$msg}</p>

            </div>
		{elseif $msg != "" && $err == 0}
            <div class="alert alert-dismissible alert-info">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <p>{$msg}</p><br/>
            </div>
		{/if}
    </div>
</div>
{if $data_edit == NULL}
<div class="mt-5">
    <form id="new_form" method="post">
        <div class="row">

            <div class="col-xs-12 col-sm-2 col-md-2">
                <label>通貨 <font color="red">*</font></label>
                <div class="form-group">
                    <input type="text" name="num_rate" class="form-control" id="check_price" placeholder="100" value="{$num_rate}" required>
                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="form-group">
                    <label>から</label>
                    <select class="form-control" name="usd_rate">
                        <option value="USD">USD - U.S. Dollar</option>
                    </select>

                </div>
            </div>

            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="form-group">
                    <label>まで</label>
                    <select class="form-control" name="jpy_rate">
                        <option value="JPY">JPY - Japanese Yen</option>
                    </select>

                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="form-group">
                    <label>年月 <font color="red">*</font></label>
                    <div class="input-group date">
                        <input type="text" name="date_rate" class="form-control date-input" autocomplete="off" value="{$date_rate}" required>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-1 col-md-1">
                <label>保存</label>
                <div class="form-group">
                    <button type="submit" name="currency" class="btn btn-primary">保存</button>
                </div>
            </div>
        </div>
    </form>

</div>



  {if $paginationStr}
      <div class="col-xs-12">
          <div class="row">
              {$paginationStr}
          </div>
      </div>
  {/if}

    <table class="table table-hover sortable">
        <thead>
            <tr>
                <th class="sort-header">年月</th>
                <th class="sort-header">通貨</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$data item=value}
                <tr>
                    <td>{$value['ym']}</td>
                    <td>{$value['dy']}  円</td>
                    <td>
                      <a href="./exchangerate.php?ym={$value['ym']}" class='btn btn-primary'>変更</a>
                        <!--<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#changeData' data-dataid='{$value['ym']}' data-name='{$value['dy']}''>変更</button>-->
                        <button type='button' class='btn btn-danger' data-toggle='modal' data-target='#deleteData' data-dataid='{$value['ym']}' data-name='{$value['dy']} 円'>削除</button>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>


  {if $paginationStr}
      <div class="col-xs-12">
          <div class="row">
        {$paginationStr}
          </div>
      </div>
  {/if}

  {else}
    {foreach from=$data_edit item=value_edit}
      <form method="POST" action="./exchangerate.php">

        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="form-group">
            <label>通貨 <font color="red">*</font></label>
            <input type="text" class="form-control num" id="check_price_edit" value="{$value_edit['dy']}" name="num_rate" required />
            <input type="hidden" value="{$value_edit['ym']}" name="ym_edit" />
          </div>
        </div>

        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="form-group">
            <label>年月 <font color="red">*</font></label>
            <div class="input-group date">
              <input type="text" name="date_rate" class="form-control date-input" value="{$value_edit['ym']}" autocomplete="off" required>
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
          <label>保存</label>
          <div class="form-group">
            <button type="submit" name="update" class="btn btn-primary" value="保存"><i class="glyphicon glyphicon-ok"></i> 保存</button>
          </div>
        </div>
      </form>
    {/foreach}

{/if}

<div class="modal" tabindex="-1" role="dialog" id="deleteData">
    <div class="modal-dialog">
        <div class="modal-content" style="padding:10px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"> レート削除</h4>
            </div>
            <form action="{$curURI}" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                      <label>通貨</label><p class="content"></p>
                    </div>
                    <div class="form-group">
                        <label>年月</label><p class="uid"></p>
                        <input type="hidden" class="form-control title" name="title" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                    <button type="submit" name="button" class="btn btn-danger" value="削除"><i class="glyphicon glyphicon-remove"></i> 削除</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.input-group.date').datepicker({
            format: 'yyyy/mm',
            viewMode: "months", //this
            minViewMode: "months",//and this
            language: "ja"
        });
        var isEdit = '{$data_edit}' + '';
        var $rate;
        if (isEdit.trim() === '') {
            $rate = $('#check_price');
        } else {
            $rate = $('#check_price_edit');
        }
        $('#new_form').on('submit', function(e){
            var value = $rate.val();
            if (!value.match(/^(\d+\.\d+$)/)) {
                $rate.addClass('error');
                e.preventDefault();
            } else if (!value.match(/\d+/)) {
                $rate.addClass('error');
                e.preventDefault();
            }
        });
    })
</script>