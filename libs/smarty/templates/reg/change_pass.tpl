<h1>パスワードをリセットする</h1>
<div class="login-form">
    <form action="" method="post" class="form-box title-form-01">
        <div class="form-group row justify-content-md-center">
            <div class="col-md-9">
                <div class="row">
                    <label class="sr-only" for="login">新しいパスワード</label>
                    <div class="input-group mb_20">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-lock fa-fw"></i></div>
                        </div>
                        <input type="password" class="form-control" name="pass" placeholder="新しいパスワード" required autofocus>
                    </div>
                    <label class="sr-only" for="login">新しいパスワード(再度)</label>
                    <div class="input-group mb_20">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-lock fa-fw"></i></div>
                        </div>
                        <input type="password" class="form-control" name="pass2" placeholder="新しいパスワード(再度)" required>
                    </div>
                </div>

            </div>
        </div>
        <div class="form-group row justify-content-md-center">
            <div class="col-md-9">
                {if $errmsg}
                    <div class="row alert alert-dismissible alert-danger">
                        {$errmsg}
                    </div>
                {/if}
            </div>
        </div>
        <div class="form-group row justify-content-md-center">
            <div class="col-md-9 text-center">
                <button type="submit" name="btn" class="btn btn-orange pull-block">
                    パスワードをリセットする
                </button>
            </div>
        </div>
    </form>
</div>