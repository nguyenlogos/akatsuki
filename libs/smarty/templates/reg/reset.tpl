<h1>パスワードの再設定</h1>
<div class="login-form-wrap">
    <form class="form-box title-form-01" method="post">
        <div class="row justify-content-md-center submit-contents">
            <div class="col-md-9">
                {if $errmsg}
                    <div class="alert alert-danger">
                        {$errmsg}
                    </div>
                {/if}
            </div>
        </div>
        <div class="form-group row justify-content-md-center">
            <div class="col-md-9">
                <div class="row">
                パスワードを再設定します。<br />
                新しいパスワードを入力して、[リセット]ボタンを押してください。<br /><br />
                </div>
                <div class="row">
                    <label class="sr-only" for="login">メールアドレス</label>
                    <div class="input-group mb_20">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-lock fa-fw"></i></div>
                        </div>
                        <input type="password"
                               class="form-control"
                               name="pass"
                               placeholder="新しいパスワード"
                               required
                               autofocus>
                    </div>
                    <label class="sr-only" for="login">パスワードを入力</label>
                    <div class="input-group mb_20">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-lock fa-fw"></i></div>
                        </div>
                        <input type="password"
                               class="form-control"
                               name="pass2"
                               placeholder="新しいパスワード(再度)"
                               required>
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-md-center submit-contents">
            <div class="col-md-3 text-center">
                <button type="submit" class="btn btn-orange pull-block">
                    <i class="fas fa-check"></i>リセット
                </button>
            </div>
        </div>
    </form>
</div>
