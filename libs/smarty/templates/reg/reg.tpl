{if $msg != "" && $err == 2}
  <div class="alert alert-normal text-center">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
    <p class="mb-0"><a href="/" class="btn btn-orange mt-4">ログイン画面</a></p>
  </div>
{elseif $policy_pass != "" && $err == 1 || $msg != "" && $err == 1}
  <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {$msg}
    <p class="mb-0">{$policy_pass}</p>
  </div>
{/if}
{if $err == "1" || ($err == "0" && $msg == "") }
  <div class="sign_up mt-5">
    <div class="sign-header">
      {*<p>*}
        {*登録されている方はこちらへ<br/>*}
        {*<a href="/" class="pull-right btn btn-orange mt-4">ログイン画面</a>*}
      {*</p>*}
      {*<div class="line-or">または</div>*}
      <h4 class="card-title">新規登録される方は、下記の必要事項を入力してサインアップしてください。</h4>
    </div>

    <article class="sign-body mt-4">
      <form method="post" action="" id="reg_form" class="form-box title-form-01">
        <div class="form-row">
          <div class="form-group col-md-6 my-3">
            <label>お名前 <span class="badge badge-large badge-red">必須</span></label>
            <input type="text"
                   class="form-control {if $name_err}error{/if}"
                   id="name"
                   name="name"
                   maxlength="64"
                   placeholder="64文字まで" 
                   value="{$name|escape}"
            />
            {if $name_err}<label class="error">{$name_err}</label>{/if}
          </div>
          <div class="form-group col-md-6 my-3">
            <label>電話番号 <span class="badge badge-large badge-red">必須</span></label> 例) 03-5484-7811
            <input type="text"
                   maxlength="15"
                   name="tel"
                   id="phone"
                   class="form-control {if $tel_err}error{/if} phoneCheck"
                   placeholder="半角ハイフンで区切って市外局番から入力" 
                   value="{$tel|escape}"
            />
            {if $tel_err}<label class="error">{$tel_err}</label>{/if}
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12 my-3">
            <label>会社名 <span class="badge badge-large badge-red">必須</span></label>
            <input type="text"
                   name="company"
                   id="company"
                   class="form-control {if $company_err}error{/if}"
                   maxlength="128"
                   placeholder="128文字まで。法人格からの正式名称" 
                   value="{$company|escape}"
            />
            {if $company_err}<label class="error">{$company_err}</label>{/if}
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6 my-3">
            <label>部署名(任意)</label>
            <input type="text"
                   name="dept"
                   id="dept"
                   class="form-control"
                   maxlength="64"
                   placeholder="64文字まで" 
                   value="{$dept|escape}"
            />
          </div>
          <div class="form-group col-md-6 my-3">
            <label>役職(任意)</label>
            <input type="text"
                   name="position"
                   id="position"
                   class="form-control"
                   maxlength="64"
                   placeholder="64文字まで" 
                   value="{$position|escape}"
            />
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6 my-3">
            <label>メールアドレス <span class="badge badge-large badge-red">必須</span></label>
            <input type="email"
                   name="email"
                   id="email"
                   class="form-control {if $email_err}error{/if}"
                   maxlength="64"
                   placeholder="64文字まで" 
                   value="{$email|escape}"
            />
            {if $email_err}<label class="error">{$email_err}</label>{/if}
          </div>
          <div class="form-group col-md-6 my-3">
            <label>メールアドレス(再入力) <span class="badge badge-large badge-red">必須</span></label>
            <input type="email"
                   name="email2"
                   id="confirm_email"
                   class="form-control {if $email_confirm_err}error{/if}"
                   maxlength="64"
                   placeholder="64文字まで"
                   value="{$email2|escape}"
            />
            {if $email_confirm_err}<label class="error">{$email_confirm_err}</label>{/if}
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6 my-3">
            <label>パスワード <span class="badge badge-large badge-red">必須</span></label>
            <input type="password"
                   name="pass"
                   maxlength="32"
                   id="password"
                   class="form-control {if $pass_err}error{elseif $policy_pass != "" && $err == 1}error{/if} polyci_pass"
                   placeholder="32文字まで"
                   value="{$pass|escape}"
            />
            {if $pass_err}<label class="error">{$pass_err}</label>{/if}
          </div>
          <div class="form-group col-md-6 my-3">
            <label>パスワード(再入力) <span class="badge badge-large badge-red">必須</span></label>
            <input type="password"
                   name="pass2"
                   maxlength="32"
                   id="confirm_password"
                   class="form-control {if $pass_confirm_err}error{/if}"
                   placeholder="32文字まで"
                   value="{$pass2|escape}"
            />
            {if $pass_confirm_err}<label class="error">{$pass_confirm_err}</label>{/if}
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-12 my-3">
            <div class="form-check-required">
              <input class="form-check-input" type="checkbox" name="agree" id="agree" {$agree|escape}/>
              <label for="agree" class="form-check-label">利用規約に同意しました。</label>
            </div>
            {if $agree_err}<label class="error">{$agree_err}</label>{/if}
          </div>
          <div class="form-group col-md-12 my-3 text-center">
            <div class="g-recaptcha d-inline-block"
                 data-sitekey="{$G_RECAPTCHA_SITE_KEY}"
                 data-callback="recaptchaCallback"></div>
            <input type="hidden" class="hiddenRecaptcha" name="hiddenRecaptcha" id="hiddenRecaptcha">
          </div>
        </div>

        <div class="row justify-content-md-center submit-contents mt-5">
          <div class="col-md-3 text-center">
            <button class="btn btn-orange bt-sign" id="update_sign" value="サインアップ">サインアップ</button>
          </div>
        </div>

        <div class="row justify-content-md-center submit-contents mt_60">
          <div class="col-md-6 text-center">
            <p>既にアカウントをお持ちですか？ <a href="/" class="text-link">ログイン画面</a></p>
          </div>
        </div>

      </form>
    </article>
  </div>
{/if}
<script>
  function recaptchaCallback(token) {
    $('#hiddenRecaptcha').valid();
  }
  // $('#reg_form').on('submit', function(e){
  //   e.preventDefault();
  // })
  $('#reg_form').validate({
      rules: {
          tel: {
              maxlength: 15
          },
          email: {
              maxlength: 64
          },
          email2: {
              maxlength: 64
          },
          pass: {
              maxlength: 32
          },
          pass2: {
              maxlength: 32
          }
      }
  });
</script>
