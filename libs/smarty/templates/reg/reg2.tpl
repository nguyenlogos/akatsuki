{if $msg != "" && $err == 1}
<div class="alert alert-danger text-center">
  {$msg}
  <p class="mt-4"><button type="button" class="close" data-dismiss="alert">$B!_(B</button></p>
</div>
{elseif $msg != "" && $err == 0}
<div class="alert alert-normal text-center">
  {$msg}
  <p><button type="button" class="close" data-dismiss="alert">$B!_(B</button></p>
</div>
{/if}
