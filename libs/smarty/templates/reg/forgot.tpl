{if $msg != "" && $err == 2}
    <div class="alert alert-normal">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <a href="/" class="pull-right btn btn-outline-primary mt-2">ログイン画面</a>
        {$msg}
    </div>
{elseif $msg != "" && $err == 1}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$msg}
    </div>
{/if}
<h1>パスワードの再設定</h1>
<div class="login-form-wrap">
    <form class="form-box title-form-01" action="" method="post">
        <div class="form-group row justify-content-md-center">
            <div class="col-md-9">
                <div class="row">
                    <p>登録済みのメールアドレスを入力して[リセット]ボタンを押してください。</p>
                    <label class="sr-only" for="login">メールアドレス</label>
                    <div class="input-group mb_20">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="far fa-envelope fa-fw"></i></div>
                        </div>
                        <input type="email"
                               class="form-control"
                               name="email"
                               placeholder="メールアドレス"
                               required="required" autofocus>
                    </div>
                </div>

            </div>
        </div>
        <div class="row justify-content-md-center submit-contents">
            <div class="col-md-3 text-center">
                <button type="submit" class="btn btn-primary btn-orange">リセット</button>
            </div>
        </div>
        <div class="row justify-content-md-center submit-contents">
            <div class="col-md-9">
                <p class="text-center mt-5"><a class="text-link" href="/">ログイン画面に戻る</a></p>
            </div>
        </div>
    </form>
</div>
