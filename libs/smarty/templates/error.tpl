<div class="alert alert-danger text-center">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <p>{$msg}</p>
  <p class="mt-5 mb-0"><a class="btn btn-orange" href="/">ログイン画面</a></p>
</div>
