{if $active != ""}
<div class="alert alert-normal mgT">
    {$active}
</div>
{elseif $lock != ""}
<div class="alert alert-danger mgT">
    {$lock}
</div>
{else}
    {if $msg != ""}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {/if}
    <h1>ログイン</h1>
    <div class="login-form-wrap">
        <form class="form-box title-form-01" action="/index.php" method="post">
            <div class="form-group row justify-content-md-center">
                <div class="col-md-9">
                    <div class="row">
                        <label class="sr-only" for="login">メールアドレス</label>
                        <div class="input-group mb_20">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="far fa-envelope fa-fw"></i></div>
                            </div>
                            <input type="text"
                                   class="form-control"
                                   id="inputEmail"
                                   name="Email"
                                   placeholder="メールアドレス"
                                   required="required"
                                   autofocus
                            >
                        </div>
                        <label class="sr-only" for="login">パスワードを入力</label>
                        <div class="input-group mb_20">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><i class="fas fa-lock fa-fw"></i></div>
                            </div>
                            <input type="password"
                                   class="form-control"
                                   id="inputPassword"
                                   name="pass"
                                   placeholder="パスワード"
                                   required="required">
                        </div>
                        <p>パスワードを忘れた方は<a class="text-link" href="/reg/forgot.php">こちらから</a></p>
                    </div>

                </div>
            </div>
            <div class="row justify-content-md-center submit-contents">
                <div class="col-md-3 text-center">
                    <button type="submit"
                            name="btn"
                            class="btn btn-orange pull-block">
                        ログイン
                    </button>
                </div>
            </div>
            <div class="row justify-content-md-center submit-contents">
                <div class="col-md-9">
                    <p class="text-center mt-5">無料サインアップは
                        <a class="text-link" href="/reg/reg.php">こちら</a>
                        からどうぞ</p>
                </div>

            </div>
        </form>
    </div>
{/if}