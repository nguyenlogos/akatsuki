<div id="message">
    {if $errmsg != ''}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    {/if}
    {if $msg != ''}
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    {/if}
</div>
<div class="block-content">
    <p>申請内容を確認して[承認]または[却下]を選択してください。</p>
</div>
<form method="get">
    <div class="form-row">
        <div class="form-group col-md-6 col-lg-6 my-1">
            <select class="form-control" name="f_type">
                <option value="ec2" {($frmValues['f_type'] == "ec2") ? "selected" : ""}>インスタンスの申請</option>
                <option value="ebs" {($frmValues['f_type'] == "ebs") ? "selected" : ""}>ディスクの申請</option>
            </select>
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1">
            <button type="submit" class="btn btn-orange btn-filter">表示</button>
        </div>
        <div class="form-group col-md-3 col-lg-4 my-1 text-right">
            <div class="form-check d-inline-block">
                <input class="form-check-input" type="checkbox" name="f_approved" {($frmValues['f_approved'] > 0) ? "checked" : ""}>
                <label class="form-check-label">承認済</label>
            </div>
        </div>

    </div>
</form>
{if ($reqList|@count) == 0}
    <div class="alert alert-normal">申請はありません。</div>
{else}
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="table-responsive">
    {tablelist headers=$headerList data=$reqList actions=$actionList}
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<form name="frmApproval" method="post">
    <input type="hidden" name="id" value="">
    <input type="hidden" name="action" value="req_approve">
</form>
{/if}
<input type="hidden" name="sk" value="{$sortIndex}">
<div class="instance-forms"></div>
<script type="text/javascript">
    const ACTION_ROLES = {
        '1' : {
            'role_manager' : ['inst_delete'],
            'role_owner' : ['inst_request_delete'],
        },
        '0' : {
            'role_manager' : ['inst_reject', 'inst_approve'],
            'role_owner' : ['inst_delete'],
        },
        '-1' : {
            'role_owner' : [,'inst_delete'],
        },
        '-3' : {
            'role_manager' : ['inst_reject_delete', 'inst_approve_delete'],
        },
        '-4' : {
            'role_manager' : [],
            'role_owner' : [],
        },
        '-5' : {
            'role_manager' : ['inst_reject', 'inst_approve'],
            'role_owner' : ['inst_delete'],
        }
    };
    const EDITABLE_STATES = [-5, -1, 0, 1];

    $(document).ready(function(){
        var isAdmin = +('{$smarty.session.sysadmin}' + '');
        var isManager = +('{$smarty.session.admin}' + '');
        var currentEmpid = +('{$smarty.session.empid}' + '');
        var instFN = new Instance();

        var downloadable = false;
        $('.sortable tbody > tr').each(function(i, e){
            var itemID = $(e).data('item-id');
            var empID = $(e).find('[data-item-empid]').data('itemEmpid');
            var changeState = $(e).find('[data-item-change_state]').data('itemChange_state');
            var reqState = changeState ? 0 : +$(e).find('td.req-state').text();
            if (!ACTION_ROLES[reqState]) {
                console.error('Invalid State');
            }
            var actions  = [];
            if (isAdmin || isManager) {
                actions = _.clone(ACTION_ROLES[reqState]['role_manager']);
            } else {
                actions = _.clone(ACTION_ROLES[reqState]['role_owner'])
            }
            if (empID == currentEmpid && EDITABLE_STATES.indexOf(reqState) > -1) {
                actions.unshift('inst_edit');
            }

            $(e).find('td > .btn-act').each(function(i, btn){
                $btn = $(btn).removeClass('hide');
                $(btn).on('click', function(){
                    instFN.inst_view(itemID, actions);
                });
            });
        });

        dialog.on('closed', function(){
            dialog.setState();
        });

        $('[name="f_approved"]').change(function(){
            $(this).closest('form').submit();
        });
    });
</script>
<script type="text/javascript">
    window.history.replaceState({}, '', window.location.pathname + window.location.search);
    (function(){
        var $instanceForms = $('.instance-forms');
        var TEMPLATES = {
            'btn_cancel': {
                'button': '<button type="button" class="btn btn-secondary btn-cancel">キャンセル</button>',
                'callback': 'dialog.close'
            },
        };

        var DIALOG_TITLES = {
            'inst_view' : 'インスタンスの情報',
        };

        var DIALOG_BUTTON_CONFIGS = {
            'inst_view' : {
                'icon' : '<i class="fas fa-eye"></i>&nbsp;情報',
                'class' : 'btn-blue',
            },
            'inst_edit' : {
                'icon' : '<i class="fas fa-edit"></i>&nbsp;変更',
                'class' : 'btn-blue',
                'callback' : function(reqInfo) {
                    window.location.href = '/cons/req_inst.php?id='+ reqInfo.id;
                }
            },
            'inst_delete' : {
                'icon' : '<i class="fas fa-trash"></i>&nbsp;削除',
                'class' : 'btn-danger',
                'callback' : true,
            },
            'inst_approve' : {
                'icon' : '<i class="fas fa-play-circle"></i>&nbsp;承認',
                'class': 'btn-orange',
                'callback' : true,
            },
            'inst_approve_delete' : {
                'icon' : '<i class="fas fa-play-circle"></i>&nbsp;承認',
                'class': 'btn-approve btn-orange',
                'callback' : true,
            },
            'inst_reject' : {
                'icon' : '<i class="fas fa-window-restore"></i>&nbsp;却下',
                'class': 'btn-danger btn-reject',
                'callback' : function(reqInfo, btn) {
                    var frmName = 'frm_'+reqInfo.action;
                    var $frmAction = $('form[name="'+frmName+'"]');
                    var $requiredInputs = $frmAction.find('[data-required="1"]').prop("required", "required");
                    $frmAction.removeClass('hide');
                    var btns = $(btn).siblings('.btn-inst').remove();
                    if (btns.length == 0) {
                        $frmAction.valid() && $frmAction.submit();
                    }
                }
            },
            'inst_reject_delete' : {
                'icon' : '<i class="fas fa-window-restore"></i>&nbsp;却下',
                'class': 'btn-danger btn-reject',
            },
            'inst_request_delete' : {
                'icon' : '<i class="fas fa-times-circle"></i>&nbsp;削除依頼',
                'class': 'btn-sm btn-warning w-100',
            },
            'inst_download_keypair' : {
                'icon' : 'DownloadKey',
            },
        };

        var ActionForm = function(reqInfo) {
            var frmName = 'frm_' + reqInfo.action;
            var frmNote = reqInfo.has_note ? `
                <div class="mt_20 error_sub">
                    <textarea name="req_note"
                        class="form-control no-br"
                        placeholder="説明"
                        maxlength="512"
                        rows="4"
                        autofocus
                        data-required="1"></textarea>
                </div>
            ` : '';
            var actionForm = `
                <form name="`+frmName+`" method="post" class="hide" style="margin-bottom: 0;">
                    <input type="hidden" name="id" value="`+reqInfo.id+`">
                    <input type="hidden" name="action" value="`+reqInfo.action+`">
                    `+frmNote+`
                    <input type="submit" id="`+frmName+`" hidden />
                </form>
            `;
            var btnConfigs = DIALOG_BUTTON_CONFIGS[reqInfo.action] || {};
            var button = {
                'button' : `
                    <button type="button" class="btn btn-inst btn-custom `+btnConfigs.class+`">
                        `+btnConfigs.icon+`
                    </button>
                `,
                'callback' : function() {
                    var $actionForm = $('form[name="'+frmName+'"]');
                    if (typeof btnConfigs.callback === 'function') {
                        btnConfigs.callback(reqInfo, this);
                        return;
                    }
                    if (!btnConfigs.callback) {
                        $actionForm.submit();
                        return;
                    }
                    var $this = $(this);
                    var html = $this.html();
                    $this.prop('disabled', 'disabled')
                        .html('<i class="fas fa-redo spinner"></i>&nbsp;承認');
                    dialog.setState('no-close');
                    var errmsg = '不明なエラーが発生しました。';
                    window.util.ajaxPost('/api/inst_req.php', {
                        'id': reqInfo.id,
                        'action' : reqInfo.action,
                    }).then(function(response){
                        if (response) {
                            if (response.result) {
                                errmsg = '';
                            } else if (response.message) {
                                errmsg = response.message;
                            }
                        }
                    }).always(function(){
                        if (errmsg) {
                            toastr.error(errmsg);
                            $this.removeProp('disabled').html(html);
                            dialog.setState();
                        } else {
                            $actionForm.submit();
                        }
                    });
                }
            };
            return {
                html : actionForm,
                button : button
            };
        };

        function Instance() {};

        Instance.prototype.inst_view = function(itemID, actions) {
            var showConfirmDialog = function(requestInfo) {
                var showNote = !!requestInfo.note;
                var tableInfo = generateTableTemplate(requestInfo, showNote);
                var html = '';
                var buttons = [];
                actions.forEach(function(action){
                    var form = new ActionForm({
                        id : itemID,
                        action : action,
                        has_note : action === 'inst_reject'
                    });
                    html += form.html;
                    buttons.push(form.button);
                });
                tableInfo += html;
                buttons.unshift(TEMPLATES.btn_cancel);
                dialog.confirmDialog(tableInfo, {
                    'title' : DIALOG_TITLES.inst_view,
                    'modalClass' : 'modal-xl',
                    'buttons' : buttons
                });
            };
            getRequestInfo(itemID).then(function(response){
                if (!response) {
                    return;
                }
                showConfirmDialog(response);
            });
        };

        function generateTableTemplate(requestInfo, shownote) {
            var headerClass = 'col-xs-3';
            var contentClass = 'col-xs-9';
            var noteStr = '';
            if (shownote && requestInfo.note) {
                var noteClass = 'alert-danger';
                noteStr = `
                    <div class="alert no-br `+noteClass+`">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        `+requestInfo.note+`
                    </div>
                `
            }
            if (requestInfo['change_state']) {
                var editableFields = [
                    'deptname',
                    'pcode',
                    'pname',
                    'using_purpose',
                    'inst_address',
                ];
                editableFields.forEach(function(field){
                    var old_value = requestInfo[field] || null;
                    var new_value = requestInfo[field+'_new'] || null;
                    if (old_value != new_value) {
                        requestInfo[field] = old_value + ' ⇒ ' + new_value;
                    }
                });
            }
            var htmlStr = noteStr + `
                <table class="table table-bordered table-striped table-wrap mb_20">
                    <colgroup>
                        <col width="50%">
                        <col width="50%">
                    </colgroup>
                    <tr>
                        <td class="`+headerClass+`">申請者</td>
                        <td class="`+contentClass+`">`+requestInfo.uname+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">所属グループ</td>
                        <td class="`+contentClass+`">`+requestInfo.deptname+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">プロジェクトコード</td>
                        <td class="`+contentClass+`">`+requestInfo.pcode+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">プロジェクト名</td>
                        <td class="`+contentClass+`">`+requestInfo.pname+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">データセンター</td>
                        <td class="`+contentClass+`">`+requestInfo.inst_region+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">AMI</td>
                        <td class="`+contentClass+`">`+requestInfo.inst_ami+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">インスタンスタイプ</td>
                        <td class="`+contentClass+`">`+requestInfo.inst_type+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">インスタンス個数</td>
                        <td class="`+contentClass+`">`+requestInfo.inst_count+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">シャットダウン時の後処理</td>
                        <td class="`+contentClass+`">`+requestInfo.shutdown_behavior+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">コメント</td>
                        <td class="`+contentClass+`">`+requestInfo.using_purpose+`</td>
                    </tr>
                     <tr>
                        <td class="`+headerClass+`">キー名</td>
                        <td class="`+contentClass+`">`+requestInfo.key_name+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">ＩＰアドレス</td>
                        <td class="`+contentClass+`">`+requestInfo.inst_address+`</td>
                    </tr>
                </table>
                @bstable
            `;
            var bsStr = `
                <table class="table table-bordered table-striped" style="margin-bottom: 0;">
                    <tr>
                        <th>デバイス名</th>
                        <th>デバイスの種類</th>
                        <th>サイズ(GiB)</th>
                        <th>IOPS</th>
                        <th>自動削除</th>
                        <th>暗号化</th>
                        <th>コピー元スナップショット</th>
                    </tr>
                    @nodes
                </table>
            `;
            var devices = requestInfo.inst_bs || [];
            var nodes = [];
            devices.forEach(function(device){
                device.iops || (device.iops = 'Auto');
                device.snapshot_id || (device.snapshot_id = 'None');
                device.auto_removal = device.auto_removal == '1' ? 'Yes' : 'No';
                device.encrypted = device.encrypted == '1' ? 'Yes' : 'No';
                var node = `
                    <tr>
                        <td>`+device.device_name+`</td>
                        <td>`+device.volumn_type+`</td>
                        <td>`+device.volumn_size+`</td>
                        <td>`+device.iops+`</td>
                        <td>`+device.auto_removal+`</td>
                        <td>`+device.encrypted+`</td>
                        <td>`+device.snapshot_id+`</td>
                    </tr>
                `;
                nodes.push(node);
            });
            if ( nodes.length === 0 ) {
                nodes.push('<tr><td colspan="7" class="text-center">データなし</tr>');
            }
            bsStr = bsStr.replace(/@nodes/, nodes.join(''));
            htmlStr = htmlStr.replace(/@bstable/, bsStr);
            return htmlStr;
        };

        function getRequestInfo(itemID) {
            return $.when(
                $.ajax({
                    'url' : './request.php',
                    'type' : 'get',
                    'data' : {
                        'ajax' : 1,
                        'id' : itemID
                    },
                    'success' : function(response){
                        if ( !response ) {
                            toastr.error('Failed to get instance information');
                        }
                    }
                })
            );
        };
        window.Instance = Instance;
    })();
</script>
