{if $msg != ""}
    {if $err == 1}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {else}
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {/if}
{/if}
<div class="block-content">
    <p>ご契約窓口のご担当者様のプロファイルを編集して、[変更]ボタンを押してください。</p>
    <p>ご契約に関して等、弊社よりご連絡・お問い合わせをさせていただく場合がございます。</p>
</div>

<form METHOD="POST" ACTION="/cons/profile.php" class="form-box title-form-01" id="reg_form">
    <div class="form-group row">
        <label class="col-md-3 col-form-label">お名前</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input type="text" class="form-control" name="name" placeholder="64文字まで" maxlength="64" value="{$name|escape}" required/>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label">会社名</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input type="text" name="company" class="form-control" placeholder="128文字まで" maxlength="128" value="{$company|escape}" required/>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label">部署名(任意)</label>
        <div class="col-md-1"></div>
        <div class="col-md-7">
            <input type="text" name="dept" class="form-control" placeholder="64文字まで" maxlength="64" value="{$dept|escape}" />
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label">役職(任意)</label>
        <div class="col-md-1"></div>
        <div class="col-md-7">
            <input type="text" name="position" class="form-control" placeholder="64文字まで" maxlength="64" value="{$position|escape}" />
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label">電話番号</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input type="text"
                   maxlength="15"
                   name="tel"
                   id="phone"
                   class="form-control {if $tel_err}error{/if}"
                   value="{$tel|escape}"
            />
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label">メールアドレス</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input type="email" name="uid" class="form-control" placeholder="64文字まで" maxlength="64" value="{$uid|escape}" id="email" required/>
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label">契約</label>
        <div class="col-md-1"></div>
        <div class="col-md-7">
            <div class="toggle mt-4">
                <input type="checkbox" name="plan" onclick='changeValueCheckbox(this)'
                       class="check_lock" id="check"
                       value='{$plan}' {$check_lock} >
                <label for="check"></label>
            </div>
        </div>
    </div>

    <div class="form-group row submit-contents">
        <div class="col-md-4"></div>
        <div class="col-md-7">
            <input type="submit" name="button" class="btn btn-orange" value="保存" />
        </div>

    </div>
</form>
<script type="text/javascript">
    $('form').validate({
        rules: {
            name: {
                maxlength: 64
            },
            company: {
                maxlength: 128
            },
            dept: {
                maxlength: 64
            },
            position: {
                maxlength: 64
            },
            uid: {
                maxlength: 64
            },
            tel: {
                phonecheck: true
            }
        }
    });
</script>