{if !$frmValues['sg_id']}
    {assign var="sg_new_css" value='active show'}
    {assign var="sg_existing_css" value=''}
{else}
    {assign var="sg_new_css" value=''}
    {assign var="sg_existing_css" value='active show'}
{/if}
{if $errmsg != ""}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$errmsg}
    </div>
{/if}
<div class="bs-wizard row">
    <div class="col-md-3 bs-wizard-step complete">
        <div class="text-center bs-wizard-stepnum">インスタンスの設定</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step complete">
        <div class="text-center bs-wizard-stepnum">ストレージの設定</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step active">
        <div class="text-center bs-wizard-stepnum">セキュリティグループの作成</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step disabled">
        <div class="text-center bs-wizard-stepnum">最終確認 </div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>
</div>

<div>
    <h2>Assign a security group :</h2>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="nav-item">
            <a class="nav-link {$sg_new_css}" href="#sg_new" role="tab" data-toggle="tab">
                Create a new security group
            </a>
        </li>
        <li role="presentation" class="nav-item">
            <a class="nav-link {$sg_existing_css}" href="#sg_existing" role="tab" data-toggle="tab">
                Select an existing security group
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane {$sg_new_css}" id="sg_new">
            <form name="frmCreateSg" method="POST">
                <div class="sg-info row justify-content-md-center submit-contents mt-5">
                    <div class="col-md-9">
                        <div class="form-group my-1 row">
                            <label class="col-md-2 col-form-label my-1">ＶＰＣ</label>
                            <div class="col-md-1 my-1"></div>
                            <div class="col-md-7 my-1">
                                <label class="vpc_sg">{$frmValues['sg_vpc']}</label>
                            </div>
                        </div>
                        <div class="form-group my-1 row">
                            <label class="col-md-2 col-form-label my-1">ＳＧ名</label>
                            <div class="col-md-1 my-1">
                                <span class="badge badge-large badge-red">必須</span></div>
                            <div class="col-md-7 my-1">
                                <input type="text"
                                    class="form-control"
                                    name="sg_name_new"
                                    value="{$frmValues['sg_name_new']}"
                                    required>
                            </div>
                        </div>
                        <div class="form-group my-1 row">
                            <label class="col-md-2 col-form-label my-1">ノート</label>
                            <div class="col-md-1 my-1">
                                <span class="badge badge-large badge-red">必須</span></div>
                            <div class="col-md-7 my-1">
                                <input type="text"
                                    class="form-control"
                                    name="sg_desc_new"
                                    value="{$frmValues['sg_desc_new']}"
                                    required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sg-rules">
                    <h3 class="h2 mt_60">Security group rules :</h3>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="nav-item">
                            <a class="nav-link active" href="#sg_inbound" aria-controls="sg_inbound" role="tab" data-toggle="tab">
                                Inbound
                            </a>
                        </li>
                        <li role="presentation" class="nav-item">
                            <a class="nav-link" href="#sg_outbound" aria-controls="sg_outbound" role="tab" data-toggle="tab">
                                Outbound
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane tab-sg active show" id="sg_inbound">
                            <div id="tbl_sg_inbound"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane tab-sg" id="sg_outbound">
                            <div id="tbl_sg_outbound"></div>
                        </div>
                        <button type="button" class="btn btn-green btn-add-rule" disabled>
                            <i class="fas fa-plus-circle"></i>&nbsp;新規登録
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div role="tabpanel" class="tab-pane {$sg_existing_css}" id="sg_existing">
            <form name="frmSelectSg" method="POST">
                <div id="tbl_sg_list"></div>
            </form>
        </div>
    </div>
    <div class="row justify-content-md-center">
        <button type="button" class="btn btn-orange btn-next-step hide">最終確認画面</button>
    </div>
</div>
<script>
    $(document).ready(function(){
        function loadSgObjs() {
            return window.util.ajax('/api/sg.php?type=obj');
        };
        function loadSgs(vpc) {
            return window.util.ajax('/api/sg.php?type=avail&vpc='+vpc);
        };
        const SG_TYPES = {
            Inbound  : 'inbound',
            Outbound : 'outbound',
        };
        const TBL_RULE_HEADERS = {
            'type' : {
                'text': 'タイプ',
                'width': 180
            },
            'proto' : {
                'text': 'プロトコル',
                'width': 130
            },
            'range' : {
                'text': 'ポート範囲',
                'width': 130
            },
            'cidr' : {
                'text': 'ＣＩＤＲ・ＩＰ',
                'width': 200
            },
            'desc' : {
                'text': 'ノート',
                'min_width' : 100,
            },
            'action' : {
                'text': '',
                'width': 100,
            },
        };
        const TBL_SG_HEADERS = {
            'chk' : {
                'text': '利用可能',
                'width': 100
            },
            'id' : {
                'text': 'セキュリティグループＩＤ',
                'width': 250
            },
            'name' : {
                'text': 'セキュリティグループ名',
                'width': 250
            },
            'desc' : {
                'text': 'ノート',
            }
        };
        const IP_SOURCE_TYPES = {
            MYIP : {
                'text'  : 'My IP',
                'value' : 'myip',
                'target': {
                    'ipaddr' : '{$CLIENTIP}'+ '',
                    'submask': '32',
                },
            },
            CUSTOM : {
                'text'  : 'Custom',
                'value' : 'custom',
                'target': {},
            },
            ANYWHERE: {
                'text'  : 'Anywhere',
                'value' : 'anywhere',
                'target': {
                    'ipaddr' : '0.0.0.0',
                    'submask': '0',
                }
            },
        };
        var g_index = 0;
        var g_caches = {};
        var g_hashing = {};
        var g_sgCount = 0;
        var SgRule = function(sg_rule) {
            sg_rule || (sg_rule = {});
            var _defaults = {
                'id' : ++g_index, // row index
                'stype' : '',     // source type,
                'proto' : '',     // protocol (TCP/UDP)
                'range' : '',     // port range
                'source': '',     // ＣＩＤＲ・ＩＰ source
                'target': '',     // ＣＩＤＲ・ＩＰ target
                'desc'  : '',     // description
                'type'  : '',     // inbound/outbound,
            };
            var properties = _.assignIn({}, _defaults, sg_rule);
            return properties;
        };

        var $btnAddRule = $('.btn-add-rule');
        $btnAddRule.checkStatus= function() {
            var isDisabled = (g_targetType === SG_TYPES.Inbound && $btnAddRule.inbound >= 5) ||
                (g_targetType === SG_TYPES.Outbound && $btnAddRule.outbound >= 5);
            $btnAddRule.attr('disabled', isDisabled);
            return isDisabled;
        };
        var $btnNextStep = $('.btn-next-step');
        var $frmCreateSg = $('form[name="frmCreateSg"]');
        var $frmSelectSg = $('form[name="frmSelectSg"]').css("margin-bottom", 0);

        var $tblSgInbound = generateTableTemplate(TBL_RULE_HEADERS).appendTo('#tbl_sg_inbound');
        var $tblSgOutbound = generateTableTemplate(TBL_RULE_HEADERS).appendTo('#tbl_sg_outbound');
        var $tblSgList = generateTableTemplate(TBL_SG_HEADERS).appendTo('#tbl_sg_list');

        var $sgInboundBody = $tblSgInbound.find('tbody');
        var $sgOutboundBody = $tblSgOutbound.find('tbody');
        var $sgListBody = $tblSgList.find('tbody');

        var g_selectedVpc = '{$frmValues["sg_vpc"]}' + '';
        var g_selectedSgId = '{$frmValues["sg_id"]}' + '';
        var g_targetSg;
        var g_targetType = SG_TYPES.Inbound;

        $('a[data-toggle="tab"]').click(function() {
            var href = this.getAttribute('href');
            var $target = $(href);
            $target.siblings().removeClass('show active');
            $target.addClass('show active');
            switch (href) {
                case '#sg_inbound':
                    g_targetType = SG_TYPES.Inbound;
                    break;
                case '#sg_outbound':
                    g_targetType = SG_TYPES.Outbound;
                    break;
                case '#sg_new':
                    g_targetSg = 'new';
                    break;
                case '#sg_existing':
                    g_targetSg = 'existing';
                    break;
                default:
                    break;
            }
            if (g_targetSg === 'existing' && g_sgCount === 0) {
                $btnNextStep.hide();
            } else {
                $btnNextStep.show();
            }
            $btnAddRule.checkStatus();
        });

        function generateSgStypeTemplate(stypes, sgRule, onchangeCallback) {
            var $select = $('<select>')
                    .attr({
                        'name' : 'sg_stype_'+sgRule.type+'_'+sgRule.id,
                        'required' : 'required',
                    })
                    .css({
                        'display' : 'none',
                    })
                if (typeof onchangeCallback === 'function') {
                    $select.change(function() {
                        onchangeCallback(this.value);
                    });
                };
            stypes.forEach(stype => {
                var $option =
                    $('<option>')
                        .attr('value', stype.name)
                        .text(stype.name);
                // if (sgRule.stype && sgRule.stype == stype.name) {
                //     $option.attr('selected', 'selected');
                // }
                $select.append($option);
            });

            return $select;
        };

        function generateIpSourceTemplate(sgRule, onchangeCallback) {
            var $select = $(`
                <select name="sg_source_`+sgRule.type+`_`+sgRule.id+`" style="display:none; margin-bottom: 5px;">
                    <option value="`+IP_SOURCE_TYPES.CUSTOM.value+`">`+IP_SOURCE_TYPES.CUSTOM.text+`</option>
                    <option value="`+IP_SOURCE_TYPES.ANYWHERE.value+`">`+IP_SOURCE_TYPES.ANYWHERE.text+`</option>
                    <option value="`+IP_SOURCE_TYPES.MYIP.value+`">`+IP_SOURCE_TYPES.MYIP.text+`</option>
                </select>
            `);
            if (typeof onchangeCallback === 'function') {
                $select.change(function() {
                    var type = this.value.toUpperCase();
                    onchangeCallback(IP_SOURCE_TYPES[type]);
                });
            };

            return $select;
        };

        function generateIpTargetTemplate(objs, sgRule, onchangeCallback) {
            var $optionDefault =
                $('<option>').attr('value', '').text('--- オブジェクトを選択 ---');
            var $select = $('<select>')
                    .attr({
                        'name' : 'sg_target_' + sgRule.type + '_' + sgRule.id,
                        'cidr' : true,
                        'required' : true,
                        'data-required' : true,
                    }).css({
                        'display' : 'none',
                    })
                    .append($optionDefault);
                if (typeof onchangeCallback === 'function') {
                    $select.change(function() {
                        onchangeCallback(this.value);
                    });
                };
            objs.forEach(obj => {
                var value = obj.ipaddr + '/' + obj.submask;
                var $option =
                    $('<option>')
                        .attr('value', value)
                        .text(value + ' ('+obj.name+')');
                // if (sgRule.target && sgRule.target == value) {
                //     $option.attr('selected', 'selected');
                // }
                $select.append($option);
            });

            return $select;
        };

        function generateTableRowTemplate(sgRule, onRemoveCallback) {
            var rowIndex = ++g_index;
            var $row = $('<tr>').attr('data-item-id', sgRule.id);

            var $btnDelete = $('<button type="button" class="btn btn-danger delete-record">').text("取消")
            $btnDelete.click(function() {
                if (typeof onRemoveCallback === 'function') {
                    onRemoveCallback($row);
                    $row.remove();
                } else {
                    $row.remove();
                }
            });

            function onSgTypeChange(selectedType) {
                var stype = g_hashing['stypes'][selectedType] || {};
                var portrange = '';
                if (stype.portfrom === '') {
                    inputs.$range.attr({
                        readonly : false,
                        required : true,
                    });
                    portrange = sgRule.range;
                } else {
                    inputs.$range.attr({
                        readonly : true,
                        required : false,
                    });
                    if (stype.portfrom == -1 && stype.portto == -1) {
                        portrange = 'all';
                    } else if (stype.portfrom == 0 && stype.portto == -1) {
                        portrange = 'n/a';
                    } else if (stype.portfrom == stype.portto) {
                        portrange = stype.portfrom;
                    } else {
                        portrange = stype.portfrom + '-' + stype.portto;
                    }
                }
                inputs.$range.val(portrange);

                if (stype.protocol == '-1') {
                    inputs.$proto.val('').attr({
                        readonly : false,
                        required : true,
                    })
                } else {
                    inputs.$proto.val(stype.alias || stype.protocol).attr({
                        readonly : true,
                        required : false
                    });
                }
            };

            function onSourceChange(source) {
                var selectize = inputs.$target.selectize()[0].selectize;
                switch (source.value) {
                    case 'anywhere':
                    case 'myip':
                        var value = source.target.ipaddr + '/' + source.target.submask;
                        selectize.setValue(value);
                        selectize.disable();
                        break;
                    default:
                        var match = _.find(g_caches['objs'], function(obj) {
                            var target = obj.ipaddr + '/' + obj.submask;
                            return target === sgRule.target;
                        });
                        if (!match && sgRule.target) {
                            selectize.addOption({
                                'text' : sgRule.target,
                                'value' : sgRule.target,
                            });
                        }
                        selectize.setValue(sgRule.target);
                        selectize.enable();
                        break;
                }
            };

            var inputs = {
                $stype : generateSgStypeTemplate(g_caches['stypes'], sgRule, onSgTypeChange),
                $proto :
                    $('<input>').attr({
                        type    : "text",
                        class   : "form-control",
                        name    : "sg_proto_"+sgRule.type+"_"+sgRule.id,
                        // value   : sgRule.proto, // value will be load from stype
                        readonly: true,
                    }),
                $range :
                    $('<input>').attr({
                        type    : "text",
                        class   : "form-control",
                        name    : "sg_range_"+sgRule.type+"_"+sgRule.id,
                        // value   : sgRule.range,  // value will be load from stype
                        readonly: true,
                        portrange: true,
                    }),
                $source : generateIpSourceTemplate(sgRule, onSourceChange),
                $target : generateIpTargetTemplate(g_caches['objs'], sgRule),
                $desc :
                    $('<input>').attr({
                        type  : "text",
                        class : "form-control",
                        name  : "sg_desc_"+sgRule.type+"_"+sgRule.id,
                        value : sgRule.desc,
                    }),
            };

            var elements = {
                $stype  : $('<td class="stype">').append(inputs.$stype),
                $proto  : $('<td class="proto">').append(inputs.$proto),
                $range  : $('<td class="range">').append(inputs.$range),
                $cidr   : $('<td class="cidr">').append(inputs.$source, inputs.$target),
                $desc   : $('<td class="desc">').append(inputs.$desc),
                $action : $('<td class="">').append($btnDelete),
            };

            for (var field in TBL_RULE_HEADERS) {
                if (TBL_RULE_HEADERS[field].invisible) {
                    elements['$'+field].addClass('hide');
                }
            };

            $row.append(
                elements.$stype,
                elements.$proto,
                elements.$range,
                elements.$cidr,
                elements.$desc,
                elements.$action,
            );

            return $row;
        };

        function generateSgRowTemplate(sg, checked) {
            var rowIndex = ++g_index;
            var $row = $('<tr>').attr('data-item-id', sg.id);

            var $chkwapper = $('<div class="form-check d-inline-block">');
            var inputs = {
                $chk :
                $('<input type="radio">')
                    .attr({
                        'id' : 'sg_item_'+sg.id,
                        'name' : 'sg_item_id',
                        'value' : sg.id,
                        'checked' : checked
                    }),
                $id   : $('<span>').text(sg.id),
                $name : $('<span>').text(sg.name),
                $desc : $('<span>').text(sg.desc),
            };
            $chkwapper.append(
                inputs.$chk,
                $('<label>').attr('for', inputs.$chk.attr('id'))
            );
            var elements = {
                $chk  : $('<td class="text-center border">').append($chkwapper),
                $id   : $('<td class="sg_item_id border">').append(inputs.$id),
                $name : $('<td class="sg_item_range border">').append(inputs.$name),
                $desc : $('<td class="sg_item_desc border">').append(inputs.$desc),
            };

            for (var field in TBL_SG_HEADERS) {
                if (TBL_SG_HEADERS[field].invisible) {
                    elements['$'+field].addClass('hide');
                }
            };

            $row.append(
                elements.$chk,
                elements.$id,
                elements.$name,
                elements.$desc,
            );

            return $row;
        };

        function generateTableTemplate(template_headers) {
            var $table = $('<table class="table">');

            var $tr = $('<tr>');
            for (var field in template_headers) {
                var setttings = template_headers[field];
                var $th = $('<th>').addClass(field).text(setttings.text);
                if (setttings.width > 0) {
                    $th.css({
                        'width' : setttings.width,
                    });
                }
                if (setttings.min_width > 0) {
                    $th.css({
                        'min-width' : setttings.min_width,
                    });
                }
                if (setttings.invisible) {
                    $th.addClass('hide');
                }
                $th.appendTo($tr);
            }
            $('<thead>').append($tr).appendTo($table);
            $('<tbody>').appendTo($table);
            return $table;
        };

        function createNewRule(sgRule) {
            var $targetElement = '';
            if (sgRule.type === SG_TYPES.Inbound) {
                $targetElement = $sgInboundBody;
                $btnAddRule.inbound++;
            } else if (sgRule.type === SG_TYPES.Outbound) {
                $targetElement = $sgOutboundBody;
                $btnAddRule.outbound++;
            }
            if (!$targetElement) {
                console.error('invalid security group type');
                return;
            }
            var $row = generateTableRowTemplate(sgRule, function(){
                if (sgRule.type === SG_TYPES.Inbound) {
                    if ($btnAddRule.inbound > 0) {
                        $btnAddRule.inbound--;
                    }
                } else if (sgRule.type === SG_TYPES.Outbound) {
                    if ($btnAddRule.outbound > 0) {
                        $btnAddRule.outbound--;
                    }
                }

                $btnAddRule.checkStatus();
            });
            $targetElement.append($row);
            $row.find('select[name^="sg_stype_"]').selectize({
                onInitialize : function() {
                    this.setValue(sgRule.stype);
                }
            });

            $row.find('select[name^="sg_target_"]').each(function(i, e){
                var label = e.getAttribute('name');
                $(e).attr('id', label).selectize({
                    create: true
                });
            });

            $row.find('select[name^="sg_source_"]').selectize({
                onInitialize : function() {
                    this.setValue(sgRule.source);
                }
            });

            $btnAddRule.checkStatus();
        };

        $btnAddRule.click(function() {
            if (!g_caches['stypes']) {
                return;
            }
            var isDisabled = $btnAddRule.checkStatus();
            if (isDisabled) {
                return;
            }
            createNewRule(new SgRule({
                type  : g_targetType,
                stype : g_caches['stypes'][0].name,
                source: 'custom',
            }));
        });

        $btnNextStep.click(function(e) {
            $(this).focus();

            if (g_targetSg === 'existing') {
                $frmSelectSg.submit();
            } else {
                $frmCreateSg.validate({
                    ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input'
                });
                var valid = $frmCreateSg.valid();
                if (valid) {
                    $frmCreateSg.submit();
                }
            }
        });

        function init() {
            if (g_selectedSgId) {
                g_targetSg = 'existing';
            } else {
                g_targetSg = 'new';
            }

            var r2 = loadSgObjs().then(function(response) {
                if (response && response.length) {
                    g_caches['objs'] = response;
                } else {
                    g_caches['objs'] = [];
                }
                ['ANYWHERE', 'MYIP'].forEach(function(type) {
                    g_caches['objs'].push({
                        ipaddr     : IP_SOURCE_TYPES[type].target.ipaddr,
                        submask    : IP_SOURCE_TYPES[type].target.submask,
                        name       : IP_SOURCE_TYPES[type].text,
                        description: "",
                    });
                });
            });

            var r3 = loadSgs(g_selectedVpc).then(function (response) {
                if (response && response.length) {
                    g_caches['sgs'] = response;
                } else {
                    g_caches['sgs'] = [];
                }
                g_sgCount = g_caches['sgs'].length;
                if (g_sgCount) {
                    g_caches['sgs'].forEach(function(sg) {
                        var $row = generateSgRowTemplate(sg, g_selectedSgId === sg.id);
                        $row.addClass('border')
                        $row.appendTo($sgListBody);
                    });
                    $btnNextStep.removeClass('hide');
                } else {
                    $('<tr>').append(
                        $('<td>')
                            .attr('colspan', 4)
                            .addClass('text-center')
                            .text('データが見つかりません。 VPCによってSecurity Groupの表示を変更しています。')
                    ).appendTo($sgListBody);
                    $btnNextStep.removeClass('hide').hide();
                }
            });

            window.util.ajaxAll([r2, r3]).done(function() {
                g_caches['stypes'] = window.util.parseJSON('{$sgSTypes}') || [];
                g_hashing['stypes'] = {};
                g_caches['stypes'].forEach(function(stype) {
                    g_hashing['stypes'][stype.name] = {
                        protocol : stype.protocol,
                        portfrom : stype.portfrom,
                        portto   : stype.portto,
                        alias    : stype.alias,
                    };
                });

                $btnAddRule.inbound = 0;
                $btnAddRule.outbound = 0;
                var sgRules = window.util.parseJSON('{$sgRules}') || [];
                sgRules.forEach(function(rule) {
                    createNewRule(new SgRule(rule));
                });
                $btnAddRule.checkStatus();
            });
        };
        init();
    });
</script>