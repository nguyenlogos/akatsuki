{if $errMsg != ""}
    <div class="alert {($err == 1) ? 'alert-danger' : 'alert-normal'}">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$errMsg}
    </div>
{/if}
<div class="bs-wizard row">
    <div class="col-md-3 bs-wizard-step complete">
        <div class="text-center bs-wizard-stepnum">インスタンスの設定</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step complete">
        <div class="text-center bs-wizard-stepnum">ストレージの設定</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step complete">
        <div class="text-center bs-wizard-stepnum">セキュリティグループの作成</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step active">
        <div class="text-center bs-wizard-stepnum">最終確認 </div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>
</div>
<div class="block-content">
    <form method="POST" action="{$formActionUrl}" id="orderform">
        <h2 class="">STEP 1: 基本情報</h2>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>グループ</th>
                    <th class="w_250">プロジェクト</th>
                    <th>データセンター</th>
                    <th>コピー元イメージ</th>
                    <th>インスタンスタイプ</th>
                    <th>利用開始日</th>
                    <th>利用終了日</th>
                    <th>シャットダウン時の後処理</th>
                    <th>インスタンス個数</th>
                </tr>
                </thead>
                {if $dataarr}
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>{$dt_dept['deptname']}</td>
                        <td>{$dt_proj['pname']}</td>
                        <td>{$dataarr['azone']}</td>
                        <td>{$dt_ami['name']}</td>
                        <td>{$dataarr['insttype']}</td>
                        <td>{$dataarr['datefrom']}</td>
                        <td>{$dataarr['dateto']}</td>
                        <td>{if $dataarr['shutdownbe'] == "terminate"}利用終了のため消去 {else} 停止したまま保存(再起動可能) {/if}</td>
                        <td>{$dataarr['instnum']}</td>
                    </tr>
                    </tbody>
                {/if}

            </table>
        </div>
        <p><a class="btn w_250 btn-orange" href="{$stepOneUrl}" role="button">基本情報を変更</a></p>

        <h2 class="">STEP 2: ディスクの構成</h2>
        {if $err === 'errDevName'}
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                デバイス名はユニークしないといけないです。
            </div>
        {/if}
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>ﾀｲﾌﾟ</th>
                    <th>ﾃﾞﾊﾞｲｽ名</th>
                    <th>ｺﾋﾟｰ元ｽﾅｯﾌﾟｼｮｯﾄ</th>
                    <th>ｻｲｽﾞ(GiB)</th>
                    <th>ﾃﾞｨｽｸの種類</th>
                    <th width="10%">IOPS</th>
                    <th>自動削除</th>
                </tr>
                </thead>
                <tbody>
                {assign var=j value=1}
                {foreach from=$listDev key=myId item=i}
                    <tr class="seson">
                        <td>{$j}</td>
                        <td>{$i.type}</td>
                        <td class="w_145">
                            {$i.device}
                            <input type="hidden" name="devname{$i.no}" value="{$i.device}">
                        </td>
                        <td>{$i.snapid}</td>
                        <td>
                            {$i.size}
                            <input type="hidden" name="stsize{$i.no}" value="{$i.size}"
                        </td>
                        <td>
                            {local_DeviceType($i.devtype)}
                            <input type="hidden" name="devtype{$i.no}" value="{$i.devtype}">
                        </td>
                        {if $i['devtype']== "io1"}
                            <script>
                                $(document).ready(function () {
                                    $('.target').each(function() {
                                        var value = $(this).find(":selected").val();
                                        if(value === 'io1'){
                                            $(this).parents('.seson').find('input[data-hidden]').removeClass('hidden_iops');
                                        }else{
                                            $(this).parents('.seson').find('input[data-hidden]').addClass('hidden_iops');
                                        }
                                    });
                                });
                            </script>
                        {/if}
                        <td class="w_145">
                            {if $i.iops}{$i.iops}{/if}
                            <input type="hidden" name="iops{$i.no}" value="{$i.iops}">
                        </td>
                        <td class="text-center">
                            {if $i.autodel}<i class="fas fa-check"></i>{/if}
                            <input type="hidden" name="autodel{$i.no}" value="{$i.autodel}">
                        </td>
                    </tr>
                    {assign var=j value=$j+1}
                {/foreach}
                </tbody>
            </table>
        </div>
        <p>{$reture}</p>
        <input type="hidden" name="numberKeyDisks" id="numberKeyDisks" value="" />
        {if $instReqInfo['sg']}
            {assign var="sg" value=$instReqInfo['sg']}
            <h2>STEP 3: Security Group</h2>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>セキュリティグループＩＤ</th>
                        <th>セキュリティグループ名</th>
                        <th>ノート</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{$sg['sg_id']}</td>
                        <td>{$sg['sg_name']}</td>
                        <td>{$sg['sg_desc']}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <p>{$ButtonSG}</p>
        {/if}

        <p class="mt_60">上記の内容で申請を行います。よろしいですか？</p>
        {if $errMsgKey != ""}
            <div class="alert {($err == 1) ? 'alert-danger' : 'alert-normal'}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {$errMsgKey}
            </div>
        {/if}
        <div class="row">
            <div class="col-md-12">
                <button type="button"
                        {if $pair}disabled{/if}
                        class="btn btn-info mr-3 unlock_keypair"
                        data-toggle="modal"
                        data-target="#createKaypair">
                    Create Key Pair
                </button>
                <input type="submit" name="btUPdate" class="btn btn-green mr-3 btn-validate" id="update" value="はい、申請します。" />
                <input type="submit" name="btDelete" class="btn btn-danger" value="申請を中止します。（入力した内容はクリアーされます）" />
            </div>
        </div>
    </form>

</div>
<div class="modal" tabindex="-1" role="dialog" id="createKaypair">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Select an existing key pair or create a new key pair</h3>
            </div>
            <form class="mb-0" method="post" id="Keypair">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Select Key Pair</label>
                        <select class="form-control keypair" name="createKey">
                            <option value="NO_KEYPAIR">Proceed without a key pair</option>
                            <option value="EXISTING_KEYPAIR">Choose an existing key pair</option>
                            <option value="NEW_KEYPAIR">Create a new key pair</option>
                        </select>
                    </div>

                    <div class="form-group hidden_iops" data-select>
                        <label>Select a key pair</label>
                        <select class="form-control" name="key_pair">
                            {foreach $listKey as $value}
                                <option value="{$value}">{$value}</option>
                            {/foreach}
                        </select>
                    </div>

                    <div class="form-group hidden_input" data-input>
                        <label>Key pair name <font color="red">*</font></label>
                        <input type="text" name="name_pair" class="form-control name_key_pair"/>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
                    <button type="submit" name="btn_key_pair" class="btn btn-orange btn-validate">保存</button>
                </div>
            </form>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('input[type="submit"]').click(function(){
            var numberKeyDisks = [];
            $("input[name^='stsize']").each(function(){
                 numberKeyDisks.push($(this).attr('name').split('stsize')[1]);
            });
            $('#numberKeyDisks').val(numberKeyDisks.join(','));
        });
        var ebsEditable = '{$ebsEditable}' + '';
        $('.keypair').on('change', function() {
            var keyval = $(this).val();
            if (keyval === 'EXISTING_KEYPAIR') {
                $('div[data-select]').removeClass('hidden_iops');
            } else {
                $('div[data-select]').addClass('hidden_iops');
            }
            if (keyval === 'NEW_KEYPAIR') {
                $('div[data-input]').removeClass('hidden_input');
            } else {
                $('div[data-input]').addClass('hidden_input');
            }
        });

        $('#Keypair').validate({
            rules: {
                name_pair: {
                    required: true,
                }
            },
            messages: {
                name_pair: "Please enter a name for your key pair",
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        if (!+ebsEditable) {
            $(`
                select[name^="devname"],
                select[name^="devtype"],
                input[name^="autodel"],
                input[name^="enc"],
                input[name^="stsize"],
                input[name^="iops"]
            `).attr('disabled', 'disabled');
            $('.unlock_keypair').addClass('hidden_input');
            $('.unlock_sg').addClass('hidden_input');
        }
    });
</script>