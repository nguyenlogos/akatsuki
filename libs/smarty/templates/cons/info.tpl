<form>
    <div class="form-row">
        <div class="col-md-6 my-1">
            {pulldown4notifystatus id="notification_status" selected=$selectedStatus infoid=$infoid}
        </div>
    </div>
</form>
<div class="info-list">
    {if $msg != "" && $err == 1}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
        {elseif $msg != "" && $err == 0}
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
        {else}
        {if $htmlStr != ""}
            <p><span>未読：<span class="info-count">{$midokuCount}</span>件</span></p>
            <div class="list-group">
                <ul class='list-link no-disc-list'>
                    {$htmlStr}
                </ul>
            </div>
        {else}
            <div class="block-content">
                <span>配信日時：{$senddate}</span>
            </div>
            <div class="block-content">
                {$titleHTML}
            </div>
            <div class="block-content">
                <div class="jumbotron">
                    <span>{$msgBody}</span><br />
                </div>
            </div>
            <div class="block-content mb-4">
                <a class="btn btn-secondary" href="/cons/info.php">お知らせの一覧に戻る</a>
            </div>
        {/if}
    {/if}
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var $modalContainer = $('#messageModal');
        var $modalContent = $modalContainer.find('.modal-body');
        var $infoCount = $('.info-list .info-count');
        var $sidebarInfoCount = $("#sticky-sidebar .info-count");
        function updateUnreadCount(newValue) {
            $infoCount.text(newValue);
            $sidebarInfoCount.text(newValue);
        };
        showMessage = function(contentID, target){
            var $target = $(target);
            var unreadCount = +$infoCount.text();
            $modalContent.load('/cons/info.php?ajax=1&ID='+contentID, function(){
                if ( !$target.hasClass('opened') ) {
                    $target.addClass('opened');
                    updateUnreadCount(--unreadCount);
                }
                if ( this.innerText.trim() !== "" ) {
                    $modalContainer
                        .attr('data-content-id', contentID)
                        .modal({
                            'show' : true
                        });
                    $target.parent().remove();
                }
            });
        };
        //Reload page whien model is close
        $('#messageModal').on('hidden.bs.modal', function () {
            location.reload();
        });
        $('#notification_status').on('change', function(){
            var selectedStatus = this.value;
            var form = $(this).closest('form')[0];
            form.action = "/cons/info.php?status=" + selectedStatus;
            form.method = "post";
            form.submit();
        });
    });
</script>
