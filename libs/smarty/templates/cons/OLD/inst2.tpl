<a href="#modal" id="newspaper"></a>

<div id="modal" style="display: none;" class="modal-content">
    <div class="modal-header">
        <h4>AWS API認証情報の設定</h4>
    </div>
    <div class="modal-body">
    <form method="POST" target="/cons/inst.php">
    本システムでは、Amazon社が提供するAPIを使って機能を実現しています。<br />
    そのため、API接続のための認証情報が必要です。<br />
    以下に認証情報を入力して、[保存]ボタンを押してください。<br /><br />
    <table border="0">
    <tr><td align="right">Access Key Id:</td><td><input type="text" size="64" name="key" /></td></tr>
    <tr><td align="right">Secret Access Key:</td><td><input type="text" size="64" name="secret" /></td></tr>
    <tr><td colspan="2" align="right"><input type="submit" value="保存" /></td></tr>
    </table>
    </form>
     </div>
</div>

<script>
$(function() {
    $('#newspaper').on('click', function ( e ) {
        $.fn.custombox( this, {
            effect: 'newspaper',
	    overlayClose : false,
	    escKey : false
        });
        e.preventDefault();
    });
   $('#newspaper').click();
});
</script>
