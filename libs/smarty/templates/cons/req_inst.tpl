<div class="block-content">
    <p>インスタンス作成の申請を行います。<br />必要な情報を入力して[次のステップ]を選択してください。</p>
</div>
<div class="bs-wizard row">
    <div class="col-md-3 bs-wizard-step active">
        <div class="text-center bs-wizard-stepnum">インスタンスの設定</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step disabled">
        <div class="text-center bs-wizard-stepnum">ストレージの設定</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step disabled">
        <div class="text-center bs-wizard-stepnum">セキュリティグループの作成</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>

    <div class="col-md-3 bs-wizard-step disabled">
        <div class="text-center bs-wizard-stepnum">最終確認 </div>
        <div class="progress"><div class="progress-bar"></div></div>
        <a class="bs-wizard-dot"></a>
    </div>
</div>
<form method="POST" action="{$formActionUrl}" class="instance_sub" name="frm-next-step">
    {if $errMsg != ""}
        <div class="block-errors">
        <p class="text-danger">{$errMsg}</p>
        </div>{/if}
    <div class="form-row">
        <div class="form-group col-md-{($hideDept)?6:4} {$hideDept} my-4 {if $showgroupErr}error_sub{/if}">
            <label>グループ <span class="badge badge-large badge-red">必須</span></label>
            <div data-dropdown-source="dept"
                 data-dropdown-target="#proj"
                 data-selected="{$showgroup}"></div>
            {if $showgroupErr}<label class="error">{$showgroupErr}</label>{/if}
        </div>
        <div class="form-group col-md-{($hideDept)?6:4} my-4 {if $showgprojectErr}error_sub{/if}">
            <label>プロジェクト <span class="badge badge-large badge-red">必須</span></label>
            <div id="proj" data-dropdown-source="proj" data-selected='{$showgproject}'></div>
            {if $showgprojectErr}<label class="error">{$showgprojectErr} が入力されていません。</label>{/if}
        </div>
        <div class="form-group col-md-{($hideDept)?6:4} my-4 {if $azoneErr}error_sub{/if}">
            <label>データセンター <span class="badge badge-large badge-red">必須</span></label>
            {pulldown4azone name=azone selected=$azone}
            {if $azoneErr}<label class="error">{$azoneErr} が入力されていません。</label>{/if}
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-4 my-4 {if $vpcErr}error_sub{/if}">
            <label>ネットワーク <span class="badge badge-large badge-red">必須</span></label>
            {pulldown4vpc name=vpc selected=$selectedVPC}
            {if $vpcErr}<label class="error">{$vpcErr} が入力されていません。</label>{/if}
        </div>
        <div class="form-group col-md-4 my-4 {if $subnetErr}error_sub{/if}">
            <label>サブネット <span class="badge badge-large badge-red">必須</span></label>
            <select name="subnet" class="form-control">
                <option value="">--- サブネットを選択 ---</option>
            </select>
            {if $subnetErr}<label class="error">{$subnetErr} が入力されていません。</label>{/if}
        </div>
        <div class="form-group col-md-4 my-4 {if $addressErrErr}error_sub{/if}">
            <label>Elastic IP <span class="invisible badge badge-large badge-red">必須</span></label>
            <div data-dropdown="address" data-selected="{$selectedAddress}"></div>
            {if $addressErrErr}<label class="error">{$addressErrErr}</label>{/if}
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6 my-4 {if ($amiErr) || ($amiErrSize)}error_sub{/if}">
            <label>AMI <span class="badge badge-large badge-red">必須</span></label>
            <select name="ami" class="form-control">
                <option value="">--- AMIを選択 ---</option>
            </select>
            {if $amiErr}<label class="error">{$amiErr} が入力されていません。</label>{elseif $amiErrSize}<label class="error">{$amiErrSize}</label>{/if}
        </div>
        <div class="form-group col-md-6 my-4 {if $insttypeErr}error_sub{/if}">
            <label>インスタンスタイプ <span class="badge badge-large badge-red">必須</span></label>
            <div id="combo_zone_script"></div>
            <label id="-error" class="error" for=""></label>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12 my-4 {if $addressErrErr}error_sub{/if}">
            <label>用途 <span class="badge badge-large badge-red">必須</span></label>
            <textarea   name="usage" id="usage" class="form-control" rows="3"
                        maxlength="512" placeholder="512文字まで">{$usage}</textarea>
            {if $usageErr}<label class="error">{$usageErr}</label>{/if}
        </div>
    </div>
    <h2 class="mt_60">利用期間(予定)</h2>
    <div class="form-row">
        <div class="form-group col-md-6 my-4 {if $datefromErr}error_sub{/if}">
            <label>利用開始日<span class="badge badge-large badge-red">必須</span></label>
            <div class="input-group input-group-date">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
                <input  type="text" name="datefrom" class="form-control date-input"
                        placeholder="YYYY/MM/DD" value="{$datefrom}" autocomplete="off">
            </div>
            {if $datefromErr}<label class="error">{$datefromErr}</label>{/if}
        </div>
        <div class="form-group col-md-6 my-4 {if $datetoErr}error_sub{/if}">
            <label>利用終了日<span class="badge badge-large badge-red">必須</span></label>
            <div class="input-group input-group-date">
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                </div>
                <input  type="text" name="dateto" class="form-control date-input"
                        placeholder="YYYY/MM/DD" value="{$dateto}" autocomplete="off">
            </div>
            {if $datetoErr}<label class="error">{$datetoErr}</label>{/if}
            {if $total_dateErr}<label class="error">{$total_dateErr}が入力されていません。</label>{/if}
        </div>
    </div>
    {if $check_dateErr}<div style="text-align:left"><label class="error">{$check_dateErr}</div></label>{/if}
    <div class="form-row">
        <div class="form-group col-md-12 my-4">
            <label>シャットダウン時の後処理 <span class="badge badge-large badge-red">必須</span></label>
            {radio4shutdownbe name=shutdownbe selected=$shutdownbe}
            {if $shutdownbeErr}<label class="error">{$shutdownbeErr} が入力されていません。</label>{/if}
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6 my-4 {if $instnumErr}error_sub{/if}">
            <label>インスタンス個数 <span class="badge badge-large badge-red">必須</span></label>
            {input4instnum name=instnum curval=$instnum}
            {if $instnumErr}<label class="error">{$instnumErr} が入力されていません。</label>{/if}
        </div>
    </div>

    <div class="row justify-content-md-center submit-contents">
        <div class="col-md-12 text-center">
            <div class="form-group">
                {if $approvedStatus}
                    <input type="submit" name="step4" class="btn btn-orange btn-next-step" value="最終確認画面">
                {else}
                    <input  type="submit" name="next_step" class="btn btn-blue btn-next-step mr-3"
                    value="次のステップ" disabled>
                {/if}
                {if $showLastStepButton}
                    <input type="submit" name="step4" class="btn btn-orange btn-next-step" value="最終確認画面">
                {/if}
                <input type="hidden" name="getid" class="btn btn-primary" value="{$get_id_ins}">
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    var approvedStatus = '{$approvedStatus}' + '';
    var g_subnet_loaded = false;
    $(document).ready(function(){
        var tempList = [{pulldown4insttype}];
        function showMultiInstList(tempList) {
            //Add select compo
            var myCombo = new dhtmlXCombo("combo_zone_script", "insttype", 200);
            myCombo.setTemplate({
            input: "#name#",
            header: true, // if you want to hide header
            columns: [
                    {literal}{header: "Name", width:  100, option: "#name#"},
                    {header: "vCPU", width:  100, option: "#cpu#"},
                    {header: "Memory", width:  100, option: "#memory#"},
                    {header: "Storage", width:  150, option: "#storage#"},
                    {header: "Price", width:  150, option: "#price#"}{/literal}
                ]
            });
            myCombo.addOption(
                tempList
            );
            myCombo.readonly(true);
            myCombo.setPlaceholder("--- インスタンスタイプを選択 ---");
            myCombo.setSize(300);
        }
        showMultiInstList(tempList);
        function loadAMIList(azone, selectedAMI) {
            var $defaultOption = $('select[name="ami"] > option[value=""]');
            $defaultOption.nextAll().remove();
            $.ajax({
                type:'POST',
                url:'/api/load_ami.php',
                data: {
                    "azone" : azone,
                    "selected_ami" : selectedAMI
                },
                success:function(html){
                    if (html) {
                        $defaultOption.after(html);
                    }
                }
            });
        };

        function loadInstTypeList(azone, selectedInstType) {
            // var $defaultOption = $('.dhxcombo_input');
            $.ajax({
                type:'POST',
                url:'/api/load_insttype.php',
                data: {
                    "azone" : azone,
                    "selected_insttype" : selectedInstType
                },
                dataType : 'json',
                success:function(html){
                    if (html) {
                        $('.dhxcombo_material').remove();
                        showMultiInstList(JSON.parse(html.data));
                        $('.dhxcombo_input').val(html.selected);
                        // $defaultOption.after(html);
                    }
                }
            });
        };

        function loadSubnetList(vpc, selectedSubnet) {
            var $select = $('select[name="subnet"]');
            var $defaultOption = $select.find('option[value=""]');
            $select.prop('disabled', 'disabled');
            $select.prop('readonly', 'readonly');
            $defaultOption.nextAll().remove();
            $.ajax({
                type:'POST',
                url:'/api/load_subnet.php',
                data: {
                    "vpc" : vpc,
                    "selected_subnet" : selectedSubnet
                },
                success:function(html){
                    if ( html ) {
                        $defaultOption.after(html);
                    }
                }
            }).done(function(){
                $select.removeProp('disabled');
                approvedStatus != '1' && $select.removeProp('readonly');
                g_subnet_loaded = true;
                $('.btn-next-step').removeProp('disabled');
            });
        };

        $('.input-group .form-control').datepicker({
            format: 'yyyy/mm/dd',
            todayHighlight: true,
            autoclose: true,
            language: "ja"
        });

        $('select[name="azone"]').on('change', function(){
            loadAMIList(this.value);
            loadInstTypeList(this.value);
        });

        $('select[name="vpc"]').on('change', function(){
            loadSubnetList(this.value);
        });

        $('form[name="frm-next-step"]').on('submit', function(e){
            var $proj = $('[name="proj"]');
            var isDisabled = $proj.is('[disabled]');
            var isReadonly = $proj.is('[readonly]');
            if (isDisabled || isReadonly) {
                e.preventDefault();
            }
        });

        (function(){
            var selectedAZ = '{$azone}' + '';
            var selectedAMI = '{$ami}' + '';
            var selectedInstType = '{$insttype}' + '';
            var selectedVPC = '{$selectedVPC}' + '';
            var selectedSubnet = '{$selectedSubnet}' + '';
            if (selectedAZ) {
                loadAMIList(selectedAZ, selectedAMI);
                loadInstTypeList(selectedAZ, selectedInstType);
            }
            if (selectedVPC) {
                loadSubnetList(selectedVPC, selectedSubnet);
            }
        })();

        (function(){
            function generateIpsTemplate(ips, selected) {
                ips && ips.length || (ips = [])
                var $optionDefault =
                    $('<option>').attr('value', '').text('--- IPを選択 ---');
                var $select = $('<select>')
                    .attr('name', 'address')
                    .addClass('form-control')
                    .append($optionDefault);
                var $optionNew = $('<option>').attr('value', 'new').text('Allocate new address (max 5)');
                $select.append($optionNew);
                if (ips.length >= 5) {
                    $optionNew.prop('disabled', 'disabled');
                }
                ips.forEach(function(ip){
                    var $option =
                        $('<option>')
                            .attr('value', ip.PublicIp)
                            .text(ip.PublicIp)
                    if (selected && selected == ip.PublicIp) {
                        $option.attr('selected', 'selected');
                    }
                    if (ip.AssociationId) {
                        $option.text(ip.PublicIp + ' （中古）').prop('disabled', 'disabled');
                    }
                    $select.append($option);
                });
                return $select;
            };

            var $address_container = $('[data-dropdown="address"]');
            var selectedAddress = $address_container.data('selected');
            generateIpsTemplate().prop('disabled', 'disabled').appendTo($address_container);
            window.util.ajax('/api/eip.php').then(function(ips){
                var $ipsTemplate = generateIpsTemplate(ips, selectedAddress);
                $address_container.empty().append($ipsTemplate);
            });
        })();
    });

    if (approvedStatus == '1') {
        $(`
            select[name="vpc"],
            select[name="subnet"],
            select[name="azone"],
            select[name="ami"],
            select[name="insttype"],
            input[name="instnum"]
        `).attr('readonly', 'readonly')
        .on('mousedown', function(e){
            e.preventDefault();
            this.blur();
            window.focus();
        });
    }
    $('.instance_sub').validate({
        rules: {
            usage: {
                maxlength: 512
            }
        }
    });
</script>
