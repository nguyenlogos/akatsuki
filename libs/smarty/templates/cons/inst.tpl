<script src="/src/assets/js/dhtmlx/dhtmlx.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/src/assets/fonts/font_awesome/css/font-awesome.min.css"/>
<link href="/src/assets/css/dhtmlx/dhtmlx.css" rel="stylesheet" type="text/css" />
<style>
    div#instance_list {
        position: relative;
        width: 100%;
    }
</style>

<!-- BEGIN FILTER AREA-->
<script type="text/javascript">
    var myTreeView;
    var data = {$dataStr},items = data.item;
    function doOnLoad() {
        myTreeView = new dhtmlXTreeView({
            parent: "instance_list",
            iconset: "font_awesome",
            items: items
        });
    }
    function findElement(ele) {
        var _this = ele;
        setTimeout(function() {
            var widthParent = $(_this).parent().next().find('.dhxtreeview_item:first').innerWidth();
            $(_this).parent().next().find('.dhxtreeview_item:first').siblings().andSelf().each(function () {
                var widthItemLabel = $(this).find('.dhxtreeview_item_label:first').innerWidth();
                var right = -(widthParent - widthItemLabel - 90);
                $(this).find('.btn-state:first').css('right', right);
            });
        }, 50);
    }
    function setPositionButtonState() {
        $('.dhxtreeview_area > .dhxtreeview_item').each(function() {
            $(this).find('.dhxtreeview_item_icon').on('click', function() {
                findElement(this);
            });

            $(this).find('.dhxtreeview_item_label').on('dblclick', function() {
                findElement(this);
            });
        });
    }

    $(document).ready(function() {
        $(window).bind("load", function() {
            setPositionButtonState();
        });

        doOnLoad();
        var $activeCountEl = $('.instance-overview .active-count');
        var $inactiveCountEl = $('.instance-overview .inactive-count');
        var activeCount = +$activeCountEl.text(),
            inactiveCount = $inactiveCountEl.text();

        function updateCounter() {
            $activeCountEl.text(activeCount);
            $inactiveCountEl.text(inactiveCount);
        }

        (function(){
            const STATES = {
                'pending': {
                    text: '準備中',
                    icon: 'fa-cog spinner',
                    color: 'text-info',
                    state: 'pending',
                    target: 'running',
                    clickable: false,
                },
                'running': {
                    text: '稼働中',
                    icon: 'fa-play',
                    color: 'text-info',
                    state: 'running',
                    target: 'stopping',
                    clickable: true,
                },
                'stopping': {
                    text: '停止中',
                    icon: 'fa-cog spinner',
                    color: 'text-danger',
                    state: 'stopping',
                    target: 'stopped',
                    clickable: false,
                },
                'stopped': {
                    text: '停止済',
                    icon: 'fa-pause',
                    color: 'text-danger',
                    state: 'stopped',
                    target: 'pending',
                    clickable: true,
                },
                'error': {
                    text: '未定義',
                    icon: 'fa-ban',
                    color: 'text-muted',
                    state: 'error',
                    clickable: false,
                }
                // 'terminated' : {
                //     text: '未定義',
                //     icon: 'fa-ban',
                //     color: 'text-muted',
                //     state: 'terminated',
                //     clickable: false,
                // },
            };

            const REQUEST_INTERVAL = 5000; // miliseconds
            const ERR_UNKNOWN_MSG = "不明なエラーが発生しました。";

            var StateCtrl = function(btnState) {
                function _updateStateAWS(instid, targetState, callback) {
                    var ret = false;
                    window.util.ajaxPost('/api/inst.php?action=ss', {
                        state: targetState,
                        instid: instid,
                    }).then(function(response){
                        ret = response;
                    }).always(function(){
                        if (typeof callback === 'function') {
                            callback(ret);
                        }
                    });
                }

                function _updateState(instid, targetState) {
                    var deferred = $.Deferred();
                    var interval = 50,
                        sec = REQUEST_INTERVAL;
                    var isProcessing = false;
                    var intv = window.setInterval(function(){
                        if (isProcessing) {
                            return;
                        }
                        if (sec < REQUEST_INTERVAL) {
                            sec += interval;
                            return;
                        } else {
                            sec = 0;
                            isProcessing = true;
                        }
                        var errmsg = "";
                        _updateStateAWS(instid, targetState, function(response){
                            isProcessing = false;
                            if (response) {
                                if (response.err || !response.state) {
                                    errmsg = response.msg || ERR_UNKNOWN_MSG;
                                } else {
                                    if (response.state === targetState) {
                                        window.clearInterval(intv);
                                        deferred.resolve();
                                    }
                                }
                            } else {
                                errmsg = response.msg || ERR_UNKNOWN_MSG;
                            }
                            if (errmsg) {
                                window.clearInterval(intv);
                                deferred.reject(errmsg);
                            }
                        });
                    }, interval);

                    return deferred.promise();
                };

                function updateStateClient(targetState, onerror) {
                    var instid = btnState.getInstid();
                    var hasError = false;
                    _updateState(instid, targetState.state).then(function(){
                        btnState.setState(targetState);
                    }, function(msg){
                        toastr.error(msg);
                        hasError = true;
                    }).always(function(){
                        if (hasError && typeof onerror === 'function') {
                            onerror();
                        }
                    });
                }

                return {
                    toggle: function() {
                        var currentState = _.clone(btnState.getState());
                        var changingState = STATES[currentState.target];
                        var targetState = STATES[changingState.target];
                        updateStateClient(targetState, function(){
                            btnState.setState(currentState);
                        });
                        btnState.setState(changingState);
                    },
                    watch: function() {
                        var currentState = _.clone(btnState.getState());
                        if (currentState.clickable) {
                            return;
                        }
                        var targetState = STATES[currentState.target];
                        updateStateClient(targetState, function(){
                            btnState.setState(STATES['error']);
                        });
                    }
                }
            };

            var ButtonState = function(el) {
                var $btn = $(el);
                var _el = {
                    $btn: $btn,
                    $text: $btn.find('span'),
                    $icon: $btn.find('i')
                };

                var state = _el.$btn.data('state');
                var _instid = _el.$btn.data('instid');
                var _currentState = STATES[state];
                function updateDisplay() {
                    var disabled = _currentState.clickable ? false : true;
                    _el.$btn.data('state', _currentState.state).attr('disabled', disabled);
                    _el.$text.attr({
                        'class' : _currentState.color
                    }).text(_currentState.text)
                    _el.$icon.attr({
                        'class' : _currentState.icon + " fas " + _currentState.color
                    });
                };
                return {
                    getState: function() {
                        return _currentState;
                    },
                    getInstid: function() {
                        return _instid
                    },
                    setState: function(state) {
                        _currentState = state;
                        updateDisplay();
                    },
                    init: function() {
                        updateDisplay();
                        StateCtrl(this).watch();
                        _el.$btn.click(() => {
                            var message = _currentState.text === "running" ?
                                "インスタンスを起動します。よろしいですか？" : "インスタンスを停止します。よろしいですか？";
                            window.dialog.confirmDialog(message, {
                            }, (response) => {
                                if (response == 1) {
                                    StateCtrl(this).toggle();
                                    window.dialog.close();
                                }
                            })
                        });
                    }
                }
            };
            $('.btn-state[data-state]').each(function(i, e){
                (new ButtonState(e)).init();
            });
        })();
    });
</script>
<!-- END FILTER AREA-->
<!-- BEGIN WORK STATION -->
{if $msg != ""}
    <div class="alert alert-danger">
        {$msg}
    </div>
{/if}

<form method="POST" action="./inst.php">
    {instDB info=$instInfo hideA="1"}
    <div class="form-wrap">
        <div class="form-row filter-section">
            <div class="form-group col-md-6 my-1 {$hideDept}">
                <div data-dropdown-source="dept"
                    data-dropdown-target="#proj"
                    data-selected="{$frmValues['dept']}"></div>
            </div>
            <div class="form-group col-md-5 my-1">
                <div id="proj" data-dropdown-source="proj" data-selected="{$frmValues['proj']}"></div>
            </div>
            <div class="form-group col-md-1 my-1">
                <button type="submit" name="submit_inst" class="btn btn-orange">検索</button>
            </div>
        </div>
    </div>
    <input type="hidden" name="inst_filter">
</form>

{if $totalCount == 0}
<div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{/if}

<div class="row row-tree">
    <div class="col-md">
        <div id="instance_list" class="instance_tree_view"></div>
    </div>
</div>
<!-- END WORK STATION -->

<script>
    $(document).ready(function(){
        $('a[data-filter]').on('click', function(){
            var filterValue = $(this).attr('data-filter');
            var $form = $(this).closest('form');
            $form.find('input[name="inst_filter"]').val(filterValue);
            $form.submit();
        });
    });
</script>
