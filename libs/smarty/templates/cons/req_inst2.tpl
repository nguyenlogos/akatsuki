<div class="bs-wizard row">
  <div class="col-md-3 bs-wizard-step complete">
    <div class="text-center bs-wizard-stepnum">インスタンスの設定</div>
    <div class="progress"><div class="progress-bar"></div></div>
    <a class="bs-wizard-dot"></a>
  </div>

  <div class="col-md-3 bs-wizard-step active">
    <div class="text-center bs-wizard-stepnum">ストレージの設定</div>
    <div class="progress"><div class="progress-bar"></div></div>
    <a class="bs-wizard-dot"></a>
  </div>

  <div class="col-md-3 bs-wizard-step disabled">
    <div class="text-center bs-wizard-stepnum">セキュリティグループの作成</div>
    <div class="progress"><div class="progress-bar"></div></div>
    <a class="bs-wizard-dot"></a>
  </div>

  <div class="col-md-3 bs-wizard-step disabled">
    <div class="text-center bs-wizard-stepnum">最終確認 </div>
    <div class="progress"><div class="progress-bar"></div></div>
    <a class="bs-wizard-dot"></a>
  </div>
</div>
<form method="POST" action="{$link}" id="orderform">

  {if $errMsg != ""}<p class="text-danger">{$errMsg}</p>{/if}
  {if $err == 'errDevName'}
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
      デバイス名はユニークしないといけないです。
    </div>
  {/if}
  {if $err == 'emptyListDev'}
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
      ストレージのが入力されていません。
    </div>
  {/if}
  <table class="table table-striped table-hover mt-4" id="countstep">
    <thead>
    <tr>
      <th>No.</th>
      <th>ﾀｲﾌﾟ</th>
      <th>デバイス名</th>
      <th>サイズ(GiB) <font color="red">*</font></th>
      <th>デバイスの種類</th>
      <th width="10%">IOPS</th>
      <th>自動削除</th>
      <th class="w_100"></th>
    </tr>
    </thead>
    <tbody>

    {if count($listsseion) == 0}
      {foreach from=$listDev key=myId item=i}
        <tr class="seson">
          <td>{if $listsseion}
              {$i.no}
            {else}
              {$i.no + 1}
            {/if} </td>
          <td>{$i.type}</td>
          <td class="w_145">{pulldown4devicename name=devname counter=($i.no + 1) selected=$i.device stat=$i.stat}</td>
          <td {if $errSize != ""}class="error_sub"{/if}>{input4stsize name=stsize counter=($i.no + 1) curval=$i.size}</td>
          <td>{pulldown4storage name=devtype selected=$i.devtype counter=($i.no + 1)}</td>

          {if $i['devtype']== "io1"}
            <script>
                $(document).ready(function () {
                    $('.target').each(function() {
                        var value = $(this).find(":selected").val();
                        if(value === 'io1'){
                            $(this).parents('.seson').find('input[data-hidden]').removeClass('hidden_iops');
                        }else{
                            $(this).parents('.seson').find('input[data-hidden]').addClass('hidden_iops');
                        }
                    });
                });
            </script>
          {/if}
          <td class="w_145">{input4iops name=iops counter=($i.no + 1) curval=$i.iops}</td>
          <td class="text-center">{input4autodel name=autodel counter=($i.no + 1) curval=$i.autodel}</td>
          <td><input type="submit" class="btn btn-sm btn-danger" value="取消" /></td>
        </tr>
      {/foreach}
    {/if}

    {if count($listsseion) > 0}
      {foreach from=$listDev key=myId item=i}
        <tr class="seson">
          <td>{if $listsseion}
              {$i.no + 1}
            {else}
              {$i.no + 2}
            {/if} </td>
          <td>{$i.type}</td>
          <td class="w_145">{pulldown4devicename name=devname counter=($i.no) selected=$i.device stat=$i.stat}</td>
          <td {if $errSize != ""}class="error_sub"{/if}>{input4stsize name=stsize counter=($i.no) curval=$i.size}
            {if $errSize != ""}<font color="red">{$errSize}</font>{/if}
          </td>
          <td>{pulldown4storage name=devtype selected=$i.devtype counter=($i.no)}</td>

          {if $i['devtype']== "io1"}
            <script>
                $(document).ready(function () {
                    $('.target').each(function() {
                        var value = $(this).find(":selected").val();
                        if(value === 'io1'){
                            $(this).parents('.seson').find('input[data-hidden]').removeClass('hidden_iops');
                        }else{
                            $(this).parents('.seson').find('input[data-hidden]').addClass('hidden_iops');
                        }
                    });
                });
            </script>
          {/if}
          <td class="w_145">{input4iops name=iops counter=($i.no) curval=$i.iops}</td>
          <td class="text-center">{input4autodel name=autodel counter=($i.no) curval=$i.autodel}</td>
          <td><input type="submit" class="btn btn-danger" value="取消" /></td>
        </tr>
      {/foreach}
      {if $addNew == true}
        <tr class="seson">
          <td>{$diskTotalNum+1}</td>
          <td>EBS</td>
          <td class="w_145">{pulldown4devicename name=devname counter=$diskTotalNum}</td>
          <td {if $errSize != ""}class="error_sub"{/if}>{input4stsize name=stsize counter=$diskTotalNum}
            {if $errSize != ""}<font color="red">{$errSize}</font>{/if}
          </td>
          <td>{pulldown4storage name=devtype counter=$diskTotalNum}</td>
          <td class="w_145">{input4iops name=iops counter=$diskTotalNum}</td>
          <td class="text-center">{input4autodel name=autodel counter=$diskTotalNum}</td>
          <td><input type="submit" class="btn btn-danger" value="取消" /></td>
        </tr>
      {/if}
    {/if}

    </tbody>
  </table>

  <div class="row justify-content-md-center submit-contents mt-5">
    <div class="col-md-12 text-center">
      <input type="submit" class="btn btn-success mr-3" id="addrow"  name="add_disk" value="ディスクの追加" />
      <input type="submit" class="btn btn-blue btn-validate mr-3"  name="confim" value="次のステップ" />
      {if $showLastStepButton}
        <input type="submit" name="step4" class="btn btn-orange btn-next-step btn-validate" value="最終確認画面">
      {/if}
      <input type="hidden" name="diskNum" value="{$diskNum|escape}" />
      <input type="hidden" name="addDiskNum" id="grandtotal" value="" />
      <input type="hidden" name="numberKeyDisks" id="numberKeyDisks" value="" />
    </div>
  </div>

</form>

  <script>
    $(document).ready(function () {
      $(".two_stsize").each(function(index, el) {
          if ($(this).val() != '' && $(this).val() < {$amiSize}) {
            $(this).addClass("error");
            $(this).after('<label class="error">EBS is smaller than AMI</label>');
          }
      });
      $.validator.addMethod('compare_size', function(value) {
          if (value < {$amiSize}) {
              return false;
          }
          return true;
      });
      $.validator.addClassRules({
          two_stsize: {
              compare_size: true
          }
      });
      function forceNumeric(){
        var $input = $(this);
        $input.val($input.val().replace(/[^\d]+/g,''));
      }
      $('body').on('propertychange input', 'input[type="number"]', forceNumeric);

      var rowCount = $('#countstep .seson').length;
      $('#grandtotal').val(rowCount);

      $('input[type="submit"]').click(function(e){
        $(this).closest('.seson').remove();
        //update class seson
        $('.seson').each(function(i, value) {
          $(this).children('td:first').text(i + 1);
        });
        var rowSeason = $('#countstep .seson');
        var rowCount = rowSeason.length;
        $('#grandtotal').val(rowCount);
        var numberKeyDisks = [];
        $("input[name^='stsize']").each(function(){
             numberKeyDisks.push($(this).attr('name').split('stsize')[1]);
        });
        $('#numberKeyDisks').val(numberKeyDisks.join(','));
      });
    });
  </script>
