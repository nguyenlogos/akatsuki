<link rel="stylesheet" type="text/css" href="/src/assets/fonts/font_awesome/css/font-awesome.min.css"/>
<link href="/src/assets/css/dhtmlx/dhtmlx.css" rel="stylesheet" type="text/css" />
<script src="/src/assets/js/dhtmlx/dhtmlx.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        var g_index = 1;
        var id_prefix = 'c';
        var g_selectedItemLeft = {};
        var g_selectedItemRight = {};
        var g_selectedProj = undefined;
        var g_idMaps = {};
        var g_listChanges = [];
        var g_listDevices = _.map('defghijklmnopqrstuvwxyz'.split(''), function(device){
            return '/dev/sd' + device;
        });
        var textV = "Volume";
        var textS = "Security Group";
        var textE = "Elastic Ips";
        var g_iconsMapping = {
            'Volume' : {
                'file' : 'fa-hdd-o',
                'folder_opened' : 'fa-hdd-o',
                'folder_closed' : 'fa-hdd-o'
            },
            'Security Group' : {
                'file' : 'fa-shield',
                'folder_opened' : 'fa-shield',
                'folder_closed' : 'fa-shield'
            },
            'Elastic Ips' : {
                'file' : 'fa-share-alt',
                'folder_opened' : 'fa-share-alt',
                'folder_closed' : 'fa-share-alt'
            }
        };
        var g_emptyListTemplate = {
            id:0,
            items:[]
        };
        var myTree1, myTree2;
        var g_list1, g_list2;
        function getSelectedItem($tree, id) {
            var textSubItem = $tree.getItemText(id);
            var subItemIds = $tree.getSubItems(id);

            var items = [];
            subItemIds.forEach(function(subItemId){
                item = getSelectedItem($tree, subItemId);
                items.push(item);
            });

            return {
                id : id,
                items : items,
                text : textSubItem
            }
        }

        function getItemByLevel($tree, id, level) {
            var currentLevel = +$tree.getLevel(id);
            if (currentLevel == level) {
                return getSelectedItem($tree, id);
            }
            if (currentLevel < level) {
                return getItemByLevel($tree, id, 1);
            }
            var parentId = $tree.getParentId(id);

            return getItemByLevel($tree, parentId, level);
        }

        function findItemByType(list, type) {
            var items = list.items;
            for (var i = 0, length = items.length; i < length; ++i) {
                var item = items[i];
                if (item.text == type) {
                    return item;
                }
            }
        }

        function renderLayout(list1, list2) {
            if (list1) {
                myTree1 = new dhtmlXTreeView({
                    parent: "treeboxbox_tree",
                    iconset: "font_awesome",
                    items: list1.items
                });
                myTree1.attachEvent("onSelect", function(id, mode){
                    if(mode === true) {
                        g_selectedItemLeft = getSelectedItem(myTree1, id);
                    }
                });
            }

            if (list2) {
                g_listChanges = [];
                myTree2 = new dhtmlXTreeView({
                    parent: "treeboxbox_tree2",
                    iconset: "font_awesome",
                    items: list2.items
                });
                myTree2.attachEvent("onSelect", function(id, mode){
                    if(mode === true) {
                        g_selectedItemRight = getSelectedItem(myTree2, id);
                    }
                });
            }
        }

        $('#addNode').on('click', function () {
            var tree1 = myTree1;
            var tree2 = myTree2;

            var f1ID = g_selectedItemLeft.id || undefined;
            var f2ID = g_selectedItemRight.id || undefined;

            if (f1ID && f2ID) {
                var currentLeftLevel = tree1.getLevel(f1ID);
                var currentRightLevel = tree2.getLevel(f2ID);
                if (currentLeftLevel == 1) {
                    // move instance
                    var targetRightItem = getItemByLevel(tree2, f2ID, 1);
                    f2ID = targetRightItem.id;
                    var selectItemText = g_selectedItemLeft.text;
                    var isDuplicate = false;

                    var tree2SubItemIds = tree2.getSubItems(f2ID);
                    for (var i = 0, length = tree2SubItemIds.length; i < length; i++) {
                        var itemId = tree2SubItemIds[i];
                        var itemText = tree2.getItemText(itemId);
                        if (itemText == selectItemText) {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (isDuplicate) {
                        return;
                    }
                    // var idNewFolder = id_prefix+ ++g_index;
                    var idNewFolder = g_selectedItemLeft.id;
                    var nameNewFolder = g_selectedItemLeft.text;

                    tree2.addItem(idNewFolder, nameNewFolder, f2ID);
                    tree2.setItemIcons(idNewFolder, {
                        file:  "fa-notebook-cs",
                        folder_opened:  "fa-notebook-cs",
                        folder_closed:  "fa-notebook-cs",
                    });

                    for (var i = 0; i < g_selectedItemLeft.items.length; i++){
                        var idSubItemF2 =  id_prefix+ ++g_index;
                        var nameSubItemF2 = g_selectedItemLeft.items[i].text;
                        var listSubItemF3 = g_selectedItemLeft.items[i].items;

                        tree2.addItem(idSubItemF2, nameSubItemF2, idNewFolder);

                        tree2.setItemIcons(idSubItemF2, {
                            file: g_iconsMapping[nameSubItemF2].file,
                            folder_opened: g_iconsMapping[nameSubItemF2].folder_opened,
                            folder_closed:  g_iconsMapping[nameSubItemF2].folder_closed
                        });

                        listSubItemF3.forEach(function(item){
                            tree2.addItem(item.id, item.text, idSubItemF2);
                        });
                    }
                    var instItem = getItemByLevel(tree1, f1ID, 1);
                    g_listChanges = _.unionWith(g_listChanges, [instItem.id]);
                    tree1.deleteItem(f1ID);
                } else if (g_selectedItemLeft.items.length == 0) {
                    var selectedLeftParentId = tree1.getParentId(f1ID);
                    var selectedLeftParentText = tree1.getItemText(selectedLeftParentId);

                    var sourceInstItem = getItemByLevel(tree1, f1ID, 1);
                    var targetInstItem = getItemByLevel(tree2, f2ID, 2);

                    var sourceInstZone = sourceInstItem.text.replace(/.*\(([^)]+)\)$/, '$1');
                    var targetInstZone = targetInstItem.text.replace(/.*\(([^)]+)\)$/, '$1');
                    if (sourceInstZone !== targetInstZone) {
                        toastr.error('移動元と移動先が異なるデータセンターの場合、移動できません。');
                        return;
                    }
                    var targetItem = findItemByType(targetInstItem, selectedLeftParentText);
                    if (!targetItem || targetItem.text === 'Security Group') {
                        return;
                    }

                    // check duplicate
                    var selectItemText = g_selectedItemLeft.text;
                    var isDuplicate = false;

                    var tree2SubItemIds = tree2.getSubItems(targetItem.id);
                    for (var i = 0, length = tree2SubItemIds.length; i < length; i++) {
                        var itemId = tree2SubItemIds[i];
                        if (itemId == f1ID) {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (isDuplicate) {
                        return;
                    }

                    var newItemId = f1ID+ '|' + ++g_index;
                    var newItemText = '';
                    if (selectedLeftParentText == textV) {
                        var deviceInfo = g_selectedItemLeft.text.split(' ');
                        var deviceName = deviceInfo.shift().trim();
                        var usedDevices = _.map(targetItem.items, function(item){
                            var device = item.text.split(' ')[0].trim();
                            return device;
                        })
                        if (deviceName == '/dev/sdc' || usedDevices.indexOf(deviceName) > -1) {
                            var availableDevices = _.differenceWith(g_listDevices, usedDevices, _.isEqual);
                            deviceName = availableDevices[0];
                        }
                        deviceInfo.unshift(deviceName);
                        newItemText = deviceInfo.join(' ');
                    } else {
                        newItemText = g_selectedItemLeft.text;
                    }

                    tree2.addItem(newItemId, newItemText, targetItem.id);
                    tree2.setIconColor(newItemId, '#39c');
                    g_idMaps[newItemId] = {
                        text : selectItemText,
                        parentId : tree1.getParentId(f1ID),
                    };
                    g_selectedItemLeft = {};
                    g_listChanges = _.unionWith(g_listChanges, [targetInstItem.id]);
                    tree1.deleteItem(f1ID);
                }
            }
        });

        $('#removeNode').on('click', function(){
            if (g_selectedItemRight.id) {
                var selectedItemId = g_selectedItemRight.id;
                var currentRightLevel = myTree2.getLevel(selectedItemId);
                if (currentRightLevel === 1 || currentRightLevel === 3) {
                    return;
                }
                if (currentRightLevel == 2) {
                    myTree2.deleteItem(selectedItemId);
                } else {
                    var targetItem = getItemByLevel(myTree2, selectedItemId, 3);
                    if (targetItem.text == textS) {
                        return;
                    }
                    if (g_idMaps[selectedItemId]) {
                        var parentId = g_idMaps[selectedItemId]['parentId'];
                        try {
                            var parentText = myTree1.getItemText(parentId);
                        } catch (err) {}
                        finally {
                            if (parentText) {
                                var oldText = g_idMaps[selectedItemId]['text'];
                                var oldItemId = selectedItemId.split('|')[0];
                                myTree1.addItem(oldItemId, oldText, parentId);
                            }
                            delete g_idMaps[selectedItemId];
                        }
                    } else {
                        var instItem = getItemByLevel(myTree2, selectedItemId, 2);
                        g_listChanges = _.unionWith(g_listChanges, [instItem.id]);
                    }
                    myTree2.deleteItem(selectedItemId);
                }
                g_selectedItemRight = {};
            }
        });

        $('#btnSave').on('click', function(){
            if (!myTree2) {
                return;
            }
            var data = {};
            var items = getItemByLevel(myTree2, g_selectedProj, 2);
            items.items.forEach(function(instItem){
                var instid = instItem.id;
                if (g_listChanges.indexOf(instid) === -1) {
                    return;
                }
                var volumesItem = findItemByType(instItem, textV) || {};
                var ipsItem = findItemByType(instItem, textE) || {};
                data[instid] = {
                    'ebs' : volumesItem.items,
                    'ips' : ipsItem.items,
                };
            });

            var $this = $(this);
            $this.attr('disabled', 'disabled').html('<i class="fas fa-cog fa-spin"></i>');
            var errmsg = '不明なエラーが発生しました。';

            window.util.ajaxPost('/cons/user_inst.php?ajax=add', {
                data : JSON.stringify({
                    'pcode' : g_selectedProj,
                    'listNew' : data,
                })
            }).then(function(response){
                if (response) {
                    if (!response.err) {
                        errmsg = '';
                    } else if (response.msg) {
                        errmsg = response.msg;
                    }
                }
            }).always(function(){
                if (errmsg) {
                    toastr.error(errmsg);
                } else {
                    toastr.success('更新に成功しました。');
                }
                $this.removeProp('disabled').html('<i class="fas fa-floppy-o"></i>');
            })
        });

        window.util.ajax('/api/proj.php?all=1').then(function(projs){
            var proj = {
                pcode: "未設定",
                pname: "未設定",
            };
            projs = [proj].concat(projs);
            $('[data-dropdown-source="proj"]').each(function(i, el){
                var name = $(el).data('name');
                var ajax = name === 'proj_src' ? '1' : '2';
                var $projsTemplate = window.window.generateProjsTemplate(projs, {
                    'name' : name,
                    'onChange' : function(){
                        var selectedValue = this.value;
                        if (ajax === '2') {
                            g_selectedProj = selectedValue;
                        }
                        var $this = $(this);
                        $this.prop('disabled', 'disabled');
                        window.util.ajaxPost('/cons/user_inst.php?ajax='+ajax, {
                            pcodeID: selectedValue
                        }).then(function (response) {
                            var data = undefined;
                            if (response.result) {
                                try {
                                    eval(response.result);
                                } catch(err) {
                                }
                            }
                            if (typeof data === 'undefined') {
                                data = g_emptyListTemplate;
                            }
                            if (ajax == '1') {
                                renderLayout(data);
                            } else {
                                renderLayout(undefined, data);
                            }
                        }).always(function () {
                            $this.removeProp('disabled');
                        })
                    }
                });
                $(el).append($projsTemplate);
            })
        });
    });

</script>

<div id="message">
    {if $errmsg != ''}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    {/if}
    {if $msg != ''}
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    {/if}
</div>
<div class="block-content">
    <p>プロジェクト間でリソースを移動することができます。<br />移動元、移動先のプロジェクトやインスタンスを選択して[移動]ボタンを押して移動してください。<br />最後に[変更を保存]ボタンを押してください。</p>
</div>
<table class="table-wrap-tree">
    <colgroup>
        <col width="350">
        <col width="auto">
        <col width="350">
    </colgroup>
    <tr>
        <td colspan="3">
            <input type="hidden" id="itemAdd">
            <input type="hidden" id="placeAdd">
            <input type="hidden" id="itemDelete">
            <input type="hidden" id="listNew">
            <input type="hidden" id="infoAddFolder">
        </td>
    </tr>
    <tr>
        <td>
            <h2>移動元</h2>
            <div class="form-group">
                プロジェクト:
                <div data-dropdown-source="proj"
                    data-selected='{$showgproject}'
                    data-name="proj_src">
                </div>
            </div>
            プロジェクトのリソース:
            <div id="treeboxbox_tree" class="tree_inst"></div>
        </td>
        <td align='center'>
            <p><button type="button" id="addNode" class="btn btn-green">移動 <i class="fas fa-angle-double-right"></i></button></p>
            <p><button type="button" id="removeNode" class="btn btn-danger"><i class="fa fa-trash-o"> 削除</i></button></p>
        </td>
        <td valign="top">
            <h2>移動先</h2>
            <div class="form-group">
                プロジェクト:
                <div data-dropdown-source="proj"
                    data-selected='{$showgproject}'
                    data-name="proj_dst">
                </div>
            </div>
            プロジェクトのリソース:
            <div id="treeboxbox_tree2" class="tree_demo_samples"></div>
        </td>
    </tr>
</table>
<p class="text-center">
    <button type="button" id="btnSave" class="btn btn-blue"><i class="fas fa-floppy-o"> 変更を保存</i></button>
</p>
