<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">×</button>
  {$msg}<br />
  <br />
  ログインし直して再度試してみてください。<br />
  それでも解決しない場合は、弊社サポート窓口までお問合せください。<br />
</div>

