<div id="message">
{if $errmsg != ''}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <span>{$errmsg}</span>
    </div>
{/if}
{if $msg != ''}
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <span>{$msg}</span>
    </div>
{/if}
</div>
<div class="block-content">
    <p>申請内容を確認して[承認]または[却下]を選択してください。</p>
</div>
<div class="col-xs-12">
<div class="clearfix"></div>
<form method="get">
    <input type="hidden" name="sk" value="1">
    <div class="form-row">
        <div class="form-group col-md-6 my-1">
            <select class="form-control" name="f_type">
                <option value="ec2" {($frmValues['f_type'] == "ec2") ? "selected" : ""}>インスタンスの申請</option>
                <option value="ebs" {($frmValues['f_type'] == "ebs") ? "selected" : ""}>ディスクの申請</option>
            </select>
        </div>
        <div class="form-group col-md-3 my-1">
            <button type="submit" class="btn btn-orange btn-filter">表示</button>
        </div>
        <div class="form-group col-md-3 my-1 text-right">
            <div class="form-check d-inline-block">
                <input class="form-check-input" type="checkbox" name="f_approved" {($frmValues['f_approved'] > 0) ? "checked" : ""}>
                <label class="form-check-label">承認済</label>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="table-responsive">
    {tablelist headers=$headerList data=$reqList actions=$actionList}
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
{if count($reqList) == 0}
    <div class="alert alert-normal">
        申請はありません。<br />
    </div>
{/if}
<input type="hidden" name="sk" value="{$sortIndex}">
<script type="text/javascript">
    $(document).ready(function(){
        dialog.on('closed', function(){
            dialog.setState();
        });
        $('[name="f_approved"]').change(function(){
            $(this).closest('form').submit();
        });
    });
</script>
<script type="text/javascript">
    (function(){
        var g_loading = false;
        var isAdmin = +('{$smarty.session.sysadmin}' + '');
        var isManager = +('{$smarty.session.admin}' + '');
        var g_current_role = (isAdmin || isManager ) ? 'role_manager' : 'role_owner';
        var ACTION_ROLES = {
            '1' : {
                'role_manager' : ['ebs_approve_and_reject'],
                'role_owner' : ['ebs_view_and_detele'],
            },
            '0' : {
                'role_manager' : ['ebs_approve_and_reject'],
                'role_owner' : ['ebs_view_and_detele'],
            }
        };
        var ACTION_BUTTONS = {
            'ebs_approve' : {
                'icon' : '<i class="fas fa-play-circle"></i>&nbsp;承認',
            },
            'ebs_reject' : {
                'icon' : '<i class="fas fa-window-restore"></i>&nbsp;却下',
            },
            'ebs_view' : {
                'icon' : '<i class="fas fa-eye"></i>&nbsp;情報',
            },
            'ebs_delete' : {
                'icon' : '<i class="fas fa-trash"></i>&nbsp;削除',
            },
            'ebs_approve_and_reject' : {
                'icon' : '表示（承認・却下）',
            },
            'ebs_view_and_detele' : {
                'icon' : '表示（承認・却下)',
            }
        };

        var TEMPLATES = {
            'btn_cancel': {
                'button': '<button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-secondary btn-cancel">キャンセル</button>',
                'callback': 'dialog.close'
            },
        };

        var DIALOG_TITLES = {
            'ebs_view'    : 'EBSの情報',
            'ebs_approve' : 'EBS作成の承認',
            'ebs_delete'  : 'EBSの削除',
            'ebs_reject'  : 'EBS作成の却下',
        };

        var FORMS = {
            'ebs_approve' : 'frmEbsApprove',
            'ebs_reject'  : 'frmEbsReject',
            'ebs_delete'  : 'frmEbsDelete',
        }
        function generateTableTemplate(requestInfo) {
            var info = requestInfo.info || {};
            var headerClass = 'col-xs-3';
            var contentClass = 'col-xs-9';
            var htmlStr =  `
                <table class="table table-bordered table-striped" style="margin-bottom: 15px;">
                    <tr>
                        <td class="`+headerClass+`">申請者</td>
                        <td class="`+contentClass+`">`+info.empname+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">インスタンス名</td>
                        <td class="`+contentClass+`">`+info.instance_name+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">インスタンスID</td>
                        <td class="`+contentClass+`">`+info.instance_id+`</td>
                    </tr>
                    <tr>
                        <td class="`+headerClass+`">プロジェクトID</td>
                        <td class="`+contentClass+`">`+info.pcode+`</td>
                    </tr>
                </table>
                @bstable
            `;
            var bsStr = `
                <table class="table table-bordered table-striped" style="margin-bottom: 0;">
                    <tr>
                        <th>デバイスID</th>
                        <th>デバイス名</th>
                        <th>サイズ(GiB)</th>
                        <th>デバイスの種類</th>
                        <th>IOPS</th>
                        <th>状態</th>
                        <th>変更の内容</th>
                    </tr>
                    @nodes
                </table>
            `;
            var devices = requestInfo.devices;
            var nodes = [];
            devices.forEach(function(volume){
                var deviceText = volume.device,
                    sizeText = volume.size,
                    iopsText = volume.type == 'io1' ? volume.iops : 'Auto';
                if (volume.device_new && volume.device_new != volume.device) {
                    deviceText = volume.device + ' ⇒ ' + volume.device_new;
                }
                if (volume.size_new && volume.size_new != volume.size) {
                    sizeText = volume.size + ' ⇒ ' + volume.size_new;
                }
                volume.icons = "";
                if (volume.status === 'new' || volume.status === 'attach') {
                    volume.icons = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
                } else if (volume.status === 'detach') {
                    volume.icons = '<i class="fa fa-minus-square" aria-hidden="true"></i>';
                } else if (volume.status === 'change') {
                    volume.icons = '<i class="fa fa-pencil-square" aria-hidden="true"></i>';
                }
                var node = `
                    <tr>
                        <td>`+volume.id+`</td>
                        <td>`+deviceText+`</td>
                        <td>`+sizeText+`</td>
                        <td>`+volume.type+`</td>
                        <td>`+iopsText+`</td>
                        <td>`+volume.state+`</td>
                        <td class="text-center">`+volume.icons+`</td>
                    </tr>
                `;
                nodes.push(node);
            });
            if (nodes.length === 0) {
                nodes.push('<tr><td colspan="7" class="text-center">データなし</tr>');
            }
            bsStr = bsStr.replace(/@nodes/, nodes.join(''));
            htmlStr = htmlStr.replace(/@bstable/, bsStr);
            return htmlStr;
        };

        function generateRequestInfoLayout($row) {
            var deffered = $.Deferred();
            var instid = $row.find('[data-item-instid]').data('itemInstid');
            var reqid = $row.find('[data-item-id]').data('itemId');
            window.util.ajax('/api/ebs.php', {
                reqid: reqid,
                action: 'info',
            }).then(function(response){
                if (!response || response.err || !response.data) {
                    deffered.reject();
                    return;
                }
                var requestInfo = response.data;
                var isOwner = ('{$smarty.session.empid}' + '') == requestInfo.empid;
                var params = {
                    reqid: reqid,
                    action: 'change'
                };

                window.util.ajax('/api/ebs.php', params).then(function(response){
                    if (response && response.data) {
                        var data = response.data;
                        var list = _.uniqBy(data.list.concat(data.list_change), 'id');
                        list.forEach(function(item){
                            if (!item.id.match(/^vol-/)) {
                                item.id = '';
                                item.status = 'new';
                            } else {
                                var volume = _.find(data.list_change, ['id', item.id]);
                                if (volume) {
                                    item.size_new = +volume.size;
                                    item.device_new = volume.device;
                                    if ((item.size != item.size_new) || (item.attached && item.device != item.device_new)) {
                                        item.status = 'change';
                                    } else {
                                        item.status = 'attach';
                                    }
                                } else {
                                    item.status = 'unchange';
                                }
                            }
                        });
                        // TODO: check if detach list exist in volumes
                        var list_detach = data.list_detach.map(function(item){
                            item.status = 'detach';
                            return item;
                        });
                        list_detach = _.uniqBy(list_detach, 'id');
                        list = _.differenceWith(list, list_detach, function(a,b){
                            return a.id == b.id;
                        });

                        var contents = generateTableTemplate({
                            info : requestInfo,
                            devices: list.concat(list_detach)
                        });
                        deffered.resolve({
                            contents : contents,
                            isOwner : isOwner,
                            isEditable: isOwner && requestInfo.status == 0
                        });
                    } else{
                        deffered.reject();
                    }
                }).fail(function(){
                    deffered.reject();
                });
            }).fail(function(){
                deffered.reject();
            });

            return deffered.promise();
        };

        function setIcon($btn, reqState, position) {
            if (!ACTION_ROLES[reqState]) {
                console.error('Invalid State');
            }
            var actions = ACTION_ROLES[reqState][g_current_role];
            var icon = ACTION_BUTTONS[actions[position]]['icon'];
            $btn.html(icon);
            return actions[position];
        }

        $('td > .btn-approve').each(function(i, btn){
            var $btn = $(btn);
            var $tr = $(this).closest('tr');
            var reqid = $tr.find('[data-item-id]').data('itemId');
            var reqState = +$tr.find('[data-item-status]').data('itemStatus');
            var action = setIcon($btn, reqState, 0);
            $btn.click(function(){
                if (g_loading) {
                    return;
                }
                g_loading = true;
                $btn.attr('disabled', 'disabled');
                generateRequestInfoLayout($tr).then(function(response){
                    var contents = response.contents,
                        isOwner = response.isOwner,
                        isEditable = response.isEditable
                    var frmName, frmAction;
                    var htmlStr = $(`
                            <form name="`+FORMS['ebs_delete']+`" method="post" action="/cons/request.php?f_type=ebs" class="hide">
                                <input type="hidden" name="reqid" value="`+reqid+`">
                                <input type="hidden" name="action" value="ebs_delete">
                                <input type="submit" id="`+FORMS['ebs_delete']+`" class="hide" />
                            </form>
                            <form name="`+FORMS['ebs_approve']+`" method="post" action="/cons/request.php?f_type=ebs" class="hide">
                                <input type="hidden" name="reqid" value="`+reqid+`">
                                <input type="hidden" name="action" value="ebs_approve">
                                <input type="submit" id="`+FORMS['ebs_approve']+`" class="hide" />
                            </form>
                            <form name="`+FORMS['ebs_reject']+`" method="post" action="/cons/request.php?f_type=ebs" class="hide">
                                <input type="hidden" name="reqid" value="`+reqid+`">
                                <input type="hidden" name="action" value="ebs_reject">
                                <input type="submit" id="`+FORMS['ebs_reject']+`" class="hide" />
                            </form>
                            `+contents+`
                    `);
                    var title = DIALOG_TITLES[action];
                    var actionBtn = {};
                    var buttons = [
                        TEMPLATES.btn_cancel
                    ];
                    var pcode  = $tr.find('[data-item-pcode]').data('itemPcode');
                    var instid = $tr.find('[data-item-instid]').data('itemInstid');
                    if (isEditable) {
                        buttons.push({
                            'button': '<button type="button" class="btn btn-blue btn-edit"><i class="fas fa-edit"></i>&nbsp;変更</button>',
                            'callback': function () {
                                var editUrl = '/admin/ebs.php?proj='+pcode+'&inst='+instid+'&init=1';
                                window.location.href = editUrl;
                                return false;
                            }
                        });
                    }
                    if (action === 'ebs_view_and_detele' && isEditable) {
                        buttons.push({
                            'button' :`
                                <label for="`+FORMS['ebs_delete']+`" type="button" class="btn btn-danger mb-0">
                                    `+ACTION_BUTTONS['ebs_delete']['icon']+`
                                </button>
                            `
                        });
                    } else if (action === 'ebs_approve_and_reject' && isEditable) {
                        buttons.push({
                            'button' :`
                                <label for="`+FORMS['ebs_reject']+`" class="btn btn-danger mb-0">
                                    `+ACTION_BUTTONS['ebs_reject']['icon']+`
                                </button>
                            `
                        });

                        buttons.push({
                            'button' : `
                                <button type="button" class="btn btn-orange btn-approve">
                                    `+ACTION_BUTTONS['ebs_approve']['icon']+`
                                </button>`,
                            'callback' : function(){
                                var $this = $(this);
                                var html = $this.html();
                                $this.prop('disabled', 'disabled').html('<i class="fas fa-redo spinner"></i>&nbsp;承認')
                                dialog.setState('no-close');

                                var errmsg =  '不明なエラーが発生しました。';
                                window.util.ajaxPost('/api/ebs.php?action=approve', {
                                    reqid : reqid
                                }).then(function(response){
                                    if (response) {
                                        if (response.err == false) {
                                            errmsg = '';
                                        } else if (response.msg) {
                                            errmsg = response.msg;
                                        }
                                    }
                                }).always(function(){
                                    if (!errmsg) {
                                        $('form[name="'+FORMS['ebs_approve']+'"]').submit();
                                    } else {
                                        toastr.error(errmsg);
                                        dialog.setState();
                                        $this.removeProp('disabled').html(html);
                                    }
                                });
                            }
                        });
                    }
                    dialog.confirmDialog(htmlStr, {
                        'title' : title,
                        'modalClass' : 'modal-lg',
                        'buttons' : buttons
                    });

                }).always(function(){
                    g_loading = false;
                    $btn.removeAttr('disabled');
                });
            });
        });
    })();
</script>
