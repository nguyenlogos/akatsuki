<script type="text/javascript">
    $(function() {
        $('#deleteData').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var recipient1 = button.data('dataid');
            var recipient2 = button.data('name');
            var modal = $(this);

            modal.find('.uid').text(recipient1);
            modal.find('.uid2').val(recipient1);
            modal.find('.userName').text(recipient2);
        });

        $('input.check_lock').change(function() {
            var $this = $(this);
            var isChecked = $this.is(':checked');

            var check_id = $(this).attr('data-lockid');
            var check_email = $(this).attr('data-email');
            var check_val = $(this).val();
            if (isChecked) {
                var confirm1 = confirm('Update lock email');
            } else {
                var confirm1 = confirm('Update unlock email');
            }

            if (confirm1 == true) {
                $.ajax({
                    'url' : '/mst/user.php?lock=1',
                    'type' : 'post',
                    'data' : {
                        'check_id' : check_id,
                        'check_val' : check_val,
                        'check_email' : check_email,
                    },
                    'success' : function(response){
                        var jsonData;
                        try {
                            jsonData = JSON.parse(response);
                        } catch (err) {}
                        finally {
                            if ( jsonData && jsonData.result ) {
                                toastr.success("更新に成功しました。");
                            }
                            else {
                                toastr.error("失敗しました。");
                            }
                        }
                    },
                    'error' : function(){
                        toastr.error("失敗しました。");
                    }
                });
            } else {
                if (isChecked) {
                    $this.removeProp('checked');
                } else {
                    $this.prop('checked', 'checked');
                }
            }
        });
    });
</script>

{if $msg != ""}{/if}

<div id="result"></div>

<div class="row">
    <div class="col-md-8">
        <p>ユーザーを新規登録する場合は[新規登録]ボタンを押してください。</p>
    </div>
    <div class="col-md-4 text-right">
        <a href="./user2.php" class="btn btn-green" role="button"><i class="fas fa-plus-circle fa-fw"></i> 新規登録</a>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md">
        {if $msg != "" && $err == 1}
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {$msg}
            </div>
        {elseif $msg != "" && $err == 0}
            <div class="alert alert-normal">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {$msg}
            </div>
        {/if}
        {if $managerCount > 0}
            <div class="alert alert-normal">
                <span class="badge badge-warning">!</span> マネージャーが存在していないグループがあります。
                <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
        {/if}
    </div>
</div>
<h2 class="mt-5">{$dataCount}件、登録されています。</h2>
<form method="get">
    <div class="form-row">
        {if $hide_dept == '0'}
            <div class="form-group col-md-6 my-1">
                {pulldown4dept name=showdept selected=$show_dept onchange=true}
            </div>
        {/if}
        <div class="form-group col-md-5 my-1">
            <input type="text" name="user_name" class="form-control" placeholder="ユーザー名" value="{$user_name}" />
        </div>
        <div class="form-group col-md-1 my-1">
            <button type="submit" class="btn btn-orange">検索</button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="row">
    <div class="col-md">
        {if $userList}
        <div class="table-responsive">
            <table class="table table-striped sort-table sortable" id="fixed-drop-table">
                <colgroup>
                    <col width="80">
                    <col width="140">
                    <col width="">
                    <col width="140">
                    <col width="70">
                    <col width="100">
                    <col width="150">
                    <col width="190">
                </colgroup>
                <thead>
                <tr>
                    <th scope="col" class="sort-header column-index">表示順</th>
                    <th scope="col" class="sort-header">ユーザー名</th>
                    <th scope="col" class="sort-header">グループ名</th>
                    <th scope="col">権限</th>
                    <th scope="col" class="{$toggleLockPermission}">番号<br/>ロック</th>
                    <th scope="col" class="{$toggleLockPermission}">ロックまたは<br/>ロック解除</th>
                    <th scope="col" class="sort-header">最終ログイン日時</th>
                    <th scope="col">操作</th>
                </tr>
                </thead>
                <tbody>
                {$userList}
                </tbody>
            </table>
        </div>
        {else}
            <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
        {/if}
    </div>
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="changeData">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ユーザー情報の変更</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <form method="POST" action="{$curURI}" class="form-horizontal mb-0">
                <div class="modal-body">
                    <div class="form-group">
                        <label>ユーザーID</label><p class="gid"></p>
                    </div>
                    <div class="form-group">
                        <label>現在のグループ名</label><p class="oldName"></p>
                    </div>
                    <div class="form-group">
                        <label>新しいグループ名</label>
                        <input type="text" class="form-control newName" name="newName" />
                        <input type="hidden" class="form-control gid2" name="gid2" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
                    <button type="submit" name="button" class="btn btn-orange" value="保存">保存</button>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="deleteData">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ユーザーの削除</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <form class="mb-0" action="{$curURI}" method="POST">
                <div class="modal-body">
                    ユーザーを削除します。<br /><br />
                    <div class="form-group">
                        <label>ユーザーID</label><p class="uid"></p>
                        <input type="hidden" class="form-control uid2" name="uid2" />
                    </div>
                    <div class="form-group">
                        <label>ユーザー名</label><p class="userName"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
                    <button type="submit" name="button" class="btn btn-danger" value="削除"><i class="fas fa-trash"></i> 削除</button>
                </div>
            </form>
        </div>
    </div>
</div>
