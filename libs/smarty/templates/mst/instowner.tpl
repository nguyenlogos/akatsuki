{if $msg != ""}{$msg}{/if}
<form method="post" action="./instowner.php">
    <div class="form-row">
        <div class="form-group col-md-6 my-1 {$hideDept}">
            <label>グループ</label>
            <div data-dropdown="dept" data-selected="{$frmValues['dept']}"></div>
        </div>
        <div class="form-group col-md-5 my-1">
            <label>プロジェクト</label>
            <div data-dropdown="proj"
                 data-selected-from="{$frmValues['dept']}"
                 data-selected="{$frmValues['proj']}"
                 data-dropdown-name="proj-filter">
            </div>
        </div>
        <div class="form-group col-md-1 my-1">
            <label>検索</label>
            <button type="submit" name="submit_inst" class="btn btn-orange">検索</button>
        </div>
    </div>
</form>

{if $instList != ""}
    <div class="table-responsive" id="project-resource">
        <table class="table table-striped sort-table sortable">
            <thead>
            <tr>
                <th scope="col" class="sort-header w_250">インスタンス名 (インスタンスID)</th>
                <th scope="col" class="d-none"></th>
                <th scope="col" class="w_145 sort-header">タイプ</th>
                <th scope="col" class="sort-header">状態</th>
                <th scope="col" class="sort-header header-dept">グループ</th>
                <th scope="col" class="sort-header header-proj w_145">プロジェクト</th>
                <th scope="col" class="w_100">操作</th>
            </tr>
            </thead>
            <tbody>
            {$instList}
            </tbody>
        </table>
    </div>
{else}
    <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
{/if}
<script>
    $(document).ready(function(){
        var projectCaches = {};
        function loadDepts() {
            return window.util.ajax("/api/dept.php");
        }
        function loadProjs(dept) {
            return window.util.ajax('/api/proj.php?all=1', {
                dept : dept
            });
        }
        function updateInstanceInfo(instID, newDept, newProj) {
            var data = {
                'id' : instID,
                'dept' : newDept,
                'proj' : newProj
            };
            return window.util.ajaxPost("/api/inst.php?action=update_info", data)
        }
        function generateProjsTemplate(projs, selected) {
            projs || (projs = [])
            var $optionDefault =
                $('<option>').attr('value', '').text('--- プロジェクトを選択 ---');
            var $select = $('<select>')
                    .attr('name', 'proj')
                    .addClass('form-control')
                    .append($optionDefault);
                projs.forEach(function(proj){
                var $option =
                    $('<option>')
                        .attr('value', proj.pcode)
                        .text(proj.pname + ' (' +proj.pcode+ ')')
                    if (selected && selected == proj.pcode) {
                        $option.attr('selected', 'selected');
                    }
                $select.append($option);
            });
            return $select;
        }
        function generateDeptsTemplate(depts, selected, onchangeCallback) {
            depts || (depts = [])
            var $optionDefault =
                $('<option>').attr('value', '').text('--- グループを選択 ---');
            var $select =
                $('<select>')
                    .attr('name', 'dept')
                    .addClass('form-control')
                    .append($optionDefault)
                    .change(function(){
                        var dept = this.value;
                        if (!dept) {
                            onchangeCallback([]);
                        } else if (projectCaches[dept]) {
                            onchangeCallback(projectCaches[dept]);
                        } else {
                            loadProjs(dept).then(function(projs){
                                if (projs.length >= 0) {
                                    projectCaches[dept] = projs;
                                } else {
                                    projs = [];
                                }
                                onchangeCallback(projs);
                            })
                        }
                    })
            depts.forEach(function(dept){
                var $option =
                    $('<option>')
                        .attr('value', dept.dept)
                        .text(dept.deptname)
                    if (selected && selected == dept.dept) {
                        $option.attr('selected', 'selected');
                    }
                $select.append($option);
            });
            return $select;
        }
        loadDepts().then(function(depts){
            $('[data-dropdown="dept"]').each(function(index, element){
                var $element = $(element);
                var selected = $element.data('selected');
                var $deptsTemplate = generateDeptsTemplate(depts, selected, function(projs){
                    var $projsTemplate = generateProjsTemplate(projs);
                    $('[data-dropdown="proj"]:eq('+index+')').empty().append($projsTemplate);
                });
                $element.append($deptsTemplate);
            });

            var depts = $('[data-dropdown="proj"][data-selected-from]').map(function(){
                return $(this).data('selectedFrom');
            }).get();
            depts = depts.filter(function(v, i, self){
                return v > 0 && self.indexOf(v) === i;
            });

            var requests = [];
            depts.forEach(function(dept){
                var request = loadProjs(dept).then(function(projs){
                    if (projs.length >= 0){
                        projectCaches[dept] = projs;
                    }
                });
                requests.push(request);
            });

            $.when.apply(undefined, requests).then(function(){
                $('[data-dropdown="proj"]').each(function(index, element){
                    var $element = $(element);
                    var selected = $element.data('selected');
                    var selectedFrom = $element.data('selectedFrom');
                    var dropdownName = $element.data('dropdownName');
                    if (selectedFrom && (selected || dropdownName === 'proj-filter')) {
                        var projs = projectCaches[selectedFrom] || [];
                        var $projsTemplate = generateProjsTemplate(projs, selected);
                        $element.append($projsTemplate);
                    } else {
                        var $projsTemplate = generateProjsTemplate();
                        $element.append($projsTemplate);
                    }
                });
            });
        });

        (function(){
            $('.btn-save').each(function(index, btn){
                $(btn).click(function(){
                    var $this =  $(this);
                    var $row = $(this).closest('tr');
                    var $sel_dept = $row.find('.inst_dept > select');
                    var $sel_proj = $row.find('.inst_proj > select');
                    var newDeptValue = $sel_dept.val();
                    var newProjValue = $sel_proj.val();
                    if (newDeptValue && !newProjValue) {
                        $sel_proj.addClass('error');
                    } else {
                        $sel_proj.removeClass('error');

                        var instID = $row.find('.inst_id').data('instid');
                        $errmsg = $msg = '';
                        $this.prop('disabled', 'disabled');
                        updateInstanceInfo(instID, newDeptValue, newProjValue).then(function(response){
                            if (response) {
                                if (!response.err) {
                                    $msg = '更新に成功しました。';
                                    var instInfo = response.inst || {};
                                    if (instInfo.name) {
                                        $row.find('.inst_name').html(instInfo.name + '<br/>(' +instID+')');
                                    }
                                }
                                else if (response.msg) {
                                    $errmsg  = response.msg;
                                }
                                else {
                                    $errmsg = '不明なエラーが発生しました。';
                                }
                            }
                            else {
                                $errmsg = '不明なエラーが発生しました。';
                            }
                        }, function(response){
                            $errmsg = '不明なエラーが発生しました。';
                        }).always(function(){
                            if ($errmsg) {
                                toastr.error($errmsg)
                            }
                            else if ($msg) {
                                toastr.success($msg);
                            }
                            $this.removeProp('disabled');
                        });
                    }
                });
            });
        })();
    });
</script>
