<script type="text/javascript">
    $(function() {
        $('#deleteData').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var recipient1 = button.data('dataid');
            var recipient2 = button.data('name');
            var recipient3 = button.data('id');
            var modal = $(this);

            modal.find('.pcode').text(recipient1);
            modal.find('.pcode2').val(recipient3);
            modal.find('.pname').text(recipient2);
        });
    });
</script>

<div id="result"></div>
<div class="row">
    <div class="col-md-8">
        <p>プロジェクトを新規登録する場合は[新規登録]ボタンを押してください。</p>
    </div>
    <div class="col-md-4 text-right">
        <a href="./proj2.php?cmd=new" class="btn btn-green" role="button"><i class="fas fa-plus-circle fa-fw"></i> 新規登録</a>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md">
        {if $msg != "" && $err == 1}
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {$msg}
            </div>
        {elseif $msg != "" && $err == 0}
            <div class="alert alert-normal">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {$msg}
            </div>
        {/if}
    </div>
</div>

<h2>{$dataCount}件、登録されています。</h2>
<form method="get">
    <div class="form-row">
        {if $showDept == 1}
            <div class="form-group col-md-6 my-1">
                {pulldown4dept name=dept selected=$dept}
            </div>
        {/if}
        <div class="form-group col-md-5 my-1">
            <input type="text" name="proj" class="form-control" placeholder="プロジェクト名、またはID" value="{$proj}"/>
        </div>
        <div class="form-group col-md-1 my-1">
            <button type="submit" class="btn btn-orange">検索</button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<div class="row">
    <div class="col-md">
        {if $projList != ""}
            <div class="table-responsive">
                <table class="table table-striped sort-table sortable" id="fixed-drop-table">
                    <colgroup>
                        <col width="80">
                        <col width="">
                        <col width="200">
                        <col width="200">
                        <col width="140">
                        <col width="85">
                        <col width="190">
                    </colgroup>
                    <thead>
                    <tr>
                        <th scope="col" class="sort-header column-index">表示順</th>
                        <th scope="col" class="sort-header">ID</th>
                        <th scope="col" class="sort-header">プロジェクト名</th>
                        <th scope="col" class="sort-header">所属グループ</th>
                        <th scope="col">インスタンス数</th>
                        <th scope="col">HDD数</th>
                        <th scope="col">操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$projList}
                    </tbody>
                </table>
            </div>
        {else}
            <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
        {/if}
    </div>
</div>

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="deleteData">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">プロジェクトの削除</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <form class="mb-0" action="{$curURI}" method="POST">
                <div class="modal-body">
                    <p>プロジェクトを削除します。</p>
                    <div class="form-group">
                        <label>プロジェクトID</label><p class="pcode"></p>
                        <input type="hidden" class="form-control pcode2" name="pcode2" />
                    </div>
                    <div class="form-group">
                        <label>プロジェクト名</label><p class="pname"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
                    <button type="submit" name="button" class="btn btn-danger" value="削除"><i class="fas fa-trash"></i> 削除</button>
                </div>
            </form>
        </div>
    </div>
</div>
