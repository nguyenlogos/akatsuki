<div class="row">
    <div class="col-md">
        {if $msg != "" && $err == 0}
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {$msg}
            </div>
        {else}
            <p>下記に必要事項を入力して、[{$submitButton}]ボタンを押してください。</p>
            {if $msg != "" && $err == 1}
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {$msg}
                </div>
            {/if}
        {/if}
    </div>
</div>
<div class="row">
    <div class="col-md">
        <form method="POST" action="./proj2.php?cmd={$cmd}&id={$projID}" class="form-box title-form-01" id="reg_form">
            <div class="form-group">
                <label>プロジェクトコード <span class="badge badge-large badge-red">必須</span></label>
                <input type="text" class="form-control" id="pcode" name="pcode" value="{$pcode}" placeholder="128文字まで" maxlength="128"/>
            </div>
            <div class="form-group">
                <label>プロジェクト名 <span class="badge badge-large badge-red">必須</span></label>
                <input type="text" class="form-control" id="pname" name="pname" value="{$pname}" placeholder="256文字まで" maxlength="256"/>
            </div>
            <div class="form-group">
                <label>所属グループ <span class="badge badge-large badge-red">必須</span></label>
                {pulldown4dept name=dept selected=$dept}
            </div>
            <div class="form-group">
                <label>備考・メモ</label>
                <input type="text" class="form-control" name="memo" value="{$memo}" placeholder="" />
            </div>
            <div class="form-group text-center">
                <a href="./proj.php" class="btn btn-secondary mr-3" role="button">戻る</a>
                <button type="submit" name="button" class="btn btn-orange bt-sign" value="{$submitButton}">{$submitButton}</button>
            </div>

            <input type="hidden" name="oldpcode" value="{$pcode}" />
        </form>
    </div>
</div>
<script type="text/javascript">
    $('form').validate({
        rules: {
            pcode: {
                maxlength: 128
            },
            pname: {
                maxlength: 256
            }
        }
    });
</script>