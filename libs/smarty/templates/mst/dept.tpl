<script type="text/javascript">
    $(function() {
        $('#changeData').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var recipient1 = button.data('dataid');
            var recipient2 = button.data('name');
            var modal = $(this);

            modal.find('.gid').text(recipient1);
            modal.find('.gid2').val(recipient1);
            modal.find('.oldName').text(recipient2);
            modal.find('.newName').val(recipient2);
        });

        $('#deleteData').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var recipient1 = button.data('dataid');
            var recipient2 = button.data('name');
            var modal = $(this);

            modal.find('.gid').text(recipient1);
            modal.find('.gid2').val(recipient1);
            modal.find('.groupName').text(recipient2);
        });
    });
</script>

<div id="result"></div>

<form method="POST" action="{$curURI}">
    <p>グループ名を新規登録する場合は、新しいグループ名を入力して[新規登録]ボタンを押してください。</p>
    <div class="form-row">
        <div class="form-group col-md-9 col-lg-10 my-1">
            <input type="text" name="newDeptName" placeholder="新しいグループ名 (最大128文字)" class="form-control" maxlength="128" />
        </div>
        <div class="form-group col-md-3 col-lg-2 my-1 text-right">
            <button type="submit" name="button" class="btn btn-green" value="新規登録"><i class="fas fa-plus-circle fa-fw"></i> 新規登録</button>
        </div>
    </div>

    {if $msg != "" && $err == 1}
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {elseif $msg != "" && $err == 0}
        <div class="alert alert-normal" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {$msg}
        </div>
    {/if}
</form>


<h2>{$dataCount}件、登録されています。</h2>

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
{if $deptList != ""}
    <div class="table-responsive">
        <table class="table table-striped sort-table sortable" id="fixed-drop-table">
            <colgroup>
                <col width="80">
                <col width="">
                <col width="200">
                <col width="180">
            </colgroup>
            <thead>
            <tr>
                <th scope="col" class="column-index">表示順</th>
                <th scope="col" class="sort-header">グループ名</th>
                <th scope="col" class="sort-header">更新日時 (人数)</th>
                <th scope="col">操作</th>
            </tr>
            </thead>
            <tbody>
            {$deptList}
            </tbody>
        </table>
    </div>
{/if}
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="changeData">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">グループ名の変更</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <form method="POST" action="{$curURI}" class="mb-0 form-horizontal">
                <div class="modal-body">
                    <div class="form-group">
                        <label>グループID</label><p class="gid"></p>
                    </div>
                    <div class="form-group">
                        <label>現在のグループ名</label><p class="oldName"></p>
                    </div>
                    <div class="form-group">
                        <label>新しいグループ名</label>
                        <input type="text" class="form-control newName" name="newName" maxlength="128" />
                        <input type="hidden" class="form-control gid2" name="gid2" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
                    <button type="submit" name="button" class="btn btn-orange" value="保存">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="deleteData">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">グループの削除</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <form class="mb-0" action="{$curURI}" method="POST">
                <div class="modal-body">
                    <p>グループを削除します。</p>
                    <div class="form-group">
                        <label>グループID</label><p class="gid"></p>
                    </div>
                    <div class="form-group">
                        <label>グループ名</label><p class="groupName"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" class="form-control gid2" name="gid2" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
                    <button type="submit" name="button" class="btn btn-danger" value="削除"><i class="fas fa-trash"></i> 削除</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('form').validate({
        rules: {
            newDeptName: {
                maxlength: 128
            },
            newName: {
                maxlength: 128
            }
        }
    });
</script>