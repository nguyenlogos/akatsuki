{if $msg != ""}
    <div class="alert {($err == 1) ? 'alert-danger' : 'alert-normal'}">
        <button type="button" class="close" data-dismiss="alert">×</button>{$msg}
    </div>
{/if}

<div class="row">
    <article class="col-md">
        <form method="POST" action="./user2.php?id={$uid}" class="form-box title-form-01" id="reg_form">
            <div class="form-group row">
                <label class="col-md-3 col-form-label">氏名</label>
                <div class="col-md-1"><span class="badge badge-large badge-red">必須</span></div>
                <div class="col-md-7">
                    <input type="text" class="form-control name" id="name_group" name="name" value="{$name}" placeholder="64文字まで" maxlength="64" required/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label">メールアドレス</label>
                <div class="col-md-1"><span class="badge badge-large badge-red">必須</span></div>
                <div class="col-md-7">
                    <input type="text" class="form-control email" id="email" name="email" value="{$email}" placeholder="64文字まで" maxlength="64" required/>
                </div>
            </div>
            {if !$uid}
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">パスワード</label>
                    <div class="col-md-1"><span class="badge badge-large badge-red">必須</span></div>
                    <div class="col-md-7">
                        <input type="password" class="form-control" id="password" name="pass" value="{$pass}" placeholder="64文字まで" maxlength="64" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">パスワード(再入力)</label>
                    <div class="col-md-1"><span class="badge badge-large badge-red">必須</span></div>
                    <div class="col-md-7">
                        <input type="password" class="form-control" id="password2" name="pass2" value="{$pass2}" placeholder="64文字まで" maxlength="64" required/>
                    </div>
                </div>
            {/if}
            <div class="form-group row">
                <label class="col-md-3 col-form-label">所属グループ</label>
                <div class="col-md-1"><span class="badge badge-large badge-red">必須</span></div>
                <div class="col-md-7">
                    {pulldown4dept name=group selected=$dept onchange=false}
                </div>
            </div>
            {if $toggleLockPermission}
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">ロックまたはロック解除</label>
                    <div class="col-md-1"></div>
                    <div class="col-md-7">
                        <div class="form-check">
                            <input type="checkbox"
                                   name="lock"
                                   onclick="changeValueCheckbox(this)"
                                   value="{$lock}" {if $lock == 1}checked{/if}>
                            <label>ロック</label>
                        </div>
                    </div>
                </div>
            {/if}

            <div class="form-group row">
                <label class="col-md-3 col-form-label">パーミッション</label>
                <div class="col-md-1"></div>
                <div class="col-md-7">
                    {if $isSystemAdmin}
                        <div class="form-check">
                            <input type="checkbox" name="sysadmin" {$sysadmin} />
                            <label>システム管理者</label>
                        </div>
                    {/if}

                    <div class="form-check">
                        <input type="checkbox" name="admin" {$admin} />
                        <label>マネージャー</label>
                    </div>
                </div>
            </div>

            <div class="row justify-content-md-center submit-contents">
                <div class="col-md-4">
                    <a href="./user.php" class="btn btn-secondary mr-3" role="button">戻る</a>
                    <button type="submit" name="button" class="btn btn-orange bt-sign" value="{$submitButton}">{$submitButton}</button>
                </div>
            </div>
        </form>
    </article>
</div>
<script>
    $(document).ready(function(){
        var id = {$uid} + '';
        var newParams = util.replaceUrlParam(window.location.search, 'id', id);
        var newUrl = window.location.pathname + newParams;
        window.history.replaceState({}, '', newUrl);
    });
</script>