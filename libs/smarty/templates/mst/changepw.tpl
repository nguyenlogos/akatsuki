{if $msg != "" && $err == 0}
    <div class="alert alert-normal">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$msg}
    </div>
{else}

{if $policy_pass != "" && $err == 1 || $msg != "" && $err == 1}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$msg}
        {$policy_pass}
    </div>
{/if}

<div class="block-content">
    <p>ログイン用のパスワードを変更します。<br />
        以下の情報を入力して、[変更]ボタンを押してください。
    </p>
</div>
<form method="POST" action="/mst/changepw.php" class="form-box title-form-01" id="reg_form">
    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="rule_name">現在のパスワード</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="col-md-7">
            <input type="password"
                   class="form-control"
                   required placeholder="現在のパスワードを入力してください"
                   name="p1"
                   value="{$p1|escape}" />
        </div>
    </div>

    <div class="form-group row">
        <label class="col-md-3 col-form-label" for="rule_name">新しいパスワード</label>
        <div class="col-md-1">
            <span class="badge badge-large badge-red">必須</span>
        </div>
        <div class="multiple-input col-md-7">
            <input type="password"
                   class="form-control"
                   id="password"
                   required
                   placeholder="新しいパスワードを入力してください"
                   name="p2"
                   value="{$p2|escape}" />
            <input type="password"
                   class="form-control"
                   id="confirm_password"
                   required
                   placeholder="新しいパスワード(再度)を入力してください"
                   name="p3"
                   value="{$p3|escape}" />
        </div>
    </div>

    <div class="form-group row submit-contents">
        <div class="col-md-4"></div>
        <div class="col-md-7">
            <button type="submit" name="button" class="btn btn-orange" value="変更">変更</button>
        </div>
    </form>
</div>
{/if}

