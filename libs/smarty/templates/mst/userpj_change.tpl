{if $msg != ''}
    <div id="message">
        <div class="alert alert-normal">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$msg}</span>
        </div>
    </div>
{/if}
{if $errmsg != ''}
    <div id="message">
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <span>{$errmsg}</span>
        </div>
    </div>
{/if}
<form>
    <div class="form-row">
        <div class="form-group col-md-6 my-1 {$hideDept}">
            <div data-dropdown-source="dept"
                 data-selected="{$frmValues['dept']}"
                 data-onchange="deptChanged">
            </div>
        </div>
        <div class="form-group col-md-5 my-1">
            <input type="text" name="uname" class="form-control" placeholder="ユーザー名" value="{$frmValues['uname']}" max-bytes="128">
        </div>
        <div class="form-group col-md-1 my-1">
            <button type="submit" class="btn btn-orange btn-filter">検索</button>
        </div>
        <input type="hidden" name="proj" value="{$frmValues['proj']}">
    </div>
</form>

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="row">
    <div class="col-md">
        {tablelist headers=$headerList data=$tablelist actions=$actionList}
    </div>
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<div class="form-group text-center">
    <a href="./userpj.php" class="btn btn-secondary mr-3" role="button">戻る</a>
    <button type="button" name="button" class="btn btn-orange btn-save" value="保存" disabled>保存</button>
</div>
<script>
    $(document).ready(function(){
        var g_pcode = '{$frmValues["proj"]}' + '';
        var g_changed = false;
        var $btnSave = $('.btn-save');

        $('td[data-item-epcode]').each(function(i, el){
            var $td = $(el);
            var empid = $td.closest('tr').find('td[data-item-empid]').data('itemEmpid');
            var epcode = +$td.data('itemEpcode');
            var $chkbox = $('<input type="checkbox" name="epcode">')
                .attr('data-empid', empid)
                .change(function(){
                    if (!g_changed) {
                        g_changed = true;
                        $btnSave.removeProp('disabled');
                    }
                });
            if (epcode) {
                $chkbox.prop('checked', 'checked');
            }
            var $label = '<label></label>';
            var $wrapper = $('<div class="form-check checkbox-simple">').append($chkbox, $label);
            $td.empty().append($wrapper);
        });
        $('thead > tr > th:first').addClass('text-center');
        $btnSave.click(function(){
            if ($btnSave.isSaving) {
                return false;
            }
            var checkedList = [];
            var uncheckedList = [];

            $('input[name="epcode"]').each(function(i, el){
                if (el.checked) {
                    checkedList.push($(el).data('empid'));
                } else {
                    uncheckedList.push($(el).data('empid'));
                }
            });
            var errmsg = '不明なエラーが発生しました。';
            $btnSave.isSaving = true;
            $btnSave.prop('disabled', 'disabled');

            window.util.ajaxPost('/api/userpj.php?action=update', {
                'pcode' : g_pcode,
                'checked' : checkedList,
                'unchecked' : uncheckedList
            }).then(function(response){
                if (response && !response.err) {
                    errmsg = '';
                }
            }).always(function(){
                if (errmsg) {
                    toastr.error(errmsg);
                    $btnSave.removeProp('disabled');
                } else {
                    toastr.success('更新に成功しました。');
                    g_changed = false;
                }
                $btnSave.isSaving = false;
            });
        });
    });
</script>