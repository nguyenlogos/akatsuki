<div class="row">
    <div class="col-md">
        {if $msg != ''}
            <div class="alert alert-normal">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <span>{$msg}</span>
            </div>
        {/if}
        {if $errmsg != ''}
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <span>{$errmsg}</span>
            </div>
        {/if}
    </div>
</div>

<form>
    <div class="form-row">
        {if $hideDept == ''}
            <div class="form-group col-md-6 my-1">
                <div data-dropdown-source="dept"
                     data-selected="{$frmValues['dept']}"
                     data-onchange="deptChanged">
                </div>
            </div>
        {/if}
        <div class="form-group col-md-5 my-1">
            <input type="text"
                   name="pname"
                   class="form-control"
                   placeholder="プロジェクト名、またはID"
                   value="{$frmValues['pname']}"
                   max-bytes="128">
        </div>
        <div class="form-group col-md-1 my-1">
            <button type="submit" class="btn btn-orange">検索</button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>
<div class="row">
    <div class="col-md">
        {if ($tablelist|@count) == 0}
            <div class="alert alert-normal">{$smarty.const.ERR_DATA_NOT_FOUND}</div>
        {else}
            {tablelist headers=$headerList data=$tablelist actions=$actionList}
        {/if}
    </div>
</div>
<div class="row">
    <div class="col-md">
        {$paginationStr}
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.btn-change').click(function(){
            var pcode = $(this).closest('tr').find('td[data-item-pcode]').data('itemPcode');
            window.location.href = './userpj_change.php?proj=' + pcode;
        });
        // window.deptChanged = function(selectedDept) {
        //     window.util.setParamsURL({
        //         dept : selectedDept || undefined
        //     });
        // };
    });
</script>