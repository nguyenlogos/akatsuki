<div class="col-xs-12">
<p>ディスクのバックアップ設定を行います。</p>
	<div class="row" style="padding-top: 30px;">
		<div class="col-sm-12">
      {if $msg != "" && $err == 1}
        <div class="alert alert-dismissible alert-info">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <p>{$msg}</p>
          <p><a href="./cronjob.php">戻る</a></p>
        </div>
      {elseif $msg != "" && $err == 2}
        <div class="alert alert-dismissible alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <p>{$msg}</p>
          <p><a href="./cronjob.php">戻る</a></p>
        </div>
      {/if}

      {if $err == 0}
				<form method="post" action="" enctype="multipart/form-data" id="unmount_validation">

          <div class="row">
            <div class="col-xs-5" >
              <label>プロジェクト</label>
              {pulldown4cronproj name="cronproj" cid=$CID }
            </div>
            <div class="col-xs-5 no-padding-left">
              <label>インスタンス</label>
              <select name="showgintance" id="instance-list" class="form-control">
                <option value="">すべて</option>
                {if $html}{$html}{/if}
              </select>
            </div>
          </div>

          <table class="table table-hover mgT">
            <thead>
            <tr>
              <th>ディスク</th>
              <th width="10%">頻度</th>
              <th width="45%">開始時刻</th>
              <th width="10%">世代</th>
            </tr>
            </thead>
            <tbody id="showhdd">
              {if $hddStr}{$hddStr}{/if}
            </tbody>
            <tbody id="showUnmount">
              {if $hddStr}{$hddStr}{/if}
            </tbody>
          </table>
          <div class="form-group">
            <input type="submit" class="btn btn-primary pull-right" name="generate" value="設定" />
          </div>
				</form>
      {/if}
		</div>
	</div>
</div>

<script>
  $('#project').on('change',function(){
    var intanceID = $(this).val();
    if (intanceID) {
      $.ajax({
        type:'POST',
        url:'/cronjob/load_intance.php',
        data:'intance_id='+intanceID,
        success:function(html){
          $('#instance-list').html(html);
          $('#showhdd').html('');
        }
      });
    } else {
      $('#instance-list').html('<option value = "0">すべて</option>');
      $('#showhdd').html('');
    }
  });

  $('#project').on('change',function(){
    var unmountID = $(this).val();
    if (unmountID) {
      $.ajax({
        type:'POST',
        url:'/cronjob/load_intance.php',
        data:'unmount_id='+unmountID,
        success:function(html){
          $('#showUnmount').html(html);
        }
      });
    }
  });

  $('#instance-list').on('change',function(){
    var hddID = $(this).val();
    if(hddID){
      $.ajax({
        type:'POST',
        url:'/cronjob/load_intance.php',
        data:'hdd_id='+hddID,
        success:function(html){
          $('#showhdd').html(html);
        }
      });
    }
  });

</script>
