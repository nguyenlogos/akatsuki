<script src="/src/assets/js/Chart.min.js"></script>
{if $msg != ""}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$msg}
    </div>
{/if}
{if $rateMessage != ""}
    <div class="alert alert-normal">
        {$rateMessage}
    </div>
{/if}
<form class="mt-5" method="POST" action="/cons/inst.php" target="_blank">
    {instDB info=$instInfo}
    <input type="hidden" name="inst_filter">
</form>
<div class="row">
    <div class="col-md mt-5">
        <canvas id="chart6M"></canvas>
    </div>
    <div class="w-100 mt-5"></div>
    <div class="col-md">
        <canvas id="chart6MInstances"></canvas>
    </div>
    <div class="w-100"></div>
    <div class="col-md mt-5">
        <canvas id="chart6MEBS"></canvas>
    </div>

</div>
<form class="form-inline" METHOD="POST" ACTION="./db.php">
    <div class="form-row mt_60">
        <div class="col-md">
            {$YMlist}
            <input type="submit" class="form-control btn btn-orange" name="button" value="表示" />
        </div>
    </div>
</form>

<div class="alert alert-normal">
    <table border="0">
        <tr>
            <td>為替レート：</td>
            <td colspan="3">1 ドル = {$rate1M} 円</td>
        </tr>
        <tr>
            <td>利用料：</td>
            <td align='right'>{number_format($usageTotal,2)} ドル</td>
            <td width="50">&nbsp;</td>
            <td align='right'>{number_format($usageTotalJPY)} 円</td>
        </tr>
        <tr>
            <td>消費税：</td>
            <td align='right'>{number_format($taxTotal,2)} ドル</td>
            <td width="50">&nbsp;</td>
            <td align='right'>{number_format($taxTotalJPY)} 円</td>
        </tr>
        <tr>
            <td>合　計：</td>
            <td align='right'>{number_format($usageTotal + $taxTotal,2)} ドル</td>
            <td width="50">&nbsp;</td>
            <td align='right'>{number_format($usageTotalJPY + $taxTotalJPY)} 円</td>
        </tr>
    </table>
</div>

{if $dataSetsStr != ""}
<div class="row">
    <div class="col-md">
        <canvas id="chart1"></canvas>
    </div>
    <div class="w-100"></div>
    <div class="col-md chart2box">
        <canvas id="chart2"></canvas>
    </div>
</div>
<style>
    <!--
    .chart2box {
        width  : 80%;
        margin : 15px auto;
    }
    -->
</style>
{/if}
<script>
    $(document).ready(function () {
        $('a[data-filter]').on('click', function () {
            var filterValue = $(this).attr('data-filter');
            if (!filterValue) {
                return;
            }
            var $form       = $(this).closest('form');
            $form.find('input[name="inst_filter"]').val(filterValue);
            $form.submit();
        });
    })
</script>
<script>
    var ctx6M          = document.getElementById('chart6M').getContext('2d');
    var ctx6MInstances = document.getElementById('chart6MInstances').getContext('2d');
    var ctx6MEBS       = document.getElementById('chart6MEBS').getContext('2d');

    {if $dataSetsStr != ""}
    var ctx  = document.getElementById('chart1').getContext('2d');
    var ctx2 = document.getElementById('chart2').getContext('2d');
    {/if}

    var gradientFill = ctx6MInstances.createLinearGradient(0, 0, 0, 400);
    gradientFill.addColorStop(0, "rgba(0, 0, 255, 0.6)");
    gradientFill.addColorStop(1, "rgba(0, 0, 255, 0)");

    var chart = new Chart(ctx6MEBS, {
        type: 'bar',
        // The data for our dataset
        data: {
            labels  : [{$labels6M}],
            datasets: [{$dataSetsStr6MEBS}]
        },

        // Configuration options go here
        options: {
            legend  : {
                display: true,
                labels : {
                    boxWidth     : 10,
                    usePointStyle: true
                },
            },
            title   : {
                display: true,
                text   : '{$graphTitle6MEBS}'
            },
            tooltips: {
                mode     : 'index',
                intersect : false,
                titleFontSize : 10,
                titleFontStyle : 'normal',
                bodyFontSize : 10,
                footerFontSize : 10,
                footerFontStyle : 'normal',
                position : 'nearest',
                callbacks: {
                    footer: function (tooltipItems, data) {
                        var sum_size = 0;
                        var sum_count = 0;
                        var datalabel_size;
                        var datalabel_count;

                        tooltipItems.forEach(function (tooltipItem) {
                            if (tooltipItem.datasetIndex < 2) {
                                sum_size += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            } else {
                                sum_count += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            }
                        });
                        datalabel_size = Math.floor(sum_size * Math.pow(10, 2)) / Math.pow(10, 2);
                        datalabel_count = Math.floor(sum_count);
                        return '合計: ' + datalabel_size.toLocaleString() + 'GiB ' + datalabel_count.toLocaleString() + '個';
                    },
                    label : function (tooltipItem, data) {
                        if (tooltipItem.datasetIndex < 2) {
                            return tooltipItem.yLabel.toLocaleString() + 'GiB';
                        } else {
                            return tooltipItem.yLabel.toLocaleString() + '個';
                        }
                    },
                },
            },
            scales  : {
                xAxes: [
                    {
                        stacked: false,
                    }
                ],
                yAxes: [
                    {
                        id        : "y1",
                        type      : "linear",
                        position  : "left",
                        stacked   : false,
                        scaleLabel: {
                            display    : true,
                            labelString: 'ディスク容量(GiB)',
                        },
                        ticks     : {
                            min         : 0,
                            userCallback: thousandsSeparator
                        }
                    }, {
                        id        : "y2",
                        type      : "linear",
                        position  : "right",
                        stacked   : false,
                        scaleLabel: {
                            display    : true,
                            labelString: 'ディスク個数',
                        },
                        ticks     : {
                            min         : 0,
                            userCallback: thousandsSeparator
                        },
                        gridLines : {
                            drawOnChartArea: false,
                        },
                    }
                ]
            }
        }
    });

    var chart = new Chart(ctx6MInstances, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels  : [{$labels6M}],
            datasets: [{$dataSetsStr6MInstances}]
        },

        // Configuration options go here
        options: {
            legend  : {
                display: true,
                labels : {
                    boxWidth     : 10,
                    usePointStyle: true
                },
            },
            title   : {
                display: true,
                text   : '{$graphTitle6MInstances}'
            },
            tooltips: {
                mode     : 'index',
                intersect : false,
                titleFontSize : 10,
                titleFontStyle : 'normal',
                bodyFontSize : 10,
                footerFontSize : 10,
                footerFontStyle : 'normal',
                position : 'nearest',
                callbacks: {
                    footer: function (tooltipItems, data) {
                        var sum = 0;
                        var datalabel;

                        tooltipItems.forEach(function (tooltipItem) {
                            sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        });
                        datalabel = Math.floor(sum * Math.pow(10, 2)) / Math.pow(10, 2);
                        return '合計: ' + datalabel.toLocaleString() + '台';
                    },
                    label : function (tooltipItem, data) {
                        if (tooltipItem.yLabel != 0) {
                          return data.datasets[tooltipItem.datasetIndex].label + ' x ' + tooltipItem.yLabel.toLocaleString();
                        }
                    },
                },
            },
            scales  : {
                xAxes: [
                    {
                        stacked: true,
                    }
                ],
                yAxes: [
                    {
                        stacked   : true,
                        scaleLabel: {
                            display    : true,
                            labelString: 'インスタンス数',
                        },
                        ticks     : {
                            min         : 0,
                            userCallback: thousandsSeparator
                        },
                    }
                ]
            }
        }
    });

    var chart = new Chart(ctx6M, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels  : [{$labels6M}],
            datasets: [{$dataSetsStr6M}]
        },

        // Configuration options go here
        options: {
            legend  : {
                display: true,
                labels : {
                    boxWidth     : 10,
                    usePointStyle: true
                },
            },
            title   : {
                display: true,
                text   : '{$graphTitle6M}'
            },
            tooltips: {
                mode     : 'index',
                intersect : false,
                titleFontSize : 10,
                titleFontStyle : 'normal',
                bodyFontSize : 10,
                footerFontSize : 10,
                footerFontStyle : 'normal',
                position : 'nearest',
                callbacks: {
                    footer: function (tooltipItems, data) {
                        var sum = 0;
                        var datalabel;

                        tooltipItems.forEach(function (tooltipItem) {
                            if (tooltipItem.datasetIndex != 0) {
                                sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            }
                        });
//                        datalabel = Math.floor(sum * Math.pow(10, 2)) / Math.pow(10, 2);
                        datalabel = Math.floor(sum);
                        return '合計: ' + datalabel.toLocaleString() + '円';
                    },
                    label : function (tooltipItem, data) {
                        if (tooltipItem.datasetIndex != 0) {
                            return tooltipItem.yLabel.toLocaleString() + '円';
                        } else {
                            return tooltipItem.yLabel.toLocaleString() + '円/ドル';
                        }
                    },
                },
            },
            scales  : {
                xAxes: [
                    {
                        stacked: true,
                    }
                ],
                yAxes: [
                    {
                        id        : 'y1',
                        stacked   : true,
                        scaleLabel: {
                            display    : true,
                            labelString: '利用料 (円)',
                        },
                        ticks     : {
                            min         : 0,
                            userCallback: thousandsSeparator
                        },
                    }, {
                        id        : 'y2',
                        type      : 'linear',
                        position  : 'right',
                        stacked   : 'ralse',
                        scaleLabel: {
                            display    : true,
                            labelString: '為替レート (円/ドル)',
                        },
                        ticks     : {
                            min         : 70,
                            userCallback: thousandsSeparator
                        },
                        gridLines : {
                            drawOnChartArea: false,
                        }
                    }
                ]
            }
        }
    });

    {if $dataSetsStr != ""}
    var chart  = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels  : [{$labels}],
            datasets: [{$dataSetsStr}]
        },

        // Configuration options go here
        options: {
            legend  : {
                display: true,
                labels : {
                    boxWidth     : 10,
                    usePointStyle: true
                },
            },
            title   : {
                display: true,
                text   : '{$graphTitle}'
            },
            tooltips: {
                mode     : 'index',
                intersect : false,
                titleFontSize : 10,
                titleFontStyle : 'normal',
                bodyFontSize : 10,
                footerFontSize : 10,
                footerFontStyle : 'normal',
                position : 'nearest',
                callbacks: {
                    title: function (tooltipItems, data) {
                        return tooltipItems[0].xLabel + '日';
                    },
                    footer: function (tooltipItems, data) {
                        var sum = 0;
                        var datalabel;

                        tooltipItems.forEach(function (tooltipItem) {
                            sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                        });
                        datalabel = Math.floor(sum * Math.pow(10, 2)) / Math.pow(10, 2);
                        return '合計: ' + datalabel.toLocaleString() + '円';
                    },
                    label : function (tooltipItem, data) {
                        // 1ヵ月コスト推移
                        var value = tooltipItem.yLabel.toLocaleString() + '円';
                        return value;
                    },
                },
            },
            scales  : {
                xAxes: [
                    {
                        stacked: true,
                    }
                ],
                yAxes: [
                    {
                        stacked   : true,
                        scaleLabel: {
                            display    : true,
                            labelString: '利用料 (円)',
                        },
                        ticks     : {
                            min         : 0,
                            userCallback: thousandsSeparator
                        },
                    }
                ]
            }
        }
    });
    var chart2 = new Chart(ctx2, {
        // The type of chart we want to create
        type   : 'doughnut',
        data   : {
            labels  : [{$labels2}],
            datasets: [
                {
                    data           : [{$dataSetsStr2}],
                    backgroundColor: [{$colors2}],
                }
            ],
        },
        // Configuration options go here
        options: {
            legend  : {
                position: 'right',
                labels  : {
                    boxWidth     : 10,
                    usePointStyle: false
                },
            },
            title   : {
                display: true,
                text   : '{$graphTitle2}'
            },
            tooltips: {
                intersect : false,
                callbacks: {
                    label: function (tooltipItem, data) {
                        var dataLabel = data.labels[tooltipItem.index];
                        var value     = ': ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();

                        // make this isn't a multi-line label (e.g. [["label 1 - line 1, "line 2, ], [etc...]])
                        if (Chart.helpers.isArray(dataLabel)) {
                            // show value on first line of multiline label
                            // need to clone because we are changing the value
                            dataLabel = dataLabel.slice();
                            dataLabel[0] += value;
                        } else {
                            dataLabel += value;
                        }

                        // return the text to display on the tooltip
                        return dataLabel + '円';
                    },
                },
            },
        }
    });
    {/if}

    function thousandsSeparator(value, index, values)
    {
        value = value.toString().split(/(?=(?:...)*$)/).join(',');
        return value;
    }

</script>
{if $dataSetsStr != ""}
    <div class="row">
        <div class="col-md">
            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#detail">
                <i class="fas fa-exclamation-triangle"></i> 明細の表示/非表示
            </button>
            <div id="detail" class="collapse mt-3 mb-3">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th width="150">利用日</th>
                        <th width="400">サービス名</th>
                        <th>料金(ドル)</th>
                    </tr>
                    </thead>
                    <tbody>
                    {$recs}
                    </tbody>
                </table>
            </div>
        </div>
    </div>

{else}
    <div class="alert alert-warning">
        <p class="mb-0">データがありません。</p>
    </div>
{/if}
