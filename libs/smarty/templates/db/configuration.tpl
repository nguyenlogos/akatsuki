{if $msg != "" && $err == 1}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <p class="mb-0">{$msg}</p>
    </div>
{elseif $msg != "" && $err == 0}
    <div class="alert alert-normal">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <p class="mb-0">{$msg}</p>
    </div>
{/if}

<form method="post" class="mt-5">
    <div class="chat-panel card card-default mb-5">
        <div class="card-header">
            <i class="icon-comments"></i> パスワードに関する設定
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6 my-1">
                    <label>長さ</label>
                    <input type="number"
                           name="pass_lenght"
                           min="1"
                           max="24"
                           class="form-control"
                           placeholder="パスワードの長さ"
                           value="{$pass_length}"
                    >
                    <p class="text-danger">0は指定できません。</p>
                </div>
                <div class="form-group col-md-6 my-1">
                    <label>有効期限 (日数)</label>
                    <input type="number"
                           name="pass_expired"
                           class="form-control"
                           min="0" max="12"
                           placeholder="日数"
                           value="{$pass_expired}"
                    >
                    <p class="text-danger">0に設定した場合は無期限です。</p>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12 my-1">
                    <div class="form-check">
                        <input type="checkbox"
                               name="pass_lowercase"
                               id="pass_lowercase_chk"
                               class="form-check-input"
                               value="{$pass_lowercase}"
                               {if $pass_lowercase == 1}checked{/if}
                               onclick="changeValueCheckbox(this)">
                        <label for="pass_lowercase_chk" class="form-check-label">必ず大文字・小文字を使用</label>
                    </div>
                    <div class="form-check">
                        <input type="checkbox"
                               name="pass_number"
                               id="pass_number_chk"
                               class="form-check-input"
                               value="{$pass_number}"
                               {if $pass_number == 1}checked{/if}
                               onclick="changeValueCheckbox(this)"
                        >
                        <label for="pass_number_chk" class="form-check-label">必ず数字を使用</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="chat-panel card card-default mb-5">
        <div class="card-header">
            <i class="icon-comments"></i> システムに関する設定
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-6 my-1">
                    <label>パスワード変更などのURLの有効期限 (秒)</label>
                    <input type="number"
                           name="admin_active"
                           class="form-control"
                           value="{$admin_active}"
                           min="0"
                           max="1000000000"
                    >
                    <p class="text-danger">0に設定した場合は無期限です。</p>
                </div>
                <div class="form-group col-md-6 my-1">
                    <label>アカウントロックする連続パスワード間違い (回数)</label>
                    <input type="number"
                           name="number_lock_failed"
                           class="form-control"
                           value="{$number_lock_failed}"
                           min="0"
                           max="1000000000"
                    >
                    <p class="text-danger">0に設定した場合はロックしません。</p>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6 my-1">
                    <label>インスタンスの期限前通知メール (日数)</label>
                    <input type="number"
                           name="instance_expires"
                           class="form-control"
                           value="{$instance_expires}"
                           min="0"
                           max="1000000000"
                    >
                    <p class="text-danger">0に設定した場合はメール通知しません。</p>
                </div>
                <div class="form-group col-md-6 my-1">
                    <label>Timezone Settings</label>
                    {pulldown4timezone name="timezone" zone="{$timezone}" zoneid="{$timezoneid}"}
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-12 my-1">
                    <label>ロックされたアカウントの解除権限</label>
<!--                     <div class="form-check">
                        <input type="checkbox"
                               name="check_sys_admin"
                               id="check_sys_admin_chk"
                               class="form-check-input"
                               onclick="changeValueCheckbox(this)"
                               value="{$check_sys_admin}"
                               {if $check_sys_admin == 1}checked{/if}
                        >
                        <label for="check_sys_admin_chk" class="form-check-label">システム管理者</label>
                    </div> -->
                    <div class="form-check">
                        <input type="checkbox"
                               name="check_manager"
                               id="check_manager_chk"
                               class="form-check-input"
                               onclick="changeValueCheckbox(this)"
                               value="{$check_manager}"
                               {if $check_manager == 1}checked{/if}
                        >
                        <label for="check_manager_chk" class="form-check-label">マネージャー</label>
                    </div>
                </div>
            </div>

            <div class="form-row mt-4">
                <div class="form-group col-md-12 my-1">
                    <label>インスタンスに関するメール通知</label>
                    <div class="form-check">
                        <input type="checkbox"
                               name="email_instance"
                               id="email_instance_chk"
                               class="form-check-input"
                               onclick="changeValueCheckbox(this)"
                               value="{$email_instance}"
                               {if $email_instance == 1}checked{/if}
                        >
                        <label for="email_instance_chk" class="form-check-label">起動・停止などについてメール通知</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-md-center submit-contents">
        <div class="col-md-3 text-center">
            <button type="submit" name="save_setting" class="btn btn-orange" id="reveal">保存</button>
            <input type="hidden" name="cid_reg" value="{$cid_reg}">
        </div>
    </div>

</form>
