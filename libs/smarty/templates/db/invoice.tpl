<div class="col-xs-12">
<p>請求書をダウンロードしてください。</p>
<div class="alert alert-dismissible alert-info">
  <button type="button" class="close" data-dismiss="alert">×</button>
  通常、請求書は末締め翌月3日頃にご用意しております。
</div>
<ul>
{if $count == 0}
<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">×</button>
  請求書はまだ作成されていません。
</div>
{else}
{for $i=0 to ($count -1)}
  <li>{$ymstr[$i]} {number_format($totaljpy[$i])}円(税込) <a href='./pdf.php?ym={$ym[$i]}'>ダウンロード (PDF形式)</a>
{/for}
</ul>
{/if}
<br />
<br />
</div>
