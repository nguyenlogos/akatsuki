<style type="text/css">
    img {
        max-width: 100%;
        width: auto;
    }
</style>
<div class="text-center page-403">
    {*<h1>403 エラー!</h1>*}
    <p><img src="/src/assets/images/403.png" alt="403 エラー!"></p>
    <p>指定された操作は許可されていません。</p>
    <p><a href="/src/logout.php" class="btn btn-orange">ログアウト</a></p>
</div>
