<div class="alert alert-normal text-center">
  <button type="button" class="close" data-dismiss="alert">×</button>
  {$msg}
  <p class="mt-5 mb-0"><a href="/" class="btn btn-orange">ログイン画面</a></p>
</div>

