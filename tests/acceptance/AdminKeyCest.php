<?php

class AdminKeyCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function httpCodeTest(AcceptanceTester $I)
    {
        $I->amOnPage('/admin/key.php');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeElement('h1.page-header');
    }
}
