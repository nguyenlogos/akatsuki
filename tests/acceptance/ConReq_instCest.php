<?php

class ConReq_instCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function httpCodeTest(AcceptanceTester $I)
    {
        $I->amOnPage('/cons/req_inst.php');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeElement('h1.page-header');
    }
}
