<?php

class MstProjCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function httpCodeTest(AcceptanceTester $I)
    {
        $I->amOnPage('/mst/proj.php');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeElement('h1.page-header');
    }
}
