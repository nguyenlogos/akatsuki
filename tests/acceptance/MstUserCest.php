<?php

class MstUserCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function httpCodeTest(AcceptanceTester $I)
    {
        $I->amOnPage('/mst/user.php');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeElement('h1.page-header');
    }
}
