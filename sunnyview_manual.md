- Install prerequisites

  ```bash
  apt update && apt install git nginx php
  ```

- Generate SSH Key

  - Goto http://13.231.13.129/  then log in

  - Goto to Profile > Settings > SSH Keys (http://13.231.13.129/profile/keys) > Click Generate one

    ``` bash
    ssh-keygen -t rsa -C "haivd@vn.ids.jp" -b 4096
    cat ~/.ssh/id_rsa.pub
    ```

- Check nginx location

  ```bash
  nginx -V
  ```

- Create new configuration file in `/etc/nginx/sites-available`

  ```nginx
  server {
  	listen 80;
  	server_name akatsuki_dev.local www.akatsuki_dev.local;
  
  	root /var/www/akatsuki;
  	index index.php index.html;
  
  	location / {
  		try_files $uri $uri/ /index.php?$args;
  	}
  
  	location ~ ^/(?!(src|assets)).* {
  		rewrite ^/(.*)$ /src/index.php?r=$1;
  	}
  
  	location = /managerids/ {
  		 rewrite ^/(.*)$ /managerids/index.php;
  	}
  
  	location ~ \.php$ {
  		fastcgi_pass unix:/run/php/php7.2-fpm.sock;
  		include snippets/fastcgi-php.conf;
  		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
  	}
  
  	location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
  		expires max;
  		log_not_found off;
  	}
  }
  ```

-  Change folder owner to current user

  ```bash
  sudo chown ubuntu:ubuntu /var/www/akatsuki/ -R
  ```

- Install composer

  ```bash
  curl -sS https://getcomposer.org/installer -o composer-setup.php
  sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
  cd /var/www/akatsuki && sudo composer install
  ```

- Login to postgres and generate database

  ```bash
  sudo -u postgres psql
  \password
  root@123
  create database awsfw;
  \c awsfw;
  \i /tmp/awsfw_latest.sql;
  \q
  ```

- Run migrations

  ```bash
  cd /var/www/akatsuki && sudo php src/app/phpmig.php migrate
  ```