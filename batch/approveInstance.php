<?php
sleep(2);

require_once "batch_loader.php";

use Common\Logger;
use Common\Mailer;
use AwsServices\Ec2;
use Akatsuki\Models\Instance;
use Akatsuki\Models\InstReq;
use Akatsuki\Models\InstReqChange;
use Akatsuki\Models\Sg;
use Akatsuki\Models\SgTemp;
use Akatsuki\Models\SgTempRules;
use Akatsuki\Models\SgEnable;
use Aws\Exception\AwsException as AwsException;

$options = [
    "cid:",
    "reqid:",
];
$params = getopt(null, $options);

$cid = $params['cid'];
$reqID = $params['reqid'];

$reqInfo = getInstanceRequestInfo($smarty, $reqID);
$ec2Client = Ec2::getInstance(null, $cid);

$updateInstReq = function ($reqID, $status = null, $note = '') {
    global $smarty;
    if ($status === INST_REQ_APPROVED) {
        InstReqChange::where("inst_req_id", $reqID)->delete();
    };
    $instReq = InstReq::find($reqID);
    if (!$instReq) {
        return false;
    }
    if (!$status) {
        $status = $instReq->approved_status;
    } elseif ($status === INST_REQ_APPROVE_FAILED && $instReq->approved_status == INST_REQ_APPROVED) {
        $status = INST_REQ_APPROVED;
    }
    $result = $instReq->changeStatus($status, $note, false);
    if ($result) {
        $emp = $instReq->empids()->first();
        if ($emp) {
            $requestInfoUrl = SERVER_URL . "/cons/request.php?id={$reqID}";
            $mailBody = "Check your request info at: {$requestInfoUrl}";

            $mailTo = $emp->email;
            $mailer = new Mailer();
            $mailer
                ->set('subject', '[Sunny View] Your instance request has been approved.')
                ->set('to', $mailTo)
                ->set('body', $mailBody);
            $mailer->setBcc($smarty->_db, $mailTo);

            $mailer->send();
        }
        return true;
    }
    return false;
};

$logError = function ($message) use ($reqID, $updateInstReq) {
    $updateInstReq($reqID, INST_REQ_APPROVE_FAILED, $message);
    exit;
};

$updateInstanceInfo = function ($instID, $reqID, $pcode) {
    $sql = sprintf("
        UPDATE instance SET inst_req_id = %s, pcode = '%s' WHERE id = '%s'
    ", $reqID, $pcode, $instID);

    return Instance::selectRaw($sql);
};

$createSecurityGroup = function ($reqID, $cid) use ($ec2Client, $logError) {
    $sgTemp = SgTemp::where('cid', $cid)
        ->where('inst_req_id', $reqID)->first();
    if (!$sgTemp) {
        return false;
    }
    if ($sgTemp->sg_id) {
        $sg = Sg::where('cid', $cid)
            ->where('id', $sgTemp->sg_id)->first();
        if (!$sg) {
            $logError(MESSAGES['ERR_SG_NOT_EXIST']);
        }
    } else {
        $sg = Sg::where('cid', $cid)
            ->where('name', $sgTemp->sg_name)
            ->where('vpcid', $sgTemp->sg_vpc)
            ->first();
        if ($sg) {
            $logError(MESSAGES['ERR_AWS_DUPLICATE_SG']);
        } else {
            $sgTemp->sg_id = $ec2Client->createSecurityGroup([
                'VpcId'      => $sgTemp->sg_vpc,
                'GroupName'  => $sgTemp->sg_name,
                'Description'=> $sgTemp->sg_desc,
            ]);
            if (!$sgTemp->sg_id) {
                $errmsg = MESSAGES['ERR_AWS_CREATE_SG'];
                $logError($errmsg);
            }
            // $sgTemp->is_created = 1;
            $sgTemp->saveWithoutEvents(function () use (&$sgTemp) {
                return $sgTemp->save();
            });

            SgEnable::enable($cid, [$sgTemp->sg_id]);
        }
    }

    $sgTempRules = (array)SgTempRules::where('cid', $cid)
        ->where('sg_temp_id', $sgTemp->id)
        ->get();
    $result = $ec2Client->updateSecurityGroupRules($sgTemp->sg_id, array_shift($sgTempRules));
    if ($result) {
        return $sgTemp->sg_id;
    }

    return false;
};

// reset note
$updateInstReq($reqID);
// update security group
Sg::updateSecurityGroups($cid);

if ((int)$reqInfo['approved_status'] == INST_REQ_APPROVED) {
    if (!(int)$reqInfo['change_state']) {
        $result['message'] = MESSAGES['ERR_PARAMS_MISSING'];
    }

    $changes = [];
    $editableFields = [
        'dept',
        'pcode',
        'using_purpose',
        'using_from_date',
        'using_till_date',
        'inst_address',
    ];
    if (!empty($reqInfo['dept_new'])) {
        foreach ($editableFields as $field) {
            if ($reqInfo[$field] != $reqInfo[$field.'_new']) {
                $changes[$field] = $reqInfo[$field.'_new'];
            };
        }
    }

    if (count($changes) == 0) {
        $updateInstReq($reqID, INST_REQ_APPROVED, '');
        exit;
    }

    $instReq = InstReq::where('id', $reqInfo['id'])->first();
    $instances = Instance::where('cid', $cid)
        ->where('inst_req_id', $reqInfo['id'])->get();
    if (!$instReq) {
        $logError(MESSAGES['ERR_DATA_NOT_FOUND']);
    }
    if (count($instances) === 0) {
        $updateInstReq($reqID, INST_REQ_APPROVED);
    }

    $instance = null;
    if ($instReq['inst_address']) {
        foreach ($instances as $instance) {
            if ($instance['publicip'] === $instReq['inst_address']) {
                break;
            }
        }
    } else {
        $instance = $instances[0];
    }

    $ec2Client = Ec2::getInstance($instance->availabilityzone, $cid);
    $publicIpAddress = !empty($changes['inst_address']) ? $changes['inst_address'] : '';
    if ($publicIpAddress) {
        $available = $ec2Client->checkAddressAvailability($publicIpAddress);
        if (!$available) {
            $logError(MESSAGES['ERR_ELASTIC_IP_UNAVAILABLE']);
        }

        $attachmentsInfo = $ec2Client->associateAddress($instance->id, $publicIpAddress);

        if (!$attachmentsInfo) {
            $logError(MESSAGES['ERR_ADDRESS_ASSOCIATE']);
        }
    } elseif ($instReq['inst_address']) {
        $success = $ec2Client->disassociateAddress($instReq['inst_address']);
        if (!$success) {
            $logError(MESSAGES['ERR_ADDRESS_DISASSOCIATE']);
        }
    }

    $ret = InstReq::beginTransaction(function () use ($instReq, $changes) {
        $instReq->setValues($changes);
        $ret = $instReq->saveWithoutEvents(function () use (&$instReq) {
            return $instReq->save();
        });
        $ret && $ret = InstReqChange::where("inst_req_id", $instReq->id)->delete();
        return $ret;
    });
    if (!$ret) {
        $logError(MESSAGES['ERR_UPDATE']);
    }
    $updateInstReq($reqID, INST_REQ_APPROVED);
    exit;
}
$BlockDeviceMappings = [];
foreach ($reqInfo['inst_bs'] as $BlockDevice) {
    $info = [
        "DeviceName" => $BlockDevice['device_name'],
        "Ebs" => [
            "VolumeType" => $BlockDevice['volumn_type'],
            "VolumeSize" => (int)$BlockDevice['volumn_size'],
            "DeleteOnTermination" => !!(int)$BlockDevice['auto_removal'],
        ]
    ];
    if ($BlockDevice['snapshot_id']) {
        $info['Ebs']['SnapshotId'] = $BlockDevice['snapshot_id'];
    }
    if ($BlockDevice['volumn_type'] === 'io1') {
        $info['Ebs']['Iops'] = (int)$BlockDevice['iops'];
    }
    $BlockDeviceMappings[] = $info;
}

$Ec2Region = $reqInfo['inst_region'];
$InstanceType = $reqInfo['inst_type'];
$InstanceCount = (int)$reqInfo['inst_count'];
$ShutdownBehavior = $reqInfo['shutdown_behavior'];
$ImageId = $reqInfo['inst_ami'];
$SubnetId = $reqInfo['inst_subnet'];
// get key, secret based on requested user
$ec2Client = \AwsServices\Ec2::getInstance($Ec2Region, $cid);
$publicIpAddress = trim($reqInfo['inst_address']);
if ($publicIpAddress) {
    $available = $ec2Client->checkAddressAvailability($publicIpAddress);
    if (!$available) {
        $logError(MESSAGES['ERR_ELASTIC_IP_UNAVAILABLE']);
    }
}

// count number of project
$sql = sprintf("SELECT COUNT(i.id) FROM instance i WHERE i.pcode = $1");
$r = pg_query_params($smarty->_db, $sql, [
    $reqInfo['pcode']
]);
$projectIndexNumber = (int)pg_fetch_result($r, 0, 0) + 1;
$instanceName = sprintf("%s-%05d", $reqInfo['pname'], $projectIndexNumber);

$instanceConfigs = [
    'ImageId'      => $ImageId,
    'InstanceType' => $InstanceType,
    'InstanceInitiatedShutdownBehavior' => $ShutdownBehavior, // stop|terminate
    'MaxCount'     => $InstanceCount,
    'MinCount'     => $InstanceCount,
    'TagSpecifications' => [
        [
            'ResourceType' => 'instance',
            'Tags' => [
                [
                    'Key'   => 'Name',
                    'Value' => $instanceName,
                ],
            ],
        ],
    ],
    'SubnetId' => $SubnetId,
];
if (!empty($reqInfo['key_name'])) {
    $instanceConfigs['KeyName'] = $reqInfo['key_name'];
}
if (count($BlockDeviceMappings)) {
    $instanceConfigs['BlockDeviceMappings'] = $BlockDeviceMappings;
}
$SecurityGroupId = $createSecurityGroup($reqID, $cid);
if (!$SecurityGroupId) {
    $logError(MESSAGES['ERR_AWS_CREATE_SG']);
}
$instanceConfigs['SecurityGroupIds'] = [$SecurityGroupId];

$instances   = null;
$logType     = LOG_TYPES['AWS'];
$logAction   = LOG_ACTIONS['INST_RUN'];
$logContents = [
    'inst_info' => $reqInfo
];
try {
    $instances = $ec2Client->runInstances($instanceConfigs);
    if (!$instances) {
        $errmsg = 'FAILED TO START INSTANCE';
        $logError($errmsg);
    };
    $instances = $instances->toArray();
    $result = [
        'result' => true,
        'data'   => $instances
    ];
    $instanceID = $instances['Instances'][0]['InstanceId'];
    $publicIpAddress && $attachmentsInfo = $ec2Client->associateAddress($instanceID, $publicIpAddress);
    Instance::updateInstances($cid, DEFAULT_AWS_REGION);
    foreach ($instances['Instances'] as $instance) {
        $updateInstanceInfo($instance['InstanceId'], $reqID, $reqInfo['pcode']);
    }
    $logContents['tbl_name'] = 'inst_req';
    Logger::info(json_encode($logContents), $logType, $logAction);
    $updateInstReq($reqID, INST_REQ_APPROVED, '');
} catch (AwsException $e) {
    $result['message'] = $logContents['err_msg'] = $e->getAwsErrorMessage();
    $updateInstReq($reqID, INST_REQ_APPROVE_FAILED, $result['message']);
    Logger::error(json_encode($logContents), $logType, $logAction);
}
