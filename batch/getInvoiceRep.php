<?php
require_once "batch_loader.php";

$smarty = new MySmarty();
$smarty->commonInfoSet();

$LABEL1 = 'Total for linked account# ';
$LABEL2 = 'Total statement amount for period ';
$LABEL3 = 'This report reflects your estimated monthly bill for activity through approximately';

$ACCTID = "";
$MSG = "";
$STATUS = "";
$COSTID = "";

// 起動パラメタのチェック
switch (count($argv)) {
    case 1:
        // 処理ターゲットの年月日取得
        // 昨日の一か月間
        $sY = date("Y", strtotime("- 1 days"));
        $sM = date("m", strtotime("- 1 days"));
        break;

    case 2:
        $offsetM = $argv[1];
        $sY = date("Y", strtotime($offsetM . " month"));
        $sM = date("m", strtotime($offsetM . " month"));
        break;

    default:
        echo "起動パラメタに誤りがあります。\n";
        echo "Usage : " . $argv[0] . " {offset month}\n";
        echo "ex) " . $argv[0] . " -1\n";
        exit -1;
}

$eD = date('t', strtotime($sY . "-" . $sM . "-" . "01"));

// 翌月のYM
$eY = date("Y", strtotime($sY . "-" . $sM . "-" . $eD . " +1 days"));
$eM = date("m", strtotime($sY . "-" . $sM . "-" . $eD . " +1 days"));

$targetDate = $sY . $sM . "01-" . $eY . $eM . "01";

// mkdir
mkdir(TMP_DIR . $sY . $sM . '-billing');


$sql = "
    SELECT 
        a.cid,
        name,
        key,
        secret,
        costacctid,
        s3bucket 
    FROM 
        users a, 
        parent b 
    WHERE 
        a.cid = b.cid 
        AND a.status = 0 
    ORDER BY a.cid;
";

$pidres = pg_query($smarty->_db, $sql);
$pidLoopLimit = pg_num_rows($pidres);

echo "INFO: " . $sY . "年" . $sM . "月分の処理を実行します。\n";
echo "INFO: " . $pidLoopLimit . "件のユーザを処理します。\n";

$sql = "
    DELETE 
        FROM invoice2
        WHERE ym='" . $sY . $sM . "'
";
$r = pg_query($smarty->_db, $sql);

echo "注意：当該月のすべての請求データをいったん削除しました。\n";


for ($pidLoop = 0; $pidLoop < $pidLoopLimit; $pidLoop ++) {
    $row = pg_fetch_row($pidres, $pidLoop);

    $PARENT_CID = $row[0];
    $PARENT_KEY = decryptIt($row[2]);
    $PARENT_SEC = $row[3];
    $PARENT_ACID = $row[4];
    $PARENT_BUCKET = $row[5];

    echo "INFO: pid=" . $PARENT_CID . " " . $row[1] . " の処理を開始します。\n";

    if ($PARENT_KEY == "" || $PARENT_SEC == "") {
        echo ">>> API認証情報が設定されていないため、コスト計算処理をスキップしました。\n";
        continue;
    }

    $config = array(
    'credentials' => [
      'key'    => $PARENT_KEY,
      'secret' => $PARENT_SEC,
    ],
    'region' => "us-east-1",
    'version' => 'latest'
    );


    $sdk = new Aws\Sdk($config);

    if ($PARENT_ACID == "") {
        // アカウントIDの取得
        try {
            $STSClient = $sdk->createMultiRegionSts();
            $r = $STSClient->getCallerIdentity();
            $PARENT_ACID = $r["Account"];
        } catch (Exception $e) {
            $MSG = "アカウントIDの取得に失敗しました。";
            echo "ACID:" . $PARENT_ACID . " " . $MSG . "\n";
            echo $e->getMessage();
            continue;
        }
        $sql = "
            UPDATE
                parent
                SET costacctid='" . $PARENT_ACID . "'
            WHERE
                pid=" . $PARENT_CID . "
        ";
        pg_query($smarty->_db, $sql);
    }


    echo "アカウントID: " . $PARENT_ACID . "\n";

    $S3Client = $sdk->createMultiRegionS3();

    // repfilenameの決定
    $repFileName = $PARENT_ACID . "-aws-billing-csv-" . $sY . "-"
        . $sM . ".csv";
    // tmpファイル名の決定
    $tmpFileName = TMP_DIR . $sY . $sM . "-billing/"
        . $PARENT_ACID . "-aws-billing-csv-" . $sY . "-" . $sM . ".csv";

    echo "--- バケット名:" . $PARENT_BUCKET . "\n";
    echo "--- ターゲットファイル名:" . $repFileName . "\n";
    echo "--- 保存先ファイル名:" . $tmpFileName . "\n";

    try {
        $r = $S3Client->getObject(
            [
            'Bucket' => $PARENT_BUCKET,
            'Key' => $repFileName,
            'SaveAs' => $tmpFileName
            ]
        );
    } catch (Exception $e) {
        $MSG = "請求レポートファイルの取得に失敗しました。";
        echo $e->getMessage();
        continue;
    }


    $file = new SplFileObject($tmpFileName);
    $file->setFlags(SplFileObject::READ_CSV);

    $estimateFlag = false;
    foreach ($file as $line) {
        if (!is_null($line[0])) {
            if (0 == strncmp($line[18], $LABEL3, strlen($LABEL3))) {
            // estimated mark
                $estimateFlag = true;
                break;
            }
        }
    }
    if ($estimateFlag == true) {
        echo "**** 未確定データです。スキップします。\n";
        continue;
    }

    // ファイルポインタを先頭に戻す
    $file->fseek(0, SEEK_SET);
    $sub_usage = $sub_tax = $sub_credit = $sub_total = 0;
 
    foreach ($file as $line) {
        if (!is_null($line[0])) {
            if (0 == strncmp($line[18], $LABEL1, strlen($LABEL1))) {
                // ユーザーごと請求行を見つけた場合
                $tmp=array();
                preg_match("/^" . $LABEL1 . "([0-9]+) \((.*)\)/", $line[18], $tmp);
                $acid = $tmp[1];
                $usage = round($line[24], 2);
                $credit = round($line[25], 2);
                $tax   = round($line[26], 2);
                $total = round($line[28], 2);
                echo "--- ACID:" . $acid . " " . $tmp[2] . "\n";
                echo "--- Usage:" . $usage
                    . " Credit:" . $credit
                    . " Tax:" . $tax
                    . " Total:" . $total . "\n";

                $sql = "
                    SELECT
                        companyname,
                        dept,
                        position,
                        zip,
                        address1,
                        address2,
                        address3,
                        name,
                        tel,
                        email,
                        howtosend,
                        payday,
                        internaluse
                    FROM
                        invoice_addr
                    WHERE costacctid='" . $acid . "'
                ";
                $r = pg_query($smarty->_db, $sql);
                if (pg_num_rows($r) == 0) {
                    // invoice_addrテーブルにINSERT
                    $internaluse = "false";
                    $sql = "
                        INSERT
                            INTO invoice_addr 
                            (
                                costacctid, 
                                companyname, 
                                payeracctid,
                                internaluse,
                                howtosend,
                                payday
                            )
                            VALUES
                            (
                                '" . $acid . "',
                                '" . $tmp[2] . "',
                                '" . $PARENT_ACID . "',
                                false,
                                'PAPER',
                                30
                             )
                    ";
                    $r = pg_query($smarty->_db, $sql);

                    $companyname = $tmp[2];
                    $dept = $position = $zip = $address1 = $address2
                      = $address3 = $name = $tel = $email = "";
                    $payday = 30;
                    $internaluse = 'false';
                    $howtosend = "PAPER";
                } else {
                    $row = pg_fetch_row($r, 0);
                    $companyname = $row[0];
                    $dept        = $row[1];
                    $position    = $row[2];
                    $zip         = $row[3];
                    $address1    = $row[4];
                    $address2    = $row[5];
                    $address3    = $row[6];
                    $name        = $row[7];
                    $tel         = $row[8];
                    $email       = $row[9];
                    $howtosend   = $row[10];
                    if ($row[11] == "") {
                          $payday = 30;
                    } else {
                          $payday = $row[11];
                    }
                    if ($row[12] == "t") {
                        $internaluse = 'true';
                    } else {
                        $internaluse = 'false';
                    }
                }
                
                // invoice2にINSERT
                $sql = "
                    INSERT
                        INTO invoice2
                        (
                            costacctid,
                            ym,
                            companyname,
                            usageusd,
                            creditusd,
                            taxusd,
                            totalusd,
                            payeracctid,
                            calcdate,
                            dept,
                            position,
                            zip,
                            address1,
                            address2,
                            address3,
                            name,
                            tel,
                            email,
                            howtosend,
                            payday,
                            internaluse
                        ) 
                        VALUES 
                        ( 
                            '" . $acid . "',
                            '" . $sY . $sM . "',
                            '" . $companyname . "',
                            " . $usage . ",
                            " . $credit . ",
                            " . $tax . ",
                            " . $total . ",
                            '" . $PARENT_ACID . "',
                            now(),
                            '" . $dept . "',
                            '" . $position . "',
                            '" . $zip . "',
                            '" . $address1 . "',
                            '" . $address2 . "',
                            '" . $address3 . "',
                            '" . $name . "',
                            '" . $tel . "',
                            '" . $email . "',
                            '" . $howtosend . "',
                            " . $payday . ",
                            " . $internaluse . "
                        )
                ";
                $r = pg_query($smarty->_db, $sql);

                $sub_usage += $usage;
                $sub_credit += $credit;
                $sub_tax += $tax;
                $sub_total += $total;
            } elseif (0 == strncmp($line[18], $LABEL2, strlen($LABEL2))) {
            // ground total
                $usage  = round($line[24], 2);
                $credit = round($line[25], 2);
                $tax    = round($line[26], 2);
                $total  = round($line[28], 2);

                $sub_usage = round($usage - $sub_usage, 2);
                $sub_credit = round($credit - $sub_credit, 2);
                $sub_tax   = round($tax   - $sub_tax, 2);
                $sub_total = round($total - $sub_total, 2);

                echo "--- ground total ----\n";
                echo " Usage:" . $sub_usage
                    . " Credit:" . $sub_credit
                    . " Tax:" . $sub_tax
                    . " Total:" . $sub_total . "\n";
            }
        }
    }


    echo ">>> 正常終了\n";
} // end for CID loop
