<?php
ob_start();

require_once(dirname(__DIR__) . "/src/config/consts.php");
require_once(dirname(__DIR__) . "/src/config/messages.php");

require_once(COMMON_PATH . "/CommonClass.class.php");
require_once(COMMON_PATH . "/common.php");
require_once(COMMON_PATH . "/auth.php");

require_once(ROOT_PATH . "/src/autoloader.php");
require_once(ROOT_PATH . "/vendor/autoload.php");

require_once(dirname(__DIR__) . "/src/managerids/etp_module.php");

new Akatsuki\Models\Database();

$smarty = new MySmarty();
$fileLogger = new \Common\FileLogger('general');
register_shutdown_function(function () use ($fileLogger, $smarty) {
    $output = trim(ob_get_flush(), " \r\n\t");
    if ($output) {
        $fileLogger->info($output);

        $options = [
            "cid:",
            "reqid:",
        ];
        $params = getopt(null, $options);
        $cid = !empty($params['cid']) ? $params['cid'] : 0;
        $reqID = !empty($params['reqid']) ? $params['reqid'] : 0;
        if ($cid > 0 && $reqID > 0) {
            $sql = sprintf("
                UPDATE inst_req
                SET approved_status = %d
                WHERE id = %s
                    AND cid = %s
                    AND approved_status <> %s;
            ", INST_REQ_APPROVE_FAILED, $reqID, $cid, INST_REQ_APPROVED);
            $fileLogger->info($sql);
            pg_query($smarty->_db, $sql);
        }
    } else {
        $fileLogger->info('No error');
    }
});
