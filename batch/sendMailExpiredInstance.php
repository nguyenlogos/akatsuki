<?php
require_once('batch_loader.php');
use Akatsuki\Models\Users;
use Akatsuki\Models\Configs;
use Illuminate\Database\Capsule\Manager as DB;
use Common\Mailer;

$smarty = new MySmarty(false);
$smarty->commonInfoSet();

$opts = [
    "cids::"
];
$options = getopt("", $opts);

$query = Users::select('cid');
if (!empty($options['cids'])) {
    $cids = explode(',', $options['cids']);
    $query->whereIn('cid', $cids);
}
$users = $query->orderBy('cid')->get();
foreach ($users as $user) {
    $cid = $user->cid;
    $config = Configs::where('cid', $cid)
        ->where('instance_expires', '>', 0)->get()->first();
    if (!$config) {
        continue;
    }
    $expiredPeriod = (int)$config->instance_expires;
    $sql = "
        SELECT
            i.name AS instname,
            i.id AS instid,
            ir.id AS reqid,
            e.email AS empmail,
            DATE_PART('day', ir.using_till_date::date - now()) AS till_expired
        FROM
            instance i
        INNER JOIN
            inst_req ir
            ON
                ir.id = i.inst_req_id
            AND
            (
                ir.using_till_date::DATE <= now()
                OR
                ir.using_till_date::DATE - now() <= '{$expiredPeriod} days'::INTERVAL
            )
            AND
                ir.expired_notified = 0
        INNER JOIN
            emp e
            ON
                ir.cid = e.cid
                AND
                ir.empid = e.empid
        WHERE
            i.cid = {$cid}
        AND
            i.inst_req_id > 0
    ";
    $instReqs = DB::select($sql);
    if (count($instReqs)) {
        $requests = [];
        foreach ($instReqs as $instReq) {
            $instReq = (array)$instReq;
            $email = $instReq['empmail'];
            if (!array_key_exists($email, $requests)) {
                $requests[$email] = [];
            }
            $requests[$email][] = $instReq;
        }
        $mailer= new Mailer();
        $mailSubject = "[Sunny View] Your intance(s) are about to expired";
        $mailer->set('subject', $mailSubject);
        foreach ($requests as $email => $reqList) {
            $mailBody = [];
            foreach ($reqList as $reqInfo) {
                $tillExpired = (int)$reqInfo['till_expired'];
                if ($tillExpired > 0) {
                    $message = "Expired in {$tillExpired} day(s).";
                } else {
                    $tillExpired = abs($tillExpired);
                    $message = "Expired {$tillExpired} day(s) ago.";
                }
                $mailBody[] = "\n{$reqInfo['instname']}({$reqInfo['instid']}) - {$message}";
            }
            $mailBody = implode("", $mailBody);
            $mailer
                ->set('charset', 'iso-2022-jp')
                ->set('to', $email)
                ->set('body', $mailBody);
            $mailer->setBcc($smarty->_db, $email);
            $resultMail = $mailer->send();
            if ($resultMail) {
                $ids = implode(",", array_column($reqList, 'reqid'));
                $sql = "UPDATE inst_req SET expired_notified = 1 WHERE id IN($ids)";
                $result = DB::update($sql);
            }
        }
    };
}
