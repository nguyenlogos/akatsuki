<?php
require_once('batch_loader.php');
use Akatsuki\Models\Users;
use Akatsuki\Models\CwLogs;
use AwsServices\CWLogs as CWEventLogs;

$opts = [
    "cids::"
];
$options = getopt("", $opts);

$query = Users::select('cid', 'key', 'secret')
                ->whereNotNull('key')
                ->whereNotNull('secret');
if (!empty($options['cids'])) {
    $cids = explode(',', $options['cids']);
    $query->whereIn('cid', $cids);
}
$users = $query->orderBy('cid')->get()->toArray();

foreach ($users as $user) {
    $clientConfig = [
        'credentials' => [
            'key'     => decryptIt($user['key']),
            'secret'  => $user['secret']
        ],
        'region'      => 'ap-northeast-1', // Tokyo
        'version'     => 'latest',
    ];

    try {
        $cwLogs = new CWEventLogs($clientConfig);
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
        echo sprintf("%s (cid=%d)", $errorMessage, $user['cid']) . PHP_EOL;
        continue;
    }

    $logGroupName = '/aws/lambda/ids-ebs-backup';

    $lastBackupTime = CwLogs::where('cid', $user['cid'])->max('stream_time');
    $lastBackupTime = strtotime($lastBackupTime);
    $streams = [];

    $skipOldStreams = false;
    do {
        $logStreams = $cwLogs->getLogStreams($logGroupName, 5, $nextToken);
        foreach ($logStreams as $stream) {
            if ($lastBackupTime) {
                $lastEventTime = (int)($stream['lastEventTimestamp']/1000);
                if ($lastEventTime <= $lastBackupTime) {
                    $skipOldStreams = true;
                    break;
                }
            }
            $streams[] = $stream;
        }
    } while ($nextToken && !$skipOldStreams);

    if (count($streams)) {
        $stream = array_shift($streams);
        $lastEventTime = (int)($stream['lastEventTimestamp']/1000);
        $lastAvailTime = time() - CW_LOGS_UPDATE_INTERVAL;

        if ($lastEventTime < $lastAvailTime) {
            $streams[] = $stream;
        }
    }

    foreach ($streams as $stream) {
        $nextForwardToken = null;
        $eventsCount = 0;
        echo PHP_EOL . "=== Fetching log events for {$stream['logStreamName']} ===";
        do {
            $logEvents = $cwLogs->getLogEvents($logGroupName, $stream['logStreamName'], $nextForwardToken);
            $count = count($logEvents);
            if ($count === 0) {
                break;
            }
            $eventsCount += $count;
            $request = [];
            foreach ($logEvents as $event) {
                $request[] = $event;
                if (preg_match('/^REPORT RequestId:/', $event['message'])) {
                    CwLogs::insertLogs($user['cid'], $stream, $request);
                    $request = [];
                }
            }
        } while (true);
        echo PHP_EOL . "=== Total events: {$eventsCount} ===";
        echo PHP_EOL;
    }
}
