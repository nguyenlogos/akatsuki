<?php
require_once "batch_loader.php";

$smarty = new MySmarty();
$smarty->commonInfoSet();

$label = array(
    'lineItem/LineItemType',
    'lineItem/UsageStartDate',
    'lineItem/UsageEndDate',
//    'lineItem/ProductCode',
    'product/ProductName',
    'lineItem/UsageType',
    'lineItem/Operation',
    'lineItem/ResourceId',

    'lineItem/UsageAmount',
    'lineItem/CurrencyCode',
    'lineItem/UnblendedCost',
    'lineItem/BlendedCost',
    'lineItem/LineItemDescription',
    'product/instanceType',
    'product/location',

    'product/memory',
    'product/networkPerformance',
    'product/normalizationSizeFactor',
    'product/operatingSystem',
    'product/operation',
    'product/physicalProcessor',
    'product/preInstalledSw',

    'product/processorArchitecture',
    'product/processorFeatures',
    'product/region',
    'product/servicename',
    'product/transferType',
    'bill/BillingEntity',
    'pricing/publicOnDemandCost',

    'pricing/publicOnDemandRate',
    'pricing/term',
    'pricing/unit',
    'lineItem/UsageAccountId',
    'product/servicecode',
    'product/productFamily',
//    'product/databaseEngine',
    'lineItem/UnblendedRate',
    'lineItem/BlendedRate'
);

// S=String D=Digit
$datatype = array(
    'S','S','S','S','S','S','S',
    'D','S','D','D','S','S','S',
    'S','S','S','S','S','S','S',
    'S','S','S','S','S','S','D',
    'D','S','S','S','S','S','D','D'
);

$ACCTID = "";
$MSG = "";
$STATUS = "";
$COSTID = "";
$COSTTYPE = "DAILY";

// 起動パラメタのチェック
switch (count($argv)) {
    case 1:
        // 処理ターゲットの年月日取得
        // 昨日の一か月間
        $sY = date("Y", strtotime("- 1 days"));
        $sM = date("m", strtotime("- 1 days"));
        break;

    case 2:
        $offsetM = $argv[1];
        $sY = date("Y", strtotime($offsetM . " month"));
        $sM = date("m", strtotime($offsetM . " month"));
        break;

    default:
        echo "起動パラメタに誤りがあります。\n";
        echo "Usage : " . $argv[0] . " {offset month}\n";
        echo "ex) " . $argv[0] . " -1\n";
        exit -1;
}

$eD = date('t', strtotime($sY . "-" . $sM . "-" . "01"));

// 翌月のYM
$eY = date("Y", strtotime($sY . "-" . $sM . "-" . $eD . " +1 days"));
$eM = date("m", strtotime($sY . "-" . $sM . "-" . $eD . " +1 days"));

$targetDate = $sY . $sM . "01-" . $eY . $eM . "01";

// mkdir
mkdir(TMP_DIR . $sY . $sM);

echo "INFO: " . $sY . "年" . $sM . "月分の処理を実行します。\n";

$sql = "SELECT cid,key,secret,companyname FROM users WHERE status = 0 ORDER BY cid;";
$cidres = pg_query($smarty->_db, $sql);
$cidLoopLimit = pg_num_rows($cidres);

echo "INFO: " . $cidLoopLimit . "件のユーザを処理します。\n";

for ($cidLoop = 0; $cidLoop < $cidLoopLimit; $cidLoop ++) {
    $row = pg_fetch_row($cidres, $cidLoop);

    $TARGET_CID = $row[0];
    $TARGET_KEY = decryptIt($row[1]);
    $TARGET_SEC = $row[2];

    echo "INFO: cid=" . $TARGET_CID . " " . $row[3] . " の処理を開始します。\n";

    if ($TARGET_KEY == "" || $TARGET_SEC == "") {
        echo ">>> API認証情報が設定されていないため、コスト計算処理をスキップしました。\n";
        continue;
    }

    $config = array(
    'credentials' => [
      'key'    => $TARGET_KEY,
      'secret' => $TARGET_SEC,
    ],
    //    'region' => "ap-northeast-1",
    'region' => "us-east-1",
    'version' => 'latest'
    );


    $sdk = new Aws\Sdk($config);

    // アカウントIDの取得
    try {
        $STSClient = $sdk->createMultiRegionSts();
        $r = $STSClient->getCallerIdentity();
        $ACCTID = $r["Account"];
    } catch (Exception $e) {
        $MSG = "アカウントIDの取得に失敗しました。";
        echo "CID:" . $TARGET_CID . " " . $MSG . "\n";
        echo $e->getMessage();
        continue;
    }
    echo "アカウントID: " . $ACCTID . "\n";
    // レポート定義の取得
    $CUClient = $sdk->createCostandUsageReportService();

    try {
        $r = $CUClient->DescribeReportDefinitions();
    } catch (Exception $e) {
        $MSG = "レポート定義の取得に失敗しました。";
        echo "CID:" . $TARGET_CID . " " . $MSG . "\n";
        echo $e->getMessage();
        continue;
    }
//    var_dump($r["ReportDefinitions"]);

    $repName = $repBucket = $repPrefix = $repRegion = "";

    for ($i = 0; $i < count($r["ReportDefinitions"]); $i++) {
        $tmp = $r["ReportDefinitions"][$i];
        if ($tmp["TimeUnit"] == "DAILY"
            && $tmp["Compression"] == "GZIP"
            && $tmp["AdditionalSchemaElements"][0] == "RESOURCES"
            && $tmp["Format"] == "textORcsv"
        ) {
            // found correct report.
            $repName = $tmp["ReportName"];
            $repBucket = $tmp["S3Bucket"];
            $repPrefix = $tmp["S3Prefix"];
            $repRegion = $tmp["S3Region"];
            if ($repName == "sunnyview") {
                break;
            }
        }
    }

    if ($repName == "") {
        $MSG = "正しいレポート定義が見つかりませんでした。";
        $MSG .="日別,「リソースIDを含める」,GZIPを設定してください。";
        echo $MSG . "\n";
        continue;
    }

    echo ">>> レポート定義を見つけました。(repname:" .
        $repName . " bucket:" . $repBucket . " prefix:" .
        $repPrefix . " region:" . $repRegion . "\n";


    $S3Client = $sdk->createMultiRegionS3();

    //$S3Client->registerStreamWrapper();
    // s3:で直接エンコードしようかと思ったけど、ファイルサイズが巨大な
    // 場合があるので、いったんtmpファイルに保存して1行ずつ集計することにします

    // tmpファイル名の決定
    $tmpFileName = TMP_DIR . $sY . $sM . "/" . $ACCTID . ".Manifest.json";

    // if ($repPrefix != "") { $repBucket .= "/" . $repPrefix; }
    // if ($repName != "") { $repBucket .= "/" . $repName; }
    // $repBucket .= "/" . $targetDate;

    $repFilePath = "";
    if ($repPrefix != "") {
        $repFilePath .= $repPrefix . "/";
    }
    if ($repName != "") {
        $repFilePath .= $repName . "/";
    }
    $repFilePath .= $targetDate . "/";

    $repFileName = $repName . "-" . "Manifest.json";

    echo "Bucket:" . $repBucket . "\n";
    echo "Key:" . $repFilePath . $repFileName . "\n";

    try {
        $r = $S3Client->getObject(
            [
            'Bucket' => $repBucket,
            'Key' => $repFilePath . $repFileName,
            'SaveAs' => $tmpFileName
            ]
        );
    } catch (Exception $e) {
        $MSG = "コスト情報ファイルの取得に失敗しました。";
        $MSG .= "ファイル名：" . $repBucket . " " . $repFilePath . $repFileName;
        echo $MSG . "\n";
        echo $e->getMessage();
        continue;
    }

    $decoded = json_decode($r['Body'], true);

    $COSTID = $decoded["assemblyId"];

    //var_dump($decoded);

    // マージ出力ファイルのオープン
    $outFileName = TMP_DIR . $sY . $sM . "/" .  $ACCTID . "-all.csv"  ;
    $fp_w = fopen($outFileName, "w");
    if ($fp_w == false) {
        echo "ERROR:" . $outFileName . "のオープンに失敗しました。fopen()\n";
        continue;
    }

    // レポートファイルが複数あるのでループ
    for ($i = 0; $i < count($decoded["reportKeys"]); $i++) {
        echo "Key:" . $decoded["reportKeys"][$i] . "\n";

        $tmpFileName = TMP_DIR . $sY . $sM . "/" .  $ACCTID . "-"
            . ($i+1);

        try {
            $r = $S3Client->getObject(
                [
                'Bucket' => $repBucket,
                'Key' => $decoded["reportKeys"][$i],
                'SaveAs' => ($tmpFileName . ".csv.gz")
                ]
            );
        } catch (Exception $e) {
            $MSG = "コスト情報ファイルのダウンロードに失敗しました。ファイル名：" . $decoded["reportKeys"][$i];
            echo $MSG . "\n";
            echo $e->getMessage();
            continue;
        }
        echo ">>> " . $tmpFileName . ".csv.gzの保存に成功しました。\n";

        $fp = gzopen($tmpFileName . ".csv.gz", "r");
        if ($fp == false) {
            echo "ERROR:" . $tmpFileName . ".csv.gzのオープンに失敗しました。gzopen()\n";
            continue;
        }

        while (($line = gzgets($fp)) != false) {
            fputs($fp_w, $line);
        }
        gzclose($fp);
    }
    fclose($fp_w);

    // CSVをcostテーブルに取り込み
    // ラベルから配列インデックスを取得
    $file = new SplFileObject($outFileName);
    $file->setFlags(SplFileObject::READ_CSV);

    $labelpos = array();
    $firstline = $file->fgetcsv();
    for ($i=0; $i<count($label); $i++) {
        for ($j=0; $j<count($firstline); $j++) {
            if ($firstline[$j] == $label[$i]) {
                array_push($labelpos, $j);
                break;
            }
            if ($j >= count($firstline)) {
                $MSG = "コスト情報ファイルの処理中にエラーが発生しました。"
                    . "(LABEL NOT FOUND : " . $label[$i] . ")";
                echo $MSG . "\n";
                continue;
            }
        }
    }



    $sql = "
        DELETE FROM cost
            WHERE cid = " . $TARGET_CID
        . " AND to_char(usagestartdate,'YYYYMM') = '" . $sY . $sM . "'";
    $r = pg_query($smarty->_db, $sql);

    foreach ($file as $line) {
        if (!is_null($line[0])) {
            if (is_numeric($line[22])) {
                $sql = "INSERT INTO cost VALUES (" . $TARGET_CID;
                for ($i = 0; $i < count($labelpos); $i++) {
                    $sql .= ",";
                    $idx = $labelpos[$i];
                    if (is_null($line[$idx]) || $line[$idx] == '') {
                        $sql .= "null";
                    } else {
                        if ($datatype[$i] == 'S') {
                            $sql .= "'";
                        }
                        $sql .= $line[$idx];
                        if ($datatype[$i] == 'S') {
                            $sql .= "'";
                        }
                    }
                }
                $sql .= ",now());";
                $r = pg_query($smarty->_db, $sql);
            }
        }
    }
    echo ">>> 正常終了\n";
} // end for CID loop
