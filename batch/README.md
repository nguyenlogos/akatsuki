AKATSUKI
====

## 1. Requirement:

## 2. TODO


## 3. Dependencies

- Postgre
- php7.1

## 4. Usage:

- Run service:

    ```bash
    
	#Install Docker
        - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        - sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        - sudo apt-get update
        - apt-cache policy docker-ce
        - sudo apt-get install -y docker-ce
        
    # Install Docker compose
        - sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
        - sudo chmod +x /usr/local/bin/docker-compose
        
    # Start project
        - docker-compose build
        - docker-compose up -d
        
    ```
    
- Migration step:

    ```bash
    
	#Create migration
        - bin/phpmig.php generate AddRatingToLolCats
        - change file migrations/20111101000144_AddRatingToLolCats.php from `use Phpmig\Api\PhpmigApplication;` to `use ec\bin\Migration;`
        
    # Up migration
        - bin/phpmig.php migrate

        
    # Down migration
        - bin/phpmig.php rollback -t 20111101000144
        - bin/phpmig.php down 20111101000144

        
    ```
    


## 5. Control the services:

- Control Service:

    ```bash
        #up
        - docker-compose up -d
        #down
        - docker-compose down
        #restart
        - docker-composer restart
    ```
    
## 6. Note:

- Dev url: http://akatsuki_dev.local

## 7. Reference

