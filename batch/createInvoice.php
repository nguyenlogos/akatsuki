<?php
/**
  Create invoice batch
 **/

require_once "batch_loader.php";
require_once "calcCostDetail.php";

require_once LIBS_PATH . "/tcpdf/tcpdf.php";
require_once LIBS_PATH . "/tcpdf/fpdi.php";
require_once LIBS_PATH . "/tcpdf/fpdf_merge.php";

$smarty = new MySmarty(false);
$smarty->commonInfoSet();

// 起動パラメタのチェック
switch (count($argv)) {
    case 1:
        // 処理ターゲットの年月日取得
        // デフォルトは先月
        $Y     = date("Y", strtotime("- 1 month"));
        $M     = date("m", strtotime("- 1 month"));
        $endD  = date("t", strtotime("- 1 month"));
        $Y2    = date("y", strtotime("- 1 month"));

        break;

    case 3:
        $Y      = $argv[1];
        $M      = $argv[2];
        $endD   = date("t", strtotime($Y . "-" . $M . "-01"));
        $Y2     = date("y", strtotime($Y . "-" . $M . "-01"));
        if ($endD != "") {
            break;
        }
        // no break
    default:
        echo "起動パラメタに誤りがあります。\n";
        echo "Usage : " . $argv[0] . " {YYYY MM}\n";
        echo "ex) " . $argv[0] . " 2018 1\n";
        exit -1;
}
$M = str_pad($M, 2, 0, STR_PAD_LEFT);

$startDate = $Y . "-" . $M . "-01";
$endDate = date("Y-m-d", strtotime("+ 1 month", strtotime($startDate)));
$nextY = date("Y", strtotime("+ 1 month", strtotime($startDate)));
$nextM = date("m", strtotime("+ 1 month", strtotime($startDate)));

// 計算用レートの取得
$sql = "SELECT 
            dy 
            FROM rate 
            WHERE 
                ym = '" . $Y . "/" . $M . "'";
$logs[] = $sql;
$r   = pg_query($smarty->_db, $sql);

if (pg_num_rows($r) == 0) {
    echo "レートがまだ確定していないため請求書が生成できませんでした。\n";
    exit(-1);
}

$row = pg_fetch_row($r, 0);
$rate1M = $row[0];

//// Invoice2の顧客情報を最新のinvoice_addrを使って更新
$sql = "
    UPDATE
        invoice2
        SET 
            zip = a.zip,
            address1 = a.address1,
            address2 = a.address2,
            address3 = a.address3,
            companyname = a.companyname,
            dept = a.dept,
            position = a.position,
            name = a.name,
            payday = a.payday,
            internaluse = a.internaluse,
            usagejpy = ceil(totalusd * " . $rate1M . ") 
                - ceil(taxusd * " . $rate1M . ") 
                - ceil(creditusd * " . $rate1M . "),
            creditjpy = ceil(creditusd * " . $rate1M . "),
            taxjpy = ceil(taxusd * " . $rate1M . "),
            totaljpy = ceil(totalusd * " . $rate1M . "),
            rate = " . $rate1M . ",
            refnoserial = null,
            filename = null,
            calcdate = now()
        FROM
            invoice_addr a
        WHERE
            invoice2.ym = '" . $Y . $M . "'
            AND invoice2.costacctid = a.costacctid
";
pg_query($smarty->_db, $sql);
//// Invoiceリストの取得
$sql = "
    SELECT list.*, p.cnt
        FROM
        (
            SELECT a.zip,
                a.address1,
                a.address2,
                a.address3,
                a.companyname,
                a.dept,
                a.position,
                a.name,
                a.payday,
                a.costacctid costid,
                a.usageusd,
                a.creditusd,
                a.taxusd,
                a.totalusd,
                a.usagejpy,
                a.creditjpy,
                a.taxjpy,
                a.totaljpy
            FROM 
                invoice2 a,
                invoice_addr b
            WHERE
                a.costacctid = b.costacctid
                AND a.ym = '" . $Y . $M . "'
                AND a.internaluse = false
                AND a.totalusd != 0
                AND b.parentacctid is null
            ORDER BY companyname
        ) list
        LEFT OUTER JOIN
        (
            SELECT parentacctid, count(parentacctid) cnt FROM invoice_addr
               GROUP BY parentacctid
               ORDER BY parentacctid
        ) AS p
        ON list.costid = p.parentacctid
";
$logs[] = $sql;
$custR  = pg_query($smarty->_db, $sql);
echo pg_num_rows($custR) . "件のデータを処理します。\n";

for ($i = 0; $i < pg_num_rows($custR); $i++) {
    $custInfo = pg_fetch_row($custR, $i);

    echo "INFO: " . $custInfo[4]
        . " の請求書を生成します。\n";

    $TARGET_ACID = $custInfo[9];

    // 支払い期限の設定
    switch ($custInfo[8]) {
        case 60:
            $tmpDate = strtotime($Y . "-" . $M . "-01 +2 month");
            break;
        case 40:
            $tmpDate = strtotime($Y . "-" . $M . "-01 +2 month");
            break;
        default:
            $tmpDate = strtotime($Y . "-" . $M . "-01 +1 month");
            break;
    }

    $payday = date("Y", $tmpDate) . "/";
    $payday .= str_pad(date("m", $tmpDate), 2, 0, STR_PAD_LEFT) . "/";
    if ($custInfo[8] != 40) {
        $payday .= date("t", $tmpDate);
    } else {
        $payday .= "10";
    }

    // 有料契約日数の取得
//  $chargeDays = getChargedDays($smarty, $TARGET_ACID, $Y, $M, $nextY, $nextM);
    // 集計結果を取得してinvoice2テーブルに記録。請求額の確定。
//
//    if ($chargeDays != 0) {
        // 利用料と消費税の切り上げ誤差を調整
//        $idsFeeTotal    = ceil($subTotalUnblend * 100 * 0.05) / 100;
        // 日割り計算
//        $idsFeeTotal    = ceil($idsFeeTotal * $chargeDays / $endD * 100) / 100;
//        $idsFeeTotalJPY = ceil($idsFeeTotal * $rate1M);
//    } else {
//        $idsFeeTotal    = 0;
//        $idsFeeTotalJPY = 0;
//    }

    $usage_USD  = $custInfo[10];
    $credit_USD = $custInfo[11];
    $tax_USD    = $custInfo[12];
    $total_USD  = $custInfo[13];

    $usage_JPY  = $custInfo[14];
    $credit_JPY = $custInfo[15];
    $tax_JPY    = $custInfo[16];
    $total_JPY  = $custInfo[17];

    if ($custInfo[18] != "") {
    // 合算対象あり
        $sql = "
            SELECT 
                sum(usageusd),
                sum(creditusd),
                sum(taxusd),
                sum(totalusd),
                sum(usagejpy),
                sum(creditjpy),
                sum(taxjpy),
                sum(totaljpy)
            FROM
                invoice2 a,
                invoice_addr b
            WHERE
                b.parentacctid = '" . $custInfo[9] . "'
                AND a.costacctid = b.costacctid
                AND a.ym = '" . $Y . $M . "'
                AND a.internaluse = false
        ";
        $r_total  = pg_query($smarty->_db, $sql);
        $row_total = pg_fetch_row($r_total, 0);

        $usage_USD  += $row_total[0];
        $credit_USD += $row_total[1];
        $tax_USD    += $row_total[2];
        $total_USD  += $row_total[3];

        $usage_JPY  += $row_total[4];
        $credit_JPY += $row_total[5];
        $tax_JPY    += $row_total[6];
        $total_JPY  += $row_total[7];
    }

    // 請求書番号の取得
    $sql = "BEGIN;";
    $r   = pg_query($smarty->_db, $sql);

    $sql = "SELECT refnoserial FROM invoice2
            WHERE costacctid = '" . $TARGET_ACID . "' and ym = '" . $Y . $M . "'";
    $logs[] = $sql;
    $r   = pg_query($smarty->_db, $sql);

    $ser = "";
    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);
        if ($row[0] != "") {
            $ser = $row[0];
        }
    }
    if ($ser == "") {
        $sql = "SELECT max(refnoserial)+1 FROM invoice2 WHERE ym = '" . $Y . $M . "'";
        $logs[] = $sql;
        $r   = pg_query($smarty->_db, $sql);
        $row = pg_fetch_row($r, 0);

        if ($row[0] == "") {
            $ser = 1;
        } else {
            $ser = $row[0];
        }
    }
    $sql = "UPDATE
                invoice2
                SET 
                    refnoserial = " . $ser . ",
                    filename = '" . $TARGET_ACID . "-" . $Y . $M . ".pdf'
                WHERE
                    costacctid='" . $TARGET_ACID . "'
    ";
    $r   = pg_query($smarty->_db, $sql);

    $sql = "COMMIT;";
    $r   = pg_query($smarty->_db, $sql);

    // 請求書の生成
    $pdf = new FPDI();
    $pagecount = $pdf->setSourceFile(COMMON_PATH . "/template.pdf");

    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->setAutoPageBreak(false);
    $pdf->setFontSubsetting(true);
    $pdf->SetFont('ipaexm', '', 8);

    $pdf->AddPage();

    $importPage = $pdf->importPage(1);
    $pdf->useTemplate($importPage, 0, 0);

    //--------------------------------
    $page_info = $pdf->getPageDimensions();
    $row_position = $page_info['tm'];

    $pdf->SetXY(1, $row_position * 2);

    // 郵便番号から住所まで出力
    if ($custInfo[0] != "") {
        $pdf->Write(1, "〒" . $custInfo[0]);
        $pdf->Ln();
    }
    if ($custInfo[1] != "") {
        $pdf->Write(1, $custInfo[1]);
        $pdf->Ln();
    }
    if ($custInfo[2] != "") {
        $pdf->Write(1, $custInfo[2]);
        $pdf->Ln();
    }
    if ($custInfo[3] != "") {
        $pdf->Write(1, $custInfo[3]);
        $pdf->Ln();
    }
    $pdf->Ln();
    if ($custInfo[4] != "") {
        $pdf->Write(1, $custInfo[4]);
        $pdf->Ln();
    }
    if ($custInfo[5] != "") {
        $pdf->Write(1, $custInfo[5]);
        $pdf->Ln();
    }
    if ($custInfo[6] != "") {
        $pdf->Write(1, $custInfo[6]);
        $pdf->Ln();
    }
    $pdf->Write(1, $custInfo[7] . " 様");
    $pdf->Ln();


    // 件名に年月を表示
    $pdf->SetXY(85, 108);
    $pdf->Cell(1, 0, $Y . "年" . $M . "月分", 0, 0, 'L');


    // 発行日
    $pdf->SetXY(160, 84);
    $pdf->Cell(38, 0, $Y . "/" . $M . "/" . $endD, 0, 0, 'L');
    $pdf->SetXY(160, 89);
    $pdf->Cell(
        38,
        0,
        "C" . $Y2 . $M . str_pad($ser, 5, 0, STR_PAD_LEFT),
        0,
        0,
        'L'
    );
    $pdf->SetXY(160, 94);
    $pdf->Cell(38, 0, $payday, 0, 0, 'L');

    // ご請求額を表示
//    if ($chargeDays != 0) {
        $pdf->SetXY(150, 108);
        $pdf->Cell(
            38,
            0,
            number_format($total_JPY) . "円",
            0,
            0,
            'C'
        );
//    }
    // 利用料（ドル）の表示
    $pdf->SetXY(117, 126);
    $pdf->Cell(
        17,
        0,
        "$" . number_format($total_USD, 2),
        0,
        0,
        'R'
    );

    $pdf->SetXY(134, 126);
    $pdf->Cell(28, 0, number_format($rate1M, 2) . "円/ドル", 0, 0, 'R');

    $pdf->SetXY(162, 126);
    $pdf->Cell(
        38,
        0,
        number_format($total_JPY) . "円",
        0,
        0,
        'R'
    );

    $pdf->SetXY(162, 208);
    $pdf->Cell(
        38,
        0,
        number_format($total_JPY) . "円",
        0,
        0,
        'R'
    );
    $pdf->SetXY(162, 218);
    $pdf->Cell(
        38,
        0,
        number_format($total_JPY) . "円",
        0,
        0,
        'R'
    );

    $pdf->SetXY(10, 131);
    $pdf->Cell(
        1,
        0,
        "　利用料は$" . number_format($usage_USD, 2) . "、"
        . number_format($usage_JPY) . "円です。",
        0,
        0,
        'L'
    );
    $pdf->SetXY(10, 136);
    $pdf->Cell(
        1,
        0,
        "　消費税相当額は$" . number_format($tax_USD, 2) . "、"
        . number_format($tax_JPY) . "円です。",
        0,
        0,
        'L'
    );

//    if ($chargeDays != 0) {
//        $pdf->SetXY(10, 141);
//        $pdf->Cell(
//            1,
//            0,
//            "　SunnyViewサービス利用料は$" . number_format($idsFeeTotal, 2) . "、"
//            . number_format($idsFeeTotalJPY) . "円です。",
//            0,
//            0,
//            'L'
//        );
//    }

    //  ob_end_clean();
    $filename = str_pad($TARGET_ACID, 4, 0, STR_PAD_LEFT) . "-"
        . $Y . $M . ".pdf";
    $pdf->Output(INVOICE_PATH . $filename, "F");
    echo $filename . "を生成しました。\n";
}

//Download all

$sql = "SELECT 
    filename,
    costacctid,
    companyname
        FROM invoice2
        WHERE 
            ym = '" . $Y . $M . "'
            AND internaluse = false
            AND filename is not null
        ORDER BY refnoserial
";
$logs[] = $sql;
$data   = pg_query($smarty->_db, $sql);

echo "まとめ請求書ファイルを生成します。\n";
$pdf_all = new FPDI();
$pdf_all->setPrintHeader(false);
$pdf_all->setPrintFooter(false);

for ($pageno = 0; $pageno < pg_num_rows($data); $pageno++) {
    $row = pg_fetch_row($data, $pageno);
    $key = $row[0];

    if ($key == "") {
        echo "エラー: 請求書ファイル名がnullです。 " . $row[1] . " " . $row[2] . "\n";
        continue;
    }

    $filepath = INVOICE_PATH . $key;

    echo "... " . $filepath . "をマージします。\n";
    $page = $pdf_all->setSourceFile($filepath);
    for ($i = 1; $i <= $page; $i++) {
        $pdf_all->addPage();
        $pdf_all->useTemplate($pdf_all->importPage($i));
    }
}
$pdf_all->Output(INVOICE_PATH . "{$Y}{$M}_download_all.pdf", "F");

echo "生成終了\n";

if ($i != 0) {
    // お知らせを追加
    $sql = "BEGIN;";
    $r   = pg_query($smarty->_db, $sql);
    $sql = "SELECT max(mid)+1 FROM info";
    $logs[] = $sql;
    $r   = pg_query($smarty->_db, $sql);

    $newMID = pg_fetch_result($r, 0, 0);
    if ($newMID == "") {
        $newMID = 1;
    }

    $sql = '
        INSERT INTO info(
            mid, rank, senddate,
            title, message, toall
        ) VALUES (
            $1, $2, $3,
            $4, $5, $6
        );
    ';
    $logs[] = $sql;

    $title = "{$Y}年{$M}月のご請求書の準備ができました。";
    $message  = "{$Y}年{$M}月のご請求書の準備ができました。<br />";
    $message .= "請求書メニューからPDF形式にて、ダウンロードいただけます。<br />";
    $message .= "今月のレートは、{$rate1M}円/ドルでした。<br /><br />";
    $message .= "ご確認のほど、よろしくお願いいたします。";
    $r = pg_query_params(
        $smarty->_db,
        $sql,
        [
        $newMID, 1, "now()",
        $title, $message, 1
        ]
    );
    $sql = "COMMIT;";
    $r = pg_query($smarty->_db, $sql);
}
echo $i . "件、処理が終了しました。\n";
raise_sql($logs, 'batch_createInvoice');
exit(0);
