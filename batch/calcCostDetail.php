<?php
// 正確な請求金額を取得するため、costテーブルをサービス毎に集計して
// cost_detailテーブルにレコードを作成します。

function calcCostDetail(
    $smarty,
    $TARGET_CID,
    $Y,
    $M
) {
    $startDate = $Y . "-" . $M . "-01";
    $endDate = date("Y-m-d", strtotime("+ 1 month", strtotime($startDate)));

    // 請求金額の算出 ===> cost_detailへレコード作成


    // 古い集計データを削除
    $sql = "
        DELETE
            FROM cost_detail
        WHERE
            cid=" . $TARGET_CID . "
            AND ym='" . $Y . "/" . $M . "'";
    $r = pg_query($smarty->_db, $sql);

    // 一部のproductcodeについては、productfamily=Data Transferを
    // productcode = AWS Data Transfer に更新
    $sql = "
        UPDATE
            cost
        SET
            productcode = 'AWS Data Transfer'
        WHERE
            cid=" . $TARGET_CID . "
            AND usagestartdate >= '" . $startDate . "'
            AND usagestartdate < '" . $endDate . "'
            AND productfamily = 'Data Transfer'
            AND (
                    productcode = 'Amazon Elastic Compute Cloud'
                OR
                    productcode = 'AWS Lambda'
                OR
                    productcode = 'Amazon Relational Database Service'
                OR
                    productcode = 'Amazon Simple Notification Service'
                OR
                    productcode = 'Amazon Simple Storage Service'
                OR
                    productcode = 'AWS Storage Gateway'
                OR
                    productcode = 'Amazon Virtual Private Cloud'
            )
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        INSERT
            INTO cost_detail
            (
                cid,
                ym,
                lineitemtype,
                productcode,
                productfamily,
                region,
                unblendedrate,
                lineitemdescription,
                unblendedcost,
                blendedcost,
                usageamount,
                org_unblendedcost,
                org_blendedcost,
                org_usageamount,
                preinstalledsw,
                operatingsystem,
                unit,
                usagetype,
                billingentity
            )
        SELECT
            " . $TARGET_CID . ",
            '" . $Y . "/" . $M  . "',
            lineitemtype,
            productcode,
            productfamily,
            region,
            unblendedrate,
            lineitemdescription,
            sum(unblendedcost),
            sum(blendedcost),
            sum(usageamount),
            sum(unblendedcost),
            sum(blendedcost),
            sum(usageamount),
            preinstalledsw,
            operatingsystem,
            unit,
            usagetype1,
            usagetype2
       FROM
            cost
        WHERE
            cid=" . $TARGET_CID . "
            AND usagestartdate >= '" . $startDate . "'
            AND usagestartdate < '" . $endDate . "'
            AND (
                productfamily != 'Data Transfer'
                OR lineitemtype != 'Usage'
            )
        GROUP BY
            usagetype2,
            lineitemtype,
            productcode,
            productfamily,
            region,
            usagetype1,
            unblendedrate,
            lineitemdescription,
            preinstalledsw,
            operatingsystem,
            unit
        ORDER BY
            usagetype2 DESC,
            lineitemtype DESC,
            productcode,
            productfamily,
            region,
            usagetype1,
            unblendedrate,
            lineitemdescription;
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        INSERT
            INTO cost_detail
            (
                cid,
                ym,
                lineitemtype,
                productcode,
                productfamily,
                region,
                lineitemdescription,
                unblendedcost,
                blendedcost,
                usageamount,
                org_unblendedcost,
                org_blendedcost,
                org_usageamount,
                unit,
                usagetype,
                billingentity
            )
        SELECT
            " . $TARGET_CID . ",
            '" . $Y . "/" . $M  . "',
            lineitemtype,
            'AWS Data Transfer',
            'Data Transfer',
            region,
            lineitemdescription,
            sum(unblendedcost),
            sum(blendedcost),
            sum(usageamount),
            sum(unblendedcost),
            sum(blendedcost),
            sum(usageamount),
            unit,
            usagetype1,
            usagetype2
       FROM
            cost
        WHERE
            cid=" . $TARGET_CID . "
            AND usagestartdate >= '" . $startDate . "'
            AND usagestartdate < '" . $endDate . "'
            AND productfamily = 'Data Transfer'
            AND lineitemtype = 'Usage'
            AND productcode != 'AWS Direct Connect'
            AND productcode != 'Amazon CloudFront'
        GROUP BY
            usagetype2,
            lineitemtype,
            productcode,
            region,
            usagetype1,
            lineitemdescription,
            unit
        ORDER BY
            usagetype2 DESC,
            lineitemtype DESC,
            region,
            usagetype1,
            lineitemdescription;
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        INSERT
            INTO cost_detail
            (
                cid,
                ym,
                lineitemtype,
                productcode,
                productfamily,
                region,
                lineitemdescription,
                unblendedcost,
                blendedcost,
                usageamount,
                org_unblendedcost,
                org_blendedcost,
                org_usageamount,
                unit,
                usagetype,
                billingentity
            )
        SELECT
            " . $TARGET_CID . ",
            '" . $Y . "/" . $M  . "',
            lineitemtype,
            productcode,
            productfamily,
            region,
            lineitemdescription,
            sum(unblendedcost),
            sum(blendedcost),
            sum(usageamount),
            sum(unblendedcost),
            sum(blendedcost),
            sum(usageamount),
            unit,
            usagetype1,
            usagetype2
       FROM
            cost
        WHERE
            cid=" . $TARGET_CID . "
            AND usagestartdate >= '" . $startDate . "'
            AND usagestartdate < '" . $endDate . "'
            AND productfamily = 'Data Transfer'
            AND lineitemtype = 'Usage'
            AND (
                productcode = 'AWS Direct Connect'
                OR productcode = 'Amazon CloudFront'
            )
        GROUP BY
            usagetype2,
            lineitemtype,
            productcode,
            productfamily,
            region,
            usagetype1,
            lineitemdescription,
            unit
        ORDER BY
            usagetype2 DESC,
            lineitemtype DESC,
            productfamily,
            region,
            usagetype1,
            lineitemdescription;
    ";
    $r = pg_query($smarty->_db, $sql);

    // 0.00001未満ものは0に。
    // 非公式対応。ogura@ids.co.jpの2018/7 Lambda分対応
    $sql = "
        UPDATE
            cost_detail
        SET
            unblendedcost = 0.01
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND unblendedcost >= 0.0000001
            AND unblendedcost < 0.01
            AND lineitemtype != 'Tax'
            AND (
                (
                productcode != 'Amazon DynamoDB'
                AND productcode != 'Amazon Simple Queue Service'
                AND productcode != 'AWS Lambda'
                AND productcode != 'Amazon Simple Storage Service'
                AND productcode != 'AWS Data Transfer'
                AND productcode != 'AmazonCloudWatch'
                )
                OR (
                productcode = 'Amazon CloudFront'
                AND productfamily = 'Data Transfer'
                )
           )
    ";
    $r = pg_query($smarty->_db, $sql);

//    $sql = "
//        UPDATE
//            cost_detail
//        SET
//            unblendedcost = 0.01
//        WHERE
//            cid=" . $TARGET_CID . "
//            AND ym = '" . $Y . "/" . $M  . "'
//            AND unblendedcost >= 0.000001
//            AND unblendedcost < 0.01
//            AND productcode = 'AWS Data Transfer'
//            AND lineitemtype != 'Tax'
//    ";
//    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            unblendedcost = 0.01
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND unblendedcost >= 0.00001
            AND unblendedcost < 0.01
            AND (
                productcode = 'Amazon DynamoDB'
                OR productcode = 'Amazon Simple Queue Service'
                OR productcode = 'AWS Lambda'
            )
            AND lineitemtype != 'Tax'
    ";
    $r = pg_query($smarty->_db, $sql);


    $sql = "
        UPDATE
            cost_detail
        SET
            unblendedcost = -0.01
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND unblendedcost <= -0.0000001
            AND unblendedcost > -0.01
            AND lineitemtype != 'Tax'
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            unblendedcost = trunc(unblendedcost, 2)
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND lineitemtype='RIFee'
     ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            unblendedcost = round(unblendedcost, 2)
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND lineitemtype != 'RIFee'
     ";
    $r = pg_query($smarty->_db, $sql);

    // 小さい数字を0にリセット
    $sql = "
        UPDATE
            cost_detail
        SET
            unblendedcost = 0
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND unblendedcost > 0
            AND unblendedcost < 0.01
            AND lineitemtype != 'Tax'
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            unblendedcost = 0
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND unblendedcost < 0
            AND unblendedcost > -0.01
            AND lineitemtype != 'Tax'
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            usageamount = round(usageamount, 3)
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND usageamount >= 0.001
     ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            usageamount = round(usageamount, 6)
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND usageamount >= 0.000001
            AND usageamount < 0.001
     ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            usageamount = round(usageamount, 9)
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND usageamount < 0.000001
     ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            usageamount = round(usageamount, 0)
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND productfamily = 'Request'
     ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            lineitemtype = 'Tax Refund'
        WHERE
            cid=" .  $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND lineitemtype='Tax'
            AND unblendedcost < 0 
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        UPDATE
            cost_detail
        SET
            region = 'No Region'
        WHERE
            cid=" . $TARGET_CID . "
            AND ym = '" . $Y . "/" . $M  . "'
            AND lineitemdescription = 'PAR_APN_ProgramFee_3500'
    ";
    $r = pg_query($smarty->_db, $sql);

    return(0);
}
