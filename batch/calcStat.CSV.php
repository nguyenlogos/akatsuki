<?php
// 処理内容 costテーブルのレコードをもとに下記を計算
// 1) stat_costに月ごとサービスごと費用を集計
// 2) stat_instに月ごとインスタンスタイプごと個数を集計
// 3) stat_ebsに月ごとディスクタイプごとのサイズと個数を集計
// 4) stat_s3に（同じ）
// 5) stat_cost_dailyに日ごとサービスごとの費用を集計

require_once "batch_loader.php";
require_once "calcCostDetail.php";

$smarty = new MySmarty();
$smarty->commonInfoSet();

$SearchAndPush = function (&$typeArr, &$countArr, $insttype) {
    for ($i =0; $i < count($typeArr); $i++) {
        if ($typeArr[$i] == $insttype) {
            $countArr[$i]++;
            break;
        }
    }
    if ($i >= count($typeArr)) {
        array_push($typeArr, $insttype);
        array_push($countArr, 1);
    }
};

// 起動パラメタのチェック
switch (count($argv)) {
    case 1:
        // 処理ターゲットの年月日取得
        // 昨日の一か月間
        $sY = date("Y", strtotime("- 1 days"));
        $sM = date("m", strtotime("- 1 days"));
        break;

    case 2:
        $offsetM = $argv[1];
        $sY = date("Y", strtotime($offsetM . " month"));
        $sM = date("m", strtotime($offsetM . " month"));
        break;

    default:
        echo "起動パラメタに誤りがあります。\n";
        echo "Usage : " . $argv[0] . " {offset month}\n";
        echo "ex) " . $argv[0] . " -1\n";
        exit -1;
}
$TARGET_YM = $sY . "/" . $sM;

$sql = "SELECT cid,key,secret,costacctid FROM users WHERE status=0 ORDER BY cid;";
$cidres = pg_query($smarty->_db, $sql);
$cidLoopLimit = pg_num_rows($cidres);

echo "INFO: " . $cidLoopLimit . "件のユーザを処理します。\n";

for ($cidLoop = 0; $cidLoop < $cidLoopLimit; $cidLoop ++) {
    $row = pg_fetch_row($cidres, $cidLoop);

    $TARGET_CID = $row[0];
    $TARGET_KEY = $row[1];
    $TARGET_SEC = $row[2];
    $TARGET_AID = $row[3];

    if ($TARGET_KEY == "" || $TARGET_SEC == "") {
        echo "API認証情報が設定されていないため、コスト計算処理をスキップしました。\n";
        continue;
    }

    // 月間サービス毎コスト集計
    calcCostDetail($smarty, $TARGET_CID, $sY, $sM);

    // インスタンス関連の統計情報の取得
    $sql = "DELETE FROM stat_inst WHERE cid=" . $TARGET_CID . " and YM='" . $TARGET_YM . "'";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        SELECT
            to_char(usagestartdate, 'YYYY/MM') aaa,
            resourceid,
            instancetype,
            MIN(usagestartdate) bbb
        FROM
            cost
        WHERE
            (
                cid = " . $TARGET_CID . "
                OR usageaccountid = '" . $TARGET_AID . "'
            )
            AND to_char(usagestartdate, 'YYYY/MM') = '" . $TARGET_YM . "'
            AND servicecode = 'AmazonEC2'
            AND operation1 = 'RunInstances'
        GROUP BY
            aaa,
            resourceid,
            instancetype
        ORDER BY
            resourceid,
            bbb,
            resourceid;
    ";
    $r = pg_query($smarty->_db, $sql);
    //   aaa   |     resourceid      | instancetype |         bbb
    //---------+---------------------+--------------+---------------------
    // 2018/01 | i-03a0e65851b870ffc | t2.small     | 2018-01-01 00:00:00
    // 2018/01 | i-07a38d88          | t2.micro     | 2018-01-01 00:00:00
    // 2018/01 | i-0801daf17e4e1db43 | t2.small     | 2018-01-01 00:00:00
    // 2018/01 | i-08191a067d54ef02b | t2.micro     | 2018-01-01 00:00:00
    // 2018/01 | i-08191a067d54ef02b | t2.small     | 2018-01-04 11:00:00
    // 2018/01 | i-09dd79f5966fdb2b9 | t2.nano      | 2018-01-01 00:00:00
    // 2018/01 | i-0cd593d5fab6a534d | t2.small     | 2018-01-01 00:00:00

    $typeArr = array();
    $countArr = array();
    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);
        $preid=$row[1];
        $pretype=$row[2];
        for ($i = 1; $i < pg_num_rows($r); $i++) {
            $row = pg_fetch_row($r, $i);
            if ($row[1] != $preid) {
                $SearchAndPush($typeArr, $countArr, $pretype);
                $preid = $row[1];
                $pretype=$row[2];
            }
        }
        if ($i != 1) {
            $SearchAndPush($typeArr, $countArr, $pretype);
        }
    }

    for ($i = 0; $i < count($typeArr); $i++) {
        $sql = "
            INSERT INTO
                stat_inst
                (
                    cid,
                    ym,
                    count,
                    instancetype
                )
            VALUES
                (" . $TARGET_CID . ", '" . $TARGET_YM . "', " . $countArr[$i] . ", '" . $typeArr[$i] . "');
        ";
        $r = pg_query($smarty->_db, $sql);
    }

    // EBS関連の統計情    報
    $sql = "
        SELECT
            to_char(usagestartdate, 'YYYY/MM') aaa,
            round(SUM(usageamount), 2) bbb
        FROM
            cost
        WHERE
            (
                cid = " . $TARGET_CID . "
                OR usageaccountid = '" . $TARGET_AID . "'
            )
            AND to_char(usagestartdate, 'YYYY/MM') = '" . $TARGET_YM . "'
            AND servicecode = 'AmazonEC2'
            AND productfamily = 'Storage'
        GROUP BY
            aaa
        ORDER BY
            aaa
    ";
    $r = pg_query($smarty->_db, $sql);
    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);
        if ($row[1] == "") {
            $gib = 0;
        } else {
            $gib = $row[1];
        }
    } else {
        $gib = 0;
    }

    $sql = "
        SELECT
            COUNT(aaa)
        FROM
            (
                SELECT
                    to_char(usagestartdate, 'YYYY/MM') aaa,
                    resourceid bbb
                FROM
                    cost
                WHERE
                    (
                        cid = " . $TARGET_CID . "
                        OR usageaccountid = '" . $TARGET_AID . "'
                    )
                    AND to_char(usagestartdate, 'YYYY/MM') = '" . $TARGET_YM . "'
                    AND servicecode = 'AmazonEC2'
                    AND productfamily = 'Storage'
                GROUP BY
                    aaa,
                    resourceid
            )
            tmp
    ";
    $r = pg_query($smarty->_db, $sql);
    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);

        if ($row[0] == "") {
            $count = 0;
        } else {
            $count = $row[0];
        }
    } else {
        $count = 0;
    }

    $sql = "DELETE FROM stat_ebs WHERE cid=" . $TARGET_CID . " and YM='" . $TARGET_YM . "'";
    $r = pg_query($smarty->_db, $sql);


    $sql = "INSERT INTO stat_ebs VALUES(" . $TARGET_CID . ",'" . $TARGET_YM . "'," . $count . "," . $gib . ");";
    $r = pg_query($smarty->_db, $sql);

    // S3関連の統計情    報
    $sql = "
        SELECT
            to_char(usagestartdate, 'YYYY/MM') aaa,
            round(SUM(usageamount), 2) bbb
        FROM
            cost
        WHERE
            (
                cid = " . $TARGET_CID . "
                OR usageaccountid = '" . $TARGET_AID . "'
            )
            AND to_char(usagestartdate, 'YYYY/MM') = '" . $TARGET_YM . "'
            AND servicecode = 'AmazonEC2'
            AND productfamily = 'Storage Snapshot'
        GROUP BY
            aaa
        ORDER BY
            aaa
    ";
    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);
        if ($row[1] == "") {
            $gib = 0;
        } else {
            $gib = $row[1];
        }
    } else {
        $gib = 0;
    }

    $sql = "
        SELECT
            COUNT(aaa)
        FROM
            (
                SELECT
                    to_char(usagestartdate, 'YYYY/MM') aaa,
                    resourceid bbb
                FROM
                    cost
                WHERE
                    (
                        cid = " . $TARGET_CID . "
                        OR usageaccountid = '" . $TARGET_AID . "'
                    )
                    AND to_char(usagestartdate, 'YYYY/MM') = '" . $TARGET_YM . "'
                    AND servicecode = 'AmazonEC2'
                    AND productfamily = 'Storage Snapshot'
                GROUP BY
                    aaa,
                    resourceid
            )
            tmp
    ";

    $r = pg_query($smarty->_db, $sql);

    if (pg_num_rows($r) != 0) {
        $row = pg_fetch_row($r, 0);

        if ($row[0] == "") {
            $count = 0;
        } else {
            $count = $row[0];
        }
    } else {
        $count = 0;
    }

    $sql = "
         DELETE 
             FROM stat_s3 
             WHERE cid=" . $TARGET_CID . " 
                 AND YM='" . $TARGET_YM . "'
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        INSERT
            INTO stat_s3 
            VALUES(
                " . $TARGET_CID . ",
                '" . $TARGET_YM . "',
                " . $count . ",
                " . $gib . ");
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        DELETE 
            FROM stat_cost_daily 
            WHERE cid=" . $TARGET_CID . "
                AND ymd >= '" . $TARGET_YM . "/01' 
                AND ymd < to_char(date '" . $TARGET_YM . "/01' 
                    + interval '1 month', 'YYYY/MM/DD')
    ";
    $r = pg_query($smarty->_db, $sql);

    $sql = "
        INSERT 
            INTO stat_cost_daily 
                (
                    cid, 
                    ymd, 
                    servicecode, 
                    ubcost, 
                    bcost, 
                    usagequantity, 
                    currencycode 
                )
            SELECT 
                " . $TARGET_CID . ", 
                to_char(usagestartdate, 'YYYY/MM/DD') ymd, 
                productcode, 
                sum(unblendedcost), 
                sum(blendedcost), 
                sum(usageamount), 
                max(currencycode)
           FROM 
               cost 
           WHERE 
                (
                    cid = " . $TARGET_CID . "
                    OR usageaccountid = '" . $TARGET_AID . "'
                )
               AND unblendedcost <> 0 
               AND to_char(usagestartdate, 'YYYY/MM') = '" . $TARGET_YM . "' 
           GROUP BY 
               ymd, 
               productcode, 
               usageaccountid
    ";
    $r = pg_query($smarty->_db, $sql);
    echo "正常終了\n";
} // end for CID loop
exit;
