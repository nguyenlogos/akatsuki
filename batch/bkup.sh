#!/bin/bash

datestr=`date +%Y%m%d`

mkdir /mnt/BKUP/${datestr}
mkdir /mnt/BKUP/${datestr}/htdocs
mkdir /mnt/BKUP/${datestr}/batch
mkdir /mnt/BKUP/${datestr}/smarty

cp -R /usr/local/apache2/htdocs/* /mnt/BKUP/${datestr}/htdocs
cp -R /usr/local/apache2/batch/* /mnt/BKUP/${datestr}/batch
cp -R /usr/local/apache2/smarty/* /mnt/BKUP/${datestr}/smarty

