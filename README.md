AKATSUKI
====

## 1. Requirement:

## 2. TODO

- Change folder permisions:
`
mkdir awsfw_session/ invoice/ batch/tmp/ batch/temp/ logsql/ logs/ libs/smarty/templates_c/ db-data/
find . -type d -exec chmod 755 {} \;
find . -type f -exec chmod 644 {} \;
chmod 777 -R awsfw_session/ invoice/ logs/ logsql/ batch/tmp/ libs/smarty/templates_c/ src/Models migrations/ vendor/ db-data/
`

## 3. Dependencies

- Postgre
- php7.1

## 4. Usage:

- Run service:

    ```bash

    #install Docker
        - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        - sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
        - sudo apt-get update
        - apt-cache policy docker-ce
        - sudo apt-get install -y docker-ce

    #install Docker compose
        - sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
        - sudo chmod +x /usr/local/bin/docker-compose

    #start project
        - docker-compose build
        - docker-compose up -d

    ```

- Migration step:

    ```bash

    #create migration
        - php src/app/phpmig.php generate [MigrationName]
        - change file migrations/20111101000144_[MigrationName].php from `use Phpmig\Api\PhpmigApplication;` to `use App\Migration;`

    #up migration
        - bin/phpmig.php migrate


    #down migration
        - bin/phpmig.php rollback -t 20111101000144
        - bin/phpmig.php down 20111101000144


    ```

## 5. Import sql:

- Import:

    ```bash
        #copy sql file to container php
        - sudo docker cp /home/truong/Downloads/awsfw.sql lemp_prges:/awsfw.sql
        #login to postgre container
        - sudo docker exec -it lemp_pgres bash
        #run command import
        - psql -U root awsfw < awsfw.sql

    ```

## 6. Control the services:

- Control Service:

    ```bash
        #up
        - docker-compose up -d
        #down
        - docker-compose down
        #restart
        - docker-composer restart
    ```



## 7. Note:

- Dev url: http://akatsuki_dev.local


## 8. Cronjob setup:

- Control Service:

    ```bash
        #run script php outside docker container
        - sudo docker-compose exec php php batch/calcStat.php
    ```

## 9. Migrate setup:

- Control Service:

    ```bash
        #run script php outside docker container
        - composer install
        - sudo docker-compose exec php php src/app/phpmig.php migrate
    ```

## 10. Codeception Installation

    ```bash
        docker exec -it lemp_php bash
        #install Codeception as global
        composer global require --dev "codeception/codeception=^2.4" "codeception/specify=^1.1"
        #create symlink for codeception
        ln -s ~/.composer/vendor/bin/codecept /usr/local/bin/codecept
        #run tests
        codecept run
        #or run from outside
        docker exec lemp_php codecept run
    ```
## 11. Reference

