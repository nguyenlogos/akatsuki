<?php
use Aws\Exception\AwsException as AwsException;

function updateEBS($cid, $smarty, $config)
{
    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1 || $cid == "") {
        return -2;
    }

    if ($config["credentials"]["key"] == "" || $config["credentials"]["secret"] == "" || $config["region"] == "") {
        return (-2);
    }

    try {
        $ec2Client = new Aws\Ec2\Ec2Client($config);

        $result = $ec2Client->DescribeVolumes();
    } catch (AwsException $e) {
        aws_handle_exception($e, "[EC2] Describe Volumes");
        return (-1);
    }

    $sql = "DELETE FROM volumes WHERE cid = " . $cid . " AND stragetype = 'EBS'";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $volumes = $result['Volumes'];
    $i = 0;

    foreach ($volumes as $vol) {
        $sql = "INSERT INTO volumes (
            cid, device, instid, state, volid,
            delonterm, azone, attachtime, createtime,
            encrypt, size, snapshotid, state2, iop,
            tag_name, voltype, stragetype
        ) VALUES (" . $cid . ",";
        if (array_key_exists(0, $vol['Attachments']) == true) {
            $sql .= setStrForDB($vol['Attachments'][0]['Device'], true);
            $sql .= setStrForDB($vol['Attachments'][0]['InstanceId'], true);
            $sql .= setStrForDB($vol['Attachments'][0]['State'], true);
        } else {
            $sql .= "null,null,null,";
        }
        $sql .= setStrForDB($vol['VolumeId'], true);
        if (array_key_exists(0, $vol['Attachments']) == true) {
            if ($vol['Attachments'][0]['DeleteOnTermination'] == true) {
                $sql .= "1,";
            } else {
                $sql .= "0,";
            }
        } else {
            $sql .= "null,";
        }

        $sql .= setStrForDB($vol['AvailabilityZone'], true);
        $sql .= "'" . $vol['Attachments'][0]['AttachTime'] . "',";
        // $sql .= "null,";
        $sql .= "'" . $vol['CreateTime'] . "',";
        // $sql .= "null,";
        if ($vol['Encrypted'] == true) {
            $sql .= "1,";
        } else {
            $sql .= "0,";
        }
        $sql .= $vol['Size'] . ",";
        $sql .= setStrForDB($vol['SnapshotId'], true);
        $sql .= setStrForDB($vol['State'], true);
        if (array_key_exists('Iops', $vol) == true) {
            if ($vol['Iops'] == "") {
                $sql .= "null,";
            } else {
                $sql .= $vol['Iops'] . ",";
            }
        } else {
            $sql .= "null,";
        }
        if (array_key_exists('Tags', $vol) == true) {
            $tmp = "";
            for ($i = 0; $i < count($vol['Tags']); $i++) {
                if ($vol['Tags'][$i]['Key'] == "Name") {
                    $tmp = $vol['Tags'][$i]['Value'];
                    break;
                }
            }
            $sql .= setStrForDB($tmp, true);
        } else {
            $sql .= "null,";
        }

        $sql .= setStrForDB($vol['VolumeType'], true);
        $sql .= "'EBS')";
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);
    }
    raise_sql($logs, 'if_updateEBS');

    return(0);
}
