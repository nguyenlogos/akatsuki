<?php
use Aws\Exception\AwsException as AwsException;
use Common\Logger;

// TODO: remove (use \Akatsuki\Models\Instance::updateInstances($cid, $region) instead)
function updateInst($smarty, $config, $cid, $mode, &$instIDList = [])
{
    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1 || $cid == "") {
        return -2;
    }

    if (empty($config)) {
        $ec2Client = getEc2Client($smarty, $cid, 'ap-northeast-1');
        if (!$ec2Client) {
            return -3;
        }
    } elseif ($config["credentials"]["key"] == "" ||
            $config["credentials"]["secret"] == "" ||
            $config["region"] == ""
    ) {
        return (-4);
    } else {
        $ec2Client = new Aws\Ec2\Ec2Client($config);
    }
    try {
        $result = $ec2Client->DescribeInstances();
    } catch (AwsException $e) {
        aws_handle_exception($e, "[EC2] Describe Instances");
        return (-5);
    }
    if ($mode == "TEST") {
        return (0);
    }
    $instancesMap = [];
    $sql = "SELECT id, pcode, inst_req_id FROM instance WHERE cid = " . $cid;
    $logs[] = $sql;

    $r = pg_query($smarty->_db, $sql);
    $instances = pg_fetch_all($r);
    if ($instances) {
        foreach ($instances as $instance) {
            $instanceID = $instance['id'];
            unset($instance['id']);
            $instancesMap[$instanceID] = $instance;
        }
    }

    pg_query($smarty->_db, "BEGIN");
    $sql = "DELETE FROM instance WHERE cid = " . $cid;
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    if (!$r) {
        pg_query($smarty->_db, "ROLLBACK");
        return -6;
    }
    $reservations = $result['Reservations'];
    $instanceCount = 0;
    $optionalKeys = [
        'KernelId',
        'RamdiskId',
        'PrivateIpAddress',
        'PublicIpAddress',
    ];
    foreach ($reservations as $reservation) {
        $instances = $reservation['Instances'];
        foreach ($instances as $instance) {
            $instanceCount++;
            $instanceName = $instReqID = $pcode = null;
            $instanceID = $instance['InstanceId'];
            if (!empty($instance['Tags'])) {
                foreach ($instance['Tags'] as $tag) {
                    if ($tag['Key'] == 'Name') {
                        $instanceName = $tag['Value'];
                        break;
                    }
                }
            }
            if (array_key_exists($instanceID, $instancesMap)) {
                $info = $instancesMap[$instanceID];
                $instReqID = (int)$info['inst_req_id'];
                $pcode = $info['pcode'];
            }
            $ElasticIP = null;
            // if (count($instance['NetworkInterfaces'])) {
            //     $NetworkInterfaces = array_pop($instance['NetworkInterfaces']);
            //     if (!empty($NetworkInterfaces['Association'])) {
            //         $ElasticIP = $NetworkInterfaces['Association']['PublicIp'];
            //     }
            // }
            $sql = '
                INSERT INTO instance(
                    cid, id, name,
                    type, sg, platform,
                    kernelid, ramdiskid, imageid,
                    state, privatednsname, publicdnsname,
                    launchtime, availabilityzone, privateip,
                    publicip, rootdevicename, lastchecked,
                    pcode, inst_req_id, elasticip
                )
                VALUES(
                    $1, $2, $3,
                    $4, $5, $6,
                    $7, $8, $9,
                    $10, $11, $12,
                    $13, $14, $15,
                    $16, $17, $18,
                    $19, $20, $21
                );
            ';
            $SecurityGroup = array_pop($instance['SecurityGroups']);
            $GroupId = null;
            if ($SecurityGroup) {
                $GroupId = $SecurityGroup['GroupId'];
            }
            foreach ($optionalKeys as $key) {
                if (!array_key_exists($key, $instance)) {
                    $instance[$key] = null;
                }
            }
            $bindingValues = [
                $cid, $instanceID, $instanceName,
                $instance['InstanceType'], $GroupId, $instance['Architecture'],
                $instance['KernelId'], $instance['RamdiskId'], $instance['ImageId'],
                $instance['State']['Name'], $instance['PrivateDnsName'], $instance['PublicDnsName'],
                $instance['LaunchTime'], $instance['Placement']['AvailabilityZone'], $instance['PrivateIpAddress'],
                $instance['PublicIpAddress'], $instance['RootDeviceName'], "now()",
                $pcode, $instReqID, $ElasticIP
            ];
            $result = pg_query_params($smarty->_db, $sql, $bindingValues);
            if ($result) {
                $instIDList[] = $instanceID;
            } else {
                pg_query($smarty->_db, "ROLLBACK");
                return -7;
            }
        }
    }
    pg_query($smarty->_db, "COMMIT");
    raise_sql($logs, 'if_updateInst');

    return 0;
}
