<?php
use Aws\Exception\AwsException as AwsException;
use Common\Logger;

function updateAccountID($smarty, $config, $cid, $mode)
{
    if ($cid == "") {
        return -2;
    }

    if ($config["credentials"]["key"] == "" || $config["credentials"]["secret"] == "" || $config["region"] == "") {
        return (-2);
    }

    $ACCTID = "";

    try {
        $sts = new Aws\Sts\StsClient($config);
        $r = $sts->getCallerIdentity();
        $ACCTID = $r["Account"];
    } catch (AwsException $e) {
        aws_handle_exception($e, "[STS] Get Caller Identity");
        return (-1);
    }

    if ($ACCTID == "") {
        return (-1);
    }

    if ($mode == "TEST") {
        return(0);
    }

    $sql = "UPDATE users SET costacctid='" . $ACCTID . "' WHERE cid=" . $cid . " and status=0";
    $r = pg_query($smarty->_db, $sql);

    return(0);
}
