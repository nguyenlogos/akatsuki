<?php
use Aws\Exception\AwsException as AwsException;

function updateCOST($cid, $smarty, $config)
{
    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1 || $cid == "") {
        return -2;
    }

    if ($config["credentials"]["key"] == "" || $config["credentials"]["secret"] == "" || $config["region"] == "") {
        return (-2);
    }

    try {
        $client = new Aws\CostExplorer\CostExplorerClient($config);

        $result = $client->getCostAndUsage([
            'Filter' => [
                'Dimensions' => [
                    'Key' => 'SERVICE',
                    'Values' => ['Amazon Elastic Block Store'],
                ],
            ],
            'Granularity' => 'MONTHLY',
            'TimePeriod' => [
                'End' => '2018-03-01',
                'Start' => '2018-02-01'
            ],
            // 'GroupBy' => [['Key' => 'INSTANCE_TYPE', 'Type' => 'DIMENSION'],['Key'=> 'LINKED_ACCOUNT', 'Type' => 'DIMENSION']],
            'GroupBy' => [
                ['Key' => 'USAGE_TYPE', 'Type' => 'DIMENSION'],
                ['Key' => 'LINKED_ACCOUNT', 'Type' => 'DIMENSION'],
                ['Key' => 'SERVICE', 'Type' => 'DIMENSION']],
            'Metrics' => ['BlendedCost','UnblendedCost','UsageQuantity'],
        ]);
    } catch (AwsException $e) {
        aws_handle_exception($e, "[EBS] Get Cost And Usage]");
        return (-1);
    }

    $sql = "DELETE FROM volumes WHERE cid = " . $cid . " AND stragetype = 'SS'";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $snaps = $result['Snapshots'];
    $i = 0;

    foreach ($snaps as $snap) {
        $sql = "INSERT INTO volumes (cid, state, volid, createtime, encrypt, size, snapshotid, tag_name, progress, description, stragetype) VALUES (" . $cid . ",";
        $sql .= setStrForDB($snap['State'], true);
        $sql .= setStrForDB($snap['VolumeId'], true);
        $sql .= setStrForDB($snap['StartTime'], true);
        if ($snap['Encrypted'] == true) {
            $sql .= "1,";
        } else {
            $sql .= "0,";
        }
        $sql .= $snap['VolumeSize'] . ",";
        $sql .= setStrForDB($snap['SnapshotId'], true);
        if (array_key_exists('Tags', $snap) == true) {
            $tmp = "";
            for ($i = 0; $i < count($snap['Tags']); $i++) {
                if ($snap['Tags'][$i]['Key'] == "Name") {
                    $tmp = $snap['Tags'][$i]['Value'];
                    break;
                }
            }
            $sql .= setStrForDB($tmp, true);
        } else {
            $sql .= "null,";
        }
        $sql .= setStrForDB($snap['Progress'], true);
        $sql .= setStrForDB($snap['Description'], true);
        $sql .= "'SS')";
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);
    }
    raise_sql($logs, 'if_updateCOST');

    return(0);
}
