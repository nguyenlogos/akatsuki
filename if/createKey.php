<?php
use Aws\Exception\AwsException as AwsException;
use Common\Logger;

function ListKeyPair($config)
{
    try {
        $client = new Aws\Ec2\Ec2Client($config);

        $result = $client->describeKeyPairs();
    } catch (AwsException $e) {
        aws_handle_exception($e, "[Ec2] Describe Key Pairs");
        return (-1);
    }

    $key_pair = $result['KeyPairs'];
    $list_key = [] ;
    if ($key_pair) {
        foreach ($key_pair as $key) {
            $list_key[] = $key['KeyName'];
        }
    }

    return $list_key;
}

function CreateKeyPair($config, $name_pair)
{
    $savePath = KEYPAIR_PATH . "/{$_SESSION['accountid']}";
    if (!is_dir($savePath)) {
        $newKeypair = mkdir(KEYPAIR_PATH);
        if ($newKeypair) {
            $result = mkdir($savePath);
            if (!$result) {
                return 0;
            }
        }
    }
    try {
        $ec2Client = new Aws\Ec2\Ec2Client($config);
        $result = $ec2Client->createKeyPair(array(
            'KeyName' => ''.$name_pair.'',
        ));
    } catch (AwsException $e) {
        aws_handle_exception($e, "[Ec2] Create Key Pair");
        return false;
    }

    if (empty($result['KeyMaterial'])) {
        return 0;
    }
    $contents = $result['KeyMaterial'];

    $saveKeyLocation = "{$savePath}/{$name_pair}.pem";
    $contents = encryptIt($contents);
    $bytesWritten = file_put_contents($saveKeyLocation, $contents);
    chmod($saveKeyLocation, 0400);

    return $bytesWritten;
}
