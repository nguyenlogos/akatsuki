<?php
use Aws\Exception\AwsException as AwsException;

function updateAZ($cid, $smarty, $config)
{
    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1 || $cid == "") {
        return -2;
    }

    if ($config["credentials"]["key"] == "" || $config["credentials"]["secret"] == "") {
        return (-2);
    }


    $sql = "DELETE FROM azone WHERE cid = " . $cid;
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    // $BCO0h$r<hF@(B
    try {
        $ec2Client = new Aws\Ec2\Ec2Client($config);
        $arg = array();
        $result = $ec2Client->describeRegions($arg);
    } catch (AwsException $e) {
        aws_handle_exception($e, "[EC2] Describe Regions");
        return (-1);
    }

    // $B<hF@$7$?CO0h$4$H$K(BAZ$B$r<hF@(B
    $regions = $result['Regions'];
    foreach ($regions as $region) {
        $config['region'] = $region['RegionName'];
        try {
            $ec2Client = new Aws\Ec2\Ec2Client($config);
            $arg = array();
            $result = $ec2Client->describeAvailabilityZones($arg);
        } catch (AwsException $e) {
            aws_handle_exception($e, "Describe Availability Zones, RegionName=[{$config['region']}]");
            return (-1);
        }

        $zones = $result['AvailabilityZones'];
        foreach ($zones as $zone) {
            $sql = "INSERT INTO azone (cid,name,state,regionname,lastchecked) VALUES (" . $cid . ",";
            $sql .= "'" . $zone["ZoneName"] . "',";
            $sql .= "'" . $zone["State"] . "',";
            $sql .= "'" . $zone["RegionName"] . "',";
            $sql .= "now());";
            $logs[] = $sql;
            $r = pg_query($smarty->_db, $sql);
        }
    }
    raise_sql($logs, 'if_updateAZ');

    return(0);
}
