<?php
use Aws\Exception\AwsException as AwsException;

function updateSS($cid, $acctid, $smarty, $config)
{

    if ($cid == "" || $acctid == "") { return (-2); }

    if ($config["credentials"]["key"] == "" || $config["credentials"]["secret"] == "" || $config["region"] == "") {
        return (-2);
    }

    try {
        $ec2Client = new Aws\Ec2\Ec2Client($config);

        $result = $ec2Client->DescribeSnapshots(['OwnerIds' => [ $acctid ]]);
    } catch (AwsException $e) {
        aws_handle_exception($e, "[EC2] Describe Snapshots");
        return (-1);
    }

    $sql = "DELETE FROM volumes WHERE cid = " . $cid . " AND stragetype = 'SS'";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $snaps = $result['Snapshots'];
    $i = 0;

    foreach ($snaps as $snap) {
        $sql = "INSERT INTO volumes (cid, state, volid, createtime, encrypt, size, snapshotid, tag_name, progress, description, stragetype) VALUES (" . $cid . ",";
        $sql .= setStrForDB($snap['State'], true);
        $sql .= setStrForDB($snap['VolumeId'],true);
        $sql .= setStrForDB($snap['StartTime'],true);
        if ($snap['Encrypted'] == true) { $sql .= "1,";}
        else { $sql .= "0,"; }
        $sql .= $snap['VolumeSize'] . ",";
        $sql .= setStrForDB($snap['SnapshotId'], true);
        if (array_key_exists('Tags', $snap) == true) {
            $tmp = "";
            for ($i = 0; $i < count($snap['Tags']); $i++) {
                if ($snap['Tags'][$i]['Key'] == "Name") {
                    $tmp = $snap['Tags'][$i]['Value'];
                    break;
                }
            }
            $sql .= setStrForDB($tmp, true);
        } else {
            $sql .= "null,";
        }
        $sql .= setStrForDB($snap['Progress'], true);
        $sql .= setStrForDB($snap['Description'], true);
        $sql .= "'SS')";
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);
    }
    raise_sql($logs, 'if_updateSS');

    return(0);
}
