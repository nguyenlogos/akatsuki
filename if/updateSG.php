<?php
use Aws\Exception\AwsException as AwsException;
use Akatsuki\Models\SecurityGroup;

function updateSG($smarty, $config)
{
    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1) {
        return -2;
    }

    try {
        $ec2Client = new Aws\Ec2\Ec2Client($config);
        $result = $ec2Client->describeSecurityGroups();
    } catch (AwsException $e) {
        aws_handle_exception($e, "[EC2] Describe Security Groups");
        return (-1);
    }

    $sql = "DELETE FROM sg WHERE cid = " . $_SESSION["cid"];
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $sql = "DELETE FROM sg_ipperm WHERE cid = " . $_SESSION["cid"];
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $SecurityGroups = $result['SecurityGroups'];

    foreach ($SecurityGroups as $sg) {
        $sql = '
            INSERT INTO sg(cid, id, name, lastchecked, description)
            VALUES ($1, $2, $3, $4, $5)
        ';
        $bindingParams = [
            $_SESSION["cid"],
            $sg['GroupId'],
            $sg['GroupName'],
            "now()",
            $sg['Description']
        ];
        $result = pg_query_params($smarty->_db, $sql, $bindingParams);

        $IpPermissions = $sg['IpPermissions'];
        foreach ($IpPermissions as $Rules) {
            $FromPort = !empty($Rules['FromPort']) ? $Rules['FromPort'] : null;
            $ToPort = !empty($Rules['ToPort']) ? $Rules['ToPort'] : null;
            $IpRanges = $Rules['IpRanges'];
            foreach ($IpRanges as $Range) {
                $sql = '
                    INSERT INTO sg_ipperm(
                        cid, id, protocol,
                        fromport, toport, iprange, lastchecked
                    ) VALUES (
                        $1, $2, $3,
                        $4, $5, $6, $7
                    );
                ';
                $bindingParams = [
                    $_SESSION["cid"], $sg['GroupId'], $Rules['IpProtocol'],
                    $FromPort, $ToPort, $Range['CidrIp'], "now()"
                ];
                $result = pg_query_params($smarty->_db, $sql, $bindingParams);
            }
        }
    }
    raise_sql($logs, 'if_updateSG');

    return (0);
}

function CreateSecurityGroup($smarty, $config, $cid, $regId)
{
    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1 || $cid == "") {
        return -2;
    }

    if (empty($config)) {
        $client = getEc2Client($smarty, $cid, 'ap-northeast-1');
        if (!$client) {
            return -2;
        }
    } elseif ($config["credentials"]["key"] == "" ||
        $config["credentials"]["secret"] == "" ||
        $config["region"] == ""
    ) {
        return (-2);
    } else {
        $client = new Aws\Ec2\Ec2Client($config);
    }

    $SecurityGroup = SecurityGroup::where("inst_req_id", $regId)
                                  ->where("cid", $cid)
                                  ->first();
    if (!$SecurityGroup) {
        return null;
    }
    if (empty($SecurityGroup->sg_id)) {
        $securityGroups = \Akatsuki\Models\Sg::updateSecurityGroups($cid);
        $securityGroups = array_filter($securityGroups, function ($sg) use ($SecurityGroup) {
            return $SecurityGroup->name_sg == $sg['name'] && $SecurityGroup->vpc_sg == $sg['vpcid'];
        });

        if (count($securityGroups) === 0) {
            try {
                $result = $client->createSecurityGroup([
                    'Description' => '' . $SecurityGroup->description_sg . '',
                    'GroupName'   => '' . $SecurityGroup->name_sg . '',
                    'VpcId'       => '' . $SecurityGroup->vpc_sg . '',
                ]);
                $SecurityGroup->setValues([
                    'sg_id' => $result['GroupId']
                ]);
                $SecurityGroup->save();
            } catch (AwsException $e) {
                aws_handle_exception($e, "[EC2] Create Security Groups");
                return null;
            }
        } else {
            $securityGroup = array_shift($securityGroups);
            return $securityGroup['id'];
        }

        //query inbound
        $sql = 'SELECT * FROM sg_inbound WHERE id_security_group = $1';
        $data_intb = pg_query_params($smarty->_db, $sql, [
            $SecurityGroup->id
        ]);
        $inbound = pg_fetch_all($data_intb);

        //query outbound
        $out = 'SELECT * FROM sg_outbound WHERE id_security_group = $1';
        $data_outb = pg_query_params($smarty->_db, $out, [
            $SecurityGroup->id
        ]);
        $outbound = pg_fetch_all($data_outb);
        $sgID = '';

        if ($inbound) {
            $ListNewsInbound = [];
            $sgID = $result['GroupId'];

            if ($inbound) {
                foreach ($inbound as $intb => $key) {
                    $ListNewsInbound[] = [
                        'FromPort' => '' . $key["inb_formport_range"] . '',
                        'IpProtocol' => '' . $key["inb_protocol"] . '',
                        'IpRanges' => [
                            [
                                'CidrIp' => '' . $key['inb_source'] . '',
                                'Description' => '' . $key['inb_description'] . '',
                            ],
                        ],
                        'ToPort' => '' . $key["inb_toport_range"] . '',
                    ];
                }
            }

            try {
                $client->AuthorizeSecurityGroupIngress([
                    'GroupId' => '' . $sgID . '',
                    'IpPermissions' => $ListNewsInbound
                ]);
            } catch (AwsException $e) {
                aws_handle_exception($e, "[EC2] Create Inbound and Outbound In Security Groups");
                return (-1);
            }
        }

        if ($outbound) {
            $ListNewsOutbound = [];
            $sgID = $result['GroupId'];

            if ($outbound) {
                foreach ($outbound as $outb => $key_out) {
                    $ListNewsOutbound[] = [
                        'FromPort' => '' . $key_out["outb_formport_range"] . '',
                        'IpProtocol' => '' . $key_out["outb_protocol"] . '',
                        'IpRanges' => [
                            [
                                'CidrIp' => '' . $key_out['outb_destination'] . '',
                                'Description' => '' . $key_out['outb_description'] . '',
                            ],
                        ],
                        'ToPort' => '' . $key_out["outb_toport_range"] . '',
                    ];
                }
            }

            try {
                $client->AuthorizeSecurityGroupEgress([
                    'GroupId' => '' . $sgID . '',
                    'IpPermissions' => $ListNewsOutbound
                ]);
            } catch (AwsException $e) {
                aws_handle_exception($e, "[EC2] Create Inbound and Outbound In Security Groups");
                return (-1);
            }
        }

        return $result['GroupId'];
    } else {
        return $SecurityGroup->sg_id;
    }
}
