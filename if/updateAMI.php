<?php
use Aws\Exception\AwsException as AwsException;
use Common\Logger;

function updateAMI($cid, $smarty, $config)
{
    if (!empty($_SESSION['ERR_AWS_KEY']) && $_SESSION['ERR_AWS_KEY'] == 1 || $cid == "") {
        return -2;
    }
    if ($config["credentials"]["key"] == "" || $config["credentials"]["secret"] == "" || $config["region"] == "") {
        return (-2);
    }
    try {
        $ec2Client = new Aws\Ec2\Ec2Client($config);
        $arg = array(
            'Owners' => array('self')
        );
        $result = $ec2Client->DescribeImages($arg);
    } catch (AwsException $e) {
        aws_handle_exception($e, "[EC2] Describe Images");
        return (-1);
    }

    $sql = "DELETE FROM ami WHERE cid = {$cid} AND (region IS NULL OR region = '{$config['region']}');";
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);
    $sql = "DELETE FROM ami_tags WHERE cid = " . $cid;
    $logs[] = $sql;
    $r = pg_query($smarty->_db, $sql);

    $images = $result['Images'];
    $i = 0;

    foreach ($images as $image) {
        if (array_rap('Platform', $image) == '') {
            $image['Platform'] = 'Other Linux';
        } elseif (array_rap('Platform', $image) == 'windows') {
            $image['Platform'] = 'Windows';
        }

        $sql = "INSERT INTO ami (
                cid, imageid, imagelocation, state,
                ownerid, public, platform, architecture,
                imagetype, kernelid, ramdiskid, imageowneralias,
                name, description, rootdevicetype, rootdevicename,
                virtualizationtype, hypervisor, lastchecked, region
            ) VALUES (" . $cid . ",";
        $keyname = array(
            'ImageId',
            'ImageLocation',
            'State',
            'OwnerId',
            'Public',
            'Platform',
            'Architecture',
            'ImageType',
            'KernelId',
            'RamdiskId',
            'ImageOwnerAlias',
            'Name',
            'Description',
            'RootDeviceType',
            'RootDeviceName',
            'VirtualizationType',
            'Hypervisor'
        );
        for ($i = 0; $i < count($keyname); $i++) {
            $sql .= "'" . array_rap($keyname[$i], $image) . "',";
        }
        $sql .= "now(), '{$config['region']}');";
        $logs[] = $sql;
        $r = pg_query($smarty->_db, $sql);

        if (array_rap('Tags', $image) != '') {
            // Tags$B$N3JG<(B
            $tags = $image['Tags'];
            foreach ($tags as $tag) {
                $sql = "INSERT INTO ami_tags (cid, imageid, key, value) VALUES (" . $cid . ",";
                $sql .= "'" . array_rap('ImageId', $image) . "',";
                $sql .= "'" . $tag['Key'] . "',";
                $sql .= "'" . $tag['Value'] . "');";
                $logs[] = $sql;
                $r = pg_query($smarty->_db, $sql);
            }
        }
    }
    raise_sql($logs, 'if_updateAMI');

    return(0);
}

function getAMIdetail($ami)
{
    $config = array(
        'credentials' => [
            'key'    => $_SESSION["key"],
            'secret' => $_SESSION["secret"],
        ],
        'region' => $_SESSION["region"],
        'version' => AWS_SDK_VERSION
    );

    $ret = array();

    try {
        $ec2Client = new Aws\Ec2\Ec2Client($config);

        $arg = array(
            'Owners' => array('self'),
            'Filters' => array(
               array('Name' => 'image-id', 'Values' => array($ami)))
        );
        $result = $ec2Client->DescribeImages($arg);
    } catch (AwsException $e) {
        aws_handle_exception($e, "Describe Images, ImageId=[{$ami}]");
        return (-1);
    }

    $image = $result['Images'][0];

    $ret['KernelId'] = $image['KernelId'];
    $ret['RamdiskId'] = $image['Ramdiskid'];
    $ret['Platform'] = $image['Platform'];
    $ret['RootDeviceType'] = $image['RootDeviceType'];
    $ret['RootDeviceName'] = $image['RootDeviceName'];
    $ret['BlockDeviceMappings'] = $image['BlockDeviceMappings'];

    return($ret);
}


function getAmiDetails($ec2Client, $ami)
{
    try {
        $arg = array(
            'Owners' => array('self'),
            'Filters' => array(
               array('Name' => 'image-id', 'Values' => array($ami)))
        );
        $result = $ec2Client->DescribeImages($arg);
    } catch (AwsException $e) {
        aws_handle_exception($e, "Describe Images, ImageId=[{$ami}]");
        return (-1);
    }

    $image = array_pop($result['Images']) ?: [];
    $keys = ['KernelId','RamdiskId','Platform','RootDeviceType','RootDeviceName','BlockDeviceMappings'];
    $ret = [];
    foreach ($keys as $key) {
        $ret[$key] = !empty($image[$key]) ? $image[$key] : null;
    }
    return($ret);
}

/**
 * Gets the size ami.
 *
 * @param <type> $ami The ami
 *
 * @return integer  The size ami.
 */
function getSizeAmi($ami)
{
    $total = 0;
    $amiDetail = getAMIdetail($ami);
    foreach ($amiDetail['BlockDeviceMappings'] as $value) {
        if (isset($value['Ebs']['VolumeSize'])) {
            $total += $value['Ebs']['VolumeSize'];
        }
    }
    return $total;
}
