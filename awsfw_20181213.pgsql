--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_configs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.admin_configs (
    id integer NOT NULL,
    key character varying(64) NOT NULL,
    value character varying(256)
);


ALTER TABLE public.admin_configs OWNER TO postgres;

--
-- Name: admin_configs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.admin_configs_id_seq
    START WITH 6
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_configs_id_seq OWNER TO postgres;

--
-- Name: admin_configs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.admin_configs_id_seq OWNED BY public.admin_configs.id;


--
-- Name: ami; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ami (
    cid integer,
    imageid character varying(128),
    imagelocation character varying(1024),
    state character varying(256),
    ownerid character varying(128),
    public character varying(32),
    platform character varying(32),
    architecture character varying(32),
    imagetype character varying(32),
    kernelid character varying(64),
    ramdiskid character varying(64),
    imageowneralias character varying(64),
    name character varying(1024),
    description character varying(1024),
    rootdevicetype character varying(64),
    rootdevicename character varying(64),
    virtualizationtype character varying(64),
    hypervisor character varying(64),
    lastchecked timestamp without time zone,
    region character varying(128)
);


ALTER TABLE public.ami OWNER TO postgres;

--
-- Name: ami_enable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ami_enable (
    cid integer NOT NULL,
    imageid character varying(128) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.ami_enable OWNER TO postgres;

--
-- Name: ami_enable_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ami_enable_id_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ami_enable_id_seq OWNER TO postgres;

--
-- Name: ami_enable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ami_enable_id_seq OWNED BY public.ami_enable.id;


--
-- Name: ami_tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ami_tags (
    cid integer,
    imageid character varying(128),
    key character varying(512),
    value character varying(512)
);


ALTER TABLE public.ami_tags OWNER TO postgres;

--
-- Name: azone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.azone (
    cid integer,
    name character varying(128),
    state character varying(32),
    regionname character varying(128),
    lastchecked timestamp without time zone
);


ALTER TABLE public.azone OWNER TO postgres;

--
-- Name: azone_enable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.azone_enable (
    cid integer NOT NULL,
    regionname character varying(128) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.azone_enable OWNER TO postgres;

--
-- Name: azone_enable_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.azone_enable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.azone_enable_id_seq OWNER TO postgres;

--
-- Name: azone_enable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.azone_enable_id_seq OWNED BY public.azone_enable.id;


--
-- Name: configs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.configs (
    id integer NOT NULL,
    cid integer NOT NULL,
    insttype_curgen smallint DEFAULT 0 NOT NULL,
    pass_lenght integer DEFAULT 4 NOT NULL,
    pass_expired integer DEFAULT 0 NOT NULL,
    pass_lowercase integer DEFAULT 0 NOT NULL,
    pass_number integer DEFAULT 0 NOT NULL,
    mail_address character varying(255),
    admin_active integer DEFAULT 600 NOT NULL,
    lock_failed integer DEFAULT 5 NOT NULL,
    instance_expires integer DEFAULT 30 NOT NULL,
    check_sys_admin integer DEFAULT 1 NOT NULL,
    check_manager integer DEFAULT 1 NOT NULL,
    email_instance integer DEFAULT 0 NOT NULL,
    timezone character varying DEFAULT 9,
    timezoneid smallint DEFAULT 69,
    days_apply smallint DEFAULT '0'::smallint NOT NULL
);


ALTER TABLE public.configs OWNER TO postgres;

--
-- Name: configs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.configs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configs_id_seq OWNER TO postgres;

--
-- Name: configs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.configs_id_seq OWNED BY public.configs.id;


--
-- Name: cost; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cost (
    cid integer,
    lineitemtype character varying(64),
    usagestartdate timestamp without time zone,
    usageenddate timestamp without time zone,
    productcode character varying(64),
    usagetype1 character varying(64),
    operation1 character varying(64),
    resourceid character varying(256),
    usageamount numeric,
    currencycode character varying(64),
    unblendedcost numeric,
    blendedcost numeric,
    lineitemdescription character varying(512),
    instancetype character varying(64),
    location character varying(128),
    memory character varying(64),
    networkperformance character varying(64),
    normalizationsizefactor character varying(64),
    operatingsystem character varying(64),
    operation2 character varying(64),
    physicalprocessor character varying(64),
    preinstalledsw character varying(256),
    processorarchitecture character varying(64),
    processorfeatures character varying(64),
    region character varying(64),
    servicename character varying(128),
    transfertype character varying(128),
    usagetype2 character varying(128),
    publicondemandcost numeric,
    publicondemandrate numeric,
    term character varying(64),
    unit character varying(128),
    usageaccountid character varying(64),
    servicecode character varying(128),
    productfamily character varying(128),
    unblendedrate numeric,
    blendedrate numeric,
    loaddate timestamp without time zone,
    ondemandusagecost numeric,
    riusagecost numeric,
    riflag smallint DEFAULT 0,
    pricingpurchaseoption character varying(128),
    reservationupfrontvalue numeric,
    reservationstarttime timestamp without time zone,
    rateid character varying(64)
);


ALTER TABLE public.cost OWNER TO postgres;

--
-- Name: cost_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cost_detail (
    cid integer,
    ym character varying(7),
    lineitemtype character varying(64),
    region character varying(64),
    productcode character varying(64),
    productfamily character varying(128),
    lineitemdescription character varying(512),
    preinstalledsw character varying(256),
    operatingsystem character varying(64),
    unit character varying(128),
    unblendedcost numeric,
    blendedcost numeric,
    usageamount numeric,
    unblendedrate numeric,
    blendedrate numeric,
    usagetype character varying(64),
    org_unblendedcost numeric,
    org_blendedcost numeric,
    org_usageamount numeric,
    billingentity character varying(128)
);


ALTER TABLE public.cost_detail OWNER TO postgres;

--
-- Name: cost_ri; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cost_ri (
    accountname character varying(64),
    subscriptionid character varying(256),
    reservationid character varying(256),
    instancetype character varying(64),
    riutilization numeric,
    rihourspurchased numeric,
    rihoursused numeric,
    rihoursunused numeric,
    accountid character varying(256),
    startdate timestamp without time zone,
    enddate timestamp without time zone,
    numberofris numeric,
    scope character varying(64),
    region character varying(64),
    availabilityzone character varying(64),
    platform character varying(64),
    tenancy character varying(64),
    paymentoption character varying(64),
    offeringtype character varying(64),
    ondemandcostequivalent numeric,
    amortizedupfrontfees numeric,
    amortizedrecurringcharges numeric,
    effectivericost numeric,
    netsavings numeric,
    potentialsavings numeric,
    averageondemandrate numeric,
    totalassetvalue numeric,
    effectivehourlyrate numeric,
    upfrontfee numeric,
    hourlyrecurringfee numeric,
    ricostforunusedhours numeric
);


ALTER TABLE public.cost_ri OWNER TO postgres;

--
-- Name: cw_logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cw_logs (
    id integer NOT NULL,
    cid integer NOT NULL,
    stream character varying(64) NOT NULL,
    stream_time timestamp without time zone NOT NULL,
    event text NOT NULL,
    event_time timestamp without time zone NOT NULL,
    date_created timestamp without time zone NOT NULL,
    error_flag smallint DEFAULT 0,
    description text
);


ALTER TABLE public.cw_logs OWNER TO postgres;

--
-- Name: cw_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cw_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cw_logs_id_seq OWNER TO postgres;

--
-- Name: cw_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cw_logs_id_seq OWNED BY public.cw_logs.id;


--
-- Name: cw_rules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cw_rules (
    id integer NOT NULL,
    cid integer NOT NULL,
    name character varying NOT NULL,
    expr character varying(512) NOT NULL,
    region character varying(32) NOT NULL,
    state smallint DEFAULT 1 NOT NULL,
    notes text,
    status integer DEFAULT 0,
    dept integer NOT NULL
);


ALTER TABLE public.cw_rules OWNER TO postgres;

--
-- Name: cw_rules_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cw_rules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cw_rules_id_seq OWNER TO postgres;

--
-- Name: cw_rules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cw_rules_id_seq OWNED BY public.cw_rules.id;


--
-- Name: dept; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dept (
    cid integer,
    dept integer,
    deptname character varying(128) NOT NULL,
    regdate timestamp without time zone,
    status integer DEFAULT 0,
    disporder integer,
    id integer NOT NULL
);


ALTER TABLE public.dept OWNER TO postgres;

--
-- Name: dept_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dept_id_seq
    START WITH 13
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dept_id_seq OWNER TO postgres;

--
-- Name: dept_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dept_id_seq OWNED BY public.dept.id;


--
-- Name: ebs_change_req; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ebs_change_req (
    id integer NOT NULL,
    cid integer NOT NULL,
    instance_id character varying(32) NOT NULL,
    volume_id character varying(32) NOT NULL,
    volume_zone character varying(32) NOT NULL,
    volume_type character varying(16) NOT NULL,
    volume_size integer NOT NULL,
    volume_iops integer NOT NULL,
    device_name character varying(16) NOT NULL,
    state character varying(16) NOT NULL,
    attached integer NOT NULL,
    autoremove integer NOT NULL,
    change_type character varying(16) NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ebs_change_req OWNER TO postgres;

--
-- Name: ebs_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ebs_log (
    id integer NOT NULL,
    instid character varying(32) NOT NULL,
    ebs_req_id integer NOT NULL,
    size integer NOT NULL,
    iops integer NOT NULL,
    zone character varying(32) NOT NULL,
    state character varying(32) NOT NULL,
    type character varying(16) NOT NULL,
    device character varying(32) NOT NULL,
    is_root smallint NOT NULL,
    encrypted smallint NOT NULL,
    del_term smallint NOT NULL,
    can_modify smallint NOT NULL,
    attached smallint NOT NULL
);


ALTER TABLE public.ebs_log OWNER TO postgres;

--
-- Name: ebs_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ebs_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ebs_log_id_seq OWNER TO postgres;

--
-- Name: ebs_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ebs_log_id_seq OWNED BY public.ebs_log.id;


--
-- Name: ebs_req; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ebs_req (
    id integer NOT NULL,
    cid integer NOT NULL,
    empid integer NOT NULL,
    instance_id character varying(32) NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    note character varying(512)
);


ALTER TABLE public.ebs_req OWNER TO postgres;

--
-- Name: ebs_req_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ebs_req_detail (
    id integer NOT NULL,
    ebs_req_id integer NOT NULL,
    volume_id character varying(32) NOT NULL,
    volume_zone character varying(32) NOT NULL,
    volume_type character varying(16) NOT NULL,
    volume_size integer NOT NULL,
    volume_iops integer DEFAULT 0,
    device_name character varying(16) NOT NULL,
    state character varying(16) NOT NULL,
    attached integer DEFAULT 0,
    autoremove integer DEFAULT 0,
    change_type character varying(16) NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.ebs_req_detail OWNER TO postgres;

--
-- Name: ebs_req_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ebs_req_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ebs_req_detail_id_seq OWNER TO postgres;

--
-- Name: ebs_req_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ebs_req_detail_id_seq OWNED BY public.ebs_req_detail.id;


--
-- Name: ebs_req_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ebs_req_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ebs_req_id_seq OWNER TO postgres;

--
-- Name: ebs_req_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ebs_req_id_seq OWNED BY public.ebs_req.id;


--
-- Name: emp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.emp (
    cid integer,
    empid integer,
    email character varying(64),
    pass character varying(64),
    dept integer,
    name character varying(64),
    memo1 character varying(128),
    memo2 character varying(128),
    regdate timestamp without time zone,
    lastlogin timestamp without time zone,
    status integer,
    regkey character varying(32),
    sysadmin integer,
    admin integer,
    disporder integer,
    infocount integer,
    wfcount integer,
    idsadmin integer DEFAULT 0 NOT NULL,
    id integer NOT NULL,
    email_confirmed integer DEFAULT 0,
    pw_reset_token character varying(32),
    pw_reset_token_time timestamp without time zone,
    login_initial integer DEFAULT 0,
    number_lock integer DEFAULT 0,
    email_lock integer DEFAULT 0,
    show_privacy integer DEFAULT 1
);


ALTER TABLE public.emp OWNER TO postgres;

--
-- Name: emp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.emp_id_seq
    START WITH 142
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emp_id_seq OWNER TO postgres;

--
-- Name: emp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.emp_id_seq OWNED BY public.emp.id;


--
-- Name: emp_proj; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.emp_proj (
    id integer NOT NULL,
    cid integer NOT NULL,
    empid integer NOT NULL,
    pcode character varying(128) NOT NULL
);


ALTER TABLE public.emp_proj OWNER TO postgres;

--
-- Name: emp_proj_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.emp_proj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.emp_proj_id_seq OWNER TO postgres;

--
-- Name: emp_proj_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.emp_proj_id_seq OWNED BY public.emp_proj.id;


--
-- Name: info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.info (
    rank integer,
    senddate timestamp without time zone,
    title character varying(256),
    message text,
    toall integer,
    mid integer NOT NULL,
    id_notif integer
);


ALTER TABLE public.info OWNER TO postgres;

--
-- Name: info_ctl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.info_ctl (
    cid integer,
    uid character varying(64),
    mid integer,
    readdate timestamp without time zone
);


ALTER TABLE public.info_ctl OWNER TO postgres;

--
-- Name: info_mid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.info_mid_seq
    START WITH 33
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.info_mid_seq OWNER TO postgres;

--
-- Name: info_mid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.info_mid_seq OWNED BY public.info.mid;


--
-- Name: inst_req; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inst_req (
    id integer NOT NULL,
    cid integer NOT NULL,
    dept integer NOT NULL,
    pcode character varying(128) NOT NULL,
    inst_ami character varying(128) NOT NULL,
    inst_type character varying(32) NOT NULL,
    inst_count integer NOT NULL,
    inst_region character varying(128) NOT NULL,
    security_groups character varying(256),
    using_purpose character varying(512) NOT NULL,
    using_from_date date NOT NULL,
    using_till_date date NOT NULL,
    shutdown_behavior character varying(16) NOT NULL,
    approved_status smallint DEFAULT 0 NOT NULL,
    req_date timestamp without time zone NOT NULL,
    empid integer,
    note character varying(512),
    inst_vpc character varying(128),
    inst_subnet character varying(128),
    expired_notified smallint DEFAULT 0,
    key_name character varying(128),
    inst_address character varying(32),
    status smallint,
    inst_region_text character varying(128),
    inst_vpc_text character varying(128),
    inst_subnet_text character varying(128),
    inst_ami_text character varying(128)
);


ALTER TABLE public.inst_req OWNER TO postgres;

--
-- Name: inst_req_bs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inst_req_bs (
    id integer NOT NULL,
    inst_req_id integer NOT NULL,
    device_name character varying(32) NOT NULL,
    volumn_type character varying(16) NOT NULL,
    volumn_size integer NOT NULL,
    iops integer,
    encrypted smallint DEFAULT 0,
    auto_removal smallint DEFAULT 1,
    snapshot_id character varying(32)
);


ALTER TABLE public.inst_req_bs OWNER TO postgres;

--
-- Name: inst_req_bs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inst_req_bs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inst_req_bs_id_seq OWNER TO postgres;

--
-- Name: inst_req_bs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inst_req_bs_id_seq OWNED BY public.inst_req_bs.id;


--
-- Name: inst_req_change; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inst_req_change (
    id integer NOT NULL,
    inst_req_id integer NOT NULL,
    cid integer NOT NULL,
    empid integer NOT NULL,
    dept integer,
    pcode character varying(128),
    using_purpose character varying(512),
    using_from_date date NOT NULL,
    using_till_date date NOT NULL,
    inst_address character varying(32)
);


ALTER TABLE public.inst_req_change OWNER TO postgres;

--
-- Name: inst_req_change_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inst_req_change_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inst_req_change_id_seq OWNER TO postgres;

--
-- Name: inst_req_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inst_req_change_id_seq OWNED BY public.inst_req_change.id;


--
-- Name: inst_req_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inst_req_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inst_req_id_seq OWNER TO postgres;

--
-- Name: inst_req_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inst_req_id_seq OWNED BY public.inst_req.id;


--
-- Name: instance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instance (
    cid integer,
    id character varying(128),
    name character varying(256),
    type character varying(64),
    sg character varying(128),
    platform character varying(128),
    kernelid character varying(128),
    ramdiskid character varying(128),
    imageid character varying(128),
    state character varying(128),
    privatednsname character varying(256),
    publicdnsname character varying(256),
    launchtime character varying(128),
    availabilityzone character varying(256),
    privateip character varying(64),
    publicip character varying(64),
    rootdevicename character varying(128),
    lastchecked timestamp without time zone,
    pcode character varying(128),
    inst_req_id integer,
    elasticip character varying(64),
    tbl_id integer NOT NULL,
    interface_id character varying(64)
);


ALTER TABLE public.instance OWNER TO postgres;

--
-- Name: instance_request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.instance_request (
    cid integer,
    reqid integer,
    instance_id integer,
    createdate timestamp without time zone,
    deletedate timestamp without time zone
);


ALTER TABLE public.instance_request OWNER TO postgres;

--
-- Name: instance_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.instance_tbl_id_seq
    START WITH 293
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instance_tbl_id_seq OWNER TO postgres;

--
-- Name: instance_tbl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.instance_tbl_id_seq OWNED BY public.instance.tbl_id;


--
-- Name: insttype; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.insttype (
    curgen character varying(3),
    family character varying(32),
    name character varying(32),
    cpu character varying(3),
    memory character varying(5),
    storage character varying(32),
    ebsopt character varying(3),
    netperf character varying(32),
    cid integer,
    price numeric DEFAULT 0,
    region character varying(128),
    effdate date,
    id integer NOT NULL
);


ALTER TABLE public.insttype OWNER TO postgres;

--
-- Name: insttype_enable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.insttype_enable (
    cid integer NOT NULL,
    name character varying(32) NOT NULL,
    id integer NOT NULL,
    region character varying(128)
);


ALTER TABLE public.insttype_enable OWNER TO postgres;

--
-- Name: insttype_enable_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insttype_enable_id_seq
    START WITH 6
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insttype_enable_id_seq OWNER TO postgres;

--
-- Name: insttype_enable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insttype_enable_id_seq OWNED BY public.insttype_enable.id;


--
-- Name: insttype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insttype_id_seq
    START WITH 377
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insttype_id_seq OWNER TO postgres;

--
-- Name: insttype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insttype_id_seq OWNED BY public.insttype.id;


--
-- Name: insttype_pricing; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.insttype_pricing (
    id integer,
    cid integer,
    name character varying(128),
    region character varying(128),
    price numeric
);


ALTER TABLE public.insttype_pricing OWNER TO postgres;

--
-- Name: insttype_temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.insttype_temp (
    id integer NOT NULL,
    name character varying(32),
    region character varying(128),
    family character varying(32),
    cpu character varying(3),
    memory numeric,
    storage character varying(32),
    os character varying(64),
    netperf character varying(32),
    curgen character varying(3),
    ebsopt character varying(3),
    termtype character varying(64),
    effdate date,
    price numeric,
    price_desc character varying(255)
);


ALTER TABLE public.insttype_temp OWNER TO postgres;

--
-- Name: insttype_temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.insttype_temp_id_seq
    START WITH 1201013
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insttype_temp_id_seq OWNER TO postgres;

--
-- Name: insttype_temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.insttype_temp_id_seq OWNED BY public.insttype_temp.id;


--
-- Name: invoice2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.invoice2 (
    costacctid character varying(64),
    ym character varying(6),
    refno character varying(11),
    usageusd numeric,
    usagejpy numeric,
    creditusd numeric,
    creditjpy numeric,
    taxusd numeric,
    taxjpy numeric,
    totalusd numeric,
    totaljpy numeric,
    rate numeric,
    filename character varying(128),
    refnoserial integer,
    idsfee_usd numeric,
    idsfee_jpy numeric,
    internaluse boolean,
    companyname character varying(128),
    dept character varying(64),
    "position" character varying(64),
    zip character varying(10),
    address1 character varying(128),
    address2 character varying(128),
    address3 character varying(128),
    name character varying(64),
    tel character varying(64),
    email character varying(64),
    calcdate timestamp without time zone,
    paid boolean,
    howtosend character varying(12),
    payeracctid character varying(64),
    payday integer,
    duedate timestamp without time zone,
    cid integer,
    chk1 boolean,
    chk2 boolean,
    invoicetype integer,
    costacctname character varying(128),
    discountrate numeric,
    basecharge integer,
    totalusdondemand numeric,
    riflag integer
);


ALTER TABLE public.invoice2 OWNER TO postgres;

--
-- Name: key_pair; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.key_pair (
    id integer NOT NULL,
    cid integer NOT NULL,
    name_key character varying NOT NULL,
    file_key_pair character varying(512) NOT NULL,
    create_time timestamp without time zone
);


ALTER TABLE public.key_pair OWNER TO postgres;

--
-- Name: key_pair_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.key_pair_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.key_pair_id_seq OWNER TO postgres;

--
-- Name: key_pair_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.key_pair_id_seq OWNED BY public.key_pair.id;


--
-- Name: logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.logs (
    logdate timestamp without time zone,
    cid integer,
    email character varying(64),
    name character varying(64),
    act text
);


ALTER TABLE public.logs OWNER TO postgres;

--
-- Name: obj; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.obj (
    cid integer NOT NULL,
    name character varying(128) NOT NULL,
    ipaddr character varying(15) NOT NULL,
    submask character varying(2) NOT NULL,
    description character varying(1024),
    id integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.obj OWNER TO postgres;

--
-- Name: obj_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.obj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.obj_id_seq OWNER TO postgres;

--
-- Name: obj_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.obj_id_seq OWNED BY public.obj.id;


--
-- Name: plan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.plan (
    id integer NOT NULL,
    cid integer NOT NULL,
    plan character varying(10) NOT NULL,
    startdate timestamp without time zone,
    enddate timestamp without time zone,
    startuid character varying(64) NOT NULL
);


ALTER TABLE public.plan OWNER TO postgres;

--
-- Name: plan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.plan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plan_id_seq OWNER TO postgres;

--
-- Name: plan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.plan_id_seq OWNED BY public.plan.id;


--
-- Name: proj; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proj (
    cid integer NOT NULL,
    pcode character varying(128) NOT NULL,
    pname character varying(256),
    regdate timestamp without time zone NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    disporder integer,
    memo text,
    dept integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.proj OWNER TO postgres;

--
-- Name: proj_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proj_id_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proj_id_seq OWNER TO postgres;

--
-- Name: proj_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proj_id_seq OWNED BY public.proj.id;


--
-- Name: rate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rate (
    ym character varying(7),
    dy numeric
);


ALTER TABLE public.rate OWNER TO postgres;

--
-- Name: request; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request (
    cid integer,
    reqid integer,
    from_uid character varying(64),
    to_uid character varying(64),
    dept integer,
    reqdate timestamp without time zone,
    reqauthdate timestamp without time zone,
    reqngdate timestamp without time zone,
    reqcanceldate timestamp without time zone,
    from_comment text,
    to_comment text
);


ALTER TABLE public.request OWNER TO postgres;

--
-- Name: request_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.request_detail (
    cid integer,
    reqid integer,
    name character varying(256),
    type character varying(64),
    sg character varying(128),
    platform character varying(128),
    kernelid character varying(128),
    ramdiskid character varying(128),
    imageid character varying(128),
    availabilityzone character varying(256),
    privateip character varying(64),
    publicip character varying(64)
);


ALTER TABLE public.request_detail OWNER TO postgres;

--
-- Name: security_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.security_group (
    id integer NOT NULL,
    cid integer NOT NULL,
    name_sg character varying(255) NOT NULL,
    description_sg character varying(255) NOT NULL,
    vpc_sg character varying(1024) NOT NULL,
    create_time timestamp without time zone,
    inst_req_id integer,
    sg_id character varying
);


ALTER TABLE public.security_group OWNER TO postgres;

--
-- Name: security_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.security_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.security_group_id_seq OWNER TO postgres;

--
-- Name: security_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.security_group_id_seq OWNED BY public.security_group.id;


--
-- Name: sg; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sg (
    cid integer,
    id character varying(128),
    name character varying(128),
    lastchecked timestamp without time zone,
    description character varying,
    tbl_id integer NOT NULL,
    vpcid character varying(32)
);


ALTER TABLE public.sg OWNER TO postgres;

--
-- Name: sg_enable; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sg_enable (
    id integer NOT NULL,
    cid integer NOT NULL,
    sg_id character varying(128) NOT NULL
);


ALTER TABLE public.sg_enable OWNER TO postgres;

--
-- Name: sg_enable_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sg_enable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sg_enable_id_seq OWNER TO postgres;

--
-- Name: sg_enable_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sg_enable_id_seq OWNED BY public.sg_enable.id;


--
-- Name: sg_inbound; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sg_inbound (
    id integer NOT NULL,
    id_security_group integer NOT NULL,
    inb_type character varying(255) NOT NULL,
    inb_protocol character varying(64) NOT NULL,
    inb_cidr_ip character varying(64) NOT NULL,
    inb_source character varying(32) NOT NULL,
    inb_description character varying(1024) NOT NULL,
    create_time timestamp without time zone,
    inb_formport_range character varying(64),
    inb_toport_range character varying(64)
);


ALTER TABLE public.sg_inbound OWNER TO postgres;

--
-- Name: sg_inbound_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sg_inbound_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sg_inbound_id_seq OWNER TO postgres;

--
-- Name: sg_inbound_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sg_inbound_id_seq OWNED BY public.sg_inbound.id;


--
-- Name: sg_ipperm; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sg_ipperm (
    cid integer,
    id character varying(128),
    protocol character varying(64),
    fromport integer,
    toport integer,
    iprange character varying(64),
    lastchecked timestamp without time zone,
    type character varying(16),
    tbl_id integer NOT NULL
);


ALTER TABLE public.sg_ipperm OWNER TO postgres;

--
-- Name: sg_ipperm_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sg_ipperm_tbl_id_seq
    START WITH 5283
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sg_ipperm_tbl_id_seq OWNER TO postgres;

--
-- Name: sg_ipperm_tbl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sg_ipperm_tbl_id_seq OWNED BY public.sg_ipperm.tbl_id;


--
-- Name: sg_outbound; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sg_outbound (
    id integer NOT NULL,
    id_security_group integer NOT NULL,
    outb_type character varying(255) NOT NULL,
    outb_protocol character varying(64) NOT NULL,
    outb_cidr_ip character varying(64) NOT NULL,
    outb_destination character varying(32) NOT NULL,
    outb_description character varying(1024) NOT NULL,
    create_time timestamp without time zone,
    outb_formport_range character varying(64),
    outb_toport_range character varying(64)
);


ALTER TABLE public.sg_outbound OWNER TO postgres;

--
-- Name: sg_outbound_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sg_outbound_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sg_outbound_id_seq OWNER TO postgres;

--
-- Name: sg_outbound_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sg_outbound_id_seq OWNED BY public.sg_outbound.id;


--
-- Name: sg_tbl_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sg_tbl_id_seq
    START WITH 2205
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sg_tbl_id_seq OWNER TO postgres;

--
-- Name: sg_tbl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sg_tbl_id_seq OWNED BY public.sg.tbl_id;


--
-- Name: sg_temp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sg_temp (
    id integer NOT NULL,
    cid integer NOT NULL,
    sg_id character varying(32),
    sg_vpc character varying(32),
    sg_name character varying(128),
    sg_desc character varying(128),
    inst_req_id integer NOT NULL,
    is_created smallint DEFAULT 0
);


ALTER TABLE public.sg_temp OWNER TO postgres;

--
-- Name: sg_temp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sg_temp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sg_temp_id_seq OWNER TO postgres;

--
-- Name: sg_temp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sg_temp_id_seq OWNED BY public.sg_temp.id;


--
-- Name: sg_temp_rules; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sg_temp_rules (
    id integer NOT NULL,
    cid integer NOT NULL,
    sg_temp_id integer NOT NULL,
    type character varying(32) NOT NULL,
    stype character varying NOT NULL,
    proto character varying(32) NOT NULL,
    range character varying(32) NOT NULL,
    source character varying(32) NOT NULL,
    target character varying(64) NOT NULL,
    "desc" character varying(128),
    pfrom integer,
    pto integer
);


ALTER TABLE public.sg_temp_rules OWNER TO postgres;

--
-- Name: sg_temp_rules_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sg_temp_rules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sg_temp_rules_id_seq OWNER TO postgres;

--
-- Name: sg_temp_rules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sg_temp_rules_id_seq OWNED BY public.sg_temp_rules.id;


--
-- Name: stat_cost_daily; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stat_cost_daily (
    cid integer,
    ymd character varying(10),
    servicecode character varying(128),
    ubcost numeric,
    bcost numeric,
    usagequantity numeric,
    currencycode character varying(32),
    estimated boolean,
    dept integer,
    pcode character varying(128)
);


ALTER TABLE public.stat_cost_daily OWNER TO postgres;

--
-- Name: stat_ebs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stat_ebs (
    cid integer,
    ym character varying(7),
    count integer,
    gib numeric,
    dept integer,
    pcode character varying(128)
);


ALTER TABLE public.stat_ebs OWNER TO postgres;

--
-- Name: stat_inst; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stat_inst (
    cid integer,
    ym character varying(7),
    count integer,
    instancetype character varying(128),
    dept integer,
    pcode character varying(128)
);


ALTER TABLE public.stat_inst OWNER TO postgres;

--
-- Name: stat_s3; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stat_s3 (
    cid integer,
    ym character varying(7),
    count integer,
    gib numeric,
    dept integer,
    pcode character varying(128)
);


ALTER TABLE public.stat_s3 OWNER TO postgres;

--
-- Name: tbl_exchangerate; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_exchangerate (
    id integer NOT NULL,
    cid integer NOT NULL,
    empid integer NOT NULL,
    email character varying(64),
    num_rate integer NOT NULL,
    code_usd character varying(16),
    code_jpy character varying(16),
    count_rate numeric,
    status integer DEFAULT 0 NOT NULL,
    date_created date NOT NULL
);


ALTER TABLE public.tbl_exchangerate OWNER TO postgres;

--
-- Name: tbl_exchangerate_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tbl_exchangerate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tbl_exchangerate_id_seq OWNER TO postgres;

--
-- Name: tbl_exchangerate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tbl_exchangerate_id_seq OWNED BY public.tbl_exchangerate.id;


--
-- Name: tbl_lambda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_lambda (
    id integer NOT NULL,
    cid integer,
    pcode character varying(256) NOT NULL,
    inst_id character varying(128) NOT NULL,
    volumn_device character varying(128) NOT NULL,
    volumn_volid character varying(128) NOT NULL,
    volumn_size integer NOT NULL,
    volumn_voltype character varying(32) NOT NULL,
    time_gener character varying(64) NOT NULL,
    hours_gener integer,
    minute_gener integer,
    dai_gener integer,
    week_gener character varying(32) NOT NULL,
    day_gener integer,
    month_gener integer,
    num_gener integer,
    create_time timestamp without time zone,
    next_time timestamp without time zone,
    hours_dai_gener integer,
    minute_dai_gener integer,
    hours_week_gener integer,
    minute_week_gener integer,
    hours_month_gener integer,
    minute_month_gener integer
);


ALTER TABLE public.tbl_lambda OWNER TO postgres;

--
-- Name: tbl_lambda_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tbl_lambda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tbl_lambda_id_seq OWNER TO postgres;

--
-- Name: tbl_lambda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tbl_lambda_id_seq OWNED BY public.tbl_lambda.id;


--
-- Name: tbl_logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_logs (
    id integer NOT NULL,
    cid integer NOT NULL,
    empid integer NOT NULL,
    category character varying(16) NOT NULL,
    action character varying(16),
    description text NOT NULL,
    error_flag smallint DEFAULT 0 NOT NULL,
    date_created timestamp without time zone NOT NULL
);


ALTER TABLE public.tbl_logs OWNER TO postgres;

--
-- Name: tbl_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tbl_logs_id_seq
    START WITH 198
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tbl_logs_id_seq OWNER TO postgres;

--
-- Name: tbl_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tbl_logs_id_seq OWNED BY public.tbl_logs.id;


--
-- Name: tbl_notif; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_notif (
    id integer NOT NULL,
    cid integer NOT NULL,
    empid integer NOT NULL,
    notif_title character varying(128) NOT NULL,
    notif_msg text NOT NULL,
    notif_time timestamp without time zone NOT NULL,
    publish_date timestamp without time zone NOT NULL,
    notif_status integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.tbl_notif OWNER TO postgres;

--
-- Name: tbl_notif_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tbl_notif_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tbl_notif_id_seq OWNER TO postgres;

--
-- Name: tbl_notif_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tbl_notif_id_seq OWNED BY public.tbl_notif.id;


--
-- Name: tbl_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tbl_roles (
    id integer NOT NULL,
    cid integer NOT NULL,
    url character varying(32) NOT NULL,
    action character varying(32) NOT NULL,
    sysadmin smallint NOT NULL,
    sysadmin_role smallint DEFAULT 0 NOT NULL,
    manager smallint NOT NULL,
    manager_role smallint DEFAULT 0 NOT NULL,
    employee smallint NOT NULL,
    employee_role smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.tbl_roles OWNER TO postgres;

--
-- Name: tbl_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tbl_roles_id_seq
    START WITH 146
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tbl_roles_id_seq OWNER TO postgres;

--
-- Name: tbl_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tbl_roles_id_seq OWNED BY public.tbl_roles.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    uid character varying(64) NOT NULL,
    tel character varying(64),
    regkey character varying(32),
    regdate timestamp without time zone,
    cid integer,
    name character varying(64),
    status integer,
    dept character varying(64),
    companyname character varying(128),
    key character varying(64),
    secret character varying(64),
    "position" character varying(64),
    address1 character varying(128),
    address2 character varying(128),
    address3 character varying(128),
    zip character varying(10),
    payday integer,
    id integer NOT NULL,
    costacctid character varying(64),
    parentacctid character varying(64),
    internaluse boolean,
    howtosend character varying(12),
    updated timestamp without time zone,
    s3bucket character varying(128),
    parent boolean,
    pending boolean,
    lastresult integer,
    sumacctid character varying(64),
    invoicetype integer,
    costacctname character varying(128),
    discountrate numeric,
    basecharge integer,
    memo text,
    discount numeric DEFAULT '0'::numeric
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 491
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: users_id_seq1; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_id_seq1
    START WITH 491
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq1 OWNER TO root;

--
-- Name: users_id_seq2; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.users_id_seq2
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq2 OWNER TO root;

--
-- Name: volumes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.volumes (
    cid integer,
    device character varying(128),
    instid character varying(128),
    state character varying(32),
    volid character varying(128),
    delonterm integer,
    azone character varying(128),
    attachtime timestamp without time zone,
    createtime timestamp without time zone,
    encrypt integer,
    size numeric,
    snapshotid character varying(128),
    state2 character varying(32),
    iop numeric,
    tag_name character varying(128),
    voltype character varying(32),
    progress character varying(12),
    description text,
    stragetype character varying(3)
);


ALTER TABLE public.volumes OWNER TO postgres;

--
-- Name: admin_configs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.admin_configs ALTER COLUMN id SET DEFAULT nextval('public.admin_configs_id_seq'::regclass);


--
-- Name: ami_enable id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ami_enable ALTER COLUMN id SET DEFAULT nextval('public.ami_enable_id_seq'::regclass);


--
-- Name: azone_enable id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.azone_enable ALTER COLUMN id SET DEFAULT nextval('public.azone_enable_id_seq'::regclass);


--
-- Name: configs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.configs ALTER COLUMN id SET DEFAULT nextval('public.configs_id_seq'::regclass);


--
-- Name: cw_logs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cw_logs ALTER COLUMN id SET DEFAULT nextval('public.cw_logs_id_seq'::regclass);


--
-- Name: cw_rules id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cw_rules ALTER COLUMN id SET DEFAULT nextval('public.cw_rules_id_seq'::regclass);


--
-- Name: dept id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dept ALTER COLUMN id SET DEFAULT nextval('public.dept_id_seq'::regclass);


--
-- Name: ebs_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ebs_log ALTER COLUMN id SET DEFAULT nextval('public.ebs_log_id_seq'::regclass);


--
-- Name: ebs_req id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ebs_req ALTER COLUMN id SET DEFAULT nextval('public.ebs_req_id_seq'::regclass);


--
-- Name: ebs_req_detail id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ebs_req_detail ALTER COLUMN id SET DEFAULT nextval('public.ebs_req_detail_id_seq'::regclass);


--
-- Name: emp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emp ALTER COLUMN id SET DEFAULT nextval('public.emp_id_seq'::regclass);


--
-- Name: emp_proj id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emp_proj ALTER COLUMN id SET DEFAULT nextval('public.emp_proj_id_seq'::regclass);


--
-- Name: info mid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.info ALTER COLUMN mid SET DEFAULT nextval('public.info_mid_seq'::regclass);


--
-- Name: inst_req id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inst_req ALTER COLUMN id SET DEFAULT nextval('public.inst_req_id_seq'::regclass);


--
-- Name: inst_req_bs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inst_req_bs ALTER COLUMN id SET DEFAULT nextval('public.inst_req_bs_id_seq'::regclass);


--
-- Name: inst_req_change id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inst_req_change ALTER COLUMN id SET DEFAULT nextval('public.inst_req_change_id_seq'::regclass);


--
-- Name: instance tbl_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.instance ALTER COLUMN tbl_id SET DEFAULT nextval('public.instance_tbl_id_seq'::regclass);


--
-- Name: insttype id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insttype ALTER COLUMN id SET DEFAULT nextval('public.insttype_id_seq'::regclass);


--
-- Name: insttype_enable id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insttype_enable ALTER COLUMN id SET DEFAULT nextval('public.insttype_enable_id_seq'::regclass);


--
-- Name: insttype_temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insttype_temp ALTER COLUMN id SET DEFAULT nextval('public.insttype_temp_id_seq'::regclass);


--
-- Name: key_pair id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.key_pair ALTER COLUMN id SET DEFAULT nextval('public.key_pair_id_seq'::regclass);


--
-- Name: obj id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.obj ALTER COLUMN id SET DEFAULT nextval('public.obj_id_seq'::regclass);


--
-- Name: plan id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plan ALTER COLUMN id SET DEFAULT nextval('public.plan_id_seq'::regclass);


--
-- Name: proj id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proj ALTER COLUMN id SET DEFAULT nextval('public.proj_id_seq'::regclass);


--
-- Name: security_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.security_group ALTER COLUMN id SET DEFAULT nextval('public.security_group_id_seq'::regclass);


--
-- Name: sg tbl_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg ALTER COLUMN tbl_id SET DEFAULT nextval('public.sg_tbl_id_seq'::regclass);


--
-- Name: sg_enable id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg_enable ALTER COLUMN id SET DEFAULT nextval('public.sg_enable_id_seq'::regclass);


--
-- Name: sg_inbound id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg_inbound ALTER COLUMN id SET DEFAULT nextval('public.sg_inbound_id_seq'::regclass);


--
-- Name: sg_ipperm tbl_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg_ipperm ALTER COLUMN tbl_id SET DEFAULT nextval('public.sg_ipperm_tbl_id_seq'::regclass);


--
-- Name: sg_outbound id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg_outbound ALTER COLUMN id SET DEFAULT nextval('public.sg_outbound_id_seq'::regclass);


--
-- Name: sg_temp id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg_temp ALTER COLUMN id SET DEFAULT nextval('public.sg_temp_id_seq'::regclass);


--
-- Name: sg_temp_rules id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg_temp_rules ALTER COLUMN id SET DEFAULT nextval('public.sg_temp_rules_id_seq'::regclass);


--
-- Name: tbl_exchangerate id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_exchangerate ALTER COLUMN id SET DEFAULT nextval('public.tbl_exchangerate_id_seq'::regclass);


--
-- Name: tbl_lambda id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_lambda ALTER COLUMN id SET DEFAULT nextval('public.tbl_lambda_id_seq'::regclass);


--
-- Name: tbl_logs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_logs ALTER COLUMN id SET DEFAULT nextval('public.tbl_logs_id_seq'::regclass);


--
-- Name: tbl_notif id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_notif ALTER COLUMN id SET DEFAULT nextval('public.tbl_notif_id_seq'::regclass);


--
-- Name: tbl_roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tbl_roles ALTER COLUMN id SET DEFAULT nextval('public.tbl_roles_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: admin_configs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.admin_configs (id, key, value) FROM stdin;
\.


--
-- Name: admin_configs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.admin_configs_id_seq', 6, true);


--
-- Data for Name: ami; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ami (cid, imageid, imagelocation, state, ownerid, public, platform, architecture, imagetype, kernelid, ramdiskid, imageowneralias, name, description, rootdevicetype, rootdevicename, virtualizationtype, hypervisor, lastchecked, region) FROM stdin;
\.


--
-- Data for Name: ami_enable; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ami_enable (cid, imageid, id) FROM stdin;
\.


--
-- Name: ami_enable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ami_enable_id_seq', 39, true);


--
-- Data for Name: ami_tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ami_tags (cid, imageid, key, value) FROM stdin;
355	ami-027aa3bb52d786253	Name	nakano_svweb1_copy
355	ami-167ee516	Name	KoryuWeb_tw_to_SSG
355	ami-187ee518	Name	KoryuWeb_jp_to_SSG
355	ami-1e7ee51e	Name	KoryuDB_to_SSG
355	ami-229f4c22	Name	Nikaido_HIS_CMS_20150529
355	ami-28bf0829	Name	showcase 32bit linux
355	ami-28bf0829	kernel-id	aki-a209a2a3
355	ami-60862760	Name	Komatsu_Koryu_Base
355	ami-779a0776	Name	Komatsu_J-messe Front Web
355	ami-819a0780	Name	Komatsu_J-messe DB Web
355	ami-bb0b8dba	Name	Kawajiri_SMOJ-Knowledge-DEV_20130304
355	ami-bcd4cfbd	Name	IDS_Redmine_20150203
355	ami-c440b4a5	Name	Blog-Web-Img
355	ami-d96252d8	Name	Horii_EC_Steampunk_20141020
355	ami-e241b583	Name	Blog-Db-Img
355	ami-f3fc6ef2	Name	S.Goto_Koryu_Test
355	ami-f72ccef7	Name	nakano_showcase v2.0 64bit centos 7
\.


--
-- Data for Name: azone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.azone (cid, name, state, regionname, lastchecked) FROM stdin;
355	ap-south-1a	available	ap-south-1	2018-12-12 14:25:17.773511
355	ap-south-1b	available	ap-south-1	2018-12-12 14:25:17.775939
355	eu-west-3a	available	eu-west-3	2018-12-12 14:25:19.089213
355	eu-west-3b	available	eu-west-3	2018-12-12 14:25:19.090079
355	eu-west-3c	available	eu-west-3	2018-12-12 14:25:19.090781
355	eu-north-1a	available	eu-north-1	2018-12-12 14:25:20.524139
355	eu-north-1b	available	eu-north-1	2018-12-12 14:25:20.525123
355	eu-north-1c	available	eu-north-1	2018-12-12 14:25:20.525841
355	eu-west-2a	available	eu-west-2	2018-12-12 14:25:21.550113
355	eu-west-2b	available	eu-west-2	2018-12-12 14:25:21.551033
355	eu-west-2c	available	eu-west-2	2018-12-12 14:25:21.551749
355	eu-west-1a	available	eu-west-1	2018-12-12 14:25:22.53517
355	eu-west-1b	available	eu-west-1	2018-12-12 14:25:22.53609
355	eu-west-1c	available	eu-west-1	2018-12-12 14:25:22.536853
355	ap-northeast-2a	available	ap-northeast-2	2018-12-12 14:25:22.789866
355	ap-northeast-2c	available	ap-northeast-2	2018-12-12 14:25:22.790741
355	ap-northeast-1a	available	ap-northeast-1	2018-12-12 14:25:22.877718
355	ap-northeast-1c	available	ap-northeast-1	2018-12-12 14:25:22.878516
355	ap-northeast-1d	available	ap-northeast-1	2018-12-12 14:25:22.879252
355	sa-east-1a	available	sa-east-1	2018-12-12 14:25:24.078658
355	sa-east-1c	available	sa-east-1	2018-12-12 14:25:24.079534
355	ca-central-1a	available	ca-central-1	2018-12-12 14:25:24.938472
355	ca-central-1b	available	ca-central-1	2018-12-12 14:25:24.939345
355	ap-southeast-1a	available	ap-southeast-1	2018-12-12 14:25:25.470619
355	ap-southeast-1b	available	ap-southeast-1	2018-12-12 14:25:25.471517
355	ap-southeast-1c	available	ap-southeast-1	2018-12-12 14:25:25.472238
355	ap-southeast-2a	available	ap-southeast-2	2018-12-12 14:25:26.080572
355	ap-southeast-2b	available	ap-southeast-2	2018-12-12 14:25:26.081522
355	ap-southeast-2c	available	ap-southeast-2	2018-12-12 14:25:26.082307
355	eu-central-1a	available	eu-central-1	2018-12-12 14:25:27.212786
355	eu-central-1b	available	eu-central-1	2018-12-12 14:25:27.2137
355	eu-central-1c	available	eu-central-1	2018-12-12 14:25:27.214414
355	us-east-1a	available	us-east-1	2018-12-12 14:25:27.955477
355	us-east-1b	available	us-east-1	2018-12-12 14:25:27.956345
355	us-east-1c	available	us-east-1	2018-12-12 14:25:27.957063
355	us-east-1d	available	us-east-1	2018-12-12 14:25:27.959725
355	us-east-1e	available	us-east-1	2018-12-12 14:25:27.96061
355	us-east-1f	available	us-east-1	2018-12-12 14:25:27.961323
355	us-east-2a	available	us-east-2	2018-12-12 14:25:28.848745
355	us-east-2b	available	us-east-2	2018-12-12 14:25:28.849654
355	us-east-2c	available	us-east-2	2018-12-12 14:25:28.850407
355	us-west-1a	available	us-west-1	2018-12-12 14:25:29.585137
355	us-west-1c	available	us-west-1	2018-12-12 14:25:29.586032
355	us-west-2a	available	us-west-2	2018-12-12 14:25:30.165923
355	us-west-2b	available	us-west-2	2018-12-12 14:25:30.166789
355	us-west-2c	available	us-west-2	2018-12-12 14:25:30.167493
2	ap-south-1a	available	ap-south-1	2018-09-27 13:11:55.765826
2	ap-south-1b	available	ap-south-1	2018-09-27 13:11:55.766871
2	eu-west-3a	available	eu-west-3	2018-09-27 13:11:57.014007
2	eu-west-3b	available	eu-west-3	2018-09-27 13:11:57.015109
2	eu-west-3c	available	eu-west-3	2018-09-27 13:11:57.01593
2	eu-west-2a	available	eu-west-2	2018-09-27 13:11:58.269872
2	eu-west-2b	available	eu-west-2	2018-09-27 13:11:58.270834
2	eu-west-2c	available	eu-west-2	2018-09-27 13:11:58.271506
2	eu-west-1a	available	eu-west-1	2018-09-27 13:11:59.59682
2	eu-west-1b	available	eu-west-1	2018-09-27 13:11:59.597781
2	eu-west-1c	available	eu-west-1	2018-09-27 13:11:59.598465
2	ap-northeast-2a	available	ap-northeast-2	2018-09-27 13:11:59.856662
2	ap-northeast-2c	available	ap-northeast-2	2018-09-27 13:11:59.857531
2	ap-northeast-1a	available	ap-northeast-1	2018-09-27 13:11:59.979447
2	ap-northeast-1c	available	ap-northeast-1	2018-09-27 13:11:59.980321
2	ap-northeast-1d	available	ap-northeast-1	2018-09-27 13:11:59.980965
2	sa-east-1a	available	sa-east-1	2018-09-27 13:12:01.341735
2	sa-east-1c	available	sa-east-1	2018-09-27 13:12:01.34266
2	ca-central-1a	available	ca-central-1	2018-09-27 13:12:02.20003
2	ca-central-1b	available	ca-central-1	2018-09-27 13:12:02.201
2	ap-southeast-1a	available	ap-southeast-1	2018-09-27 13:12:02.8027
2	ap-southeast-1b	available	ap-southeast-1	2018-09-27 13:12:02.803709
2	ap-southeast-1c	available	ap-southeast-1	2018-09-27 13:12:02.804457
2	ap-southeast-2a	available	ap-southeast-2	2018-09-27 13:12:03.428803
2	ap-southeast-2b	available	ap-southeast-2	2018-09-27 13:12:03.429791
2	ap-southeast-2c	available	ap-southeast-2	2018-09-27 13:12:03.430471
2	eu-central-1a	available	eu-central-1	2018-09-27 13:12:04.677389
2	eu-central-1b	available	eu-central-1	2018-09-27 13:12:04.678274
2	eu-central-1c	available	eu-central-1	2018-09-27 13:12:04.678881
2	us-east-1a	available	us-east-1	2018-09-27 13:12:05.920228
2	us-east-1b	available	us-east-1	2018-09-27 13:12:05.921082
2	us-east-1c	available	us-east-1	2018-09-27 13:12:05.921776
2	us-east-1d	available	us-east-1	2018-09-27 13:12:05.922407
2	us-east-1e	available	us-east-1	2018-09-27 13:12:05.923215
2	us-east-1f	available	us-east-1	2018-09-27 13:12:05.923847
2	us-east-2a	available	us-east-2	2018-09-27 13:12:06.789859
2	us-east-2b	available	us-east-2	2018-09-27 13:12:06.790828
2	us-east-2c	available	us-east-2	2018-09-27 13:12:06.791495
2	us-west-1a	available	us-west-1	2018-09-27 13:12:07.492532
2	us-west-1c	available	us-west-1	2018-09-27 13:12:07.493445
2	us-west-2a	available	us-west-2	2018-09-27 13:12:08.20368
2	us-west-2b	available	us-west-2	2018-09-27 13:12:08.204632
2	us-west-2c	available	us-west-2	2018-09-27 13:12:08.205312
1	ap-south-1a	available	ap-south-1	2018-10-31 10:47:43.507454
1	ap-south-1b	available	ap-south-1	2018-10-31 10:47:43.528263
1	eu-west-3a	available	eu-west-3	2018-10-31 10:47:44.768099
1	eu-west-3b	available	eu-west-3	2018-10-31 10:47:44.769114
1	eu-west-3c	available	eu-west-3	2018-10-31 10:47:44.76987
1	eu-west-2a	available	eu-west-2	2018-10-31 10:47:46.022271
1	eu-west-2b	available	eu-west-2	2018-10-31 10:47:46.024333
1	eu-west-2c	available	eu-west-2	2018-10-31 10:47:46.025065
1	eu-west-1a	available	eu-west-1	2018-10-31 10:47:47.229431
1	eu-west-1b	available	eu-west-1	2018-10-31 10:47:47.230407
1	eu-west-1c	available	eu-west-1	2018-10-31 10:47:47.231139
1	ap-northeast-2a	available	ap-northeast-2	2018-10-31 10:47:47.443464
1	ap-northeast-2c	available	ap-northeast-2	2018-10-31 10:47:47.44441
1	ap-northeast-1a	available	ap-northeast-1	2018-10-31 10:47:47.523796
1	ap-northeast-1c	available	ap-northeast-1	2018-10-31 10:47:47.524721
1	ap-northeast-1d	available	ap-northeast-1	2018-10-31 10:47:47.525481
1	sa-east-1a	available	sa-east-1	2018-10-31 10:47:48.847841
1	sa-east-1c	available	sa-east-1	2018-10-31 10:47:48.848826
1	ca-central-1a	available	ca-central-1	2018-10-31 10:47:49.716114
1	ca-central-1b	available	ca-central-1	2018-10-31 10:47:49.717136
1	ap-southeast-1a	available	ap-southeast-1	2018-10-31 10:47:50.316704
1	ap-southeast-1b	available	ap-southeast-1	2018-10-31 10:47:50.317687
1	ap-southeast-1c	available	ap-southeast-1	2018-10-31 10:47:50.318511
1	ap-southeast-2a	available	ap-southeast-2	2018-10-31 10:47:50.959434
1	ap-southeast-2b	available	ap-southeast-2	2018-10-31 10:47:50.960451
1	ap-southeast-2c	available	ap-southeast-2	2018-10-31 10:47:50.961226
1	eu-central-1a	available	eu-central-1	2018-10-31 10:47:52.216438
1	eu-central-1b	available	eu-central-1	2018-10-31 10:47:52.217506
1	eu-central-1c	available	eu-central-1	2018-10-31 10:47:52.218254
1	us-east-1a	available	us-east-1	2018-10-31 10:47:53.381633
1	us-east-1b	available	us-east-1	2018-10-31 10:47:53.382652
1	us-east-1c	available	us-east-1	2018-10-31 10:47:53.383355
1	us-east-1d	available	us-east-1	2018-10-31 10:47:53.384059
1	us-east-1e	available	us-east-1	2018-10-31 10:47:53.384804
1	us-east-1f	available	us-east-1	2018-10-31 10:47:53.385478
1	us-east-2a	available	us-east-2	2018-10-31 10:47:54.230406
1	us-east-2b	available	us-east-2	2018-10-31 10:47:54.231595
1	us-east-2c	available	us-east-2	2018-10-31 10:47:54.232367
1	us-west-1a	available	us-west-1	2018-10-31 10:47:54.956224
1	us-west-1c	available	us-west-1	2018-10-31 10:47:54.957206
1	us-west-2a	available	us-west-2	2018-10-31 10:47:55.641697
1	us-west-2b	available	us-west-2	2018-10-31 10:47:55.642694
1	us-west-2c	available	us-west-2	2018-10-31 10:47:55.643446
3	ap-south-1a	available	ap-south-1	2018-12-13 06:05:51.434776
3	ap-south-1b	available	ap-south-1	2018-12-13 06:05:51.438437
3	eu-west-3a	available	eu-west-3	2018-12-13 06:05:53.061407
3	eu-west-3b	available	eu-west-3	2018-12-13 06:05:53.064575
3	eu-west-3c	available	eu-west-3	2018-12-13 06:05:53.067738
3	eu-north-1a	available	eu-north-1	2018-12-13 06:05:54.758009
3	eu-north-1b	available	eu-north-1	2018-12-13 06:05:54.760527
3	eu-north-1c	available	eu-north-1	2018-12-13 06:05:54.762962
3	eu-west-2a	available	eu-west-2	2018-12-13 06:05:55.989408
3	eu-west-2b	available	eu-west-2	2018-12-13 06:05:55.992508
3	eu-west-2c	available	eu-west-2	2018-12-13 06:05:55.99549
3	eu-west-1a	available	eu-west-1	2018-12-13 06:05:57.359388
3	eu-west-1b	available	eu-west-1	2018-12-13 06:05:57.362727
3	eu-west-1c	available	eu-west-1	2018-12-13 06:05:57.365929
3	ap-northeast-2a	available	ap-northeast-2	2018-12-13 06:05:58.072043
3	ap-northeast-2c	available	ap-northeast-2	2018-12-13 06:05:58.075743
3	ap-northeast-1a	available	ap-northeast-1	2018-12-13 06:05:58.536466
3	ap-northeast-1c	available	ap-northeast-1	2018-12-13 06:05:58.538745
3	ap-northeast-1d	available	ap-northeast-1	2018-12-13 06:05:58.541034
3	sa-east-1a	available	sa-east-1	2018-12-13 06:06:00.369231
3	sa-east-1c	available	sa-east-1	2018-12-13 06:06:00.37205
3	ca-central-1a	available	ca-central-1	2018-12-13 06:06:01.78329
3	ca-central-1b	available	ca-central-1	2018-12-13 06:06:01.785978
3	ap-southeast-1a	available	ap-southeast-1	2018-12-13 06:06:02.187833
3	ap-southeast-1b	available	ap-southeast-1	2018-12-13 06:06:02.189673
3	ap-southeast-1c	available	ap-southeast-1	2018-12-13 06:06:02.191475
3	ap-southeast-2a	available	ap-southeast-2	2018-12-13 06:06:03.375371
3	ap-southeast-2b	available	ap-southeast-2	2018-12-13 06:06:03.377146
3	ap-southeast-2c	available	ap-southeast-2	2018-12-13 06:06:03.378792
3	eu-central-1a	available	eu-central-1	2018-12-13 06:06:04.688084
3	eu-central-1b	available	eu-central-1	2018-12-13 06:06:04.690908
3	eu-central-1c	available	eu-central-1	2018-12-13 06:06:04.693602
3	us-east-1a	available	us-east-1	2018-12-13 06:06:05.980083
3	us-east-1b	available	us-east-1	2018-12-13 06:06:05.983298
3	us-east-1c	available	us-east-1	2018-12-13 06:06:05.98642
3	us-east-1d	available	us-east-1	2018-12-13 06:06:05.989493
3	us-east-1e	available	us-east-1	2018-12-13 06:06:05.993345
3	us-east-1f	available	us-east-1	2018-12-13 06:06:05.996378
3	us-east-2a	available	us-east-2	2018-12-13 06:06:07.123649
3	us-east-2b	available	us-east-2	2018-12-13 06:06:07.127314
3	us-east-2c	available	us-east-2	2018-12-13 06:06:07.131027
3	us-west-1a	available	us-west-1	2018-12-13 06:06:08.034481
3	us-west-1c	available	us-west-1	2018-12-13 06:06:08.037991
3	us-west-2a	available	us-west-2	2018-12-13 06:06:09.193759
3	us-west-2b	available	us-west-2	2018-12-13 06:06:09.196881
3	us-west-2c	available	us-west-2	2018-12-13 06:06:09.207534
\.


--
-- Data for Name: azone_enable; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.azone_enable (cid, regionname, id) FROM stdin;
\.


--
-- Name: azone_enable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.azone_enable_id_seq', 57, true);


--
-- Data for Name: configs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.configs (id, cid, insttype_curgen, pass_lenght, pass_expired, pass_lowercase, pass_number, mail_address, admin_active, lock_failed, instance_expires, check_sys_admin, check_manager, email_instance, timezone, timezoneid, days_apply) FROM stdin;
8	31	0	0	0	0	0	\N	0	0	0	0	0	0	\N	\N	0
10	44	0	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
12	47	0	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
11	45	0	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
13	54	1	4	0	0	0		5	5	5	1	1	0	9	69	0
9	17	0	8	0	1	1		0	3	1	1	1	1	7	62	0
1	3	0	7	1	1	1		5	3	5	1	1	1	7	62	0
15	2	1	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
16	7	0	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
14	1	0	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
17	591	0	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
18	355	1	4	0	0	0	\N	600	5	30	1	1	0	9	69	0
\.


--
-- Name: configs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.configs_id_seq', 18, true);


--
-- Data for Name: cost; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cost (cid, lineitemtype, usagestartdate, usageenddate, productcode, usagetype1, operation1, resourceid, usageamount, currencycode, unblendedcost, blendedcost, lineitemdescription, instancetype, location, memory, networkperformance, normalizationsizefactor, operatingsystem, operation2, physicalprocessor, preinstalledsw, processorarchitecture, processorfeatures, region, servicename, transfertype, usagetype2, publicondemandcost, publicondemandrate, term, unit, usageaccountid, servicecode, productfamily, unblendedrate, blendedrate, loaddate, ondemandusagecost, riusagecost, riflag, pricingpurchaseoption, reservationupfrontvalue, reservationstarttime, rateid) FROM stdin;
\.


--
-- Data for Name: cost_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cost_detail (cid, ym, lineitemtype, region, productcode, productfamily, lineitemdescription, preinstalledsw, operatingsystem, unit, unblendedcost, blendedcost, usageamount, unblendedrate, blendedrate, usagetype, org_unblendedcost, org_blendedcost, org_usageamount, billingentity) FROM stdin;
\.


--
-- Data for Name: cost_ri; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cost_ri (accountname, subscriptionid, reservationid, instancetype, riutilization, rihourspurchased, rihoursused, rihoursunused, accountid, startdate, enddate, numberofris, scope, region, availabilityzone, platform, tenancy, paymentoption, offeringtype, ondemandcostequivalent, amortizedupfrontfees, amortizedrecurringcharges, effectivericost, netsavings, potentialsavings, averageondemandrate, totalassetvalue, effectivehourlyrate, upfrontfee, hourlyrecurringfee, ricostforunusedhours) FROM stdin;
\.


--
-- Data for Name: cw_logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cw_logs (id, cid, stream, stream_time, event, event_time, date_created, error_flag, description) FROM stdin;
\.


--
-- Name: cw_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cw_logs_id_seq', 1, false);


--
-- Data for Name: cw_rules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cw_rules (id, cid, name, expr, region, state, notes, status, dept) FROM stdin;
4911	355	aaa	cron(0 1 ? * 3 *)	ap-northeast-1	1		0	0
4912	355	haivd_test_rule	cron(1 16 ? * 5 *)	ap-northeast-1	1		0	0
4913	355	weekly_test_rule	cron(1 16 ? * 1 *)	ap-northeast-1	1	123	0	0
\.


--
-- Name: cw_rules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cw_rules_id_seq', 4913, true);


--
-- Data for Name: dept; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dept (cid, dept, deptname, regdate, status, disporder, id) FROM stdin;
1	3	cccddd	2017-12-08 15:29:12.616634	-1	3	1
1	4	aaa	2017-12-08 15:33:28.196323	-1	3	2
1	5	testaaa	2017-12-11 10:52:31.979811	-1	1	3
3	2	aaa	2018-02-08 18:35:42.980883	-1	2	8
3	4	あああ	2018-02-20 13:32:36.007766	-1	3	9
3	5	aaa	2018-03-29 17:43:31.356718	-1	3	11
3	3	テストです。	2018-02-20 13:32:06.25939	-1	2	12
13	1	title email 02	2018-07-30 12:19:10.531378	0	1	23
14	1	Test	2018-07-30 12:45:46.53917	0	1	24
16	1	Test123	2018-07-30 13:20:16.971022	0	1	26
19	1	444	2018-07-31 16:33:55.003774	0	1	29
20	1	444	2018-07-31 16:35:56.687268	0	1	30
21	1	444	2018-07-31 16:37:59.999317	0	1	31
23	1	444	2018-07-31 16:46:15.650814	0	1	33
25	1	dmo fe	2018-07-31 16:50:30.967591	0	1	35
17	7	thoaaaa dexuong	2018-09-24 19:36:51.021919	-1	5	65
54	1	thuygroup	2018-09-25 12:26:33.435046	0	1	66
17	2	group01	2018-07-31 17:50:26.937095	0	1	37
17	5	thoa	2018-09-14 11:19:20.073995	0	3	55
30	1	ahaha	2018-08-03 19:22:13.376732	0	1	42
30	2	new dept 1	2018-08-07 19:23:39.929612	0	2	43
32	1	demo	2018-08-08 11:00:03.50267	0	1	44
31	1	ses5b4b53e5b4	2018-08-13 11:26:11.112342	0	1	45
31	2	aq3v412	2018-08-13 11:47:35.718943	0	2	46
35	1	000	2018-08-21 19:04:27.499052	0	1	47
17	3	02group	2018-07-31 17:58:13.10973	0	2	38
37	1	12	2018-08-23 17:59:08.479913	0	1	50
17	4	sfaf	2018-09-04 13:29:30.038875	-1	3	51
42	1	abc	2018-09-06 16:58:18.549619	0	1	52
44	1	ab	2018-09-11 17:34:40.611264	0	1	54
3	8	GROUP12321	2018-08-21 20:03:33.172789	-1	4	49
3	10	aaaa	2018-09-14 16:10:00.662801	-1	5	56
3	7	123	2018-08-21 20:00:17.203846	-1	4	48
11	1	test	2018-09-19 15:46:06.853726	0	1	57
48	1	1	2018-09-20 16:13:14.115502	0	1	58
49	1	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA	2018-09-20 16:16:14.291498	0	1	59
50	1	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA	2018-09-20 16:18:27.56744	0	1	60
51	1	Xperia	2018-09-20 16:22:27.067386	0	1	61
53	1	sưde	2018-09-20 16:25:10.946122	0	1	62
3	11	123123	2018-09-23 14:44:34.591056	-1	4	63
17	6	thoa test 02	2018-09-24 12:07:09.957481	0	4	64
1	1	情報システム本部	2017-12-08 14:27:29.480002	0	1	6
1	2	管理部	2017-12-08 14:36:07.089739	0	2	5
1	6	開発部	2017-12-13 16:56:53.44748	0	3	7
3	9	GroupName 123	2018-09-11 16:54:41.291969	-1	1	53
3	1	情報システム本部	2018-03-29 17:43:24.411386	-1	2	10
3	6	サニークラウドGrp.	2018-04-03 18:01:02.348881	-1	4	13
3	13	GROUP_TEST_002	2018-09-25 17:08:36.154047	-1	5	68
3	14	GROUP_TEST_003	2018-09-25 17:08:42.840666	-1	6	69
3	15	GROUP_TEST_004	2018-09-25 17:41:28.533954	-1	7	70
3	16	GROUP_TEST_001	2018-09-25 17:41:49.734375	-1	8	71
3	17	GROUP_TEST_005	2018-09-25 18:02:09.740834	-1	9	72
2	1	役員室	2017-12-12 11:02:17.912403	-1	1	4
1	7	ISS部	2018-10-31 10:46:00.750965	0	4	73
591	1	Default	2018-11-08 14:56:05.622777	0	1	75
590	1	Default	2018-11-07 18:04:19.346701	-1	1	74
355	3	自社サービス	2018-11-15 13:42:30.424299	0	1	78
355	1	管理部	2018-11-15 13:32:50.421477	0	2	76
355	2	開発部	2018-11-15 13:42:12.236102	0	3	77
\.


--
-- Name: dept_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dept_id_seq', 78, true);


--
-- Data for Name: ebs_change_req; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ebs_change_req (id, cid, instance_id, volume_id, volume_zone, volume_type, volume_size, volume_iops, device_name, state, attached, autoremove, change_type, updated_at) FROM stdin;
12	3	i-075fd2964e51a8237	vol-0d1b96f027a23dc33	ap-northeast-1d	io1	8	100	/dev/sdb	available	0	0	change	2018-09-06 11:06:34.405075
13	3	i-075fd2964e51a8237	vol-02119fbf31c5caf8e	ap-northeast-1d	io1	7	100	/dev/sdb	available	0	0	attach	2018-09-06 11:06:34.405075
14	3	i-075fd2964e51a8237	vol-0d1b96f027a23dc33	ap-northeast-1d	io1	8	100	/dev/sdb	available	0	0	attach	2018-09-06 11:06:34.405075
\.


--
-- Data for Name: ebs_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ebs_log (id, instid, ebs_req_id, size, iops, zone, state, type, device, is_root, encrypted, del_term, can_modify, attached) FROM stdin;
\.


--
-- Name: ebs_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ebs_log_id_seq', 1, false);


--
-- Data for Name: ebs_req; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ebs_req (id, cid, empid, instance_id, updated_at, status, note) FROM stdin;
22	17	39	i-09f4abc6df3c4636e	2018-09-21 15:38:33.887937	1	\N
1	3	1	i-07ee4ae812f7f77a7	2018-09-11 18:03:24.884912	1	\N
2	17	1	i-04d1ba710a31a68c8	2018-09-11 18:10:18.456958	-1	\N
23	17	38	i-09f4abc6df3c4636e	2018-09-21 15:42:32.199978	1	\N
4	17	1	i-04d1ba710a31a68c8	2018-09-12 15:06:45.823138	0	\N
5	3	1	i-02e816848b8b41673	2018-09-12 15:50:38.159375	1	\N
6	3	1	i-02e816848b8b41673	2018-09-13 16:22:00.958254	0	\N
25	17	39	i-09f4abc6df3c4636e	2018-09-21 15:51:59.230213	1	\N
26	17	38	i-09f4abc6df3c4636e	2018-09-21 15:54:52.636353	1	\N
24	17	39	i-02a1acbb1bc5aef9e	2018-09-21 15:44:56.756544	1	\N
27	17	38	i-033546000ab06b43e	2018-09-21 18:17:34.531877	0	\N
28	17	38	i-02a1acbb1bc5aef9e	2018-09-21 19:26:47.793268	1	\N
7	17	38	i-00f969f9fb9e78185	2018-09-13 18:29:57.309594	0	\N
29	3	18	i-0fc59d93b65604e67	2018-09-24 17:26:57.892935	0	\N
30	17	38	i-02993ed9cf3e3dc01	2018-09-24 19:40:30.69985	0	\N
10	17	41	i-06bf8d296e1ff1bdc	2018-09-14 13:16:58.942458	0	\N
31	17	40	i-006201718729dde90	2018-09-25 13:08:48.534918	1	\N
3	3	1	i-07ee4ae812f7f77a7	2018-09-14 14:57:51.660665	0	\N
13	3	1	i-056b96eac26c4e1f0	2018-09-19 11:58:35.417529	-1	\N
32	17	40	i-006201718729dde90	2018-09-25 13:09:43.300791	1	\N
12	17	38	i-01b3c573dc04c4cf9	2018-09-19 20:10:28.259386	1	\N
15	17	39	i-0e1db47a03bf9aee2	2018-09-19 20:11:50.033943	-1	\N
14	17	39	i-02b60dba2674e88cc	2018-09-19 19:26:46.158622	-1	\N
16	3	18	i-091b1f82cff9a018f	2018-09-20 19:49:34.914297	1	\N
17	3	18	i-091b1f82cff9a018f	2018-09-20 19:53:00.511089	1	\N
18	3	2	i-056b96eac26c4e1f0	2018-09-21 12:55:21.69412	1	\N
33	17	40	i-006201718729dde90	2018-09-25 13:18:22.460894	1	\N
19	17	40	i-09f4abc6df3c4636e	2018-09-21 14:56:30.783377	1	\N
20	17	40	i-09f4abc6df3c4636e	2018-09-21 14:59:36.494542	1	\N
34	54	1	i-0b8a56e4d56dea124	2018-09-25 18:30:31.128817	0	\N
35	1	1	i-0c3892d7dc533cbb3	2018-10-25 16:24:57.907343	1	\N
36	1	1	i-0c3892d7dc533cbb3	2018-10-25 17:31:01.272041	0	\N
37	1	1	i-0db71c5582752e5ab	2018-10-26 15:27:54.763641	1	\N
\.


--
-- Data for Name: ebs_req_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ebs_req_detail (id, ebs_req_id, volume_id, volume_zone, volume_type, volume_size, volume_iops, device_name, state, attached, autoremove, change_type, status) FROM stdin;
2	1	vol-0b0136ab3aeb91c57	ap-northeast-1a	standard	11	0	/dev/sdj	in-use	1	0	change	1
3	1	vol-0797be1c9f8cdb455	ap-northeast-1a	standard	6	0	/dev/sdb	in-use	1	0	detach	1
4	2	1	ap-northeast-1d	standard	2	0	/dev/sdb		0	1	change	0
5	2	2	ap-northeast-1d	standard	3	0	/dev/sdb		0	1	change	0
82	16	vol-0859662a1516de9ca	ap-northeast-1d	standard	8	0	/dev/sdc	in-use	1	1	detach	1
83	17	vol-0f4afd43bddab7b04	ap-northeast-1d	standard	9	0	/dev/sdj	available	0	1	change	1
12	4	vol-02119fbf31c5caf8e	ap-northeast-1d	io1	5	100	/dev/sdb	available	0	0	change	0
13	4	1	ap-northeast-1d	standard	1	322	/dev/sdb		0	1	change	0
14	5	vol-0b741303a6eca83c8	ap-northeast-1d	standard	9	0	/dev/sdb	in-use	1	1	detach	1
17	6	1	ap-northeast-1d	gp2	5	100	/dev/sdb		0	1	change	0
88	18	vol-0b0136ab3aeb91c57	ap-northeast-1a	standard	12	0	/dev/sdd	available	0	0	change	1
100	19	vol-07a2a74c9d9eb1903	ap-northeast-1d	gp2	6	0	/dev/sda1	in-use	1	1	change	0
101	19	1	ap-northeast-1d	standard	2	0	/dev/sdk		0	1	change	0
102	19	2	ap-northeast-1d	standard	1	0	/dev/sdj		0	1	change	0
103	19	vol-04fa308e212884414	ap-northeast-1d	standard	1	0	/dev/sdb	in-use	1	1	detach	0
104	20	vol-0b741303a6eca83c8	ap-northeast-1d	standard	9	0	/dev/sdf	available	0	0	change	1
108	21	vol-0859662a1516de9ca	ap-northeast-1d	standard	1	0	/dev/sdj	available	0	0	change	0
109	21	vol-05b716036171991b1	ap-northeast-1d	standard	1	0	/dev/sdk	available	0	0	change	0
42	8	vol-0d5ba0bf22a935ef6	ap-northeast-1d	gp2	5	0	/dev/sda1	in-use	1	1	change	0
43	8	1	ap-northeast-1d	gp2	3	0	/dev/sdi		0	1	change	0
44	8	2	ap-northeast-1d	standard	1	0	/dev/sdl		0	1	change	0
45	9	vol-03bb0937684a3132b	ap-northeast-1d	gp2	5	0	/dev/sda1	in-use	1	1	change	0
46	9	1	ap-northeast-1d	gp2	1	0	/dev/sdb		0	1	change	0
47	9	2	ap-northeast-1d	gp2	1	0	/dev/sdb		0	1	change	0
119	27	1	ap-northeast-1d	standard	1	0	/dev/sdb		0	1	change	0
111	22	vol-0c8a13363198b9812	ap-northeast-1d	standard	1	0	/dev/sdl	available	0	1	change	1
51	10	vol-0c9f33d5d2007cdf3	ap-northeast-1d	standard	1	0	/dev/sdb	available	0	0	change	0
52	10	1	ap-northeast-1d	standard	1	0	/dev/sdb		0	1	change	0
53	10	vol-0d1b96f027a23dc33	ap-northeast-1d	io1	5	100	/dev/sdb	available	0	0	change	0
112	23	vol-0d1b96f027a23dc33	ap-northeast-1d	io1	5	100	/dev/sdg	available	0	0	change	1
115	25	vol-0c8a13363198b9812	ap-northeast-1d	standard	1	0	/dev/sdl	available	0	0	change	1
62	11	1	ap-northeast-1d	standard	2	0	/dev/sdb		0	1	change	0
63	11	vol-013f8eaaaefcf71a3	ap-northeast-1d	standard	1	0	/dev/sdb	available	0	0	change	0
64	11	2	ap-northeast-1d	io1	4	223	/dev/sdb		0	1	change	0
116	25	vol-010c456d8ac2c56c0	ap-northeast-1d	standard	1	0	/dev/sdj	available	0	1	change	1
117	26	vol-0c8a13363198b9812	ap-northeast-1d	standard	1	0	/dev/sdl	in-use	1	0	detach	1
118	26	vol-04fa308e212884414	ap-northeast-1d	standard	1	0	/dev/sdb	in-use	1	1	detach	1
120	28	vol-06a6309866ab6ac33	ap-northeast-1d	standard	1	0	/dev/sdb	in-use	1	1	detach	1
71	3	vol-0797be1c9f8cdb455	ap-northeast-1a	standard	6	0	/dev/sdb	available	0	0	change	0
72	3	1	ap-northeast-1a	standard	5	0	/dev/sdb		0	1	change	0
73	13	1	ap-northeast-1a	standard	2	0	/dev/sdb		0	1	change	0
121	29	1	ap-northeast-1d	standard	1	0	/dev/sdc		0	1	change	0
113	24	vol-0fc03df794fb2701c	ap-northeast-1d	standard	1	0	/dev/sde	available	0	1	change	1
114	24	vol-0ded5ff4323f849b9	ap-northeast-1d	standard	1	0	/dev/sdk	available	0	1	change	1
77	14	vol-0c8dfefeeb3fb781b	ap-northeast-1d	standard	4	0	/dev/sdb	in-use	1	1	change	0
81	15	1	ap-northeast-1d	standard	1	0	/dev/sdf		0	1	change	0
122	30	vol-0e610d0a7725bed92	ap-northeast-1d	standard	1	0	/dev/sdh	available	0	0	change	0
78	12	vol-0c9f33d5d2007cdf3	ap-northeast-1d	standard	1	0	/dev/sdf	available	0	0	change	1
80	12	vol-013f8eaaaefcf71a3	ap-northeast-1d	standard	1	0	/dev/sde	available	0	0	change	1
79	12	vol-0bc453dd7295a1a51	ap-northeast-1d	standard	3	0	/dev/sdd	available	0	1	change	1
130	31	vol-0003d72d408438b61	ap-northeast-1d	gp2	0	0	/dev/sda1	in-use	1	1	change	0
131	32	vol-0003d72d408438b61	ap-northeast-1d	gp2	11	0	/dev/sda1	in-use	1	1	change	1
136	33	vol-0347abdb562ff9467	ap-northeast-1d	standard	0	0	/dev/sdb	in-use	1	1	change	0
138	34	vol-079879fe5ba67dfeb	ap-northeast-1d	standard	8	0	/dev/sdb	in-use	1	1	detach	0
143	36	vol-08391b39f5cd0c1d9	ap-northeast-1a	gp2	1	0	/dev/sdd	in-use	1	0	detach	0
141	35	vol-08391b39f5cd0c1d9	ap-northeast-1a	gp2	1	0	/dev/sdd	available	0	0	change	1
142	35	vol-0c729a07120b72bd4	ap-northeast-1a	standard	6	0	/dev/sde	available	0	1	change	1
144	37	vol-046d6ae96fb4a466f	ap-northeast-1c	standard	16	0	/dev/sdb	in-use	1	1	change	1
145	37	vol-084fc71812afe774d	ap-northeast-1c	standard	1	0	/dev/sdk	available	0	1	change	1
\.


--
-- Name: ebs_req_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ebs_req_detail_id_seq', 145, true);


--
-- Name: ebs_req_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ebs_req_id_seq', 37, true);


--
-- Data for Name: emp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.emp (cid, empid, email, pass, dept, name, memo1, memo2, regdate, lastlogin, status, regkey, sysadmin, admin, disporder, infocount, wfcount, idsadmin, id, email_confirmed, pw_reset_token, pw_reset_token_time, login_initial, number_lock, email_lock, show_privacy) FROM stdin;
2	1	nakano+manage@ids.co.jp	55c64b2e108beea023871df110e4f08f	1	中野 貴志	\N	\N	2018-09-27 12:56:05.875832	2018-09-27 14:38:29.6336	-1	\N	1	1	1	44	0	0	143	1	\N	\N	1	0	0	1
6	1	nakano+test@ids.co.jp	55c64b2e108beea023871df110e4f08f	1	中野 貴志	\N	\N	2018-10-23 13:27:26.235054	\N	-1	gqK3B2qICT6eJPdNakjjjZdlGnmyfWHG	1	1	1	\N	\N	0	146	0	\N	\N	1	0	0	1
7	1	nakano+test5@ids.co.jp	b59c67bf196a4758191e42f76670ceba	1	中野 貴志	\N	\N	2018-10-23 14:11:21.533753	2018-10-23 14:12:05.01217	-1	\N	1	1	1	112	0	0	147	1	\N	2018-10-23 14:10:55.562665	1	0	0	1
591	1	kana_shin16@yahoo.co.jp	974924fe1ae3fa4923cc6f5c14d31ca9	1	Kodera	\N	\N	2018-11-08 14:56:05.622777	2018-11-08 16:25:22.760248	0	\N	1	1	1	116	0	0	149	1	\N	\N	1	0	0	1
4	1	nakano+1234@ids.co.jp	81dc9bdb52d04dc20036dbd8313ed055	1	ナカノタカシ	\N	\N	2018-10-05 11:46:36.252672	\N	-1	GGTpSAGHsw6bycEYVsT3uMfRMKmf2ToU	1	1	1	\N	\N	0	145	0	\N	\N	1	0	0	1
355	3	a.komatsu@ids.co.jp	ddd1df443471e3abe89933f20d08116a	2	小松 愛	\N	\N	2018-11-15 13:50:05.849958	\N	-1	4e8d3f4639f3a5d750ea884b5818d5f4	1	1	3	\N	\N	0	151	0	m4EK6H0FkbsXRvLTEWm1RkAkue8wIGjZ	2018-12-04 15:58:35.81838	0	3	0	1
355	4	komatsu@ids.co.jp	ddd1df443471e3abe89933f20d08116a	2	小松　愛	\N	\N	2018-12-04 17:39:52.79248	2018-12-04 18:12:01.915063	0	a3a9112773a419ffc37259267f80742d	1	1	3	228	0	0	152	1	\N	\N	1	0	0	0
355	1	nakano@ids.co.jp	85b4461287a1e581e52800f25c9e494d	1	中野 貴志	\N	\N	2018-10-25 15:57:19.082315	2018-12-12 10:59:22.881521	0	\N	1	1	1	228	1	1	142	1	\N	2018-10-25 15:54:01.289037	1	0	0	0
590	1	kodera@ids.co.jp	974924fe1ae3fa4923cc6f5c14d31ca9	1	小寺加奈子	\N	\N	2018-11-07 18:04:19.346701	\N	-1	hXXkSMogIY4ZDn4cOTYozMzQl3rwc9gU	1	1	1	\N	\N	0	148	0	\N	\N	1	0	0	0
355	2	kodera@ids.co.jp	974924fe1ae3fa4923cc6f5c14d31ca9	2	小寺 加奈子	\N	\N	2018-11-15 13:47:52.003356	2018-12-12 16:41:26.056326	0	cb88851056acdeaf44a597f54e102eb5	1	1	2	224	1	0	150	1	\N	\N	1	0	0	0
3	1	nakano+111@ids.co.jp	81dc9bdb52d04dc20036dbd8313ed055	1	ナカノタカシ	\N	\N	2018-10-03 11:39:55.119057	\N	-1	ScWuue749cqWOqtkKpiRH8ixrzBgKE8s	1	1	1	\N	2	0	144	0	\N	\N	1	0	0	1
\.


--
-- Name: emp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.emp_id_seq', 152, true);


--
-- Data for Name: emp_proj; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.emp_proj (id, cid, empid, pcode) FROM stdin;
251	17	35	tesstttt
316	17	38	test009
317	17	42	test009
318	17	40	test009
319	17	41	test009
320	17	39	test009
8	3	12	AAABCDEFGH
9	3	11	AAABCDEFGH
263	3	14	AAABCDEFGH
268	3	2	コードテスト
269	3	14	コードテスト
270	3	17	コードテスト
271	3	13	コードテスト
272	3	10	コードテスト
273	3	2	CCC
274	3	14	CCC
275	3	18	CCC
277	17	38	uytuy
264	3	17	AAABCDEFGH
279	17	35	demo1
280	17	38	demo1
265	3	16	AAABCDEFGH
286	3	2	abccccsd
287	3	19	abccccsd
288	3	14	abccccsd
289	3	18	abccccsd
290	3	17	abccccsd
170	17	34	demo1
335	17	38	thoa_pcode_test
336	17	42	thoa_pcode_test
337	17	41	thoa_pcode_test
338	17	39	thoa_pcode_test
339	17	40	thoa_pcode_test
254	17	35	A001
255	17	38	A001
340	11	1	testproject
341	17	39	007tesst 007
342	17	40	007tesst 007
343	17	42	007tesst 007
344	17	41	007tesst 007
345	17	38	007tesst 007
346	17	29	007tesst 007
347	17	27	007tesst 007
348	17	28	007tesst 007
349	17	4	007tesst 007
350	17	30	007tesst 007
351	17	15	007tesst 007
352	17	26	007tesst 007
353	17	17	007tesst 007
354	17	5	007tesst 007
355	17	10	007tesst 007
356	17	1	007tesst 007
357	17	35	007tesst 007
358	17	32	007tesst 007
359	17	45	007tesst 007
360	17	33	007tesst 007
361	17	44	007tesst 007
362	17	31	007tesst 007
50	3	12	AABDDDD
51	3	1	AABDDDD
52	3	9	AABDDDD
53	3	8	AABDDDD
365	1	1	SMOJ
364	1	1	AAB
363	1	1	AA2
\.


--
-- Name: emp_proj_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.emp_proj_id_seq', 365, true);


--
-- Data for Name: info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.info (rank, senddate, title, message, toall, mid, id_notif) FROM stdin;
1	2018-02-19 13:25:53.729882	メンテナンスのお知らせ	メンテナンスを行います<br />	\N	1	\N
2	2018-02-19 13:25:53.736089	メンテナンスのお知らせ	メンテナンスを行います<br />	\N	2	\N
3	2018-02-19 13:25:53.749082	メンテナンスのお知らせ	メンテナンスを行います<br />	\N	3	\N
2	2018-02-19 13:26:15.773941	メンテナンスのお知らせ	メンテナンスを行います<br />	1	4	\N
1	2018-03-20 10:21:51.210083	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	5	\N
1	2018-04-03 13:45:41.959148	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	6	\N
1	2018-04-03 13:45:53.830324	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	7	\N
1	2018-04-03 13:47:57.781449	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	8	\N
1	2018-04-03 13:48:06.249652	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	9	\N
1	2018-02-20 11:31:44.25562	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	10	\N
1	2018-02-20 11:32:08.674733	2017年12月のご請求書の準備ができました。	2017年12月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、114.92円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	11	\N
3	2018-02-19 13:26:15.759026	メンテナンスのお知らせ	メンテナンスを行います<br />	1	12	\N
1	2018-04-03 13:49:32.061173	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	13	\N
1	2018-04-03 13:51:24.226181	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	14	\N
1	2018-04-03 13:51:29.880904	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	15	\N
1	2018-04-03 15:08:39.563042	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	16	\N
1	2018-04-03 15:08:44.943243	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	17	\N
1	2018-04-03 17:08:20.018929	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	18	\N
1	2018-04-03 17:11:45.160857	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	19	\N
1	2018-04-03 17:55:59.078197	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	20	\N
1	2018-04-03 17:56:00.395256	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	21	\N
1	2018-04-03 18:49:03.831045	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	22	\N
1	2018-04-03 18:49:05.162052	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	23	\N
1	2018-04-03 18:49:09.909095	2017年12月のご請求書の準備ができました。	2017年12月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、114.92円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	24	\N
1	2018-04-05 17:42:06.820458	2018年03月のご請求書の準備ができました。	2018年03月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	25	\N
1	2018-04-05 17:43:39.600501	2018年03月のご請求書の準備ができました。	2018年03月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	26	\N
1	2018-04-05 17:45:20.115712	2018年03月のご請求書の準備ができました。	2018年03月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	27	\N
1	2018-04-05 18:01:00.597639	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	28	\N
1	2018-04-05 18:02:13.208141	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	29	\N
1	2018-04-09 18:58:35.194793	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	30	\N
1	2018-04-09 18:58:38.333424	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	31	\N
1	2018-04-09 18:58:41.795911	2018年03月のご請求書の準備ができました。	2018年03月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	32	\N
1	2018-08-07 16:30:38.994915	2018年03月のご請求書の準備ができました。	2018年03月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	33	\N
1	2018-08-07 16:30:58.68004	2018年02月のご請求書の準備ができました。	2018年02月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.34円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	34	\N
1	2018-08-07 16:31:08.034919	2018年01月のご請求書の準備ができました。	2018年01月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.73円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	35	\N
1	2018-08-07 16:31:22.98898	2017年12月のご請求書の準備ができました。	2017年12月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、114.92円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	36	\N
1	2018-08-07 16:31:33.326924	2017年11月のご請求書の準備ができました。	2017年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.16円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	37	\N
3	2018-09-25 00:00:00	Thuy test mesage 24/09	Thuy test	1	38	6
1	2018-09-26 00:00:00	Thuy test message 24/09 2nd	Thuy test message 24/09 2nd	1	39	7
2	2018-09-25 00:00:00	お知らせテスト	Context	1	40	8
3	2018-09-27 00:00:00	xxxxxxx	xxxx	1	41	9
1	2018-09-30 00:00:00	test notif 3	demo notif 33	1	42	10
1	2018-09-27 00:00:00	lamlamlam	lamlamlam	1	43	11
1	2018-09-29 00:00:00	admin dev again	admin dev again	1	45	13
1	2018-10-04 00:00:00	test admin_dev	test admin_dev\r\ntest admin_dev	1	44	12
1	2018-09-28 00:00:00	xxxxxx	xxx	1	46	14
1	2018-09-27 12:17:38.507403	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、256.66円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	47	\N
1	2018-09-27 12:17:45.695451	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、256.66円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	48	\N
1	2018-09-27 12:17:48.827825	2018年07月のご請求書の準備ができました。	2018年07月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.82円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	49	\N
1	2018-09-27 12:17:51.658331	2018年06月のご請求書の準備ができました。	2018年06月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.80円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	50	\N
1	2018-09-27 12:17:54.051062	2018年05月のご請求書の準備ができました。	2018年05月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.80円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	51	\N
1	2018-09-27 13:31:11.495561	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	52	\N
1	2018-10-02 14:13:01.33066	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	53	\N
1	2018-10-02 14:34:00.204185	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	54	\N
1	2018-10-02 14:37:24.975567	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	55	\N
1	2018-10-02 15:13:57.786729	2018年07月のご請求書の準備ができました。	2018年07月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.82円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	56	\N
1	2018-10-02 15:14:36.727603	2018年06月のご請求書の準備ができました。	2018年06月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.80円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	57	\N
1	2018-10-02 15:59:20.078849	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	58	\N
1	2018-10-02 16:02:17.990236	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	59	\N
1	2018-10-02 16:13:36.14379	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	60	\N
1	2018-10-02 16:21:18.646289	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	61	\N
1	2018-10-03 13:37:31.688921	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	62	\N
1	2018-10-03 13:37:36.297832	2018年07月のご請求書の準備ができました。	2018年07月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.82円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	63	\N
1	2018-10-03 13:38:29.000238	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	64	\N
1	2018-10-03 13:44:32.47764	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	65	\N
1	2018-10-03 13:44:38.268573	2018年07月のご請求書の準備ができました。	2018年07月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.82円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	66	\N
1	2018-10-03 13:44:44.128762	2018年06月のご請求書の準備ができました。	2018年06月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、107.80円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	67	\N
1	2018-10-03 14:08:53.308935	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	68	\N
1	2018-10-03 15:07:17.712481	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	69	\N
1	2018-10-03 15:44:20.198921	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	70	\N
1	2018-10-03 15:45:45.753781	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	71	\N
1	2018-10-03 15:48:34.427545	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	72	\N
1	2018-10-03 15:48:58.795437	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	73	\N
1	2018-10-03 15:51:55.268883	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	74	\N
1	2018-10-03 15:58:54.880704	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	75	\N
1	2018-10-03 15:59:43.701947	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	76	\N
1	2018-10-03 16:02:56.891394	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	77	\N
1	2018-10-03 16:15:27.720539	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	78	\N
1	2018-10-03 16:45:12.702303	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	79	\N
1	2018-10-03 17:06:35.056743	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	80	\N
1	2018-10-03 17:12:38.061806	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	81	\N
1	2018-10-03 17:50:18.945502	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	82	\N
1	2018-10-03 17:55:16.38827	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	83	\N
1	2018-10-03 17:58:17.954264	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	84	\N
1	2018-10-03 18:00:12.927253	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	85	\N
1	2018-10-03 18:01:47.846806	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	86	\N
1	2018-10-03 18:06:40.299273	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	87	\N
1	2018-10-03 18:10:34.081151	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	88	\N
1	2018-10-04 12:42:20.644739	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	89	\N
1	2018-10-04 15:28:42.877021	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	90	\N
1	2018-10-04 16:48:28.707929	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	91	\N
1	2018-10-05 18:01:49.430641	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	92	\N
1	2018-10-09 10:54:22.018787	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	93	\N
1	2018-10-09 11:20:53.173718	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	94	\N
1	2018-10-09 11:51:38.206089	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	95	\N
1	2018-10-09 12:00:26.020649	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	96	\N
1	2018-10-09 12:38:48.633064	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	97	\N
1	2018-10-09 14:05:28.078134	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	98	\N
1	2018-10-09 14:05:36.065628	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	99	\N
1	2018-10-09 14:23:00.013385	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	100	\N
1	2018-10-09 14:23:53.603872	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	101	\N
1	2018-10-09 14:29:31.129373	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	102	\N
1	2018-10-09 16:23:45.900444	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	103	\N
1	2018-10-09 16:32:13.30363	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	104	\N
1	2018-10-09 16:39:21.67685	2018年08月のご請求書の準備ができました。	2018年08月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、108.75円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	105	\N
1	2018-10-09 16:39:51.906833	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	106	\N
1	2018-10-15 15:45:56.639295	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	107	\N
1	2018-10-15 15:51:48.255564	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	108	\N
1	2018-10-16 10:12:40.867923	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	109	\N
1	2018-10-16 11:26:19.840252	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	110	\N
1	2018-10-16 11:31:26.99105	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	111	\N
1	2018-10-16 11:47:58.487528	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	112	\N
1	2018-10-16 11:50:54.676935	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	113	\N
1	2018-10-16 12:17:23.70676	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	114	\N
1	2018-10-16 14:36:02.327259	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	115	\N
1	2018-10-16 14:38:03.702042	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	116	\N
1	2018-10-16 14:59:47.853183	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	117	\N
1	2018-10-16 15:00:49.51323	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	118	\N
1	2018-10-16 15:06:39.337062	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、112.05円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	119	\N
1	2018-10-16 15:08:55.668318	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、116.37円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	120	\N
1	2018-10-16 15:27:34.059425	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、116.37円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	121	\N
1	2018-10-17 14:08:19.450174	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、116.37円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	122	\N
1	2018-10-22 10:38:43.645772	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、116.37円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	123	\N
1	2018-10-22 10:40:55.341115	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、116.37円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	124	\N
1	2018-10-30 14:13:29.677036	2018年09月のご請求書の準備ができました。	2018年09月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、116.37円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	125	\N
1	2018-11-05 19:45:02.520169	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	126	\N
1	2018-11-05 19:50:01.412666	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	127	\N
1	2018-11-08 12:08:52.022155	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	128	\N
1	2018-11-08 15:19:20.254155	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	129	\N
1	2018-11-13 12:45:29.972017	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	130	\N
1	2018-11-13 13:02:08.016515	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	131	\N
1	2018-11-13 13:14:37.686646	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	132	\N
1	2018-11-14 17:18:37.867443	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	133	\N
1	2018-11-19 17:07:43.210394	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	134	\N
1	2018-11-19 18:03:49.931407	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	135	\N
1	2018-11-19 18:12:01.637438	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	136	\N
1	2018-11-19 18:27:53.786559	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	137	\N
1	2018-11-19 19:44:15.854082	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	138	\N
1	2018-11-19 19:54:56.694282	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	139	\N
1	2018-11-19 20:03:06.545597	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	140	\N
1	2018-11-20 13:30:36.454654	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	141	\N
1	2018-11-20 13:36:43.773697	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	142	\N
1	2018-11-20 13:40:01.389362	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	143	\N
1	2018-11-20 13:44:03.607443	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	144	\N
1	2018-11-20 15:04:47.818521	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	145	\N
1	2018-11-20 15:46:11.805844	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	146	\N
1	2018-11-20 16:00:47.777121	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	147	\N
1	2018-11-20 16:03:34.252275	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	148	\N
1	2018-11-20 16:49:49.437846	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	149	\N
1	2018-11-20 19:22:21.942931	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	150	\N
1	2018-11-20 19:28:05.82218	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	151	\N
1	2018-11-20 19:31:28.837232	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	152	\N
1	2018-11-20 19:53:49.806658	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	153	\N
1	2018-11-21 15:23:27.774975	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	154	\N
1	2018-11-21 15:30:30.630111	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	155	\N
1	2018-11-21 17:43:41.628932	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	156	\N
1	2018-11-21 17:49:10.508849	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	157	\N
1	2018-11-21 18:32:50.917389	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	158	\N
1	2018-11-21 18:33:55.219427	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	159	\N
1	2018-11-21 18:36:50.235736	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	160	\N
1	2018-11-21 19:23:49.119595	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	161	\N
1	2018-11-26 14:34:05.440314	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	162	\N
1	2018-11-26 14:36:15.433067	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	163	\N
1	2018-11-26 15:35:06.555191	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	164	\N
1	2018-11-26 15:36:55.058781	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	165	\N
1	2018-11-26 15:40:58.545077	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	166	\N
1	2018-11-26 15:44:24.876448	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	167	\N
1	2018-11-26 15:47:23.056361	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	168	\N
1	2018-11-26 15:48:41.883217	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	169	\N
1	2018-11-26 15:49:41.165573	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	170	\N
1	2018-11-26 15:50:43.508864	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	171	\N
1	2018-11-26 15:51:41.021331	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	172	\N
1	2018-11-26 15:52:38.632656	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	173	\N
1	2018-11-26 15:53:53.25329	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	174	\N
1	2018-11-26 15:59:46.145943	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	175	\N
1	2018-11-26 16:00:12.809755	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	176	\N
1	2018-11-26 16:04:31.360083	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	177	\N
1	2018-11-26 16:58:07.657588	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	178	\N
1	2018-11-26 16:58:51.087443	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	179	\N
1	2018-11-26 17:15:38.512184	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	180	\N
1	2018-11-26 17:17:51.161295	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	181	\N
1	2018-11-26 17:18:17.01369	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	182	\N
1	2018-11-26 17:20:11.620243	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	183	\N
1	2018-11-26 17:21:03.463463	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	184	\N
1	2018-11-26 17:23:47.986252	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	185	\N
1	2018-11-26 17:25:19.425778	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	186	\N
1	2018-11-26 17:29:22.034311	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	187	\N
1	2018-11-26 17:32:16.583234	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	188	\N
1	2018-11-26 17:37:23.365591	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	189	\N
1	2018-11-26 17:39:39.09937	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	190	\N
1	2018-11-26 17:40:51.245271	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	191	\N
1	2018-11-26 17:41:55.399136	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	192	\N
1	2018-11-26 18:02:58.290363	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	193	\N
1	2018-11-27 13:50:18.945885	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	194	\N
1	2018-11-27 13:53:00.696596	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	195	\N
1	2018-11-27 14:30:00.459548	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	196	\N
1	2018-11-27 14:33:37.205885	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	197	\N
1	2018-11-27 14:34:18.690798	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	198	\N
1	2018-11-27 15:00:58.976814	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	199	\N
1	2018-11-27 15:01:24.618975	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	200	\N
1	2018-11-27 15:02:59.046682	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	201	\N
1	2018-11-27 15:06:31.415195	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	202	\N
1	2018-11-27 15:06:36.551556	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	203	\N
1	2018-11-27 15:36:18.471474	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	204	\N
1	2018-11-27 15:53:27.584912	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	205	\N
1	2018-11-27 15:59:43.612378	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	206	\N
1	2018-11-27 16:10:49.342167	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	207	\N
1	2018-12-03 15:19:45.287902	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	208	\N
1	2018-12-03 15:28:58.32283	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	209	\N
1	2018-12-03 15:31:24.765702	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	210	\N
1	2018-12-03 15:31:58.773981	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	211	\N
1	2018-12-03 15:33:48.183668	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	212	\N
1	2018-12-03 15:38:51.82653	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	213	\N
1	2018-12-03 15:43:13.000485	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	214	\N
1	2018-12-03 15:44:08.013768	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	215	\N
1	2018-12-03 16:12:02.795523	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	216	\N
1	2018-12-03 18:48:28.947792	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	217	\N
1	2018-12-03 18:51:24.659763	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	218	\N
1	2018-12-04 10:33:17.170662	2018年11月のご請求書の準備ができました。	2018年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	219	\N
1	2018-12-04 11:12:25.097873	2018年11月のご請求書の準備ができました。	2018年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	220	\N
1	2018-12-04 11:24:14.229325	2018年11月のご請求書の準備ができました。	2018年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	221	\N
1	2018-12-04 11:53:28.091569	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	222	\N
1	2018-12-04 11:53:51.104502	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	223	\N
1	2018-12-04 11:54:18.979595	2018年11月のご請求書の準備ができました。	2018年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	224	\N
1	2018-12-04 13:42:39.491604	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	225	\N
1	2018-12-04 13:43:02.624402	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	226	\N
1	2018-12-04 13:43:23.664767	2018年11月のご請求書の準備ができました。	2018年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	227	\N
1	2018-12-04 13:49:39.706824	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	228	\N
1	2018-12-04 13:49:43.249502	2018年11月のご請求書の準備ができました。	2018年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	229	\N
1	2018-12-04 13:51:12.814727	2018年11月のご請求書の準備ができました。	2018年11月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	230	\N
1	2018-12-04 15:29:02.46629	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	231	\N
1	2018-12-04 15:34:21.544991	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	232	\N
1	2018-12-04 15:34:39.734396	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	233	\N
1	2018-12-04 16:35:24.622243	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	234	\N
1	2018-12-04 16:36:17.567963	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	235	\N
1	2018-12-04 16:42:36.730266	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	236	\N
1	2018-12-04 16:43:04.104353	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	237	\N
1	2018-12-04 16:45:56.323204	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	238	\N
1	2018-12-04 16:46:14.139278	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	239	\N
1	2018-12-04 16:59:05.335996	2018年10月のご請求書の準備ができました。	2018年10月のご請求書の準備ができました。<br />請求書メニューからPDF形式にて、ダウンロードいただけます。<br />今月のレートは、115.41円/ドルでした。<br /><br />ご確認のほど、よろしくお願いいたします。	1	240	\N
\.


--
-- Data for Name: info_ctl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.info_ctl (cid, uid, mid, readdate) FROM stdin;
3	nakano@ids.co.jp	6	2018-02-20 11:14:26.814605
3	nakano@ids.co.jp	7	2018-02-20 11:32:27.758397
3	nakano@ids.co.jp	30	2018-07-31 12:17:47.168771
3	nakano@ids.co.jp	32	2018-07-31 12:28:00.921874
3	nakano@ids.co.jp	28	2018-08-06 13:39:56.850393
17	baohq@vn.ids.jp	13	2018-09-04 18:54:18.910509
17	baohq@vn.ids.jp	9	2018-09-04 18:54:25.630906
17	baohq@vn.ids.jp	12	2018-09-04 18:54:42.585385
17	baohq@vn.ids.jp	5	2018-09-04 18:58:07.484257
17	baohq@vn.ids.jp	11	2018-09-04 18:58:22.2342
17	baohq@vn.ids.jp	10	2018-09-04 19:00:54.329741
17	baohq@vn.ids.jp	8	2018-09-04 19:01:07.951475
17	baohq@vn.ids.jp	17	2018-09-04 19:02:14.143841
17	baohq@vn.ids.jp	16	2018-09-04 19:20:22.778116
17	baohq@vn.ids.jp	15	2018-09-04 19:22:22.247262
17	bao_tssayninh307@yahoo.com	37	2018-09-05 12:20:57.065777
17	bao_tssayninh307@yahoo.com	35	2018-09-05 12:21:04.07355
17	bao_tssayninh307@yahoo.com	34	2018-09-05 12:41:26.737539
17	bao_tssayninh307@yahoo.com	36	2018-09-05 16:11:36.737193
17	baohq@vn.ids.jp	35	2018-09-10 17:57:49.921323
17	baohq@vn.ids.jp	33	2018-09-10 17:58:38.757977
3	nakano@ids.co.jp	35	2018-09-11 15:49:55.287243
3	nakano@ids.co.jp	20	2018-09-11 15:50:00.261987
3	nakano@ids.co.jp	16	2018-09-11 15:50:03.191136
3	nakano@ids.co.jp	9	2018-09-11 15:52:50.018436
3	nakano@ids.co.jp	26	2018-09-11 15:53:21.438549
3	nakano@ids.co.jp	11	2018-09-11 15:53:55.516563
3	nakano@ids.co.jp	25	2018-09-11 15:55:21.174877
3	nakano@ids.co.jp	22	2018-09-11 15:55:35.819039
3	nakano@ids.co.jp	21	2018-09-11 15:55:37.440594
3	nakano@ids.co.jp	14	2018-09-11 15:55:48.493862
3	develop@ids.co.jp	37	2018-09-25 17:05:46.485014
3	nakano@ids.co.jp	34	2018-09-11 15:57:12.68761
3	nakano@ids.co.jp	24	2018-09-11 15:57:19.072249
3	nakano@ids.co.jp	19	2018-09-11 15:58:10.248101
3	nakano@ids.co.jp	17	2018-09-11 15:58:23.972241
3	nakano@ids.co.jp	13	2018-09-11 15:59:24.747009
3	nakano@ids.co.jp	23	2018-09-11 16:00:17.147623
3	nakano@ids.co.jp	29	2018-09-11 16:04:24.704446
3	nakano@ids.co.jp	27	2018-09-11 16:05:45.912286
3	nakano@ids.co.jp	15	2018-09-11 16:39:59.156573
17	truongthoa007@gmail.com	35	2018-09-20 19:04:14.785431
3	nakano@ids.co.jp	8	2018-09-11 16:53:47.828721
17	truongthoa007@gmail.com	34	2018-09-20 19:04:18.698653
17	baohqad@vn.ids.jp	37	2018-09-12 12:05:39.790133
17	baohq@vn.ids.jp	37	2018-09-13 11:49:42.266155
17	baohq@vn.ids.jp	36	2018-09-13 11:49:50.351275
17	baohq@vn.ids.jp	32	2018-09-13 11:49:54.957486
17	baohq@vn.ids.jp	25	2018-09-13 11:49:58.828442
17	baohq@vn.ids.jp	34	2018-09-13 11:50:02.726016
17	baohq@vn.ids.jp	21	2018-09-13 11:50:06.830383
17	baohq@vn.ids.jp	31	2018-09-13 11:50:13.075779
17	baohq@vn.ids.jp	30	2018-09-13 11:50:35.249744
17	baohq@vn.ids.jp	18	2018-09-13 11:50:40.14942
17	baohq@vn.ids.jp	29	2018-09-13 11:50:51.687073
3	nakano@ids.co.jp	5	2018-09-19 12:42:47.295042
11	talumobile@gmail.com	37	2018-09-19 15:52:12.342604
3	nakano@ids.co.jp	36	2018-09-19 16:56:56.034719
3	nakano@ids.co.jp	33	2018-09-19 16:57:00.996434
17	truongthoa007@gmail.com	32	2018-09-20 19:04:22.851228
17	truongthoa007@gmail.com	31	2018-09-20 19:04:24.618758
17	truongthoa007@gmail.com	30	2018-09-20 19:04:28.275485
17	truongthoa007@gmail.com	4	2018-09-20 19:07:01.070553
17	truongthoa007@gmail.com	12	2018-09-20 19:07:35.056985
54	phamdangthuy2310@gmail.com	37	2018-09-25 17:09:03.092671
3	nakano@ids.co.jp	4	2018-09-19 16:58:56.945498
3	nakano@ids.co.jp	12	2018-09-19 16:59:04.925108
54	phamdangthuy2310@gmail.com	35	2018-09-25 17:09:08.336247
54	phamdangthuy2310@gmail.com	31	2018-09-26 14:59:22.381183
3	nakano@ids.co.jp	10	2018-09-19 17:00:15.455516
54	phamdangthuy2310@gmail.com	36	2018-09-25 17:09:15.857014
3	nakano@ids.co.jp	1	2018-09-19 17:00:38.946854
3	nakano@ids.co.jp	2	2018-09-19 17:00:50.166897
3	develop@ids.co.jp	36	2018-09-20 16:16:19.448291
3	develop@ids.co.jp	35	2018-09-20 16:35:44.929519
3	develop@ids.co.jp	33	2018-09-20 16:35:47.283874
3	develop@ids.co.jp	14	2018-09-20 16:38:18.422551
3	lam123@mailinator.com	37	2018-09-20 17:21:59.30727
3	lam123@mailinator.com	36	2018-09-20 17:22:03.460896
17	truongthoa007@gmail.com	24	2018-09-20 19:02:02.168011
3	nakano@ids.co.jp	41	2018-09-25 17:20:43.286832
54	phamdangthuy2310@gmail.com	28	2018-09-26 14:59:24.78126
54	phamdangthuy2310@gmail.com	39	2018-09-24 12:35:07.742234
3	lam123@mailinator.com	39	2018-09-24 15:07:00.207195
3	lam123@mailinator.com	38	2018-09-24 15:07:06.115923
3	lam123@mailinator.com	35	2018-09-24 15:07:09.274628
3	develop@ids.co.jp	43	2018-09-25 18:27:03.594751
54	phamdangthuy2310@gmail.com	34	2018-09-25 18:28:51.635149
3	develop@ids.co.jp	40	2018-09-25 11:50:09.654275
3	nakano@ids.co.jp	37	2018-09-25 11:56:13.445035
3	nakano@ids.co.jp	31	2018-09-25 11:56:37.687194
3	nakano@ids.co.jp	18	2018-09-25 11:56:44.5014
54	phamdangthuy2310@gmail.com	33	2018-09-25 20:01:28.234261
54	phamdangthuy2310@gmail.com	32	2018-09-25 20:02:41.345324
3	lam123@mailinator.com	44	2018-09-26 11:32:51.632212
3	develop@ids.co.jp	44	2018-09-26 11:41:04.625561
3	develop@ids.co.jp	41	2018-09-25 17:05:11.857037
54	phamdangthuy2310@gmail.com	26	2018-09-26 14:59:27.302868
54	phamdangthuy2310@gmail.com	42	2018-09-26 14:59:39.223542
17	truongthoa007@gmail.com	36	2018-09-26 13:18:50.000379
17	truongthoa007@gmail.com	37	2018-09-26 13:18:57.03343
17	truongthoa007@gmail.com	33	2018-09-26 13:19:27.472579
54	phamdangthuy2310@gmail.com	44	2018-09-26 14:59:19.426539
1	nakano@ids.co.jp	44	2018-09-27 16:09:07.729279
1	nakano@ids.co.jp	42	2018-09-27 16:09:13.522293
1	nakano@ids.co.jp	125	2018-10-31 10:49:46.584781
591	kana_shin16@yahoo.co.jp	128	2018-11-08 15:11:10.83366
101	nakano@ids.co.jp	133	2018-11-14 19:32:20.042489
355	kodera@ids.co.jp	133	2018-11-15 15:56:09.058773
355	kodera@ids.co.jp	207	2018-12-03 01:09:02.156196
355	kodera@ids.co.jp	205	2018-12-03 11:08:29.800005
355	kodera@ids.co.jp	233	2018-12-04 16:13:47.945384
\.


--
-- Name: info_mid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.info_mid_seq', 46, true);


--
-- Data for Name: inst_req; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inst_req (id, cid, dept, pcode, inst_ami, inst_type, inst_count, inst_region, security_groups, using_purpose, using_from_date, using_till_date, shutdown_behavior, approved_status, req_date, empid, note, inst_vpc, inst_subnet, expired_notified, key_name, inst_address, status, inst_region_text, inst_vpc_text, inst_subnet_text, inst_ami_text) FROM stdin;
160	17	3	uytuy	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	tesst thoa	2018-09-19	2018-09-25	stop	0	2018-09-21 18:25:20.020308	38	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	54.250.207.211	\N	\N	\N	\N	\N
154	17	5	test009	ami-e25e959d	t2.micro	1	ap-northeast-1a	\N	thoa teeesstdsfghjkaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	2018-09-26	2018-09-25	stop	-1	2018-09-21 12:19:08.48352	39	id không hợp lệ	vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.113.133.184	\N	\N	\N	\N	\N
155	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	thoa tesst	2018-09-20	2018-10-02	stop	-2	2018-09-21 13:42:52.980177	42		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	54.250.207.211	\N	\N	\N	\N	\N
167	17	5	thoa_pcode_test	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thoa la ai	2018-09-28	2018-09-19	stop	-2	2018-09-24 13:28:42.98	39		vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	52.69.135.146	\N	\N	\N	\N	\N
164	17	5	thoa_pcode_test	ami-e25e959d	t2.micro	1	ap-northeast-1a	\N	thoa tsssss	2018-09-21	2018-09-30	stop	-2	2018-09-24 13:14:11.278981	39		vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	54.250.207.211	\N	\N	\N	\N	\N
144	17	5	test009	ami-e25e959d	t2.micro	1	ap-northeast-1a	\N	thoa test 利用終了日  1ngay nhân mail	2018-09-20	2018-09-20	stop	1	2018-09-21 12:43:10.668898	39	test	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	13.113.133.184	\N	\N	\N	\N	\N
165	17	5	thoa_pcode_test	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	thoa test	2018-09-24	2018-09-24	stop	-2	2018-09-24 13:21:37.500601	39		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	52.69.135.146	\N	\N	\N	\N	\N
185	17	6	test009	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thoa test 002	2018-09-20	2018-09-26	stop	-2	2018-09-25 12:13:34.726822	39		vpc-95669ef2	subnet-c2eeafea	0	\N	13.115.66.111	\N	\N	\N	\N	\N
195	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	2	ap-northeast-1c	\N	Test count instant	2018-09-25	2018-11-30	stop	1	2018-09-25 15:20:33.858955	1		vpc-95669ef2	subnet-8057c8c9	0	\N		\N	\N	\N	\N	\N
175	17	5	test009	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	ứdgfthoa	2018-10-05	2018-09-27	stop	-2	2018-09-24 16:37:05.227277	38		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	18.179.73.157	\N	\N	\N	\N	\N
150	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	ewfew	2018-10-04	2018-09-16	stop	-2	2018-09-20 17:58:20.203912	40		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.113.133.184	\N	\N	\N	\N	\N
149	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	abc	2018-09-13	2018-09-25	stop	-2	2018-09-20 17:56:36.082792	40		vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	52.199.82.25	\N	\N	\N	\N	\N
145	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	tesst truongthoa007@gmail.com	2018-09-20	2018-09-20	stop	-2	2018-09-20 15:21:06.629329	40		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	52.199.82.25	\N	\N	\N	\N	\N
171	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thoa test	2018-09-27	2018-09-11	stop	-2	2018-09-24 15:11:04.581213	40		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	52.198.143.231	\N	\N	\N	\N	\N
163	17	5	test009	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	sad thoa	2018-09-12	2018-09-23	stop	-2	2018-09-24 12:48:04.24279	38		vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	13.114.128.47	\N	\N	\N	\N	\N
157	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	1	2018-10-04	2018-09-19	stop	0	2018-09-21 16:19:11.035291	38	\N	vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	54.250.207.211	\N	\N	\N	\N	\N
168	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thoa laa	2018-09-26	2018-09-20	stop	-1	2018-09-24 13:35:03.457641	39	thất bai	vpc-95669ef2	subnet-c2eeafea	0	\N	18.179.73.157	\N	\N	\N	\N	\N
188	54	1	thuyproject	ami-e25e959d	t2.micro	2	ap-northeast-1c	\N	reasons	2018-09-01	2018-09-30	stop	1	2018-09-25 12:31:17.649529	1		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
169	17	5	test009	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	thoa	2018-09-18	2018-09-26	stop	0	2018-09-24 13:38:41.274874	39	\N	vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	18.179.73.157	\N	\N	\N	\N	\N
174	17	5	gfvnm v	ami-e25e959d	t2.micro	1	ap-northeast-1a	\N	thoa test	2018-09-28	2018-09-13	stop	0	2018-09-24 16:19:09.702631	38	\N	vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	18.179.73.157	\N	\N	\N	\N	\N
158	17	3	A001	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	tjozzdg	2018-09-27	2018-09-24	stop	0	2018-09-21 16:27:27.059732	38	\N	vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	54.250.207.211	\N	\N	\N	\N	\N
206	17	2	demo1	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	2ka2	2018-09-27	2018-10-06	terminate	0	2018-09-26 13:46:59.186947	38	\N	vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	13.114.128.47	\N	\N	\N	\N	\N
152	17	3	A001	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	test a	2018-09-17	2018-09-20	stop	1	2018-09-20 20:14:28.597622	38		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.113.133.184	\N	\N	\N	\N	\N
153	17	2	A001	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thoa tesst 123	2018-09-29	2018-09-18	stop	0	2018-09-21 12:11:00.864527	38	\N	vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N	52.192.85.181	\N	\N	\N	\N	\N
193	3	1	abccccsd	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1c	\N	aaaa	2018-09-20	2018-10-04	stop	1	2018-09-25 15:00:25.541399	18		vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N		\N	\N	\N	\N	\N
194	3	1	CCC	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1c	\N	zazzzzz	2018-09-20	2018-10-05	stop	1	2018-09-25 15:14:46.739202	18		vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N		\N	\N	\N	\N	\N
197	3	1	CCC	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	kakaka	2018-09-11	2018-10-04	stop	1	2018-09-25 15:32:22.504258	18		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N		\N	\N	\N	\N	\N
204	3	9	abccccsd	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1c	\N	123	2018-09-01	2018-09-30	stop	1	2018-09-25 17:29:01.973263	1		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
202	3	9	CCC	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1c	\N	test	2018-09-25	2018-09-26	stop	-2	2018-09-25 17:18:40.50521	14		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
203	3	9	abccccsd	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1c	\N	111	2018-09-01	2018-09-30	stop	1	2018-09-25 17:20:19.217644	1		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
151	17	5	thoa_pcode_test	ami-e25e959d	t2.micro	1	ap-northeast-1a	\N	test	2018-09-21	2018-09-25	stop	-2	2018-09-20 18:25:39.362804	39		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.113.133.184	\N	\N	\N	\N	\N
156	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	deagtrthoa	2018-09-28	2018-09-24	stop	-2	2018-09-21 15:06:02.486315	39		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	54.250.207.211	\N	\N	\N	\N	\N
182	17	5	dfheadgt2133g	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thoa kho	2018-09-20	2018-09-25	stop	0	2018-09-24 18:19:04.290673	38	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	13.113.133.184	\N	\N	\N	\N	\N
146	3	1	コードテスト	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1a	\N	abc	2018-09-20	2018-09-21	stop	1	2018-09-20 16:22:28.997981	14		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
200	54	1	thuyproject	ami-e25e959d	t2.micro	2	ap-northeast-1c	\N	free test	2018-09-25	2018-09-26	terminate	0	2018-09-25 16:06:36.226408	1	\N	vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N		\N	\N	\N	\N	\N
187	3	1	xcxcxcv	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1c	\N	Test count instant	2018-09-25	2018-09-29	stop	1	2018-09-25 12:22:07.818363	1		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	13.115.66.111	\N	\N	\N	\N	\N
176	17	5	007tesst 007	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thóaafdg	2018-09-20	2018-10-01	stop	0	2018-09-24 16:24:24.349396	39	\N	vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	18.179.73.157	\N	\N	\N	\N	\N
147	3	1	コードテスト	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1a	\N	abc	2018-09-20	2018-09-21	stop	1	2018-09-20 16:31:22.869274	14	i don't like it	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
201	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1c	\N	test	2018-09-25	2018-09-25	stop	0	2018-09-25 16:49:05.015293	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
189	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1d	\N	Test count instant	2018-09-27	2018-10-03	stop	1	2018-09-25 12:38:16.507776	1		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	52.192.85.181	\N	\N	\N	\N	\N
190	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1c	\N	t ugjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh	2018-09-25	2018-09-28	stop	0	2018-09-25 12:45:25.89252	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	52.192.85.181	\N	\N	\N	\N	\N
205	17	5	123456あ	ami-e25e959d	t2.micro	1	ap-northeast-1a	\N	th	2018-09-27	2018-10-03	stop	1	2018-09-26 11:42:18.270887	42		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.114.128.47	\N	\N	\N	\N	\N
191	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1c	\N	Test count instant	2018-09-26	2018-09-24	stop	1	2018-09-25 13:23:22.769745	1		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	52.198.143.231	\N	\N	\N	\N	\N
196	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	2	ap-northeast-1c	\N	test count instant 2	2018-09-26	2018-09-29	stop	1	2018-09-25 15:22:20.594456	1		vpc-00a59e518e08e65a3	subnet-0747a3e970cee2551	0	\N		\N	\N	\N	\N	\N
166	3	1	CCC	ami-0dd0178f887fc0a08	t1.micro	1	ap-northeast-1a	\N	aaa	2018-09-24	2018-10-04	stop	1	2018-09-24 13:29:01.590841	18	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
172	3	6	CCC	ami-0dd0178f887fc0a08	t1.micro	2	ap-northeast-1d	\N	Tri test	2018-09-02	2018-09-20	stop	1	2018-09-24 15:27:18.750695	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	18.179.73.157	\N	\N	\N	\N	\N
161	3	1	コードテスト	ami-e25e959d	t1.micro	1	ap-northeast-1c	\N	Yrdy	2018-09-04	2018-09-28	stop	1	2018-09-21 18:44:58.913405	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	52.69.135.146	\N	\N	\N	\N	\N
162	3	1	コードテスト	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1a	\N	aa	2018-09-21	2018-09-22	stop	1	2018-09-21 18:55:46.146578	13	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
173	3	1	abccccsd	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1c	\N	13: 29 test micro2	2018-09-18	2018-10-04	stop	1	2018-09-24 15:39:11.928882	18	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
177	3	1	xcxcxcv	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1a	\N	test dashboard	2018-09-24	2018-09-30	stop	1	2018-09-24 16:25:05.889662	1	\N	vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	18.179.73.157	\N	\N	\N	\N	\N
170	3	1	abccccsd	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1a	\N	t2 micro	2018-09-18	2018-09-28	stop	1	2018-09-24 13:45:14.673399	18	not goodあああ	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
178	3	6	CCC	ami-e25e959d	t2.micro	1	ap-northeast-1a	\N	test	2018-09-24	2018-09-26	stop	1	2018-09-24 16:45:20.408483	1		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	18.179.73.157	\N	\N	\N	\N	\N
180	3	6	CCC	ami-0dd0178f887fc0a08	t1.micro	2	ap-northeast-1c	\N	testing	2018-09-03	2018-09-20	stop	1	2018-09-24 17:15:10.742736	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
181	3	1	abccccsd	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1a	\N	lam xxx	2018-09-19	2018-10-05	stop	1	2018-09-24 17:17:52.866245	18	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
159	3	1	CCC	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1a	\N	change to t2 micro	2018-09-19	2018-09-28	stop	1	2018-09-24 17:10:23.839123	18		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
179	3	1	コードテスト	ami-e25e959d	t1.micro	2	ap-northeast-1a	\N	Tri Truong	2018-09-11	2018-09-27	stop	1	2018-09-24 17:12:47.129987	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
148	3	1	コードテスト	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1a	\N	sony sony	2018-09-20	2018-09-28	stop	1	2018-09-20 17:40:58.270237	18		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
186	17	6	thoa_pcode_test	ami-e25e959d	t2.micro	1	ap-northeast-1d	\N	thoaaa	2018-09-14	2018-09-27	stop	0	2018-09-25 12:15:26.200699	39	\N	vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.115.66.111	\N	\N	\N	\N	\N
198	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	2	ap-northeast-1c	\N	test count instant approve	2018-09-25	2018-09-26	stop	0	2018-09-25 15:23:53.965923	1	\N	vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N		\N	\N	\N	\N	\N
199	3	1	abccccsd	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1c	\N	micro micro	2018-09-18	2018-10-05	stop	0	2018-09-25 15:34:55.792286	18	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
192	54	1	thuyproject	ami-0dd0178f887fc0a08	t2.micro	1	ap-northeast-1c	\N	test instant expired	2018-09-23	2018-09-24	stop	1	2018-09-25 13:32:34.393569	1	'';;	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	52.192.85.181	\N	\N	\N	\N	\N
207	17	5	demo1	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	thoa	2018-09-29	2018-09-25	stop	0	2018-09-26 16:09:02.599357	38	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	13.114.128.47	\N	\N	\N	\N	\N
184	17	6	thoa_pcode_test	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	thoa thoa	2018-09-19	2018-09-21	stop	-2	2018-09-26 11:11:30.982062	42		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.114.128.47	\N	\N	\N	\N	\N
183	17	6	thoa_pcode_test	ami-e25e959d	t2.micro	1	ap-northeast-1c	\N	thoa test test	2018-09-21	2018-09-14	stop	-2	2018-09-26 11:08:19.838084	42		vpc-0fc82035797217ee1	subnet-0db52ddfbe325d32c	0	\N	13.114.128.47	\N	\N	\N	\N	\N
208	3	9	abccccsd	ami-06dabe70154bf1d7b	m1.small	1	ap-northeast-1c	\N	Testing	2018-09-02	2018-09-27	stop	0	2018-09-26 16:25:08.964249	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	13.114.128.47	\N	\N	\N	\N	\N
210	1	1	SMOJ	ami-00ec38aafebf4d73f	t2.micro	1	ap-northeast-1a	\N	aaa	2018-10-26	2019-03-14	stop	1	2018-10-03 11:22:55.264853	1		vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	52.69.67.233	\N	\N	\N	\N	\N
209	1	1	SMOJ	ami-00ec38aafebf4d73f	t2.micro	1	ap-northeast-1a	\N	aaa	2018-09-29	2018-11-14	stop	0	2018-10-04 10:28:23.140917	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	52.192.85.181	\N	\N	\N	\N	\N
212	1	1	SMOJ	ami-00ec38aafebf4d73f	t2.micro	1	ap-northeast-1a	\N	あああ\r\n1\r\n2\r\n3\r\n4\r\n5\r\n6\r\n6\r\n7\r\n8\r\n8\r\n9\r\n10\r\n12\r\nあ\r\nあああ\r\nあ	2018-10-31	2019-02-21	stop	0	2018-10-15 11:16:28.500712	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	13.230.1.16	\N	\N	\N	\N	\N
213	1	1	SMOJ	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1a	\N	アカウント	2018-10-25	2019-01-24	stop	0	2018-10-15 11:32:59.916004	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	13.230.1.16	\N	\N	\N	\N	\N
211	1	1	SMOJ	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1a	\N	aaa	2018-10-18	2019-01-11	stop	0	2018-10-15 11:36:40.686756	1	\N	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N		\N	\N	\N	\N	\N
214	1	1	SMOJ	ami-06dabe70154bf1d7b	t2.micro	1	ap-northeast-1d	\N	Web server.	2018-11-01	2019-06-01	stop	-5	2018-10-31 10:55:10.937503	1	セキュリティグループの作成に失敗しました。	vpc-00a59e518e08e65a3	subnet-070c3e8351b94ebef	0	\N	18.179.63.147	\N	\N	\N	\N	\N
215	355	3	SV	ami-002cda05db7b2adef	t2.micro	1	ap-northeast-1	\N	あ	2018-12-26	2019-03-14	stop	0	2018-12-12 15:20:02.806443	1		vpc-0c9fed11115de5599	subnet-0e7f0e809e92e854d	0	\N	52.69.108.208	\N	東京	vpc-0c9fed11115de5599 (10.0.0.0/16)	10.0.2.0/24 (ap-northeast-1c)	20180905_backup (Other Linux x86_64)
\.


--
-- Data for Name: inst_req_bs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inst_req_bs (id, inst_req_id, device_name, volumn_type, volumn_size, iops, encrypted, auto_removal, snapshot_id) FROM stdin;
277	145	/dev/sdb	standard	1	0	0	1	\N
283	146	/dev/sdd	standard	8	0	0	1	\N
284	147	/dev/sdd	standard	8	0	0	1	\N
285	148	/dev/sdb	standard	8	0	0	1	\N
287	149	/dev/sdb	standard	1	0	0	1	\N
288	150	/dev/sdb	standard	1	0	0	1	\N
289	151	/dev/sdb	standard	1	0	0	1	\N
290	152	/dev/sdb	standard	1	0	0	1	\N
291	153	/dev/sdb	standard	1	0	0	1	\N
292	154	/dev/sdb	standard	1	0	0	1	\N
293	144	/dev/sdb	standard	1	0	0	1	\N
294	155	/dev/sdb	standard	1	0	0	1	\N
295	156	/dev/sdb	standard	1	0	0	1	\N
296	157	/dev/sdb	standard	1	0	0	1	\N
302	158	/dev/sdb	standard	1	0	0	1	\N
303	158	/dev/sdc	standard	1	0	0	0	\N
304	158	/dev/sdh	standard	1	0	0	0	\N
305	158	/dev/sdj	standard	1	0	0	0	\N
307	160	/dev/sdf	standard	1	0	0	0	\N
308	160	/dev/sdd	standard	1	0	0	0	\N
309	160	/dev/sdh	standard	1	0	0	0	\N
310	160	/dev/sdj	standard	1	0	0	0	\N
312	161	/dev/sdb	standard	12	0	0	1	\N
313	162	/dev/sdb	standard	2	0	0	1	\N
314	162	/dev/sdc	standard	3	0	0	1	\N
315	162	/dev/sdd	standard	4	0	0	1	\N
318	163	/dev/sdb	standard	1	0	0	1	\N
319	164	/dev/sdb	standard	1	0	0	1	\N
320	165	/dev/sdb	standard	1	0	0	1	\N
322	167	/dev/sdb	standard	1	0	0	0	\N
323	166	/dev/sdb	standard	10	0	0	1	\N
324	168	/dev/sdb	standard	1	0	0	1	\N
325	169	/dev/sdb	standard	1	0	0	1	\N
326	170	/dev/sdb	standard	9	0	0	1	\N
327	171	/dev/sdb	standard	1	0	0	1	\N
328	172	/dev/sdb	standard	23	0	0	1	\N
329	172	/dev/sdg	standard	21	0	0	1	\N
330	172	/dev/sdf	standard	34	0	0	1	\N
339	173	/dev/sdb	gp2	8	0	0	1	\N
340	173	/dev/sdc	standard	9	0	0	1	\N
341	173	/dev/sdg	standard	10	0	0	0	\N
342	174	/dev/sdb	standard	1	0	0	1	\N
344	176	/dev/sdb	standard	1	0	0	1	\N
345	177	/dev/sdb	standard	8	0	0	1	\N
347	175	/dev/sdb	standard	1	0	0	1	\N
348	178	/dev/sdb	standard	8	0	0	1	\N
349	178	/dev/sdg	standard	4	0	0	1	\N
351	159	/dev/sdd	standard	8	0	0	1	\N
352	179	/dev/sdb	standard	21	0	0	0	\N
353	179	/dev/sdj	standard	20	0	0	1	\N
355	180	/dev/sdb	standard	10	0	0	1	\N
356	181	/dev/sdb	standard	9	0	0	1	\N
357	182	/dev/sdl	standard	1	0	0	0	\N
358	182	/dev/sdb	standard	1	0	0	1	\N
361	185	/dev/sdb	standard	1	0	0	0	\N
362	186	/dev/sdb	standard	1	0	0	1	\N
365	187	/dev/sdk	standard	4	0	0	1	\N
366	187	/dev/sdb	standard	8	0	0	1	\N
368	188	/dev/sde	standard	8	0	0	1	\N
370	189	/dev/sdk	standard	8	0	0	1	\N
372	190	/dev/sdk	standard	4	0	0	1	\N
374	191	/dev/sdk	standard	4	0	0	1	\N
376	192	/dev/sdk	standard	4	0	0	1	\N
378	193	/dev/sdb	standard	10	0	0	1	\N
379	194	/dev/sdb	standard	9	0	0	1	\N
380	195	/dev/sdk	standard	4	0	0	1	\N
381	196	/dev/sdk	standard	4	0	0	1	\N
386	198	/dev/sdk	standard	4	0	0	1	\N
387	197	/dev/sdb	standard	8	0	0	0	\N
388	197	/dev/sdc	io1	9	101	0	1	\N
389	197	/dev/sde	standard	10	0	0	1	\N
390	197	/dev/sdh	gp2	11	0	0	0	\N
391	199	/dev/sdb	standard	9	0	0	1	\N
392	200	/dev/sdb	standard	1	0	0	1	\N
393	200	/dev/sdl	standard	1	0	0	1	\N
394	200	/dev/sdj	standard	1	0	0	1	\N
395	200	/dev/sdf	standard	1	0	0	1	\N
396	200	/dev/sdd	standard	1	0	0	1	\N
397	200	/dev/sdc	standard	1	0	0	1	\N
398	201	/dev/sdb	standard	8	0	0	1	\N
399	202	/dev/sdb	standard	4	0	0	1	\N
400	203	/dev/sdb	standard	1	0	0	1	\N
401	204	/dev/sdb	gp2	1	0	0	1	\N
402	183	/dev/sdb	standard	1	0	0	0	\N
404	184	/dev/sdj	standard	1	0	0	1	\N
405	205	/dev/sdb	standard	1	0	0	1	\N
409	206	/dev/sdb	standard	1	0	0	0	\N
410	207	/dev/sdb	standard	1	0	0	1	\N
416	208	/dev/sdb	standard	1	0	0	1	\N
417	208	/dev/sdk	standard	5	0	0	1	\N
418	208	/dev/sdj	standard	6	0	0	1	\N
420	210	/dev/sdk	standard	5	0	0	1	\N
421	209	/dev/sdf	gp2	10	0	0	1	\N
423	212	/dev/sdb	standard	5	0	0	1	\N
424	213	/dev/sdb	standard	10	0	0	1	\N
425	211	/dev/sdb	standard	3	0	0	1	\N
426	214	/dev/sdb	gp2	30	0	0	1	\N
427	214	/dev/sdc	gp2	30	0	0	1	\N
429	215	/dev/sdb	standard	10	0	0	1	\N
\.


--
-- Name: inst_req_bs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inst_req_bs_id_seq', 429, true);


--
-- Data for Name: inst_req_change; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inst_req_change (id, inst_req_id, cid, empid, dept, pcode, using_purpose, using_from_date, using_till_date, inst_address) FROM stdin;
9	24	3	1	6	CCC	Test edit instance (edited)	2018-08-01	2018-08-31	
26	165	17	39	5	thoa_pcode_test	thoa test thay đổi thông tin	2018-09-13	2018-09-24	52.69.135.146
3	103	17	35	3	A001	drgsdasdasdasd	2018-09-04	2018-09-22	18.182.143.50
8	110	17	35	3	A001	test change instance approved	2018-09-03	2018-09-12	
22	152	17	38	3	A001	test a	2018-09-17	2018-09-23	
28	189	54	1	1	thuyproject	Test count instant	2018-08-23	2018-08-31	
29	191	54	1	1	thuyproject	Test count instant	2018-09-24	2018-09-24	
1	15	3	1	1	AABDDDD	this is demo (edited 5)	2018-08-02	2018-09-29	52.193.244.128
\.


--
-- Name: inst_req_change_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inst_req_change_id_seq', 29, true);


--
-- Name: inst_req_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inst_req_id_seq', 215, true);


--
-- Data for Name: instance; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instance (cid, id, name, type, sg, platform, kernelid, ramdiskid, imageid, state, privatednsname, publicdnsname, launchtime, availabilityzone, privateip, publicip, rootdevicename, lastchecked, pcode, inst_req_id, elasticip, tbl_id, interface_id) FROM stdin;
\.


--
-- Data for Name: instance_request; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.instance_request (cid, reqid, instance_id, createdate, deletedate) FROM stdin;
\.


--
-- Name: instance_tbl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.instance_tbl_id_seq', 49181, true);


--
-- Data for Name: insttype; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.insttype (curgen, family, name, cpu, memory, storage, ebsopt, netperf, cid, price, region, effdate, id) FROM stdin;
Yes	General purpose	a1.2xlarge	8	16	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65032
Yes	General purpose	a1.4xlarge	16	32	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65033
Yes	General purpose	a1.large	2	4	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65034
Yes	General purpose	a1.medium	1	2	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65035
Yes	General purpose	a1.xlarge	4	8	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65036
No	Compute optimized	c1.medium	2	1.7	1 x 350		Moderate	0	0	\N	2018-12-01	65037
No	Compute optimized	c1.xlarge	8	7	4 x 420	Yes	High	0	0	\N	2018-12-01	65038
No	Compute optimized	c3.2xlarge	8	15	2 x 80 SSD	Yes	High	0	0	\N	2018-12-01	65040
No	Compute optimized	c3.4xlarge	16	30	2 x 160 SSD	Yes	High	0	0	\N	2018-12-01	65042
No	Compute optimized	c3.8xlarge	32	60	2 x 320 SSD		10 Gigabit	0	0	\N	2018-12-01	65044
No	Compute optimized	c3.large	2	3.75	2 x 16 SSD		Moderate	0	0	\N	2018-12-01	65045
No	Compute optimized	c3.xlarge	4	7.5	2 x 40 SSD	Yes	Moderate	0	0	\N	2018-12-01	65046
Yes	Compute optimized	c4.2xlarge	8	15	EBS only		High	0	0	\N	2018-12-01	65048
Yes	Compute optimized	c4.4xlarge	16	30	EBS only		High	0	0	\N	2018-12-01	65049
Yes	Compute optimized	c4.8xlarge	36	60	EBS only		10 Gigabit	0	0	\N	2018-12-01	65050
Yes	Compute optimized	c4.large	2	3.75	EBS only		Moderate	0	0	\N	2018-12-01	65051
Yes	Compute optimized	c4.xlarge	4	7.5	EBS only		High	0	0	\N	2018-12-01	65052
Yes	Compute optimized	c5.18xlarge	72	144	EBS only		25 Gigabit	0	0	\N	2018-12-01	65053
Yes	Compute optimized	c5.2xlarge	8	16	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65054
Yes	Compute optimized	c5.4xlarge	16	32	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65055
Yes	Compute optimized	c5.9xlarge	36	72	EBS only		10 Gigabit	0	0	\N	2018-12-01	65056
Yes	Compute optimized	c5.large	2	4	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65057
Yes	Compute optimized	c5.xlarge	4	8	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65058
Yes	Compute optimized	c5d.18xlarge	72	144	2 x 900 NVMe SSD		25 Gigabit	0	0	\N	2018-12-01	65059
Yes	Compute optimized	c5d.2xlarge	8	16	1 x 200 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65060
Yes	Compute optimized	c5d.4xlarge	16	32	1 x 400 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65061
Yes	Compute optimized	c5d.9xlarge	36	72	1 x 900 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65062
Yes	Compute optimized	c5d.large	2	4	1 x 50 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65063
Yes	Compute optimized	c5d.xlarge	4	8	1 x 100 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65064
Yes	Compute optimized	c5n.18xlarge	72	192	EBS only		100 Gigabit	0	0	\N	2018-12-01	65065
Yes	Compute optimized	c5n.2xlarge	8	21	EBS only		Up to 25 Gigabit	0	0	\N	2018-12-01	65066
Yes	Compute optimized	c5n.4xlarge	16	42	EBS only		Up to 25 Gigabit	0	0	\N	2018-12-01	65067
Yes	Compute optimized	c5n.9xlarge	36	96	EBS only		50 Gigabit	0	0	\N	2018-12-01	65068
Yes	Compute optimized	c5n.large	2	5.25	EBS only		Up to 25 Gigabit	0	0	\N	2018-12-01	65069
Yes	Compute optimized	c5n.xlarge	4	10.5	EBS only		Up to 25 Gigabit	0	0	\N	2018-12-01	65070
No	Compute optimized	cc2.8xlarge	32	60.5	4 x 840		10 Gigabit	0	0	\N	2018-12-01	65071
No	Memory optimized	cr1.8xlarge	32	244	2 x 120 SSD		10 Gigabit	0	0	\N	2018-12-01	65072
Yes	Storage optimized	d2.2xlarge	8	61	6 x 2000 HDD		High	0	0	\N	2018-12-01	65073
Yes	Storage optimized	d2.4xlarge	16	122	12 x 2000 HDD		High	0	0	\N	2018-12-01	65074
Yes	Storage optimized	d2.8xlarge	36	244	24 x 2000 HDD		10 Gigabit	0	0	\N	2018-12-01	65075
Yes	Storage optimized	d2.xlarge	4	30.5	3 x 2000 HDD		Moderate	0	0	\N	2018-12-01	65076
Yes	FPGA Instances	f1.16xlarge	64	976	EBS only		20 Gigabit	0	0	\N	2018-12-01	65077
Yes	FPGA Instances	f1.2xlarge	8	122	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65078
Yes	FPGA Instances	f1.4xlarge	16	244	1 x 940 GB		10 Gigabit	0	0	\N	2018-12-01	65079
No	GPU instance	g2.2xlarge	8	15	1 x 60 SSD	Yes	High	0	0	\N	2018-12-01	65080
No	GPU instance	g2.8xlarge	32	60	2 x 120 SSD		10 Gigabit	0	0	\N	2018-12-01	65082
Yes	GPU instance	g3.16xlarge	64	488	EBS only		20 Gigabit	0	0	\N	2018-12-01	65083
Yes	GPU instance	g3.4xlarge	16	122	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65084
Yes	GPU instance	g3.8xlarge	32	244	EBS only		10 Gigabit	0	0	\N	2018-12-01	65085
Yes	GPU instance	g3s.xlarge	4	30.5	EBS only		10 Gigabit	0	0	\N	2018-12-01	65086
Yes	Storage optimized	h1.16xlarge	64	256	8 x 2000 HDD		25 Gigabit	0	0	\N	2018-12-01	65087
Yes	Storage optimized	h1.2xlarge	8	32	1 x 2000 HDD		Up to 10 Gigabit	0	0	\N	2018-12-01	65088
Yes	Storage optimized	h1.4xlarge	16	64	2 x 2000 HDD		Up to 10 Gigabit	0	0	\N	2018-12-01	65089
Yes	Storage optimized	h1.8xlarge	32	128	4 x 2000 HDD		10 Gigabit	0	0	\N	2018-12-01	65090
No	Storage optimized	hs1.8xlarge	17	117	24 x 2000		10 Gigabit	0	0	\N	2018-12-01	65091
No	Storage optimized	i2.2xlarge	8	61	2 x 800 SSD		High	0	0	\N	2018-12-01	65092
No	Storage optimized	i2.4xlarge	16	122	4 x 800 SSD		High	0	0	\N	2018-12-01	65093
No	Storage optimized	i2.8xlarge	32	244	8 x 800 SSD		10 Gigabit	0	0	\N	2018-12-01	65094
No	Storage optimized	i2.xlarge	4	30.5	1 x 800 SSD		Moderate	0	0	\N	2018-12-01	65095
Yes	Storage optimized	i3.16xlarge	64	488	8 x 1900 NVMe SSD		20 Gigabit	0	0	\N	2018-12-01	65096
Yes	Storage optimized	i3.2xlarge	8	61	1 x 1900 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65097
Yes	Storage optimized	i3.4xlarge	16	122	2 x 1900 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65098
Yes	Storage optimized	i3.8xlarge	32	244	4 x 1900 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65099
Yes	Storage optimized	i3.large	2	15.25	1 x 475 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65100
Yes	Storage optimized	i3.metal	72	512	8 x 1900 NVMe SSD		25 Gigabit	0	0	\N	2018-12-01	65101
Yes	Storage optimized	i3.xlarge	4	30.5	1 x 950 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65102
No	General purpose	m1.large	2	7.5	2 x 420	Yes	Moderate	0	0	\N	2018-12-01	65103
No	General purpose	m1.medium	1	3.75	1 x 410		Moderate	0	0	\N	2018-12-01	65105
No	General purpose	m1.small	1	1.7	1 x 160		Low	0	0	\N	2018-12-01	65106
No	General purpose	m1.xlarge	4	15	4 x 420	Yes	High	0	0	\N	2018-12-01	65107
No	Memory optimized	m2.2xlarge	4	34.2	1 x 850	Yes	Moderate	0	0	\N	2018-12-01	65109
No	Memory optimized	m2.4xlarge	8	68.4	2 x 840	Yes	High	0	0	\N	2018-12-01	65111
No	Memory optimized	m2.xlarge	2	17.1	1 x 420		Moderate	0	0	\N	2018-12-01	65113
No	General purpose	m3.2xlarge	8	30	2 x 80 SSD	Yes	High	0	0	\N	2018-12-01	65114
No	General purpose	m3.large	2	7.5	1 x 32 SSD		Moderate	0	0	\N	2018-12-01	65116
No	General purpose	m3.medium	1	3.75	1 x 4 SSD		Moderate	0	0	\N	2018-12-01	65117
No	General purpose	m3.xlarge	4	15	2 x 40 SSD	Yes	High	0	0	\N	2018-12-01	65118
Yes	General purpose	m4.10xlarge	40	160	EBS only		10 Gigabit	0	0	\N	2018-12-01	65120
Yes	General purpose	m4.16xlarge	64	256	EBS only		20 Gigabit	0	0	\N	2018-12-01	65121
Yes	General purpose	m4.2xlarge	8	32	EBS only		High	0	0	\N	2018-12-01	65122
Yes	General purpose	m4.4xlarge	16	64	EBS only		High	0	0	\N	2018-12-01	65123
Yes	General purpose	m4.large	2	8	EBS only		Moderate	0	0	\N	2018-12-01	65124
Yes	General purpose	m4.xlarge	4	16	EBS only		High	0	0	\N	2018-12-01	65125
Yes	General purpose	m5.12xlarge	48	192	EBS only		10 Gigabit	0	0	\N	2018-12-01	65126
Yes	General purpose	m5.24xlarge	96	384	EBS only		25 Gigabit	0	0	\N	2018-12-01	65127
Yes	General purpose	m5.2xlarge	8	32	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65128
Yes	General purpose	m5.4xlarge	16	64	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65129
Yes	General purpose	m5.large	2	8	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65130
Yes	General purpose	m5.xlarge	4	16	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65131
Yes	General purpose	m5a.12xlarge	48	192	EBS only		10 Gigabit	0	0	\N	2018-12-01	65132
Yes	General purpose	m5a.24xlarge	96	384	EBS only		20 Gigabit	0	0	\N	2018-12-01	65133
Yes	General purpose	m5a.2xlarge	8	32	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65134
Yes	General purpose	m5a.4xlarge	16	64	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65135
Yes	General purpose	m5a.large	2	8	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65136
Yes	General purpose	m5a.xlarge	4	16	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65137
Yes	General purpose	m5d.12xlarge	48	192	2 x 900 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65138
Yes	General purpose	m5d.24xlarge	96	384	4 x 900 NVMe SSD		25 Gigabit	0	0	\N	2018-12-01	65139
Yes	General purpose	m5d.2xlarge	8	32	1 x 300 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65140
Yes	General purpose	m5d.4xlarge	16	64	2 x 300 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65141
Yes	General purpose	m5d.large	2	8	1 x 75 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65142
Yes	General purpose	m5d.xlarge	4	16	1 x 150 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65143
Yes	GPU instance	p2.16xlarge	64	768	EBS only		20 Gigabit	0	0	\N	2018-12-01	65144
Yes	GPU instance	p2.8xlarge	32	488	EBS only		10 Gigabit	0	0	\N	2018-12-01	65145
Yes	GPU instance	p2.xlarge	4	61	EBS only		High	0	0	\N	2018-12-01	65146
Yes	GPU instance	p3.16xlarge	64	488	EBS only		25 Gigabit	0	0	\N	2018-12-01	65147
Yes	GPU instance	p3.2xlarge	8	61	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65148
Yes	GPU instance	p3.8xlarge	32	244	EBS only		10 Gigabit	0	0	\N	2018-12-01	65149
Yes	GPU instance	p3dn	96	768	2 x 900 NVMe SSD		100 Gigabit	0	0	\N	2018-12-01	65150
Yes	GPU instance	p3dn.24xlarge	96	768	2 x 900 NVMe SSD		100 Gigabit	0	0	\N	2018-12-01	65151
No	Memory optimized	r3.2xlarge	8	61	1 x 160 SSD		High	0	0	\N	2018-12-01	65152
No	Memory optimized	r3.4xlarge	16	122	1 x 320 SSD		High	0	0	\N	2018-12-01	65153
No	Memory optimized	r3.8xlarge	32	244	2 x 320 SSD		10 Gigabit	0	0	\N	2018-12-01	65154
No	Memory optimized	r3.large	2	15.25	1 x 32 SSD		Moderate	0	0	\N	2018-12-01	65155
No	Memory optimized	r3.xlarge	4	30.5	1 x 80 SSD		Moderate	0	0	\N	2018-12-01	65156
Yes	Memory optimized	r4.16xlarge	64	488	EBS only		20 Gigabit	0	0	\N	2018-12-01	65157
Yes	Memory optimized	r4.2xlarge	8	61	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65158
Yes	Memory optimized	r4.4xlarge	16	122	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65159
Yes	Memory optimized	r4.8xlarge	32	244	EBS only		10 Gigabit	0	0	\N	2018-12-01	65160
Yes	Memory optimized	r4.large	2	15.25	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65161
Yes	Memory optimized	r4.xlarge	4	30.5	EBS only		Up to 10 Gigabit	0	0	\N	2018-12-01	65162
Yes	Memory optimized	r5.12xlarge	48	384	EBS only		10 Gigabit	0	0	\N	2018-12-01	65163
Yes	Memory optimized	r5.24xlarge	96	768	EBS only		25 Gigabit	0	0	\N	2018-12-01	65164
Yes	Memory optimized	r5.2xlarge	8	64	EBS only		10 Gigabit	0	0	\N	2018-12-01	65165
Yes	Memory optimized	r5.4xlarge	16	128	EBS only		10 Gigabit	0	0	\N	2018-12-01	65166
Yes	Memory optimized	r5.large	2	16	EBS only		10 Gigabit	0	0	\N	2018-12-01	65167
Yes	Memory optimized	r5.xlarge	4	32	EBS only		10 Gigabit	0	0	\N	2018-12-01	65168
Yes	Memory optimized	r5a.12xlarge	48	384	EBS only		10 Gigabit	0	0	\N	2018-12-01	65169
Yes	Memory optimized	r5a.24xlarge	96	768	EBS only		20 Gigabit	0	0	\N	2018-12-01	65170
Yes	Memory optimized	r5a.2xlarge	8	64	EBS only		10 Gigabit	0	0	\N	2018-12-01	65171
Yes	Memory optimized	r5a.4xlarge	16	128	EBS only		10 Gigabit	0	0	\N	2018-12-01	65172
Yes	Memory optimized	r5a.large	2	16	EBS only		10 Gigabit	0	0	\N	2018-12-01	65173
Yes	Memory optimized	r5a.xlarge	4	32	EBS only		10 Gigabit	0	0	\N	2018-12-01	65174
Yes	Memory optimized	r5d.12xlarge	48	384	2 x 900 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65175
Yes	Memory optimized	r5d.24xlarge	96	768	4 x 900 NVMe SSD		25 Gigabit	0	0	\N	2018-12-01	65176
Yes	Memory optimized	r5d.2xlarge	8	64	1 x 300 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65177
Yes	Memory optimized	r5d.4xlarge	16	128	2 x 300 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65178
Yes	Memory optimized	r5d.large	2	16	1 x 75 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65179
Yes	Memory optimized	r5d.xlarge	4	32	1 x 150 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65180
No	Micro instances	t1.micro	1	0.613	EBS only		Very Low	0	0	\N	2018-12-01	65181
Yes	General purpose	t2.2xlarge	8	32	EBS only		Moderate	0	0	\N	2018-12-01	65182
Yes	General purpose	t2.large	2	8	EBS only		Low to Moderate	0	0	\N	2018-12-01	65183
Yes	General purpose	t2.medium	2	4	EBS only		Low to Moderate	0	0	\N	2018-12-01	65184
Yes	General purpose	t2.micro	1	1	EBS only		Low to Moderate	0	0	\N	2018-12-01	65185
Yes	General purpose	t2.nano	1	0.5	EBS only		Low	0	0	\N	2018-12-01	65186
Yes	General purpose	t2.small	1	2	EBS only		Low to Moderate	0	0	\N	2018-12-01	65187
Yes	General purpose	t2.xlarge	4	16	EBS only		Moderate	0	0	\N	2018-12-01	65188
Yes	General purpose	t3.2xlarge	8	32	EBS only		Moderate	0	0	\N	2018-12-01	65189
Yes	General purpose	t3.large	2	8	EBS only		Low to Moderate	0	0	\N	2018-12-01	65190
Yes	General purpose	t3.medium	2	4	EBS only		Low to Moderate	0	0	\N	2018-12-01	65191
Yes	General purpose	t3.micro	2	1	EBS only		Low to Moderate	0	0	\N	2018-12-01	65192
Yes	General purpose	t3.nano	2	0.5	EBS only		Low	0	0	\N	2018-12-01	65193
Yes	General purpose	t3.small	2	2	EBS only		Low to Moderate	0	0	\N	2018-12-01	65194
Yes	General purpose	t3.xlarge	4	16	EBS only		Moderate	0	0	\N	2018-12-01	65195
Yes	Memory optimized	u-12tb1	448	12288	EBS only		25 Gigabit	0	0	\N	2018-12-01	65196
Yes	Memory optimized	u-6tb1	448	6144	EBS only		25 Gigabit	0	0	\N	2018-12-01	65197
Yes	Memory optimized	u-9tb1	448	9216	EBS only		25 Gigabit	0	0	\N	2018-12-01	65198
Yes	Memory optimized	x1.16xlarge	64	976	1 x 1920 SSD		High	0	0	\N	2018-12-01	65199
Yes	Memory optimized	x1.32xlarge	128	1	2 x 1920 SSD		High	0	0	\N	2018-12-01	65200
Yes	Memory optimized	x1e.16xlarge	64	1	1 x 1920 SSD		10 Gigabit	0	0	\N	2018-12-01	65201
Yes	Memory optimized	x1e.2xlarge	8	244	1 x 240 SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65202
Yes	Memory optimized	x1e.32xlarge	128	3	2 x 1,920 SSD		25 Gigabit	0	0	\N	2018-12-01	65203
Yes	Memory optimized	x1e.4xlarge	16	488	1 x 480 SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65204
Yes	Memory optimized	x1e.8xlarge	32	976	1 x 960 SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65205
Yes	Memory optimized	x1e.xlarge	4	122	1 x 120 SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65206
Yes	Memory optimized	z1d.12xlarge	48	384	2 x 900 NVMe SSD		25 Gigabit	0	0	\N	2018-12-01	65207
Yes	Memory optimized	z1d.2xlarge	8	64	1 x 300 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65208
Yes	Memory optimized	z1d.3xlarge	12	96	1 x 450 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65209
Yes	Memory optimized	z1d.6xlarge	24	192	1 x 900 NVMe SSD		10 Gigabit	0	0	\N	2018-12-01	65210
Yes	Memory optimized	z1d.large	2	16	1 x 75 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65211
Yes	Memory optimized	z1d.xlarge	4	32	1 x 150 NVMe SSD		Up to 10 Gigabit	0	0	\N	2018-12-01	65212
\.


--
-- Data for Name: insttype_enable; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.insttype_enable (cid, name, id, region) FROM stdin;
\.


--
-- Name: insttype_enable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insttype_enable_id_seq', 68, true);


--
-- Name: insttype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insttype_id_seq', 65212, true);


--
-- Data for Name: insttype_pricing; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.insttype_pricing (id, cid, name, region, price) FROM stdin;
\N	0	c1.medium	ap-northeast-1	0.158
\N	0	c1.xlarge	ap-northeast-1	0.632
\N	0	c3.2xlarge	ap-northeast-1	3.512
\N	0	c3.4xlarge	ap-northeast-1	7.024
\N	0	c3.8xlarge	ap-northeast-1	15.548
\N	0	c3.large	ap-northeast-1	0.608
\N	0	c3.xlarge	ap-northeast-1	1.756
\N	0	c4.2xlarge	ap-northeast-1	3.504
\N	0	c4.4xlarge	ap-northeast-1	7.008
\N	0	c4.8xlarge	ap-northeast-1	15.516
\N	0	c4.large	ap-northeast-1	0.606
\N	0	c4.xlarge	ap-northeast-1	1.752
\N	0	c5.18xlarge	ap-northeast-1	30.852
\N	0	c5.2xlarge	ap-northeast-1	3.428
\N	0	c5.4xlarge	ap-northeast-1	6.856
\N	0	c5.9xlarge	ap-northeast-1	15.426
\N	0	c5.large	ap-northeast-1	0.587
\N	0	c5.xlarge	ap-northeast-1	1.714
\N	0	c5d.18xlarge	ap-northeast-1	31.392
\N	0	c5d.2xlarge	ap-northeast-1	3.488
\N	0	c5d.4xlarge	ap-northeast-1	6.976
\N	0	c5d.9xlarge	ap-northeast-1	15.696
\N	0	c5d.large	ap-northeast-1	0.602
\N	0	c5d.xlarge	ap-northeast-1	1.744
\N	0	cc2.8xlarge	ap-northeast-1	2.349
\N	0	cr1.8xlarge	ap-northeast-1	4.105
\N	0	d2.2xlarge	ap-northeast-1	4.688
\N	0	d2.4xlarge	ap-northeast-1	9.376
\N	0	d2.8xlarge	ap-northeast-1	20.252
\N	0	d2.xlarge	ap-northeast-1	2.344
\N	0	g2.2xlarge	ap-northeast-1	1.858
\N	0	g2.8xlarge	ap-northeast-1	15.592
\N	0	g3.16xlarge	ap-northeast-1	6.32
\N	0	g3.4xlarge	ap-northeast-1	1.58
\N	0	g3.8xlarge	ap-northeast-1	3.16
\N	0	g3s.xlarge	ap-northeast-1	1.04
\N	0	hs1.8xlarge	ap-northeast-1	5.4
\N	0	i2.2xlarge	ap-northeast-1	5.002
\N	0	i2.4xlarge	ap-northeast-1	10.004
\N	0	i2.8xlarge	ap-northeast-1	20.008
\N	0	i2.xlarge	ap-northeast-1	2.501
\N	0	i3.16xlarge	ap-northeast-1	29.856
\N	0	i3.2xlarge	ap-northeast-1	3.732
\N	0	i3.4xlarge	ap-northeast-1	7.464
\N	0	i3.8xlarge	ap-northeast-1	14.928
\N	0	i3.large	ap-northeast-1	0.663
\N	0	i3.xlarge	ap-northeast-1	1.866
\N	0	m1.large	ap-northeast-1	0.243
\N	0	m1.medium	ap-northeast-1	0.122
\N	0	m1.small	ap-northeast-1	0.061
\N	0	m1.xlarge	ap-northeast-1	0.486
\N	0	m2.2xlarge	ap-northeast-1	0.575
\N	0	m2.4xlarge	ap-northeast-1	1.15
\N	0	m2.xlarge	ap-northeast-1	0.287
\N	0	m3.2xlarge	ap-northeast-1	3.768
\N	0	m3.large	ap-northeast-1	0.672
\N	0	m3.medium	ap-northeast-1	0.576
\N	0	m3.xlarge	ap-northeast-1	1.884
\N	0	m4.10xlarge	ap-northeast-1	17.58
\N	0	m4.16xlarge	ap-northeast-1	28.128
\N	0	m4.2xlarge	ap-northeast-1	3.516
\N	0	m4.4xlarge	ap-northeast-1	7.032
\N	0	m4.large	ap-northeast-1	0.609
\N	0	m4.xlarge	ap-northeast-1	1.758
\N	0	m5.12xlarge	ap-northeast-1	20.976
\N	0	m5.24xlarge	ap-northeast-1	41.952
\N	0	m5.2xlarge	ap-northeast-1	3.496
\N	0	m5.4xlarge	ap-northeast-1	6.992
\N	0	m5.large	ap-northeast-1	0.604
\N	0	m5.xlarge	ap-northeast-1	1.748
\N	0	m5d.12xlarge	ap-northeast-1	21.504
\N	0	m5d.24xlarge	ap-northeast-1	43.008
\N	0	m5d.2xlarge	ap-northeast-1	3.584
\N	0	m5d.4xlarge	ap-northeast-1	7.168
\N	0	m5d.large	ap-northeast-1	0.626
\N	0	m5d.xlarge	ap-northeast-1	1.792
\N	0	p2.16xlarge	ap-northeast-1	24.672
\N	0	p2.8xlarge	ap-northeast-1	12.336
\N	0	p2.xlarge	ap-northeast-1	1.542
\N	0	p3.16xlarge	ap-northeast-1	33.552
\N	0	p3.2xlarge	ap-northeast-1	4.194
\N	0	p3.8xlarge	ap-northeast-1	16.776
\N	0	r3.2xlarge	ap-northeast-1	3.8
\N	0	r3.4xlarge	ap-northeast-1	7.6
\N	0	r3.8xlarge	ap-northeast-1	15.2
\N	0	r3.large	ap-northeast-1	0.68
\N	0	r3.xlarge	ap-northeast-1	1.9
\N	0	r4.16xlarge	ap-northeast-1	29.12
\N	0	r4.2xlarge	ap-northeast-1	3.64
\N	0	r4.4xlarge	ap-northeast-1	7.28
\N	0	r4.8xlarge	ap-northeast-1	14.56
\N	0	r4.large	ap-northeast-1	0.64
\N	0	r4.xlarge	ap-northeast-1	1.82
\N	0	r5.12xlarge	ap-northeast-1	21.648
\N	0	r5.24xlarge	ap-northeast-1	43.296
\N	0	r5.2xlarge	ap-northeast-1	3.608
\N	0	r5.4xlarge	ap-northeast-1	7.216
\N	0	r5.large	ap-northeast-1	0.632
\N	0	r5.xlarge	ap-northeast-1	1.804
\N	0	r5d.12xlarge	ap-northeast-1	22.176
\N	0	r5d.24xlarge	ap-northeast-1	44.352
\N	0	r5d.2xlarge	ap-northeast-1	3.696
\N	0	r5d.4xlarge	ap-northeast-1	7.392
\N	0	r5d.large	ap-northeast-1	0.654
\N	0	r5d.xlarge	ap-northeast-1	1.848
\N	0	t1.micro	ap-northeast-1	0.026
\N	0	t2.2xlarge	ap-northeast-1	3.4864
\N	0	t2.large	ap-northeast-1	0.1892
\N	0	t2.medium	ap-northeast-1	0.1284
\N	0	t2.micro	ap-northeast-1	0.0828
\N	0	t2.nano	ap-northeast-1	0.0076
\N	0	t2.small	ap-northeast-1	0.098
\N	0	t2.xlarge	ap-northeast-1	1.7432
\N	0	t3.2xlarge	ap-northeast-1	3.4352
\N	0	t3.large	ap-northeast-1	0.1764
\N	0	t3.medium	ap-northeast-1	0.122
\N	0	t3.micro	ap-northeast-1	0.0812
\N	0	t3.nano	ap-northeast-1	0.0068
\N	0	t3.small	ap-northeast-1	0.0948
\N	0	t3.xlarge	ap-northeast-1	1.7176
\N	0	x1.16xlarge	ap-northeast-1	33.671
\N	0	x1.32xlarge	ap-northeast-1	67.342
\N	0	x1e.16xlarge	ap-northeast-1	43.344
\N	0	x1e.2xlarge	ap-northeast-1	5.418
\N	0	x1e.32xlarge	ap-northeast-1	86.688
\N	0	x1e.4xlarge	ap-northeast-1	10.836
\N	0	x1e.8xlarge	ap-northeast-1	21.672
\N	0	x1e.xlarge	ap-northeast-1	2.709
\N	0	z1d.12xlarge	ap-northeast-1	23.448
\N	0	z1d.2xlarge	ap-northeast-1	3.908
\N	0	z1d.3xlarge	ap-northeast-1	5.862
\N	0	z1d.6xlarge	ap-northeast-1	11.724
\N	0	z1d.large	ap-northeast-1	0.707
\N	0	z1d.xlarge	ap-northeast-1	1.954
\N	0	c3.2xlarge	ap-northeast-2	0.46
\N	0	c3.4xlarge	ap-northeast-2	0.919
\N	0	c3.8xlarge	ap-northeast-2	1.839
\N	0	c3.large	ap-northeast-2	0.115
\N	0	c3.xlarge	ap-northeast-2	0.23
\N	0	c4.2xlarge	ap-northeast-2	3.456
\N	0	c4.4xlarge	ap-northeast-2	6.912
\N	0	c4.8xlarge	ap-northeast-2	15.324
\N	0	c4.large	ap-northeast-2	0.594
\N	0	c4.xlarge	ap-northeast-2	1.728
\N	0	c5.18xlarge	ap-northeast-2	30.456
\N	0	c5.2xlarge	ap-northeast-2	3.384
\N	0	c5.4xlarge	ap-northeast-2	6.768
\N	0	c5.9xlarge	ap-northeast-2	15.228
\N	0	c5.large	ap-northeast-2	0.576
\N	0	c5.xlarge	ap-northeast-2	1.692
\N	0	c5d.18xlarge	ap-northeast-2	30.96
\N	0	c5d.2xlarge	ap-northeast-2	3.44
\N	0	c5d.4xlarge	ap-northeast-2	6.88
\N	0	c5d.9xlarge	ap-northeast-2	15.48
\N	0	c5d.large	ap-northeast-2	0.59
\N	0	c5d.xlarge	ap-northeast-2	1.72
\N	0	d2.2xlarge	ap-northeast-2	4.688
\N	0	d2.4xlarge	ap-northeast-2	9.376
\N	0	d2.8xlarge	ap-northeast-2	20.252
\N	0	d2.xlarge	ap-northeast-2	2.344
\N	0	g2.2xlarge	ap-northeast-2	0.898
\N	0	g2.8xlarge	ap-northeast-2	3.592
\N	0	i2.2xlarge	ap-northeast-2	5.002
\N	0	i2.4xlarge	ap-northeast-2	10.004
\N	0	i2.8xlarge	ap-northeast-2	20.008
\N	0	i2.xlarge	ap-northeast-2	2.501
\N	0	i3.16xlarge	ap-northeast-2	29.856
\N	0	i3.2xlarge	ap-northeast-2	3.732
\N	0	i3.4xlarge	ap-northeast-2	7.464
\N	0	i3.8xlarge	ap-northeast-2	14.928
\N	0	i3.large	ap-northeast-2	0.663
\N	0	i3.xlarge	ap-northeast-2	1.866
\N	0	m3.2xlarge	ap-northeast-2	0.732
\N	0	m3.large	ap-northeast-2	0.183
\N	0	m3.medium	ap-northeast-2	0.091
\N	0	m3.xlarge	ap-northeast-2	0.366
\N	0	m4.10xlarge	ap-northeast-2	17.46
\N	0	m4.16xlarge	ap-northeast-2	27.936
\N	0	m4.2xlarge	ap-northeast-2	3.492
\N	0	m4.4xlarge	ap-northeast-2	6.984
\N	0	m4.large	ap-northeast-2	0.603
\N	0	m4.xlarge	ap-northeast-2	1.746
\N	0	m5.12xlarge	ap-northeast-2	20.832
\N	0	m5.24xlarge	ap-northeast-2	41.664
\N	0	m5.2xlarge	ap-northeast-2	3.472
\N	0	m5.4xlarge	ap-northeast-2	6.944
\N	0	m5.large	ap-northeast-2	0.598
\N	0	m5.xlarge	ap-northeast-2	1.736
\N	0	m5d.12xlarge	ap-northeast-2	21.336
\N	0	m5d.24xlarge	ap-northeast-2	42.672
\N	0	m5d.2xlarge	ap-northeast-2	3.556
\N	0	m5d.4xlarge	ap-northeast-2	7.112
\N	0	m5d.large	ap-northeast-2	0.619
\N	0	m5d.xlarge	ap-northeast-2	1.778
\N	0	p2.16xlarge	ap-northeast-2	23.44
\N	0	p2.8xlarge	ap-northeast-2	11.72
\N	0	p2.xlarge	ap-northeast-2	1.465
\N	0	p3.16xlarge	ap-northeast-2	33.872
\N	0	p3.2xlarge	ap-northeast-2	4.234
\N	0	p3.8xlarge	ap-northeast-2	16.936
\N	0	r3.2xlarge	ap-northeast-2	3.8
\N	0	r3.4xlarge	ap-northeast-2	7.6
\N	0	r3.8xlarge	ap-northeast-2	15.2
\N	0	r3.large	ap-northeast-2	0.68
\N	0	r3.xlarge	ap-northeast-2	1.9
\N	0	r4.16xlarge	ap-northeast-2	29.12
\N	0	r4.2xlarge	ap-northeast-2	3.64
\N	0	r4.4xlarge	ap-northeast-2	7.28
\N	0	r4.8xlarge	ap-northeast-2	14.56
\N	0	r4.large	ap-northeast-2	0.64
\N	0	r4.xlarge	ap-northeast-2	1.82
\N	0	r5.12xlarge	ap-northeast-2	21.648
\N	0	r5.24xlarge	ap-northeast-2	43.296
\N	0	r5.2xlarge	ap-northeast-2	3.608
\N	0	r5.4xlarge	ap-northeast-2	7.216
\N	0	r5.large	ap-northeast-2	0.632
\N	0	r5.xlarge	ap-northeast-2	1.804
\N	0	r5d.12xlarge	ap-northeast-2	22.152
\N	0	r5d.24xlarge	ap-northeast-2	44.304
\N	0	r5d.2xlarge	ap-northeast-2	3.692
\N	0	r5d.4xlarge	ap-northeast-2	7.384
\N	0	r5d.large	ap-northeast-2	0.653
\N	0	r5d.xlarge	ap-northeast-2	1.846
\N	0	t2.2xlarge	ap-northeast-2	3.4608
\N	0	t2.large	ap-northeast-2	0.1828
\N	0	t2.medium	ap-northeast-2	0.1252
\N	0	t2.micro	ap-northeast-2	0.082
\N	0	t2.nano	ap-northeast-2	0.0072
\N	0	t2.small	ap-northeast-2	0.0964
\N	0	t2.xlarge	ap-northeast-2	1.7304
\N	0	t3.2xlarge	ap-northeast-2	3.416
\N	0	t3.large	ap-northeast-2	0.1716
\N	0	t3.medium	ap-northeast-2	0.1196
\N	0	t3.micro	ap-northeast-2	0.0806
\N	0	t3.nano	ap-northeast-2	0.0065
\N	0	t3.small	ap-northeast-2	0.0936
\N	0	t3.xlarge	ap-northeast-2	1.708
\N	0	x1.16xlarge	ap-northeast-2	33.671
\N	0	x1.32xlarge	ap-northeast-2	67.342
\N	0	c3.2xlarge	ap-northeast-3	3.512
\N	0	c3.4xlarge	ap-northeast-3	7.024
\N	0	c3.8xlarge	ap-northeast-3	15.548
\N	0	c3.large	ap-northeast-3	0.608
\N	0	c3.xlarge	ap-northeast-3	1.756
\N	0	c4.2xlarge	ap-northeast-3	3.504
\N	0	c4.4xlarge	ap-northeast-3	7.008
\N	0	c4.8xlarge	ap-northeast-3	15.516
\N	0	c4.large	ap-northeast-3	0.606
\N	0	c4.xlarge	ap-northeast-3	1.752
\N	0	d2.2xlarge	ap-northeast-3	4.688
\N	0	d2.4xlarge	ap-northeast-3	9.376
\N	0	d2.8xlarge	ap-northeast-3	20.252
\N	0	d2.xlarge	ap-northeast-3	2.344
\N	0	i3.16xlarge	ap-northeast-3	29.856
\N	0	i3.2xlarge	ap-northeast-3	3.732
\N	0	i3.4xlarge	ap-northeast-3	7.464
\N	0	i3.8xlarge	ap-northeast-3	14.928
\N	0	i3.large	ap-northeast-3	0.663
\N	0	i3.xlarge	ap-northeast-3	1.866
\N	0	m3.2xlarge	ap-northeast-3	3.768
\N	0	m3.large	ap-northeast-3	0.672
\N	0	m3.medium	ap-northeast-3	0.576
\N	0	m3.xlarge	ap-northeast-3	1.884
\N	0	m4.10xlarge	ap-northeast-3	17.58
\N	0	m4.16xlarge	ap-northeast-3	28.128
\N	0	m4.2xlarge	ap-northeast-3	3.516
\N	0	m4.4xlarge	ap-northeast-3	7.032
\N	0	m4.large	ap-northeast-3	0.609
\N	0	m4.xlarge	ap-northeast-3	1.758
\N	0	r3.2xlarge	ap-northeast-3	3.8
\N	0	r3.4xlarge	ap-northeast-3	7.6
\N	0	r3.8xlarge	ap-northeast-3	15.2
\N	0	r3.large	ap-northeast-3	0.68
\N	0	r3.xlarge	ap-northeast-3	1.9
\N	0	r4.16xlarge	ap-northeast-3	29.12
\N	0	r4.2xlarge	ap-northeast-3	3.64
\N	0	r4.4xlarge	ap-northeast-3	7.28
\N	0	r4.8xlarge	ap-northeast-3	14.56
\N	0	r4.large	ap-northeast-3	0.64
\N	0	r4.xlarge	ap-northeast-3	1.82
\N	0	t2.2xlarge	ap-northeast-3	3.4864
\N	0	t2.large	ap-northeast-3	0.1892
\N	0	t2.medium	ap-northeast-3	0.1284
\N	0	t2.micro	ap-northeast-3	0.0828
\N	0	t2.nano	ap-northeast-3	0.0076
\N	0	t2.small	ap-northeast-3	0.098
\N	0	t2.xlarge	ap-northeast-3	1.7432
\N	0	c4.2xlarge	ap-south-1	3.4
\N	0	c4.4xlarge	ap-south-1	6.8
\N	0	c4.8xlarge	ap-south-1	15.1
\N	0	c4.large	ap-south-1	0.58
\N	0	c4.xlarge	ap-south-1	1.7
\N	0	c5.18xlarge	ap-south-1	30.06
\N	0	c5.2xlarge	ap-south-1	3.34
\N	0	c5.4xlarge	ap-south-1	6.68
\N	0	c5.9xlarge	ap-south-1	15.03
\N	0	c5.large	ap-south-1	0.565
\N	0	c5.xlarge	ap-south-1	1.67
\N	0	d2.2xlarge	ap-south-1	4.654
\N	0	d2.4xlarge	ap-south-1	9.308
\N	0	d2.8xlarge	ap-south-1	20.116
\N	0	d2.xlarge	ap-south-1	2.327
\N	0	i2.2xlarge	ap-south-1	4.934
\N	0	i2.4xlarge	ap-south-1	9.868
\N	0	i2.8xlarge	ap-south-1	19.736
\N	0	i2.xlarge	ap-south-1	2.467
\N	0	i3.16xlarge	ap-south-1	29.664
\N	0	i3.2xlarge	ap-south-1	3.708
\N	0	i3.4xlarge	ap-south-1	7.416
\N	0	i3.8xlarge	ap-south-1	14.832
\N	0	i3.large	ap-south-1	0.657
\N	0	i3.xlarge	ap-south-1	1.854
\N	0	m4.10xlarge	ap-south-1	17.1
\N	0	m4.16xlarge	ap-south-1	27.36
\N	0	m4.2xlarge	ap-south-1	3.42
\N	0	m4.4xlarge	ap-south-1	6.84
\N	0	m4.large	ap-south-1	0.585
\N	0	m4.xlarge	ap-south-1	1.71
\N	0	m5.12xlarge	ap-south-1	20.424
\N	0	m5.24xlarge	ap-south-1	40.848
\N	0	m5.2xlarge	ap-south-1	3.404
\N	0	m5.4xlarge	ap-south-1	6.808
\N	0	m5.large	ap-south-1	0.581
\N	0	m5.xlarge	ap-south-1	1.702
\N	0	p2.16xlarge	ap-south-1	27.488
\N	0	p2.8xlarge	ap-south-1	13.744
\N	0	p2.xlarge	ap-south-1	1.718
\N	0	r3.2xlarge	ap-south-1	3.76
\N	0	r3.4xlarge	ap-south-1	7.52
\N	0	r3.8xlarge	ap-south-1	15.04
\N	0	r3.large	ap-south-1	0.67
\N	0	r3.xlarge	ap-south-1	1.88
\N	0	r4.16xlarge	ap-south-1	28.384
\N	0	r4.2xlarge	ap-south-1	3.548
\N	0	r4.4xlarge	ap-south-1	7.096
\N	0	r4.8xlarge	ap-south-1	14.192
\N	0	r4.large	ap-south-1	0.617
\N	0	r4.xlarge	ap-south-1	1.774
\N	0	t2.2xlarge	ap-south-1	3.3968
\N	0	t2.large	ap-south-1	0.1668
\N	0	t2.medium	ap-south-1	0.1172
\N	0	t2.micro	ap-south-1	0.08
\N	0	t2.nano	ap-south-1	0.0062
\N	0	t2.small	ap-south-1	0.0924
\N	0	t2.xlarge	ap-south-1	1.6984
\N	0	x1.16xlarge	ap-south-1	33.187
\N	0	x1.32xlarge	ap-south-1	66.374
\N	0	c1.medium	ap-southeast-1	0.164
\N	0	c1.xlarge	ap-southeast-1	0.655
\N	0	c3.2xlarge	ap-southeast-1	3.528
\N	0	c3.4xlarge	ap-southeast-1	7.056
\N	0	c3.8xlarge	ap-southeast-1	15.612
\N	0	c3.large	ap-southeast-1	0.612
\N	0	c3.xlarge	ap-southeast-1	1.764
\N	0	c4.2xlarge	ap-southeast-1	3.46
\N	0	c4.4xlarge	ap-southeast-1	6.92
\N	0	c4.8xlarge	ap-southeast-1	15.34
\N	0	c4.large	ap-southeast-1	0.595
\N	0	c4.xlarge	ap-southeast-1	1.73
\N	0	c5.18xlarge	ap-southeast-1	30.528
\N	0	c5.2xlarge	ap-southeast-1	3.392
\N	0	c5.4xlarge	ap-southeast-1	6.784
\N	0	c5.9xlarge	ap-southeast-1	15.264
\N	0	c5.large	ap-southeast-1	0.578
\N	0	c5.xlarge	ap-southeast-1	1.696
\N	0	c5d.18xlarge	ap-southeast-1	31.032
\N	0	c5d.2xlarge	ap-southeast-1	3.448
\N	0	c5d.4xlarge	ap-southeast-1	6.896
\N	0	c5d.9xlarge	ap-southeast-1	15.516
\N	0	c5d.large	ap-southeast-1	0.592
\N	0	c5d.xlarge	ap-southeast-1	1.724
\N	0	d2.2xlarge	ap-southeast-1	4.74
\N	0	d2.4xlarge	ap-southeast-1	9.48
\N	0	d2.8xlarge	ap-southeast-1	20.46
\N	0	d2.xlarge	ap-southeast-1	2.37
\N	0	g2.2xlarge	ap-southeast-1	1.96
\N	0	g2.8xlarge	ap-southeast-1	16
\N	0	g3.16xlarge	ap-southeast-1	6.68
\N	0	g3.4xlarge	ap-southeast-1	1.67
\N	0	g3.8xlarge	ap-southeast-1	3.34
\N	0	hs1.8xlarge	ap-southeast-1	5.57
\N	0	i2.2xlarge	ap-southeast-1	5.036
\N	0	i2.4xlarge	ap-southeast-1	10.072
\N	0	i2.8xlarge	ap-southeast-1	20.144
\N	0	i2.xlarge	ap-southeast-1	2.518
\N	0	i3.16xlarge	ap-southeast-1	29.984
\N	0	i3.2xlarge	ap-southeast-1	3.748
\N	0	i3.4xlarge	ap-southeast-1	7.496
\N	0	i3.8xlarge	ap-southeast-1	14.992
\N	0	i3.large	ap-southeast-1	0.667
\N	0	i3.xlarge	ap-southeast-1	1.874
\N	0	m1.large	ap-southeast-1	0.233
\N	0	m1.medium	ap-southeast-1	0.117
\N	0	m1.small	ap-southeast-1	0.058
\N	0	m1.xlarge	ap-southeast-1	0.467
\N	0	m2.2xlarge	ap-southeast-1	0.592
\N	0	m2.4xlarge	ap-southeast-1	1.183
\N	0	m2.xlarge	ap-southeast-1	0.296
\N	0	m3.2xlarge	ap-southeast-1	3.784
\N	0	m3.large	ap-southeast-1	0.676
\N	0	m3.medium	ap-southeast-1	0.578
\N	0	m3.xlarge	ap-southeast-1	1.892
\N	0	m4.10xlarge	ap-southeast-1	17.5
\N	0	m4.16xlarge	ap-southeast-1	28
\N	0	m4.2xlarge	ap-southeast-1	3.5
\N	0	m4.4xlarge	ap-southeast-1	7
\N	0	m4.large	ap-southeast-1	0.605
\N	0	m4.xlarge	ap-southeast-1	1.75
\N	0	m5.12xlarge	ap-southeast-1	20.88
\N	0	m5.24xlarge	ap-southeast-1	41.76
\N	0	m5.2xlarge	ap-southeast-1	3.48
\N	0	m5.4xlarge	ap-southeast-1	6.96
\N	0	m5.large	ap-southeast-1	0.6
\N	0	m5.xlarge	ap-southeast-1	1.74
\N	0	m5a.12xlarge	ap-southeast-1	20.592
\N	0	m5a.24xlarge	ap-southeast-1	41.184
\N	0	m5a.2xlarge	ap-southeast-1	3.432
\N	0	m5a.4xlarge	ap-southeast-1	6.864
\N	0	m5a.large	ap-southeast-1	0.588
\N	0	m5a.xlarge	ap-southeast-1	1.716
\N	0	m5d.12xlarge	ap-southeast-1	21.384
\N	0	m5d.24xlarge	ap-southeast-1	42.768
\N	0	m5d.2xlarge	ap-southeast-1	3.564
\N	0	m5d.4xlarge	ap-southeast-1	7.128
\N	0	m5d.large	ap-southeast-1	0.621
\N	0	m5d.xlarge	ap-southeast-1	1.782
\N	0	p2.16xlarge	ap-southeast-1	27.488
\N	0	p2.8xlarge	ap-southeast-1	13.744
\N	0	p2.xlarge	ap-southeast-1	1.718
\N	0	p3.16xlarge	ap-southeast-1	33.872
\N	0	p3.2xlarge	ap-southeast-1	4.234
\N	0	p3.8xlarge	ap-southeast-1	16.936
\N	0	r3.2xlarge	ap-southeast-1	3.8
\N	0	r3.4xlarge	ap-southeast-1	7.6
\N	0	r3.8xlarge	ap-southeast-1	15.2
\N	0	r3.large	ap-southeast-1	0.68
\N	0	r3.xlarge	ap-southeast-1	1.9
\N	0	r4.16xlarge	ap-southeast-1	29.12
\N	0	r4.2xlarge	ap-southeast-1	3.64
\N	0	r4.4xlarge	ap-southeast-1	7.28
\N	0	r4.8xlarge	ap-southeast-1	14.56
\N	0	r4.large	ap-southeast-1	0.64
\N	0	r4.xlarge	ap-southeast-1	1.82
\N	0	r5.12xlarge	ap-southeast-1	21.648
\N	0	r5.24xlarge	ap-southeast-1	43.296
\N	0	r5.2xlarge	ap-southeast-1	3.608
\N	0	r5.4xlarge	ap-southeast-1	7.216
\N	0	r5.large	ap-southeast-1	0.632
\N	0	r5.xlarge	ap-southeast-1	1.804
\N	0	r5a.12xlarge	ap-southeast-1	21.264
\N	0	r5a.24xlarge	ap-southeast-1	42.528
\N	0	r5a.2xlarge	ap-southeast-1	3.544
\N	0	r5a.4xlarge	ap-southeast-1	7.088
\N	0	r5a.large	ap-southeast-1	0.616
\N	0	r5a.xlarge	ap-southeast-1	1.772
\N	0	r5d.12xlarge	ap-southeast-1	22.176
\N	0	r5d.24xlarge	ap-southeast-1	44.352
\N	0	r5d.2xlarge	ap-southeast-1	3.696
\N	0	r5d.4xlarge	ap-southeast-1	7.392
\N	0	r5d.large	ap-southeast-1	0.654
\N	0	r5d.xlarge	ap-southeast-1	1.848
\N	0	t1.micro	ap-southeast-1	0.02
\N	0	t2.2xlarge	ap-southeast-1	3.4672
\N	0	t2.large	ap-southeast-1	0.1844
\N	0	t2.medium	ap-southeast-1	0.126
\N	0	t2.micro	ap-southeast-1	0.0822
\N	0	t2.nano	ap-southeast-1	0.0073
\N	0	t2.small	ap-southeast-1	0.0968
\N	0	t2.xlarge	ap-southeast-1	1.7336
\N	0	t3.2xlarge	ap-southeast-1	3.4224
\N	0	t3.large	ap-southeast-1	0.1732
\N	0	t3.medium	ap-southeast-1	0.1204
\N	0	t3.micro	ap-southeast-1	0.0808
\N	0	t3.nano	ap-southeast-1	0.0066
\N	0	t3.small	ap-southeast-1	0.094
\N	0	t3.xlarge	ap-southeast-1	1.7112
\N	0	x1.16xlarge	ap-southeast-1	33.671
\N	0	x1.32xlarge	ap-southeast-1	67.342
\N	0	z1d.12xlarge	ap-southeast-1	23.424
\N	0	z1d.2xlarge	ap-southeast-1	3.904
\N	0	z1d.3xlarge	ap-southeast-1	5.856
\N	0	z1d.6xlarge	ap-southeast-1	11.712
\N	0	z1d.large	ap-southeast-1	0.706
\N	0	z1d.xlarge	ap-southeast-1	1.952
\N	0	c1.medium	ap-southeast-2	0.164
\N	0	c1.xlarge	ap-southeast-2	0.655
\N	0	c3.2xlarge	ap-southeast-2	3.528
\N	0	c3.4xlarge	ap-southeast-2	7.056
\N	0	c3.8xlarge	ap-southeast-2	15.612
\N	0	c3.large	ap-southeast-2	0.612
\N	0	c3.xlarge	ap-southeast-2	1.764
\N	0	c4.2xlarge	ap-southeast-2	3.52
\N	0	c4.4xlarge	ap-southeast-2	7.04
\N	0	c4.8xlarge	ap-southeast-2	15.58
\N	0	c4.large	ap-southeast-2	0.61
\N	0	c4.xlarge	ap-southeast-2	1.76
\N	0	c5.18xlarge	ap-southeast-2	30.996
\N	0	c5.2xlarge	ap-southeast-2	3.444
\N	0	c5.4xlarge	ap-southeast-2	6.888
\N	0	c5.9xlarge	ap-southeast-2	15.498
\N	0	c5.large	ap-southeast-2	0.591
\N	0	c5.xlarge	ap-southeast-2	1.722
\N	0	c5d.18xlarge	ap-southeast-2	31.536
\N	0	c5d.2xlarge	ap-southeast-2	3.504
\N	0	c5d.4xlarge	ap-southeast-2	7.008
\N	0	c5d.9xlarge	ap-southeast-2	15.768
\N	0	c5d.large	ap-southeast-2	0.606
\N	0	c5d.xlarge	ap-southeast-2	1.752
\N	0	d2.2xlarge	ap-southeast-2	4.74
\N	0	d2.4xlarge	ap-southeast-2	9.48
\N	0	d2.8xlarge	ap-southeast-2	20.46
\N	0	d2.xlarge	ap-southeast-2	2.37
\N	0	g2.2xlarge	ap-southeast-2	1.858
\N	0	g2.8xlarge	ap-southeast-2	15.592
\N	0	g3.16xlarge	ap-southeast-2	7.016
\N	0	g3.4xlarge	ap-southeast-2	1.754
\N	0	g3.8xlarge	ap-southeast-2	3.508
\N	0	g3s.xlarge	ap-southeast-2	1.154
\N	0	hs1.8xlarge	ap-southeast-2	5.57
\N	0	i2.2xlarge	ap-southeast-2	5.036
\N	0	i2.4xlarge	ap-southeast-2	10.072
\N	0	i2.8xlarge	ap-southeast-2	20.144
\N	0	i2.xlarge	ap-southeast-2	2.518
\N	0	i3.16xlarge	ap-southeast-2	29.984
\N	0	i3.2xlarge	ap-southeast-2	3.748
\N	0	i3.4xlarge	ap-southeast-2	7.496
\N	0	i3.8xlarge	ap-southeast-2	14.992
\N	0	i3.large	ap-southeast-2	0.667
\N	0	i3.xlarge	ap-southeast-2	1.874
\N	0	m1.large	ap-southeast-2	0.233
\N	0	m1.medium	ap-southeast-2	0.117
\N	0	m1.small	ap-southeast-2	0.058
\N	0	m1.xlarge	ap-southeast-2	0.467
\N	0	m2.2xlarge	ap-southeast-2	0.592
\N	0	m2.4xlarge	ap-southeast-2	1.183
\N	0	m2.xlarge	ap-southeast-2	0.296
\N	0	m3.2xlarge	ap-southeast-2	3.744
\N	0	m3.large	ap-southeast-2	0.666
\N	0	m3.medium	ap-southeast-2	0.573
\N	0	m3.xlarge	ap-southeast-2	1.872
\N	0	m4.10xlarge	ap-southeast-2	17.5
\N	0	m4.16xlarge	ap-southeast-2	28
\N	0	m4.2xlarge	ap-southeast-2	3.5
\N	0	m4.4xlarge	ap-southeast-2	7
\N	0	m4.large	ap-southeast-2	0.605
\N	0	m4.xlarge	ap-southeast-2	1.75
\N	0	m5.12xlarge	ap-southeast-2	20.88
\N	0	m5.24xlarge	ap-southeast-2	41.76
\N	0	m5.2xlarge	ap-southeast-2	3.48
\N	0	m5.4xlarge	ap-southeast-2	6.96
\N	0	m5.large	ap-southeast-2	0.6
\N	0	m5.xlarge	ap-southeast-2	1.74
\N	0	m5d.12xlarge	ap-southeast-2	21.408
\N	0	m5d.24xlarge	ap-southeast-2	42.816
\N	0	m5d.2xlarge	ap-southeast-2	3.568
\N	0	m5d.4xlarge	ap-southeast-2	7.136
\N	0	m5d.large	ap-southeast-2	0.622
\N	0	m5d.xlarge	ap-southeast-2	1.784
\N	0	p2.16xlarge	ap-southeast-2	24.672
\N	0	p2.8xlarge	ap-southeast-2	12.336
\N	0	p2.xlarge	ap-southeast-2	1.542
\N	0	p3.16xlarge	ap-southeast-2	33.872
\N	0	p3.2xlarge	ap-southeast-2	4.234
\N	0	p3.8xlarge	ap-southeast-2	16.936
\N	0	r3.2xlarge	ap-southeast-2	3.8
\N	0	r3.4xlarge	ap-southeast-2	7.6
\N	0	r3.8xlarge	ap-southeast-2	15.2
\N	0	r3.large	ap-southeast-2	0.68
\N	0	r3.xlarge	ap-southeast-2	1.9
\N	0	r4.16xlarge	ap-southeast-2	29.12
\N	0	r4.2xlarge	ap-southeast-2	3.64
\N	0	r4.4xlarge	ap-southeast-2	7.28
\N	0	r4.8xlarge	ap-southeast-2	14.56
\N	0	r4.large	ap-southeast-2	0.64
\N	0	r4.xlarge	ap-southeast-2	1.82
\N	0	r5.12xlarge	ap-southeast-2	21.624
\N	0	r5.24xlarge	ap-southeast-2	43.248
\N	0	r5.2xlarge	ap-southeast-2	3.604
\N	0	r5.4xlarge	ap-southeast-2	7.208
\N	0	r5.large	ap-southeast-2	0.631
\N	0	r5.xlarge	ap-southeast-2	1.802
\N	0	r5d.12xlarge	ap-southeast-2	22.176
\N	0	r5d.24xlarge	ap-southeast-2	44.352
\N	0	r5d.2xlarge	ap-southeast-2	3.696
\N	0	r5d.4xlarge	ap-southeast-2	7.392
\N	0	r5d.large	ap-southeast-2	0.654
\N	0	r5d.xlarge	ap-southeast-2	1.848
\N	0	t1.micro	ap-southeast-2	0.02
\N	0	t2.2xlarge	ap-southeast-2	3.4672
\N	0	t2.large	ap-southeast-2	0.1844
\N	0	t2.medium	ap-southeast-2	0.126
\N	0	t2.micro	ap-southeast-2	0.0822
\N	0	t2.nano	ap-southeast-2	0.0073
\N	0	t2.small	ap-southeast-2	0.0968
\N	0	t2.xlarge	ap-southeast-2	1.7336
\N	0	t3.2xlarge	ap-southeast-2	3.4224
\N	0	t3.large	ap-southeast-2	0.1732
\N	0	t3.medium	ap-southeast-2	0.1204
\N	0	t3.micro	ap-southeast-2	0.0808
\N	0	t3.nano	ap-southeast-2	0.0066
\N	0	t3.small	ap-southeast-2	0.094
\N	0	t3.xlarge	ap-southeast-2	1.7112
\N	0	x1.16xlarge	ap-southeast-2	33.671
\N	0	x1.32xlarge	ap-southeast-2	67.342
\N	0	x1e.16xlarge	ap-southeast-2	43.344
\N	0	x1e.2xlarge	ap-southeast-2	5.418
\N	0	x1e.32xlarge	ap-southeast-2	86.688
\N	0	x1e.4xlarge	ap-southeast-2	10.836
\N	0	x1e.8xlarge	ap-southeast-2	21.672
\N	0	x1e.xlarge	ap-southeast-2	2.709
\N	0	c4.2xlarge	ca-central-1	3.44
\N	0	c4.4xlarge	ca-central-1	6.88
\N	0	c4.8xlarge	ca-central-1	15.26
\N	0	c4.large	ca-central-1	0.59
\N	0	c4.xlarge	ca-central-1	1.72
\N	0	c5.18xlarge	ca-central-1	30.348
\N	0	c5.2xlarge	ca-central-1	3.372
\N	0	c5.4xlarge	ca-central-1	6.744
\N	0	c5.9xlarge	ca-central-1	15.174
\N	0	c5.large	ca-central-1	0.573
\N	0	c5.xlarge	ca-central-1	1.686
\N	0	c5d.18xlarge	ca-central-1	30.816
\N	0	c5d.2xlarge	ca-central-1	3.424
\N	0	c5d.4xlarge	ca-central-1	6.848
\N	0	c5d.9xlarge	ca-central-1	15.408
\N	0	c5d.large	ca-central-1	0.586
\N	0	c5d.xlarge	ca-central-1	1.712
\N	0	d2.2xlarge	ca-central-1	4.518
\N	0	d2.4xlarge	ca-central-1	9.036
\N	0	d2.8xlarge	ca-central-1	19.572
\N	0	d2.xlarge	ca-central-1	2.259
\N	0	g3.16xlarge	ca-central-1	5.664
\N	0	g3.4xlarge	ca-central-1	1.416
\N	0	g3.8xlarge	ca-central-1	2.832
\N	0	i3.16xlarge	ca-central-1	29.504
\N	0	i3.2xlarge	ca-central-1	3.688
\N	0	i3.4xlarge	ca-central-1	7.376
\N	0	i3.8xlarge	ca-central-1	14.752
\N	0	i3.large	ca-central-1	0.652
\N	0	i3.xlarge	ca-central-1	1.844
\N	0	m4.10xlarge	ca-central-1	17.22
\N	0	m4.16xlarge	ca-central-1	27.552
\N	0	m4.2xlarge	ca-central-1	3.444
\N	0	m4.4xlarge	ca-central-1	6.888
\N	0	m4.large	ca-central-1	0.591
\N	0	m4.xlarge	ca-central-1	1.722
\N	0	m5.12xlarge	ca-central-1	20.568
\N	0	m5.24xlarge	ca-central-1	41.136
\N	0	m5.2xlarge	ca-central-1	3.428
\N	0	m5.4xlarge	ca-central-1	6.856
\N	0	m5.large	ca-central-1	0.587
\N	0	m5.xlarge	ca-central-1	1.714
\N	0	m5d.12xlarge	ca-central-1	21.024
\N	0	m5d.24xlarge	ca-central-1	42.048
\N	0	m5d.2xlarge	ca-central-1	3.504
\N	0	m5d.4xlarge	ca-central-1	7.008
\N	0	m5d.large	ca-central-1	0.606
\N	0	m5d.xlarge	ca-central-1	1.752
\N	0	p3.16xlarge	ca-central-1	26.928
\N	0	p3.2xlarge	ca-central-1	3.366
\N	0	p3.8xlarge	ca-central-1	13.464
\N	0	r4.16xlarge	ca-central-1	28.672
\N	0	r4.2xlarge	ca-central-1	3.584
\N	0	r4.4xlarge	ca-central-1	7.168
\N	0	r4.8xlarge	ca-central-1	14.336
\N	0	r4.large	ca-central-1	0.626
\N	0	r4.xlarge	ca-central-1	1.792
\N	0	r5.12xlarge	ca-central-1	21.312
\N	0	r5.24xlarge	ca-central-1	42.624
\N	0	r5.2xlarge	ca-central-1	3.552
\N	0	r5.4xlarge	ca-central-1	7.104
\N	0	r5.large	ca-central-1	0.618
\N	0	r5.xlarge	ca-central-1	1.776
\N	0	r5d.12xlarge	ca-central-1	21.792
\N	0	r5d.24xlarge	ca-central-1	43.584
\N	0	r5d.2xlarge	ca-central-1	3.632
\N	0	r5d.4xlarge	ca-central-1	7.264
\N	0	r5d.large	ca-central-1	0.638
\N	0	r5d.xlarge	ca-central-1	1.816
\N	0	t2.2xlarge	ca-central-1	3.4096
\N	0	t2.large	ca-central-1	0.17
\N	0	t2.medium	ca-central-1	0.1188
\N	0	t2.micro	ca-central-1	0.0804
\N	0	t2.nano	ca-central-1	0.0064
\N	0	t2.small	ca-central-1	0.0932
\N	0	t2.xlarge	ca-central-1	1.7048
\N	0	t3.2xlarge	ca-central-1	3.3712
\N	0	t3.large	ca-central-1	0.1604
\N	0	t3.medium	ca-central-1	0.114
\N	0	t3.micro	ca-central-1	0.0792
\N	0	t3.nano	ca-central-1	0.0058
\N	0	t3.small	ca-central-1	0.0908
\N	0	t3.xlarge	ca-central-1	1.6856
\N	0	x1.16xlarge	ca-central-1	31.336
\N	0	x1.32xlarge	ca-central-1	62.672
\N	0	c3.2xlarge	eu-central-1	3.516
\N	0	c3.4xlarge	eu-central-1	7.032
\N	0	c3.8xlarge	eu-central-1	15.564
\N	0	c3.large	eu-central-1	0.609
\N	0	c3.xlarge	eu-central-1	1.758
\N	0	c4.2xlarge	eu-central-1	3.456
\N	0	c4.4xlarge	eu-central-1	6.912
\N	0	c4.8xlarge	eu-central-1	15.324
\N	0	c4.large	eu-central-1	0.594
\N	0	c4.xlarge	eu-central-1	1.728
\N	0	c5.18xlarge	eu-central-1	30.492
\N	0	c5.2xlarge	eu-central-1	3.388
\N	0	c5.4xlarge	eu-central-1	6.776
\N	0	c5.9xlarge	eu-central-1	15.246
\N	0	c5.large	eu-central-1	0.577
\N	0	c5.xlarge	eu-central-1	1.694
\N	0	c5d.18xlarge	eu-central-1	30.996
\N	0	c5d.2xlarge	eu-central-1	3.444
\N	0	c5d.4xlarge	eu-central-1	6.888
\N	0	c5d.9xlarge	eu-central-1	15.498
\N	0	c5d.large	eu-central-1	0.591
\N	0	c5d.xlarge	eu-central-1	1.722
\N	0	d2.2xlarge	eu-central-1	4.588
\N	0	d2.4xlarge	eu-central-1	9.176
\N	0	d2.8xlarge	eu-central-1	19.852
\N	0	d2.xlarge	eu-central-1	2.294
\N	0	g2.2xlarge	eu-central-1	1.732
\N	0	g2.8xlarge	eu-central-1	15.088
\N	0	g3.16xlarge	eu-central-1	5.7
\N	0	g3.4xlarge	eu-central-1	1.425
\N	0	g3.8xlarge	eu-central-1	2.85
\N	0	g3s.xlarge	eu-central-1	0.938
\N	0	i2.2xlarge	eu-central-1	5.026
\N	0	i2.4xlarge	eu-central-1	10.052
\N	0	i2.8xlarge	eu-central-1	20.104
\N	0	i2.xlarge	eu-central-1	2.513
\N	0	i3.16xlarge	eu-central-1	29.952
\N	0	i3.2xlarge	eu-central-1	3.744
\N	0	i3.4xlarge	eu-central-1	7.488
\N	0	i3.8xlarge	eu-central-1	14.976
\N	0	i3.large	eu-central-1	0.666
\N	0	i3.metal	eu-central-1	29.952
\N	0	i3.xlarge	eu-central-1	1.872
\N	0	m3.2xlarge	eu-central-1	3.632
\N	0	m3.large	eu-central-1	0.638
\N	0	m3.medium	eu-central-1	0.559
\N	0	m3.xlarge	eu-central-1	1.816
\N	0	m4.10xlarge	eu-central-1	17.4
\N	0	m4.16xlarge	eu-central-1	27.84
\N	0	m4.2xlarge	eu-central-1	3.48
\N	0	m4.4xlarge	eu-central-1	6.96
\N	0	m4.large	eu-central-1	0.6
\N	0	m4.xlarge	eu-central-1	1.74
\N	0	m5.12xlarge	eu-central-1	20.76
\N	0	m5.24xlarge	eu-central-1	41.52
\N	0	m5.2xlarge	eu-central-1	3.46
\N	0	m5.4xlarge	eu-central-1	6.92
\N	0	m5.large	eu-central-1	0.595
\N	0	m5.xlarge	eu-central-1	1.73
\N	0	m5d.12xlarge	eu-central-1	21.264
\N	0	m5d.24xlarge	eu-central-1	42.528
\N	0	m5d.2xlarge	eu-central-1	3.544
\N	0	m5d.4xlarge	eu-central-1	7.088
\N	0	m5d.large	eu-central-1	0.616
\N	0	m5d.xlarge	eu-central-1	1.772
\N	0	p2.16xlarge	eu-central-1	21.216
\N	0	p2.8xlarge	eu-central-1	10.608
\N	0	p2.xlarge	eu-central-1	1.326
\N	0	p3.16xlarge	eu-central-1	30.584
\N	0	p3.2xlarge	eu-central-1	3.823
\N	0	p3.8xlarge	eu-central-1	15.292
\N	0	r3.2xlarge	eu-central-1	3.8
\N	0	r3.4xlarge	eu-central-1	7.6
\N	0	r3.8xlarge	eu-central-1	15.2
\N	0	r3.large	eu-central-1	0.68
\N	0	r3.xlarge	eu-central-1	1.9
\N	0	r4.16xlarge	eu-central-1	29.12
\N	0	r4.2xlarge	eu-central-1	3.64
\N	0	r4.4xlarge	eu-central-1	7.28
\N	0	r4.8xlarge	eu-central-1	14.56
\N	0	r4.large	eu-central-1	0.64
\N	0	r4.xlarge	eu-central-1	1.82
\N	0	r5.12xlarge	eu-central-1	21.648
\N	0	r5.24xlarge	eu-central-1	43.296
\N	0	r5.2xlarge	eu-central-1	3.608
\N	0	r5.4xlarge	eu-central-1	7.216
\N	0	r5.large	eu-central-1	0.632
\N	0	r5.xlarge	eu-central-1	1.804
\N	0	r5d.12xlarge	eu-central-1	22.152
\N	0	r5d.24xlarge	eu-central-1	44.304
\N	0	r5d.2xlarge	eu-central-1	3.692
\N	0	r5d.4xlarge	eu-central-1	7.384
\N	0	r5d.large	eu-central-1	0.653
\N	0	r5d.xlarge	eu-central-1	1.846
\N	0	t2.2xlarge	eu-central-1	3.4288
\N	0	t2.large	eu-central-1	0.1748
\N	0	t2.medium	eu-central-1	0.1212
\N	0	t2.micro	eu-central-1	0.081
\N	0	t2.nano	eu-central-1	0.0067
\N	0	t2.small	eu-central-1	0.0944
\N	0	t2.xlarge	eu-central-1	1.7144
\N	0	t3.2xlarge	eu-central-1	3.384
\N	0	t3.large	eu-central-1	0.1636
\N	0	t3.medium	eu-central-1	0.1156
\N	0	t3.micro	eu-central-1	0.0796
\N	0	t3.nano	eu-central-1	0.006
\N	0	t3.small	eu-central-1	0.0916
\N	0	t3.xlarge	eu-central-1	1.692
\N	0	x1.16xlarge	eu-central-1	33.337
\N	0	x1.32xlarge	eu-central-1	66.674
\N	0	x1e.16xlarge	eu-central-1	42.672
\N	0	x1e.2xlarge	eu-central-1	5.334
\N	0	x1e.32xlarge	eu-central-1	85.344
\N	0	x1e.4xlarge	eu-central-1	10.668
\N	0	x1e.8xlarge	eu-central-1	21.336
\N	0	x1e.xlarge	eu-central-1	2.667
\N	0	a1.2xlarge	eu-west-1	0.2304
\N	0	a1.4xlarge	eu-west-1	0.4608
\N	0	a1.large	eu-west-1	0.0576
\N	0	a1.medium	eu-west-1	0.0288
\N	0	a1.xlarge	eu-west-1	0.1152
\N	0	c1.medium	eu-west-1	0.148
\N	0	c1.xlarge	eu-west-1	0.592
\N	0	c3.2xlarge	eu-west-1	3.48
\N	0	c3.4xlarge	eu-west-1	6.96
\N	0	c3.8xlarge	eu-west-1	15.42
\N	0	c3.large	eu-west-1	0.6
\N	0	c3.xlarge	eu-west-1	1.74
\N	0	c4.2xlarge	eu-west-1	3.452
\N	0	c4.4xlarge	eu-west-1	6.904
\N	0	c4.8xlarge	eu-west-1	15.308
\N	0	c4.large	eu-west-1	0.593
\N	0	c4.xlarge	eu-west-1	1.726
\N	0	c5.18xlarge	eu-west-1	30.456
\N	0	c5.2xlarge	eu-west-1	3.384
\N	0	c5.4xlarge	eu-west-1	6.768
\N	0	c5.9xlarge	eu-west-1	15.228
\N	0	c5.large	eu-west-1	0.576
\N	0	c5.xlarge	eu-west-1	1.692
\N	0	c5d.18xlarge	eu-west-1	30.924
\N	0	c5d.2xlarge	eu-west-1	3.436
\N	0	c5d.4xlarge	eu-west-1	6.872
\N	0	c5d.9xlarge	eu-west-1	15.462
\N	0	c5d.large	eu-west-1	0.589
\N	0	c5d.xlarge	eu-west-1	1.718
\N	0	c5n.18xlarge	eu-west-1	31.392
\N	0	c5n.2xlarge	eu-west-1	3.488
\N	0	c5n.4xlarge	eu-west-1	6.976
\N	0	c5n.9xlarge	eu-west-1	15.696
\N	0	c5n.large	eu-west-1	0.602
\N	0	c5n.xlarge	eu-west-1	1.744
\N	0	cc2.8xlarge	eu-west-1	2.25
\N	0	cr1.8xlarge	eu-west-1	3.75
\N	0	d2.2xlarge	eu-west-1	4.47
\N	0	d2.4xlarge	eu-west-1	8.94
\N	0	d2.8xlarge	eu-west-1	19.38
\N	0	d2.xlarge	eu-west-1	2.235
\N	0	f1.16xlarge	eu-west-1	14.52
\N	0	f1.2xlarge	eu-west-1	1.815
\N	0	f1.4xlarge	eu-west-1	3.63
\N	0	g2.2xlarge	eu-west-1	1.662
\N	0	g2.8xlarge	eu-west-1	14.808
\N	0	g3.16xlarge	eu-west-1	4.84
\N	0	g3.4xlarge	eu-west-1	1.21
\N	0	g3.8xlarge	eu-west-1	2.42
\N	0	g3s.xlarge	eu-west-1	0.796
\N	0	h1.16xlarge	eu-west-1	28.152
\N	0	h1.2xlarge	eu-west-1	3.519
\N	0	h1.4xlarge	eu-west-1	7.038
\N	0	h1.8xlarge	eu-west-1	14.076
\N	0	hs1.8xlarge	eu-west-1	4.9
\N	0	i2.2xlarge	eu-west-1	4.876
\N	0	i2.4xlarge	eu-west-1	9.752
\N	0	i2.8xlarge	eu-west-1	19.504
\N	0	i2.xlarge	eu-west-1	2.438
\N	0	i3.16xlarge	eu-west-1	29.504
\N	0	i3.2xlarge	eu-west-1	3.688
\N	0	i3.4xlarge	eu-west-1	7.376
\N	0	i3.8xlarge	eu-west-1	14.752
\N	0	i3.large	eu-west-1	0.652
\N	0	i3.metal	eu-west-1	29.504
\N	0	i3.xlarge	eu-west-1	1.844
\N	0	m1.large	eu-west-1	0.19
\N	0	m1.medium	eu-west-1	0.095
\N	0	m1.small	eu-west-1	0.047
\N	0	m1.xlarge	eu-west-1	0.379
\N	0	m2.2xlarge	eu-west-1	0.55
\N	0	m2.4xlarge	eu-west-1	1.1
\N	0	m2.xlarge	eu-west-1	0.275
\N	0	m3.2xlarge	eu-west-1	3.584
\N	0	m3.large	eu-west-1	0.626
\N	0	m3.medium	eu-west-1	0.553
\N	0	m3.xlarge	eu-west-1	1.792
\N	0	m4.10xlarge	eu-west-1	17.22
\N	0	m4.16xlarge	eu-west-1	27.552
\N	0	m4.2xlarge	eu-west-1	3.444
\N	0	m4.4xlarge	eu-west-1	6.888
\N	0	m4.large	eu-west-1	0.591
\N	0	m4.xlarge	eu-west-1	1.722
\N	0	m5.12xlarge	eu-west-1	20.568
\N	0	m5.24xlarge	eu-west-1	41.136
\N	0	m5.2xlarge	eu-west-1	3.428
\N	0	m5.4xlarge	eu-west-1	6.856
\N	0	m5.large	eu-west-1	0.587
\N	0	m5.xlarge	eu-west-1	1.714
\N	0	m5a.12xlarge	eu-west-1	20.304
\N	0	m5a.24xlarge	eu-west-1	40.608
\N	0	m5a.2xlarge	eu-west-1	3.384
\N	0	m5a.4xlarge	eu-west-1	6.768
\N	0	m5a.large	eu-west-1	0.576
\N	0	m5a.xlarge	eu-west-1	1.692
\N	0	m5d.12xlarge	eu-west-1	21.024
\N	0	m5d.24xlarge	eu-west-1	42.048
\N	0	m5d.2xlarge	eu-west-1	3.504
\N	0	m5d.4xlarge	eu-west-1	7.008
\N	0	m5d.large	eu-west-1	0.606
\N	0	m5d.xlarge	eu-west-1	1.752
\N	0	p2.16xlarge	eu-west-1	15.552
\N	0	p2.8xlarge	eu-west-1	7.776
\N	0	p2.xlarge	eu-west-1	0.972
\N	0	p3.16xlarge	eu-west-1	26.44
\N	0	p3.2xlarge	eu-west-1	3.305
\N	0	p3.8xlarge	eu-west-1	13.22
\N	0	r3.2xlarge	eu-west-1	3.74
\N	0	r3.4xlarge	eu-west-1	7.48
\N	0	r3.8xlarge	eu-west-1	14.96
\N	0	r3.large	eu-west-1	0.665
\N	0	r3.xlarge	eu-west-1	1.87
\N	0	r4.16xlarge	eu-west-1	28.736
\N	0	r4.2xlarge	eu-west-1	3.592
\N	0	r4.4xlarge	eu-west-1	7.184
\N	0	r4.8xlarge	eu-west-1	14.368
\N	0	r4.large	eu-west-1	0.628
\N	0	r4.xlarge	eu-west-1	1.796
\N	0	r5.12xlarge	eu-west-1	21.384
\N	0	r5.24xlarge	eu-west-1	42.768
\N	0	r5.2xlarge	eu-west-1	3.564
\N	0	r5.4xlarge	eu-west-1	7.128
\N	0	r5.large	eu-west-1	0.621
\N	0	r5.xlarge	eu-west-1	1.782
\N	0	r5a.12xlarge	eu-west-1	21.048
\N	0	r5a.24xlarge	eu-west-1	42.096
\N	0	r5a.2xlarge	eu-west-1	3.508
\N	0	r5a.4xlarge	eu-west-1	7.016
\N	0	r5a.large	eu-west-1	0.607
\N	0	r5a.xlarge	eu-west-1	1.754
\N	0	r5d.12xlarge	eu-west-1	21.84
\N	0	r5d.24xlarge	eu-west-1	43.68
\N	0	r5d.2xlarge	eu-west-1	3.64
\N	0	r5d.4xlarge	eu-west-1	7.28
\N	0	r5d.large	eu-west-1	0.64
\N	0	r5d.xlarge	eu-west-1	1.82
\N	0	t1.micro	eu-west-1	0.02
\N	0	t2.2xlarge	eu-west-1	3.4032
\N	0	t2.large	eu-west-1	0.1684
\N	0	t2.medium	eu-west-1	0.118
\N	0	t2.micro	eu-west-1	0.0802
\N	0	t2.nano	eu-west-1	0.0063
\N	0	t2.small	eu-west-1	0.0928
\N	0	t2.xlarge	eu-west-1	1.7016
\N	0	t3.2xlarge	eu-west-1	3.3648
\N	0	t3.large	eu-west-1	0.1588
\N	0	t3.medium	eu-west-1	0.1132
\N	0	t3.micro	eu-west-1	0.079
\N	0	t3.nano	eu-west-1	0.0057
\N	0	t3.small	eu-west-1	0.0904
\N	0	t3.xlarge	eu-west-1	1.6824
\N	0	x1.16xlarge	eu-west-1	32.003
\N	0	x1.32xlarge	eu-west-1	64.006
\N	0	x1e.16xlarge	eu-west-1	40
\N	0	x1e.2xlarge	eu-west-1	5
\N	0	x1e.32xlarge	eu-west-1	80
\N	0	x1e.4xlarge	eu-west-1	10
\N	0	x1e.8xlarge	eu-west-1	20
\N	0	x1e.xlarge	eu-west-1	2.5
\N	0	z1d.12xlarge	eu-west-1	22.992
\N	0	z1d.2xlarge	eu-west-1	3.832
\N	0	z1d.3xlarge	eu-west-1	5.748
\N	0	z1d.6xlarge	eu-west-1	11.496
\N	0	z1d.large	eu-west-1	0.688
\N	0	z1d.xlarge	eu-west-1	1.916
\N	0	c4.2xlarge	eu-west-2	3.476
\N	0	c4.4xlarge	eu-west-2	6.952
\N	0	c4.8xlarge	eu-west-2	15.404
\N	0	c4.large	eu-west-2	0.599
\N	0	c4.xlarge	eu-west-2	1.738
\N	0	c5.18xlarge	eu-west-2	30.636
\N	0	c5.2xlarge	eu-west-2	3.404
\N	0	c5.4xlarge	eu-west-2	6.808
\N	0	c5.9xlarge	eu-west-2	15.318
\N	0	c5.large	eu-west-2	0.581
\N	0	c5.xlarge	eu-west-2	1.702
\N	0	c5d.18xlarge	eu-west-2	31.14
\N	0	c5d.2xlarge	eu-west-2	3.46
\N	0	c5d.4xlarge	eu-west-2	6.92
\N	0	c5d.9xlarge	eu-west-2	15.57
\N	0	c5d.large	eu-west-2	0.595
\N	0	c5d.xlarge	eu-west-2	1.73
\N	0	d2.2xlarge	eu-west-2	4.544
\N	0	d2.4xlarge	eu-west-2	9.088
\N	0	d2.8xlarge	eu-west-2	19.676
\N	0	d2.xlarge	eu-west-2	2.272
\N	0	i3.16xlarge	eu-west-2	29.792
\N	0	i3.2xlarge	eu-west-2	3.724
\N	0	i3.4xlarge	eu-west-2	7.448
\N	0	i3.8xlarge	eu-west-2	14.896
\N	0	i3.large	eu-west-2	0.661
\N	0	i3.metal	eu-west-2	29.792
\N	0	i3.xlarge	eu-west-2	1.862
\N	0	m4.10xlarge	eu-west-2	17.32
\N	0	m4.16xlarge	eu-west-2	27.712
\N	0	m4.2xlarge	eu-west-2	3.464
\N	0	m4.4xlarge	eu-west-2	6.928
\N	0	m4.large	eu-west-2	0.596
\N	0	m4.xlarge	eu-west-2	1.732
\N	0	m5.12xlarge	eu-west-2	20.664
\N	0	m5.24xlarge	eu-west-2	41.328
\N	0	m5.2xlarge	eu-west-2	3.444
\N	0	m5.4xlarge	eu-west-2	6.888
\N	0	m5.large	eu-west-2	0.591
\N	0	m5.xlarge	eu-west-2	1.722
\N	0	m5d.12xlarge	eu-west-2	21.144
\N	0	m5d.24xlarge	eu-west-2	42.288
\N	0	m5d.2xlarge	eu-west-2	3.524
\N	0	m5d.4xlarge	eu-west-2	7.048
\N	0	m5d.large	eu-west-2	0.611
\N	0	m5d.xlarge	eu-west-2	1.762
\N	0	p3.16xlarge	eu-west-2	28.712
\N	0	p3.2xlarge	eu-west-2	3.589
\N	0	p3.8xlarge	eu-west-2	14.356
\N	0	r4.16xlarge	eu-west-2	28.992
\N	0	r4.2xlarge	eu-west-2	3.624
\N	0	r4.4xlarge	eu-west-2	7.248
\N	0	r4.8xlarge	eu-west-2	14.496
\N	0	r4.large	eu-west-2	0.636
\N	0	r4.xlarge	eu-west-2	1.812
\N	0	r5.12xlarge	eu-west-2	21.552
\N	0	r5.24xlarge	eu-west-2	43.104
\N	0	r5.2xlarge	eu-west-2	3.592
\N	0	r5.4xlarge	eu-west-2	7.184
\N	0	r5.large	eu-west-2	0.628
\N	0	r5.xlarge	eu-west-2	1.796
\N	0	r5d.12xlarge	eu-west-2	22.056
\N	0	r5d.24xlarge	eu-west-2	44.112
\N	0	r5d.2xlarge	eu-west-2	3.676
\N	0	r5d.4xlarge	eu-west-2	7.352
\N	0	r5d.large	eu-west-2	0.649
\N	0	r5d.xlarge	eu-west-2	1.838
\N	0	t2.2xlarge	eu-west-2	3.4224
\N	0	t2.large	eu-west-2	0.1732
\N	0	t2.medium	eu-west-2	0.1204
\N	0	t2.micro	eu-west-2	0.0808
\N	0	t2.nano	eu-west-2	0.0066
\N	0	t2.small	eu-west-2	0.094
\N	0	t2.xlarge	eu-west-2	1.7112
\N	0	t3.2xlarge	eu-west-2	3.3776
\N	0	t3.large	eu-west-2	0.162
\N	0	t3.medium	eu-west-2	0.1148
\N	0	t3.micro	eu-west-2	0.0794
\N	0	t3.nano	eu-west-2	0.0059
\N	0	t3.small	eu-west-2	0.0912
\N	0	t3.xlarge	eu-west-2	1.6888
\N	0	x1.16xlarge	eu-west-2	32.403
\N	0	x1.32xlarge	eu-west-2	64.806
\N	0	c5.18xlarge	eu-west-3	30.636
\N	0	c5.2xlarge	eu-west-3	3.404
\N	0	c5.4xlarge	eu-west-3	6.808
\N	0	c5.9xlarge	eu-west-3	15.318
\N	0	c5.large	eu-west-3	0.581
\N	0	c5.xlarge	eu-west-3	1.702
\N	0	d2.2xlarge	eu-west-3	4.544
\N	0	d2.4xlarge	eu-west-3	9.088
\N	0	d2.8xlarge	eu-west-3	19.676
\N	0	d2.xlarge	eu-west-3	2.272
\N	0	i3.16xlarge	eu-west-3	29.792
\N	0	i3.2xlarge	eu-west-3	3.724
\N	0	i3.4xlarge	eu-west-3	7.448
\N	0	i3.8xlarge	eu-west-3	14.896
\N	0	i3.large	eu-west-3	0.661
\N	0	i3.xlarge	eu-west-3	1.862
\N	0	m5.12xlarge	eu-west-3	20.688
\N	0	m5.24xlarge	eu-west-3	41.376
\N	0	m5.2xlarge	eu-west-3	3.448
\N	0	m5.4xlarge	eu-west-3	6.896
\N	0	m5.large	eu-west-3	0.592
\N	0	m5.xlarge	eu-west-3	1.724
\N	0	r4.16xlarge	eu-west-3	28.992
\N	0	r4.2xlarge	eu-west-3	3.624
\N	0	r4.4xlarge	eu-west-3	7.248
\N	0	r4.8xlarge	eu-west-3	14.496
\N	0	r4.large	eu-west-3	0.636
\N	0	r4.xlarge	eu-west-3	1.812
\N	0	r5.12xlarge	eu-west-3	21.552
\N	0	r5.24xlarge	eu-west-3	43.104
\N	0	r5.2xlarge	eu-west-3	3.592
\N	0	r5.4xlarge	eu-west-3	7.184
\N	0	r5.large	eu-west-3	0.628
\N	0	r5.xlarge	eu-west-3	1.796
\N	0	t2.2xlarge	eu-west-3	3.4224
\N	0	t2.large	eu-west-3	0.1732
\N	0	t2.medium	eu-west-3	0.1204
\N	0	t2.micro	eu-west-3	0.0808
\N	0	t2.nano	eu-west-3	0.0066
\N	0	t2.small	eu-west-3	0.094
\N	0	t2.xlarge	eu-west-3	1.7112
\N	0	t3.2xlarge	eu-west-3	3.3776
\N	0	t3.large	eu-west-3	0.162
\N	0	t3.medium	eu-west-3	0.1148
\N	0	t3.micro	eu-west-3	0.0794
\N	0	t3.nano	eu-west-3	0.0059
\N	0	t3.small	eu-west-3	0.0912
\N	0	t3.xlarge	eu-west-3	1.6888
\N	0	x1.16xlarge	eu-west-3	32.403
\N	0	x1.32xlarge	eu-west-3	64.806
\N	0	a1.2xlarge	us-east-1	0.204
\N	0	a1.4xlarge	us-east-1	0.408
\N	0	a1.large	us-east-1	0.051
\N	0	a1.medium	us-east-1	0.0255
\N	0	a1.xlarge	us-east-1	0.102
\N	0	c1.medium	us-east-1	0.13
\N	0	c1.xlarge	us-east-1	0.52
\N	0	c3.2xlarge	us-east-1	3.42
\N	0	c3.4xlarge	us-east-1	6.84
\N	0	c3.8xlarge	us-east-1	15.18
\N	0	c3.large	us-east-1	0.585
\N	0	c3.xlarge	us-east-1	1.71
\N	0	c4.2xlarge	us-east-1	3.4
\N	0	c4.4xlarge	us-east-1	6.8
\N	0	c4.8xlarge	us-east-1	15.1
\N	0	c4.large	us-east-1	0.58
\N	0	c4.xlarge	us-east-1	1.7
\N	0	c5.18xlarge	us-east-1	30.06
\N	0	c5.2xlarge	us-east-1	3.34
\N	0	c5.4xlarge	us-east-1	6.68
\N	0	c5.9xlarge	us-east-1	15.03
\N	0	c5.large	us-east-1	0.565
\N	0	c5.xlarge	us-east-1	1.67
\N	0	c5d.18xlarge	us-east-1	30.456
\N	0	c5d.2xlarge	us-east-1	3.384
\N	0	c5d.4xlarge	us-east-1	6.768
\N	0	c5d.9xlarge	us-east-1	15.228
\N	0	c5d.large	us-east-1	0.576
\N	0	c5d.xlarge	us-east-1	1.692
\N	0	c5n.18xlarge	us-east-1	30.888
\N	0	c5n.2xlarge	us-east-1	3.432
\N	0	c5n.4xlarge	us-east-1	6.864
\N	0	c5n.9xlarge	us-east-1	15.444
\N	0	c5n.large	us-east-1	0.588
\N	0	c5n.xlarge	us-east-1	1.716
\N	0	cc2.8xlarge	us-east-1	2
\N	0	cr1.8xlarge	us-east-1	3.5
\N	0	d2.2xlarge	us-east-1	4.38
\N	0	d2.4xlarge	us-east-1	8.76
\N	0	d2.8xlarge	us-east-1	19.02
\N	0	d2.xlarge	us-east-1	2.19
\N	0	f1.16xlarge	us-east-1	13.2
\N	0	f1.2xlarge	us-east-1	1.65
\N	0	f1.4xlarge	us-east-1	3.3
\N	0	g2.2xlarge	us-east-1	1.61
\N	0	g2.8xlarge	us-east-1	14.6
\N	0	g3.16xlarge	us-east-1	4.56
\N	0	g3.4xlarge	us-east-1	1.14
\N	0	g3.8xlarge	us-east-1	2.28
\N	0	g3s.xlarge	us-east-1	0.75
\N	0	h1.16xlarge	us-east-1	27.744
\N	0	h1.2xlarge	us-east-1	3.468
\N	0	h1.4xlarge	us-east-1	6.936
\N	0	h1.8xlarge	us-east-1	13.872
\N	0	hs1.8xlarge	us-east-1	4.6
\N	0	i2.2xlarge	us-east-1	4.706
\N	0	i2.4xlarge	us-east-1	9.412
\N	0	i2.8xlarge	us-east-1	18.824
\N	0	i2.xlarge	us-east-1	2.353
\N	0	i3.16xlarge	us-east-1	28.992
\N	0	i3.2xlarge	us-east-1	3.624
\N	0	i3.4xlarge	us-east-1	7.248
\N	0	i3.8xlarge	us-east-1	14.496
\N	0	i3.large	us-east-1	0.636
\N	0	i3.metal	us-east-1	28.992
\N	0	i3.xlarge	us-east-1	1.812
\N	0	m1.large	us-east-1	0.175
\N	0	m1.medium	us-east-1	0.087
\N	0	m1.small	us-east-1	0.044
\N	0	m1.xlarge	us-east-1	0.35
\N	0	m2.2xlarge	us-east-1	0.49
\N	0	m2.4xlarge	us-east-1	0.98
\N	0	m2.xlarge	us-east-1	0.245
\N	0	m3.2xlarge	us-east-1	3.536
\N	0	m3.large	us-east-1	0.614
\N	0	m3.medium	us-east-1	0.547
\N	0	m3.xlarge	us-east-1	1.768
\N	0	m4.10xlarge	us-east-1	17
\N	0	m4.16xlarge	us-east-1	27.2
\N	0	m4.2xlarge	us-east-1	3.4
\N	0	m4.4xlarge	us-east-1	6.8
\N	0	m4.large	us-east-1	0.58
\N	0	m4.xlarge	us-east-1	1.7
\N	0	m5.12xlarge	us-east-1	20.304
\N	0	m5.24xlarge	us-east-1	40.608
\N	0	m5.2xlarge	us-east-1	3.384
\N	0	m5.4xlarge	us-east-1	6.768
\N	0	m5.large	us-east-1	0.576
\N	0	m5.xlarge	us-east-1	1.692
\N	0	m5a.12xlarge	us-east-1	20.064
\N	0	m5a.24xlarge	us-east-1	40.128
\N	0	m5a.2xlarge	us-east-1	3.344
\N	0	m5a.4xlarge	us-east-1	6.688
\N	0	m5a.large	us-east-1	0.566
\N	0	m5a.xlarge	us-east-1	1.672
\N	0	m5d.12xlarge	us-east-1	20.712
\N	0	m5d.24xlarge	us-east-1	41.424
\N	0	m5d.2xlarge	us-east-1	3.452
\N	0	m5d.4xlarge	us-east-1	6.904
\N	0	m5d.large	us-east-1	0.593
\N	0	m5d.xlarge	us-east-1	1.726
\N	0	p2.16xlarge	us-east-1	14.4
\N	0	p2.8xlarge	us-east-1	7.2
\N	0	p2.xlarge	us-east-1	0.9
\N	0	p3.16xlarge	us-east-1	24.48
\N	0	p3.2xlarge	us-east-1	3.06
\N	0	p3.8xlarge	us-east-1	12.24
\N	0	p3dn.24xlarge	us-east-1	31.212
\N	0	r3.2xlarge	us-east-1	3.664
\N	0	r3.4xlarge	us-east-1	7.328
\N	0	r3.8xlarge	us-east-1	14.656
\N	0	r3.large	us-east-1	0.646
\N	0	r3.xlarge	us-east-1	1.832
\N	0	r4.16xlarge	us-east-1	28.256
\N	0	r4.2xlarge	us-east-1	3.532
\N	0	r4.4xlarge	us-east-1	7.064
\N	0	r4.8xlarge	us-east-1	14.128
\N	0	r4.large	us-east-1	0.613
\N	0	r4.xlarge	us-east-1	1.766
\N	0	r5.12xlarge	us-east-1	21.024
\N	0	r5.24xlarge	us-east-1	42.048
\N	0	r5.2xlarge	us-east-1	3.504
\N	0	r5.4xlarge	us-east-1	7.008
\N	0	r5.large	us-east-1	0.606
\N	0	r5.xlarge	us-east-1	1.752
\N	0	r5a.12xlarge	us-east-1	20.712
\N	0	r5a.24xlarge	us-east-1	41.424
\N	0	r5a.2xlarge	us-east-1	3.452
\N	0	r5a.4xlarge	us-east-1	6.904
\N	0	r5a.large	us-east-1	0.593
\N	0	r5a.xlarge	us-east-1	1.726
\N	0	r5d.12xlarge	us-east-1	21.456
\N	0	r5d.24xlarge	us-east-1	42.912
\N	0	r5d.2xlarge	us-east-1	3.576
\N	0	r5d.4xlarge	us-east-1	7.152
\N	0	r5d.large	us-east-1	0.624
\N	0	r5d.xlarge	us-east-1	1.788
\N	0	t1.micro	us-east-1	0.02
\N	0	t2.2xlarge	us-east-1	3.3712
\N	0	t2.large	us-east-1	0.1604
\N	0	t2.medium	us-east-1	0.114
\N	0	t2.micro	us-east-1	0.0792
\N	0	t2.nano	us-east-1	0.0058
\N	0	t2.small	us-east-1	0.0908
\N	0	t2.xlarge	us-east-1	1.6856
\N	0	t3.2xlarge	us-east-1	3.3328
\N	0	t3.large	us-east-1	0.1508
\N	0	t3.medium	us-east-1	0.1092
\N	0	t3.micro	us-east-1	0.078
\N	0	t3.nano	us-east-1	0.0052
\N	0	t3.small	us-east-1	0.0884
\N	0	t3.xlarge	us-east-1	1.6664
\N	0	x1.16xlarge	us-east-1	30.669
\N	0	x1.32xlarge	us-east-1	61.338
\N	0	x1e.16xlarge	us-east-1	37.344
\N	0	x1e.2xlarge	us-east-1	4.668
\N	0	x1e.32xlarge	us-east-1	74.688
\N	0	x1e.4xlarge	us-east-1	9.336
\N	0	x1e.8xlarge	us-east-1	18.672
\N	0	x1e.xlarge	us-east-1	2.334
\N	0	z1d.12xlarge	us-east-1	22.464
\N	0	z1d.2xlarge	us-east-1	3.744
\N	0	z1d.3xlarge	us-east-1	5.616
\N	0	z1d.6xlarge	us-east-1	11.232
\N	0	z1d.large	us-east-1	0.666
\N	0	z1d.xlarge	us-east-1	1.872
\N	0	a1.2xlarge	us-east-2	0.204
\N	0	a1.4xlarge	us-east-2	0.408
\N	0	a1.large	us-east-2	0.051
\N	0	a1.medium	us-east-2	0.0255
\N	0	a1.xlarge	us-east-2	0.102
\N	0	c4.2xlarge	us-east-2	3.4
\N	0	c4.4xlarge	us-east-2	6.8
\N	0	c4.8xlarge	us-east-2	15.1
\N	0	c4.large	us-east-2	0.58
\N	0	c4.xlarge	us-east-2	1.7
\N	0	c5.18xlarge	us-east-2	30.06
\N	0	c5.2xlarge	us-east-2	3.34
\N	0	c5.4xlarge	us-east-2	6.68
\N	0	c5.9xlarge	us-east-2	15.03
\N	0	c5.large	us-east-2	0.565
\N	0	c5.xlarge	us-east-2	1.67
\N	0	c5d.18xlarge	us-east-2	30.456
\N	0	c5d.2xlarge	us-east-2	3.384
\N	0	c5d.4xlarge	us-east-2	6.768
\N	0	c5d.9xlarge	us-east-2	15.228
\N	0	c5d.large	us-east-2	0.576
\N	0	c5d.xlarge	us-east-2	1.692
\N	0	c5n.18xlarge	us-east-2	30.888
\N	0	c5n.2xlarge	us-east-2	3.432
\N	0	c5n.4xlarge	us-east-2	6.864
\N	0	c5n.9xlarge	us-east-2	15.444
\N	0	c5n.large	us-east-2	0.588
\N	0	c5n.xlarge	us-east-2	1.716
\N	0	d2.2xlarge	us-east-2	4.38
\N	0	d2.4xlarge	us-east-2	8.76
\N	0	d2.8xlarge	us-east-2	19.02
\N	0	d2.xlarge	us-east-2	2.19
\N	0	g3.16xlarge	us-east-2	4.56
\N	0	g3.4xlarge	us-east-2	1.14
\N	0	g3.8xlarge	us-east-2	2.28
\N	0	g3s.xlarge	us-east-2	0.75
\N	0	h1.16xlarge	us-east-2	27.744
\N	0	h1.2xlarge	us-east-2	3.468
\N	0	h1.4xlarge	us-east-2	6.936
\N	0	h1.8xlarge	us-east-2	13.872
\N	0	i2.2xlarge	us-east-2	4.706
\N	0	i2.4xlarge	us-east-2	9.412
\N	0	i2.8xlarge	us-east-2	18.824
\N	0	i2.xlarge	us-east-2	2.353
\N	0	i3.16xlarge	us-east-2	28.992
\N	0	i3.2xlarge	us-east-2	3.624
\N	0	i3.4xlarge	us-east-2	7.248
\N	0	i3.8xlarge	us-east-2	14.496
\N	0	i3.large	us-east-2	0.636
\N	0	i3.metal	us-east-2	28.992
\N	0	i3.xlarge	us-east-2	1.812
\N	0	m4.10xlarge	us-east-2	17
\N	0	m4.16xlarge	us-east-2	27.2
\N	0	m4.2xlarge	us-east-2	3.4
\N	0	m4.4xlarge	us-east-2	6.8
\N	0	m4.large	us-east-2	0.58
\N	0	m4.xlarge	us-east-2	1.7
\N	0	m5.12xlarge	us-east-2	20.304
\N	0	m5.24xlarge	us-east-2	40.608
\N	0	m5.2xlarge	us-east-2	3.384
\N	0	m5.4xlarge	us-east-2	6.768
\N	0	m5.large	us-east-2	0.576
\N	0	m5.xlarge	us-east-2	1.692
\N	0	m5a.12xlarge	us-east-2	20.064
\N	0	m5a.24xlarge	us-east-2	40.128
\N	0	m5a.2xlarge	us-east-2	3.344
\N	0	m5a.4xlarge	us-east-2	6.688
\N	0	m5a.large	us-east-2	0.566
\N	0	m5a.xlarge	us-east-2	1.672
\N	0	m5d.12xlarge	us-east-2	20.712
\N	0	m5d.24xlarge	us-east-2	41.424
\N	0	m5d.2xlarge	us-east-2	3.452
\N	0	m5d.4xlarge	us-east-2	6.904
\N	0	m5d.large	us-east-2	0.593
\N	0	m5d.xlarge	us-east-2	1.726
\N	0	p2.16xlarge	us-east-2	14.4
\N	0	p2.8xlarge	us-east-2	7.2
\N	0	p2.xlarge	us-east-2	0.9
\N	0	p3.16xlarge	us-east-2	24.48
\N	0	p3.2xlarge	us-east-2	3.06
\N	0	p3.8xlarge	us-east-2	12.24
\N	0	r3.2xlarge	us-east-2	3.664
\N	0	r3.4xlarge	us-east-2	7.328
\N	0	r3.8xlarge	us-east-2	14.656
\N	0	r3.large	us-east-2	0.646
\N	0	r3.xlarge	us-east-2	1.832
\N	0	r4.16xlarge	us-east-2	28.256
\N	0	r4.2xlarge	us-east-2	3.532
\N	0	r4.4xlarge	us-east-2	7.064
\N	0	r4.8xlarge	us-east-2	14.128
\N	0	r4.large	us-east-2	0.613
\N	0	r4.xlarge	us-east-2	1.766
\N	0	r5.12xlarge	us-east-2	21.024
\N	0	r5.24xlarge	us-east-2	42.048
\N	0	r5.2xlarge	us-east-2	3.504
\N	0	r5.4xlarge	us-east-2	7.008
\N	0	r5.large	us-east-2	0.606
\N	0	r5.xlarge	us-east-2	1.752
\N	0	r5a.12xlarge	us-east-2	20.712
\N	0	r5a.24xlarge	us-east-2	41.424
\N	0	r5a.2xlarge	us-east-2	3.452
\N	0	r5a.4xlarge	us-east-2	6.904
\N	0	r5a.large	us-east-2	0.593
\N	0	r5a.xlarge	us-east-2	1.726
\N	0	r5d.12xlarge	us-east-2	21.456
\N	0	r5d.24xlarge	us-east-2	42.912
\N	0	r5d.2xlarge	us-east-2	3.576
\N	0	r5d.4xlarge	us-east-2	7.152
\N	0	r5d.large	us-east-2	0.624
\N	0	r5d.xlarge	us-east-2	1.788
\N	0	t2.2xlarge	us-east-2	3.3712
\N	0	t2.large	us-east-2	0.1604
\N	0	t2.medium	us-east-2	0.114
\N	0	t2.micro	us-east-2	0.0792
\N	0	t2.nano	us-east-2	0.0058
\N	0	t2.small	us-east-2	0.0908
\N	0	t2.xlarge	us-east-2	1.6856
\N	0	t3.2xlarge	us-east-2	3.3328
\N	0	t3.large	us-east-2	0.1508
\N	0	t3.medium	us-east-2	0.1092
\N	0	t3.micro	us-east-2	0.078
\N	0	t3.nano	us-east-2	0.0052
\N	0	t3.small	us-east-2	0.0884
\N	0	t3.xlarge	us-east-2	1.6664
\N	0	x1.16xlarge	us-east-2	30.669
\N	0	x1.32xlarge	us-east-2	61.338
\N	0	c1.medium	us-west-1	0.148
\N	0	c1.xlarge	us-west-1	0.592
\N	0	c3.2xlarge	us-west-1	3.48
\N	0	c3.4xlarge	us-west-1	6.96
\N	0	c3.8xlarge	us-west-1	15.42
\N	0	c3.large	us-west-1	0.6
\N	0	c3.xlarge	us-west-1	1.74
\N	0	c4.2xlarge	us-west-1	3.496
\N	0	c4.4xlarge	us-west-1	6.992
\N	0	c4.8xlarge	us-west-1	15.484
\N	0	c4.large	us-west-1	0.604
\N	0	c4.xlarge	us-west-1	1.748
\N	0	c5.18xlarge	us-west-1	30.816
\N	0	c5.2xlarge	us-west-1	3.424
\N	0	c5.4xlarge	us-west-1	6.848
\N	0	c5.9xlarge	us-west-1	15.408
\N	0	c5.large	us-west-1	0.586
\N	0	c5.xlarge	us-west-1	1.712
\N	0	c5d.18xlarge	us-west-1	31.32
\N	0	c5d.2xlarge	us-west-1	3.48
\N	0	c5d.4xlarge	us-west-1	6.96
\N	0	c5d.9xlarge	us-west-1	15.66
\N	0	c5d.large	us-west-1	0.6
\N	0	c5d.xlarge	us-west-1	1.74
\N	0	d2.2xlarge	us-west-1	4.562
\N	0	d2.4xlarge	us-west-1	9.124
\N	0	d2.8xlarge	us-west-1	19.748
\N	0	d2.xlarge	us-west-1	2.281
\N	0	f1.16xlarge	us-west-1	15.304
\N	0	f1.2xlarge	us-west-1	1.913
\N	0	f1.4xlarge	us-west-1	3.826
\N	0	g2.2xlarge	us-west-1	1.662
\N	0	g2.8xlarge	us-west-1	14.808
\N	0	g3.16xlarge	us-west-1	6.136
\N	0	g3.4xlarge	us-west-1	1.534
\N	0	g3.8xlarge	us-west-1	3.068
\N	0	g3s.xlarge	us-west-1	1.009
\N	0	i2.2xlarge	us-west-1	4.876
\N	0	i2.4xlarge	us-west-1	9.752
\N	0	i2.8xlarge	us-west-1	19.504
\N	0	i2.xlarge	us-west-1	2.438
\N	0	i3.16xlarge	us-west-1	29.504
\N	0	i3.2xlarge	us-west-1	3.688
\N	0	i3.4xlarge	us-west-1	7.376
\N	0	i3.8xlarge	us-west-1	14.752
\N	0	i3.large	us-west-1	0.652
\N	0	i3.metal	us-west-1	29.504
\N	0	i3.xlarge	us-west-1	1.844
\N	0	m1.large	us-west-1	0.19
\N	0	m1.medium	us-west-1	0.095
\N	0	m1.small	us-west-1	0.047
\N	0	m1.xlarge	us-west-1	0.379
\N	0	m2.2xlarge	us-west-1	0.55
\N	0	m2.4xlarge	us-west-1	1.1
\N	0	m2.xlarge	us-west-1	0.275
\N	0	m3.2xlarge	us-west-1	3.616
\N	0	m3.large	us-west-1	0.634
\N	0	m3.medium	us-west-1	0.557
\N	0	m3.xlarge	us-west-1	1.808
\N	0	m4.10xlarge	us-west-1	17.34
\N	0	m4.16xlarge	us-west-1	27.744
\N	0	m4.2xlarge	us-west-1	3.468
\N	0	m4.4xlarge	us-west-1	6.936
\N	0	m4.large	us-west-1	0.597
\N	0	m4.xlarge	us-west-1	1.734
\N	0	m5.12xlarge	us-west-1	20.688
\N	0	m5.24xlarge	us-west-1	41.376
\N	0	m5.2xlarge	us-west-1	3.448
\N	0	m5.4xlarge	us-west-1	6.896
\N	0	m5.large	us-west-1	0.592
\N	0	m5.xlarge	us-west-1	1.724
\N	0	m5d.12xlarge	us-west-1	21.192
\N	0	m5d.24xlarge	us-west-1	42.384
\N	0	m5d.2xlarge	us-west-1	3.532
\N	0	m5d.4xlarge	us-west-1	7.064
\N	0	m5d.large	us-west-1	0.613
\N	0	m5d.xlarge	us-west-1	1.766
\N	0	r3.2xlarge	us-west-1	3.74
\N	0	r3.4xlarge	us-west-1	7.48
\N	0	r3.8xlarge	us-west-1	14.96
\N	0	r3.large	us-west-1	0.665
\N	0	r3.xlarge	us-west-1	1.87
\N	0	r4.16xlarge	us-west-1	28.736
\N	0	r4.2xlarge	us-west-1	3.592
\N	0	r4.4xlarge	us-west-1	7.184
\N	0	r4.8xlarge	us-west-1	14.368
\N	0	r4.large	us-west-1	0.628
\N	0	r4.xlarge	us-west-1	1.796
\N	0	r5.12xlarge	us-west-1	21.36
\N	0	r5.24xlarge	us-west-1	42.72
\N	0	r5.2xlarge	us-west-1	3.56
\N	0	r5.4xlarge	us-west-1	7.12
\N	0	r5.large	us-west-1	0.62
\N	0	r5.xlarge	us-west-1	1.78
\N	0	r5d.12xlarge	us-west-1	21.888
\N	0	r5d.24xlarge	us-west-1	43.776
\N	0	r5d.2xlarge	us-west-1	3.648
\N	0	r5d.4xlarge	us-west-1	7.296
\N	0	r5d.large	us-west-1	0.642
\N	0	r5d.xlarge	us-west-1	1.824
\N	0	t1.micro	us-west-1	0.025
\N	0	t2.2xlarge	us-west-1	3.4416
\N	0	t2.large	us-west-1	0.178
\N	0	t2.medium	us-west-1	0.1228
\N	0	t2.micro	us-west-1	0.0814
\N	0	t2.nano	us-west-1	0.0069
\N	0	t2.small	us-west-1	0.0952
\N	0	t2.xlarge	us-west-1	1.7208
\N	0	t3.2xlarge	us-west-1	3.3968
\N	0	t3.large	us-west-1	0.1668
\N	0	t3.medium	us-west-1	0.1172
\N	0	t3.micro	us-west-1	0.08
\N	0	t3.nano	us-west-1	0.0062
\N	0	t3.small	us-west-1	0.0924
\N	0	t3.xlarge	us-west-1	1.6984
\N	0	z1d.12xlarge	us-west-1	23.064
\N	0	z1d.2xlarge	us-west-1	3.844
\N	0	z1d.3xlarge	us-west-1	5.766
\N	0	z1d.6xlarge	us-west-1	11.532
\N	0	z1d.large	us-west-1	0.691
\N	0	z1d.xlarge	us-west-1	1.922
\N	0	a1.2xlarge	us-west-2	0.204
\N	0	a1.4xlarge	us-west-2	0.408
\N	0	a1.large	us-west-2	0.051
\N	0	a1.medium	us-west-2	0.0255
\N	0	a1.xlarge	us-west-2	0.102
\N	0	c1.medium	us-west-2	0.13
\N	0	c1.xlarge	us-west-2	0.52
\N	0	c3.2xlarge	us-west-2	3.42
\N	0	c3.4xlarge	us-west-2	6.84
\N	0	c3.8xlarge	us-west-2	15.18
\N	0	c3.large	us-west-2	0.585
\N	0	c3.xlarge	us-west-2	1.71
\N	0	c4.2xlarge	us-west-2	3.4
\N	0	c4.4xlarge	us-west-2	6.8
\N	0	c4.8xlarge	us-west-2	15.1
\N	0	c4.large	us-west-2	0.58
\N	0	c4.xlarge	us-west-2	1.7
\N	0	c5.18xlarge	us-west-2	30.06
\N	0	c5.2xlarge	us-west-2	3.34
\N	0	c5.4xlarge	us-west-2	6.68
\N	0	c5.9xlarge	us-west-2	15.03
\N	0	c5.large	us-west-2	0.565
\N	0	c5.xlarge	us-west-2	1.67
\N	0	c5d.18xlarge	us-west-2	30.456
\N	0	c5d.2xlarge	us-west-2	3.384
\N	0	c5d.4xlarge	us-west-2	6.768
\N	0	c5d.9xlarge	us-west-2	15.228
\N	0	c5d.large	us-west-2	0.576
\N	0	c5d.xlarge	us-west-2	1.692
\N	0	c5n.18xlarge	us-west-2	30.888
\N	0	c5n.2xlarge	us-west-2	3.432
\N	0	c5n.4xlarge	us-west-2	6.864
\N	0	c5n.9xlarge	us-west-2	15.444
\N	0	c5n.large	us-west-2	0.588
\N	0	c5n.xlarge	us-west-2	1.716
\N	0	cc2.8xlarge	us-west-2	2
\N	0	cr1.8xlarge	us-west-2	3.5
\N	0	d2.2xlarge	us-west-2	4.38
\N	0	d2.4xlarge	us-west-2	8.76
\N	0	d2.8xlarge	us-west-2	19.02
\N	0	d2.xlarge	us-west-2	2.19
\N	0	f1.16xlarge	us-west-2	13.2
\N	0	f1.2xlarge	us-west-2	1.65
\N	0	f1.4xlarge	us-west-2	3.3
\N	0	g2.2xlarge	us-west-2	1.61
\N	0	g2.8xlarge	us-west-2	14.6
\N	0	g3.16xlarge	us-west-2	4.56
\N	0	g3.4xlarge	us-west-2	1.14
\N	0	g3.8xlarge	us-west-2	2.28
\N	0	g3s.xlarge	us-west-2	0.75
\N	0	h1.16xlarge	us-west-2	27.744
\N	0	h1.2xlarge	us-west-2	3.468
\N	0	h1.4xlarge	us-west-2	6.936
\N	0	h1.8xlarge	us-west-2	13.872
\N	0	hs1.8xlarge	us-west-2	4.6
\N	0	i2.2xlarge	us-west-2	4.706
\N	0	i2.4xlarge	us-west-2	9.412
\N	0	i2.8xlarge	us-west-2	18.824
\N	0	i2.xlarge	us-west-2	2.353
\N	0	i3.16xlarge	us-west-2	28.992
\N	0	i3.2xlarge	us-west-2	3.624
\N	0	i3.4xlarge	us-west-2	7.248
\N	0	i3.8xlarge	us-west-2	14.496
\N	0	i3.large	us-west-2	0.636
\N	0	i3.metal	us-west-2	28.992
\N	0	i3.xlarge	us-west-2	1.812
\N	0	m1.large	us-west-2	0.175
\N	0	m1.medium	us-west-2	0.087
\N	0	m1.small	us-west-2	0.044
\N	0	m1.xlarge	us-west-2	0.35
\N	0	m2.2xlarge	us-west-2	0.49
\N	0	m2.4xlarge	us-west-2	0.98
\N	0	m2.xlarge	us-west-2	0.245
\N	0	m3.2xlarge	us-west-2	3.536
\N	0	m3.large	us-west-2	0.614
\N	0	m3.medium	us-west-2	0.547
\N	0	m3.xlarge	us-west-2	1.768
\N	0	m4.10xlarge	us-west-2	17
\N	0	m4.16xlarge	us-west-2	27.2
\N	0	m4.2xlarge	us-west-2	3.4
\N	0	m4.4xlarge	us-west-2	6.8
\N	0	m4.large	us-west-2	0.58
\N	0	m4.xlarge	us-west-2	1.7
\N	0	m5.12xlarge	us-west-2	20.304
\N	0	m5.24xlarge	us-west-2	40.608
\N	0	m5.2xlarge	us-west-2	3.384
\N	0	m5.4xlarge	us-west-2	6.768
\N	0	m5.large	us-west-2	0.576
\N	0	m5.xlarge	us-west-2	1.692
\N	0	m5a.12xlarge	us-west-2	20.064
\N	0	m5a.24xlarge	us-west-2	40.128
\N	0	m5a.2xlarge	us-west-2	3.344
\N	0	m5a.4xlarge	us-west-2	6.688
\N	0	m5a.large	us-west-2	0.566
\N	0	m5a.xlarge	us-west-2	1.672
\N	0	m5d.12xlarge	us-west-2	20.712
\N	0	m5d.24xlarge	us-west-2	41.424
\N	0	m5d.2xlarge	us-west-2	3.452
\N	0	m5d.4xlarge	us-west-2	6.904
\N	0	m5d.large	us-west-2	0.593
\N	0	m5d.xlarge	us-west-2	1.726
\N	0	p2.16xlarge	us-west-2	14.4
\N	0	p2.8xlarge	us-west-2	7.2
\N	0	p2.xlarge	us-west-2	0.9
\N	0	p3.16xlarge	us-west-2	24.48
\N	0	p3.2xlarge	us-west-2	3.06
\N	0	p3.8xlarge	us-west-2	12.24
\N	0	p3dn.24xlarge	us-west-2	31.212
\N	0	r3.2xlarge	us-west-2	3.664
\N	0	r3.4xlarge	us-west-2	7.328
\N	0	r3.8xlarge	us-west-2	14.656
\N	0	r3.large	us-west-2	0.646
\N	0	r3.xlarge	us-west-2	1.832
\N	0	r4.16xlarge	us-west-2	28.256
\N	0	r4.2xlarge	us-west-2	3.532
\N	0	r4.4xlarge	us-west-2	7.064
\N	0	r4.8xlarge	us-west-2	14.128
\N	0	r4.large	us-west-2	0.613
\N	0	r4.xlarge	us-west-2	1.766
\N	0	r5.12xlarge	us-west-2	21.024
\N	0	r5.24xlarge	us-west-2	42.048
\N	0	r5.2xlarge	us-west-2	3.504
\N	0	r5.4xlarge	us-west-2	7.008
\N	0	r5.large	us-west-2	0.606
\N	0	r5.xlarge	us-west-2	1.752
\N	0	r5a.12xlarge	us-west-2	20.712
\N	0	r5a.24xlarge	us-west-2	41.424
\N	0	r5a.2xlarge	us-west-2	3.452
\N	0	r5a.4xlarge	us-west-2	6.904
\N	0	r5a.large	us-west-2	0.593
\N	0	r5a.xlarge	us-west-2	1.726
\N	0	r5d.12xlarge	us-west-2	21.456
\N	0	r5d.24xlarge	us-west-2	42.912
\N	0	r5d.2xlarge	us-west-2	3.576
\N	0	r5d.4xlarge	us-west-2	7.152
\N	0	r5d.large	us-west-2	0.624
\N	0	r5d.xlarge	us-west-2	1.788
\N	0	t1.micro	us-west-2	0.02
\N	0	t2.2xlarge	us-west-2	3.3712
\N	0	t2.large	us-west-2	0.1604
\N	0	t2.medium	us-west-2	0.114
\N	0	t2.micro	us-west-2	0.0792
\N	0	t2.nano	us-west-2	0.0058
\N	0	t2.small	us-west-2	0.0908
\N	0	t2.xlarge	us-west-2	1.6856
\N	0	t3.2xlarge	us-west-2	3.3328
\N	0	t3.large	us-west-2	0.1508
\N	0	t3.medium	us-west-2	0.1092
\N	0	t3.micro	us-west-2	0.078
\N	0	t3.nano	us-west-2	0.0052
\N	0	t3.small	us-west-2	0.0884
\N	0	t3.xlarge	us-west-2	1.6664
\N	0	x1.16xlarge	us-west-2	30.669
\N	0	x1.32xlarge	us-west-2	61.338
\N	0	x1e.16xlarge	us-west-2	37.344
\N	0	x1e.2xlarge	us-west-2	4.668
\N	0	x1e.32xlarge	us-west-2	74.688
\N	0	x1e.4xlarge	us-west-2	9.336
\N	0	x1e.8xlarge	us-west-2	18.672
\N	0	x1e.xlarge	us-west-2	2.334
\N	0	z1d.12xlarge	us-west-2	22.464
\N	0	z1d.2xlarge	us-west-2	3.744
\N	0	z1d.3xlarge	us-west-2	5.616
\N	0	z1d.6xlarge	us-west-2	11.232
\N	0	z1d.large	us-west-2	0.666
\N	0	z1d.xlarge	us-west-2	1.872
\.


--
-- Data for Name: insttype_temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.insttype_temp (id, name, region, family, cpu, memory, storage, os, netperf, curgen, ebsopt, termtype, effdate, price, price_desc) FROM stdin;
\.


--
-- Name: insttype_temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.insttype_temp_id_seq', 233785381, true);


--
-- Data for Name: invoice2; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.invoice2 (costacctid, ym, refno, usageusd, usagejpy, creditusd, creditjpy, taxusd, taxjpy, totalusd, totaljpy, rate, filename, refnoserial, idsfee_usd, idsfee_jpy, internaluse, companyname, dept, "position", zip, address1, address2, address3, name, tel, email, calcdate, paid, howtosend, payeracctid, payday, duedate, cid, chk1, chk2, invoicetype, costacctname, discountrate, basecharge, totalusdondemand, riflag) FROM stdin;
\.


--
-- Data for Name: key_pair; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.key_pair (id, cid, name_key, file_key_pair, create_time) FROM stdin;
\.


--
-- Name: key_pair_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.key_pair_id_seq', 19, true);


--
-- Data for Name: logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.logs (logdate, cid, email, name, act) FROM stdin;
\.


--
-- Data for Name: obj; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.obj (cid, name, ipaddr, submask, description, id, status) FROM stdin;
\.


--
-- Name: obj_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.obj_id_seq', 12, true);


--
-- Data for Name: plan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.plan (id, cid, plan, startdate, enddate, startuid) FROM stdin;
2	17	1	2018-09-07 15:47:05.199057	2018-09-20 19:31:00.610409	baohq@vn.ids.jp
4	17	1	2018-09-20 19:33:07.386852	\N	tha@gmail
3	3	1	2018-09-07 15:48:45.641379	2018-09-26 15:13:37.059275	nakano@ids.co.jp
5	17	1	2018-09-26 15:37:15.257002	\N	thoattt@vn.ids.jp
\.


--
-- Name: plan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.plan_id_seq', 5, true);


--
-- Data for Name: proj; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proj (cid, pcode, pname, regdate, status, disporder, memo, dept, id) FROM stdin;
3	asdfadf	asdfasdfasdf	2018-03-19 12:57:06.419393	-1	2	asd	1	1
17	提交会议讨论决定的建议提交会议讨论决定的建议提交会议讨论决定的建议あああああああああああああああああああああああああ１２３	提交会议讨论决定的建议提交会议讨论决定的建议提交会议讨论决定的建议あああああああああああああああああああああああああ	2018-09-24 11:45:49.870806	-1	10		5	24
17	45354	ểtte4	2018-08-01 18:24:02.412719	-1	1		2	5
31	3404	64564564	2018-08-13 11:26:55.967456	0	1	45645	1	7
17	dsfstểtr	sdfsdểtr	2018-09-04 16:06:52.769839	-1	4	gfgd	3	10
17	ưerweghdrsrtse	rưerwe	2018-09-05 12:33:17.927069	-1	4	ưerw	2	11
17	tesstttt	tesstttt	2018-09-05 16:08:31.907921	0	2	sds	2	12
17	demo1	hehehe	2018-08-02 16:20:07.315163	0	3		2	6
17	baoosacds	jedfhgfj	2018-09-24 11:32:31.271874	-1	8	jdfggfghjkl;'';liuytrewqascvbnl;lkiuyterfd	5	21
3	project111	project111	2018-09-14 15:06:58.430906	-1	2	new memo	1	16
17	uytuy	gogogo	2018-08-29 11:23:53.141971	-1	4		3	8
17	fhncvb	fgvnmfvcn	2018-09-24 12:02:51.493953	-1	14	gfnfgn	5	30
17	fgjndf235	fdgfdbcfv	2018-09-24 12:02:44.026921	-1	13		3	29
17	bhfgj	fghbfdbfg	2018-09-24 12:02:31.316485	-1	12	fghdf	3	28
17	dfheadgt2133g	bfgn	2018-09-24 12:02:21.600111	-1	11		5	27
17	ghfgbsdhfgjxdgfj	dfhgfhf	2018-09-24 12:02:10.556802	-1	10		5	26
17	提交会议讨论决定的建议提交会议讨论决定的建议提交会议讨论决定的建议あああああああああああああああああああああああああaaあ1333	提交会议讨论决定的建议提交会议讨论决定的建议提交会议讨论决定的建议aaあああああああああああああああああああああああああああaaあああああああああああああああああああああああああああああaaあああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああ	2018-09-24 11:53:45.219459	0	8		5	25
17	thoa_pcode_test	thoa_pname_test	2018-09-14 12:35:33.070678	0	4	abc dat de ddi dao	6	13
17	123456あ	123456提交会议讨论决定的建	2018-09-24 11:42:19.046082	0	7		5	22
17	A001	A001plant ajj	2018-08-30 17:55:42.908601	0	1	350332112 aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	3	9
17	test009	thoa test009	2018-09-14 12:37:41.652507	0	6	asdfghjkl	5	15
17	fdncvb	vm bnj.cx cx	2018-09-24 12:04:35.91137	-1	20		5	36
17	sdhg,bn	,k.gjh,m	2018-09-24 12:04:21.576699	-1	19		3	35
17	sdgsdgvse	rhfgkj	2018-09-24 12:04:11.518591	-1	18		2	34
17	fdmnh,uiu232434	cb,	2018-09-24 12:03:25.638102	-1	17	vm	3	33
17	gfvnm v	gbhmfgvb	2018-09-24 12:03:14.612183	-1	16	mngdn	5	32
17	gfnjmvc	gvbmgvbn	2018-09-24 12:02:59.591248	-1	15	gncvn	2	31
17	提交会议讨论决定的建议提交会议讨论决定的建议提交会议讨论决定的建议あああああああああああああああああああああああああ	提交会议讨论决定的建议提交会议讨论决定的建议提交会议讨论决定的建议	2018-09-24 11:43:59.407662	-1	8		5	23
54	thuyproject	thuyproject	2018-09-25 12:27:01.939021	0	1		1	37
17	007tesst 007	thoa007_pname_test	2018-09-14 12:36:03.173524	0	5	abc dat de ddi dao	6	14
11	testproject	testproject	2018-09-25 19:30:58.014253	0	1		1	38
1	SMOJ	ASONY修理受付ワークフローシステム開発	2018-09-27 12:02:27.515972	0	1		1	40
1	AAB	CギターECサイト構築	2018-09-27 12:02:59.394328	0	2		1	41
1	AA2	B社内ポータルサイト構築	2018-09-27 12:03:31.906751	0	3		1	42
3	CCC	テスト３2	2018-03-20 13:58:27.722721	-1	3		6	3
3	コードテスト	メイ名前	2018-09-14 17:20:16.845171	-1	4	note note111	1	17
3	abccccsd	abctest	2018-09-19 18:08:46.232009	-1	5		9	18
3	xcxcxcv	xcvxcv	2018-09-20 15:48:52.438412	-1	6	xcv	1	19
3	dfbdfbdf	32423423	2018-09-20 15:51:18.045076	-1	7	fdgdfgdfg	1	20
3	AAABCDEFGH	テストプロジェクトAAAB	2018-03-29 20:15:33.679227	-1	2	あああaa	9	4
3	AABDDDD	test log123	2018-03-19 16:25:53.765821	-1	1	afdasfeasdsd	9	2
3	no-manager	project-test-no manager	2018-09-26 14:55:39.785307	-1	8		13	39
355	SV	SunnyView	2018-11-15 13:53:59.77369	0	1		3	43
355	IDS	社内利用	2018-11-15 13:54:38.113917	0	2		1	44
\.


--
-- Name: proj_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proj_id_seq', 44, true);


--
-- Data for Name: rate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rate (ym, dy) FROM stdin;
2017/01	115.31
2017/02	116.63
2017/03	114.04
2017/04	114.73
2017/05	113.94
2017/06	115.62
2017/07	113.24
2017/08	112.66
2017/09	114.92
2017/10	116.53
2017/11	115.16
2017/12	114.92
2018/01	112.73
2018/02	108.34
2018/03	108.75
2018/06	107.80
2018/05	107.80
2018/07	107.82
2018/08	108.75
2018/04	108.75
2018/09	116.37
2018/10	115.41
2018/11	115.92
\.


--
-- Data for Name: request; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.request (cid, reqid, from_uid, to_uid, dept, reqdate, reqauthdate, reqngdate, reqcanceldate, from_comment, to_comment) FROM stdin;
\.


--
-- Data for Name: request_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.request_detail (cid, reqid, name, type, sg, platform, kernelid, ramdiskid, imageid, availabilityzone, privateip, publicip) FROM stdin;
\.


--
-- Data for Name: security_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.security_group (id, cid, name_sg, description_sg, vpc_sg, create_time, inst_req_id, sg_id) FROM stdin;
\.


--
-- Name: security_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.security_group_id_seq', 244, true);


--
-- Data for Name: sg; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sg (cid, id, name, lastchecked, description, tbl_id, vpcid) FROM stdin;
\.


--
-- Data for Name: sg_enable; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sg_enable (id, cid, sg_id) FROM stdin;
\.


--
-- Name: sg_enable_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sg_enable_id_seq', 200, true);


--
-- Data for Name: sg_inbound; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sg_inbound (id, id_security_group, inb_type, inb_protocol, inb_cidr_ip, inb_source, inb_description, create_time, inb_formport_range, inb_toport_range) FROM stdin;
\.


--
-- Name: sg_inbound_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sg_inbound_id_seq', 731, true);


--
-- Data for Name: sg_ipperm; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sg_ipperm (cid, id, protocol, fromport, toport, iprange, lastchecked, type, tbl_id) FROM stdin;
\.


--
-- Name: sg_ipperm_tbl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sg_ipperm_tbl_id_seq', 502180, true);


--
-- Data for Name: sg_outbound; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sg_outbound (id, id_security_group, outb_type, outb_protocol, outb_cidr_ip, outb_destination, outb_description, create_time, outb_formport_range, outb_toport_range) FROM stdin;
\.


--
-- Name: sg_outbound_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sg_outbound_id_seq', 377, true);


--
-- Name: sg_tbl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sg_tbl_id_seq', 262141, true);


--
-- Data for Name: sg_temp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sg_temp (id, cid, sg_id, sg_vpc, sg_name, sg_desc, inst_req_id, is_created) FROM stdin;
\.


--
-- Name: sg_temp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sg_temp_id_seq', 2, true);


--
-- Data for Name: sg_temp_rules; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sg_temp_rules (id, cid, sg_temp_id, type, stype, proto, range, source, target, "desc", pfrom, pto) FROM stdin;
\.


--
-- Name: sg_temp_rules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sg_temp_rules_id_seq', 3, true);


--
-- Data for Name: stat_cost_daily; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stat_cost_daily (cid, ymd, servicecode, ubcost, bcost, usagequantity, currencycode, estimated, dept, pcode) FROM stdin;
\.


--
-- Data for Name: stat_ebs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stat_ebs (cid, ym, count, gib, dept, pcode) FROM stdin;
\.


--
-- Data for Name: stat_inst; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stat_inst (cid, ym, count, instancetype, dept, pcode) FROM stdin;
\.


--
-- Data for Name: stat_s3; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stat_s3 (cid, ym, count, gib, dept, pcode) FROM stdin;
\.


--
-- Data for Name: tbl_exchangerate; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_exchangerate (id, cid, empid, email, num_rate, code_usd, code_jpy, count_rate, status, date_created) FROM stdin;
\.


--
-- Name: tbl_exchangerate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tbl_exchangerate_id_seq', 1, false);


--
-- Data for Name: tbl_lambda; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_lambda (id, cid, pcode, inst_id, volumn_device, volumn_volid, volumn_size, volumn_voltype, time_gener, hours_gener, minute_gener, dai_gener, week_gener, day_gener, month_gener, num_gener, create_time, next_time, hours_dai_gener, minute_dai_gener, hours_week_gener, minute_week_gener, hours_month_gener, minute_month_gener) FROM stdin;
\.


--
-- Name: tbl_lambda_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tbl_lambda_id_seq', 1, false);


--
-- Data for Name: tbl_logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_logs (id, cid, empid, category, action, description, error_flag, date_created) FROM stdin;
\.


--
-- Name: tbl_logs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tbl_logs_id_seq', 428277, true);


--
-- Data for Name: tbl_notif; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_notif (id, cid, empid, notif_title, notif_msg, notif_time, publish_date, notif_status) FROM stdin;
8	3	1	お知らせテスト	Context	2018-09-17 00:00:00	2018-09-25 00:00:00	2
3	9	2	test post	Titletest post	2018-09-26 00:00:00	2018-10-03 00:00:00	1
9	3	1	xxxxxxx	xxxx	2018-09-24 00:00:00	2018-09-27 00:00:00	3
10	9	2	test notif 3	demo notif 33	2018-09-01 00:00:00	2018-09-30 00:00:00	1
11	3	1	lamlamlam	lamlamlam	2018-09-24 00:00:00	2018-09-27 00:00:00	1
13	9	2	admin dev again	admin dev again	2018-09-19 00:00:00	2018-09-29 00:00:00	1
12	9	2	test admin_dev	test admin_dev\r\ntest admin_dev	2018-09-18 00:00:00	2018-10-04 00:00:00	1
14	9	2	xxxxxx	xxx	2018-09-12 00:00:00	2018-09-28 00:00:00	1
\.


--
-- Name: tbl_notif_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tbl_notif_id_seq', 14, true);


--
-- Data for Name: tbl_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tbl_roles (id, cid, url, action, sysadmin, sysadmin_role, manager, manager_role, employee, employee_role) FROM stdin;
442	0	mst/dept.php	create	1	0	0	0	0	0
443	0	mst/dept.php	read	1	0	0	0	0	0
444	0	mst/dept.php	update	1	0	0	0	0	0
445	0	mst/dept.php	delete	1	0	0	0	0	0
446	0	mst/user.php	read	1	0	1	1	0	0
447	0	mst/user.php	delete	1	0	1	1	0	0
448	0	mst/userpj.php	read	1	0	1	1	0	0
449	0	mst/userpj_change.php	read	1	0	1	1	0	0
450	0	mst/userpj_change.php	update	1	0	1	1	0	0
451	0	mst/user2.php	read	1	0	1	1	0	0
452	0	mst/user2.php	create	1	0	1	1	0	0
453	0	mst/user2.php	update	1	0	1	1	0	0
454	0	mst/user2.php	change_permission	1	0	1	3	0	0
455	0	mst/proj.php	read	1	0	1	1	0	0
456	0	mst/proj.php	delete	1	0	1	1	0	0
457	0	mst/proj2.php	read	1	0	1	1	0	0
458	0	mst/proj2.php	create	1	0	1	1	0	0
459	0	mst/proj2.php	update	1	0	1	1	0	0
460	0	admin/obj.php	read	1	0	1	1	0	0
461	0	admin/obj.php	delete	1	0	1	1	0	0
462	0	admin/objproc.php	read	1	0	1	1	0	0
463	0	admin/objproc.php	create	1	0	1	1	0	0
464	0	admin/objproc.php	update	1	0	1	1	0	0
465	0	mst/instowner.php	read	1	0	1	1	0	0
466	0	mst/instowner.php	update	1	0	1	1	0	0
467	0	admin/key.php	read	1	0	0	0	0	0
468	0	admin/key.php	update	1	0	0	0	0	0
469	0	admin/ami.php	read	1	0	0	0	0	0
470	0	admin/ami.php	update	1	0	0	0	0	0
471	0	admin/az.php	read	1	0	0	0	0	0
472	0	admin/az.php	update	1	0	0	0	0	0
473	0	admin/insttype.php	read	1	0	0	0	0	0
474	0	admin/insttype.php	update	1	0	0	0	0	0
476	0	admin/cw_rules.php	read	1	0	1	1	0	0
477	0	admin/cw_rules.php	update	1	0	1	1	0	0
478	0	admin/cw_rules.php	delete	1	0	1	1	0	0
479	0	admin/cw_rules_proc.php	create	1	0	1	1	0	0
480	0	admin/cw_rules_proc.php	read	1	0	1	1	0	0
481	0	admin/cw_rules_proc.php	update	1	0	1	1	0	0
482	0	cons/profile.php	read	1	0	0	0	0	0
483	0	cons/profile.php	update	1	0	0	0	0	0
484	0	mst/changepw.php	read	1	4	1	4	1	4
485	0	mst/changepw.php	update	1	4	1	4	1	4
486	0	cons/info.php	read	1	0	1	0	1	0
487	0	cons/info.php	update	1	0	1	0	1	0
488	0	cons/request.php	read	1	0	1	1	1	5
489	0	cons/request.php	remove	1	0	1	1	1	5
490	0	cons/request.php	approve	1	0	1	1	0	0
491	0	cons/request.php	reject	1	0	1	1	0	0
492	0	cons/request.php	request_delete	0	0	0	0	1	5
493	0	cons/request.php	approve_delete	1	0	1	1	0	0
494	0	cons/inst.php	read	1	0	1	1	1	5
495	0	cons/inst.php	start_stop	1	0	1	1	1	5
496	0	cons/download.php	read	1	0	1	0	1	0
497	0	cons/req_inst.php	read	1	0	1	1	1	5
498	0	cons/req_inst2.php	read	1	0	1	1	1	5
499	0	cons/req_inst3.php	read	1	0	1	1	1	5
500	0	cons/security_group.php	read	1	0	1	1	1	5
501	0	admin/log.php	read	1	0	1	1	1	5
502	0	db/configuration.php	read	1	0	0	0	0	0
503	0	db/configuration.php	update	1	0	0	0	0	0
504	0	db/db.php	read	1	0	1	1	1	5
505	0	db/view_pdf.php	read	1	1	1	1	1	1
506	0	admin/backup.php	read	1	0	1	1	1	5
507	0	admin/backup.php	update	1	0	1	1	1	5
508	0	admin/ebs.php	read	1	0	1	1	1	5
509	0	admin/ebs.php	update	1	0	1	1	1	5
510	0	mst/userpj.php	read	1	0	1	1	0	0
511	0	mst/userpj_change.php	read	1	0	1	1	0	0
512	0	mst/userpj_change.php	update	1	0	1	1	0	0
513	0	cons/user_inst.php	read	1	0	1	1	1	5
514	0	cons/user_inst.php	update	1	0	1	1	1	5
515	0	db/privacy.php	read	1	1	1	1	1	1
\.


--
-- Name: tbl_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tbl_roles_id_seq', 515, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (uid, tel, regkey, regdate, cid, name, status, dept, companyname, key, secret, "position", address1, address2, address3, zip, payday, id, costacctid, parentacctid, internaluse, howtosend, updated, s3bucket, parent, pending, lastresult, sumacctid, invoicetype, costacctname, discountrate, basecharge, memo, discount) FROM stdin;
manage+387427046517@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	332		0	対象外	株式会社インボイス？	null	null		対象外	\N	\N	対象外	0	2062	387427046517	627722188647	t	\N	2018-12-11 15:54:18.52688	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+685034808910@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	382	有本 慎	0	事業推進室	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル6F	\N	\N	164-0011	0	2112	685034808910	881814943668	t	\N	2018-12-04 16:56:00.547196	\N	f	f	\N	\N	0	\N	0	0	\N	\N
gsi_aws@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	293	総務部長	0	官署支出官 国土地理院	国土地理院	null	null		茨城県つくば市北郷1番	\N	\N	305-0811	30	2023	749554519080	669828113555	f	\N	2018-12-04 16:56:22.989602	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_goof@ids.co.jp		\N	2018-11-01 12:17:49.963566	144	高岡 博剛	0	\N	株式会社グーフ	OTStN6ZFzatOXCyrW65skuYzbykZwudV0ANe6R1edUDRNoEr9gcL8V6J7mn/DyYg	2Tnl94ygqfeEAkBJUmKkx1P5wnjWNSk1zVAx6yCo	\N	東京都品川区大崎4-1-2	ウイン第2五反田ビル3F	\N	141-0032	30	926	638182312418	638182312418	f	\N	2018-12-04 16:56:19.429388	sunnyview-638182312418	t	f	\N	\N	1	\N	0	0	\N	\N
rsv_sunny_jp.panasonic@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	175	須山　統昭	0	AIS社インフォ（事）クラウド事業開発室	パナソニック株式会社	9s4MSMdOOrGeKOcf8Ds5cXrGGB8kQWqdwTRNCJwhth++D/oJkGWrkv5/em9EQKSX	oMAR55S20X43oySrobdQ87T6zH/BFVadvNwMR5cv	\N	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	957	972838442246	972838442246	f	\N	2018-12-04 16:56:41.302499	sunnyview-972838442246	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_kobunsha@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	188	畠山　雄一	0	総務局情報システム部	株式会社光文社	xnGnwaz4F8CODo9Ds1S8XiUZNqB59xn1pGnQYC9E0NgcK3yONEpZfM8c4GjuH4yi	5PxZicWDxv4UE4w0F1hVuPHn8eR6OlJKlVNp3l4e	\N	東京都文京区音羽1-16-6	\N	\N	112-8011	0	970	134067479084	134067479084	t	\N	2018-12-04 16:56:49.965306	kobunsha-billing	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_fns_xble_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	395	浦野　由香	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2125	548025531434	337521816362	f	\N	2018-12-04 16:57:20.77994	\N	f	f	\N	337521816362	0	\N	0	0	\N	\N
seaos_tico_airas_stg@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	409	山下 直宏	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2139	067750790426	430239111239	f	\N	2018-12-04 16:57:24.136688	\N	f	f	\N	430239111239	0	seaos_tico_airas_stg	0	0	\N	\N
seaos_truckberth_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	414	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2144	921257825859	886821596825	f	\N	2018-12-04 16:57:25.489005	\N	f	f	\N	886821596825	0	seaos_truckberth_pro	0	0	\N	\N
rsv_sunny_bushiroad+783388786474@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	381	広瀬　和彦	0	モバイルコンテンツ部	株式会社ブシロード　停止	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル	\N	\N	164-0011	0	2111	783388786474	\N	t	\N	2018-12-03 18:31:57.798257	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+888653386834@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	383		0	対象外	株式会社ブシロードメディア　停止	null	null	\N	対象外	\N	\N	対象外	0	2113	888653386834	\N	t	\N	2018-12-03 18:32:48.863244	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+54578415506@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	313		0	対象外	東洋ビジネスエンジニアリング	null	null		対象外	\N	\N	対象外	0	2043	54578415506	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
ajis_iot_prj@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	278		0	対象外	株式会社エイジス	null	null	\N	対象外	\N	\N	262-0032	0	2008	921568778705	\N	t	\N	2018-12-03 18:34:46.322657	\N	f	f	\N	\N	0	\N	0	0	\N	\N
dev_sunny@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	287		0	対象外	自社	null	null		対象外	\N	\N	対象外	0	2017	322276704206 	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunnyofficial@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	270		-1	対象外	サニーHP用アカウント	9vM56KItX8M9o/q5AqwoSgsXPhzI9gKTp25iuQ7Xm5NZmxH1EjTHSoOlY8+/Yj8L	vlu+xzhQsO6D01Gvv4KZ9Y6ZuLQt7goN11M54x8c	\N	対象外	\N	\N	対象外	0	1052	285693525620	\N	t	\N	2018-11-01 12:17:49.963566	sunnyview-285693525620	t	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	453	朝田 美恵子	0	技術開発本部要素技術開発本部第三部	シスメックス株式会社	null	null		兵庫県神戸市西区高塚台4-4-4	\N	\N	651-2271	30	2183	036230671360	524994771186	f	\N	2018-12-04 16:57:42.541241	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_wowtech@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	259	壁本 典之	0	ソリューションセールス部	ワウテック株式会社	KpML5d+TWGIs0K8tOPepCwYs1EV7hEByUo+fdKih/9/66LBU126TPyGIvp+Acqqs	rwSy+0Y0nVd5b4mhKcvRxDh0BtHWRglJyS3IhTZ9	\N	東京都港区赤坂四丁目15番1号	赤坂ガーデンシティ4階	\N	107-0052	30	1041	934134119942	934134119942	f	\N	2018-12-04 16:57:37.485181	sunnyview-934134119942	t	f	\N	\N	0	\N	0	0	\N	\N
t.takahashi@looop.co.jp	\N	\N	2018-11-07 10:55:39.236947	467	秋田 周作	0		株式会社Looop	null	null		東京都台東区上野三丁目24番6号上野フロンティアタワー15階	\N	\N	110-0005	30	2197	130055118079	405273292958	f	\N	2018-12-04 16:56:54.568495	\N	f	f	\N	405273292958	0	AlfaSystem CIS	0	0	\N	\N
rsv_sunny_chodai2@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	120	元木 賢一	0	社会システム事業部	株式会社長大	qYFovXXSNDYFWkNaeYud+O57aoed2fwdqaxwYn83y9uRZy0k8RtH2atZce8+CLBq	QdMxQaSImJVwfbUcBp/ZmSWjhsUD5vwMYUHKwcX5	\N	東京都中央区勝どき1丁目13-1	\N	\N	104-0054	30	902	235104986164	235104986164	f	\N	2018-12-04 16:56:02.584027	sunnyview-235104986164	t	f	\N	\N	0	\N	0	0	\N	\N
aws_manage@nabiq.co.jp	\N	\N	2018-11-07 10:55:39.236947	272	大川 宏	0		株式会社ナビック	null	null	美術本部長	東京都千代田区神田紺屋町11岩田ビル 5F	\N	\N	101-0035	30	2002	354209027122	414456009824	f	\N	2018-12-04 16:57:01.219366	\N	f	f	\N	414456009824	0	\N	0	0	\N	\N
rsv_sunny_conffort@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	123	ご担当者	0	\N	株式会社ウェルフェアビジネスソリューションズ	p3bKqYaaIgflGSimpQt124n9vwGI4XE+hc/RWgwPisLX+FkbIKmTLyQ/YEdamB4W	cZ7G2j8W9O0UBuX/tzlz1msLbDcaroRcpznUni+/	\N	東京都中央区東日本橋2-16-4　NSビル6階	\N	\N	103-0004	30	905	664904911309	664904911309	f	\N	2018-12-04 16:56:04.70894	sunnyview-664904911309	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_gali@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	140	加藤 裕稔	0	\N	株式会社ガリインターナショナル	i9xkRK9X5jCbQMZCCGD3RX5KLb+wRTN7dgQZ8w7sFa5t3k4s4n4jWxZ87KNxuLvD	Y8XqCNrLs0B7VkEuMhEVi78ugEeaon4/rKObG4nJ	\N	東京都中野区上高田3-1-3	野田ビル201	\N	164-0002	45	922	138865218164	138865218164	f	\N	2018-12-04 16:56:16.714049	sunnyview-138865218164	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_his_test@ids.co.jp	\N	\N	2018-11-01 12:17:49.963566	155		0	対象外	株式会社エイチ・アイ・エス	dH0q1fR3bte/ufMDLpMtpf7psfWb0SeEpsFjwGIhaGDtEcsWUfSZKa+jpqYligCO	P1u4+Vh2JtZvYfokJMMbpsvLKYOUJdUlmsxYPY9k		対象外	\N	\N	対象外	0	937	635606617962	635606617962	t	\N	2018-12-04 16:56:27.121858	sunnyview-635606617962	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_jmdc@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	173	早川 宣浩	0	営業企画部	健康年齢少額短期保険準備株式会社	AROcffVmHzg1vYWK87crn50gZeqYwMEvLBAu4lp+Mj/KUzmFoYHi4WmYSrb4IkW8	NZjXSvcBiWvOpifO/NO6jtQscMM1eBl9bSMRN2QV	部長	東京都港区芝大門2-5-5	住友芝大門ビル11階	\N	105-0012	30	955	484927217584	484927217584	f	\N	2018-12-04 16:56:39.962504	sunnyview-484927217584	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_kcme2@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	182	木虎 央果	0	ソリューション開発部	KCCSモバイルエンジニアリング株式会社	rkXezYj8mfEcBexvGOszh9C2rGsRrya6wkXTkp4z1Co4o/H5I/V3koNCYD69ZFPU	HBegiZuSqwfU6PU6x+v6B2Bo0Wuz7qwVy6q0jmFX	\N	東京都港区三田3-13-16	三田43MTビル16階	\N	108-0073	50	964	236812449032	236812449032	f	\N	2018-12-04 16:56:45.994163	sunnyview-236812449032	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_l-m@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	194	板垣 秀典	0	制作課	株式会社ランドマーク	7f+sX369ktOyK2+vmmQeTR9LvmufJC45cyFHVItDVrldkQM3FZby5ZV5dvL1b35M	Sk1d2GX1KbHyuavyjOgEJdkaRbfoG78X2ti9VWNE	課長	東京都新宿区西新宿 6-5-1	新宿アイランドタワー 8F	\N	163-1308	0	976	027509846516	027509846516	t	\N	2018-12-04 16:56:53.906686	sunnyview-027509846516	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_mobileit@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	386	神作 美光	0	管理部	株式会社モバイルインターネットテクノロジー	null	null		東京都千代田区紀尾井町3-6紀尾井町パークビル 1F	\N	\N	102-0094	30	2116	445352436901	562973278378	f	\N	2018-12-04 16:56:58.549297	\N	f	f	\N	562973278378	0	(mobileit-aws-asp-hitsuzikaifx@)	0	0	\N	\N
rsv_sunny_looop@ids.co.jp		\N	2018-11-01 12:17:49.963566	195	秋田 周作	0	\N	株式会社Looop	7wSTAtju+DR1TepyTAqW62Kocz8oBSGqnYtZ6+nzpQkT/G9ZtBDiCdkWta1ycbT/	ZS0QJVM1Fj4Q3pEGJA5gaj0+8EeWRoVTm1vJkfll	\N	東京都台東区上野三丁目24番6号上野フロンティアタワー15階	\N	\N	110-0005	30	977	405273292958	405273292958	f	\N	2018-12-04 16:56:54.580319	sunnyview-405273292958	t	f	\N	\N	2	\N	0	0	\N	\N
seaos_saaswms_stg@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	403	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2133	808360515943	647397808689	f	\N	2018-12-04 16:57:16.172212	\N	f	f	\N	647397808689	0	seaos_saaswms_stg	0	0	\N	\N
rsv_sunny_panasonic_02@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	216	須山　統昭	0	クラウド事業開発室	パナソニック株式会社	5Wzr2djKWNgdnuOPGVNo3FP5yJ6cXI2Ckm8U+WpbLco/Ki+QUlg79xjcQVQyW1B6	BVGV6DOf1zw+7If2Et/FwN0cq9HKPLrC2WMtvcOh	\N	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	0	998	535443676446	535443676446	t	\N	2018-12-04 16:57:08.842083	sunnyview-535443676446	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_raku-den@ids.co.jp		\N	2018-11-01 12:17:49.963566	223	井川 雅央	0	IT事業部	株式会社楽電	+oX3Eipdjz0vBdNs16+6E50KqycGt9o5IXLmlhq22LEqz8OMs92eZIfyryLD3Ivl	+OcllbszJKyLrxbxnIZPYurU6sq/4MhqqTPGw9Vr	\N	東京都豊島区東池袋3-13-7	池三ビル2階	\N	170-0013	30	1005	749434594113	749434594113	f	\N	2018-12-04 16:57:13.537403	sunnyview-749434594113	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_mtq_quent_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	397	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2127	207866260799	121215650074	f	\N	2018-12-04 16:57:21.482343	\N	f	f	\N	121215650074	0	seaos_mtq_quent_pro	0	0	\N	\N
manage+651737375868@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	345		0	対象外	XPI開発？	null	null		対象外	\N	\N	対象外	0	2075	651737375868	627722188647	t	\N	2018-12-11 15:54:18.487772	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+298325701191@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	326		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2056	298325701191	627722188647	t	\N	2018-12-11 15:54:18.492056	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_tokailease@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	249	小池 靖史	0	総務部総務課 	東海リース株式会社	NzIkFu++mBAVUtWa4Wlo216HxnvoL5cehUVbujdjx3uP+/xr/8/ksIy2xcSiLqBi	F9RBbE2UoxhcRnSAv2s2v+Uv+FsZJriYFvpyDDYw		大阪府大阪市北区天神橋二丁目北二番六号	\N	\N	530 - 0041	60	1031	816777147720	816777147720	f	\N	2018-12-04 16:57:30.836075	sunnyview-816777147720	t	f	\N	\N	0	\N	0	0	\N	\N
wencolite-aws@hitachi-kenki.com	\N	\N	2018-11-07 10:55:39.236947	472		0		日立建機株式会社	null	null		対象外	\N	\N	対象外	0	2202	348176045278	778372751271	t	\N	2018-12-04 16:56:24.416081	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_arkrey@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	113	統括事務センタ（情C通）	0		アークレイ株式会社	cTZeDISm7mfksaU1AI53eqSv+In69pn2avprj74r0JkskqhhyfsEU9L6YvSyDQob	x83EJ/Wy2EyMU9V1T6rBAnlTP2QFBjzEaVX55vjA		京都市南区東九条西明田町57	\N	\N	601-8045	30	895	588403246770	588403246770	f	\N	2018-12-04 16:55:57.73859	sunnyview-588403246770	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+020271326797@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	370	金原 威也	0	事業推進室	株式会社ブシロードミュージック	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル2F	\N	\N	164-0011	0	2100	020271326797	881814943668	t	\N	2018-12-04 16:56:00.560807	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_healthcare_relations02@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	154	山田 美子	0	総合企画管理グループ	株式会社ヘルスケアリレイションズ	d9iZPadb0GNfnN5icp0JCr3EF7LGxlSwKC1mdFHOC/SLifrVzULrZiVVtyv9Fr8C	VL1YAQefWJd2nwrfTFjbSnBC5On3PcP9nmx3wJOu		東京都調布市多摩川3-35-4	\N	\N	182-0025	30	936	903399657825	903399657825	f	\N	2018-12-04 16:56:26.378479	sunnyview-903399657825	t	f	\N	241214219430	0	\N	0	0	\N	\N
rsv_sunny_mobileit+562973278378@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	201	神作 美光	0	管理部	株式会社モバイルインターネットテクノロジー	r5zKnqvl6F6dwfrs5n1JfneXNRCho/oeVybge/7jXyJXbIK03b4w2XwKKe6dityL	O43yCTSACBq+f30T1uoa8LLBscmU4jVKqY3f43Lm	\N	東京都千代田区紀尾井町3-6	紀尾井町パークビル 1F	\N	102-0094	30	983	562973278378	562973278378	f	\N	2018-12-04 16:56:58.556803	sunnyview-562973278378	t	f	\N	\N	3	\N	0	0	\N	\N
rsv_sunny_nmsys@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	212	市原 幹子	0	システム事業部 システム営業部HS営業課	株式会社NMシステムズ	DVPr/aRk9jz3bUDEHq6j2KnAnpfMOvBRI4WMr9adP9KwOpYI/HgLQwrcXoYvPUjg	bMmWUIuMto44qCuPWFpqvDwURMpVygM+Oe259Zu7	課長補佐	東京都千代田区神田須田町 1-5-10相鉄万世橋ビル 6階	\N	\N	101-0041	30	994	289698158691	289698158691	f	\N	2018-12-04 16:57:06.177006	sunnyview-289698158691	t	f	\N	\N	0	\N	0	0	\N	\N
manage+376365147650@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	330		0	情報システム部	カトーレック株式会社	null	null	\N	香川県高松市朝日町5－5－1	\N	\N	760-0065	0	2060	376365147650	627722188647	t	\N	2018-12-11 15:54:18.483401	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+454565533497@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	334		0	対象外	株式会社トーハン	null	null		対象外	\N	\N	対象外	0	2064	454565533497	627722188647	t	\N	2018-12-11 15:54:18.486258	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_quicktranslate2@ids.co.jp		\N	2018-11-01 12:17:49.963566	221	鳥原 剛	0	\N	スピード翻訳株式会社	IOYQ0uPgF/9sxhUpGEjQaWNJpPzVP0UbY5DKBUS/tRh+1S1IpPQ0fpiYofwE3UjJ	R0D3tGdt3kxy511EoeKWjBi48MpXpiXRtcrOzoC0	\N	東京都千代区神保町3-7-1 3F	\N	\N	101-0051	30	1003	802830634063	802830634063	f	\N	2018-12-04 16:57:12.217585	sunnyview-802830634063	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_mtq_quent_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	396	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2126	180248187608	121215650074	f	\N	2018-12-04 16:57:21.480526	\N	f	f	\N	121215650074	0	seaos_mtq_quent_dev	0	0	\N	\N
yamamoto@saice.co.jp		\N	2018-11-07 10:55:39.236947	473	山本 優	-1	技術開発部　計測制御技術課	株式会社サイス	null	null	係長	東京都品川区東五反田5丁目23番1号第2五反田不二越ビル3F	\N	\N	141-0023	30	2203		\N	f	\N	2018-11-20 15:52:39.573304	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+281252087677@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	378	広瀬　和彦	0	モバイルコンテンツ部	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1	住友中野坂上ビル	\N	164-0011	0	2108	281252087677	881814943668	t	\N	2018-12-04 16:56:00.56858	\N	f	f	\N	\N	4	KasukabeCity : kasukabe+aws@bushiroad.com	0	0	\N	\N
rsv_sunny_comvex@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	121	伊藤 尚貴	0	管理本部	株式会社コンベックス	MFuVPrCc9CD1FxPlxSlz76v8ozjN2iQSinSSBeQQr5XzaSeX9mQp5dkFLFJjLY9G	47ME/lokTumIvxMneYlLEAmvI2r/INMQh7gzlQ6D	取締役管理本部長	東京都渋谷区渋谷2-15-1	渋谷クロスタワー15階	\N	150-0002	30	903	860966311693	860966311693	f	\N	2018-12-04 16:56:03.287033	sunnyview-860966311693	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_informetis_02@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	163	宮澤	0	\N	インフォメティス株式会社	J1fL02ID1pAKS1ay4HFhLsjb4BZG8lcGfy0Aan6clJ3sJeN1iQDTDGZ2/foerFsS	qAV7XvGCMwtNDBQ9WaJrXmftIVSefjC0bjkQpntt	\N	東京都港区高輪1-5-4常和高輪ビル 3F	\N	\N	108-0074	0	945	498864889965	498864889965	t	\N	2018-12-04 16:56:33.101029	sunnyview-498864889965	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_ctcg_04@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	127	寺島 由加	0	ICT技術統轄部	伊藤忠テクノソリューションズ株式会社	MZKJ2xJwDzazkh5fnCorm1s0YAyGMmRK/IHcP2iMZ5qHVgEAjrN46qwKNa3d2rnw	OqFttUL+8DsayiAlQL2lIRr4a5czSEN44vQzCSZH	\N	東京都千代田区霞ヶ関3-2-5	霞ヶ関ビル22F	\N	100-6080	30	909	763456056660	763456056660	f	\N	2018-12-04 16:56:07.474645	sunnyview-763456056660	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_dataservice@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	129	成田 智之	0	第2ソリューション事業部	株式会社データサービス	VyxjtYyP2+Wh7CutrJzIWAWXLZOqzb3oOlJ59L2xjOc/lDMZxhBqGSu3ewp0XlJa	LdICgQgG3Cfo7LsUDomFHkj6ZTMmaHm/jw+s4/rN		東京都新宿区西新宿3丁目20番2号東京オペラシティタワー39階	\N	\N	163-1439	30	911	389904163382	389904163382	f	\N	2018-12-04 16:56:08.915226	sunnyview-389904163382	t	f	\N	158012712342	0	\N	0	0	\N	\N
seaos_sbsl_quent_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	405	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2135	045931348597	739026619319	f	\N	2018-12-04 16:57:23.466348	\N	f	f	\N	739026619319	0	\N	0	0	\N	\N
rsv_sunny_toppan_kansaitic@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	252	辰己 元昭	0	トッパンアイデアセンター 関西TIC本部ICTソリューション部 セキュアソリューションチーム	凸版印刷株式会社	oOwrkCcHKgEvQGyVPUdItB+SQOkvMUle9NE4F2CCH4QvzC17iHm6hm88mb4eDLT/	T6iTzQxFxTyLnGlH5wAdvEl0vJwAkSW9vnkBYAEj	\N	大阪府大阪市北区中之島2-3-18	中之島フェスティバルタワー	\N	530-0005	60	1034	082103927637	082103927637	f	\N	2018-12-04 16:57:32.772129	sunnyview-082103927637	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_seaos_sbsl_quent@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	238	大和田 美雪	0	\N	シーオス株式会社	1ZMZEU8uVUBs8DnPAzEmesP/LNHwYodTCTHWI5o2w3vqA11eWFZ2QqqSfFKqvyQ7	9+4Gz/gUDomnAZWbCUAuMtc7DuX1bu1zPQkiOOUW	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1020	739026619319	739026619319	f	\N	2018-12-04 16:57:23.467875	sunnyview-739026619319	t	f	\N	\N	1	\N	0	0	\N	\N
aws_idsall@ids.co.jp		\N	2018-11-01 12:17:49.963566	101		-1	対象外	IDS共通	mobmXNP0+RnTnyHiSvw743Nf/n71T31c6+Zmwe0StY4=		\N	対象外	\N	\N	対象外	0	883		\N	t	\N	2018-11-27 15:43:21.729354		t	t	\N	\N	0	\N	0	0	\N	\N
manage+534347854147@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	337		0	対象外	IDS（ソリューションG）？	null	null		対象外	\N	\N	対象外	0	2067	534347854147	627722188647	t	\N	2018-12-11 15:54:18.494788	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+374712845072@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	329		0	対象外	IDS（旧Akatsuki)	null	null		対象外	\N	\N	対象外	0	2059	374712845072	627722188647	t	\N	2018-12-04 13:46:58.367865	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+97061654617@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	315		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2045	97061654617	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sys-alert@kobunsha.com	\N	\N	2018-11-07 10:55:39.236947	461	畠山　雄一	0	総務局情報システム部	株式会社光文社	null	null		東京都文京区音羽1-16-6	\N	\N	112-8011	30	2191	432176729279	134067479084	f	\N	2018-12-04 16:56:49.963817	\N	f	f	\N	134067479084	0	\N	0	0	\N	\N
manage+786410540614@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	350		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2080	786410540614	627722188647	t	\N	2018-12-11 15:54:18.497379	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+947587701943@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	359		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2089	947587701943	627722188647	t	\N	2018-12-11 15:54:18.49882	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+659641315633@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	346		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2076	659641315633	627722188647	t	\N	2018-12-11 15:54:18.508627	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+180925193455@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	320		0	対象外	株式会社エムアールエム・ワールドワイド	null	null		対象外	\N	\N	対象外	0	2050	180925193455	627722188647	t	\N	2018-12-11 15:54:18.531449	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_diamondmedia@ids.co.jp		\N	2018-11-01 12:17:49.963566	131	中根 愛	0	コーポレート部	ダイヤモンドメディア株式会社	7shLLAJGHWTxDUS4ztj4+XabVmvd3EI5rXsKfUksAFqsSgQbMvKMcF3gOMcpUiIS	oV9GQ6bkMohzAc0ak8+hT3lhxWc6tMDr8s0DlQWi	\N	東京都港区南青山4-9-1	シンプル青山ビル1F	\N	107-0062	30	913	823451081780	823451081780	f	\N	2018-12-04 16:56:10.374768	sunnyview-823451081780	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_nabiq@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	205	大川 宏	0	\N	株式会社ナビック	NjOn8C/oW2MVxBCL29qxuGbWtKHlUAYc7PM2cEivoGTBSMd8JiCV+2veO91jv/sQ	/jyp/tJBn4Nb1E1vy5wlde76ddaTOZv77eyD1gdN	美術本部長	東京都千代田区神田紺屋町11	岩田ビル 5F	\N	101-0035	30	987	414456009824	414456009824	f	\N	2018-12-04 16:57:01.226794	rsv-sunny-nabiq-billingreport	t	f	\N	\N	0	\N	0	0	\N	\N
it-dev@greenpacks.co.jp	\N	\N	2018-11-07 10:55:39.236947	302	山本 達也	0	IT企画サービス課	日本グリーンパックス株式会社	null	null		東京都中央区日本橋浜町3-26浜町京都ビル3F	\N	\N	103-0007	30	2032	582609048294	560570045510	f	\N	2018-12-04 16:56:22.282284	\N	f	f	\N	560570045510	0	NGP_Solum	0	0	\N	\N
seaos_sbsl_quent_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	404	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2134	122696049140	739026619319	f	\N	2018-12-04 16:57:23.464375	\N	f	f	\N	739026619319	0	\N	0	0	\N	\N
rsv_sunny_mgfc@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	199		0	対象外	株式会社マグノリアファクトリー	seWFi6cqqdGo66UfEEG69BAtAizO5z++1jGvorhYQ1xTQRPrs/IutZrAQ+oYAVbr	lj90K1b71fpL8D1JUpGud55WcUb61KPnOSYxBdsH		対象外	\N	\N	対象外	0	981	690994107840	690994107840	t	\N	2018-12-04 16:56:57.243186	sunnyview-690994107840	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_iscube.ihi@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	167	八木 栄朗	0	ビジネスソリューション事業部 アプリケーションサービスグループ	株式会社IHIエスキューブ	N8rHanQXBcMOqRZDbs1mEHSeLR/oJw4dlVykOkHTJWmaM2Rxl8db+AAMYeQ+5aD/	W53pymBb1YF/OqmpzC7UmYEbrOEwKClPmNbzlUDa	\N	東京都江東区豊洲3-1-1	豊洲IHIビル	\N	135-0061	30	949	965970626413	965970626413	f	\N	2018-12-04 16:56:35.812807	sunnyview-965970626413	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_sacn@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	228	安見 英二	0	第4システム部	システム・アナライズ株式会社	KvAelb1tkJ4Umc9O/+LXkvsSraSmPr70qXQSnf6BFHtpPcAiJioccD3BnN2aufAu	hd3fbowqbTQ0BER9yYEDzfjTxPtBG+pznBS2eE73	\N	東京都千代田区飯田橋1-12-7	飯田橋センタービル 2F	\N	102-0072	30	1010	757823835123	757823835123	f	\N	2018-12-04 16:57:16.857522	sunnyview-757823835123	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_looop_vpp@ids.co.jp		\N	2018-11-01 12:17:49.963566	196	秋田 周作	0	\N	株式会社Looop	dkuVnn+CJGt/LkzPKCEW8I2BPOHkJ+p1sUNfBtORWKlb+gAIEiXkkbA3+Q1H9HOs	qkER4tWOjjWoa/qFoITaVvWf/EU0hXtNg27h7QrT	\N	東京都台東区上野三丁目24番6号	上野フロンティアタワー15階	\N	110-0005	30	978	340439544105	340439544105	f	\N	2018-12-04 16:56:55.227849	sunnyview-340439544105	t	f	\N	\N	3	\N	0	0	\N	\N
rsv_sunny_arrowsystem@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	114	詰光 隆之	0	首都圏事業部	株式会社アローシステム開発	0a3Jse6VG6+aSdPIwY369UX5FeJGPXgmB3qRYTI8a9igY2oEGrzhu8vQzNdYFleC	QQNWjDyUAZEGvHZgQZ8rZo7+GcwxartwitNajVc6	\N	東京都足立区梅島3-32-6第8矢野新ビル5F	\N	\N	121-8521	0	896	276649302238	276649302238	t	\N	2018-12-04 16:55:58.393525	arrowsystem-billingreport	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_yumeshin_ir@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	261	志村　秀夫	0	戦略推進本部 兼 IR室	株式会社夢真ホールディングス	CSTabI9V4L//i7+pbUmqzJoDt7udqWCcoMcnhY7oK9X70lDiM7MdbbqFox17NJN8	IEvWDwUA/85fYfoLXHFGF5uaL5dvBwwgTDOiwiR0	\N	東京都千代田区丸の内1-4-1丸の内永楽ビルディング22F	\N	\N	100-0005	0	1043	459457459923	459457459923	t	\N	2018-12-04 16:57:38.818621	sunnyview-459457459923	t	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+130413059480@ids.co.jp		\N	2018-11-07 10:55:39.236947	432	山口 祥吾	0	医療・介護ソリューション事業部	株式会社パイプドビッツ	null	null	\N	東京都港区赤坂2丁目9番11号オリックス赤坂2丁目ビル2階、3階	\N	\N	107-0052	30	2162	130413059480	524994771186	f	\N	2018-12-04 16:57:42.506042	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+654520052715@ids.co.jp		\N	2018-11-07 10:55:39.236947	437	菅野　久美子	0	\N	株式会社デザイン百貨店	null	null	代表取締役	東京都渋谷区富ヶ谷1-41-7クリサンテ2006	\N	\N	151-0063	30	2167	654520052715	524994771186	f	\N	2018-12-04 16:57:42.523959	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+771947951026@ids.co.jp		\N	2018-11-07 10:55:39.236947	439	北山　朋	0	\N	特定非営利活動法人アイ・コラボレーション神戸	null	null	サービス管理責任者	神戸市中央区港島９丁目１番地103KIO(神戸インキュベーションオフィス) 103号	\N	\N	650-0045	35	2169	771947951026	524994771186	f	\N	2018-12-04 16:57:42.529503	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_jp.panasonic_pdc@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	176	須山　統昭	0	クラウド事業開発室	パナソニック株式会社	UdyxtTwB8wmbKJp1WcYEx3EqKCcghHbIJx9o95MKn7ixlm7a8S6MD7GLRte1lqmG	sD26AzvQO3+ofINT6hipP2BPTHTRdHzMnhckUUiA	\N	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	958	452507188348	452507188348	f	\N	2018-12-04 16:56:41.962794	sunnyview-452507188348	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_panasonic@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	215	須山　統昭	0	クラウド事業開発室	パナソニック株式会社	DlHYEF6RmsnaV5jtn8i7k7c/5nT/938lsRZclikvfDQ1HGvcrwsG6Mx4sUmODqUa	75r8TKJAX1k7gWawmE1rQZpFdTlbWeVM0CKJR/rC	\N	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	997	174327050188	174327050188	f	\N	2018-12-04 16:57:08.188213	billing-174327050188	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_nice@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	207	岩城 正江	0	\N	ナイスジャパン株式会社	TBoN5rhkKGHs6O9LfbrMLgu04Ih2J0Ih3XsnrJ1Ax53/QuUjg7mv+cYy7Wabcvnn	rTD1cnZp4oyC/b2ms/qwkcnqZj4ybUvvO7ehJbyi	\N	東京都港区赤坂 2-2-17	ニッセイ溜池山王ビル 7F	\N	107-0052	30	989	663522027464	663522027464	f	\N	2018-12-04 16:57:02.597966	sunnyview-663522027464	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_magazine@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	197	三原	0	経理部	株式会社マガジンハウス	J1NWch/8PLDuyjzNO+3gHX26HWs9sME1d+Wf0ly3femTTGbrsCOuB8umyv/OSbfO	ZRcCkfer4qT/KEGPv8Mq8mv4run/7YbyDoDUMi8K	\N	東京都中央区銀座3-13-10	\N	\N	104-8003	65	979	707986523907	707986523907	f	\N	2018-12-04 16:56:55.892556	sunnyview-707986523907	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_saaswms@ids.co.jp		\N	2018-11-01 12:17:49.963566	227	小川 弥蓉	0	管理本部	シーオス株式会社	Hyz+SMWUhaQKgnQ7nxrLglGhOSAVKhBwt9MD6UMl39DYtCnlkZAxGnaD+njqLBmy	C1hsVNllk3ci/ivzI6MIw1UU9tZTVIfZLriKapLC	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1009	647397808689	647397808689	f	\N	2018-12-04 16:57:16.175592	sunnyview-647397808689	t	f	\N	\N	2	\N	0	0	\N	\N
seaos_sbsl_quent_stg@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	406	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2136	512988657972	739026619319	f	\N	2018-12-04 16:57:23.462417	\N	f	f	\N	739026619319	0	\N	0	0	\N	\N
sunnycloud+242246708215@ids.co.jp		\N	2018-11-07 10:55:39.236947	459	札幌市長	0	札幌市市民文化局市民生活部消費生活課	札幌市役所	null	null	\N	北海道札幌市中央区北１条西２丁目	\N	\N	060-8611	30	2189	242246708215	524994771186	f	\N	2018-12-04 16:57:42.539196	\N	f	f	\N	\N	4	さっぽろ暮らしまなBOOKWebサーバ利用料	0	0	\N	\N
manage+616716945066@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	342		-1	対象外	IDS（ラクスル調査用踏み台サーバ）	null	null		対象外	\N	\N	対象外	0	2072	616716945066	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+635520816226@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	344	高岡 博剛	0	SE・企画サポート リーダー	株式会社グーフ	null	null	\N	東京都品川区大崎4-1-2	ウイン第2五反田ビル3F	\N	141-0032	30	2074	635520816226	627722188647	f	\N	2018-12-11 15:54:18.500146	\N	f	f	0	\N	1	\N	0	0	\N	\N
manage+872763797342@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	354		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2084	872763797342	627722188647	t	\N	2018-12-11 15:54:18.50591	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+245112351820@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	323		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2053	245112351820	627722188647	t	\N	2018-12-11 15:54:18.510272	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+260249930508@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	324	ご担当者	0	\N	学校法人国際基督教大学	null	null	\N	東京都 三鷹市 大沢3-10-2	パブリックリレーションズ・オフィス	\N	181-8585	60	2054	260249930508	627722188647	f	\N	2018-12-11 15:54:18.515603	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+171427857597@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	319	ご担当者	0	営業統括本部営業本部セールスサポートメディア部	日立建機株式会社	null	null	\N	東京都台東区東上野二丁目１６番１号野イーストタワー１５階	\N	\N	110-0015	0	2049	171427857597	627722188647	t	\N	2018-12-11 15:54:18.530111	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+378263640644@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	331		-1	対象外	株式会社NDT	null	null		対象外	\N	\N	対象外	0	2061	378263640644	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
seaos_ticohino_smlg_stg@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	412	山下直宏	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2142	297725583354	511061991579	f	\N	2018-12-04 16:57:24.799593	\N	f	f	\N	511061991579	0	seaos_ticohino_smlg_stg	0	0	\N	\N
rsv_sunny_banzai@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	115	大川　健治	0	システム部	株式会社バンザイ	5onxJdZ65ynpOVInGz9/DKqTAuwbYjQXHpGu5XrX7VryS5bo2YyIOlmtg+iknoQH	zsoJNv6Agesun56b9rB8TZmOLWN5X24urOXJbJmP	次長	東京都港区芝2-31-19	\N	\N	105-8580	30	897	117564722212	117564722212	f	\N	2018-12-04 16:55:59.162113	sunnyview-117564722212	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_katolec2@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	179		0	対象外	カトーレック株式会社	MF9MwVtB0wvytZBh6rdVTaNuFrUTll13xqB/Tr0nweX0ZMOsB79cL+m2B/++IsrI	7PnSqfRXx47EoSxalSkJ7g2ImOYjdALr4dQUPY0c	\N	対象外	\N	\N	対象外	0	961	287839638206	287839638206	t	\N	2018-12-04 16:56:43.933958	sunnyview-287839638206	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_kyodonews@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	191	横井 英夫	0	情報技術局	一般社団法人共同通信社	rOkELWUYAHPn9ev/EA1crwa6LayC+w1EkpTFZRIyGb886hdbMTLDtXYPcGUfsWv4	tu3MuPDEcQfpzPuSbqWhRiKInPmPGU1xU6j9Jk2D	\N	東京都港区東新橋 1-7-1	汐留メディアタワー 6F	\N	105-7201	40	973	991690471439	991690471439	f	\N	2018-12-04 16:56:51.934614	sunnyview-991690471439	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_bushiroad+010213649563@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	375	有本 慎	0	事業推進室	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル6F	\N	\N	164-0011	0	2105	010213649563	881814943668	t	\N	2018-12-04 16:56:00.556958	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+777970358410@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	384	原田 克彦	0	\N	株式会社キックスロード	null	null	\N	東京都中野区中央1-38-1	\N	\N	164-0011	0	2114	777970358410	881814943668	t	\N	2018-12-04 16:56:00.558945	\N	f	f	\N	\N	0	\N	0	0	\N	\N
mcse_admin@concier.com	\N	\N	2018-11-07 10:55:39.236947	364	石井 豪一	0	情報システム部	株式会社メディカル・コンシェルジュ	null	null		東京都渋谷区恵比寿南1-5-5JR恵比寿ビル11F	\N	\N	150-0022	30	2094	867269330997	483519549373	f	\N	2018-12-04 16:56:04.061707	\N	f	f	\N	483519549373	0	\N	0	0	\N	\N
cloud-admin@ebara.com	\N	\N	2018-11-07 10:55:39.236947	285		0	対象外	株式会社荏原製作所	null	null		対象外	\N	\N	対象外	0	2015	715621633112	131330460059	t	\N	2018-12-04 16:56:13.152822	\N	f	f	\N	131330460059	0	\N	0	0	\N	\N
app_dev@gali-int.com	\N	\N	2018-11-07 10:55:39.236947	281	加藤 裕稔	0		株式会社ガリインターナショナル	null	null		東京都中野区上高田3-1-3野田ビル201	\N	\N	164-0002	45	2011	666413015729	138865218164	f	\N	2018-12-04 16:56:16.70867	\N	f	f	\N	138865218164	0	MAG_1153 (No.40_freecomic)	0	0	\N	\N
rsv_sunny_mosk.tytlabs@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	202	野々山 浩和	0	調達室　設備・資材G	株式会社豊田中央研究所	MHPWbtRpyNWYy++33iedZVOHRfL4kmjYFEMpdJe4OEKeYkDIe+MrgPtqTGfThdeu	Csh+/5uoTxfq4X86ruXqzMwQtAjdtmGFQFSUKzQ8	\N	愛知県長久手市横道41番地の1	\N	\N	480-1192	30	984	565123696282	565123696282	f	\N	2018-12-04 16:56:59.21917	sunnyview-565123696282	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_multisoup.aws@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	390	吉野　智則	0	テクニカルGr	マルティスープ株式会社	null	null		東京都千代田区神田錦町3-11精興竹橋共同ビル3F	\N	\N	101-0054	30	2120	893421010059	389721610116	f	\N	2018-12-04 16:56:59.890334	\N	f	f	\N	389721610116	0	\N	0	0	\N	\N
manage+767225523084@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	348		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2078	767225523084	627722188647	t	\N	2018-12-11 15:54:18.511578	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+535539005631@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	377	岩倉 亜貴	0	\N	株式会社響	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル6F	\N	\N	164-0011	0	2107	535539005631	881814943668	t	\N	2018-12-04 16:56:00.570479	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+861137548419@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	374	有本 慎	0	事業推進室	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル6F	\N	\N	164-0011	0	2104	861137548419	881814943668	t	\N	2018-12-04 16:56:00.572353	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_ctcg_02@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	125	有馬 正行	0	情報通信第１本部ｻｰﾋﾞｽﾋﾞｼﾞﾈｽ推進ﾁｰﾑ	伊藤忠テクノソリューションズ株式会社	Bv8zmTZhE5vXTe2m+V0B8thpdiF9r/DMln11VQevRspzVtEvfeh2vEUxqwAxJiyg	ywH80ZilWSiR18M5nWyR0H/5pmdozsKdMHY4Xa8a	チーム長	東京都千代田区霞ヶ関3-2-5霞ヶ関ビル	\N	\N	100-6080	50	907	370917222042	370917222042	f	\N	2018-12-04 16:56:06.138736	sunnyview-370917222042	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_kcme@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	181	中島 早紀	0	事業推進部	KCCSモバイルエンジニアリング株式会社	ufeRBmJ5olugvkn0q4S74qBmEuCQkjxaHTK2htpYPH7OWObSGOZdemnfNFnajZhQ	SHCYbFvZPGFEYDAnqEp9MEaRSid7J70CJAyvPgaP	\N	東京都港区三田3-13-16	三田43MTビル16階	\N	108-0073	50	963	114786104228	114786104228	f	\N	2018-12-04 16:56:45.307822	sunnyview-114786104228	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_katolec1@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	178		0	対象外	カトーレック株式会社	0BCB0oTrhDTiJWGbkLh5jHt+jsjBuj7FZNsrvZpBrefimGiuHDcnzVkj2LmxPyuK	t2ZRrVjQnuf1I1D0aCMWi0g6WeTVMs3YkY8hRqQz	\N	対象外	\N	\N	対象外	0	960	404528255413	404528255413	t	\N	2018-12-04 16:56:43.299982	sunnyview-404528255413	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_keio@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	185	五十嵐 健一	0	メディアセンター本部	慶應義塾大学	JJhVGB2zOs9IjuzZ8/+CsDxjjBFF3d5Yg3ibvUYryr8AwrAZ7SmNpZniFDZVaN1i	ePORrb6WYPQtqzWIql/r7By/7DV3wcXS3D151BuO		東京都港区三田 2-15-45	\N	\N	108-8345	60	967	696006258316	696006258316	f	\N	2018-12-04 16:56:47.941923	sunnyview-696006258316	t	f	\N	\N	0	\N	0	0	\N	\N
dev-sunny-201608@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	102		0	対象外	サニー開発用(2016年度）	A0GyCPfCaDeyrdVKVAs3ykIfd5J7atGRT2dFTzrHD81ojp/INICWAX/UoI4WEjxs	6ZT9NgTxKdqAl/PelSbwpR3TGldPgGnXg07+WRxq		対象外	\N	\N	対象外	0	884	687874003018	687874003018	t	\N	2018-12-10 16:29:26.348881	sunnyview-687874003018	t	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_seaos_owh@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	236	小川 弥蓉	0	管理本部	シーオス株式会社	pK91K6bvaL6/dvSwwRV87HxfzNLSH040v2bWdJih2AH33BOWQ6LHYFf4U/RhZzfX	J8YMzJdUEK0o4G+RjqdeGH4DDCiYjXryK501akBm	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1018	347855887727	347855887727	f	\N	2018-12-04 16:57:22.120517	report02	t	f	\N	\N	1	\N	0	0	\N	\N
rsv_sunny_rios_2@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	225	近藤　正浩	0	\N	株式会社リオス	O2Jg6G6cRBAp5a9agzhpgbf3KjM4qJtVySsL/XIL7lyeIcIG6omxkYqOANdOWQIr	SVPW5cCESI2H3SNxXFPdfOZ2ETALgzE0eUp4hoTW	\N	東京都港区芝5丁目33-1森永プラザビル 16Ｆ	\N	\N	108-0014	0	1007	167134776434	\N	t	\N	2018-12-04 11:07:29.434908	sunnyview-167134776434	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_fujitsu_sil@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	139	小林 裕幸	0	管理部	株式会社富士通システム統合研究所	+vhG4n/el4FeHcKTZgpvcTanM0a4mEEVH0lz5ewcKwitprVLsy60lwuNbEJKsn4F	GoHHK2ZL/fRQzSY4KaItgWta+BP5BVD6NfEs3Xtj	部長	神奈川県川崎市中原区	上小田中4-1‐1	\N	211-8588	30	921	667583187628	667583187628	f	\N	2018-12-04 16:56:16.006189	sunnyview-667583187628	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_tbcscat1@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	246	森　純一	0	WEB企画部	株式会社ティビィシィ・スキヤツト	9Ux6EK6HCsSo23gBK7IFFVYWLaTXeb2KYRDdqT2y+NvV4yQdHvFrWW2IIX5YPjD4	JBhTuC1Qu21G9064Uf3WIX/QQMEqQRec8aZXE7ln	\N	東京都中央区日本橋本町3-8-4第二東硝ビル7F	\N	\N	103-0023	0	1028	579694412533	579694412533	t	\N	2018-12-04 16:57:28.840258	sunnyview-579694412533	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_koizumig@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	189	三浦 昭寿	0	プロストック事業部	株式会社小泉	oeL4x+U3Ts2ccJBe7RKTK0pAC90C1EVEO6BnI16FdwnFxetxo6zm4KjibCvc/GWH	6D5r8aCeUsXyoDo8W4Up0D7yK03MVYYQGDAte00y	係長	東京都杉並区荻窪4-32-9	アネックスビル2F	\N	167-0051	45	971	578429476568	578429476568	f	\N	2018-12-04 16:56:50.620101	sunnyview-578429476568	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_kel@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	186	森 研人	0	\N	兼松エレクトロニクス株式会社	+HkiOpEu+D9pUUQIQsS1CD077SYZ4rh7FcXqgyeh0AZopONeuBYKSiwKyaZrB5bR	EgNkAwZf6tzgerq4vbbMI2Hzi/eKdm4ibs3qYKhj	\N	東京都中央区京橋2-13-10	京橋MIDビル8F	\N	104-8338	30	968	329486351300	329486351300	f	\N	2018-12-04 16:56:48.628433	sunnyview-329486351300	t	f	\N	\N	0	\N	0	0	\N	\N
ltjp20171127@gmail.com	\N	\N	2018-11-07 10:55:39.236947	309	根本 美紀	0		菱通ジャパン株式会社	null	null		東京都中央区新川1-22-11フジライト新川ビル2階	\N	\N	104-0033	30	2039	954871613068	984226601754	f	\N	2018-12-04 16:56:53.261298	\N	f	f	\N	984226601754	0	\N	0	0	\N	\N
rsv_sunny_rozetta@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	226	尾崎 京花	0	\N	株式会社ロゼッタ	WMlbRYUfbOjE4SafZjRxLB1H6wURoB3CQjQDuar9+wsv9MMrt/59xhsrtiQoR7v0	38dDkn4j2EHu9c6bejGrlvJXK0gYxSr5/oBhLZwa	\N	東京都千代田区神田神保町3-7-1ニュー九段ビル	\N	\N	101-0051	0	1008	869601446920	869601446920	t	\N	2018-12-04 16:57:15.522479	sunnyview-869601446920	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_tico_airas_verify@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	410	山下 直宏	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2140	171494105468	430239111239	f	\N	2018-12-04 16:57:24.138662	\N	f	f	\N	430239111239	0	seaos_tico_airas_verify	0	0	\N	\N
sunnycloud+254244830977@ids.co.jp		\N	2018-11-07 10:55:39.236947	443	佐藤　直斗	0	基盤サービス事業部第一ＩＴサービス部	三井E&Sシステム技研株式会社	null	null	\N	千葉県千葉市美浜区中瀬1-3幕張テクノガーデンD9	\N	\N	261-8501	30	2173	254244830977	524994771186	f	\N	2018-12-04 16:57:42.562672	\N	f	f	\N	\N	2	MSR	0	0	\N	\N
rsv_sunny_kyodonews3933@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	192	木原 泰志	0	ソリューショングループ	一般社団法人共同通信社	o+8KbTbwEHpeZXqAMBkzZrNmokgX4O5iadiwgeHrnH91iDpMrdBvJ0uorrM4OzlM	qEOgHXP6C6ajzAQI8GilBfMGRF8FLJeriJOnTre9	次長	東京都港区東新橋 1-7-1	汐留メディアタワー 6F	\N	105-7201	40	974	621671386233	621671386233	f	\N	2018-12-04 16:56:52.598792	sunnyview-621671386233	t	f	\N	\N	0	\N	0	0	\N	\N
lixin@lingtong.jp	\N	\N	2018-11-07 10:55:39.236947	307	根本 美紀	0		菱通ジャパン株式会社	null	null		東京都中央区新川1-22-11フジライト新川ビル2階	\N	\N	104-0033	30	2037	756780608695	984226601754	f	\N	2018-12-04 16:56:53.265149	\N	f	f	\N	984226601754	0	\N	0	0	\N	\N
seaos_tico_airas_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	408	山下 直宏	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2138	867584046595	430239111239	f	\N	2018-12-04 16:57:24.140647	\N	f	f	\N	430239111239	0	seaos_tico_airas_pro	0	0	\N	\N
rsv_sunny_nagano-cci@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	206	中嶋 佐和子	0	マリッジサポートセンター内	長野商工会議所	Kh1Hbo7z2EMYYAvwY7VDUiFT2ZOeILayxHkIHU2rzo47Ug3BBDUxX9fekbQabzbe	kB5PnmGmbvE1MgG+i3hcrF4ei4CkTvSPQMvQnuDu	\N	長野市七瀬中町276 3F	\N	\N	380-0904	30	988	103286371451	103286371451	f	\N	2018-12-04 16:57:01.917857	sunnyview-103286371451	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_seaos_tico_airas@ids.co.jp		\N	2018-11-01 12:17:49.963566	239	山下 直宏	0	\N	シーオス株式会社	ISAq5LKap4cX43H2eYspH1bget3+9jSHkarmXd4eHS2HnC015Si7grPKvgRIhlFr	B44RKSPFwVL0tIHOP+162tb0KNzKxHJHsApoXth3	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1021	430239111239	430239111239	f	\N	2018-12-04 16:57:24.142129	sunnyview-430239111239	t	f	\N	\N	2	\N	0	0	\N	\N
sunnycloud+616869527196@ids.co.jp		\N	2018-11-07 10:55:39.236947	440	山本 裕子	0	技術部	株式会社ソフテック	null	null	\N	東京都新宿区四谷2-8第7新菱ビル	\N	\N	160-0004	30	2170	616869527196	524994771186	f	\N	2018-12-04 16:57:42.527659	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunnyportal@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	271		0	対象外	自社	AXUpgp5wXFqu5pF2hX4b4uGs3/efAFvFVv0L55bYRcK43RzTrC5yNmTeQCczhCwb	mApGYv+s2oyNEIoXOMRHaT1eLCEjPr5U5gbd79T3		対象外	\N	\N	対象外	0	1053	358159549079	358159549079	t	\N	2018-12-04 16:57:43.239315	sunnyview-358159549079	t	f	\N	\N	0	\N	0	0	\N	\N
masahirom@it.secnet.co.jp	\N	\N	2018-11-07 10:55:39.236947	362	前野 剛宏	0	産業ソリューション事業部 システム開発部	株式会社エスイーシー	null	null	課長代理	北海道函館市末広町18-17	\N	\N	141-0022	30	2092	278931308604	\N	f	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+955896648533@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	360		-1	対象外	株式会社NDT	null	null		対象外	\N	\N	対象外	0	2090	955896648533	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
dev-sunny-201612@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	103		-1	\N	サニー開発用(2016年度12月）	Vrwk4P3VnYXB9J4/vjRg1aJydScWQpq3CHgS/rkUtDmdsG3c8DcN8t8rcFpCUMZ9	/InOOTYv6eVCmqoYdnT1IA1rDG4/7L6T+X8I6E4e	\N	対象外	\N	\N	対象外	0	885	166888973065	\N	t	\N	2018-11-01 12:17:49.963566	\N	t	t	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_ctcg@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	124	三木 真人	0	ICT技術統轄部	伊藤忠テクノソリューションズ株式会社	blWGg6FvgORF0MuADuZwrrCPi1AIntYe+pkMqsbHkrH5nU5aiofVK6q3OqGsyDyS	v/iuJiGIorCfKTxtLdiyrwl0rZ8zYrmedJXG8WKl	課長	東京都千代田区霞ヶ関3-2-5	霞ヶ関ビル	\N	100-6080	30	906	093704086207	093704086207	f	\N	2018-12-04 16:56:05.366065	sunnyview-093704086207	t	f	\N	\N	0	\N	0	0	\N	\N
manage+771088468311@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	349	ご担当者	0	営業統括本部営業本部セールスサポートメディア部	日立建機株式会社	null	null	\N	東京都台東区東上野二丁目１６番１号野イーストタワー１５階	\N	\N	110-0015	0	2079	771088468311	627722188647	t	\N	2018-12-11 15:54:18.504365	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_goof.aws@ids.co.jp		\N	2018-11-01 12:17:49.963566	141	高岡 博剛	0	\N	株式会社グーフ	9p3Ep06X6uoryc7bgiuR3BD3yjETatcbwRQApjTDtNUlG/QzW16zBMCycmPDxcbE	iB5QGBppIvGaMtgX3yUPwRff5Tjv7Ulh6kmZ5lqu	\N	東京都品川区大崎4-1-2	ウィン第2五反田ビル3F	\N	141-0032	30	923	130714452253	130714452253	f	\N	2018-12-04 16:56:17.405013	sunnyview-130714452253	t	f	\N	\N	1	\N	0	0	\N	\N
rsv_sunny_isid@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	170	吉川 哲弘	0	カスタマーソリューション部	株式会社電通国際情報サービス	H0ytC4AMqyTJC2kUJ6ncsEnuqZsWObNj3lPiK48XMLkN5Qqlw72YmZFVofeJxl3Z	vvJsIyO54b0NEYIxjYF79snhs957Im5yYgiy/Lf2	マネージャー	東京都港区港南2-17-1	\N	\N	108-0075	0	952	734349648527	734349648527	t	\N	2018-12-04 16:56:37.760072	sunnyview-734349648527	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_dts-insight@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	133	伊瀬 賢啓	0	第二システム事業部	株式会社DTSインサイト	iZyzsOoSeqGztzx1TpFv49oAYzxuCzxl68/PeUG753tHSwXYUhr25pNLREVsxQm0	mjMhqQSEZDeJfOr21WftATt60/2MszT7r3b9f5hO	\N	東京都新宿区下落合2-3-18	SKビル5F	\N	161-0033	30	915	579700001820	579700001820	f	\N	2018-12-04 16:56:11.740777	sunnyview-579700001820	t	f	\N	\N	0	\N	0	0	\N	\N
mitsuhiro.nakayama@looop.co.jp	\N	\N	2018-11-07 10:55:39.236947	366	秋田 周作	0		株式会社Looop	null	null		東京都台東区上野三丁目24番6号上野フロンティアタワー15階	\N	\N	110-0005	30	2096	582497690923	405273292958	f	\N	2018-12-04 16:56:54.574639	\N	f	f	\N	405273292958	0	looop	0	0	\N	\N
smartcloud@mazak.co.jp		\N	2018-11-01 12:17:49.963566	263	堀部 和也	0	技術本部ソリューション開発部	ヤマザキマザック株式会社	YXNjwAiVR/KFOSHCyGzDAkgI9u/75N3Bwo7UKLDYTqY=		部長	愛知県丹波郡大口町竹田1-131	\N	\N	480-0197	30	1045	728474942509	733844046660	f	\N	2018-12-04 16:57:26.807165		f	f	\N	196613940500	0	MSCAdmin	0	0	\N	\N
system-hakken-aws@his-world.com	\N	\N	2018-11-07 10:55:39.236947	464		0		株式会社エイチ・アイ・エス	null	null		対象外	\N	\N	対象外	0	2194	630803431035	822609446484	t	\N	2018-12-04 16:57:39.582806	\N	f	f	\N	\N	0	\N	0	0	\N	\N
system@yumeshin.co.jp		\N	2018-11-07 10:55:39.236947	463		0	対象外	株式会社 夢真ホールディングス	null	null	\N	対象外	\N	\N	対象外	0	2193	249606927133	\N	t	\N	2018-12-03 18:27:04.324157	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_appbase@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	112	澁木 太一	0	\N	株式会社AppBASE	gHhOn8HpUl6QFjv7U5s/FtjWzI+Orm8S+4X70pE0gjqvqCEjCU0ikcmeYXbdc1JB	ZGAzTkBUzmpJ37TWsR8ILMIq99wn4IBpfARh17Ew	\N	東京都江東区住吉1-17-20	住吉ビル8階	\N	135-0002	30	894	481654841523	481654841523	f	\N	2018-12-04 16:55:57.099034	sunnyview-481654841523	t	f	\N	\N	0	\N	0	0	\N	\N
it-dev+aschild@greenpacks.co.jp	\N	\N	2018-11-07 10:55:39.236947	303	山本 達也	0	IT企画サービス課	日本グリーンパックス株式会社	null	null		東京都中央区日本橋浜町3-26浜町京都ビル3F	\N	\N	103-0007	30	2033	535455052226	560570045510	f	\N	2018-12-04 16:56:22.284147	\N	f	f	\N	560570045510	0	NGP_Aschild	0	0	\N	\N
rsv_sunny_invesco@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	166		0	対象外	インベスコアセットマネジメント株式会社	OX5ns47+P0FFigf3VTYTcGzaEJuuVD995xqFfW3hgfZi40siSyX74DGeKTD+omqX	OMDenGz/Dt/GsWhRffcrZkgdbEPyKZu353ffT5lh		対象外	\N	\N	対象外	0	948	660600559702	660600559702	t	\N	2018-12-04 16:56:35.159976	sunnyview-660600559702	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_gsi@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	149	総務部長	0	官署支出官 国土地理院	国土地理院	15QmKGfKGyVXS2n3c+i32P9Mu8OvfUQlT1s+zwC8NznhRElELZCJpwY75hgwuu3j	yZsCeNYMWHdaWpTpQc1LfXIAxmJ8T9/Lz9bCV0zX		茨城県つくば市北郷1番	\N	\N	305-0811	30	931	669828113555	669828113555	f	\N	2018-12-04 16:56:22.992908	sunnyview-669828113555	t	f	\N	\N	0	\N	0	0	\N	\N
aiscloud_aws_hatc_dev@ml.jp.panasonic.com 	\N	\N	2018-11-07 10:55:39.236947	274	須山　統昭	0	AIS社インフォ（事）クラウド事業開発室	パナソニック株式会社	null	null		神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	2004	752403713436	972838442246	f	\N	2018-12-04 16:56:41.3011	\N	f	f	\N	972838442246	0	aiscloud_aws_hatc_dev@ml.jp.panasonic.com	0	0	\N	\N
rsv_sunny_kccs@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	180	FGLご担当者	0	ITインフラソリューション部ITIS5課 	京セラコミュニケーションシステム株式会社	ByYD3CM+ZSAXG4piKSPURM9gSnHqpr5OipjAdciawYp4E09F5ZbIDEVH0JBSRWT/	c37J5GF+51BU34NZ6MeqqJSSS5m+rkrcdb0pFSIV		東京都港区三田3-11-34センチュリー三田ビル	\N	\N	108-8605	30	962	649642544020	649642544020	f	\N	2018-12-04 16:56:44.626277	sunnyview-649642544020	t	f	\N	\N	0	\N	0	0	\N	\N
manage+800385523427@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	351		0	対象外	株式会社エイチ・アイ・エス	null	null		対象外	\N	\N	対象外	0	2081	800385523427	627722188647	t	\N	2018-12-11 15:54:18.535482	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+152414370820@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	373	伊澤 真奈美	0	\N	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル6F	\N	\N	164-0011	0	2103	152414370820	881814943668	t	\N	2018-12-04 16:56:00.549156	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+817085419763@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	353		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2083	817085419763	627722188647	t	\N	2018-12-11 15:54:18.476651	\N	f	f	0	\N	0	\N	0	0	\N	\N
nakano@ids.co.jp	03-5484-7811	0f38f7c3427e7ff3a73f57ab87e5646d	2018-01-23 12:39:58	355	ご担当者(社内利用)	0	対象外	中野さん利用	EsHHs3um7ZO2k+ooVTwXxyF2z7Uob0VZ2kTC/vyl9uU/9++caqEZl9Qk9LMONP3u	+dBG48Q45ctETcbc2WXM9PPHqoA0nA+CHVfP8MEG	\N	対象外	\N	\N	対象外	0	2085	877185579420	627722188647	t	\N	2018-12-11 15:54:18.512911	\N	f	f	0	\N	0	\N	0	0	\N	\N
garo_aws_staging@gali-int.com	\N	\N	2018-11-07 10:55:39.236947	292	加藤 裕稔	0		株式会社ガリインターナショナル	null	null		東京都中野区上高田3-1-3野田ビル201	\N	\N	164-0002	45	2022	951735216549	138865218164	f	\N	2018-12-04 16:56:16.706741	\N	f	f	\N	138865218164	0	Hirotoshi Kato GAROstaging	0	0	\N	\N
ids_ri_test_member@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	298		0		IDSRI用子アカウント（テスト用）	null	null		対象外	\N	\N	対象外	0	2028	635043033257	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunny_training@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	429		-1	対象外	開発部トレーニング用アカウント	null	null		対象外	\N	\N	対象外	0	2159	593596402331	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+18583534596@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	310		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2040	18583534596	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+288034687309@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	380	広瀬　和彦	0	モバイルコンテンツ部	株式会社ブシロード　停止	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル	\N	\N	164-0011	0	2110	288034687309	881814943668	t	\N	2018-12-04 16:56:00.56276	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_cb-inc@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	118	菊池 嘉裕	0	メディアストラテジー事業部	株式会社ZAIZEN	LRgH8VyjYUFopNPmXRwc8Mk1ELvgx6LLm6iDHRbxIjP19VrDIzYLnDV21QRuUG5n	pGmbn6sVE3fA9eNOR1KhcsJSM2q0LGfM+U9WZW4e	\N	東京都渋谷区恵比寿4-3-8	KDX恵比寿ビル3F	\N	150-0013	0	900	180335782935	180335782935	t	\N	2018-12-04 16:56:01.228932	sunnyview-180335782935	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_concier@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	122	石井 豪一	0	情報システム部	株式会社メディカル・コンシェルジュ	WFBHaK4/5gBFKlVN8/PZtyJVW1bhLCfZ8Tz4aaM3mtFplwAa+28728eT2aC6XJfI	fG8KQH3tbIce+ngdmVwTQuu/pzX+hP+WOwsHDkQ+	\N	東京都渋谷区恵比寿南1-5-5	JR恵比寿ビル11F	\N	150-0022	0	904	483519549373	483519549373	t	\N	2018-12-04 16:56:04.063188	sunnyview-483519549373	t	f	\N	\N	0	\N	0	0	\N	\N
sunny_ei@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	421		0	対象外	株式会社エイ出版社	null	null		対象外	\N	\N	対象外	0	2151	877731691785	685291271584	t	\N	2018-12-04 16:56:13.892882	\N	f	f	\N	685291271584	0	\N	0	0	\N	\N
rsv_sunny_e-aidma@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	134	阿部 世志夫	0	\N	株式会社アイドママーケティングコミュニケーション	DCO1MbD7qczrdWLIWtbt1yJmvk9tMntrS8IIU7KFhZmzw1DhSzogOV1VY0J8BXsm	0d6nqrEHknvJ/pMHry2/R4yOrScfAusvUeZPoDDE	\N	富山県富山市豊田町１丁目3番31号	\N	\N	931-8313	0	916	552009840040	552009840040	t	\N	2018-12-04 16:56:12.426598	sunnyview-552009840040	t	f	\N	\N	0	\N	0	0	\N	\N
it-dev+dev@greenpacks.co.jp	\N	\N	2018-11-07 10:55:39.236947	304	山本 達也	0	IT企画サービス課	日本グリーンパックス株式会社	null	null		東京都中央区日本橋浜町3-26浜町京都ビル3F	\N	\N	103-0007	30	2034	617814930481	560570045510	f	\N	2018-12-04 16:56:22.286028	\N	f	f	\N	560570045510	0	NGP_Dev	0	0	\N	\N
sunny_kusanagi@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	426	安達 文昭	0	サービス・イノベーション室 室長	ハウスコム株式会社	null	null		東京都港区港南2-16-1	\N	\N	108-0075	30	2156	813841827311	453696413027	f	\N	2018-12-04 16:56:28.825997	\N	f	f	\N	453696413027	0	\N	0	0	\N	\N
rsv_sunny_ihi@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	159		0	高度情報マネジメント統括本部基盤通信(東京) 	株式会社IHI	Xyk0UrpRi4R/dlFpHZCWKT+meywsZ1hityEWhvTMnkHZJfItgGv+qWR3A/p/iNq4	byQL09yGiUNZrnHQQWVSh3VjALRHalBIk50og/Ie		東京都江東区豊洲三丁目1-1豊洲IHIビル6階	\N	\N	135-8710	30	941	256576960204	256576960204	f	\N	2018-12-04 16:56:30.221486	sunnyview-256576960204	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_seikyuudaiko@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	242		0	対象外	請求代行用アカウント	WLFEv7VuN/yUvtWrR1mg3UHkEuUtaPtOvaYSmxeSYy+dkSFiHpUgHhCPCsnUAnJx	hMvdcewm9SVHLJVeJiA0UQvdZGXYo9NNIgL0AxjN		対象外	\N	\N	対象外	0	1024	750787724936	750787724936	t	\N	2018-12-04 16:57:26.149263	sunnyview-750787724936	t	f	\N	\N	0	\N	0	0	\N	\N
manage+097061654617@sunnycloud.jp		\N	2018-11-07 10:56:15.73026	475		0	\N	IDS_TGK_prj	\N	\N	\N	\N	\N	\N	\N	0	2205	097061654617	627722188647	t	PAPER	2018-12-11 15:54:18.475279	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_isehan@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	169	橋本 雅人	0	情報システム部	株式会社伊勢半本店	N9TrD1O61dGZ8/1RvxZqIhAA+NLBr+VB3Mt91uZbB7JIg/K3dsosB1mgeZublImE	f3J2ej7qgpGvXP/+Vy7BazSv14OV26E75WcPUKmg	主事	東京都千代田区四番町6-11ELFE四番町ビル	\N	\N	102-8370	60	951	176110895237	176110895237	f	\N	2018-12-04 16:56:37.099632	sunnyview-176110895237	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_salesone@ids.co.jp		\N	2018-11-01 12:17:49.963566	230	山本圭一	0	\N	セールスワン株式会社	cl1qdhaovDdHcp8kbQ3nEcJ/Pz66PpfJajDxbsoNz+3tA2X/c7ks2+JNECX50ZDi	w72jFkrw+2rUOWjlc4zrVDLNqnU9K1A79gjjCTWI	\N	東京都港区芝浦 3-14-5 5F	\N	\N	108-0023	0	1012	855779220738	855779220738	t	\N	2018-12-04 16:57:18.198536	sunnyview-855779220738	t	f	\N	\N	0	\N	0	0	\N	\N
manage+807643744651@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	352		0	対象外	IDS（Webグループ）？	null	null		対象外	\N	\N	対象外	0	2082	807643744651	627722188647	t	\N	2018-12-11 15:54:18.521466	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+541396569514@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	338	山田 美子	0	総合企画管理グループ	株式会社ヘルスケアリレイションズ	null	null		東京都調布市多摩川3-35-4	\N	\N	182-0025	30	2068	541396569514	627722188647	f	\N	2018-12-11 15:54:18.484819	\N	f	f	0	241214219430	0	hcrc-aws-i18n	0	0	\N	\N
aiscloud_aws_hatc_pdc@ml.jp.panasonic.com 	\N	\N	2018-11-07 10:55:39.236947	275	須山　統昭	0	クラウド事業開発室	パナソニック株式会社	null	null		神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	2005	319416533889	452507188348	f	\N	2018-12-04 16:56:41.961291	\N	f	f	\N	452507188348	0	aiscloud_aws_hatc_pdc@ml.jp.panasonic.com	0	0	\N	\N
g.oohori@looop.co.jp	\N	\N	2018-11-07 10:55:39.236947	290	秋田 周作	0		株式会社Looop	null	null		東京都台東区上野三丁目24番6号上野フロンティアタワー15階	\N	\N	110-0005	30	2020	573503400158	405273292958	f	\N	2018-12-04 16:56:54.57273	\N	f	f	\N	405273292958	0	admin	0	0	\N	\N
rsv_sunny_seaos_truckberth@ids.co.jp		\N	2018-11-01 12:17:49.963566	241	大和田 美雪	0	\N	シーオス株式会社	IjTipOCaBZ6WY2D4i5xgWM3yMcaPbnehIZM3DjZkyPCiInaibuEI4k3Tib31qTED	H3x/NNtL5qFQlY+YAXrHK4ndOQ6Wg957Kb3z6Rgk	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1023	886821596825	886821596825	f	\N	2018-12-04 16:57:25.49045	sunnyview-886821596825	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_welpark@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	258	坂本 崇弘	0	情報システム部	株式会社ウェルパーク	oHAggUUBH/U9yYZfAXN4bGONikRz8UNG+PlXQZsUqTAxtaMeimCW99fJPBp/1hy9	p6KCjORuJ7VWzhbisisa4sBmOvjAtI/LrdLrbnZt	マネージャー	東京都立川市栄町６-１-１	\N	\N	190-0003	30	1040	348344348129	348344348129	f	\N	2018-12-04 16:57:36.785311	sunnyview-348344348129	t	f	\N	\N	0	\N	0	0	\N	\N
sunny_seikei@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	267	松尾　雄治	0	図書館事務室 書記	学校法人成蹊学園	v/+zQolWX8O9W+t9G2EfrumGyy5XMmLxSyGEhjp7lAiFBw9PuQLQMBWvJaajZ0Eb	7JGrqSOwavBdzOAulkV76+umXQqpLMHysT0e7inA		東京都武蔵野市 吉祥寺北町3-3-1	\N	\N	180-8633	30	1049	697871290880	697871290880	f	\N	2018-12-04 16:57:41.794337	sunnyview-697871290880	t	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+523847113870@ids.co.jp		\N	2018-11-07 10:55:39.236947	436	加藤　重美	0	人事総務部	アネックス株式会社	null	null	\N	東京都目黒区下目黒3-7-2小西ビル6F	\N	\N	153-0064	30	2166	523847113870	524994771186	f	\N	2018-12-04 16:57:42.5102	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+362901815791@ids.co.jp		\N	2018-11-07 10:55:39.236947	442	木下 壮太	0	経営管理部	株式会社ライフステージ	null	null	係長	大阪市淀川区西中島5-5-15新大阪セントラルタワー10F	\N	\N	532-0011	30	2172	362901815791	524994771186	f	\N	2018-12-04 16:57:42.514134	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+457498299407@ids.co.jp		\N	2018-11-07 10:55:39.236947	431	渡邊 哲雄	0	東京支店	三誠株式会社	null	null	常務取締役	東京都杉並区荻窪5-15-22	HOLD荻窪ビル3F	\N	167-0051	30	2161	457498299407	524994771186	f	\N	2018-12-04 16:57:42.518184	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_informetis@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	162	宮澤	0	\N	インフォメティス株式会社	ANOv5aPZ4XGZLqWpfaSjkoTT+8RWPwESt1yP3G8v7qyri+Xq47adOrNX4AzH4j5Z	/snK0D2bFoFuLzk8dpDeXrv3qdvzZcSuT8OkjYif	\N	東京都港区高輪1-5-4	常和高輪ビル 3F	\N	108-0074	0	944	429250571507	429250571507	t	\N	2018-12-04 16:56:32.426234	sunnyview-429250571507	t	f	\N	\N	0	\N	0	0	\N	\N
aws@ml.kel.co.jp	\N	\N	2018-11-07 10:55:39.236947	282		0	対象外	兼松エレクトロニクス株式会社	null	null		対象外	\N	\N	対象外	30	2012	706063074686	329486351300	f	\N	2018-12-04 16:56:48.626815	\N	f	f	\N	329486351300	0	\N	0	0	\N	\N
dg-aws@mxmobiling.com		\N	2018-11-07 10:55:39.236947	288	若林 優一	0	スマートサービス事業本部サービス開発事業部	MXモバイリング株式会社	null	null	\N	東京都江東区豊洲 3-2-24豊洲フォレシア 15F	\N	\N	135-0061	30	2018	597543915380	704308844416	f	\N	2018-12-04 16:57:00.538389	\N	f	f	\N	704308844416	0	\N	0	0	\N	\N
rsv_sunny_lingtong@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	193	根本 美紀	0	\N	菱通ジャパン株式会社	Skj2V05E4MIVzPVBpoNPaYmQkXjFEvFRE4134wVAe6HUeNZdzZmbEVZEz57zJ9KY	JV/QzxubiCztkhJFMVkYcL7kS80tDqaOWCNcXKsM	\N	東京都中央区新川1-22-11	フジライト新川ビル2階	\N	104-0033	30	975	984226601754	984226601754	f	\N	2018-12-04 16:56:53.266545	sunnyview-984226601754	t	f	\N	\N	1	\N	0	0	\N	\N
seaos_nichirei_quent_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	398	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2128	592370397268	323472269056	f	\N	2018-12-04 16:57:03.250431	\N	f	f	\N	323472269056	0	seaos_nichirei_quent_dev	0	0	\N	\N
owh_idtr_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	368	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2098	124407189607	347855887727	f	\N	2018-12-04 16:57:22.11904	\N	f	f	\N	347855887727	0	\N	0	0	\N	\N
rsv_sunny_nomura-g@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	213	水内 義男	0	経営システム室	野村ユニソン株式会社	a/ofmgntBjxX3WVajQJy3aahKKepc6qWjI9vDMn0Uc2wGQE+RyxVS+gkQ2h93zWD	ExL4zx+fE8YMqVOVLQZQuks9CdlFnHG+TOBq9qJf	室長	長野県茅野市ちの650番地	\N	\N	391-0001	30	995	572131055370	572131055370	f	\N	2018-12-04 16:57:06.827808	sunnyview-572131055370	t	f	\N	\N	0	\N	0	0	\N	\N
seimani-sv2@tekunodo.jp	\N	\N	2018-11-07 10:55:39.236947	417	鎌田 寛昭	0		株式会社テクノード	null	null	代表取締役	東京都目黒区上目黒2-15-14AKビル4F 5F	\N	\N	153-0051	30	2147	381671519003	765150201305	f	\N	2018-12-04 16:57:29.507727	\N	f	f	\N	765150201305	0	HikaruMiwa	0	0	\N	\N
rsv_sunny_pp.update-tokyo@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	219	砂川 武貴	0	経済学研究科	国立大学法人 神戸大学	E60e5joeDjIL+tCyLCG1S46WWmzaVByxeOnmP+Pk594whSCLJK4sgGhzGXmEBRrn	DhEfCECN3jCpGAG81PWo+SeqdgY5XgnH753Tm2FV	会計係	兵庫県神戸市灘区六甲台町2-1	\N	\N	657-8501	0	1001	997246758518	997246758518	t	\N	2018-12-04 16:57:10.860952	sunnyview-997246758518	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_kcme3@ids.co.jp		\N	2018-11-01 12:17:49.963566	183	中島 早紀	0	事業推進部	KCCSモバイルエンジニアリング株式会社	Adz/AFjr62d9F4qNGJpHeJycJGHzictyeKjFdzH48rcCm9v3vupHt9vUqcsG52JY	YO+dQMF1CRgmh0rJ1cNmOkGAEZsWm2UnvzW4SSQw	\N	東京都港区三田3-13-16	三田43MTビル16階	\N	108-0073	50	965	738710330901	738710330901	f	\N	2018-12-04 16:56:46.642829	sunnyview-738710330901	t	f	\N	\N	0	\N	0	0	\N	\N
manage+73407108473@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	314	鎌田 寛昭	0		株式会社テクノード	null	null	代表取締役	東京都目黒区上目黒2-15-14AKビル4F 5F	\N	\N	153-0051	30	2044	073407108473	627722188647	f	\N	2018-12-11 15:54:18.53687	\N	f	f	0	765150201305	0	fengshen	0	0	\N	\N
rsv_sunny_milleporte@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	200	岩見正潤	0		株式会社B4F	nfHzoqxO3kCG1eLJzOiW6i/CxP5ldQHlYYxAR94ZaSd924IZLMr41pSc1OQEdhbY	t/joZnpHeSw1cvMwCCY9sDoPKkSZ55x1oaWUUTii		東京都渋谷区富ヶ谷1-13-9	\N	\N	151-0063	30	982	108370854378	108370854378	f	\N	2018-12-04 16:56:57.898177	sunnyview-108370854378	t	f	\N	\N	0	\N	0	0	\N	\N
manage+455931256641@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	335		0	対象外	日酸TANAKA？	null	null		対象外	\N	\N	対象外	0	2065	455931256641	627722188647	t	\N	2018-12-11 15:54:18.534073	\N	f	f	0	\N	0	\N	0	0	\N	\N
master@raku-den.ne	\N	\N	2018-11-07 10:55:39.236947	363		0	対象外	株式会社楽電	null	null		対象外	\N	\N	対象外	0	2093	431139014062	749434594113	t	\N	2018-12-04 16:57:13.535833	\N	f	f	\N	749434594113	0	\N	0	0	\N	\N
seaos_saaswms_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	401	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2131	727090432173	647397808689	f	\N	2018-12-04 16:57:16.174109	\N	f	f	\N	647397808689	0	seaos_saaswms_dev@seaos.jp	0	0	\N	\N
rsv_sunny_it.secnet@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	171	前野 剛宏	0	産業ソリューション事業部 システム開発部	株式会社エスイーシー	1VZCOtSN7wvoB5gF+tV+6BJn+WTHfK3xGk30jOL3oxKN6XTKOopisAHPy9zfxQFx	5X0IeIcrDhMGFzSBj+g6mZvPxgu+vLtasVYpq1RP	課長代理	北海道函館市末広町18-17	\N	\N	141-0022	30	953	484821414153	484821414153	f	\N	2018-12-04 16:56:38.56773	sunnyview-484821414153	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_multisoup.aws+389721610116@ids.co.jp		\N	2018-11-01 12:17:49.963566	203	吉野　智則	0	テクニカルGr	マルティスープ株式会社	trHcjEPkNaUGIGvHe8gsNh7r9OmEVWNIpbK0Qq6V70cGP7PGRGXnnmidAc+jUQjJ	2qIuJcUGtD0q+l9lgup+jjVpzIYw9S0ho8RkQf43	\N	東京都千代田区神田錦町3-11	精興竹橋共同ビル3F	\N	101-0054	30	985	389721610116	389721610116	f	\N	2018-12-04 16:56:59.891813	sunnyview-389721610116	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_aiscloud_msil_pdc@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	110	須山 統昭	0	AIS社インフォ（事）クラウド事業開発室	パナソニック株式会社	WByv9Fht2UvpyQjjzkxVYl1nKpqfYSdrbPB5YY/fQwvecZsi2gJb6yXSiTlzTnKP	659YmpKPVYUf1QIXMr8BYcWQk+8Jm9o56AsCmxBz	主幹	横浜市都筑区池辺町4261番地	\N	\N	224-8520	0	892	257968528965	257968528965	t	\N	2018-12-04 16:55:55.723717	sunnyview-257968528965	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_ajis-group@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	111		0	対象外	株式会社エイジス	kOKniS3IUs8sKiEQoV+07r9leTpb67YEWrMP3G01yJs8KWHxKKkHQlgYJHvVwO/N	6OQxYOosQxSdoDDWoJvW2DOO1alr/9QcvDlJM7EQ	\N	対象外	\N	\N	262-0032	0	893	844457253870	844457253870	t	\N	2018-12-04 16:55:56.43911	sunnyview-844457253870	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_peacs@ids.co.jp		\N	2018-11-01 12:17:49.963566	217	矢野 充宏	0	デジタルコミュニケーションセンター	ピークス株式会社	bl7GOwzuzapvzKUzRdEgz9VViHfo5mpxG89ux3JtUrwQvck8JPbWMC2GMZ8+ZHiW	waR9BRGswkIqgcVFCGC2uekGGzauAOSA9nnK2hWN	\N	東京都世田谷区玉川台2-13-2玉川台ビル	\N	\N	158-0096	0	999	469287071569	469287071569	t	\N	2018-12-04 16:57:09.531202	sunnyview-469287071569	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_truckberth_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	413	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2143	702616106259	886821596825	f	\N	2018-12-04 16:57:25.487186	\N	f	f	\N	886821596825	0	seaos_truckberth_dev	0	0	\N	\N
rsv_sunny_seaos_product@ids.co.jp		\N	2018-11-01 12:17:49.963566	237	小川 弥蓉	0	管理本部	シーオス株式会社	kh2WQyzuPkODqeM7XwMgtA8vaDUWYHojBbE1/E5pxm18D+MYpIJtv7VlMmgsokYV	dWYl2RKY1c3nWqkM7DIAqlY6PRB6Sms4W6BXwy/8	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1019	741039434071	741039434071	f	\N	2018-12-04 16:57:22.774539	sunnyview-741039434071	t	f	\N	\N	1	\N	0	0	\N	\N
toyobody@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	470	若林 優一 	0	スマートサービス事業本部サービス開発事業部	株式会社東洋ボデー	null	null		東京都江東区豊洲 3-2-24豊洲フォレシア 15F	\N	\N	135-0061	30	2200	218307468360	880135449318	f	\N	2018-12-04 16:57:34.106489	\N	f	f	\N	880135449318	0	\N	0	0	\N	\N
dbs-aws-infra@osk.toppan.co.jp		\N	2018-11-07 10:55:39.236947	286	金本 博隆	0	メディア事業推進本部	凸版印刷株式会社	null	null	主任	大阪市北区中之島 2-3-18	\N	\N	530-0005	60	2016	177923562451	018106178297	f	\N	2018-12-04 16:57:33.464371	\N	f	f	\N	018106178297	1	\N	0	0	\N	\N
rsv_sunny_toppan03@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	253	金本 博隆	0	メディア事業推進本部	凸版印刷株式会社	nDaEcucGqzN/fyUtn5PScErtOAvcjVAeuRA5E/cBOqebrHjHXyyBLgU4R598eihy	6RR/mA2+rxdwnwv7xUp6+WEXqbzYbJ2Wq1+J2lBo	主任	大阪市北区中之島2-3-18	中之島フェスティバルタワー	\N	530-0005	60	1035	018106178297	018106178297	f	\N	2018-12-04 16:57:33.467965	toppan03-billingreport	t	f	\N	\N	1	\N	0	0	\N	\N
solasto_hosp002@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	266		0	対象外	株式会社ソラスト	MkGlxI1U2iHRYT4PQe+zwMpsoMH+lrhRp6u8rmDNcwMxpmPAXX29ObfO6mjn0Wuy	CLhWOR4xRMBuJDsC1j/L9wZpi18cNTV76bf0p1Gj	\N	対象外	\N	\N	対象外	0	1048	127775157083	127775157083	t	\N	2018-12-04 16:57:41.13943	sunnyview-127775157083	t	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+830620035779@ids.co.jp		\N	2018-11-07 10:55:39.236947	451	小川 倫	0	創造工学部 社会シミュレーション室	株式会社構造計画研究所	null	null	\N	東京都中野区中央4-5-3	\N	\N	164-0011	30	2181	830620035779	524994771186	f	\N	2018-12-04 16:57:42.522105	\N	f	f	\N	\N	0	\N	0	0	\N	\N
digicatat@gmail.com	\N	\N	2018-11-07 10:55:39.236947	289	片松 祐司	0	メディア事業推進本部電子ｶﾀﾛｸﾞ事業推進部	凸版印刷株式会社	null	null	主任	東京都文京区水道1-3-3	\N	\N	112-8531	60	2019	971866892655	018106178297	f	\N	2018-12-04 16:57:33.466332	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_nitech1788@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	211		0	しくみ領域伊藤孝行研究室	国立大学法人名古屋工業大学	psk8DQIy8gacbNC8eYFBOf/y5vKCWVqwOt2Qz7jJFtZRjI0hb7krYwSV8qRFaD6Y	kONhyMxTQKdlKeXmGPZ5sVPTb5JuJe+2bRGxfMT4		愛知県名古屋市昭和区御器所町（4号館702室）	\N	\N	466-8555	60	993	785758689215	785758689215	f	\N	2018-12-04 16:57:05.500743	sunnyview-785758689215	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_truckberth_stg@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	415	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2145	828241633006	886821596825	f	\N	2018-12-04 16:57:25.484982	\N	f	f	\N	886821596825	0	seaos_truckberth_stg	0	0	\N	\N
manage+342692132653@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	328		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2058	342692132653	627722188647	t	\N	2018-12-11 15:54:18.54374	\N	f	f	0	\N	0	\N	0	0	\N	\N
osamu_ikemoto@mazak.co.jp	\N	\N	2018-11-07 10:55:39.236947	367	堀部 和也	0	技術本部ソリューション開発部	ヤマザキマザック株式会社	null	null	部長	愛知県丹波郡大口町竹田1-131	\N	\N	480-0197	30	2097	768992151524	196613940500	f	\N	2018-12-04 16:56:56.5582	\N	f	f	\N	196613940500	0	MAZAK_AWS_ACCOUNT	0	0	\N	\N
rsv_sunny_imperialhotel@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	160	八木　研吏	0	ホテル事業統括部	株式会社帝国ホテル	8WRhuAE1P33kT6C+4hkAoTkn/f9H1M9g8bzOw6pNhQ6fBQK3aoNk5p4wlXqkKvVr	IZlnMHZwSREWwqcjzGpSX8uwXQmKT3XtNtwRfX2u	支配人	東京都千代田区内幸町1-1-1	\N	\N	100-8558	0	942	175471411341	175471411341	t	\N	2018-12-04 16:56:30.915046	sunnyview-175471411341	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_informetis_04@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	165	宮澤	0	\N	インフォメティス株式会社	VmtG/pdGwqjYzHSYslvlVdcmFntRBGN+NK2qiiXt4ULHpreGYZKpVhPkWSsfSlFF	aMqWXffzLgYW5ikVh4FCaH6IQakILciiuQFc5oWA	\N	東京都港区高輪1-5-4常和高輪ビル 3F	\N	\N	108-0074	0	947	656171579609	656171579609	t	\N	2018-12-04 16:56:34.47742	sunnyview-656171579609	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_fsx@ids.co.jp	\N	\N	2018-11-01 12:17:49.963566	138	秋葉　勝	0		FSX株式会社	raMs0d9N3xML4NGiuOhy51nvQupfxgkeuIf0eoo6ZhmguyZ4E7JcR/utZpb9svy5	AGIB6vr8ijUAEKw5TJuvOjfTEGzGLno47QybZiPA		東京都国立市泉1-12-3	\N	\N	186-0012	30	920	707868645401	707868645401	f	\N	2018-12-04 16:56:15.297736	sunnyview-707868645401	t	f	\N	\N	0	\N	0	0	\N	\N
sunny_quantos@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	428		0	対象外	株式会社NDT	null	null		対象外	\N	\N	対象外	0	2158	955896648533	627722188647	t	\N	2018-12-11 15:54:18.47398	\N	f	f	0	\N	0	\N	0	0	\N	\N
sunny_code_manage@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	420		0	対象外	サニーコード管理用アカウント	null	null		対象外	\N	\N	対象外	0	2150	192874593917	627722188647	t	\N	2018-12-11 15:54:18.49344	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+706266061586@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	347	中島 早紀	0	事業推進部	KCCSモバイルエンジニアリング株式会社	null	null	\N	東京都港区三田3-13-16	三田43MTビル16階	\N	108-0073	50	2077	706266061586	627722188647	f	\N	2018-12-11 15:54:18.514266	\N	f	f	0	\N	0	\N	0	0	\N	\N
well-corp@goof.buzz		\N	2018-11-07 10:55:39.236947	471	近藤 寛一	0	経営企画	株式会社グーフ	null	null	COO	東京都品川区大崎4-1-2	\N	\N	141-0032	30	2201	861408576636	472370278951	f	\N	2018-12-04 16:56:18.030354	\N	f	f	\N	472370278951	4	※(株)ウイル・コーポレーション様用アカウント分  AWS ID:861408576636	0	0	\N	\N
info+housecom@diamondmedia.co.jp	\N	\N	2018-11-07 10:55:39.236947	299	安達 文昭	0	サービス・イノベーション室 室長	ハウスコム株式会社	null	null		東京都港区港南2-16-1	\N	\N	108-0075	30	2029	936890233926	453696413027	f	\N	2018-12-04 16:56:28.824022	\N	f	f	\N	453696413027	0	\N	0	0	\N	\N
seimani-sv3@tekunodo.jp	\N	\N	2018-11-07 10:55:39.236947	418	鎌田 寛昭	0		株式会社テクノード	null	null	代表取締役	東京都目黒区上目黒2-15-14AKビル4F 5F	\N	\N	153-0051	30	2148	747481720127	765150201305	f	\N	2018-12-04 16:57:29.509707	\N	f	f	\N	765150201305	0	HikaruMiwa	0	0	\N	\N
rsv_sunny_kcme4@ids.co.jp		\N	2018-11-01 12:17:49.963566	184	宮城 吉彦	0	新規ビジネス開発部	KCCSモバイルエンジニアリング株式会社	oliBw4csviO6MwGt+bFM/rKl43LqvDAQdMrltTuKjVcWUv6KoKxAzSRwOserzwvE	YQQABwcywv6w6MCbtbf4o9lXTQ9jPX9I31g2QTsz	\N	東京都港区三田3-13-16	三田43MTビル16階	\N	108-0073	50	966	732615154809	732615154809	f	\N	2018-12-04 16:56:47.283655	sunnyview-732615154809	t	f	\N	\N	0	\N	0	0	\N	\N
manage+526634979028@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	336		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2066	526634979028	627722188647	t	\N	2018-12-11 15:54:18.489397	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_toyoko-inn@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	255	堀江 裕美	0	管理部	株式会社東横イン IT集客ソリューション	Ji81oFj4uAp71PzXgcSH1iO3h3B7FF6gzvdMFUzrbVQGQnhVl9zO4j+hxbu1y8s5	xdD/AcmheFbGCNzpVplPpP844HF9Qaj5jaC4dbo+	部長補佐	東京都大田区新蒲田1-7-4	\N	\N	144-0054	30	1037	724861742662	724861742662	f	\N	2018-12-04 16:57:34.773605	sunnyview-724861742662	t	f	\N	\N	2	\N	0	0	\N	\N
system-kokunai-land-proto-aws@his-world.com	\N	\N	2018-11-07 10:55:39.236947	466		0		株式会社エイチ・アイ・エス	null	null		対象外	\N	\N	対象外	0	2196	416895888347	822609446484	t	\N	2018-12-04 16:57:39.58059	\N	f	f	\N	\N	0	\N	0	0	\N	\N
mcse_awsws@concier.com	\N	\N	2018-11-07 10:55:39.236947	365	石井 豪一	0	情報システム部	株式会社メディカル・コンシェルジュ	null	null		東京都渋谷区恵比寿南1-5-5JR恵比寿ビル11F	\N	\N	150-0022	30	2095	103831371886	483519549373	f	\N	2018-12-04 16:56:04.05789	\N	f	f	\N	483519549373	0	\N	0	0	\N	\N
rsv_sunny_conffort_dev@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	385		0	対象外	コンフォート株式会社？	null	null		対象外	\N	\N	対象外	0	2115	299051606414	664904911309	t	\N	2018-12-04 16:56:04.707504	\N	f	f	\N	664904911309	0	\N	0	0	\N	\N
solasto_hosp001@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	265		0	対象外	株式会社ソラスト	z/tLM/xiVqxt+yp1QJuXDdG/ME7HW4zVLhn0jJ5/FsRmdUuhki7/hmTcFM596OjH	S2AFwQv1gsm5iJc066aOXYC4KifORbG6CvLtdYMi	\N	対象外	\N	\N	対象外	0	1047	070262509459	070262509459	t	\N	2018-12-04 16:57:40.232928	sunnyview-070262509459	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_smartcloud_mazak@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	243	堀部 和也	0	技術本部ソリューション開発部	ヤマザキマザック株式会社	Dt6qzT1ihTXRN4KoBLybq5Jg+JKvVOOj376cybOFpRN6Z6k3whFHlxCNKEJ0BMvk	kUdStCxM7HEj2jTcEIEgVTsaTRbPDbncUphUXZ6+	部長	愛知県丹波郡大口町竹田1-131	\N	\N	480-0197	30	1025	733844046660	733844046660	f	\N	2018-12-04 16:57:26.808657	sunnyview-733844046660	t	f	\N	196613940500	0	\N	0	0	\N	\N
seaos_owh_system_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	399	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2129	370046040580	347855887727	f	\N	2018-12-04 16:57:22.117139	\N	f	f	\N	347855887727	0	\N	0	0	\N	\N
rsv_sunny_mazak@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	198	堀部 和也	0	技術本部ソリューション開発部	ヤマザキマザック株式会社	3MX2kchAUpthWrSA/ZLLtAqlC9jggMdBvSSShntOivTlkctDS3PTj6/v+sIr/A+d	k0wUKeAAuQYErSFSDj2o2IE968ybGkR3qoI1agv6	部長	愛知県丹波郡大口町竹田1-131	\N	\N	480-0197	30	980	196613940500	196613940500	f	\N	2018-12-04 16:56:56.561622	sunnyview-196613940500	t	f	\N	\N	2	\N	0	0	\N	\N
infra@nabiq.co.jp	\N	\N	2018-11-07 10:55:39.236947	300	大川 宏	0		株式会社ナビック	null	null	美術本部長	東京都千代田区神田紺屋町11岩田ビル 5F	\N	\N	101-0035	30	2030	283791106687	414456009824	f	\N	2018-12-04 16:57:01.225284	\N	f	f	\N	414456009824	0	\N	0	0	\N	\N
seaos_ticohino_smlg_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	411	山下直宏	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2141	573645663769	511061991579	f	\N	2018-12-04 16:57:24.803461	\N	f	f	\N	511061991579	0	seaos_ticohino_smlg_pro	0	0	\N	\N
rsv_sunny_opensite@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	214	滝川 恵美	0	\N	株式会社オープンサイト	oHn1KEXZjsY7ajib4XE+GTOSO5NV1Pa1yFVr0ayFCE9uJbVpXhkbmjamVH8GXicW	gKxm6t/u0xRRz5HDum5MzjgxIuU3b7Nc0S0q6iWn	\N	愛知県名古屋市中村区名駅2-38-2	オーキッドビル6F	\N	450-0002	30	996	253881833417	253881833417	f	\N	2018-12-04 16:57:07.498115	sunnyview-253881833417	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_plmj@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	218	檜山 匡俊	0	営業本部 第1営業部	株式会社PLMジャパン	cRAqz47peoVct97ke77gqcgjgnhFvBmy2j1hf2o2410GmQowfKprLxW7m6kQcRp0	tpJEsXUHUglomthUbcYWDqxGRBDzUb2I0gGhL1Vd	部長	東京都港区西新橋1-2-9日比谷セントラルビル 5F	\N	\N	105-0003	0	1000	055877088746	055877088746	t	\N	2018-12-04 16:57:10.192903	sunnyview-055877088746	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_quicktranslate@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	220	鳥原 剛	0	\N	スピード翻訳株式会社	9BZ7L2pOxQ8cPBQXGPOahbjiOxIJGqjuL9NnYPo+kXKuKPNVnKkLpvAzQB6SBBqi	oLnTprdZ6HkG1cjMvq+SlDFHRllAlEUpTYakfKJw	\N	東京都千代区神保町3-7-1 3F	\N	\N	101-0051	30	1002	174552428163	174552428163	f	\N	2018-12-04 16:57:11.558737	sunnyview-174552428163	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_dnp_tmssystem_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	394	大和田 美雪	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2124	336257810386	879594214584	f	\N	2018-12-04 16:57:20.115547	\N	f	f	\N	879594214584	0	\N	0	0	\N	\N
rsv_sunny_data-service@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	130	成田 智之	0	第2ソリューション事業部	株式会社データサービス	lqgOtgZj+MNW1ZYcrHYsqaYns14rafb4IIT0YI/fkJU/Vcd+deo8//D5GVpmaIoj	mFFSUSxjI6pk6HVYuSOa6ogSGvYk/fD4UbfyOUcj	\N	東京都新宿区西新宿3丁目20番2号東京オペラシティタワー39階	\N	\N	163-1439	30	912	158012712342	158012712342	f	\N	2018-12-04 16:56:09.560948	sunnyview-158012712342	t	f	\N	\N	1	\N	0	0	\N	\N
lpsvr_mgmt@looop.co.jp	\N	\N	2018-11-07 10:55:39.236947	308	秋田 周作	0		株式会社Looop	null	null		東京都台東区上野三丁目24番6号上野フロンティアタワー15階	\N	\N	110-0005	30	2038	468876663973	405273292958	f	\N	2018-12-04 16:56:54.576675	\N	f	f	\N	405273292958	0	Account ID: 468876663973	0	0	\N	\N
seimani-sv@tekunodo.jp	\N	\N	2018-11-07 10:55:39.236947	416	鎌田 寛昭	0		株式会社テクノード	null	null	代表取締役	東京都目黒区上目黒2-15-14AKビル4F 5F	\N	\N	153-0051	30	2146	246959116207	765150201305	f	\N	2018-12-04 16:57:29.513406	\N	f	f	\N	765150201305	0	HiroakiKamata	0	0	\N	\N
rsv_sunny_seaos_mtq_quent@ids.co.jp		\N	2018-11-01 12:17:49.963566	235	小川 弥蓉	0	管理本部	シーオス株式会社	f7gJ7Wtvpz3Ts6GbJDzv1DuP24mD+9lE/PYFEsFxUA7v2d6Oht6rJm7JHVPKLuC/	gjDODDFsHwddEWOC3m+1WUL0WjBj8Tu2shXB0htv	\N	東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	1017	121215650074	121215650074	f	\N	2018-12-04 16:57:21.483793	billingreport-mtq-quent	t	f	\N	\N	2	\N	0	0	\N	\N
manage+052378875952@sunnycloud.jp		\N	2018-11-07 10:56:15.81333	478		0	\N	IDS_EC_TTP	\N	\N	\N	\N	\N	\N	\N	0	2208	052378875952	627722188647	t	PAPER	2018-12-11 15:54:18.538152	\N	f	f	0	\N	0	\N	0	0	\N	\N
oversea_sunny@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	107		0	対象外	自社：海外送金用のアカウント	4FvAEUcb1Ea4daysq2GM/UIEk3Y2JC3OUZTWBHOHqrcSN393Xi0OAN3tRX1Q7K8T	VaGoe9WT0kHjlr4cCHnSt3AI+RTlNW3j1ofvMscS		対象外	\N	\N	対象外	0	889	933367895419	933367895419	t	\N	2018-12-10 16:29:28.696508	sunnyview-933367895419	t	f	0	\N	0	\N	0	0	\N	\N
sunny_guia@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	422		0	対象外	株式会社NDT	null	null		対象外	\N	\N	対象外	0	2152	378263640644	627722188647	t	\N	2018-12-11 15:54:18.540805	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_granzella@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	147	吉田 佳幸	0		株式会社グランゼーラ・オンラインエンターテインメント	78tgs6enaBtNta1iCnk3bT1aVDtq3EMPpOkwseHsP0KP9BzjIb+paizmb/bbfFCw	xW1gNSQoBr8QeACVwZ3xup0IBbNre55YTR1F7Tzk	代表取締役社長	石川県野々市市末松3丁目570	\N	\N	921-8836	30	929	017007754427	017007754427	f	\N	2018-12-04 16:56:21.520726	sunnyview-017007754427	t	f	\N	\N	0	\N	0	0	\N	\N
manage+113251977603@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	316		-1	対象外	インベスコ・アセット・マネジメント株式会社？	null	null		対象外	\N	\N	対象外	0	2046	113251977603	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
kana_shin16@yahoo.co.jp	03-6365-6126	eIe1R23ckwXvP3y91p9fCse8zFvOSK2Y	2018-11-08 14:56:05.622777	591	Kodera	0		IDS	\N	\N		\N	\N	\N	\N	\N	2321	\N	\N	f	\N	\N	\N	f	f	\N	\N	0	\N	0	0	\N	\N
kodera@ids.co.jp	03-6365-6126	hXXkSMogIY4ZDn4cOTYozMzQl3rwc9gU	2018-11-07 18:04:19.346701	590	小寺加奈子	-1		IDS	\N	\N		\N	\N	\N	\N	\N	2320	\N	\N	f	\N	\N	\N	f	f	\N	\N	0	\N	0	0	\N	\N
s_josys@isehan.co.jp	\N	\N	2018-11-07 10:55:39.236947	393		-1	対象外	株式会社伊勢半本店	null	null		対象外	\N	\N	対象外	0	2123	47501293401	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+804845655026@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	371	広瀬　和彦	0	モバイルコンテンツ部	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル	\N	\N	164-0011	0	2101	804845655026	881814943668	t	\N	2018-12-04 16:56:00.553129	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+932396886780@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	358		0	対象外	日商エレクトロニクス株式会社	null	null	\N	対象外	\N	\N	対象外	0	2088	932396886780	627722188647	t	\N	2018-12-11 15:54:18.496092	\N	f	f	0	\N	0	\N	0	0	\N	\N
carbon@itaccess.co.jp		\N	2018-11-07 10:55:39.236947	284		0	対象外	アイティアクセス株式会社	null	null	\N	対象外	\N	\N	対象外	30	2014	683263343141	516801775842	f	\N	2018-12-04 16:56:39.319572	\N	f	f	\N	516801775842	0	\N	0	0	\N	\N
rsv_sunny_fcl@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	137	高橋 知久	0	事業管理部 システム＆サポートグループ	株式会社フジイ	3ftagPe6/OCYwO3uIAxIHZcBHfHsSNXbo9sej+sDZnuzg4xwVQ+YBG7miwXiAsR9	pfk81t2TT5wvJgR7z0YErHV1fthhF+ULVa/a3o1F	\N	東京都文京区白山1-33-18	\N	\N	113-0001	30	919	071641251336	071641251336	f	\N	2018-12-04 16:56:14.619801	sunnyview-071641251336	t	f	\N	\N	0	\N	0	0	\N	\N
sunny_kobunsha_test@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	425	畠山　雄一	0	総務局情報システム部	光文社 検証アカウント	null	null		東京都文京区音羽1-16-6	\N	\N	112-8011	30	2155	588662383565	134067479084	f	\N	2018-12-04 16:56:49.961929	\N	f	f	\N	134067479084	0	\N	0	0	\N	\N
aws_analysis@looop.co.jp		\N	2018-11-07 10:55:39.236947	283	秋田 周作	0	\N	株式会社Looop	null	null	\N	東京都台東区上野三丁目24番6号	上野フロンティアタワー15階	\N	110-0005	30	2013	826403339291	405273292958	f	\N	2018-12-04 16:56:54.578787	\N	f	f	\N	\N	3	Looop_analysis	0	0	\N	\N
rsv_sunny_todai_sekimotoken@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	248	ご担当者	0	生産技術研究所Ce507	東京大学	DrVlJj0eFbfWMP8ijG1RSIDx23BQxY8Moj2BojCeJ7u4sI5rhUE5oiMViaFx8jcJ	126/wujmf0351OnYwZir259Axda2DYhs+t4yrc7S	\N	東京都目黒区駒場4-6-1	\N	\N	153-8505	55	1030	847981533029	847981533029	f	\N	2018-12-04 16:57:30.194597	sunnyview-847981533029	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_toyoko-inn_tss_kiban@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	256	堀江 裕美	0	管理部	株式会社東横インIT集客ソリューション	4wZIMS/TgblvxOOIB2uEbpLv3yJYp7iIQ/7qbOFtUhAxuPm2fprLjwf9ZfbVc1ii	OSV2pS9YJAuAAyPsY4nojoaQAk0H3nzSa51xapNW	部長補佐	東京都大田区新蒲田1-7-4	\N	\N	144-0054	30	1038	266026984445	266026984445	f	\N	2018-12-04 16:57:35.444402	sunnyview-266026984445	t	f	\N	724861742662	0	\N	0	0	\N	\N
sunny_xpi@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	268		-1	対象外	日商エレクトロニクス株式会社	jHYJQqwdu96w6ylO9n4MD69xJ5ttx/XAgpyKcr0vSXT1oFxu6D79SMNEf6/lvTL2	/ZhGd1rl5UOwC7NLrVGpVPboiPmqHfwfvnCqZivr	\N	対象外	\N	\N	対象外	0	1050	932396886780	\N	t	\N	2018-11-01 12:17:49.963566	sunnyview-932396886780	t	f	\N	\N	0	\N	0	0	\N	\N
tech@nabiq.co.jp	\N	\N	2018-11-07 10:55:39.236947	469	大川 宏	0		株式会社ナビック	null	null	美術本部長	東京都千代田区神田紺屋町11岩田ビル 5F	\N	\N	101-0035	30	2199	011628324545	414456009824	f	\N	2018-12-04 16:57:01.223463	\N	f	f	\N	414456009824	0	\N	0	0	\N	\N
rsv_sunny_seaos_ticohino_smlg@ids.co.jp		\N	2018-11-01 12:17:49.963566	240	山下直宏	0	\N	シーオス株式会社	2STOwoxihJakqBNUr/zWSv2pinQrHBV/0sEPamaZAYs25UkdJMkLD/1o0FFtpB18	NVBcuGw6ChS1BoG1fRGsoyuDKgOCpSQGgB3eAczc	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1022	511061991579	511061991579	f	\N	2018-12-04 16:57:24.804902	sunnyview-511061991579	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_bushiroad+710051702383@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	376	広瀬　和彦	0	モバイルコンテンツ部	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル	\N	\N	164-0011	0	2106	710051702383	881814943668	t	\N	2018-12-04 16:56:00.576033	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+41190973911@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	311		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2041	41190973911	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
insight.test@saice.co.jp	\N	\N	2018-11-07 10:55:39.236947	301	山本 優	-1	技術開発部　計測制御技術課	株式会社サイス	null	null	係長	東京都品川区東五反田5丁目23番1号第2五反田不二越ビル3F	\N	\N	141-0023	30	2031		\N	f	\N	2018-11-20 15:52:39.573304	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunny_vpn_partner@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	430		0	対象外	サニー社内開発用アカウント（リセラー子）	null	null		対象外	\N	\N	対象外	0	2160	616716945066	627722188647	t	\N	2018-12-11 15:54:18.46998	\N	f	f	0	\N	0	\N	0	0	\N	\N
sunny_beng@ids.co.jp	\N	\N	2018-11-07 10:55:39.236947	419		0	対象外	東洋ビジネスエンジニアリング株式会社	null	null		対象外	\N	\N	対象外	0	2149	054578415506	627722188647	t	\N	2018-12-11 15:54:18.472679	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+593596402331@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	341		0	対象外	IDS（サニー開発部トレーニング用）	null	null		対象外	\N	\N	対象外	0	2071	593596402331	627722188647	t	\N	2018-12-11 15:54:18.478072	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+976441407676@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	361		0	対象外	IDS（WebECグループ）？	null	null		対象外	\N	\N	対象外	0	2091	976441407676	627722188647	t	\N	2018-12-11 15:54:18.52013	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+582205831624@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	340		0	情報システム部	カトーレック株式会社	null	null	\N	香川県高松市朝日町5－5－1	\N	\N	760-0065	0	2070	582205831624	627722188647	t	\N	2018-12-11 15:54:18.522961	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+320694683988@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	327		0	対象外	イデラキャピタルマネジメント	null	null		対象外	\N	\N	対象外	0	2057	320694683988	627722188647	t	\N	2018-12-11 15:54:18.524261	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+240813253377@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	322	及川 奨	0	ソリューション1部1グループ	株式会社システムベース	null	null		岩手県北上市村崎野19地割116番地4	\N	\N	023-1134	30	2052	240813253377	627722188647	f	\N	2018-12-11 15:54:18.532785	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_abbyy@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	109	稲田 はる子	0	\N	ABBYYジャパン株式会社	e21VC1Fxu1A81iBwZpt9KEss7KHPfG9Pgx6PJDzLTkGNxH+cu/t/JTucHJWVnQ7W	O2PrPRVhq1v1WuUVI+okKYarfk4j1rf/KqqxP99g	\N	神奈川県横浜市港北区新横浜2-5-14	WISENEXT新横浜3F	\N	222-0033	30	891	648644249161	648644249161	f	\N	2018-12-04 16:55:55.072534	sunnyview-648644249161	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_mxmobiling@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	204	若林 優一	0	スマートサービス事業本部サービス開発事業部	MXモバイリング株式会社	RVL0Fr7n/O4KQdwL1p7h5pDFiu7Lq8bSQhgNMHWP9Hbr3R1481hm4/PBD9pBQil2	IaeoiSNqiY+7at2hGZz2HYH29hUS4c2++05UKL7k		東京都江東区豊洲 3-2-24豊洲フォレシア 15F	\N	\N	135-0061	30	986	704308844416	704308844416	f	\N	2018-12-04 16:57:00.541826	sunnyview-704308844416	t	f	\N	\N	0	\N	0	0	\N	\N
housecom_kusanagi@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	297	安達 文昭	0	サービス・イノベーション室 室長	ハウスコム株式会社	null	null		東京都港区港南2-16-1	\N	\N	108-0075	30	2027	273749441829	453696413027	f	\N	2018-12-04 16:56:28.82794	\N	f	f	\N	453696413027	0	\N	0	0	\N	\N
aiscloud_aws_msil_pdc@ml.jp.panasonic.com	\N	\N	2018-11-07 10:55:39.236947	277	須山 統昭	0	AIS社インフォ（事）クラウド事業開発室	パナソニック株式会社	null	null	主幹	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	2007	191069184662	257968528965	f	\N	2018-12-04 16:55:55.720297	\N	f	f	\N	257968528965	0	\N	0	0	\N	\N
hcrc.sado_analysis@hcr.co.jp	\N	\N	2018-11-07 10:55:39.236947	295	山田 美子	0	総合企画管理グループ	株式会社ヘルスケアリレイションズ	null	null		東京都調布市多摩川3-35-4	\N	\N	182-0025	30	2025	049372840412	493640493830	f	\N	2018-12-04 16:56:25.72596	\N	f	f	\N	241214219430	0	Takayuki Nikai (sd-analysis)	0	0	\N	\N
hcrc-aws-mmwin@carecom.co.jp	\N	\N	2018-11-07 10:55:39.236947	296	山田 美子	0	総合企画管理グループ	株式会社ヘルスケアリレイションズ	null	null		東京都調布市多摩川3-35-4	\N	\N	182-0025	30	2026	956903782162	493640493830	f	\N	2018-12-04 16:56:25.727917	\N	f	f	\N	241214219430	0	hcrc-aws-mmwin@carecom.co.jp (hcrc-aws-mmwin)	0	0	\N	\N
rsv_sunny_raisingflag@ids.co.jp	\N	\N	2018-11-01 12:17:49.963566	222	尼留 昌幸	0	デジタルイノベーション部	RaisingFlag(株式会社　夢真ホールディングス)	WRg+DoT4JLoSou5yLsF5NJo+y1lRLUkPGVbFDoTsR/l37jB3QNf9GV10uCC7fDTB	DzkUtrPrrHg30ia7JJgDgyeqiW3q6s71BBJNQacc		東京都千代田区丸の内1-4-1丸の内永楽ビルディング22F	\N	\N	100 - 0005	30	1004	523514437940	523514437940	f	\N	2018-12-04 16:57:12.87846	sunnyview-523514437940	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_try@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	257	佐々木 大輔	0	財務経理部	株式会社トライグループ	N5FFJyIQdiODQIrRBWebVaH/RpInHKn7nZNIkRephK4i0xd3vqAHjmOr/Fne38v0	UIjuAXrRcsnu5QNRESLzQfvWrtfDP9iD7+1Za+vF		東京都千代田区九段北1-8-10住友不動産九段ビル7Ｆ	\N	\N	102-0073	30	1039	088225789639	088225789639	f	\N	2018-12-04 16:57:36.092471	sunnyview-088225789639	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_seaos@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	232	小川 弥蓉	0	管理本部	シーオス株式会社	k1sqqELilMddCL/avZN6owznU7tWlnEZ9ZreTFfXRD8h7UDAOFRDPMuZpvSFPnrS	VpVX5nu0jtex1+vIaO2pDpOfHs045ZgnBejTgPIk		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	1014	153781334543	153781334543	f	\N	2018-12-04 16:57:19.476653	sunnyview-153781334543	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_idera@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	158		0	対象外	株式会社イデラキャピタルマネジメント	B5JAeGJbdsEqeWxmLh16GuH7EmG3vOueOOIje8gdekPrZp9hpAkbzmig0XO8cCSD	vhvJBDCRQAZV+/T/MsZ4fWLlvMNmGQBhgAjZtapY		対象外	\N	\N	対象外	0	940	770972187900	770972187900	t	\N	2018-12-04 16:56:29.467872	sunnyview-770972187900	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_jprc01@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	177	塙 英紀	0	情報戦略部	株式会社JPリサーチ&コンサルティング	FfeslMS85kU+WEoB0z3ScC4R5Lc7u4GRW3pJZAYq8koywkkelSmq0mWMRpPk1YA2	iu/viw36dMmC09bGC0jNYWd6i4Z3Ni+z7hrjwVF4	\N	東京都港区虎ノ門3-7-12	虎ノ門アネックス6階	\N	105-0001	30	959	153255463408	153255463408	f	\N	2018-12-04 16:56:42.647639	sunnyview-153255463408	t	f	\N	\N	0	\N	0	0	\N	\N
amazon.aws@imperialhotel.co.jp	\N	\N	2018-11-07 10:55:39.236947	280		-1	対象外	株式会社帝国ホテル	null	null		対象外	\N	\N	対象外	30	2010	77060064533	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunny_kobunsha@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	424	畠山　雄一	0	総務局情報システム部	株式会社光文社	null	null		東京都文京区音羽1-16-6	\N	\N	112-8011	60	2154	524865907832	134067479084	f	\N	2018-12-04 16:56:49.958072	\N	f	f	\N	134067479084	0	\N	0	0	\N	\N
rsv_sunny_nitech@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	391	酒向 慎司	0	大学院工学研究科情報工学専攻	国立大学法人名古屋工業大学	null	null	助教	愛知県名古屋市昭和区御器所町（20号館408室）	\N	\N	466-8555	60	2121	257718684406	723172711502	f	\N	2018-12-04 16:57:04.845088	\N	f	f	\N	723172711502	0	\N	0	0	\N	\N
rsv_sunny_ei@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	136	矢野 充宏	0	デジタルコミュニケーションセンター	ピークス株式会社	iN32NBe3zlAqSi/Yd3vcuAWlKPhw+O87Uw3dyluZJEkywJw5Te+8FpAYdDEUIeAO	uL6E0lpJS+02ZDohkcOy7xWY0wHUawDicWyMH0ZV	\N	東京都世田谷区玉川台2-13-2	\N	\N	158-0096	0	918	685291271584	685291271584	t	\N	2018-12-04 16:56:13.89626	sunnyview-685291271584	t	f	\N	\N	0	\N	0	0	\N	\N
it-dev+realsys@greenpacks.co.jp	\N	\N	2018-11-07 10:55:39.236947	306	山本 達也	0	IT企画サービス課	日本グリーンパックス株式会社	null	null		東京都中央区日本橋浜町3-26浜町京都ビル3F	\N	\N	103-0007	30	2036	627651743496	560570045510	f	\N	2018-12-04 16:56:22.287978	\N	f	f	\N	560570045510	0	NGP_fulflii	0	0	\N	\N
sunny_invesco@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	423		0	対象外	インベスコアセットマネジメント株式会社	null	null		対象外	\N	\N	対象外	0	2153	113251977603	627722188647	t	\N	2018-12-11 15:54:18.468065	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+041190973911@sunnycloud.jp		\N	2018-11-07 10:56:15.721796	474		0	\N	IDS-SMOJ	\N	\N	\N	\N	\N	\N	\N	0	2204	041190973911	627722188647	t	PAPER	2018-12-11 15:54:18.47134	\N	f	f	0	\N	0	\N	0	0	\N	\N
admin@meister-online.jp		\N	2018-11-07 10:55:39.236947	273		0	\N	株式会社アイネス	null	null	\N	対象外	\N	\N	対象外	60	2003	891188027565	412372883355	f	\N	2018-12-04 16:56:31.713487	\N	f	f	\N	412372883355	0	\N	0	0	\N	\N
rsv_sunny_hcmfms@ids.co.jp		\N	2018-11-01 12:17:49.963566	151	池田　真由美	0	顧客ソリューション本部事業企画センタデータ分析エンジニアリンググループ	日立建機株式会社	4rdIE8dHijD/rJRInjzWGYExO5LYTv/3QbIqOVVX88yn7XqFpaCpqfipA6bfw3NQ	YGW+gXxs/J3W8ccNwhVIUhWQ/ytnLnzm9RUYi3Y1	\N	茨城県土浦市神立町650番地	\N	\N	300-0013	0	933	778372751271	778372751271	t	\N	2018-12-04 16:56:24.419472	sunnyview-778372751271	t	f	\N	\N	0	\N	0	0	\N	\N
am-aws@ml.jp.panasonic.com		\N	2018-11-07 10:55:39.236947	279	西 宏幸	0	オートモーティブ&インダストリアルシステムズ社オートモティブ情報企画部	パナソニック株式会社	null	null	\N	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	0	2009	207446165402	824370792849	t	\N	2018-12-04 16:56:49.281801	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_seaos_fns_xble@ids.co.jp		\N	2018-11-01 12:17:49.963566	234	浦野　由香	0	\N	シーオス株式会社	XYTvnQmo6nc/cz01D+Lk0lDgc6MzGbP17PWLNEYq9h0Yp/+B3SPNGjf900+2wxmh	Ge6d9yMZaHM2BH99RyqmdpxHSq3oNwvcAG7apxgK	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1016	337521816362	337521816362	f	\N	2018-12-04 16:57:20.783678	sunnyview-337521816362	t	f	\N	\N	1	\N	0	0	\N	\N
manage+52378875952@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	312		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2042	52378875952	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_goof.ko-ma-tsu@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	143		0	経理部	株式会社小松総合印刷所	LQQ2bzGanir2D40IIBEKVyQTHY9P7gj1lfkSKdY3RKq6GtAKa56NP9qnvJklWQJ7	pHzlOZfdYb/rNKuRf/cJwMx78DoBzJcn9V/90oq3	\N	長野県伊那市横山10955-1	\N	\N	396-0029	40	925	773355592474	773355592474	f	\N	2018-12-04 16:56:18.716005	sunnyview-773355592474	t	f	\N	\N	1	\N	0	0	\N	\N
rsv_sunny_his_test2@ids.co.jp	\N	\N	2018-11-01 12:17:49.963566	156		0	対象外	株式会社エイチ・アイ・エス	RxL3Gkvl8MOYOCjFJBQ2O7n65ngo9DKoIM9tpVhOAypibL71jB76DirI+T+Qaa4l	3H5yoOmi52ES5lgj+MXJ7RbVMht9uMVVBoh5kH3f		対象外	\N	\N	対象外	0	938	648034055419	648034055419	t	\N	2018-12-04 16:56:28.057027	sunnyview-648034055419	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_hanamaru@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	150	蜂須賀　俊介	0	\N	株式会社花まるラボ	bTMM6s/O9lR+SLS5MLFM7BQ+kWNfYg5l7DnTOud1OVyjvYDWbZbZczifQgOmvTFv	dxF+pSPOt+XDDcjIE1OJSF9Mza6OMxcKAkKcvbdQ	\N	東京都文京区本郷3-32-7	東京ビル 6階	\N	113-0033	30	932	330770408207	330770408207	f	\N	2018-12-04 16:56:23.662588	sunnyview-330770408207	t	f	\N	\N	0	\N	0	0	\N	\N
t_nikai@hcr.co.jp	\N	\N	2018-11-07 10:55:39.236947	468	山田 美子	0	総合企画管理グループ	株式会社ヘルスケアリレイションズ	null	null		東京都調布市多摩川3-35-4	\N	\N	182-0025	30	2198	543862711800	493640493830	f	\N	2018-12-04 16:56:25.729862	\N	f	f	\N	241214219430	0	Takayuki Nikai (products)	0	0	\N	\N
manage+166888973065@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	318		0	対象外	不明	null	null		対象外	\N	\N	対象外	0	2048	166888973065	627722188647	t	\N	2018-12-11 15:54:18.482058	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+018583534596@sunnycloud.jp		\N	2018-11-07 10:56:15.766322	476		0	\N	IDS_KORYU	\N	\N	\N	\N	\N	\N	\N	0	2206	018583534596	627722188647	t	PAPER	2018-12-11 15:54:18.501575	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+147746061627@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	317	柴村 明紀	0		荏原健康保険組合	null	null		東京都大田区羽田旭町13‐3	\N	\N	144-0042	30	2047	147746061627	627722188647	f	\N	2018-12-11 15:54:18.507268	\N	f	f	0	\N	0	\N	0	0	\N	\N
manage+554390860088@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	339		0	対象外	日酸TANAKA？	null	null		対象外	\N	\N	対象外	0	2069	554390860088	627722188647	t	\N	2018-12-11 15:54:18.539504	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_nidec@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	209	麻生 拓郎	0	情報システム部 IT事業推進室	日本電産株式会社	4lUDuY4OJlJxa5rF9A6FfYBfZJvaFtUd9fLQYRs6FSICQSk4HQs3A6aIhoaUiAsC	dZoU796AGpqksoj97iOhup8SLUapksR760LipPa4	\N	京都府京都市南区久世殿城町338	\N	\N	601-8205	55	991	284890138084	284890138084	f	\N	2018-12-04 16:57:03.913519	nidec-billingreport	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+687306606428@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	379	広瀬　和彦	0	モバイルコンテンツ部	株式会社ブシロード	null	null	\N	東京都中野区中央1-38-1住友中野坂上ビル	\N	\N	164-0011	0	2109	687306606428	881814943668	t	\N	2018-12-04 16:56:00.566586	\N	f	f	\N	\N	0	\N	0	0	\N	\N
manage+927565493760@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	357		0	代理店営業支援ツールチーム	ジブラルタ生命保険株式会社	null	null		東京都品川区西五反田2-15-7ジブラルタ生命五反田ビル6F	\N	\N	141-0031	30	2087	927565493760	627722188647	t	\N	2018-12-11 15:54:18.479384	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_konicaminolta@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	190	西田 正則	0	開発本部 電子技術ユニット開発センターコントローラー開発部	コニカミノルタ株式会社	CHRj8ThwRyd6qOkAAKBzJd3qNGJTNMy8OfIQB/eJ2YyDKfwd0391qQHN0vILrLpS	Z+86dzDCBIyQbzx3QvSbR6paiE38iQjPkvg7FSgn		兵庫県伊丹市高台4丁目18番地	\N	\N	664-8511	30	972	637092732207	637092732207	f	\N	2018-12-04 16:56:51.258258	sunnyview-637092732207	t	f	\N	\N	0	\N	0	0	\N	\N
ids_ri_test@ids.co.jp	\N	\N	2018-11-01 12:17:49.963566	105		0		IDSRI用親アカウント（テスト用）	bA0KVN8LDBQO+/7JGVYnOmxKeyz/AXSRNDBNHk1jmpQeNCRBbg8FSq8YxtzNffC0	iqJd9daldHeBlskVomRJ+P8sLDv3G5DLkreE7TsI		対象外	\N	\N	対象外	0	887	587685967464	587685967464	t	\N	2018-12-10 16:29:27.040317	sunnyview-587685967464	t	f	0	\N	0	\N	0	0	\N	\N
manage+285693525620@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	325		0	対象外	IDS（旧サニーHP用）	null	null	\N	対象外	\N	\N	対象外	0	2055	285693525620	627722188647	t	\N	2018-12-11 15:54:18.480685	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_dts@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	132	佐々木 一富	0	法人通信事業本部　第二法人事業部	株式会社DTS	rqYdI32+w2CQKpRL/MzKVMv+8g3pZuwoP+zWC/vmeJPpmRj1PQb4TMU6bNUWuwhH	dtGkpoKsp1u55hm9Wb5HyJHfHi2H/A1hsSjw1WlR	\N	東京都中央区八丁堀2-23-1	エンパイヤビル8F	\N	104-0032	30	914	793892803184	793892803184	f	\N	2018-12-04 16:56:11.07124	sunnyview-793892803184	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_ebara@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	135	磯田 明宏	0	情報通信統括部 システム保守運用部	株式会社荏原製作所	cL4TgHr9OW4s4+a9PtylpCAj81dyhjbLKwWU42XCTwQANDp2K6Jlpt4dmabzuKMs	n2Et4GTpDR2LUySed1Ej+g0xZYs/hPmbBcKgLcqb	インフラ・システム運用課 課長	東京都大田区羽田旭町11-1	\N	\N	144-8510	55	917	131330460059	131330460059	f	\N	2018-12-04 16:56:13.154311	ebara-billingreport	t	f	\N	\N	0	\N	0	0	\N	\N
garo_aws_production@gali-int.com	\N	\N	2018-11-07 10:55:39.236947	291	加藤 裕稔	0		株式会社ガリインターナショナル	null	null		東京都中野区上高田3-1-3野田ビル201	\N	\N	164-0002	45	2021	953033831198	138865218164	f	\N	2018-12-04 16:56:16.712604	\N	f	f	\N	138865218164	0	Hirotoshi Kato GAROproduction	0	0	\N	\N
rsv_sunny_jmdc_2@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	174	高橋　孝之	0	\N	株式会社JMDC	0xVhTYGCU/+ke0KXZQT/bF7eslomSohKdLnSSdnrxGnGLH/N1CyZqVjkzGx2+lEr	JtPI+EDmJiCUtDeBl9rLKbxtk6oxNqWN70SD1Qto	\N	東京都港区芝大門2-5-5	住友芝大門ビル12F	\N	105-0012	30	956	976719614207	976719614207	f	\N	2018-12-04 16:56:40.61027	sunnyview-976719614207	t	f	\N	\N	0	\N	0	0	\N	\N
it-dev+ngp@greenpacks.co.jp	\N	\N	2018-11-07 10:55:39.236947	305	山本 達也	0	IT企画サービス課	日本グリーンパックス株式会社	null	null		東京都中央区日本橋浜町3-26浜町京都ビル3F	\N	\N	103-0007	30	2035	953663845197	560570045510	f	\N	2018-12-04 16:56:22.280407	\N	f	f	\N	560570045510	0	NGP_NGP	0	0	\N	\N
sunny_l-m@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	427	板垣 秀典	0	制作課	株式会社ランドマーク	null	null	課長	東京都新宿区西新宿 6-5-1新宿アイランドタワー 8F	\N	\N	163-1308	30	2157	304113017156	027509846516	f	\N	2018-12-04 16:56:53.905289	\N	f	f	\N	027509846516	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+903825919046@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	372	成田 耕裕	0	\N	株式会社ブシロードクリエイティブ	null	null	代表取締役社長	東京都中野区中央1-38-1住友中野坂上ビル	\N	\N	164-0011	0	2102	903825919046	881814943668	t	\N	2018-12-04 16:56:00.555055	\N	f	f	\N	\N	0	\N	0	0	\N	\N
seaos_saaswms_pro@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	402	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2132	407165401438	647397808689	f	\N	2018-12-04 16:57:16.170211	\N	f	f	\N	647397808689	0	seaos_saaswms_pro	0	0	\N	\N
rsv_sunny_seaos_dnp_tmssystem@ids.co.jp		\N	2018-11-01 12:17:49.963566	233	大和田 美雪	0	\N	シーオス株式会社	5AaHSMKoPVttool3neXWxnX7le4EcmObj6xvaTbXcPugv+CqpNBi1BCu1gRLtuUF	sOVU3OO0AMTFxunvjgeiN9EGTch8ZNdb+knsVCC/	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	1015	879594214584	879594214584	f	\N	2018-12-04 16:57:20.118951	sunnyview-879594214584	t	f	\N	\N	1	\N	0	0	\N	\N
seaos_tico_airas_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	407	山下 直宏	0		シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2137	193980887254	430239111239	f	\N	2018-12-04 16:57:24.13471	\N	f	f	\N	430239111239	0	seaos_tico_airas_dev	0	0	\N	\N
rsv_sunny_tekunodo@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	247	鎌田 寛昭	0	\N	株式会社テクノード	89lZTnjLbSj7dEzuj/2AvctXgFdXBZbNmy7F0dJ/Zy1cS3qEAluzMFjlBDcRluMV	vBgm5Y8P15r1T8cXNtLBP6mKylOuVy4UzuFzJCgq	代表取締役	東京都目黒区上目黒2-15-14AKビル4F 5F	\N	\N	153-0051	30	1029	765150201305	765150201305	f	\N	2018-12-04 16:57:29.514777	sunnyview-765150201305	t	f	\N	\N	2	\N	0	0	\N	\N
aws_billing@ids.co.jp		\N	2018-11-01 12:17:49.963566	100		0	対象外	AWS利用料(EC、SOL)	AiADYrOyie7h2ynqd9dC5s9L75c65CP7Ri1S4ago7Cl1lU6zqasGSpPLQDDWwsU5	KIJEppAXr/rz4wa9Y81nihxSUMmIc2EfqM7drwsN	\N	対象外	\N	\N	対象外	0	882	209954284200	209954284200	t	\N	2018-12-10 16:29:25.623015	sunnyview-209954284200	t	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_rapinics@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	224	井上　達哉	0	システム開発部	株式会社ラピニクス	/6YEHWtK65N68QT1jN2epHkLFFKJR/F3+Erf/P0k2+Wzz/TeoviuleINfpnLg3ay	GkbOuD23MeGoZLwMpt402R/DvqJUeIm/L+pQaOPt		東京都港区港南1-8-27日新ビル3F	\N	\N	108-0075	30	1006	967450434821	967450434821	f	\N	2018-12-04 16:57:14.221608	sunnyview-967450434821	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_granvista@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	146	山口 瑛恵 様、小沢 恵	0	セールス＆マーケティング部CRM課	株式会社グランビスタ ホテル＆リゾート	VJdLN8fcrqtSBLEQN5JFkmeMEpZrmdHNFQJu3lhLdhAbPorMAuGbX7ITIpKEQBdR	Sq7wDY+XGYJ6BtpmPO+6CeC3yp3SKWv/u9bZ7s2A	\N	東京都千代田区内神田2丁目3番4号	S-GATE大手町北 5階	\N	101-0047	0	928	690364213771	690364213771	t	\N	2018-12-04 16:56:20.751241	sunnyview-690364213771	t	f	\N	\N	0	\N	0	0	\N	\N
manage+921990075823@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	356	小野田 直弘	0	情報システム部	株式会社エイジス	null	null	マネージャー	千葉市花見川区幕張町3-7727-1	エイジス本社第2ビル5階	\N	262-0032	0	2086	921990075823	627722188647	t	\N	2018-12-11 15:54:18.525596	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_toyobody@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	254	長濱 聡史	0	管理部	株式会社東洋ボデー	94H85VkFcBTeZl/rHOirf/TRqbiG/nHPvhzeBWo+/FgoD9LHD1hxGk3ULsELw89t	LTinoX3h0+SYALOvMRhclWCZbpe8OkDTeBzVNUd8		東京都武蔵村山市伊奈平2-42-1	\N	\N	208-0023	30	1036	880135449318	880135449318	f	\N	2018-12-04 16:57:34.109862	sunnyview-880135449318	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_informetis_03@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	164	宮澤	0	\N	インフォメティス株式会社	ScVMZ3qxAx+VqyrQaBREo08JGGFrIT+gvecxMPmreLla7Vp2VoTeWt1tG18KDwQe	sLXSSrd6Wkwn9Fd2eyr62jJxgGPRO8P7Y3wOxRO0	\N	東京都港区高輪1-5-4常和高輪ビル 3F	\N	\N	108-0074	0	946	963482872680	963482872680	t	\N	2018-12-04 16:56:33.7888	sunnyview-963482872680	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_nichirei@ids.co.jp		\N	2018-11-01 12:17:49.963566	208	大和田 美雪	0	\N	シーオス株式会社	EPPNIzjIZiv0oinngK4+Q8HuRwf4Am29V4Lz/nadVtT1gXtYs0wqZV6Br/z4KtXj	lVeBmfvuwHz1vOO5RjhnQXfdUQo1iZSsHFSXiwwf	\N	東京都渋谷区恵比寿1-18-18	東急不動産恵比寿ビル6F	\N	150-0013	30	990	323472269056	323472269056	f	\N	2018-12-04 16:57:03.251828	sunnyview-323472269056	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_nitech+723172711502@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	210	酒向 慎司	0	大学院工学研究科情報工学専攻	国立大学法人名古屋工業大学	+4UXZqKMyku/Nn6qxMhG62RPRMPOZ4k7BX01Fym6Vb4lJRAQgFEOlui61+ge0m2j	2lni3h2Tif0lVVvgHNbBnie/ygHQDkIlkr28EYql	助教	愛知県名古屋市昭和区御器所町（20号館408室）	\N	\N	466-8555	60	992	723172711502	723172711502	f	\N	2018-12-04 16:57:04.84875	sunnyview-723172711502	t	f	\N	\N	0	\N	0	0	\N	\N
manage+618900046361@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	343	ご担当者	0	営業統括本部営業本部セールスサポートメディア部	日立建機株式会社	null	null	\N	東京都台東区東上野二丁目１６番１号野イーストタワー１５階	\N	\N	110-0015	0	2073	618900046361	627722188647	t	\N	2018-12-11 15:54:18.542141	\N	f	f	0	\N	0	\N	0	0	\N	\N
rsv_sunny_tokyo-ct@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	250	伊藤 晴子	0	総務課用度係	独立行政法人 国立高等専門学校機構　東京工業高等専門学校	BlO+RkU+7Vzq36ILPWFSa6cwJN19m8YYJQZWjyIZS2b2ufaqLC9Of6hK4V2ASKkh	sCSsy+sYeij/p4Wx9iKKMlO2Zzewb+HnFGSX/vG6	\N	東京都八王子市椚田町1220-2	\N	\N	193-0997	30	1032	472782229427	472782229427	f	\N	2018-12-04 16:57:31.492156	sunnyview-472782229427	t	f	\N	\N	0	\N	0	0	\N	\N
h_shimura@yumeshin.co.jp		\N	2018-11-07 10:55:39.236947	294		0	対象外	株式会社夢真ホールディングス	null	null	\N	対象外	\N	\N	対象外	30	2024	311552298434	459457459923	f	\N	2018-12-04 16:57:38.817169	\N	f	f	\N	459457459923	0	\N	0	0	\N	\N
sc_his@ids.co.jp	\N	\N	2018-11-01 12:17:49.963566	262		0	対象外	株式会社エイチ・アイ・エス	SPIiYiURe5ihnxYCjYdaKHFSXFJgZAebFF9tTS+idf8/y9SnrS2T1y8WpkQDNbK3	8jlmZSyJ+WvvRJeqsWSQ9tzAP95HlB3BOYEjP5W4		対象外	\N	\N	対象外	0	1044	822609446484	822609446484	t	\N	2018-12-04 16:57:39.591885	sunnyview-822609446484	t	f	\N	\N	0	\N	0	0	\N	\N
manage+192874593917@sunnycloud.jp	\N	\N	2018-11-07 10:55:39.236947	321		-1	対象外	IDS（旧AKATSUKIコード管理用）	null	null		対象外	\N	\N	対象外	0	2051	192874593917	\N	t	\N	2018-11-07 10:55:39.236947	\N	f	f	\N	\N	0	\N	0	0	\N	\N
sunnycloud+265491119629@ids.co.jp		\N	2018-11-07 10:55:39.236947	458	奥村　史朗	0	\N	アルカディア・システムズ株式会社	null	null	\N	東京都千代田区九段南 3-8-11飛栄九段ビル2F	\N	\N	102-0074	30	2188	265491119629	524994771186	f	\N	2018-12-04 16:57:42.525799	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_kk.panasonic@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	187	西 宏幸	0	オートモーティブ&インダストリアルシステムズ社オートモティブ情報企画部	パナソニック株式会社	o4SIXDfs4ZpO3j/TfNViVouEqP3A+C71A4Hj0L+E8+Mi446NbKxv0wg/XSHKGOy9	h+9GnmNPSkB0wM9Xqd6UjOvFlyCEqB6/r5SvH1ki	\N	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	0	969	824370792849	824370792849	t	\N	2018-12-04 16:56:49.285424	sunnyview-824370792849	t	f	\N	\N	0	\N	0	0	\N	\N
seaos_product_development_dev@seaos.co.jp	\N	\N	2018-11-07 10:55:39.236947	400	小川 弥蓉	0	管理本部	シーオス株式会社	null	null		東京都渋谷区恵比寿1-18-18東急不動産恵比寿ビル6F	\N	\N	150-0013	30	2130	681213275573	741039434071	f	\N	2018-12-04 16:57:22.773079	\N	f	f	\N	741039434071	0	\N	0	0	\N	\N
rsv_sunny_solasto@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	244		0	対象外	ソラスト請求まとめ	uHuvKe+ihrdWBIECdO1wobrkRERq9ykzonE9VoFv2pNEllR3mn3i1gulVcrFVLeg	DrpYPXak7Altj4/rsKwzG/kdaG/oMIKi9Y041C/o	\N	対象外	\N	\N	対象外	0	1026	707945761985	707945761985	t	\N	2018-12-04 16:57:27.504523	sunnyview-707945761985	t	f	\N	\N	0	\N	0	0	\N	\N
hitachi_malware@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	104		0	\N	日立建機マルウェア対応	KoHyqM7wZL7hFt47hIkG2eEsmnLl+FdmLzkn0D0SqDc=		\N	対象外	\N	\N	対象外	0	886		\N	t	\N	2018-12-03 17:56:35.452957	\N	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_goof.buzz@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	142	近藤 寛一	0	経営企画	株式会社グーフ	Zxjlq33qTCT5iczLM4cn0ZzdDtcgkS7bo8Rzj1eGGZPMjWfGSYdqukJ9N1hj7Da9	LtWNA5rmG/4WSPF74Db1FZ3TYzJ8NsQ/UyS63zg2	COO	東京都品川区大崎4-1-2	ウィン第2五反田ビル3F	\N	141-0032	30	924	472370278951	472370278951	f	\N	2018-12-04 16:56:18.033804	goof-billing-report	t	f	\N	\N	4	\N	0	0	\N	\N
rsv_sunny_biee@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	116	青木 則昭	0		BIEE合同会社	hn9j1W633w0ywwFtUmogQmLMOcoeCfYw739iBEQZziCX/lgpgdzqbnoSbW8clxw5	fgYVgXxXIqAa9m6duB5/o/BJb3NShaFVpOT/w3j0	代表社員	東京都国立市泉1-12-3	\N	\N	186-0012	30	898	004743504877	004743504877	f	\N	2018-12-04 16:55:59.835997	sunnyview-004743504877	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_bushiroad+881814943668@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	117		0	対象外	株式会社ブシロード	uaHhfT092kKAQYgSXqzQWuiPDz4K2iP1JIkKk8BeSMi1reI3NiZd9AI94YldsY7I	YGtYpKAO8FTAabArgELLVBzIRRQeVf2gML/RiJUy	\N	対象外	\N	\N	対象外	0	899	881814943668	881814943668	t	\N	2018-12-04 16:56:00.577579	sunnyview-881814943668	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_chodai1@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	119	元木 賢一	0	社会システム事業部	株式会社長大	0cHH70F9LC29WcnBce9/Mamv4C5tDpFVHMEkqW1VgZMQWlnrSFcj2bZ4NWHz2Vg/	IoT0C7npDJYJWdr8OcT4xwOJ15RVcod9Z5FGckrG		東京都中央区勝どき1丁目13-1	\N	\N	104-0054	30	901	643345495175	643345495175	f	\N	2018-12-04 16:56:01.909689	sunnyview-643345495175	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_ctcg_03@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	126	有馬 正行	0	情報通信第１本部 ｻｰﾋﾞｽﾋﾞｼﾞﾈｽ推進ﾁｰﾑ	伊藤忠テクノソリューションズ株式会社	3LhPAWcd8rL84D6M38DRMcXBn+HQdl93rK+FWd6+WOCz8grThU4wOSKN8rvfrpLt	xDI/T2znKnoCATcoWfsqZl1ISxBQ5daVvc43jUQT	チーム長	東京都千代田区霞ヶ関3-2-5	霞ヶ関ビル	\N	100-6080	50	908	235224386386	235224386386	f	\N	2018-12-04 16:56:06.812007	sunnyview-235224386386	t	f	\N	\N	0	\N	0	0	\N	\N
sysmaster@nomura-g.co.jp		\N	2018-11-07 10:55:39.236947	462		0	対象外	野村ユニソン株式会社	null	null	\N	対象外	\N	\N	対象外	30	2192	843792414839	572131055370	f	\N	2018-12-04 16:57:06.824283	\N	f	f	\N	572131055370	0	\N	0	0	\N	\N
rsv_sunny_sblc@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	231	吹野 孝幸	0	BM企画部	株式会社シミズ・ビルライフケア	0VZYTly9lMJUvps2HexLPb5G+r0oydV72yXDM0fEi27izajqxk9qiiXGkBe7SA5h	+sARljvQ3KC2MTAxBWmZ5o442b1TSOvoFkcLpXhE	\N	東京都中央区京橋2-10-2	ぬ利彦ビル南館3F	\N	104-0031	30	1013	371113534145	371113534145	f	\N	2018-12-04 16:57:18.851512	sunnyview-371113534145	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_yumeshin@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	260	土田　裕文	0	システム部	株式会社 夢真ホールディングス	3wRmg+XagLbcKU5DDv89mUFny6NTp+D4/nVmcWZPwpAloZJCyo77RaUHjFcLM/mj	h/TkaMVtbHLDaIh/E4WFnRtU3eK7Em0/3aeo86V5	\N	東京都千代田区丸の内1-4-1丸の内永楽ビルディング22F	\N	\N	100-0005	0	1042	220497617317	220497617317	t	\N	2018-12-04 16:57:38.158213	sunnyview-220497617317	t	f	\N	\N	0	\N	0	0	\N	\N
system-hojincccf-aws@his-world.com	\N	\N	2018-11-07 10:55:39.236947	465		0		株式会社エイチ・アイ・エス	null	null		対象外	\N	\N	対象外	0	2195	699033494752	822609446484	t	\N	2018-12-04 16:57:39.586615	\N	f	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_govuk@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	145	ご担当者	0	\N	駐日英国大使館	ckgtKv0Z0p74r2wBjJK1taQSe9L4XN9DFo5cxyEJ/TGesRr0PoZp4qqf1d625KGw	illfcu4yYXJYQ2wDN7dQdNp23cg9L6nPBvPmj0pv	\N	東京都千代田区一番町1	\N	\N	102-8381	30	927	095848063701	095848063701	f	\N	2018-12-04 16:56:20.086917	sunnyview-095848063701	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_hcr-aws-linktest@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	152	山田 美子	0	総合企画管理グループ	株式会社ヘルスケアリレイションズ	kg0ouPzboX1jZ7zViCTAgAqWnUPvCU5m226949FywCxRCaSSJzs57znzHLAqR7gj	RCLeeFMSHiRsB7GniZJ1YA+8pKzDigEg0r2lDagf	\N	東京都調布市多摩川3-35-4	\N	\N	182-0025	30	934	241214219430	241214219430	f	\N	2018-12-04 16:56:25.071467	sunnyview-241214219430	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_healthcare_relations@sunnycloud.jp	\N	\N	2018-11-01 12:17:49.963566	153	山田 美子	0	総合企画管理グループ	株式会社ヘルスケアリレイションズ	gXy3A2Ad8btS3m1+k3an6pmr4caEfbWSIVJIixN9f4O0CMPpXMQjcldA3L/ZPvaP	1dkfComSnuW3ziMQ/nyDIIhQOgLhiJvXg7fYfZSw		東京都調布市多摩川3-35-4	\N	\N	182-0025	30	935	493640493830	493640493830	f	\N	2018-12-04 16:56:25.733287	sunnyview-493640493830	t	f	\N	241214219430	0	\N	0	0	\N	\N
rsv_sunny_stransa@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	245	西島　彰一	0	\N	株式会社ストランザ	kRWiKFiTmwiKALx1KXobjIuz+MPy88VAxrEc+qhRRjQE5ZgMjCoIhhC6x9Z56TSu	7+NICno+sV2Hmbh348kgafS563XDZ2jbMeQR4kUu	\N	東京都港区芝公園1-3-5	バルコ御成門8F	\N	105-0011	30	1027	212309556972	212309556972	f	\N	2018-12-04 16:57:28.173031	sunnyview-212309556972	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_saice@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	229	山本 優	0	技術開発部　計測制御技術課	株式会社サイス	FOiLFWV51UXnE5bHr5g+Hc9TbILJpPn4nLXjyPEyz7Jbo/HZc577Z3jYXYM711pb	iM8CyyMk+tdYZVD6jXx1C4gV9664kqMbTyUdVX/p	係長	東京都品川区東五反田5丁目23番1号	第2五反田不二越ビル2F	\N	141-0022	30	1011	797761427745	797761427745	f	\N	2018-12-04 16:57:17.519512	sunnyview-797761427745	t	f	\N	\N	2	\N	0	0	\N	\N
aiscloud_aws_msil_dev@ml.jp.panasonic.com	\N	\N	2018-11-07 10:55:39.236947	276	須山　統昭	0	AIS社インフォ（事）クラウド事業開発室	パナソニック株式会社	null	null		神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	2006	819778114497	075499321923	f	\N	2018-12-04 16:55:54.336614	\N	f	f	\N	075499321923	0	aiscloud_aws_msil_dev@ml.jp.panasonic.com	0	0	\N	\N
rsv_sunny_ines@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	161	藤崎 裕加	0	アプリケーションサービス本部 社会基礎サービス第三部	株式会社アイネス	pF5g4CekMnH07UNoIfJZylYqthRYEbrAJI1/jdzXw7t2Ki57lqaqef/JCGG9xlp8	w8hK1HuZXJ0RTwVfEiTCq0NSo8cQjXGsUsP+xBX5	\N	東京都千代田区三番町26番地	\N	\N	102-0075	60	943	412372883355	412372883355	f	\N	2018-12-04 16:56:31.721997	sunnyview-412372883355	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_greenpacks@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	148	山本 達也	0	IT企画サービス課	日本グリーンパックス株式会社	iHxRRyi7FRPSBHaP53EyfQSPVeeo56W3orl3eXnw7QCz65fcbz0s+nyiR8u5+63e	i4vm1JEV/UvCaEaReKrrKdT9RuYYzthvQRfpgcfJ	\N	東京都中央区日本橋浜町3-26	浜町京都ビル3F	\N	103-0007	30	930	560570045510	560570045510	f	\N	2018-12-04 16:56:22.291435	sunnyview-560570045510	t	f	\N	\N	2	\N	0	0	\N	\N
rsv_sunny_iscube.ihi_2@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	168	石口 阿人	0	ビジネスソリューション事業部 エネルギー・プラントグループ	株式会社IHIエスキューブ	D2NkyXwSY9z+Ippn1X+14hhoyD0G0SSQQxzy2apiHRUkZuzGuzAmLedaPfKbf6wY	ylUCr+JTM1UtnfSXiWEojODniXjjL5/ZNnpt6y92	\N	東京都江東区豊洲３丁目１番１号	豊洲IHIビル6F北	\N	135-0061	30	950	022576752985	022576752985	f	\N	2018-12-04 16:56:36.460768	sunnyview-022576752985	t	f	\N	\N	0	\N	0	0	\N	\N
manage+446742835951@sunnycloud.jp		\N	2018-11-07 10:55:39.236947	333	ご担当者	0	営業統括本部営業本部セールスサポートメディア部	日立建機株式会社	null	null	\N	東京都台東区東上野二丁目１６番１号野イーストタワー１５階	\N	\N	110-0015	0	2063	446742835951	627722188647	t	\N	2018-12-11 15:54:18.490693	\N	f	f	0	\N	0	\N	0	0	\N	\N
sunnycloud+212010214002@ids.co.jp		\N	2018-11-07 10:55:39.236947	435	関　昭夫	0	経営企画本部	ジスクソフト株式会社	null	null	\N	神奈川県川崎市中原区小杉町1-403武蔵小杉STMビル3F	\N	\N	211-0063	30	2165	212010214002	524994771186	f	\N	2018-12-04 16:57:42.554904	\N	f	f	\N	\N	2	Akio Seki	0	0	\N	\N
solasto_admin@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	264	松井　弘行	0	業務システム部　システム管理課	株式会社ソラスト	xf8POHgGHGzPPpr+BX7ptCoojZXU+X5cRl2ougzleEI=		\N	東京都港区港南1-7-18DBC品川東急ビル6階	\N	\N	108-8210	0	1046	252443905767	\N	t	\N	2018-12-03 18:38:20.580608	\N	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_itaccess@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	172	花田 亜紀子	0	管理部	アイティアクセス株式会社	LYEqa9RJo4JXZhooYPMET0+rSy3B7Qnn3w2rHWV9luwLlBLSy7PXiCTWuqYEt9lZ	yjY634Tdyn4gwHZHSBAFq+YiMoUcevHc7v+fDGSp	\N	神奈川県横浜市港北区	新横浜3-17-6	\N	222-8545	30	954	516801775842	516801775842	f	\N	2018-12-04 16:56:39.323378	sunnyview-516801775842	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_toppan@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	251	丹　寛之	0	情報コミュニケーション事業本部 セキュアビジネスセンター	凸版印刷株式会社	1FPQ89H0Q9e1THInhZ72UQWSO0WjPgBqg00B8aR9C2/cxPMz1xMfR5wrRqNMXkzo	zt6wq9biAvqlTwTyVS3ehxTw4bVo/7HnuQxqgUBC	次世代セキュア開発部	東京都千代田区丸の内３-４-１	新国際ビル2階	\N	100-0005	30	1033	610002975014	610002975014	f	\N	2018-12-04 16:57:32.119366	sunnyview-610002975014	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_daikokuya78@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	128	廣瀬 竹伸	0	\N	株式会社大黒屋	L2zAct3OlGBHYGghLi0wwo5S91Mo18GIe4/37StzABoewUTl0LtZRGeYQktA1mXX	bW/OiUzJ8Icb+fAK4BYkz5NPIX2W/lfXc9T3EIk3	総務部	東京都港区港南4-1-8	リバージュ品川3F	\N	108-0075	30	910	814586562528	814586562528	f	\N	2018-12-04 16:56:08.252199	sunnyview-814586562528	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_sunny_housecom@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	157	安達 文昭	0	サービス・イノベーション室 室長	ハウスコム株式会社	ttH+C45QaPSVMKxkW6z9DHTFVeQoN9MXircgjaOAjXCsZAVxsh7Es13wyMQLxSGE	lY536WbS6xmM7JTQbS3hUOs9dPZ4DFogCl1aXXb9	\N	東京都港区港南2-16-1	\N	\N	108-0075	0	939	453696413027	453696413027	t	\N	2018-12-04 16:56:28.83157	sunnyview-453696413027	t	f	\N	\N	0	\N	0	0	\N	\N
rsv_aiscloud_aws_msil_dev@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	108	須山 統昭	0	AIS社インフォ（事）クラウド事業開発室	パナソニック株式会社	ADn7ztq2VuyJVBwX3nsNr1Qc37gfG5CsglTk+nu3KfQgGLmhkJdJuV8JG1fShMwd	y8VUn2Bt6FAcge18LW4nv3ZWqstpQJ7nPMpyvQJH	主幹	神奈川県横浜市都筑区池辺町4261番地	\N	\N	224-8520	45	890	075499321923	075499321923	f	\N	2018-12-04 16:55:54.340071	sunnyview-075499321923	t	f	\N	\N	2	\N	0	0	\N	\N
manage@sunnycloud.jp		\N	2018-11-01 12:17:49.963566	106		0	対象外	サニーポータル（リセラー）★親	/Ep1wybLYELffoWQudo53cdeeH4kNJZ/NT1i5yuTdWOciYw3NNi4RzO8ecHTemtz	q9PWx0uZJ1JggBKkJ4lKW/ShMAVXtd5KDPg8fIuj	\N	対象外	\N	\N	対象外	0	888	627722188647	627722188647	t	\N	2018-12-11 15:54:18.545047	sunny-portal-billing	t	f	0	\N	0	\N	0	0	\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 2325, true);


--
-- Name: users_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_id_seq1', 491, false);


--
-- Name: users_id_seq2; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.users_id_seq2', 1, false);


--
-- Data for Name: volumes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.volumes (cid, device, instid, state, volid, delonterm, azone, attachtime, createtime, encrypt, size, snapshotid, state2, iop, tag_name, voltype, progress, description, stragetype) FROM stdin;
\.


--
-- Name: ami_enable ami_enable_cid_imageid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ami_enable
    ADD CONSTRAINT ami_enable_cid_imageid_key UNIQUE (cid, imageid);


--
-- Name: ami_enable ami_enable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ami_enable
    ADD CONSTRAINT ami_enable_pkey PRIMARY KEY (id);


--
-- Name: azone_enable azone_enable_cid_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.azone_enable
    ADD CONSTRAINT azone_enable_cid_name_key UNIQUE (cid, regionname);


--
-- Name: azone_enable azone_enable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.azone_enable
    ADD CONSTRAINT azone_enable_pkey PRIMARY KEY (id);


--
-- Name: dept dept_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dept
    ADD CONSTRAINT dept_pkey PRIMARY KEY (id);


--
-- Name: emp emp_cid_email_status; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emp
    ADD CONSTRAINT emp_cid_email_status UNIQUE (cid, email, status);


--
-- Name: emp emp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.emp
    ADD CONSTRAINT emp_pkey PRIMARY KEY (id);


--
-- Name: info info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.info
    ADD CONSTRAINT info_pkey PRIMARY KEY (mid);


--
-- Name: inst_req inst_req_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inst_req
    ADD CONSTRAINT inst_req_pkey PRIMARY KEY (id);


--
-- Name: insttype_enable insttype_enable_cid_name_region; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insttype_enable
    ADD CONSTRAINT insttype_enable_cid_name_region UNIQUE (cid, name, region);


--
-- Name: insttype_enable insttype_enable_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insttype_enable
    ADD CONSTRAINT insttype_enable_pkey PRIMARY KEY (id);


--
-- Name: insttype_pricing insttype_pricing_cid_name_region; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.insttype_pricing
    ADD CONSTRAINT insttype_pricing_cid_name_region UNIQUE (cid, name, region);


--
-- Name: obj obj_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.obj
    ADD CONSTRAINT obj_pkey PRIMARY KEY (id);


--
-- Name: proj proj_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proj
    ADD CONSTRAINT proj_pkey PRIMARY KEY (id);


--
-- Name: sg_enable sg_enable_cid_sg_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sg_enable
    ADD CONSTRAINT sg_enable_cid_sg_id UNIQUE (cid, sg_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_uid_status; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_uid_status UNIQUE (uid, status);


--
-- Name: idx_cost_1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cost_1 ON public.cost USING btree (cid);


--
-- Name: idx_cost_2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cost_2 ON public.cost USING btree (usageaccountid);


--
-- Name: idx_cost_detail01; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cost_detail01 ON public.cost_detail USING btree (cid);


--
-- Name: idx_cost_detail02; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_cost_detail02 ON public.cost_detail USING btree (ym);


--
-- Name: idx_emp; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_emp ON public.emp USING btree (cid);


--
-- Name: idx_emp2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_emp2 ON public.emp USING btree (empid);


--
-- Name: idx_info01; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_info01 ON public.info USING btree (senddate);


--
-- Name: idx_info02; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_info02 ON public.info USING btree (toall);


--
-- Name: idx_infoctl01; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_infoctl01 ON public.info_ctl USING btree (cid);


--
-- Name: idx_infoctl02; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_infoctl02 ON public.info_ctl USING btree (uid);


--
-- Name: idx_instance01; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_instance01 ON public.instance USING btree (cid, id);


--
-- Name: idx_instance02; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_instance02 ON public.instance USING btree (cid);


--
-- Name: idx_instance03; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_instance03 ON public.instance USING btree (id);


--
-- Name: idx_instance04; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_instance04 ON public.instance USING btree (sg);


--
-- Name: idx_proj01; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_proj01 ON public.proj USING btree (cid);


--
-- Name: idx_proj02; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_proj02 ON public.proj USING btree (disporder);


--
-- Name: idx_rate1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_rate1 ON public.rate USING btree (ym);


--
-- Name: idx_sg01; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_sg01 ON public.sg USING btree (cid);


--
-- Name: idx_sg02; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_sg02 ON public.sg USING btree (id);


--
-- Name: idx_stat_ebs1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_stat_ebs1 ON public.stat_ebs USING btree (cid, ym);


--
-- Name: idx_stat_inst1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_stat_inst1 ON public.stat_inst USING btree (cid, ym, instancetype);


--
-- Name: idx_stat_s31; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_stat_s31 ON public.stat_s3 USING btree (cid, ym);


--
-- Name: idx_users1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX idx_users1 ON public.users USING btree (cid);


--
-- Name: idx_volumes01; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_volumes01 ON public.volumes USING btree (cid);


--
-- Name: idx_volumes02; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_volumes02 ON public.volumes USING btree (stragetype);


--
-- Name: idx_volumes03; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_volumes03 ON public.volumes USING btree (volid);


--
-- Name: inv2_001; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX inv2_001 ON public.invoice2 USING btree (costacctid, ym);


--
-- PostgreSQL database dump complete
--

